<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/testgeo',function(){
    return view('testgeo');
  });
// Route::get('/getnasionaltactical', 'GraberController@getnasionaltactical');
// Route::get('/gettacticalhmtl', 'GraberController@gettacticalhmtl');
// Route::get('/kpiperbandinganioanwitel', 'GraberController@kpiperbandinganioanwitel');

// Route::get('/grabwmta', 'GraberController@grabWMTA');
// Route::get('/grabkpiioan', 'GraberController@grabKpiIoan');
// Route::get('/grabsaldounspec', 'GraberController@grabSaldoUnspec');
// Route::get('/grabprodunspec', 'GraberController@grabProdUnspec');
Route::get('/login', 'LoginController@loginPage');
Route::post('/login', 'LoginController@login');
Route::post('/TOMMANAUTH', 'LoginController@loginAPI');
Route::post('/TOMMANAKUN', 'LoginController@getAkun');
Route::get('/logout', 'LoginController@logout');
// Route::get('/grab_kpro_val', 'GraberController@grab_kpro_val');
// Route::get('/grab_kpro_tot', 'GraberController@grab_kpro_tot');
Route::get('/pleaseUpdate', 'OrderController@pleaseUpdate');
// Route::get('/syncRemo', 'ReportController@grab_remo');
// Route::get('/gnossa', 'GraberController@grabNosa');
// Route::get('/stoknocss', 'ToolsController@alistaStocknocss');

Route::get('/portal', 'QPortalController@index');
Route::get('/portal/{id}', 'QPortalController@form');
Route::post('/portal/{id}', 'QPortalController@save');
Route::get('/newuser/input', 'UserController@formnewuser');
// Route::get('/rekapracingpoin/{bln}', 'GraberController@rekapracingpoin');

Route::post('/upload_photo_ajx', 'OrderController@update_photo_ajax');

Route::get('/get_valins', 'OrderController@get_valins_data');

// Route::post('/newuser/input', 'UserController@savenewuser');
Route::group(['middleware' => 'tomman.auth'], function () {

  Route::get('/theme', 'UserController@themes');
  Route::post('/theme', 'UserController@themeSave');

  Route::get('/', 'HomeController@index');
  Route::get('/ibooster/{spd}', 'GraberController@grabIbooster');
  Route::get('/check_umur_inet', 'OrderController@ajax_umur_speedy');
  Route::get('/check_dup_OI', 'OrderController@ajax_order_id');
  Route::get('/ajaxHome/{id}/{jenis_order}', 'HomeController@ajaxHome');
	//tech
  Route::get('/hometech', 'OrderController@hometech');
  Route::get('/listabsen', 'OrderController@listabsen');
  Route::get('/listorder/{jenis}', 'OrderController@listorder');

  Route::get('/tech', 'OrderController@tech');
  Route::get('/logistik', 'OrderController@logistik');
  Route::post('/logistik', 'OrderController@listLogistik');
  Route::get('/monitor', 'OrderController@updated');
  Route::get('/tech-photos/{id}', 'OrderController@upload');
  Route::PUT('/tech-photos/{id}', 'OrderController@uploadSave');
  Route::get('/tech/{id}', 'OrderController@update');
  Route::get('/download_rfc/id/{id}', 'OrderController@download_file_abd');
  Route::put('/tech/{id}', 'OrderController@saveProgres');
  Route::post('/change_tiket/{id}', 'OrderController@change_ticket');
  Route::post('/decpass/{id}', 'OrderController@add_log_manual');
  Route::get('/getActionCause/{id}', 'OrderController@action_cause');
  //Route::get('/tech/{id}', 'OrderController@input');


  //ABSEN
  //Route::get('/absen', 'HomeController@absen');
  Route::post('/absenTech', 'HomeController@absenTech');
  Route::post('/verifAbsen', 'HomeController@absenVerif');

  //Route::get('/maintaince/search', 'MaintainceController@search');
    //Route::get('/maintaince/dummy', 'MaintainceController@listDummy');
    //Route::get('/maintaince/dummy/{id}', 'MaintainceController@insertDummyForm');
    //Route::post('/maintaince/dummy/{id}', 'MaintainceController@insertDummy');
    //ROute::PUT('/tech/{id}', 'MaintainceController@save');
    //ROute::DELETE('/maintaince/{id}', 'MaintainceController@destroy');
    //Route::get('/maintaince/getRekapBulanan/{id}', 'MaintainceController@getRekapBulanan');
    //Route::get('/maintaince/getMtRegu/{id}', 'MaintainceController@getMtRegu');
    //Route::get('/laporan/mt/mt_regu', 'LaporanController@mt_regu');
    //Route::get('/laporan/mt/mt_rekon', 'LaporanController@mt_rekon');
    //Route::get('/laporan/mt/mt_tiket', 'LaporanController@mt_tiket');
    //Route::get('/laporan/maintenanceExcel', 'MaintainceController@cekExcel');
    //Route::get('/maintenanceExcel', 'MaintainceController@cekExcel');
    //Route::get('/maintaince/{id}', 'MaintainceController@input');
  	//Route::get('/maintaince/status/{id}', 'MaintainceController@index');
	//order
  Route::get('/order/{id}', 'OrderController@input');
  Route::post('/order/{id}', 'OrderController@saveOrder');
  // Route::get('/fs/tes', 'OrderController@tes_bulk');
  Route::get('/ajaxOrder/{id}/{datel}/{tgl}', 'OrderController@ajax_matrik');
  Route::post('/refer', 'OrderController@refer');
  Route::get('/inossa/{id}', 'NossaController@registerTiketNossa');
  Route::post('/inossa/{id}', 'OrderController@saveOrder');
  Route::get('/orderM/list/{tgl}', 'OrderController@orderList');
  Route::get('/search/ibooster', 'OrderController@ibooster_search');
  Route::post('/ibooster/status/{jns}/{id}', 'OrderController@update_ibooster');
  Route::get('/order/{jns}/{sts}/{tgl}/{datel}', 'OrderController@orderdetail');
  Route::get('/ajax/refer_search', 'OrderController@ajax_refer');
  Route::get('/filter/{sd}/{ed}/{sts}/{jns}', 'OrderController@filter');
  Route::get('/listRemo/{id}', 'OrderController@listRemo');
  Route::get('/excelfoto/{jenis_order}/{tgl}', 'ReportController@toExcelFoto');
  Route::get('/excelfotoHtml/{jenis_order}/{tgl}', 'ReportController@toExcelFotoHtml');

  //register penutupan odp by teknisi marina
  Route::get('/registerODPTerbuka/{id}', 'OrderController@registerODPTerbuka');
  Route::post('/registerODPTerbuka/{id}', 'OrderController@saveRegisterODPTerbuka');

  //dispatch
  Route::get('/jsonOrder/{id}', 'OrderController@getJsonMaint');
  Route::get('/jsonOrderRemo/{notik}', 'OrderController@getJsonRemo');
  Route::get('/jsonAnak/{id}', 'OrderController@ajax_anak_tiket');
  Route::get('/jsonPsb/{id}', 'OrderController@getJsonPsb');

  //disable dispatch sementara
  Route::post('/dispatchOrder', 'OrderController@dispatchOrder');
  Route::post('/dispatchOrderRemo', 'OrderController@dispatchOrderRemo');
  Route::post('/deleteOrder', 'OrderController@deleteOrder');
  Route::post('/getIndukList', 'OrderController@getIndukList');
  //psbtomman
  Route::get('/listtommanpsb/{jns}', 'OrderController@listtommanpsb');
  Route::get('/listtommanpsbCr/{jns}/{stts}', 'OrderController@listtommanpsb_crossover');
  Route::get('/listtommanpsb/{jns}/{id}', 'OrderController@registertommanpsb');
  Route::post('/listtommanpsb/{jns}/{id}', 'OrderController@saveregistertommanpsb');
  //psbtommanodpfull
  Route::get('/listodpfulltommanpsb', 'OrderController@listodpfulltommanpsb');
  Route::get('/tpsbcutoff/{id}/{jns}', 'OrderController@tpsbcutoff');
  Route::get('/tpsbfixdispatch/{id}/{jns}', 'OrderController@tpsbfixdispatch');

	//user
  Route::get('/newuser', 'UserController@listnewuser');
  Route::get('/user', 'UserController@index');
  Route::get('/user/{id}', 'UserController@userForm');
  Route::post('/user/{id}', 'UserController@update');
  Route::get('/download/user_status/{id}', 'UserController@download_user');

  //regu
  Route::get('/regu', 'UserController@reguList');
  // disable regu sementara
  Route::get('/regu/{id}', 'UserController@reguForm');
  Route::get('/ajax/regu_search', 'UserController@regu_search_ajx');
  Route::post('/regu/{id}', 'UserController@updateRegu');
  // download regu
  Route::get('/download/regu_status/{id}', 'UserController@download_regu');


  //report
  Route::get('/reportAbsen/{date}', 'ReportController@reportAbsen');
  Route::get('/ajaxReportAbsen/{id}/{tgl}', 'ReportController@ajaxReportAbsen');
  Route::get('/matrix/{id}/{jenis_order}/{datel}/{sektor}', 'ReportController@matrix');
  Route::get('/matrix_search/sektor/ajx', 'ReportController@matrix_search_sektor');
  Route::get('/produktifitas/{id}', 'ReportController@produktifitasbulanan');
  Route::get('/prod_all', 'ReportController@all_menu_prod');
  Route::get('/detilprod/{tgl}/{jenis}/{regu}', 'ReportController@detil_produktifitasbulanan');
  Route::get('/cetak/{tgl}/{jenis_rekon}/{verif}', 'ReportController@cetak_excel');
  Route::get('/cetakSubProgram/{tgl}/{jenis_rekon}/{verif}/{generate_evi}/{generate_doc}', 'ReportController@cetak_excel_2');
  Route::get('/dptiang/{date}', 'ReportController@reportDpTiang');
  Route::get('/mitra/prod', 'ReportController@prod_mitra');
  Route::get('/mitra/prod_search/ajx', 'ReportController@prod_mitra_searchlist');
  Route::get('/matrx_prod/list/{jenis}/{regu}/{tgl}/{mitra}/{sektor}/{order}', 'ReportController@prod_mitra_list');
  Route::get('/ajaxTiang/{tgl}/{dtl}', 'ReportController@reportDpTiangAjax');
  Route::get('/tes/akutuh', 'ReportController@proc_report');
  Route::get('/odpLossC/{d}', 'ReportController@Odp_loss_Crossover');
  Route::get('/odpLossC/{jns}/{sts}/{tgl}/{from}', 'OrderController@orderdetail_crossover');
  Route::get('/marina/order/input',function(){
    dd("asd");
  });

  //prod
  Route::get('/prod_sector', 'ReportController@prod_sector');
  Route::get('/prod_sektor_detil/{tgl}', 'ReportController@prod_sektor_detil');

  Route::get('/saber/{tgl}', 'ReportController@saber_list');
  Route::get('/saber_detail/{regu}/{tomman}/{jenis}/{tgl}', 'ReportController@saber_list');

  //chart
  Route::get('/getChartStatus/{id}', 'OrderController@chart_status');

  //verify
  Route::get('/verifRemo', 'OrderController@listRemoClose');
  Route::post('/verifRemo', 'OrderController@verifRemo');
  Route::get('/verify/{tgl}', 'OrderController@dashboard_verify');
  Route::get('/ajaxVerifikasi/{ds}/{dj}/{dt}', 'OrderController@ajax_verify');
  Route::get('/listverify/{tgl}', 'OrderController@list_verify');
  Route::get('/ajaxReport/{query}', 'ReportController@detilReport');

  //decline
  Route::get('/decline/wo', 'OrderController@list_decline');

  Route::get('/dashboard/revenue/{jenis}/{tgl}', 'DashboardController@revenue');
  Route::get('/dashboard/rincianRevenue/{jenis}/{mitra}/{verif}/{tgl}', 'DashboardController@rincianRevenue');
  Route::get('/revenue/program/{tgl}', 'DashboardController@revenue_program_ta');
  Route::get('/rincianRevenueProgram/{tgl}/{jenis_rekon}/{verif}', 'DashboardController@rincianRevenueProgram');
  Route::get('/rincianRevenueProgramTa/{tgl}/{jenis_rekon}/{verif}/{stts}', 'DashboardController@rincianRevenueProgramTa');
  Route::get('/rincianRevenueProgramTa_download/{tgl}/{jenis_rekon}/{verif}/{jenis}/{generate_evi}/{generate_doc}', 'DashboardController@download_rincian_rev');
  Route::get('/revenueTelkom/{tgl}', 'DashboardController@revenuePerProgram');
  Route::get('/reportUmur/{tgl}/{id}', 'ReportController@reportUmur');

  //KHS
  Route::get('/setting/{id}', 'KhsController@setting');
  Route::post('/setting/{id}', 'KhsController@jenisOrderSave');
  Route::get('/khs', 'KhsController@index');
  Route::get('/khs/{id}', 'KhsController@input');
  Route::post('/khs/{id}', 'KhsController@saveKhs');
  Route::post('/delete/setting','KhsController@delete_khs');
  Route::post('/materialsetting/{id}', 'KhsController@save');
  Route::post('/evidentsetting/{id}', 'KhsController@evidentSave');
  Route::post('/evidentDel', 'KhsController@evidentDel');
  Route::get('/grand_sett', 'KhsController@grand_setting');
  Route::get('/sto', 'STOController@index');
  Route::get('/sto/{id}', 'STOController@form');
  Route::post('/sto/{id}', 'STOController@save');
  Route::post('/setRegisterOrder/{id}', 'KhsController@setRegisterOrderField');
  Route::post('/setLaporanOrder/{id}', 'KhsController@setLaporanOrderField');
  Route::get('/mitra', 'MitraController@index');
  Route::get('/mitra/{id}', 'MitraController@form');
  Route::post('/mitra/{id}', 'MitraController@save');
  Route::get('/teritori', 'TeritoriController@index');
  Route::get('/teritori/{id}', 'TeritoriController@form');
  Route::post('/teritori/{id}', 'TeritoriController@save');
  Route::get('/mapping_odc', 'TeritoriController@mapping_odc');
  Route::get('/mapping_odc/{id}', 'TeritoriController@mapping_odc_form');
  Route::post('/mapping_odc/{id}', 'TeritoriController@mapping_odc_save');

  //nossa
  Route::get('/nossa', 'NossaController@index');


  //download
  Route::get('/download', 'ReportController@downloadForm');
  Route::post('/download', 'ReportController@downloadSubmit');
  Route::post('/download_kategory', 'ReportController@downloadSubmit_kategoryal');

  //download underspect
  Route::get('/download_undersp', 'ReportController@downloadFormUN');
  Route::post('/download_undersp', 'ReportController@download_undresp');
  //tools
  Route::get('/stok', 'ToolsController@alistaStock');

  //saldo
  Route::get('/saldo', 'SaldoController@index');
  Route::get('/saldo/input', 'SaldoController@form');
  Route::post('/saldo/input', 'SaldoController@searchandsave');

  Route::get('/matriksaldo', 'SaldoController@matrik');
  Route::get('/outstanding', 'SaldoController@outstanding');
  Route::get('/modallist/{rfv}', 'SaldoController@listbyrfc');


  Route::get('/revenuev2/{mitra}/{tgl}', 'ReportController@revmitrav2');
  Route::get('/cetak_rev/{tgl}/{jenis_order}/{mitra}/{khs}', 'ReportController@cetak_rev');

  Route::get('/ajaxrev/{sgmt}/{id}/{tgl}', 'ReportController@ajax_rev');
  Route::get('/ajaxdetilrev/{regu}/{order}/{tgl}', 'ReportController@ajaxdetil_rev');

  //repair
  Route::get('/repair/usulanteknisi' , 'RepairController@formusulanteknisi');
  Route::post('/repair/usulanteknisi' , 'RepairController@saveusulanteknisi');
  Route::get('/repair/listusulanteknisi' , 'RepairController@listusulanteknisi');

  Route::get('/repair/matrik' , 'RepairController@index');
  Route::get('/repair/matrik/{jo}/{sts}' , 'RepairController@listmatrik');
  Route::get('/repair/approval' , 'RepairController@list_approval');
  Route::get('/repair/approval/{id}' , 'RepairController@form_approval');
  Route::post('/repair/approval/{id}' , 'RepairController@save_approval');
  Route::get('/repair/dispatch' , 'RepairController@list_todispatch');
  Route::get('/repair/dispatch/{id}' , 'RepairController@form_dispatch');
  Route::post('/repair/dispatch/{id}' , 'RepairController@save_dispatch');

  Route::get('/repair/{id}', 'RepairController@input');
  Route::post('/repair/{id}', 'RepairController@save');
  Route::get('/repair/getLogs/{id}', 'RepairController@getLogs');

  //qc odp sehat
  Route::get('/qc/{id}/{dt}', 'ReportController@qc');

  //form validasi port
  Route::get('/validasi/{id}', 'OrderController@formValidasi');
  Route::post('/validasi/{id}', 'OrderController@saveValidasi');
  Route::get('/validasiGetJson/{id}', 'OrderController@getJsonPort');
  Route::post('/saveValidasiKap/{id}', 'OrderController@saveJumlahPort');


  Route::post('/saveValidasiKapAwal/{id}', 'OrderController@saveJumlahPortAwal');
  Route::get('/validasiawal/{id}', 'OrderController@formValidasiAwal');
  Route::post('/validasiawal/{id}', 'OrderController@saveValidasiAwal');
  Route::get('/validasiGetJsonAwal/{id}', 'OrderController@getJsonPortAwal');


  Route::post('/nextStep/{id}', 'OrderController@nextstep');

  Route::get('/reboundaryGetJson/{id}', 'OrderController@getJsonReboundary');
  Route::get('/reboundary/{id}', 'OrderController@reboundaryForm');
  Route::post('/reboundary/{id}', 'OrderController@saveReboundary');


  //commerce
  Route::get('/commerce/admin/list', 'CommerceController@adminList');
  Route::get('/commerce/admin/{id}', 'CommerceController@input');
  Route::post('/commerce/admin/{id}', 'CommerceController@adminSave');

  Route::post('/input_cutoff', 'CommerceController@submit_cutoff');
  Route::get('/get_undo_cutoff', 'CommerceController@get_cutoff');
  Route::get('/undo_cutoff/{id}', 'CommerceController@undo_cutoff');

  Route::get('/commerce/approval/list', 'CommerceController@approvalList');
  Route::get('/commerce/approval/{id}', 'CommerceController@approval');
  Route::post('/commerce/approval/{id}', 'CommerceController@approvalSave');

  //procurement
  Route::get('/proc/admin/list', 'ProcurementController@adminList');
  Route::get('/proc/admin/{id}', 'ProcurementController@input');
  Route::post('/proc/admin/{id}', 'ProcurementController@adminSave');
  Route::get('/proc/ajax/hist', 'ProcurementController@ajax_hist');
  Route::get('/proc/excel/{id}', 'CommerceController@excel_download');

  //pembuatan SP
  Route::get('/proc/pembuatansp/list', 'ProcurementController@pembuatanSPList');
  Route::get('/proc/pembuatansp/{id}', 'ProcurementController@pembuatanSP');
  Route::post('/proc/pembuatansp/{id}', 'ProcurementController@pembuatanSPSave');

  //pemberkasan
  Route::get('/proc/pemberkasan/list', 'ProcurementController@pemberkasanList');
  Route::get('/proc/pemberkasan/{id}', 'ProcurementController@pemberkasan');
  Route::post('/proc/pemberkasan/{id}', 'ProcurementController@pemberkasanSave');

  //verifikasi
  Route::get('/proc/verifikasi/list', 'ProcurementController@verifikasiList');
  Route::get('/proc/verifikasi/{id}', 'ProcurementController@verifikasi');
  Route::post('/proc/verifikasi/{id}', 'ProcurementController@verifikasiSave');

  //pengirimandoc
  Route::get('/proc/pengirimandoc/list', 'ProcurementController@pengirimandocList');
  Route::get('/proc/pengirimandoc/{id}', 'ProcurementController@pengirimandoc');
  Route::post('/proc/pengirimandoc/{id}', 'ProcurementController@pengirimandocSave');

  //finance
  Route::get('/proc/finance/list', 'ProcurementController@financeList');
  Route::get('/proc/finance/{id}', 'ProcurementController@finance');
  Route::post('/proc/finance/{id}', 'ProcurementController@financeSave');

  //performance
  Route::get('/kpi', 'KpiController@index');

  //input performansi teknisi manual
  Route::get('/inputValins/{id}', 'OrderController@formValins');
  Route::post('/inputValins/{id}', 'OrderController@saveFormValins');
  Route::get('/order_manual', 'KpiController@listordermanual');
  Route::get('/order_manual/{id}', 'KpiController@update');
  Route::post('/order_manual/{id}', 'KpiController@save');

  //pwd sso

  //NOG
  Route::get('/list_saldo/{id}/{d}', 'NogController@dashboard_nog');
  Route::get('/detail_list/nog/{sto}/{jenis}/{date}', 'NogController@detail_Listnog');
  Route::get('/detail_list/kondisi_alpro/{sto}/{jenis}/{date}', 'NogController@detail_kondisi_alpro');

  Route::get('/download_alpro/{sto}/{date}/{generate_evi}/{generate_doc}', 'NogController@download_alpro');

  Route::get('/nog_manual/input', 'NogController@input_nog');
  Route::post('/nog_manual/input', 'NogController@save_nog_manual');

  Route::get('/action_nog/{id}', 'NogController@edit_nog');
  Route::post('/action_nog/{id}', 'NogController@update_nog');

  Route::get('/lapor_alpro/{id}', 'NogController@add_order_alpro');
  Route::post('/lapor_alpro/{id}', 'NogController@save_order_alpro_bot');

  Route::get('/order_Nog/{id}', 'OrderController@input_Nog');
  Route::post('/order_Nog/{id}', 'OrderController@saveOrder');
  //dashboard sisa saldo
  Route::get('/remaining_saldo/{d}', 'ReportController@remaining_saldo');
  Route::get('/kalender_alpro/{m}', 'NogController@kalender_alpro');
  Route::get('/lapor_active', 'NogController@lapor_active');
  Route::get('/lapor_active/{id}/{date}', 'NogController@lapor_active_list_wo');
});
Route::get('/changepwd/{nik}', 'HomeController@formPwd');
Route::post('/changepwd/{nik}', 'HomeController@savePwd');

//klasifikasi odc
Route::get('/classification/odc', 'OrderController@list_odc_alpro');
Route::get('/classification/list/{id}', 'OrderController@detail_list_sector');