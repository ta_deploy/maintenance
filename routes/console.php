<?php

use Illuminate\Foundation\Inspiring;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\GraberController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ToolsController;
/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');
Artisan::command('sendReport {id}', function ($id) {
	OrderController::sendtotelegram($id);
});
Artisan::command('sendDispatch {id}', function ($id) {
	OrderController::senddispatchtotelegram($id);
});

Artisan::command('pleaseUpdate', function () {
	OrderController::pleaseUpdate();
});
Artisan::command('pleaseVerif', function () {
	OrderController::pleaseVerif();
});
Artisan::command('getQuestionPortal', function () {
	GraberController::getQuestionPortal();
});
Artisan::command('alistaStocknocssSend', function () {
	ToolsController::alistaStocknocssSend();
});
Artisan::command('grabAlistaByRfc {rfc}', function ($rfc) {
	GraberController::grabAlistaByRfc($rfc);
});
Artisan::command('grabNossa', function () {
	GraberController::grabNosa();
});
Artisan::command('GrabMaterialAlistaBaDigital {date}', function ($date) {
	GraberController::MaterialAlistaBaDigital($date);
});
Artisan::command('sso', function () {
	LoginController::cek_all_login_sso();
});
Artisan::command('ssobyuser {user}', function ($user) {
	LoginController::cek_login_sso_by_user($user);
});
Artisan::command('sso_activation', function () {
	LoginController::sso_activation();
});
Artisan::command('sso_renewal {user}', function ($user) {
	LoginController::sso_renewal($user);
});
Artisan::command('valinsphp {idval} {port}', function ($idval, $port) {
	GraberController::valinsphp($idval, $port);
});
Artisan::command('grab_val', function () {
	GraberController::grab_valins2();
});


