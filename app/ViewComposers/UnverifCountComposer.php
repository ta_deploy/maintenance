<?php

namespace App\ViewComposers;

use Illuminate\Http\Request;
use Illuminate\View\View;

use App\DA\Khs;

class UnverifCountComposer
{
    protected $req;

    public function __construct(Request $req)
    {
        $this->req = $req;
    }

    public function compose(View $view)
    {
        $auth = $this->req->session()->get('auth');
        $unverif = Khs::countUnverif($auth->id_karyawan, $auth->maintenance_level);
        $view->with('unverifCount', ['unverif' => $unverif]);
    }
}