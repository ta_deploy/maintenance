<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\DA\Repair;
use Validator;
//use Telegram;
class RepairController extends Controller
{
    protected $file = [
      'boq', 'kml', 'otdr'
    ];

    public function formusulanteknisi()
    {
        return view('repair.usulanteknisi');
    }
    public function saveusulanteknisi(Request $req)
    {
        $auth = session('auth');
            $id = Repair::registerByTeknisi($req->only("jenis_alpro","nama_alpro","catatan"));
            Repair::insertLog(["repair_id"=>$id, "update_by"=>$auth->id_user."/".$auth->nama, "catatan"=>"registerByTeknisi-".$req->catatan]);
        return redirect()->back();
    }
    public function input($id)
    {
        $data = Repair::getById($id);
        return view('repair.input', compact('data'));
    }
    public function save($id, Request $req)
    {
        $auth = session('auth');
        $data = Repair::getById($id);
        if($data){
            //update
            Repair::registerUpdate($id,$req->only("jenis_alpro","nama_alpro","rab","catatan"));
            $query_file = $this->handleFileUpload($req,$id,$this->file,public_path().'/upload/repair/'.$id.'/');
            if($query_file){
              Repair::updateUsulan($id, $query_file);
            }
            Repair::insertLog(["repair_id"=>$id, "update_by"=>$auth->id_user."/".$auth->nama, "catatan"=>"Register-".$req->catatan]);
        }else{
            //input
            $id = Repair::register($req->only("jenis_alpro","nama_alpro","rab","catatan"));
            $query_file = $this->handleFileUpload($req,$id,$this->file,public_path().'/upload/repair/'.$id.'/');
            if($query_file){
              Repair::updateUsulan($id, $query_file);
            }
            Repair::insertLog(["repair_id"=>$id, "update_by"=>$auth->id_user."/".$auth->nama, "catatan"=>"Register-".$req->catatan]);
        }
        return view('repair.input', compact('data'));
    }
    private function handleFileUpload($request, $id, $file, $path){
        $nama = array();
        foreach($file as $name) {
          $input = $name;
          if ($request->hasFile($input)) {
            // dd($input);

            if (!file_exists($path)) {
              if (!mkdir($path, 0770, true))
                return 'Gagal menyiapkan folder';
            }
            $file = $request->file($input);
            //TODO: path, move, resize
            try {
                $ext = $file->clientExtension();
              $moved = $file->move("$path", "$name.$ext");
              $nama[$name] = "$name.$ext";
            }
            catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
              return 'Gagal upload';
            }
          }
        }
        return $nama;
    }
    //matrik

    public function index()
    {
        $list = Repair::getMatrik();
        return view('repair.matrik', compact('list'));
    }
    public function getLogs($id)
    {
        $list = Repair::getLogs($id);
        return json_encode($list);
    }

    public function listusulanteknisi()
    {
        $list = Repair::getByStatus('registered_by_teknisi');
        return view('repair.list', compact('list'));
    }
    public function listmatrik($jo, $sts)
    {
        $list = Repair::getMatrikList($jo,$sts);
        return view('repair.list', compact('list'));
    }
    //approval
    public function list_approval()
    {
        $list = Repair::getByStatus('registered');
        return view('repair.list', compact('list'));
    }
    public function form_approval($id)
    {
        $data = Repair::getById($id);
        return view('repair.formapproval', compact('data'));
    }
    public function save_approval($id, Request $req)
    {
        $auth = session('auth');
        Repair::updateUsulan($id, $req->only("status","catatan"));
        Repair::insertLog(["repair_id"=>$id, "update_by"=>$auth->id_user."/".$auth->nama, "catatan"=>"Approval-".$req->status."-".$req->catatan]);
        return redirect('/repair/approval');
    }
    public function list_todispatch()
    {
        $list = Repair::getByStatus('disetujui');
        return view('repair.list', compact('list'));
    }
    public function form_dispatch($id)
    {
        $data = Repair::getById($id);
        $regu = DB::table('regu')->select('*','id_regu as id', 'uraian as text')->get();
        $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();
        return view('repair.formdispatch', compact('data', 'sto', 'regu'));
    }
    public function save_dispatch($id, Request $req)
    {
        $auth = session('auth');
        $rules = array(
            'headline' => 'required',
            'koordinat' => 'required',
            'sto' => 'required',
            'regu' => 'required'
          );
          $messages = [
            'headline.required' => 'Silahkan isi kolom "Headline" Bang!',
            'koordinat.required' => 'Silahkan isi kolom "Koordinat" Bang!',
            'sto.required' => 'Silahkan Pilih "sto" Bang!',
            'regu.required' => 'Silahkan Pilih "REGU" Bang!'
          ];
          $validator = Validator::make($req->all(), $rules, $messages);
          if ($validator->fails()) {
            return redirect()->back()
                ->withInput($req->all())
                ->withErrors($validator)
                ->with('alerts', [
                  ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan order']
                ]);
          }
        $data = Repair::isOrdered($id);
        if(!$data){
            $datasto = DB::table('maintenance_datel')->where('sto', $req->sto)->first();
            $regu = DB::table('maintenance_regu')->where('id', $req->regu)->first();
            $id_mt = DB::table('maintaince')->insertGetId([
              "no_tiket"          =>$req->order_id,
              "jenis_order"       =>$req->jenis_order,
              "nama_order"        =>'Repair ALPRO',
              "headline"          =>$req->headline,
              "pic"               =>$auth->id_karyawan."/".$auth->nama,
              "koordinat"         =>$req->koordinat,
              "created_at"        =>DB::raw('now()'),
              "created_by"        =>$auth->id_karyawan,
              "repair_id"         =>$id,
              "kandatel"          =>$datasto->datel,
              "sto"               =>$req->sto,
              "nama_odp"          =>$req->nama_odp,
              "dispatch_regu_id"  =>@$regu->regu,
              "dispatch_regu_name"=>@$regu->uraian,
              "username1"         =>@$regu->nik1,
              "username2"         =>@$regu->nik2
            ]);

            if($req->input('regu')){
              // DB::table('maintenance_dispatch_log')->insert([
              //       "maintenance_id"  =>$id_mt,
              //       "regu_id"         =>@$regu->regu,
              //       "regu_name"       =>@$regu->uraian,
              //       "ts_dispatch"     =>DB::raw('now()'),
              //       "dispatch_by"     =>$auth->id_karyawan
              // ]);
              DB::table('maintenance_note')
              ->insert([
                'mt_id'     => $id_mt,
                'catatan'   => "SAVE DISPATCH",
                'status'   => "SAVE DISPATCH",
                'pelaku'    => session('auth')->id_karyawan,
                'ts'     => DB::raw('NOW()')
              ]);
              exec('cd ..;php artisan sendDispatch '.$id_mt.' > /dev/null &');
            }
            Repair::updateUsulan($id, ["status"=>"in-tech", "catatan"=>"dispatch ke tim".@$regu->nama_regu]);
            Repair::insertLog(["repair_id"=>$id, "update_by"=>$auth->id_user."/".$auth->nama, "catatan"=>"Dispatch-".@$regu->nama_regu."-".$req->catatan]);
            return redirect('/repair/dispatch');
        }else{
            return redirect()->back()
                ->with('alerts', [
                  ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Order Sudah Ada!!']
                ]);
        }
    }
}
