<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\DA\Mitra;
class MitraController extends Controller
{
    public function index(){
      $data = Mitra::getAll();
      return view('setting.mitraList', compact('data'));
    }
    public function form($id){
      $data = Mitra::getById($id);
      return view('setting.mitraForm', compact('data'));
    }
    public function save($id, Request $req){
      $exists = Mitra::getById($id);
      $query = [
        'mitra'=>$req->mitra,
        'label'=>$req->label,
        'isAktif'=>$req->isAktif
      ];
      if($exists){
        Mitra::update($id, $query);
      }else{
        Mitra::insert($query);
      }
      return redirect('/mitra');
    }
    
}
