<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests;
use DB;
use Validator;
use App\DA\OrderManual;
class KpiController extends Controller
{
  public function index()
  {
    //q=1
    $sector = DB::table('kpi_sector_ioan')->select('NAMA_SEKTOR')->where('NAMA_SEKTOR','<>','')->groupBy('NAMA_SEKTOR')->orderBy('NAMA_SEKTOR', 'ASC')->get();
    $sectorarray = array();
    $x=array("x");
    for($i=1;$i<=date("t");$i++){
      // var_dump(strlen($i));
      if(strlen($i)==1){
        $x[]=date('Y-m-')."0".$i;
      }else{
        $x[]=date('Y-m-').$i;
      }
    }
    // dd($x);
    foreach($sector as $in => $s){
      //get array sector
      $sectorarray[$s->NAMA_SEKTOR] = $in;
      //init days in key json
      $q[$in] = array("NAMA_SEKTOR"=>$s->NAMA_SEKTOR,"TARGET"=>"-");
      // $qChart[0]=$x;
      $qChart[$in][0]=str_replace('SEKTOR - KALSEL - ','',$s->NAMA_SEKTOR);

      $sugar[$in] = array("NAMA_SEKTOR"=>$s->NAMA_SEKTOR,"TARGET"=>"-");
      $sugarChart[$in][0]=str_replace('SEKTOR - KALSEL - ','',$s->NAMA_SEKTOR);

      $ttr3[$in] = array("NAMA_SEKTOR"=>$s->NAMA_SEKTOR,"TARGET"=>"-");
      $ttr3Chart[$in][0]=str_replace('SEKTOR - KALSEL - ','',$s->NAMA_SEKTOR);

      $ttr12[$in] = array("NAMA_SEKTOR"=>$s->NAMA_SEKTOR,"TARGET"=>"-");
      $ttr12Chart[$in][0]=str_replace('SEKTOR - KALSEL - ','',$s->NAMA_SEKTOR);

      $valda[$in] = array("NAMA_SEKTOR"=>$s->NAMA_SEKTOR,"TARGET"=>"-");
      $valdaChart[$in][0]=str_replace('SEKTOR - KALSEL - ','',$s->NAMA_SEKTOR);

      $tangibleKuan[$in] = array("NAMA_SEKTOR"=>$s->NAMA_SEKTOR,"TARGET"=>"-");
      $tangibleKuanChart[$in][0]=str_replace('SEKTOR - KALSEL - ','',$s->NAMA_SEKTOR);

      $tangibleCheck[$in] = array("NAMA_SEKTOR"=>$s->NAMA_SEKTOR,"TARGET"=>"-");
      $tangibleCheckChart[$in][0]=str_replace('SEKTOR - KALSEL - ','',$s->NAMA_SEKTOR);

      $unspec[$in] = array("NAMA_SEKTOR"=>$s->NAMA_SEKTOR,"TARGET"=>"-");
      $unspecChart[$in][0]=str_replace('SEKTOR - KALSEL - ','',$s->NAMA_SEKTOR);

      $prod[$in] = array("NAMA_SEKTOR"=>$s->NAMA_SEKTOR,"TARGET"=>"-");
      $prodChart[$in][0]=str_replace('SEKTOR - KALSEL - ','',$s->NAMA_SEKTOR);

      for($i=1;$i<=date("t");$i++){
        $q[$in][$i] = '-';
        $qChart[$in][$i] = '0';

        $sugar[$in][$i] = '-';
        $sugarChart[$in][$i] = '0';

        $ttr3[$in][$i] = '-';
        $ttr3Chart[$in][$i] = '0';

        $ttr12[$in][$i] = '-';
        $ttr12Chart[$in][$i] = '0';

        $valda[$in][$i] = '-';
        $valdaChart[$in][$i] = '0';

        $tangibleKuan[$in][$i] = '-';
        $tangibleKuanChart[$in][$i] = '0';

        $tangibleCheck[$in][$i] = '-';
        $tangibleCheckChart[$in][$i] = '0';

        $unspec[$in][$i] = '-';
        $unspecChart[$in][$i] = '0';

        $prod[$in][$i] = '-';
        $prodChart[$in][$i] = '0';
      }
    }

    $data = DB::table('kpi_sector_ioan')->where('NAMA_SEKTOR','<>','')->where('TGL_Sync','like',date("Y-m-").'%')->get();
    foreach($data as $d){
      if($d->ID_IND==1){
        $q[$sectorarray[$d->NAMA_SEKTOR]][substr($d->TGL_Sync, 8,1)?substr($d->TGL_Sync, 8,2):substr($d->TGL_Sync, 9,1)]=$d->REALISASI;
        $qChart[$sectorarray[$d->NAMA_SEKTOR]][substr($d->TGL_Sync, 8,1)?substr($d->TGL_Sync, 8,2):substr($d->TGL_Sync, 9,1)]=$d->REALISASI;
      }
      if($d->ID_IND==2){
        $sugar[$sectorarray[$d->NAMA_SEKTOR]][substr($d->TGL_Sync, 8,1)?substr($d->TGL_Sync, 8,2):substr($d->TGL_Sync, 9,1)]=$d->REALISASI;
        $sugarChart[$sectorarray[$d->NAMA_SEKTOR]][substr($d->TGL_Sync, 8,1)?substr($d->TGL_Sync, 8,2):substr($d->TGL_Sync, 9,1)]=$d->REALISASI;
      }
      if($d->ID_IND==3){
        $ttr3[$sectorarray[$d->NAMA_SEKTOR]][substr($d->TGL_Sync, 8,1)?substr($d->TGL_Sync, 8,2):substr($d->TGL_Sync, 9,1)]=$d->REALISASI;
        $ttr3Chart[$sectorarray[$d->NAMA_SEKTOR]][substr($d->TGL_Sync, 8,1)?substr($d->TGL_Sync, 8,2):substr($d->TGL_Sync, 9,1)]=$d->REALISASI;
      }
      if($d->ID_IND==4){
        $ttr12[$sectorarray[$d->NAMA_SEKTOR]][substr($d->TGL_Sync, 8,1)?substr($d->TGL_Sync, 8,2):substr($d->TGL_Sync, 9,1)]=$d->REALISASI;
        $ttr12Chart[$sectorarray[$d->NAMA_SEKTOR]][substr($d->TGL_Sync, 8,1)?substr($d->TGL_Sync, 8,2):substr($d->TGL_Sync, 9,1)]=$d->REALISASI;
      }
      if($d->ID_IND==5){
        $valda[$sectorarray[$d->NAMA_SEKTOR]][substr($d->TGL_Sync, 8,1)?substr($d->TGL_Sync, 8,2):substr($d->TGL_Sync, 9,1)]=$d->REALISASI;
        $valdaChart[$sectorarray[$d->NAMA_SEKTOR]][substr($d->TGL_Sync, 8,1)?substr($d->TGL_Sync, 8,2):substr($d->TGL_Sync, 9,1)]=$d->REALISASI;
      }
      if($d->ID_IND==8){
        $tangibleKuan[$sectorarray[$d->NAMA_SEKTOR]][substr($d->TGL_Sync, 8,1)?substr($d->TGL_Sync, 8,2):substr($d->TGL_Sync, 9,1)]=$d->REALISASI;
        $tangibleKuanChart[$sectorarray[$d->NAMA_SEKTOR]][substr($d->TGL_Sync, 8,1)?substr($d->TGL_Sync, 8,2):substr($d->TGL_Sync, 9,1)]=$d->REALISASI;
      }
      if($d->ID_IND==9){
        $tangibleCheck[$sectorarray[$d->NAMA_SEKTOR]][substr($d->TGL_Sync, 8,1)?substr($d->TGL_Sync, 8,2):substr($d->TGL_Sync, 9,1)]=$d->REALISASI;
        $tangibleCheckChart[$sectorarray[$d->NAMA_SEKTOR]][substr($d->TGL_Sync, 8,1)?substr($d->TGL_Sync, 8,2):substr($d->TGL_Sync, 9,1)]=$d->REALISASI;
      }
    }
    $data = DB::table('saldo_unspec_sektor')->where('SEKTOR','<>','')->where('TGL_UPDATE','like',date("Y-m-").'%')->get();
    foreach($data as $d){
      $unspec[$sectorarray[$d->SEKTOR]][substr($d->TGL_UPDATE, 8,1)?substr($d->TGL_UPDATE, 8,2):substr($d->TGL_UPDATE, 9,1)]=$d->TOTAL_SALDO;
      $unspecChart[$sectorarray[$d->SEKTOR]][substr($d->TGL_UPDATE, 8,1)?substr($d->TGL_UPDATE, 8,2):substr($d->TGL_UPDATE, 9,1)]=str_replace(',', '', $d->TOTAL_SALDO);
    }
    $data = DB::table('saldo_unspec_semesta')->where('SEKTOR','<>','')->where('TGL_UPDATE','like',date("Y-m-").'%')->get();
    foreach($data as $d){
      $prod[$sectorarray[$d->SEKTOR]][substr($d->TGL_UPDATE, 8,1)?substr($d->TGL_UPDATE, 8,2):substr($d->TGL_UPDATE, 9,1)]=$d->REAL_CLOSE;
      $prodChart[$sectorarray[$d->SEKTOR]][substr($d->TGL_UPDATE, 8,1)?substr($d->TGL_UPDATE, 8,2):substr($d->TGL_UPDATE, 9,1)]=str_replace(',', '', $d->REAL_CLOSE);
    }
    // dd($qChart);
    $qChart[]=$x;
    $sugarChart[]=$x;
    $ttr3Chart[]=$x;
    $ttr12Chart[]=$x;
    $valdaChart[]=$x;
    $tangibleKuanChart[]=$x;
    $tangibleCheckChart[]=$x;
    $unspecChart[]=$x;
    $prodChart[]=$x;
    // dd($sugarChart);
    $qChart=json_encode($qChart);
    $sugarChart= json_encode($sugarChart);
    $ttr3Chart= json_encode($ttr3Chart);
    $ttr12Chart= json_encode($ttr12Chart);
    $valdaChart= json_encode($valdaChart);
    $tangibleKuanChart= json_encode($tangibleKuanChart);
    $tangibleCheckChart= json_encode($tangibleCheckChart);
    $unspecChart= json_encode($unspecChart);
    $prodChart= json_encode($prodChart);

    return view('kpi.index', compact('q', 'sugar', 'ttr3', 'ttr12', 'valda', 'tangibleKuan', 'tangibleCheck','qChart', 'sugarChart', 'ttr3Chart', 'ttr12Chart', 'valdaChart', 'tangibleKuanChart', 'tangibleCheckChart', 'unspec', 'unspecChart', 'prod', 'prodChart'));
  }

  public function listordermanual(){
    $data = OrderManual::list();
    return view('kpi.list', compact('data'));
  }
  public function update($id){
    $data = OrderManual::getById($id);
    $regu = DB::table('regu')->select('*','id_regu as id', 'uraian as text')->where('status_team', 'ACTIVE')->get();
    return view('kpi.input', compact('data', 'regu'));
  }
  public function save($id, Request $req){
    $data = OrderManual::getById($id);
    $regu = DB::table('regu')->where('id_regu', $req->REGU_ID)->first();
    $rules = array(
      'NO_INET' => 'required',
      'NAMA_ODP' => 'required',
      'REGU_ID' => 'required',
      'STATUS' => 'required',
      'TGL_SELESAI' => 'required'
    );
    $messages = [
      'NO_INET.required' => 'Silahkan isi kolom "NO_INET" Bang!',
      'NAMA_ODP.required' => 'Silahkan isi kolom "NAMA_ODP" Bang!',
      'REGU_ID.required' => 'Silahkan Pilih "REGU_ID" Bang!',
      'STATUS.required' => 'Silahkan Pilih "STATUS" Bang!',
      'TGL_SELESAI.required' => 'Silahkan isi "TGL_SELESAI" Bang!'
    ];
    $validator = Validator::make($req->all(), $rules, $messages);
    if ($validator->fails()) {
      return redirect()->back()
          ->withInput($req->all())
          ->withErrors($validator)
          ->with('alerts', [
            ['type' => 'danger', 'text' => '<strong>GAGAL</strong> isian kurang lengkap']
          ]);
    }
    if($regu->uraian && $regu->nama_mitra && ($regu->nik1 || $regu->nik2)){
      //lanjut save
      $sqldata = [
        'NO_INET'     => $req->NO_INET,
        'NAMA_ODP'    => $req->NAMA_ODP,
        'REGU_ID'     => $req->REGU_ID,
        'MITRA'       => $regu->nama_mitra,
        'NAMA_TIM'    => $regu->uraian,
        'NIK1'        => $regu->nik1,
        'NIK2'        => $regu->nik2,
        'STATUS'      => $req->STATUS,
        'TGL_SELESAI' => $req->TGL_SELESAI
      ];
      if(count($data)){
        OrderManual::update($id, $sqldata);
      }else{
        OrderManual::input($sqldata);
      }
      return redirect('/order_manual')
        ->with('alerts', [
          ['type' => 'success', 'text' => '<strong>SUKSES MENYIMPAN DATA</strong>']
        ]);
    }else{
      //kirim error msg lengkapi data regu:nama tim/nama mitra/nik1/nik2
      return redirect()->back()->withInput($req->all())
        ->with('alerts', [
          ['type' => 'success', 'text' => '<strong>regu id : '.$regu->id_regu.', error msg lengkapi data regu:nama tim/nama mitra/nik1/nik2</strong>']
        ]);
    }
  }
}
