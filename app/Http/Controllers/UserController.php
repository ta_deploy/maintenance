<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\DA\User;
use Validator;
use Excel;
//use Telegram;
class UserController extends Controller
{
    protected $photoInputs = [
      'KTP', 'SIM', 'BPJS', 'FOTO'
    ];
    public function listnewuser()
    {
        $list = User::getUserByAproval();
        return view('user.listnewuser', compact('list'));
    }

    public function formnewuser()
    {
        $mitra = DB::table('maintenance_mitra')->select('mitra as id', 'mitra as text')->where('isAktif', 1)->get();
        return view('user.formnewuser', compact('mitra'));
    }
    public function savenewuser(Request $req)
    {
        DB::transaction(function() use($req) {
            $user = User::userExists($req->id_user);
            if(!count($user)){
                User::userCreateNew($req);
            }
            $karyawan = User::karyawanExists($req->id_user);
            if(count($karyawan)){
                User::karyawanUpdate($req);
            }else{
                // User::karyawanCreate($req);
            }
        });
        return back();
    }
    public function index()
    {
        $list = User::getUser();
        return view('user.list', compact('list'));
    }

    public function userForm($id)
    {

        $foto = $this->photoInputs;
        $user = User::getUserById($id);
        $mitra = DB::table('maintenance_mitra')->select('mitra as id', 'mitra as text')->where('isAktif', 1)->get();
        $mitra[] = (object)['id' => 'TELKOM', 'text' => 'TELKOM'];
        return view('user.input', compact('user', 'foto', 'mitra'));
    }

    public function update(Request $req, $id)
    {
    	return DB::transaction(function() use($req, $id) {
            $this->handleFileUpload($req, $id);
	        $user = User::userExists($id);
	        if(count($user)){
                User::userUpdate($req);
	        }else{
                $user = User::userExists($req->id_user);
                if(!$user){
                    User::userCreate($req);
                } else {
                    return redirect('/user')->with('alerts', [
                        ['type' => 'danger', 'text' => '<strong>GAGAL</strong> ID User Sudah Ada!']
                    ]);
                }
	        }
        	$karyawan = User::karyawanExists($id);
        	if(count($karyawan)){
        		User::karyawanUpdate($req);
        	}else{
        		// User::karyawanCreate($req);
        	}
            return redirect()->back();
    	});
    }
    private function handleFileUpload($request, $id)
    {
        foreach($this->photoInputs as $name) {
          $input = 'photo-'.$name;
          if ($request->hasFile($input)) {
            //dd($input);
            $path = public_path().'/upload/profile/'.$id.'/';
            if (!file_exists($path)) {
              if (!mkdir($path, 0770, true))
                return 'gagal menyiapkan folder foto evidence';
            }
            $file = $request->file($input);
            $ext = 'jpg';
            //TODO: path, move, resize
            try {
              $moved = $file->move("$path", "$name.$ext");
              $img = new \Imagick($moved->getRealPath());
              $img->scaleImage(100, 150, true);
              $img->writeImage("$path/$name-th.$ext");
            }
            catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
              return 'gagal menyimpan foto evidence '.$name;
            }
          }
        }
    }
    //regu
    public function reguList()
    {
        $list = User::getRegu();
        // dd($list);
        return view('regu.list', compact('list'));
    }

    public function reguForm($id)
    {
        $regu = User::getReguById($id);
        $mitra = DB::table('maintenance_mitra')->select('mitra as id', 'mitra as text')->where('isAktif', 1)->get();
        $sektor = DB::table('maintenance_sector')->select('label as id', 'sector as text')->get();
        $grup = DB::table('maintenance_grup')->select('*','maintenance_grup.chat_id as id', 'maintenance_grup.grup as text')->get();
        // dd($regu);
        return view('regu.input', compact('regu', 'grup', 'mitra', 'sektor'));
    }
    public function updateRegu(Request $req, $id)
    {
        $rules = array(
            'nama_regu'     => 'required',
            'mitra'         => 'required',
            'spesialis'     => 'required',
            'label_sektor'  => 'required',
            'nik1'          => 'required',
            'niktl'         => 'required',
            'chat_id'       => 'required',
            'status_regu'   => 'required',
        );

        $check_nama_r = DB::table('regu')->Where([
            ['id_regu', '!=', $req->hid],
            ['uraian', $req->nama_regu]
        ])->get();

        if(count($check_nama_r)) {
            return redirect()->back()
            ->withInput($req->all())
            ->with('alerts', [
                ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Nama Regu Sudah Ada!']
            ]);
        }

        $validator = Validator::make($req->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                ->withInput($req->all())
                ->withErrors($validator)
                ->with('alerts', [
                  ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan order']
                ]);
        }

        $mitra = User::getMitraByNama($req->mitra);

        if($req->label_regu) {
            $jenis = 'update';
            if ($req->nomor_urut) {
                $label_regu = $mitra->label."-".$req->label_sektor."-".$req->spesialis."/".sprintf("%02d", $req->nomor_urut);
            } else {
                $exp = explode('/', $req->label_regu);
                $label_regu = $mitra->label."-".$req->label_sektor."-".$req->spesialis."/".sprintf("%02d", $exp[1]);
            }
        } else {
            $jenis = 'new';
            $label_regu = $mitra->label."-".$req->label_sektor."-".$req->spesialis."/01";
        }

        //XXX:cari label
        $check_label = DB::table('regu')->where([
            ['id_regu', $req->hid],
            ['label_regu', $label_regu]
        ])->get();
        //XXX:Jika ini inputan baru / label regu beda dari database
        if (count($check_label) == 0) {
            $exp_reg = explode('/', $label_regu);
            $prev_urut = Intval($exp_reg[1]);

            $get_angka_label = DB::table('regu')->where('label_regu', 'like', $exp_reg[0] .'%')->where('ACTIVE', 1)->get();
            if (count($get_angka_label)) {
                $get_num = [];
                foreach ($get_angka_label as $d) {
                    $exp = explode('/', $d->label_regu);
                    $get_num[intval($exp[1])] = intval($exp[1]);
                }

                if(!in_array($prev_urut, $get_num)){
                    //XXX:tidak ada di urutan label regu nya
                    $result_number = $prev_urut;
                }elseif(in_array($prev_urut, $get_num)){
                    //XXX:ada no urut yang double
                    $get_range = range(min($get_num), max($get_num));
                    $array_res =  array_diff($get_range, $get_num);
                    if(!$array_res){
                        $result_number = max($get_range) + 1;
                    }else{
                        $result_number = min($array_res);
                    }
                    if($jenis == 'update'){
                        return redirect()->back()
                        ->withInput($req->all())
                        ->with('alerts', [
                            ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Nomor Label Regu Sudah Ada! Disarankan No Urut '.$result_number]
                        ]);
                    }
                }
                $label_regu = $mitra->label."-".$req->label_sektor."-".$req->spesialis.'/'.sprintf("%02d", $result_number);
            }
        }

        $exp = explode('/', $label_regu);
        $req->request->add(['nomor_urut' => sprintf("%02d", $exp[1])]);

        $regu = User::getReguById($id);
        if(count($regu)){
            User::reguUpdate($req, $label_regu);
        }else{
            User::reguCreate($req, $label_regu);
        }
        return redirect('/regu')
        ->with('alerts', [
            ['type' => 'success', 'text' => '<strong>Berhasil</strong> Membuat Regu']
        ]);
    }

    public function regu_search_ajx(Request $req){
        $term = trim($req->searchTerm);
        $jenis = $req->jenis;

        if (empty($term)) {
            return \Response::json([]);
        }

        $fetchData = User::getRegu_ajx($term, $jenis);

        $formatted_tags = $group_data = [];
        $msg = 'User Tidak Terdaftar';

        if($fetchData['nik_ada'])
        {
            foreach ($fetchData['nik_ada'] as $row)
            {
                $formatted_tags[] = ["id"=> $row->id_karyawan, "text"=>$row->nama.' ('. $row->id_karyawan .')'];
            }
        }

        if($fetchData['dipakai'])
        {
            $id = $fetchData['dipakai']->id_regu;
            $uraian = $fetchData['dipakai']->uraian;
            $msg = "Nik Terdaftar di Regu <a href='/regu/$id'>$uraian</a>";
        }

        $group_data = ['kosong' => $formatted_tags, 'ada' => $msg];

        return \Response::json($group_data);
    }

    public function download_regu($id)
    {
        if($id == 1)
        {
            $stts= 'Aktif';
        }
        else
        {
            $stts ='Tidak Aktif';
        }
        Excel::create('Dowonload User '.$stts, function ($excel) use ($id) {
            $excel->sheet('sheet1', function ($sheet) use ($id) {
                $data = User::getRegu_download($id);
                $sheet->loadView('regu.excelDownloadRegu', compact('data', 'id'));
            });
        })->download('xlsx');
    }

    public function download_user($id)
    {
        if($id == 1)
        {
            $stts= 'Aktif';
        }
        else
        {
            $stts ='Tidak Aktif';
        }
        Excel::create('Dowonload User '.$stts, function ($excel) use ($id) {
            $excel->sheet('sheet1', function ($sheet) use ($id) {
                $data = User::getUser_download($id);
                $sheet->loadView('user.excelDownloadUser', compact('data', 'id'));
            });
        })->download('xlsx');
    }

    public function themes()
    {

        return view('theme');
    }
    public function themeSave(Request $req )
    {

        session('auth')->pixeltheme=$req->theme;
        DB::table('user')->where('id_user', session('auth')->id_user)->update([
          'pixeltheme'  => $req->theme
        ]);
        return back();
    }
}
