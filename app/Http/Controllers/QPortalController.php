<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DA\QPortal;
//use Telegram;
class QPortalController extends Controller
{
    public function index()
    {
        $list = QPortal::getAll();
        return view('portal.list', compact('list'));
    }
    public function form($id)
    {
        $data = QPortal::getById($id);
        return view('portal.form', compact('data'));
    }
    public function save($id, Request $req)
    {
        QPortal::save($id, $req->jawaban);
        return redirect('/portal');
    }
}
