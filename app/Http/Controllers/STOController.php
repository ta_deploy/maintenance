<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\DA\STO;
class STOController extends Controller
{
    public function index(){
      $data = STO::getAllSTO();
      return view('setting.sto', compact('data'));
    }
    public function form($id){
      $data = STO::getById($id);
      return view('setting.stoform', compact('data'));
    }
    public function save(Request $req, $id){
      $data = STO::getById($id);
      if($data){
        //update
        STO::update($id,["sto"=>$req->sto,"datel"=>$req->datel,"witel"=>$req->witel,"sektor_prov"=>$req->sektor_prov,"kota"=>$req->kota]);
      }else{
        //insert
        STO::insert(["sto"=>$req->sto,"datel"=>$req->datel,"witel"=>$req->witel,"sektor_prov"=>$req->sektor_prov,"kota"=>$req->kota]);
      }
      return redirect('/sto');
    }
}
