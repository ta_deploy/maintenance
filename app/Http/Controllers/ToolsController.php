<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use DB;
use Telegram;
class ToolsController extends Controller
{
    public function alistaStock(){
      $records = DB::table('alista_stok')->get()->toArray();
      $data = array_chunk($records, 57);
      return view('tools.alistaStock', compact('data'));
    }
    public function alistaStocknocss(){
      $records = DB::table('alista_stok')->orderBy('id_barang', 'asc')->get()->toArray();
      $data = array_chunk($records, 43);
      return view('tools.alistaStockUncss', compact('data'));
    }

    public static function alistaStocknocssSend(){
      $path = '/srv/htdocs/puppet_wand/stok.png';
      Telegram::sendPhoto([
        'chat_id' => '-125769259',
        'photo' => $path
      ]);
    }
}
