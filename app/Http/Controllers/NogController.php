<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Excel;
use App\DA\Nog;
use DB;

class NogController extends Controller
{
    public function input_nog()
	{
        $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();
		return view('nog.editNog', compact('data', 'sto') );
	}

    public function save_nog_manual(request $req)
	{
		return Nog::update_or_ins_nog($req, 0);
	}

	public function edit_nog($id)
	{
        $data = Nog::get_id($id);
        $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();
        return view('nog.editNog', compact('data', 'sto') );
	}

	public function update_nog(Request $req, $id)
	{
        return Nog::update_or_ins_nog($req, $id);
	}

	public function dashboard_nog(Request $req, $jenis, $tgl)
	{
        $list_data = $moc = $get_rusak = $jk = $fn = [];

        $tampilan = 'progress';

        if($req->all() )
        {
            $list_data = $req->status;
            $tampilan = $req->tampilan_dashboard;
        }
        // $data = Nog::list_All($list_data, $tgl);

        $get_sto = DB::Table('maintenance_datel')->select('sto')->GroupBy('sto')->get();

        $get_sto = array_map(function($x){
            return $x['sto'];
        }, json_decode(json_encode($get_sto), TRUE) );

        $master_odpCare = DB::table('master_OdpCare As mo')
        ->leftjoin('maintaince As m', 'mo.maintenance_id', '=', 'm.id')
        ->select('mo.*', DB::RAW("(CASE WHEN mo.status_dalapa IS NOT NULL THEN mo.status_dalapa ELSE m.status END) As status") )
        ->Where('mo.created_at', 'LIKE', $tgl.'%')
        ->GroupBy('uniq_id')
        ->get();

        foreach($master_odpCare as $k => $v)
        {
            $jk = json_decode($v->jenis_kerusakan);

            foreach($jk as $vv)
            {
                $get_rusak[$vv] = str_replace('_', ' ', $vv);
            }

            $moc[$k] = $v;
            $sto = strtoupper(@explode('-', $v->odp)[1]) == '' ? 'NA' : strtoupper(@explode('-', $v->odp)[1]);
            $sto_label = 'NA';

            if(in_array($sto, $get_sto) )
            {
                $sto_label = $sto;
            }

            $moc[$k]->sto = $sto_label;
            $moc[$k]->jenis_kerusakan = $jk;
        }

        $grm = array_values(array_column(json_decode(json_encode($moc), TRUE), 'jenis_kerusakan') );

		$data_stts = json_decode(json_encode(Nog::all_stat()), TRUE);
        $gt = array_values(array_unique(array_column($data_stts, 'jenis_order') ) );
        $gt = array_map(function($x){
            if($x === null)
            {
                return 'NO STATUS';
            }
            else
            {
                return str_replace('_', ' ', $x);
            }
        }, $gt);

        foreach($gt as $v)
        {
            $get_stat[str_replace(' ', '_', $v)] = $v;
        }

        // foreach($data as $d)
        // {
        //     $sto = $d->sto_nog ? strtoupper($d->sto_nog) : 'NA';
        //     $status2 = $d->tindak_lanjut ?? 'In_Progress';
        //     $status3 = $d->status_order ?? 'In_Progress';

        //     if(!isset($fn[$sto]['status']) )
        //     {
        //         $fn[$sto]['sto']                 = $sto;
        //         $fn[$sto]['status']['hd_pt1']    = 0;
        //         $fn[$sto]['status']['hd_mt']     = 0;
        //         $fn[$sto]['status']['hd_pt2']    = 0;
        //         $fn[$sto]['status']['pt_1']      = 0;
        //         $fn[$sto]['status']['pt_2']      = 0;
        //         $fn[$sto]['status']['pt_3']      = 0;
        //         $fn[$sto]['status']['kendala']   = 0;
        //         $fn[$sto]['status']['progress']  = 0;
        //         $fn[$sto]['status']['done']      = 0;
        //         $fn[$sto]['status']['init']      = 0;
        //         $fn[$sto]['status']['total']     = 0;
        //         $fn[$sto]['status']['total_all'] = 0;
        //     }

        //     // $fn[$sto]['status']['total_all'] += 1;

        //     if($status3 == 'In_Progress')
        //     {
        //         switch ($status2) {
        //             case 'PT-1':
        //                 $fn[$sto]['status']['pt_1'] += 1;

        //                 if(!$d->dispatch_regu_id)
        //                 {
        //                     $fn[$sto]['status']['hd_pt1'] += 1;
        //                 }
        //             break;
        //             case 'PT-2':
        //                 $fn[$sto]['status']['pt_2'] += 1;

        //                 if(!$d->dispatch_regu_id)
        //                 {
        //                     $fn[$sto]['status']['hd_pt2'] += 1;
        //                 }
        //             break;
        //             case 'PT-3':
        //                 $fn[$sto]['status']['pt_3'] += 1;
        //             break;
        //             default:
        //                 // $fn[$sto]['status']['init'] += 1;
        //             break;
        //         }

        //         // $fn[$sto]['status']['total'] += 1;
        //     }
        //     else
        //     {
        //         // switch ($status3) {
        //         //     case 'DONE':
        //         //         $fn[$sto]['status']['done'] += 1;
        //         //     break;
        //         //     case 'KENDALA':
        //         //         $fn[$sto]['status']['kendala'] += 1;
        //         //     break;
        //         //     default:
        //         //         $fn[$sto]['status']['progress'] += 1;
        //         //     break;
        //         // }
        //     }
        // }

        foreach($moc as $k => $v)
        {
            $status3 = $v->status ?? 'In_Progress';

            if($tampilan == 'progress')
            {
                $sto = strtoupper($v->sto);

                if(!isset($fn[$sto]) )
                {
                    $fn[$sto]['sto']                 = $sto;
                    $fn[$sto]['status']['hd_pt1']    = 0;
                    $fn[$sto]['status']['hd_mt']     = 0;
                    $fn[$sto]['status']['hd_pt2']    = 0;
                    $fn[$sto]['status']['pt_1']      = 0;
                    $fn[$sto]['status']['pt_2']      = 0;
                    $fn[$sto]['status']['pt_3']      = 0;
                    $fn[$sto]['status']['kendala']   = 0;
                    $fn[$sto]['status']['progress']  = 0;
                    $fn[$sto]['status']['done']      = 0;
                    $fn[$sto]['status']['init']      = 0;
                    $fn[$sto]['status']['total']     = 0;
                    $fn[$sto]['status']['total_all'] = 0;
                }

                if(in_array(strtolower($status3), array_map('strtolower', ['close', 'selesai']) ) )
                {
                    $fn[$sto]['status']['done'] += 1;
                }
                elseif(preg_match('/kendala/i', $status3) === 1)
                {
                    $fn[$sto]['status']['kendala'] += 1;
                }
                else
                {
                    if($v->maintenance_id != 0 && is_null($v->status_dalapa) )
                    {
                        $fn[$sto]['status']['progress'] += 1;
                    }
                }

                $fn[$sto]['status']['total_all'] += 1;

                if($v->maintenance_id == 0 && is_null($v->status_dalapa) && $status3 == 'In_Progress')
                {

                    $fn[$sto]['status']['total'] += 1;
                }

                $fn[$sto]['status']['hd_mt'] += 1;
            }
            else
            {
                if(in_array($v->sto, $get_sto) )
                {
                    $find_k_moc = array_search($v->sto, array_column($fn, 'sto') );
                    $sto = strtoupper($v->sto);
                }
                else
                {
                    $sto = 'NA';
                }

                if(!isset($fn[$sto]) )
                {
                    $fn[$sto]['sto'] = $sto;
                    $fn[$sto]['kendala'] = array_map(function($x){
                        return $x = 0;
                    }, array_flip(array_values(array_flip($get_rusak) ) ) );
                }

                foreach($v->jenis_kerusakan as $vj)
                {
                    if(in_array($vj, array_values(array_flip($get_rusak) ) ) )
                    {
                        $fn[$sto]['kendala'][$vj] += 1;
                    }
                }
            }
        }

        if($tampilan == 'progress')
        {
            foreach($fn as $k => &$v)
            {
                if($v['status']['hd_mt'] == 0)
                {
                    unset($fn[$k]);
                }
            }

            unset($v);
        }

        return view('nog.dashboard', compact('fn', 'get_stat', 'list_data', 'get_rusak', 'tampilan') );
	}

    public function download_alpro(request $req, $sto, $jenis, $generate_evi, $generate_doc)
	{
        $list_data = $list_kendala_m = [];

        if($req->all() )
        {
            $list_data = $req->status;
            $list_kendala_m = $req->kendala_maintenance;
        }

        $data = [];

        // $data = Nog::list_param($sto, $jenis, $list_data, $list_kendala_m);

        $foto = DB::table('master_OdpCare As mo')
        ->leftjoin('maintaince As m', 'mo.maintenance_id', '=', 'm.id')
        ->select('m.*', 'mo.flag_kondisi_alpro', DB::RAW("COALESCE(m.nama_odp, mo.odp) As nama_odp, COALESCE(m.headline, mo.catatan) As headline, COALESCE(m.koordinat, CONCAT(mo.latt, ', ', mo.longt) ) As koordinat, COALESCE(m.sto, SUBSTRING_INDEX(SUBSTRING_INDEX(mo.odp, '-', 2), '-', -1) ) As sto"), 'mo.id As id_mo', 'mo.jenis_kerusakan', 'mo.file_id', 'mo.maintenance_id', 'mo.uniq_id', 'mo.created_at As mo_created')
        ->where(DB::raw("DATE_FORMAT(mo.created_at, '%Y-%m')"), '=', $jenis)
        ->OrderBy('mo.id', 'DESC')
        ->GroupBy('uniq_id')
        ->get();

        Excel::create('File Laporan Alpro', function($excel) use($data, $foto, $generate_evi, $generate_doc){

            if($generate_doc == 'ok')
            {
                $excel->sheet("boq", function($sheet) use($foto){

                    $header[0] = ['#', 'ODP', 'STO', 'Jenis Kerusakan', 'Catatan', 'Teknisi Mengerjakan', 'Status'];

                    $no_array = 1;

                    foreach($foto as $no => $list)
                    {
                        ++$no_array;
                        $header[$no_array][] = ++$no;
                        $header[$no_array][] = $list->nama_odp;
                        $header[$no_array][] = $list->sto;
                        $header[$no_array][] = implode(', ', array_map(function($x){
                            return str_replace('_', ' ', $x);
                        }, json_decode($list->jenis_kerusakan) ) );
                        $header[$no_array][] = $list->headline;
                        $header[$no_array][] = $list->dispatch_regu_name;
                        $header[$no_array][] = $list->status_name;
                    }

                    $border_Style = [
                        'borders' => [
                            'outline' => [
                            'style' => \PHPExcel_Style_Border::BORDER_THIN
                            ],
                            'inside' => [
                            'style' => \PHPExcel_Style_Border::BORDER_THIN
                            ],
                        ]
                    ];

                    $sheet->rows($header);

                    $sheet->getStyle('A1:G1')->getFont()->setBold(true);
                    $sheet->getStyle('A1:G'.$no_array)->applyFromArray($border_Style);
                });
            }

            if($generate_evi == 'ok')
            {
                $excel->sheet("evidence", function($sheet) use($foto){
                    $data_f = $raw_photo = [];
                    $nomor = 0;

                    for ($i = 'A'; $i !== 'ZZ'; $i++){
                    $alphabet[] = $i;
                    }

                    $token = '5774572588:AAF4JN_QEtSdCLgPchuzX0FMVgmI2dm1A5w';

                    foreach($foto as $kx => $vx)
                    {
                        $generate_foto = json_decode($vx->file_id);

                        $data_foto = http_build_query([
                            'file_id'    => $generate_foto->file_id,
                        ]);

                        $url = "https://api.telegram.org/bot".$token."/getFile?".$data_foto;

                        $res = @file_get_contents($url);
                        $res = json_decode($res)->result;

                        if(!isset($data_f[$vx->uniq_id]['no']) )
                        {
                            $data_f[$vx->uniq_id]['no'] = ++$nomor;
                            $data_f[$vx->uniq_id]['id'] = $vx->nama_odp;
                        }

                        $raw_photo[$vx->uniq_id]['no'] = null;
                        $raw_photo[$vx->uniq_id]['id'] = null;

                        $path_photo_arr[$vx->uniq_id][] = "https://api.telegram.org/file/bot".$token.'/'.$res->file_path;

                        foreach ($path_photo_arr as $k => $v)
                        {
                            foreach ($v as $kk => $vv)
                            {
                                if($k == $vx->uniq_id)
                                {
                                    $data_f[$vx->uniq_id]['photo'. $kk] = implode(', ', json_decode($vx->jenis_kerusakan) );
                                    $raw_photo[$vx->uniq_id]['photo'. $kk] = $vv;
                                }
                            }
                        }
                    }

                    $sheet->rows($data_f);
                    $no_foto = 0;

                    foreach($raw_photo as $k => $v)
                    {
                    ++$no_foto;
                    $v = array_values($v);
                    foreach($v as $kk => $vv)
                    {
                        if($vv)
                        {
                            $path = public_path().'/upload/tele_foto_alpro';

                            if (!file_exists($path) )
                            {
                                mkdir($path, 0777, true);
                            }

                            $img_path = $path.'/images'. $k .'.jpg';

                            copy($vv , $img_path);
                            $objDrawing = new \PHPExcel_Worksheet_Drawing;
                            $objDrawing->setPath($path. '/images'. $k .'.jpg');
                            $objDrawing->setCoordinates($alphabet[$kk] . '' . $no_foto);
                            $objDrawing->setHeight(138);
                            $objDrawing->setWidth(103);
                            $objDrawing->setWorksheet($sheet);
                            register_shutdown_function('unlink', $path. '/images'. $k .'.jpg');
                        }
                    }
                    }

                    if($data_f)
                    {
                        $no_pht = 0;

                        foreach ($raw_photo as $k => $v)
                        {
                            ++$no_pht;
                            foreach($v as $kk => $vv)
                            {
                            if($vv)
                            {
                                $set_height[$no_pht] = 120;
                            }
                            }
                        }

                        $sheet->setHeight($set_height);
                        //   $sheet->getStyle('A1:'.$alphabet[(count($data_f[0]) - 1)].''. count($data_f) )->applyFromArray([
                        //     'alignment' => [
                        //       'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        //       'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        //     ]
                        //   ]);
                    }
                });
            }
        })->export('xlsx');
    }

	public function detail_Listnog(request $req, $sto, $jenis, $date)
	{
        $list_data = $list_kendala_m = [];

        if($req->all() )
        {
            $list_data = $req->status;
            $list_kendala_m = $req->kendala_maintenance;
        }

        $data = Nog::list_param($sto, $jenis, $list_data, $list_kendala_m, $date);
        $data = json_decode(json_encode($data), TRUE) ;

        usort($data, function ($item1, $item2) {
            return count($item2['jenis_kerusakan']) <=> count($item1['jenis_kerusakan']);
        });

        $data = json_decode(json_encode($data), FALSE);
        return view('nog.listNog', compact('data', 'sto', 'jenis') );
	}

    public function add_order_alpro($id)
    {
      $data = DB::select("SELECT mo2.* FROM master_OdpCare mo1
      LEFT JOIN master_OdpCare mo2 ON mo2.uniq_id = mo1.uniq_id
      WHERE mo1.id = $id");

    //   if($data[0]->maintenance_id)
    //   {
    //     return back()->with('alerts', [
    //         ['type' => 'danger', 'text' => '<strong>GAGAL</strong> WO Sudah Dibuat!']
    //     ]);
    //   }

      $materials = DB::select('SELECT mmp.id as id_mmp, program_id, i.*, i.uraian as nama_item, 0 AS qty
      FROM maintenance_mtr_program mmp
      LEFT JOIN khs_maintenance i ON i.id_item = mmp.id_item
      WHERE i.id IS NOT NULL GROUP BY mmp.id_item');

      foreach($data as $key => $new_data)
      {
        if(json_encode($new_data) != false)
        {
          $renew_data[$key] = $new_data;
        }
        else
        {
          $wrong[] = $new_data;
          $renew_data[$key] = preg_replace('/[[:^print:]]/', '', $new_data);
        }
      }

      $penyebab = DB::table('maintenance_penyebab')->select('*', 'penyebab as text')->get();

      $foto = $saldo = $gamas_cause = [];

      $regu = DB::table('regu')
      ->select('*','id_regu as id', 'uraian as text')
      ->whereNotNull('uraian')
      ->where('label_regu', '!=', '')
      ->get();

      $token = '5774572588:AAF4JN_QEtSdCLgPchuzX0FMVgmI2dm1A5w';

      foreach($data as $v)
      {
        $generate_foto = json_decode($v->file_id);

        $data_foto = http_build_query([
        'file_id'    => $generate_foto->file_id,
        ]);

        $url = "https://api.telegram.org/bot".$token."/getFile?".$data_foto;

        $res = @file_get_contents($url);
        $res = json_decode($res)->result;
        $foto[$token]['foto'][] = $res->file_path;
        $foto[$token]['base64'][] = base64_encode(@file_get_contents("https://api.telegram.org/file/bot".$token.'/'.$res->file_path) );
      }

      $jenis_order = DB::table('maintenance_jenis_order')->where('aktif',1)->get();

      $get_alpro_stts = json_decode($data[0]->jenis_kerusakan);

      return view('tech.progress', ['data2' => $renew_data], compact('data', 'materials', 'regu', 'penyebab', 'foto', 'saldo', 'gamas_cause', 'jenis_order', 'get_alpro_stts') );
    }

	public function kalender_alpro(Request $req, $d)
	{
		$data = Nog::get_workdate($req, $d);
        $days = $fd = $tfoot = $mitra = $sektor = $orderjenis = $sto_get = [];

        for ($i = 1; $i <= date('t', strtotime($d) ); ++$i) {
            $days[] = $i;
        }

        $bulan = [
            1 => 'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        ];

        $bulan = $bulan[date('n', strtotime($d) )];

        foreach($data as $k => $v)
        {
            $m = $v->nama_mitra == '' ? 'Tidak Ada Mitra' : $v->nama_mitra;

            $mitra[$m]['id'] = $v->nama_mitra;
            $mitra[$m]['text'] = $m;

            $sektor[$v->nama_sector]['id'] = $v->nama_sector;
            $sektor[$v->nama_sector]['text'] = $v->nama_sector;

            $sto_get[$v->sto]['id'] = $v->sto;
            $sto_get[$v->sto]['text'] = $v->sto;

            $fd[$v->nama_odp]['odp']        = $v->nama_odp;
            $fd[$v->nama_odp]['status']     = $v->status;
            $fd[$v->nama_odp]['nama_mitra'] = $v->nama_mitra;

            if(!isset($fd[$v->nama_odp]['tgl']) )
            {
                $tgl_all = array_map(function($x){
                    return $x = 0;
                }, array_flip($days) );

                $fd[$v->nama_odp]['tgl'] = $tgl_all;

            }

            if(empty($tfoot) )
            {
                $tgl_all = array_map(function($x){
                    return $x = 0;
                }, array_flip($days) );

                $tfoot['Plan'] = $tgl_all;
                $tfoot['Realisasi'] = $tgl_all;
            }
            //dd($)
            $fd[$v->nama_odp]['tgl'][(int)date('d', strtotime($v->timeplan) )] +=1;

            $tfoot['Plan'][(int)date('d', strtotime($v->timeplan) )] +=1;

            if($v->status == 'close')
            {
                $tfoot['Realisasi'][(int)date('d', strtotime($v->timeplan) )] +=1;
            }
        }

        if($tfoot)
        {
            $tfoot['Gap'] = array_map(function($x, $y){
                return $x - $y;
            }, $tfoot['Plan'], $tfoot['Realisasi']);
        }

        $fd = array_values($fd);
        // dd($tfoot);
        $mitra = array_values($mitra);
        $sektor = array_values($sektor);
        $sto_get = array_values($sto_get);
        $requestan = $req->all();
        // dd($fd, $days, $data, $bulan, $sektor, $mitra, $sektor);
        return view('nog.kalender_nog', compact('fd', 'days', 'bulan', 'mitra', 'sektor', 'requestan', 'tfoot', 'sto_get') );
	}

	public function lapor_active(Request $req)
	{
		$data = Nog::get_active_user($req);
        $mitra = $sektor = $sto_get = $fd = [];

        foreach($data as $k => $v)
        {
            // $m = $v->nama_mitra == '' ? 'Tidak Ada Mitra' : $v->nama_mitra;
            $m = 'TIDAK ADA MITRA';

            // $mitra[$m]['id'] = $v->nama_mitra;
            // $mitra[$m]['text'] = $m;

            // $sektor[$v->nama_sector]['id'] = $v->nama_sector;
            // $sektor[$v->nama_sector]['text'] = $v->nama_sector;

            // $sto_get[$v->sto]['id'] = $v->sto;
            // $sto_get[$v->sto]['text'] = $v->sto;
            $fd[$v->nik_pelapor]['chat_id'] = $v->chat_id;
            $fd[$v->nik_pelapor]['witel'] = $v->witel;
            $fd[$v->nik_pelapor]['nama'] = "$v->nama_pelapor ($v->nik_pelapor)";

            if(!isset($fd[$v->nik_pelapor]['jml']) )
            {
                $fd[$v->nik_pelapor]['jml'] = 0;
            }

            $fd[$v->nik_pelapor]['jml'] += $v->jumlah;
        }

        usort($fd, function($a, $b) {
            return $b['jml'] - $a['jml'];
        });

        $requestan = $req->all();

        return view('nog.lapor_active', compact('fd', 'mitra', 'sektor', 'requestan', 'sto_get') );
	}

	public function save_order_alpro_bot(Request $req, $id_care)
	{
        $data = DB::select("SELECT mo2.*, emp.nik FROM master_OdpCare mo1
        LEFT JOIN master_OdpCare mo2 ON mo2.uniq_id = mo1.uniq_id
        LEFT JOIN 1_2_employee emp ON emp.chat_id = mo2.chat_id
        WHERE mo1.id = $id_care");

        if($req->regu)
        {
            $regu = DB::Table('regu')->where('id_regu', $req->regu)->first();

            $sector_id = $sector_name = NULL;

            if ($req->nama_odp)
            {
              $alpro = explode("/", $req->nama_odp);
              $alpro = explode("-", $alpro[0]);
              $check_alpro = DB::table('maintenance_sector As ms')
              ->leftJoin('maintenance_sector_alpro As msa', 'ms.id', '=', 'msa.sector_id')
              ->select('ms.*', 'msa.alpro')
              ->where('msa.alpro', $alpro[1].'-'.$alpro[2])->first();
              if ($check_alpro) {
                $sector_id = $check_alpro->id;
                $sector_name = $check_alpro->sector;
              }
            }

            $sto = DB::table('maintenance_datel')->where('sto', $alpro[1])->first();
            $jenis = DB::table('maintenance_jenis_order')->where('id', $req->jenis_order)->first();

            $array_save = [
                'no_tiket'           => $req->no_tiket,
                'timeplan'           => $req->timeplan,
                'dispatch_regu_id'   => $req->regu,
                'dispatch_regu_name' => $regu->uraian,
                "jenis_order"        => $req->jenis_order,
                "nama_order"         => $jenis->nama_order,
                "username1"          => @$regu->nik1,
                "username2"          => @$regu->nik2,
                'nama_odp'           => $req->nama_odp,
                "pic"                => $data[0]->nik,
                "sektor"             => $sector_name,
                "sector_id"          => $sector_id,
                "nama_sector"        => $sector_name,
                'koordinat'          => $req->koordinat,
                "kandatel"           => $sto->datel,
                "sto"                => $sto->sto,
                'headline'           => $req->headline,
                'action'             => $req->action,
                'created_at'         => date('Y-m-d H: i: s'),
                'created_by'         => session('auth')->id_user,
                // 'mtrcount'        => count($materials),
            ];

            $id = DB::table('maintaince')->InsertGetId($array_save);

            // $materials = json_decode($req->materials);

            // foreach($materials as $material) {
            //     DB::table('maintaince_mtr')->insert([
            //         'maintaince_id' => $id,
            //         'id_item'       => $material->id_item,
            //         'qty'           => $material->qty,
            //     ]);
            // }

            DB::table('maintenance_note')
            ->insert([
                'mt_id'    => $id,
                'catatan'  => 'ORDER ALPRO BOT',
                'status'   => 'ORDER ALPRO BOT',
                // 'mtr_json' => json_encode($materials),
                'pelaku'   => session('auth')->id_karyawan,
                'ts'       => DB::raw('NOW()')
            ]);

            DB::select("UPDATE master_OdpCare mo1
            LEFT JOIN master_OdpCare mo2 ON mo2.uniq_id = mo1.uniq_id
            SET mo1.maintenance_id = $id, mo1.flag_kondisi_alpro = '".json_encode($req->kondisi_alpro)."'
            WHERE mo1.uniq_id = '".$data[0]->uniq_id."'");
        }
        else
        {
            $id_dalapa = $req->id_dalapa ?? 0;
            DB::select("UPDATE master_OdpCare mo1
            LEFT JOIN master_OdpCare mo2 ON mo2.uniq_id = mo1.uniq_id
            SET mo1.dalapa_id = $id_dalapa, mo1.status_dalapa = '".$req->status_dalapa."'
            WHERE mo1.uniq_id = '".$data[0]->uniq_id."'");
        }

        return redirect("/list_saldo/alpro/".date('Y-m') )->with('alerts', [
            ['type' => 'success', 'text' => '<strong>BERHASIL</strong> Membuat WO!']
          ]);
	}

	public function detail_kondisi_alpro(request $req, $sto, $jenis, $date)
	{
        $data = Nog::list_kondisi_alpro($sto, $jenis, $date);
        $data = json_decode(json_encode($data), TRUE) ;

        usort($data, function ($item1, $item2) {
            return count($item2['jenis_kerusakan']) <=> count($item1['jenis_kerusakan']);
        });

        $data = json_decode(json_encode($data), FALSE);
        return view('nog.listNog', compact('data', 'sto', 'jenis') );
	}

	public function lapor_active_list_wo($id, $tgl)
	{
		$data = Nog::detail_list_laporan_active($id, $tgl);
        $sto = 'All';
        $data = json_decode(json_encode($data), TRUE) ;

        usort($data, function ($item1, $item2) {
            return count($item2['jenis_kerusakan']) <=> count($item1['jenis_kerusakan']);
        });

        $data = json_decode(json_encode($data), FALSE);

        foreach($data as $k => $v)
        {
            $data[$k]->jenis_kerusakan = json_decode($v->jenis_kerusakan);
            $data[$k]->tindak_lanjut = 'Maintenance';
        }

        return view('nog.listNog', compact('data', 'sto') );
	}
}
