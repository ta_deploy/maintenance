<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use DB;
use Telegram;
use App\DA\User;
use Excel;

class ReportController extends Controller
{
  protected $photoOdpSehat = [
      'GRID_TIANG', 'GRID_PROGRES_TIANG','GRID_AKSESORIS', 'GRID_PROGRES_AKSESORIS', 'GRID_ODP', 'GRID_PROGRES_ODP', 'GRID_REBOUNDARY', 'GRID_PROGRES_REBOUNDARY', 'GRID_DENAH'
    ];
  public function reportDpTiang($date){

    $data = DB::select(
    "SELECT *,
    (SELECT sum(qty) FROM maintaince_mtr mm
    LEFT JOIN maintaince m ON mm.maintaince_id = m.id
    WHERE m.isHapus = 0 AND m.kandatel=ms.datel AND m.status = 'close'
    AND (mm.id_item = 'TT-7' OR mm.id_item = 'TT-9')
    AND tgl_selesai LIKE '$date%') AS tiang
    FROM maintenance_so ms");
    //dd($data);
    return view('report.dptiang', compact('data'));
  }
  public function reportDpTiangAjax($tgl, $dtl){

    $data = DB::select('SELECT sto as datel,(select sum(qty) from maintaince_mtr mm left join maintaince m on mm.maintaince_id = m.id where m.sto=maintaince.sto and m.status = "close" and (mm.id_item = "TT-7" or mm.id_item = "TT-9") and tgl_selesai like "'.$tgl.'%") as tiang FROM maintaince where maintaince.isHapus = 0 AND kandatel = "'.$dtl.'" and tgl_selesai like "'.$tgl.'%" GROUP BY sto ORDER BY sto');
    //dd($data);
    return view('report.tiangAjax', compact('data'));
  }
  public function reportAbsen($date){
    $data = DB::select('select *,
    (select count(mal.id) from regu mr left join maintenance_absen_log mal on mr.nik1 = mal.id_user where mal.status=2 and mainsector = mg.chat_id and created_at like "'.$date.'%") as nik1,
    (select count(mal.id) from regu mr left join maintenance_absen_log mal on mr.nik2 = mal.id_user where mal.status=2 and mainsector = mg.chat_id and created_at like "'.$date.'%") as nik2,
    (select count(mal.id) from regu mr left join maintenance_absen_log mal on mr.nik3 = mal.id_user where mal.status=2 and mainsector = mg.chat_id and created_at like "'.$date.'%") as nik3,
    (select count(mal.id) from regu mr left join maintenance_absen_log mal on mr.nik4 = mal.id_user where mal.status=2 and mainsector = mg.chat_id and created_at like "'.$date.'%") as nik4
    from maintenance_grup mg where 1');
    return view('report.absen', compact('data'));
  }
  public function ajaxReportAbsen($id, $tgl){
    $query="";
    if($id)
      $query=" and mr.mainsector = ".$id;
    $data = DB::select("select mr.*,
            (select count(status) from maintenance_absen_log mal where id_user = mr.nik1 and mal.created_at like '".$tgl."%' and mal.status=2) as status1,
            (select count(status) from maintenance_absen_log mal2 where mal2.id_user = mr.nik2 and mal2.created_at like '".$tgl."%' and mal2.status=2) as status2,
            (select count(status) from maintenance_absen_log mal3 where mal3.id_user = mr.nik3 and mal3.created_at like '".$tgl."%' and mal3.status=2) as status3,
            (select count(status) from maintenance_absen_log mal4 where mal4.id_user = mr.nik4 and mal4.created_at like '".$tgl."%' and mal4.status=2) as status4,
            (select u_nama As nama from user where id_karyawan=nik1) as nama1,
            (select u_nama As nama from user where id_karyawan=nik2) as nama2,
            (select u_nama As nama from user where id_karyawan=nik3) as nama3,
            (select u_nama As nama from user where id_karyawan=nik4) as nama4
            from regu mr
            where mr.ACTIVE=1 ".$query);
    return view('report.ajaxAbsen', compact('data'));
  }
  public function grab_remo(){
    ini_set('max_execution_time', 60000);
    $filename = 'http://10.65.10.73:8081/pimcore/modules/3rdparty/mpdf/exportqefiberopticnew_2018.php?thnbln='.date('Ym').'&witel=KALSEL#level4';
    $serv = file_get_contents($filename);
    $clean = preg_replace('#\\x04\\x02..........#ms', "\t", substr(trim($serv), 119));
    $clean = preg_replace('#\\000\\000............#ms', "\n", $clean);
    $head = array();
    $result = array();
    $line = explode("\n", $clean);
    for($i = 1;$i<count($line);$i++){
      if($i==1){
        /*
        $heading = $line[$i];
        var_dump($line[0]);
        $heading = str_replace('ND', 'NO_SPEEDY', $heading);

        $data = explode("\t", $heading);
        $head = $data;
        array_pop($head);
        */
        $head = array("DIVRE", "WITEL", "CMDF", "RK", "DP", "NO_SPEEDY", "NODE_ID", "NODE_IP", "SLOT", "PORT", "ONU", "ONU_DESC", "ONU_TYPE", "ONU_SN", "FIBER_LENGTH", "OLT_RX_POWER", "OLT_RX_POWER_AKHIR", "ONU_RX_POWER", "ONU_RX_POWER_AKHIR", "TGL_UKUR_AKHIR", "STATUS", "ALAMAT", "STATUS_WARRANTY", "IS_CABUT", "IS_KW1");
        var_dump($head);

      }else{
        $data = explode("\t", $line[$i]);
        //dd($data);
        if($data[0] == '@'){
          foreach($head as $d => $h){
            $result[$i][$h] = trim(@$data[$d+1]);
          }
        }else{
          foreach($head as $d => $h){
            $result[$i][$h] = trim(@$data[$d]);
          }
        }
      }
    }
    //var_dump($result);
    $records = array_chunk($result, 500);
    DB::table('remo_servo')->truncate();
    foreach ($records as $batch) {
      DB::table('remo_servo')->insert($batch);
    }
  }
  public function matrix_search_sektor(Request $req){
    $data_raw = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->where('datel', trim($req->datel))->get();
    foreach($data_raw as $key => $d) {
      $data[$key]['id'] = $d->id;
      $data[$key]['text'] = $d->text;
    }
    $data[] = ['id' => 'all', 'text' => 'all'];
    $data = array_unique($data, SORT_REGULAR);
    $data = json_decode(json_encode(array_values($data)));
    return \Response::json($data);
  }
  public function all_menu_prod()
  {
    $data = DB::table('maintenance_jenis_order')->Where('Aktif', 1)->get();
    return view('report.grand_report', compact('data'));
  }
    public function matrix($id, $jenis_order, $get_datel, $get_sektor)
    {
      $auth = session('auth');
      $query = "";
      if($jenis_order)
        $query .= " and maintaince.jenis_order ='".$jenis_order."'";
      if($get_datel != 'all')
        $query .= " and maintaince.kandatel ='".$get_datel."'";
      if($get_sektor != 'all')
        $query .= " and maintaince.sto ='".$get_sektor."'";
      $list = array();
      $lastRegu = "a";
      $datel_raw = DB::table('maintenance_datel')->select('datel as id', 'datel as text')->Groupby('datel')->get();
      foreach($datel_raw as $key => $d) {
        $datel[$key]['id'] = $d->id;
        $datel[$key]['text'] = $d->text;
      }
      $data[] = ['id' => 'all', 'text' => 'all'];
      $datel = array_unique($datel, SORT_REGULAR);
      $datel = json_decode(json_encode(array_values($datel)));
      $ls = DB::select("SELECT maintaince.*,
      (CASE WHEN maintaince.jenis_order = 8 AND headline LIKE '%SABER%' THEN 'saber' else null END) as stts_sbr
       from maintaince
       where ((maintaince.status = 'kendala teknis' AND maintaince.modified_at LIKE '%$id%')
        OR (maintaince.status = 'kendala pelanggan' AND maintaince.modified_at LIKE '%$id%')
        OR (maintaince.status = 'close' AND tgl_selesai LIKE '%$id%')
        OR (maintaince.status IS NULL OR maintaince.status='ogp')) AND refer IS NULL AND isHapus = 0 $query
        ORDER BY dispatch_regu_id ASC, created_at ASC");
        // dd($ls);
      foreach($ls as $l){
        // if($lastRegu == $l->dispatch_regu_id){
        //   $list[$l->dispatch_regu_id][] = array("tiket"=>$l->no_tiket, 'odp_nama' => $l->nama_odp, "status_laporan"=>$l->status, "created_at" => $l->created_at, "id" => $l->id);
        // }else{
          $list[$l->dispatch_regu_id][] = array("tiket"=>$l->no_tiket, 'jenis_hvc' => $l->jenis_hvc, 'status' => $l->status, 'status_detail' => $l->status_detail, 'stts_sbr' => $l->stts_sbr, 'odp_nama' => $l->nama_odp, "status_laporan"=>$l->status, "created_at" => $l->created_at, "id" => $l->id, "tgl_dispatch" => $l->tgl_dispatch, "tgl_selesai" => $l->tgl_selesai);
        // }
        // $lastRegu = $l->dispatch_regu_id;
      }
      // dd($list);
      $regu = array();
      $mitra = "";

      if($auth->nama_instansi != NULL && !in_array($auth->nama_instansi, ["TELKOM AKSES", "TELKOM"]) || $auth->maintenance_level == 0 )
      {
        $mitra = " mr.nama_mitra='".$auth->nama_instansi."' and ";
      }
      // dd($mitra, $auth, !in_array($auth->nama_instansi, ["TELKOM AKSES", "TELKOM"]));
      foreach(DB::table('maintenance_grup')->orderBy('urutan', 'asc')->get() as $mr){
        $data = DB::select("SELECT *,
          (select u_mt_absen as mt_absen from user where id_karyawan = mr.nik1 LIMIT 0, 1) as absen1,
          (select u_mt_verif_absen As mt_verif_absen from user where id_karyawan = mr.nik1 LIMIT 0, 1) as verif1,
          (select u_nama As nama from user where id_karyawan = mr.nik1 LIMIT 0, 1) as nama1,
          (select u_mt_absen as mt_absen from user where id_karyawan = mr.nik2 LIMIT 0, 1) as absen2,
          (select u_mt_verif_absen As mt_verif_absen from user where id_karyawan = mr.nik2 LIMIT 0, 1) as verif2,
          (select u_nama As nama from user where id_karyawan = mr.nik2 LIMIT 0, 1) as nama2,
          (select count(*) from maintaince where isHapus = 0 AND dispatch_regu_id = mr.id_regu and refer is null ".$query.") as jumlah,
          (select count(*) from maintaince where isHapus = 0 AND dispatch_regu_id = mr.id_regu and status is null and refer is null ".$query.") as no_update,
          (select count(*) from maintaince where isHapus = 0 AND dispatch_regu_id = mr.id_regu and status = 'kendala teknis' and modified_at like '%".$id."%' and refer is null ".$query.") as kendala_teknis,
          (select count(*) from maintaince where isHapus = 0 AND dispatch_regu_id = mr.id_regu and status = 'kendala pelanggan' and modified_at like '%".$id."%' and refer is null ".$query.") as kendala_pelanggan,
          (select count(*) from maintaince where isHapus = 0 AND dispatch_regu_id = mr.id_regu and status = 'close' and tgl_selesai like '%".$id."%' and refer is null ".$query.") as close,
          (select count(*) from maintaince where isHapus = 0 AND dispatch_regu_id = mr.id_regu and status = 'close' and tgl_selesai like '%".substr($id, 0,7)."%' and refer is null ".$query.") as close_bln
          from regu mr
          left join maintenance_grup mg on mr.mainsector_marina = mg.chat_id
          where ".$mitra." mr.ACTIVE = 1 and mg.urutan = '".$mr->urutan."' order by urutan asc, id_regu asc");

          $regu[$mr->urutan]['data'] = $data;
          $regu[$mr->urutan]['grup'] = $mr->grup;

      }
      // dd($list);
      return view('report.matrix', compact('regu', 'list', 'datel'));
    }
    public function rekap(){
      $data = DB::select('select * from maintaince where isHapus = 0');
      return view('report.rekap', [$data]);
    }

    public function Odp_loss_Crossover($d) {
      $data = DB::SELECT(
        "SELECT
        (CASE WHEN order_from = 'PSB' THEN 'Prov'
        WHEN order_from = 'ASSURANCE' THEN 'IOAN'
        ELSE 'Marina' END) As jenis_order_frm,
        order_from,
        SUM(CASE WHEN tgl_selesai LIKE '$d%' AND jenis_order = 3 AND status = 'kendala pelanggan' THEN 1 ELSE 0 END) As odp_loss_kp,
        SUM(CASE WHEN tgl_selesai LIKE '$d%' AND jenis_order = 3 AND status = 'kendala teknis' THEN 1 ELSE 0 END) As odp_loss_kt,
        SUM(CASE WHEN tgl_selesai LIKE '$d%' AND jenis_order = 3 AND status = 'close' THEN 1 ELSE 0 END) As odp_loss_cl,
        SUM(CASE WHEN jenis_order = 3 AND status IS NULL AND dispatch_regu_id IS NOT NULL AND dispatch_regu_id != 0 AND refer IS NULL THEN 1 ELSE 0 END) As odp_loss_NU,
        SUM(CASE WHEN tgl_selesai LIKE '$d%' AND jenis_order = 6 AND status = 'kendala pelanggan' THEN 1 ELSE 0 END) As inst_kp,
        SUM(CASE WHEN tgl_selesai LIKE '$d%' AND jenis_order = 6 AND status = 'kendala teknis' THEN 1 ELSE 0 END) As inst_kt,
        SUM(CASE WHEN tgl_selesai LIKE '$d%' AND jenis_order = 6 AND status = 'close' THEN 1 ELSE 0 END) As inst_cl,
        SUM(CASE WHEN jenis_order = 6 AND status IS NULL AND dispatch_regu_id IS NOT NULL AND dispatch_regu_id != 0 AND refer IS NULL THEN 1 ELSE 0 END) As inst_NU,
        SUM(CASE WHEN tgl_selesai LIKE '$d%' AND jenis_order = 9 AND status = 'kendala pelanggan' THEN 1 ELSE 0 END) As rebo_kp,
        SUM(CASE WHEN tgl_selesai LIKE '$d%' AND jenis_order = 9 AND status = 'kendala teknis' THEN 1 ELSE 0 END) As rebo_kt,
        SUM(CASE WHEN tgl_selesai LIKE '$d%' AND jenis_order = 9 AND status = 'close' THEN 1 ELSE 0 END) As rebo_cl,
        (SELECT COUNT(*) FROM
          (SELECT COUNT(*),
            (CASE WHEN order_from = 'PSB' THEN 'Prov'
              WHEN order_from = 'ASSURANCE' THEN 'IOAN'
              ELSE 'Marina' END) As jenis_order_md
            FROM maintaince mts
            LEFT JOIN maintenance_valins mvd ON mts.id = mvd.maintenance_id
           WHERE mts.isHapus = 0 AND tgl_selesai LIKE '$d%' AND jenis_order = 9 AND status = 'close' AND maintenance_id IS NULL GROUP BY mts.id,mts.order_from ORDER BY mts.tgl_selesai DESC) as temp_dt WHERE temp_dt.jenis_order_md = CASE WHEN order_from = 'PSB' THEN 'Prov'
              WHEN order_from = 'ASSURANCE' THEN 'IOAN'
              ELSE 'Marina' END ) AS rebo_cl_notval,
        (SELECT COUNT(*) FROM
          (SELECT COUNT(*),
            (CASE WHEN order_from = 'PSB' THEN 'Prov'
              WHEN order_from = 'ASSURANCE' THEN 'IOAN'
              ELSE 'Marina' END) As jenis_order_md
            FROM maintaince mts
            LEFT JOIN maintenance_valins mvd ON mts.id = mvd.maintenance_id
            WHERE mts.isHapus = 0 AND refer IS NULL AND mts.status = 'close' AND mvd.maintenance_id IS NOT NULL AND (mvd.sisa_port != 0 OR port_idle IS NOT NULL AND port_idle != 0)  AND mts.jenis_order = '9' AND mts.tgl_selesai like '$d%' GROUP BY mts.id,mts.order_from ORDER BY mts.tgl_selesai DESC) as temp_dt WHERE temp_dt.jenis_order_md = CASE WHEN order_from = 'PSB' THEN 'Prov'
              WHEN order_from = 'ASSURANCE' THEN 'IOAN'
              ELSE 'Marina' END ) AS rebo_cl_val,
        SUM(CASE WHEN jenis_order = 9 AND status IS NULL AND dispatch_regu_id IS NOT NULL AND dispatch_regu_id != 0 AND refer IS NULL THEN 1 ELSE 0 END) As rebo_NU
        FROM maintaince mt
        WHERE mt.isHapus = 0
        GROUP BY jenis_order_frm");

      $psb_odp_loss = DB::select(
        "SELECT COUNT(d.id),
        SUM(CASE WHEN d.jenis_order IN ('IN', 'INT') THEN 1 ELSE 0 END ) as odp_loss_ioan,
        SUM(CASE WHEN d.jenis_order = 'SC' THEN 1 ELSE 0 END ) as odp_loss_psb
        FROM dispatch_teknisi d
        LEFT JOIN maintaince mc ON d.Ndem = mc.no_tiket
        LEFT JOIN psb_laporan pl ON d.id=pl.id_tbl_mj
        WHERE mc.id IS NULL AND pl.status_laporan = 2 AND pl.isCutOff IS NULL
        ")[0];
      $insertpsb = DB::select(
        "SELECT COUNT(d.id),
        SUM(CASE WHEN d.jenis_order IN ('IN', 'INT') THEN 1 ELSE 0 END ) as insert_ioan,
        SUM(CASE WHEN d.jenis_order = 'SC' THEN 1 ELSE 0 END ) as insert_psb
        FROM dispatch_teknisi d
        LEFT JOIN maintaince mc ON d.Ndem = mc.no_tiket
        LEFT JOIN psb_laporan pl ON d.id=pl.id_tbl_mj
        WHERE mc.id IS NULL AND pl.status_laporan = 11 AND pl.isCutOff IS NULL
        ")[0];
      $odpfullpsb = DB::select(
        "SELECT COUNT(d.id),
        SUM(CASE WHEN d.jenis_order IN ('IN', 'INT') THEN 1 ELSE 0 END ) as rebo_ioan,
        SUM(CASE WHEN d.jenis_order = 'SC' THEN 1 ELSE 0 END ) as rebo_psb
        FROM dispatch_teknisi d
        LEFT JOIN maintaince mc ON d.Ndem = mc.no_tiket
        LEFT JOIN psb_laporan pl ON d.id=pl.id_tbl_mj
        WHERE mc.id IS NULL AND pl.status_laporan = 24 AND pl.isCutOff IS NULL
        ")[0];
          return view('report.odploss_crossover', compact('data', 'psb_odp_loss', 'insertpsb', 'odpfullpsb'));
    }

    public function reportUmur($tgl, $id)
    {
      $order = DB::table('maintenance_jenis_order')->where('id', $id)->first();
      $data = DB::select('select * from
        (select *,
        (SELECT count(*)
          FROM maintaince m
          WHERE m.isHapus = 0 AND DATEDIFF( NOW( ) , created_at )<1 and jenis_order ='.$id.' and status is null and refer is null and m.sto=md.sto) as a,
        (SELECT count(*)
          FROM maintaince m
          WHERE m.isHapus = 0 AND DATEDIFF( NOW( ) , created_at )>=1 and DATEDIFF( NOW( ) , created_at )<=2 and jenis_order ='.$id.' and status is null and refer is null and m.sto=md.sto) as b,
        (SELECT count(*)
          FROM maintaince m
          WHERE m.isHapus = 0 AND DATEDIFF( NOW( ) , created_at )>2 and DATEDIFF( NOW( ) , created_at )<=3 and jenis_order ='.$id.' and status is null and refer is null and m.sto=md.sto) as c,
        (SELECT count(*)
          FROM maintaince m
          WHERE m.isHapus = 0 AND DATEDIFF( NOW( ) , created_at )>3 and DATEDIFF( NOW( ) , created_at )<=7 and jenis_order ='.$id.' and status is null and refer is null and m.sto=md.sto) as d,
        (SELECT count(*)
          FROM maintaince m
          WHERE m.isHapus = 0 AND DATEDIFF( NOW( ) , created_at )>7 and DATEDIFF( NOW( ) , created_at )<=30 and jenis_order ='.$id.' and status is null and refer is null and m.sto=md.sto) as e,
        (SELECT count(*)
          FROM maintaince m
          WHERE m.isHapus = 0 AND DATEDIFF( NOW( ) , created_at )>30 and jenis_order ='.$id.' and status is null and refer is null and m.sto=md.sto) as f,
        (SELECT count(*)
          FROM maintaince m
          WHERE m.isHapus = 0 AND jenis_order = '.$id.' and status is null and refer is null and m.sto=md.sto) as total
        from maintenance_datel md where 1 order by md.datel asc) asd where total>0');
      return view('report.odploss', compact('data', 'order'));
      /*
      day>30
      day>7 and day<=30
      day>3 and day<=7
      day>2 and day<=3
      day>=1 and day<=2
      day<1
      */
    }
     public function detilReport($query)
    {
      $data = DB::select('SELECT * from maintaince where '.$query.' and refer is null and status is null AND isHapus = 0');
      return view('order.ajaxVerify', compact('data'));
    }
    public function produktifitasbulanan($id){
      $auth = session('auth');
      $mitra = "";

      if($auth->nama_instansi && $auth->nama_instansi != "TELKOM AKSES"){
        $mitra = " mr.nama_mitra='".$auth->nama_instansi."' and ";
      }

      $data_regu = DB::select("SELECT *,
      (select u_nama As nama from user where id_karyawan = mr.nik1 GROUP BY id_karyawan) as nama1,
      (select u_nama As nama from user where id_karyawan = mr.nik2 GROUP BY id_karyawan) as nama2
      from regu mr where $mitra mr.ACTIVE =1");

      $data_maint = DB::SELECT("SELECT mr.id_regu,
      SUM(CASE WHEN refer is null THEN 1 ELSE 0 END) as close,
      SUM(CASE WHEN m.jenis_order=3 and refer is null THEN 1 ELSE 0 END) as odp_loss,
      SUM(CASE WHEN m.jenis_order=2 and refer is null THEN 1 ELSE 0 END) as gamas,
      SUM(CASE WHEN m.jenis_order=1 and refer is null THEN 1 ELSE 0 END) as benjar,
      SUM(CASE WHEN m.jenis_order=4 and refer is null THEN 1 ELSE 0 END) as remo,
      SUM(CASE WHEN m.jenis_order=5 and refer is null THEN 1 ELSE 0 END) as utilitas,
      SUM(CASE WHEN m.jenis_order=6 and refer is null THEN 1 ELSE 0 END) as insert_tiang
      from regu mr LEFT JOIN maintaince m ON m.dispatch_regu_id = mr.id_regu where m.isHapus = 0 AND $mitra m.status = 'close' and tgl_selesai like '%$id%' GROUP BY mr.id_regu");

      $data_maint_mtr = DB::SELECT("SELECT mr.id_regu,
      sum(qty*jasa_ta) as jasa_ta,
      sum(qty*material_ta) as material_ta,
      sum(qty*jasa_telkom) as jasa_telkom,
      sum(qty*material_telkom) as material_telkom
      FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item left join maintaince m on mm.maintaince_id=m.id LEFT JOIN regu as mr ON m.dispatch_regu_id = mr.id_regu WHERE m.isHapus = 0 AND $mitra m.status = 'close' and m.tgl_selesai like '%$id%' and m.refer is null GROUP BY mr.id_regu ");
      $data = array();
      $count=0;
      foreach ($data_regu as $key => $val) {
        foreach ($data_maint as $key_maint => $val_maint){
          if($val->id_regu == $val_maint->id_regu){
            $data[$count++] = (object) array_merge(
          (array) $val, (array) $val_maint);
          }
        }
      }

      foreach ($data as $key_data => $val_data){
        foreach ($data_maint_mtr as $key => $val) {
          if($val->id_regu == $val_data->id_regu){
            $data[$key_data] = (object) array_merge(
          (array) $val, (array) $val_data);
          }
        }
      }

      // $data = DB::SELECT('
      //   select * from (select *,
      //   (select nama from karyawan where id_karyawan = mr.nik1) as nama1,
      //   (select nama from karyawan where id_karyawan = mr.nik2) as nama2,
      //   (select nama from karyawan where id_karyawan = mr.nik3) as nama3,
      //   (select nama from karyawan where id_karyawan = mr.nik4) as nama4,
      //   (select count(*) from maintaince m where m.status = "close" and tgl_selesai like "%'.$id.'%" and dispatch_regu_id = mr.id_regu and refer is null) as close,
      //   (select count(*) from maintaince m where m.status = "close" and tgl_selesai like "%'.$id.'%" and dispatch_regu_id = mr.id_regu and m.jenis_order=3 and refer is null) as odp_loss,
      //   (select count(*) from maintaince m where m.status = "close" and tgl_selesai like "%'.$id.'%" and dispatch_regu_id = mr.id_regu and m.jenis_order=2 and refer is null) as gamas,
      //   (select count(*) from maintaince m where m.status = "close" and tgl_selesai like "%'.$id.'%" and dispatch_regu_id = mr.id_regu and m.jenis_order=1 and refer is null) as benjar,
      //   (select count(*) from maintaince m where m.status = "close" and tgl_selesai like "%'.$id.'%" and dispatch_regu_id = mr.id_regu and m.jenis_order=4 and refer is null) as remo,
      //   (select count(*) from maintaince m where m.status = "close" and tgl_selesai like "%'.$id.'%" and dispatch_regu_id = mr.id_regu and m.jenis_order=5 and refer is null) as utilitas,
      //   (select count(*) from maintaince m where m.status = "close" and tgl_selesai like "%'.$id.'%" and dispatch_regu_id = mr.id_regu and m.jenis_order=6 and refer is null) as insert_tiang,
      //   (SELECT sum(qty*jasa_ta) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item left join maintaince m on mm.maintaince_id=m.id WHERE m.status = "close" and m.tgl_selesai like "%'.$id.'%" and m.dispatch_regu_id = mr.id_regu and m.refer is null) as jasa_ta,
      //   (SELECT sum(qty*material_ta) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item left join maintaince m on mm.maintaince_id=m.id WHERE m.status = "close" and m.tgl_selesai like "%'.$id.'%" and m.dispatch_regu_id = mr.id_regu and m.refer is null) as material_ta,
      //   (SELECT sum(qty*jasa_telkom) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item left join maintaince m on mm.maintaince_id=m.id WHERE m.status = "close" and m.tgl_selesai like "%'.$id.'%" and m.dispatch_regu_id = mr.id_regu and m.refer is null) as jasa_telkom,
      //   (SELECT sum(qty*material_telkom) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item left join maintaince m on mm.maintaince_id=m.id WHERE m.status = "close" and m.tgl_selesai like "%'.$id.'%" and m.dispatch_regu_id = mr.id_regu and m.refer is null) as material_telkom
      //   from regu mr where '.$mitra.' mr.ACTIVE =1) asd where 1 order by close desc');

      return view('report.produktifitas', compact('data'));
    }
    public function detil_produktifitasbulanan($tgl, $jenis, $id){
      $query = '';
      if($jenis)
        $query = ' m.jenis_order='.$jenis.' and ';
      $data = DB::select('select m.*,mr.nama_mitra,
        (select u_nama As nama from user where id_karyawan = m.username1 GROUP BY id_karyawan) as nama1,
        (select u_nama As nama from user where id_karyawan = m.username2 GROUP BY id_karyawan) as nama2,
        (SELECT sum(qty*jasa_ta) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as jasa_ta,
        (SELECT sum(qty*material_ta) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as material_ta,
        (SELECT sum(qty*jasa_telkom) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as jasa_telkom,
        (SELECT sum(qty*material_telkom) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as material_telkom
        from maintaince m left join regu mr on m.dispatch_regu_id = mr.id_regu where m.isHapus = 0 AND m.status = "close" and '.$query.' m.tgl_selesai like "%'.$tgl.'%" and m.refer is null and m.dispatch_regu_id='.$id);
      return view('report.detilproduktifitas', compact('data'));
    }
    public function cetak_excel($tgl, $jenis_rekon, $verif){
      Excel::create($jenis_rekon.'-'.$tgl, function($excel) use($tgl, $jenis_rekon, $verif){
        $khs = DB::table('jenis_khs')->get();
        foreach($khs as $k){
          $excel->sheet($k->jenis_khs, function($sheet) use($tgl, $jenis_rekon, $verif, $k){
            $q = "and verify = ".$verif;
            if($verif == 'null')
              $q = "and verify is ".$verif;
            if($verif == 'all')
              $q = "";
            $q2 = " and mjo.rekon = '".$jenis_rekon."' ";
            if($jenis_rekon == "all")
              $q2 = "";
            $maintenance = DB::select("
              SELECT m.*,m2.*,i.satuan,i.jasa_telkom as jasa, i.material_telkom as material, i.uraian,
              (SELECT sum((qty*jasa)+(qty*material)) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_boq
              FROM maintaince m
              right join maintaince_mtr m2 on m.id = m2.maintaince_id
              LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
              LEFT JOIN maintenance_jenis_order mjo on m.jenis_order = mjo.id
              WHERE m.isHapus = 0 AND i.jenis=".$k->id." and m.tgl_selesai like '".$tgl."%' ".$q2." ".$q." order by m2.id_item asc"
            );
            $data = array();
            $lastNama = '';
            $no=0;
            $head = array();
            $title = array();
            foreach ($maintenance as $no => $m){
              if(!empty($m->no_tiket)){
                $head[] = $m->no_tiket;
                $title[$m->no_tiket] = array('tgl'=>$m->tgl_selesai,'no_tiket'=>$m->no_tiket,'sto'=>'STO-'.$m->sto,'action'=>date("d-m-Y",$m->tgl_selesai).' '.$m->nama_odp.' '.$m->action);
              }
            }
            $head = array_unique($head);

            //sort head per tgl
            $sorthead = array();
            $maintenance2 = DB::select("
              SELECT m.no_tiket, m.tgl_selesai
              FROM maintaince m
              right join maintaince_mtr m2 on m.id = m2.maintaince_id
              LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
              LEFT JOIN maintenance_jenis_order mjo on m.jenis_order = mjo.id
              WHERE m.isHapus = 0 AND i.jenis=".$k->id." and m.tgl_selesai like '".$tgl."%' ".$q2." ".$q." order by m.tgl_selesai asc
            ");
            foreach ($maintenance2 as $no => $m){
              if(!empty($m->no_tiket)){
                $sorthead[] = $m->no_tiket;
              }
            }
            $sorthead = array_unique($sorthead);
            $head = $sorthead;
            //endsort head
            $total = array();
            $totalmaterial = array();
            $totaljasa = array();
            foreach($head as $h){
              $total[$h] = 0;
              $totalmaterial[$h] = 0;
              $totaljasa[$h] = 0;
            }
            foreach ($maintenance as $no => $m){
              if($lastNama == $m->id_item){
                $data[count($data)-1]['no_tiket'][$m->no_tiket] = $m->qty;

                $totalmaterial[$m->no_tiket] += ($m->qty*$m->material);
                if($m->jenis_order == 4){
                  $totaljasa[$m->no_tiket] += 0;
                  $total[$m->no_tiket] += ($m->qty*$m->material);
                }
                else{
                  $totaljasa[$m->no_tiket] += ($m->qty*$m->jasa);
                  $total[$m->no_tiket] += ($m->qty*$m->jasa)+($m->qty*$m->material);
                }
                $no++;
              }else{
                $data[] = array("id_item" => $m->id_item, "satuan" => $m->satuan, "uraian" => $m->uraian, "jasa" => $m->jasa, "material" => $m->material, "total_boq" => $m->total_boq, "no_tiket" => array(), "total" => array());
                foreach($head as $h){
                  if($h == $m->no_tiket){
                    $data[count($data)-1]['no_tiket'][$h] = $m->qty;
                    $totalmaterial[$h] += ($m->qty*$m->material);
                    if($m->jenis_order == 4){
                      $total[$h] += ($m->qty*$m->material);
                      $totaljasa[$h] += 0;
                    }else{
                      $total[$h] += ($m->qty*$m->jasa)+($m->qty*$m->material);
                      $totaljasa[$h] += ($m->qty*$m->jasa);
                    }

                  }else{
                    $data[count($data)-1]['no_tiket'][$h] = 0;
                    $data[count($data)-1]['total'][$h] = 0;
                  }
                }
                $no=0;
              }
              $lastNama = $m->id_item;
            }
            //return view('report.excelRincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
            $sheet->getStyle('B')->getAlignment()->setWrapText(true);
            $sheet->loadView('report.excelRincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
          });
        }
      })->download('xlsx');

    }
    public function cetak_excel_2($tgl, $jenis_rekon, $verif, $generate_evi, $generate_doc){
      Excel::create($jenis_rekon.'-'.$tgl, function($excel) use($tgl, $jenis_rekon, $verif, $generate_evi, $generate_doc){
        $q = "and verify = ".$verif;
        if($verif == 'null')
          $q = "and verify is ".$verif;
        if($verif == 'all')
          $q = "";
        $q2 = " and m.jenis_order = '".$jenis_rekon."' ";
        if($jenis_rekon == "all")
          $q2 = "";
        $maintenance = DB::select("SELECT m.*,m2.id As id_mt2, m2.id_item, m2.qty, i.satuan,i.jasa_telkom as jasa, i.material_telkom as material, i.uraian, dispatch_regu_id, dispatch_regu_name,
           (SELECT CONCAT(u_nama, ' (', username1, ')') FROM user WHERE id_karyawan = username1 GROUP BY id_karyawan)as nik_1,
           (SELECT CONCAT(u_nama, ' (', username2, ')') FROM user WHERE id_karyawan = username2 GROUP BY id_karyawan)as nik_2,
          (SELECT sum((qty*jasa)+(qty*material)) FROM maintaince_mtr mm
          left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_boq
          FROM maintaince m
          right join maintaince_mtr m2 on m.id = m2.maintaince_id
          LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
          WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q2." ".$q." order by m2.id_item asc"
        );

        $data = $head = $title = $total = $totalmaterial = $totaljasa = array();
        // $lastNama = '';
        // $no=0;

        foreach ($maintenance as $no => $m)
        {
          if(!empty($m->no_tiket) )
          {
            // $head[] = $m->no_tiket;
            $title[$m->no_tiket] = array('tgl'=>$m->tgl_selesai, 'nik1'=>$m->nik_1, 'nik2'=>$m->nik_2, 'regu_name' =>$m->dispatch_regu_name, 'no_tiket'=>$m->no_tiket,'sto'=>'STO-'.$m->sto,'action'=>date("d-m-Y", strtotime($m->tgl_selesai)).' '.$m->nama_odp.' '.$m->action);
          }
        }

        // $head = array_unique($head);
        //sort head per tgl
        $maintenance2 = DB::select("SELECT m.no_tiket, m.tgl_selesai
          FROM maintaince m
          right join maintaince_mtr m2 on m.id = m2.maintaince_id
          LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
          WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q2." ".$q." order by m.tgl_selesai asc, m.sto asc
        ");
        // dd($maintenance2);
        foreach ($maintenance2 as $no => $m)
        {
          if(!empty($m->no_tiket)){
            $head[] = $m->no_tiket;
          }
        }
        $head = array_unique($head);
        //endsort head
        foreach($head as $h)
        {
          $total[$h] = 0;
          $totalmaterial[$h] = 0;
          $totaljasa[$h] = 0;
        }

        // foreach ($maintenance as $no => $m){
        //   if($lastNama == $m->id_item){
        //     $data[count($data)-1]['no_tiket'][$m->no_tiket] = $m->qty;

        //     $totalmaterial[$m->no_tiket] += ($m->qty*$m->material);
        //     if($m->jenis_order == 4){
        //       $totaljasa[$m->no_tiket] += 0;
        //       $total[$m->no_tiket] += ($m->qty*$m->material);
        //     }
        //     else{
        //       $totaljasa[$m->no_tiket] += ($m->qty*$m->jasa);
        //       $total[$m->no_tiket] += ($m->qty*$m->jasa)+($m->qty*$m->material);
        //     }
        //     $no++;
        //   }else{
        //     $data[] = array("id_item" => $m->id_item, "satuan" => $m->satuan, "uraian" => $m->uraian, "jasa" => $m->jasa, "material" => $m->material, "total_boq" => $m->total_boq, "no_tiket" => array(), "total" => array());
        //     foreach($head as $h){
        //       if($h == $m->no_tiket){
        //         $data[count($data)-1]['no_tiket'][$h] = $m->qty;
        //         $totalmaterial[$h] += ($m->qty*$m->material);
        //         if($m->jenis_order == 4){
        //           $total[$h] += ($m->qty*$m->material);
        //           $totaljasa[$h] += 0;
        //         }else{
        //           $total[$h] += ($m->qty*$m->jasa)+($m->qty*$m->material);
        //           $totaljasa[$h] += ($m->qty*$m->jasa);
        //         }

        //       }else{
        //         $data[count($data)-1]['no_tiket'][$h] = 0;
        //         $data[count($data)-1]['total'][$h] = 0;
        //       }
        //     }
        //     $no=0;
        //   }
        //   $lastNama = $m->id_item;
        // }

        $renew_head = array_map(function($x){
          return $x = 0;
        }, array_flip($head) );

        foreach ($maintenance as $m)
        {
          if(!isset($data[$m->id_item]) )
          {
            $data[$m->id_item] = [
              "id_item"   => $m->id_item,
              "satuan"    => $m->satuan,
              "uraian"    => $m->uraian,
              "jasa"      => $m->jasa,
              "material"  => $m->material,
              "total_boq" => $m->total_boq,
              "no_tiket"  => []
            ];
          }
          if(empty($data[$m->id_item]['no_tiket']) )
          {
            $data[$m->id_item]['no_tiket'] = $renew_head;
          }

          if(in_array($m->no_tiket, $head) )
          {
            // $tes[$m->no_tiket][] = $m->qty;
            // $tes2[$m->no_tiket]['material'][$m->id_item][] = $m->material;
            // $tes2[$m->no_tiket]['harga'][] = ($m->qty * $m->material);

            $data[$m->id_item]['no_tiket'][$m->no_tiket] += $m->qty;
            $totalmaterial[$m->no_tiket] += ($m->qty * $m->material);


            if($m->jenis_order == 4)
            {
              $total[$m->no_tiket]     += ($m->qty * $m->material);
              $totaljasa[$m->no_tiket] += 0;
            }
            else
            {
              $total[$m->no_tiket]     += ($m->qty * $m->jasa) + ($m->qty * $m->material);
              $totaljasa[$m->no_tiket] += ($m->qty * $m->jasa);
            }
          }
        }
        // dd($data);
        if($generate_doc == 'ok')
        {
          $excel->sheet("boq", function($sheet) use($tgl, $jenis_rekon, $verif, $data,$head,$total,$totalmaterial,$totaljasa,$title){
            //return view('report.excelRincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
            $sheet->getStyle('B')->getAlignment()->setWrapText(true);

            $sheet->loadView('report.excelRincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
          });
        }

        if($generate_evi == 'ok')
        {
          $temp = array_unique(array_column(array_values(json_decode(json_encode($maintenance), true)), 'no_tiket'));
          $unique_arr = array_intersect_key(array_values(json_decode(json_encode($maintenance), true)), $temp);

          $excel->sheet("evidence", function($sheet) use($unique_arr){
            $unique_arr = json_decode(json_encode(array_values($unique_arr) ) );

            foreach($unique_arr as $k => $v)
            {
              $final_m[$k] = (object)$v;
            }

            $sheet->loadView('report.excelFoto', ['mt' => $final_m]);
          });
        }
      })->download('xlsx');

    }

    // public function cetak_rev_2($tgl, $jenis_order, $mitra, $khs){
    //   Excel::create($jenis_order.'-'.$tgl, function($excel) use($tgl, $jenis_order, $mitra, $khs){
    //     if($khs == "TELKOM"){
    //       $select = ",i.jasa_telkom as jasa, i.material_telkom as material";
    //     }else{
    //       $select = ",i.jasa_ta as jasa, i.material_ta as material";
    //     }
    //     $cond_jenis_order = " and m.jenis_order in (1,2,3,4,5,6,8,9,16,17,20)";
    //     if($jenis_order != "ALL"){
    //       $cond_jenis_order = " and m.jenis_order='".$jenis_order."'";
    //     }
    //     $maintenance = DB::select("SELECT m.*, m2.*,i.satuan $select, UPPER(i.uraian) as uraian,
    //       dispatch_regu_id, dispatch_regu_name, UPPER(m2.id_item) as id_item,
    //        (SELECT CONCAT(u_nama, ' (', username1, ')') FROM user WHERE id_karyawan = username1 GROUP BY id_karyawan)as nik_1,
    //        (SELECT CONCAT(u_nama, ' (', username2, ')') FROM user WHERE id_karyawan = username2 GROUP BY id_karyawan)as nik_2,
    //        (SELECT sum((qty*jasa)+(qty*material)) FROM maintaince_mtr mm
    //       left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_boq
    //       FROM maintaince m
    //       RIGHT JOIN maintaince_mtr m2 on m.id = m2.maintaince_id
    //       LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
    //       LEFT JOIN regu mr on m.dispatch_regu_id = mr.id_regu
    //       WHERE m.isHapus = 0 AND (m.tgl_selesai like '$tgl%' OR m.tgl_selesai IS NULL) $cond_jenis_order and m.nama_mitra='$mitra' order by m.tgl_selesai asc, m2.id_item asc"
    //     );
    //     // dd($maintenance);
    //     $data = array();
    //     $lastNama = '';
    //     $no=0;
    //     $head = array();
    //     $title = array();
    //     foreach ($maintenance as $no => $m){
    //       if(!empty($m->no_tiket)){
    //         $head[] = $m->no_tiket;
    //         $title[$m->no_tiket] = array('tgl'=>$m->tgl_selesai, 'nik1'=>$m->nik_1, 'nik2'=>$m->nik_2, 'regu_name' =>$m->dispatch_regu_name, 'no_tiket'=>$m->no_tiket,'sto'=>'STO-'.$m->sto,'action'=>date("d-m-Y", strtotime($m->tgl_selesai)).' '.$m->nama_odp.' '.$m->action);
    //       }
    //     }
    //     $head = array_unique($head);
    //     //sort head per tgl
    //     $sorthead = array();
    //     $maintenance2 = DB::select("SELECT m.no_tiket, m.tgl_selesai
    //       FROM maintaince m
    //       right join maintaince_mtr m2 on m.id = m2.maintaince_id
    //       LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
    //       left join regu mr on m.dispatch_regu_id = mr.id_regu
    //       WHERE m.isHapus = 0 AND (m.tgl_selesai like '$tgl%' OR m.tgl_selesai IS NULL) $cond_jenis_order and m.nama_mitra='$mitra' order by m.tgl_selesai asc, m.sto asc
    //     ");
    //     $var_foto =array();
    //     foreach ($maintenance2 as $no => $m){
    //       if(!empty($m->no_tiket)){
    //         $sorthead[] = $m->no_tiket;
    //       }
    //     }
    //     $sorthead = array_unique($sorthead);
    //     $head = $sorthead;
    //     //endsort head
    //     $total = array();
    //     $totalmaterial = array();
    //     $totaljasa = array();
    //     foreach($head as $h){
    //       $total[$h] = 0;
    //       $totalmaterial[$h] = 0;
    //       $totaljasa[$h] = 0;
    //     }
    //     foreach ($maintenance as $no => $m){
    //       if($lastNama == $m->id_item){
    //         $data[count($data)-1]['no_tiket'][$m->no_tiket] = $m->qty;

    //         $totalmaterial[$m->no_tiket] += ($m->qty*$m->material);
    //         if($m->jenis_order == 4){
    //           $totaljasa[$m->no_tiket] += 0;
    //           $total[$m->no_tiket] += ($m->qty*$m->material);
    //         }
    //         else{
    //           $totaljasa[$m->no_tiket] += ($m->qty*$m->jasa);
    //           $total[$m->no_tiket] += ($m->qty*$m->jasa)+($m->qty*$m->material);
    //         }
    //         $no++;
    //       }else{
    //         $data[] = array("id_item" => $m->id_item, "satuan" => $m->satuan, "uraian" => $m->uraian, "jasa" => $m->jasa, "material" => $m->material, "total_boq" => $m->total_boq, "no_tiket" => array(), "total" => array());
    //         foreach($head as $h){
    //           if($h == $m->no_tiket){
    //             $data[count($data)-1]['no_tiket'][$h] = $m->qty;
    //             $totalmaterial[$h] += ($m->qty*$m->material);
    //             if($m->jenis_order == 4){
    //               $total[$h] += ($m->qty*$m->material);
    //               $totaljasa[$h] += 0;
    //             }else{
    //               $total[$h] += ($m->qty*$m->jasa)+($m->qty*$m->material);
    //               $totaljasa[$h] += ($m->qty*$m->jasa);
    //             }

    //           }else{
    //             $data[count($data)-1]['no_tiket'][$h] = 0;
    //             $data[count($data)-1]['total'][$h] = 0;
    //           }
    //         }
    //         $no=0;
    //       }
    //       $lastNama = $m->id_item;
    //     }
    //     // dd($data);
    //     $excel->sheet("boq", function($sheet) use($tgl, $jenis_order, $mitra, $data,$head,$total,$totalmaterial,$totaljasa,$title){
    //       //dd($title);
    //       //return view('report.excelRincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
    //       $sheet->getStyle('B')->getAlignment()->setWrapText(true);

    //       $sheet->loadView('report.excelRincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
    //     });
    //     $mt = DB::select("SELECT m.*, (select count(*) from maintaince_mtr where maintaince_id=m.id) as mtr
    //       FROM maintaince m
    //       LEFT JOIN regu mr on m.dispatch_regu_id = mr.id_regu
    //       WHERE m.isHapus = 0 AND (m.tgl_selesai like '$tgl%' OR m.tgl_selesai IS NULL) $cond_jenis_order and m.nama_mitra='$mitra' having mtr>0 order by m.tgl_selesai asc, m.sto asc
    //     ");
    //     $excel->sheet("evidence", function($sheet) use($tgl, $jenis_order, $mt){
    //       $sheet->loadView('report.excelFoto', compact('mt'));
    //     });
    //     $excel->sheet("evidence", function($sheet) use($tgl, $jenis_order, $mt){
    //       $sheet->loadView('report.excelFotoHorizon', compact('mt'));
    //     });
    //   })->download('xlsx');

    // }

    public function cetak_rev($tgl, $jenis_order, $mitra, $khs){
      Excel::create($jenis_order.'-'.$tgl, function($excel) use($tgl, $jenis_order, $mitra, $khs)
      {
        if($khs == "TELKOM")
        {
          $select = ",i.jasa_telkom as jasa, i.material_telkom as material";
        }
        else
        {
          $select = ",i.jasa_ta as jasa, i.material_ta as material";
        }

        $cond_jenis_order = " and m.jenis_order in (1,2,3,4,5,6,8,9,16,17,20)";

        if($jenis_order != "ALL")
        {
          $cond_jenis_order = " and m.jenis_order='".$jenis_order."'";
        }

        $maintenance = DB::select("SELECT m.*, m2.*,i.satuan $select, UPPER(i.uraian) as uraian,
          dispatch_regu_id, dispatch_regu_name, UPPER(m2.id_item) as id_item,
           (SELECT CONCAT(u_nama, ' (', username1, ')') FROM user WHERE id_karyawan = username1 GROUP BY id_karyawan)as nik_1,
           (SELECT CONCAT(u_nama, ' (', username2, ')') FROM user WHERE id_karyawan = username2 GROUP BY id_karyawan)as nik_2,
           (SELECT sum((qty*jasa)+(qty*material)) FROM maintaince_mtr mm
          left join khs_maintenance i on mm.id_item = i.id_item WHERE mm.maintaince_id = m.id) as total_boq
          FROM maintaince m
          RIGHT JOIN maintaince_mtr m2 on m.id = m2.maintaince_id
          LEFT JOIN khs_maintenance i on m2.id_item = i.id_item
          LEFT JOIN regu mr on m.dispatch_regu_id = mr.id_regu
          WHERE m.isHapus = 0 AND (m.tgl_selesai like '$tgl%' OR m.tgl_selesai IS NULL) $cond_jenis_order and m.nama_mitra = '$mitra' order by m.tgl_selesai asc, m2.id_item asc"
        );

        $data = $head = $title = array();
        // $lastNama = '';
        // $no=0;

        foreach ($maintenance as $no => $m)
        {
          if(!empty($m->no_tiket)){
            // $head[] = $m->no_tiket;
            $title[$m->no_tiket] = array('tgl'=>$m->tgl_selesai, 'nik1'=>$m->nik_1, 'nik2'=>$m->nik_2, 'regu_name' =>$m->dispatch_regu_name, 'no_tiket'=>$m->no_tiket,'sto'=>'STO-'.$m->sto,'action'=>date("d-m-Y", strtotime($m->tgl_selesai)).' '.$m->nama_odp.' '.$m->action);
          }
        }
        // $head = array_unique($head);
        //sort head per tgl
        $maintenance2 = DB::select("SELECT m.no_tiket, m.tgl_selesai
          FROM maintaince m
          right join maintaince_mtr m2 on m.id = m2.maintaince_id
          LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
          left join regu mr on m.dispatch_regu_id = mr.id_regu
          WHERE m.isHapus = 0 AND (m.tgl_selesai like '$tgl%' OR m.tgl_selesai IS NULL) $cond_jenis_order and m.nama_mitra = '$mitra' order by m.tgl_selesai asc, m.sto asc
        ");

        foreach ($maintenance2 as $no => $m)
        {
          if(!empty($m->no_tiket) )
          {
            $head[] = $m->no_tiket;
          }
        }

        $head = array_unique($head);
        //endsort head
        $total = $totalmaterial = $totaljasa = array();

        foreach($head as $h){
          $total[$h] = 0;
          $totalmaterial[$h] = 0;
          $totaljasa[$h] = 0;
        }

        $renew_head = array_map(function($x){
          return $x = 0;
        }, array_flip($head) );

        foreach ($maintenance as $m)
        {
          if(!isset($data[$m->id_item]) )
          {
            $data[$m->id_item] = [
              "id_item"   => $m->id_item,
              "satuan"    => $m->satuan,
              "uraian"    => $m->uraian,
              "jasa"      => $m->jasa,
              "material"  => $m->material,
              "total_boq" => $m->total_boq,
              "no_tiket"  => []
            ];
          }
          if(empty($data[$m->id_item]['no_tiket']) )
          {
            $data[$m->id_item]['no_tiket'] = $renew_head;
          }

          if(in_array($m->no_tiket, $head) )
          {
            // $tes[$m->no_tiket][] = $m->qty;
            // $tes2[$m->no_tiket]['material'][$m->id_item][] = $m->material;
            // $tes2[$m->no_tiket]['harga'][] = ($m->qty * $m->material);

            $data[$m->id_item]['no_tiket'][$m->no_tiket] += $m->qty;
            $totalmaterial[$m->no_tiket] += ($m->qty * $m->material);


            if($m->jenis_order == 4)
            {
              $total[$m->no_tiket]     += ($m->qty * $m->material);
              $totaljasa[$m->no_tiket] += 0;
            }
            else
            {
              $total[$m->no_tiket]     += ($m->qty * $m->jasa) + ($m->qty * $m->material);
              $totaljasa[$m->no_tiket] += ($m->qty * $m->jasa);
            }
          }
        }

        $data = array_values($data);
        $excel->sheet("boq", function($sheet) use($data, $total, $head, $totalmaterial, $totaljasa, $title){

          $header[0] = ['#', 'Id Item', 'Uraian', 'Jasa', 'Material', 'Satuan'];
          $header[1] = [null, null, null, null, null, null];
          $header[2] = [null, null, null, null, null, null];
          $header[3] = [null, null, null, null, null, null];
          $header[4] = [null, null, null, null, null, null];

          foreach($head as $h)
          {
            $header[0][] = $h;
            $header[1][] = preg_replace("/^=/", "", $title[$h]['action'], 1);
            $header[2][] = $title[$h]['regu_name'];
            $header[3][] = $title[$h]['nik1'] .'&'. $title[$h]['nik2'];
            $header[4][] = $title[$h]['sto'];
          }

          $no_array = 4;

          foreach($data as $no => $list)
          {
            $subtotal = 0;
            ++$no_array;
            $header[$no_array][] = ++$no;
            $header[$no_array][] = $list['id_item'];
            $header[$no_array][] = $list['uraian'];
            $header[$no_array][] = (int)$list['jasa'];
            $header[$no_array][] = (int)$list['material'];
            $header[$no_array][] = $list['satuan'];

            foreach($list['no_tiket'] as $l2)
            {
              $header[$no_array][] = $l2 ?: '-';
              $subtotal +=  (int)($l2 * $list['jasa']) + ($l2 * $list['material']) ;
            }

            $header[$no_array][] = $subtotal;
          }

          $header[($no_array + 2)] = ['Total Material', null, null, null, null, null];
          $header[($no_array + 3)] = ['Total Jasa', null, null, null, null, null];
          $header[($no_array + 4)] = ['Total', null, null, null, null, null];

          $header[0][] = 'Total';

          $sumtotal = $sumtotaljasa = $sumtotalmaterial = 0;

          foreach($head as $h)
          {
            $header[($no_array + 2)][] = (int)@$totalmaterial[$h];
            $sumtotalmaterial += @$totalmaterial[$h];

            $header[($no_array + 3)][] = (int)@$totaljasa[$h];
            $sumtotaljasa += @$totaljasa[$h];

            $header[($no_array + 4)][] = (int)@$total[$h];
            $sumtotal += @$total[$h];
          }

          $header[($no_array + 2)][] = (int)$sumtotalmaterial;
          $header[($no_array + 3)][] = (int)$sumtotaljasa;
          $header[($no_array + 4)][] = (int)$sumtotal;

          $border_Style = [
            'borders' => [
              'outline' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN
              ],
              'inside' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN
              ],
            ]
          ];

          $sheet->rows($header);

          $sheet->getStyle('A1:F5')->getAlignment()->setWrapText(true);
          $sheet->mergeCells('A1:A5');
          $sheet->mergeCells('B1:B5');
          $sheet->mergeCells('C1:C5');
          $sheet->mergeCells('D1:D5');
          $sheet->mergeCells('E1:E5');
          $sheet->mergeCells('F1:F5');
          $sheet->mergeCells('A'. ($no_array + 2) .':F' . ($no_array + 2) );
          $sheet->mergeCells('A'. ($no_array + 3) .':F' . ($no_array + 3) );
          $sheet->mergeCells('A'. ($no_array + 4) .':F' . ($no_array + 4) );

          $sheet->getStyle('A'. ($no_array + 2) .':F' . ($no_array + 4) )->applyFromArray([
            'alignment' => [
              'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            ]
          ]);

          $dc = new DashboardController();

          $sheet->mergeCells($dc->number_to_alphabet(count($head) + 7 ) .'1:'. $dc->number_to_alphabet(count($head) + 7 ) .'5');
          $sheet->getStyle('G1:'. $dc->number_to_alphabet(count($head) + 7 ) .'5' )->getFont()->setBold(true);
          $sheet->getStyle('A1:'.$dc->number_to_alphabet(count($head) + 7 ).''. ($no_array + 4) )->applyFromArray($border_Style);
          $sheet->getStyle('A1:'.$dc->number_to_alphabet(count($head) + 7 ).''. ($no_array + 1) )->applyFromArray([
            'alignment' => [
              'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
              'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ]
          ]);
        });

        $mt = DB::select("SELECT m.*, (select count(*) from maintaince_mtr where maintaince_id=m.id) as mtr
        FROM maintaince m
        LEFT JOIN regu mr on m.dispatch_regu_id = mr.id_regu
        WHERE m.isHapus = 0 AND (m.tgl_selesai like '$tgl%' OR m.tgl_selesai IS NULL) $cond_jenis_order and m.nama_mitra = '$mitra' having mtr > 0 order by m.tgl_selesai asc, m.sto asc
        ");

        $excel->sheet('evidence', function($sheet) use($mt) {
          $nomor_in = 0;
          $raw_data = $alphabet = $raw_photo = [];

          for ($i = 'A'; $i !== 'ZZ'; $i++){
            $alphabet[] = $i;
          }

          foreach($mt as $no => $list)
          {
            $raw_data[$no]['judul'] = ++$nomor_in .'.'. $list->no_tiket;
            $raw_photo[$no]['judul'] = null;

            if(@$list->jenis_order == 17)
            {
              $template_photo[$list->id][] = $list->id."/Redaman-IN-OUT-Sesudah";
            }
            else
            {
              if ($list->jenis_order == 5)
              {
                $template_photo[$list->id]["Pemasangan-Aksesoris-Tiang-Sesudah(dekat)"] = $list->id."/Pemasangan-Aksesoris-Tiang-Sesudah(dekat)";
              }
              else
              {
                $template_photo[$list->id]["Foto-GRID"] = $list->id."/Foto-GRID";
              }
            }

            foreach ($template_photo as $k => $v)
            {
              if ($k == $list->id)
              {
                foreach($v as $kk => $photo)
                {
                  $path  = "/upload/maintenance/".$photo;
                  $path2 = "/upload2/maintenance/".$photo;
                  $path3 = "/upload3/maintenance/".$photo;
                  $th    = "$path-th.jpg";
                  $th2   = "$path2-th.jpg";
                  $th3   = "$path3-th.jpg";

                  $raw_data[$no]['id']  = str_replace('-', ' ', $k);
                  $raw_photo[$no]['id'] = null;

                  if (file_exists(public_path().$th) )
                  {
                    $raw_photo[$no]['path_foto'] = public_path().$th;
                    $raw_data[$no]['path_foto']  = 'halaman photo';
                  }
                  elseif (file_exists(public_path().$th2) )
                  {
                    $raw_photo[$no]['path_foto'] = public_path().$th2;
                    $raw_data[$no]['path_foto']  = 'halaman photo';
                  }
                  elseif (file_exists(public_path().$th3) )
                  {
                    $raw_photo[$no]['path_foto'] = public_path().$th3;
                    $raw_data[$no]['path_foto']  = 'halaman photo';
                  }
                  else
                  {
                    $raw_photo[$no]['path_foto'] = public_path(). "/image/placeholder.gif";
                    $raw_data[$no]['path_foto']  = 'halaman photo';
                  }
                }
              }
            }
          }

          $data = $data_photo = [];
          foreach($raw_data as $v)
          {
            foreach($v as $vv)
            {
              $data[] = $vv;
            }
          }

          foreach($raw_photo as $v)
          {
            foreach($v as $vv)
            {
              $data_photo[] = $vv;
            }
          }

          if($data)
          {
            $data = array_chunk($data, 36);
          }

          $data_photo = array_chunk($data_photo, 36);

          $sheet->rows($data);

          if($data)
          {
            foreach($data_photo as $k => $v)
            {
              ++$k;
              foreach($v as $kk => $vv)
              {
                if(file_exists($vv) )
                {
                  $objDrawing = new \PHPExcel_Worksheet_Drawing;
                  $objDrawing->setPath($vv);
                  $objDrawing->setCoordinates($alphabet[$kk] . '' . $k);
                  $objDrawing->setHeight(138);
                  $objDrawing->setWidth(103);
                  $objDrawing->setWorksheet($sheet);
                }
              }
            }

            for ($i = 1 ; $i < count($data_photo) + 3; $i++) {
              $set_height[] = 120;
            }

            $sheet->setHeight($set_height);
            $sheet->getStyle('A1:'.$alphabet[(count($data[0]) - 1)].''. count($data) )->applyFromArray([
              'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
              ]
            ]);
          }
        });

        $excel->sheet("evidence 2", function($sheet) use($mt){
          $data = $raw_photo = [];
          $nomor = 0;

          for ($i = 'A'; $i !== 'ZZ'; $i++){
            $alphabet[] = $i;
          }

          foreach($mt as $no => $list)
          {
            ++$nomor;

            $path = public_path() . "/upload/maintenance/" . $list->id . "/";
            $path_file = @preg_grep('~^\w(.*-th).*$~', @scandir($path) );

            $path2 = public_path() . "/upload2/maintenance/" . $list->id . "/";
            $path2_file = @preg_grep('~^\w(.*-th).*$~', @scandir($path2) );

            $path3 = public_path() . "/upload3/maintenance/" . $list->id . "/";
            $path3_file = @preg_grep('~^\w(.*-th).*$~', @scandir($path3) );

            if(count($path_file) != 0)
            {
              $path_photo_arr = [];

              $files_path = array_values($path_file);
              if ($files_path) {
                foreach($files_path as $v)
                {
                  $path_photo_arr[$list->id][] = public_path() . "/upload/maintenance/" . $list->id . "/" . $v;
                }
              }
            }
            elseif(count($path2_file) != 0)
            {
              $path_photo_arr = [];

              $files_path = array_values($path2_file);
              if ($files_path) {
                foreach($files_path as $v)
                {
                  $path_photo_arr[$list->id][] = public_path() . "/upload2/maintenance/" . $list->id . "/" . $v;
                }
              }
            }
            elseif(count($path3_file) != 0)
            {
              $path_photo_arr = [];

              $files_path = array_values($path3_file);
              if ($files_path) {
                foreach($files_path as $v)
                {
                  $path_photo_arr[$list->id][] = public_path() . "/upload3/maintenance/" . $list->id . "/" . $v;
                }
              }
            }
            else
            {
              $path_photo_arr[$list->id][] = public_path() . "/image/placeholder.gif";
            }

            $data[$no]['no'] = $nomor;
            $data[$no]['id'] = $list->no_tiket;

            $raw_photo[$no]['no'] = null;
            $raw_photo[$no]['id'] = null;


            foreach ($path_photo_arr as $k => $v)
            {
              foreach ($v as $kk => $vv)
              {
                if($k == $list->id)
                {
                  $data[$no]['photo'. $kk] = 'Template Photo';
                  $raw_photo[$no]['photo'. $kk] = $vv;
                }
              }
            }
          }

          $sheet->rows($data);
          foreach($raw_photo as $k => $v)
          {
            ++$k;
            $v = array_values($v);
            foreach($v as $kk => $vv)
            {
              if(file_exists($vv) )
              {
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath($vv);
                $objDrawing->setCoordinates($alphabet[$kk] . '' . $k);
                $objDrawing->setHeight(138);
                $objDrawing->setWidth(103);
                $objDrawing->setWorksheet($sheet);
              }
            }
          }

          if($data)
          {
            $no_pht = 0;

            foreach ($raw_photo as $k => $v)
            {
              ++$no_pht;
              foreach($v as $kk => $vv)
              {
                if($vv)
                {
                  $set_height[$no_pht] = 120;
                }
              }
            }

            $sheet->setHeight($set_height);
            $sheet->getStyle('A1:'.$alphabet[(count($data[0]) - 1)].''. count($data) )->applyFromArray([
              'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
              ]
            ]);
          }
        });

      })->export('xlsx');
    }
    public function downloadForm(){
      $data = DB::table('maintenance_jenis_order')->where('aktif', 1)->select('*', 'nama_order as text')->get();
      return view('report.downloadForm', compact('data'));
    }

    public function downloadSubmit_kategoryal(Request $req)
    {
      $tgl = $req->tgl_flex;
      if($req->kategory == 1)
      {
        Excel::create('Rekap Closing Per Tim', function ($excel) use ($tgl) {
            $excel->sheet('sheet1', function ($sheet) use ($tgl) {
              $data = DB::SELECT(
              "SELECT m.no_tiket As nomorTiket,
              m.dispatch_regu_name As regu,
              m.refer,
              (SELECT CONCAT(sum(qty), ' TIANG (', headline, ')') FROM maintaince_mtr mm LEFT JOIN maintaince ms ON mm.maintaince_id = ms.id WHERE ms.dispatch_regu_name = m.dispatch_regu_name AND m.jenis_order = 6 AND (mm.id_item = 'TT-7' OR mm.id_item = 'TT-9')) as jumlah_tiang,
              SUM(CASE WHEN m.jenis_order = 3 THEN 1 ELSE 0 END) as odp_loss,
              SUM(CASE WHEN m.jenis_order = 18 THEN 1 ELSE 0 END) as rebo,
              SUM(CASE WHEN m.jenis_order = 9 THEN 1 ELSE 0 END) as odp_full,
              SUM(CASE WHEN m.jenis_order = 6 THEN 1 ELSE 0 END) as ins_tiang,
              SUM(CASE WHEN m.jenis_order = 8 THEN 1 ELSE 0 END) as odp_sehat,
              m.nama_order As Jenis, m2.id As id_mt2,
              m.status
              FROM maintaince m
              LEFT JOIN maintaince m2 ON m2.id = m.refer
              WHERE m.isHapus = 0 AND (m.created_at LIKE '$tgl%' OR m.tgl_selesai LIKE '$tgl%') AND m.nama_order IN ('ODP LOSS', 'ODP FULL', 'INSERT TIANG') GROUP BY m.dispatch_regu_name, m.nama_order, status HAVING odp_loss != 0 OR rebo != 0 OR jumlah_tiang  IS NOT NULL");
              // dd($data);
              $sheet->loadView('report.excelDownloadKatForm', compact('data')
            );

            foreach($data as $k => $v)
            {
              if(!is_null($v->refer) && empty($v->id_mt2) )
              {
                unset($data[$k]);
              }
            }
          });
        })->download('xlsx');
      }
      elseif($req->kategory == 2)
      {
        Excel::create('mt', function($excel) use($tgl){
          $excel->sheet('Rekap Order Majid', function($sheet) use($tgl){
            $data = DB::table('maintaince')
            ->leftjoin('regu', 'regu.id_regu', '=', 'maintaince.dispatch_regu_id')
            ->select('maintaince.*', 'regu.uraian as ur_regu', 'regu.label_regu', 'regu.TL', 'regu.label_sektor', 'regu.nama_mitra', DB::raw('(select customer from psb_myir_wo where myir = no_tiket limit 0,1) as customerName, (SELECT CUSTOMER_NAME FROM kpro_tr6 WHERE SPEEDY = maintaince.no_inet limit 0,1) as nama_customer_kpro, (SELECT CUSTOMER_ADDRESS FROM kpro_tr6 WHERE SPEEDY = maintaince.no_inet limit 0,1) as alamat_kpro, (SELECT NO_HP FROM kpro_tr6 WHERE SPEEDY = maintaince.no_inet limit 0,1) as telp_kpro, (select picPelanggan from psb_myir_wo where myir = no_tiket limit 0,1) as myirpic, (SELECT u_nama FROM user WHERE id_user = maintaince.username1 GROUP BY id_karyawan) As nama1, (SELECT u_nama FROM user WHERE id_user = maintaince.username2 GROUP BY id_karyawan) As nama2, (SELECT u_nama FROM user WHERE id_user = regu.TL GROUP BY id_karyawan) As nama_tl'))
            ->whereIn('jenis_order', [3, 6 ,8 ,9 ,18])
            ->where('maintaince.isHapus', 0)
            ->where(function($join) use($tgl)
            {
              $join->where('tgl_selesai', 'like', $tgl.'%')
              ->OrWhere('created_at', 'like', $tgl.'%');
            })
            ->get();
            // dd($data);
            $sheet->loadView('report.excelDownloadForm', compact('data'));
          });
        })->download('xlsx');
      }
      elseif($req->kategory == 3)
      {
        Excel::create('Rekap Valins', function($excel) use($tgl){
          // $data = DB::select("SELECT m.no_tiket, m.headline, m.dispatch_regu_name, valins.*, mv.maintenance_id,
          // (CASE WHEN mv.jenis = 'sebelum' THEN 'ID Valins Gendong Sebelum'
          // WHEN mv.jenis = 'sesudah' THEN 'ID Valins Gendong Sesudah'
          // WHEN mv.jenis = 'reboundary' THEN 'ID Valins ODP Reboundary'
          // ELSE 'Tidak Ada!' END) as Jenis_mv,
          // (SELECT u_nama FROM user WHERE id_user = m.username1 GROUP BY id_karyawan) As nama1, (SELECT u_nama FROM user WHERE id_user = m.username2 GROUP BY id_karyawan) As nama2, (SELECT u_nama FROM user WHERE id_user = r.TL GROUP BY id_karyawan) As nama_tl, mv.koordinat_odp, mv.koor_pelanggan
          // from valins
          // LEFT JOIN maintenance_valins mv ON valins.valins_id = mv.valins_id
          // LEFT JOIN maintaince m ON m.id = mv.maintenance_id
          // LEFT JOIN regu r ON r.id_regu = m.dispatch_regu_id
          // where m.isHapus = 0 AND (m.tgl_selesai LIKE '$tgl%' OR m.created_at LIKE '$tgl%')");

          $data = DB::SELECT("SELECT * FROM maintaince WHERE isHapus = 0 AND valins_awal IS NOT NULL AND (tgl_selesai LIKE '$tgl%' OR created_at LIKE '$tgl%')");

          $excel->sheet('Rekap Order Majid', function($sheet) use($data){
            $header[0] = ['#', 'No Tiket', 'Regu', 'Valins Awal', 'Valins Akhir', 'Tanggal Valins Awal', 'Tanggal Valins Akhir'];
            foreach($data as $k => $v)
            {
              ++$k;
              $header[$k][] = $k;
              $header[$k][] = $v->no_tiket;
              $header[$k][] = $v->dispatch_regu_name;
              $header[$k][] = $v->valins_awal;
              $header[$k][] = $v->valins_akhir;
              $header[$k][] = $v->tgl_valins_awal;
              $header[$k][] = $v->tgl_valins_akhir;
            }

            $border_Style = [
              'borders' => [
                'outline' => [
                  'style' => \PHPExcel_Style_Border::BORDER_THIN
                ],
                'inside' => [
                  'style' => \PHPExcel_Style_Border::BORDER_THIN
                ],
              ]
            ];

            $sheet->rows($header);

            $sheet->getStyle('A1:G'. ($k + 1) )->applyFromArray($border_Style);
            $sheet->getStyle('A1:G'. ($k + 1) )->applyFromArray([
              'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
              ]
            ]);
          });

        })->download('xlsx');
      }
      elseif($req->kategory == 4)
      {
        Excel::create('Rekap NOG', function($excel) use($tgl){
          $data = DB::select("SELECT nm.* FROM nog_master nm
          WHERE nm.created_at BETWEEN '2022-08-01' AND '2022-10-20' ");

          $excel->sheet('Rekap NOG', function($sheet) use($data){
            $header[0] = ['#', 'ODP', 'STO', 'Koordinat ODP', 'Jumlah Port', 'Regu', 'Jenis Order', 'Tindak Lanjut', 'Note', 'Created_by', 'Created_at'];

            foreach($data as $k => $v)
            {
              ++$k;
              $header[$k][] = $k;
              $header[$k][] = $v->nama_odp;
              $header[$k][] = $v->sto_nog;
              $header[$k][] = $v->koordinat_odp;
              $header[$k][] = $v->jml_port;
              $header[$k][] = $v->regu_nama;
              $header[$k][] = $v->jenis_order;
              $header[$k][] = $v->tindak_lanjut;
              $header[$k][] = $v->note;
              $header[$k][] = $v->created_by;
              $header[$k][] = $v->created_at;
            }

            $border_Style = [
              'borders' => [
                'outline' => [
                  'style' => \PHPExcel_Style_Border::BORDER_THIN
                ],
                'inside' => [
                  'style' => \PHPExcel_Style_Border::BORDER_THIN
                ],
              ]
            ];

            $sheet->rows($header);

            $sheet->getStyle('A1:K'. ($k + 1) )->applyFromArray($border_Style);
            $sheet->getStyle('A1:K'. ($k + 1) )->applyFromArray([
              'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
              ]
            ]);
          });

        })->download('xlsx');
      }
    }

    public function downloadFormUN(){
      return view('report.downloadUnderS');
    }
    public function download_undresp(Request $req)
    {
      //XXX: khusus approve TELKOM
      $tgl = $req->tgl;
      $data = DB::SELECT("SELECT maintaince.id, maintaince.no_tiket, maintaince.tgl_selesai, maintaince.sto, maintaince.dispatch_regu_name,
      CONCAT((SELECT CONCAT(u_nama, ' (', maintaince.username1, ')') FROM user WHERE id_karyawan = maintaince.username1 GROUP BY id_karyawan), '&', (SELECT CONCAT(u_nama, ' (', maintaince.username2, ')') FROM user WHERE id_karyawan = maintaince.username2 GROUP BY id_karyawan) ) as nama_tim,
      null as ND,
      maintaince.no_inet as nomor_internet,
      null as nama,
      maintaince.jenis_order,
      null as alamat,
      maintaince.refer,
      maintaince.nama_odp,
      maintaince.status,
      maintaince.status_detail,
      regu.label_regu,
      regu.label_sektor,
      maintaince.redaman_awal,
      maintaince.redaman_akhir,
      m2.id As id_mt2,
      maintaince.action as penyebab_remo,
      maintaince.action_cause as tindak_lanjut
      FROM maintaince
      LEFT JOIN maintaince m2 ON m2.id = maintaince.refer
      LEFT JOIN regu ON regu.id_regu = maintaince.dispatch_regu_id
      WHERE maintaince.isHapus = 0 AND maintaince.jenis_order = 4  AND maintaince.status = 'close' AND maintaince.tgl_selesai LIKE '$tgl%' AND maintaince.verify =2 AND (maintaince.mtrcount > 0 or maintaince.action NOT IN ('CANCEL ORDER', 'REDAMAN AMAN')) order by maintaince.tgl_selesai asc, maintaince.sto asc");

      foreach($data as $k => $v)
      {
        if(!is_null($v->refer) && empty($v->id_mt2) )
        {
          unset($data[$k]);
        }
      }
      $material = DB::SELECT("SELECT maintaince_id as id, id_item, qty
      FROM maintaince

      LEFT JOIN maintaince_mtr ON maintaince.id = maintaince_mtr.maintaince_id
      WHERE maintaince.isHapus = 0 AND jenis_order = 4  AND maintaince.status = 'close' AND tgl_selesai LIKE '$tgl%' AND verify =2 ");

      $list_material = $redata = [];

      foreach ($data as $keyp => $valp) {
        $redata[$keyp] = $valp;

        foreach ($material as $valc) {
          $redata[$keyp]->material[$valc->id_item]= '-';
          $list_material[$valc->id_item] = $valc->id_item;
        }

        foreach ($material as $valc) {
          if ($valc->id == $valp->id) {
            $redata[$keyp]->material[$valc->id_item]= intval($valc->qty);
          }
        }
      }
      // dd($redata);
      Excel::create('Data Underspect', function($excel) use($list_material, $redata){
        $excel->sheet('UnderSpect', function($sheet) use($list_material, $redata){
          $sheet->loadView('report.excelDownloadunderspect', compact('list_material', 'redata'));
        });
        $excel->sheet('Foto UnderSpect', function($sheet) use($list_material, $redata){
          $sheet->loadView('report.excelFoto', ['mt' => $redata]);
        });
      })->download('xlsx');
    }
    public function downloadSubmit(Request $req){
      $jenis_order = $req->jenis_order;
      $tgl =$req->tgl;

      $get_nama_jo = DB::table('maintenance_jenis_order')->where('id', $jenis_order)->first();

      if($req->type == 'dwn'){
        Excel::create('Hasil Download '. $get_nama_jo->nama_order . ' Periode ' . $tgl, function($excel) use($jenis_order, $tgl, $get_nama_jo){
          $excel->sheet($get_nama_jo->nama_order, function($sheet) use($jenis_order, $tgl){
            $data = DB::table('maintaince')
            ->leftjoin('regu', 'regu.id_regu', '=', 'maintaince.dispatch_regu_id')
            ->leftjoin('maintaince As mt2', 'mt2.id', '=', 'maintaince.refer')
            ->select('maintaince.*', 'mt2.id As id_mt2', 'regu.uraian as ur_regu', 'regu.TL', 'regu.label_regu', 'regu.label_sektor', 'regu.nama_mitra', DB::raw('(select customer from psb_myir_wo where myir = maintaince.no_tiket limit 0,1) as customerName, (SELECT CUSTOMER_NAME FROM kpro_tr6 WHERE SPEEDY = maintaince.no_inet limit 0,1) as nama_customer_kpro, (SELECT CUSTOMER_ADDRESS FROM kpro_tr6 WHERE SPEEDY = maintaince.no_inet limit 0,1) as alamat_kpro, (SELECT NO_HP FROM kpro_tr6 WHERE SPEEDY = maintaince.no_inet limit 0,1) as telp_kpro, (select picPelanggan from psb_myir_wo where myir = maintaince.no_tiket limit 0,1) as myirpic, (SELECT u_nama FROM user WHERE id_user = maintaince.username1 GROUP BY id_karyawan) As nama1, (SELECT u_nama FROM user WHERE id_user = maintaince.username2 GROUP BY id_karyawan) As nama2, (SELECT u_nama FROM user WHERE id_user = maintaince.username3 GROUP BY id_karyawan) As nama3, (SELECT u_nama FROM user WHERE id_user = maintaince.username4 GROUP BY id_karyawan) As nama4, (SELECT u_nama FROM user WHERE id_user = regu.TL GROUP BY id_karyawan) As nama_tl'))
            ->where([
              ['maintaince.jenis_order', $jenis_order],
              ['maintaince.isHapus', 0],
            ])
            ->where(function($join) use($tgl)
            {
              $join->where('maintaince.tgl_selesai', 'like', $tgl.'%')
              ->orwhere('maintaince.status', 'ogp')
              ->orwhereNull('maintaince.status')
              ->whereRaw("YEAR(maintaince.created_at) > YEAR(DATE_SUB(CURDATE(), INTERVAL 2 YEAR))");
            })
            ->get();

            foreach($data as $k => $v)
            {
              if(!is_null($v->refer) && empty($v->id_mt2) )
              {
                unset($data[$k]);
              }
            }

            $wrong = [];

            foreach($data as $key => $new_data)
            {
              if(json_encode($new_data) != false)
              {
                $renew_data[$key] = $new_data;
              }
              else
              {
                $wrong[] = $new_data;
                $renew_data[$key] = (object)preg_replace('/[[:^print:]]/', '', (array)$new_data);
              }
            }

            $data = array_values(json_decode(json_encode($renew_data), TRUE) );
            $data = json_decode(json_encode($data) );

            $sheet->loadView('report.excelDownloadForm', compact('data') );
          });
        })->download('xlsx');
      }else{
        Excel::create('Rebound', function($excel) use($jenis_order, $tgl, $get_nama_jo){
        $excel->sheet($get_nama_jo->nama_order, function($sheet) use($jenis_order, $tgl){
          $list = DB::table('maintenance_reboundary')
          ->leftjoin('maintaince', 'maintaince.id', '=', 'maintenance_reboundary.maintenance_id')
          ->leftjoin('maintaince As mt2', 'mt2.id', '=', 'maintaince.refer')
          ->leftjoin('regu', 'regu.id_regu', '=', 'maintaince.dispatch_regu_id')
          ->select('maintenance_reboundary', 'maintaince', 'regu.nama_mitra', 'regu.label_regu', 'regu.label_sektor')
          ->where([
           ['maintaince.jenis_order', $jenis_order],
           ['maintaince.created_at', 'like', $tgl.'%'],
           ['maintaince.isHapus', 0]
          ])
          ->get();

          foreach($list as $k => $v)
          {
            if(!is_null($v->refer) && empty($v->id_mt2) )
            {
              unset($list[$k]);
            }
          }

          $list = array_values(json_decode(json_encode($list), TRUE) );
          $list = json_decode(json_encode($list) );

          $sheet->loadView('report.reboundary_rep', compact('list'));
        });
      })->download('xlsx');
      }
      return redirect()->back();
    }
    public function revmitrav2($mitra,$tgl){
      if($mitra=="MITRA"){
        $sql = "(qty*jasa_ta)+(qty*material_ta)";
      }else{
        $sql = "(qty*jasa_telkom)+(qty*material_telkom)";
      }
      // dd($sql);
      // $data = DB::select("SELECT mt.*,
      //   SUM( CASE WHEN m.jenis_order not in(4,12,13,14,15) THEN $sql ELSE 0 END) AS total,
      //   SUM( CASE WHEN m.jenis_order = 1 THEN $sql ELSE 0 END) AS benjar,
      //   SUM( CASE WHEN m.jenis_order = 2 THEN $sql ELSE 0 END) AS gamas,
      //   SUM( CASE WHEN m.jenis_order = 3 THEN $sql ELSE 0 END) AS odp_loss,
      //   SUM( CASE WHEN m.jenis_order = 4 THEN $sql ELSE 0 END) AS remo,
      //   SUM( CASE WHEN m.jenis_order = 5 THEN $sql ELSE 0 END) AS utilitas,
      //   SUM( CASE WHEN m.jenis_order = 6 THEN $sql ELSE 0 END) AS insert_tiang,
      //   SUM( CASE WHEN m.jenis_order = 9 THEN $sql ELSE 0 END) AS odp_full,
      //   SUM( CASE WHEN m.jenis_order = 8 THEN $sql ELSE 0 END) AS odp_sehat,
      //   SUM( CASE WHEN m.jenis_order = 10 THEN $sql ELSE 0 END) AS val_odp,
      //   SUM( CASE WHEN m.jenis_order = 7 THEN $sql ELSE 0 END) AS rev_ftm,
      //   SUM( CASE WHEN m.jenis_order = 11 THEN $sql ELSE 0 END) AS pihak3,
      //   SUM( CASE WHEN m.jenis_order = 17 THEN $sql ELSE 0 END) AS normalisasi,
      //   SUM( CASE WHEN m.jenis_order = 18 THEN $sql ELSE 0 END) AS reboundary,
      //   SUM( CASE WHEN m.jenis_order = 19 THEN $sql ELSE 0 END) AS tutup_odp,
      //   (SELECT COUNT(*) FROM maintaince m
      //   LEFT JOIN regu mr ON m.dispatch_regu_id = mr.id_regu
      //   WHERE tgl_selesai LIKE '$tgl%' AND mr.nama_mitra = mt.mitra AND m.jenis_order = 8) AS tiket_odp_sehat
      //   FROM maintenance_mitra mt
      //   LEFT JOIN regu mr ON mr.nama_mitra = mt.mitra
      //   LEFT JOIN maintaince m ON m.dispatch_regu_id = mr.id_regu
      //   LEFT JOIN maintaince_mtr mm ON mm.maintaince_id = m.id
      //   LEFT JOIN khs_maintenance i ON mm.id_item=i.id_item
      //   WHERE (tgl_selesai LIKE '$tgl%' OR tgl_selesai IS NULL ) AND mt.isAktif=1
      //   GROUP BY mt.id
      //   ORDER BY mt.id ASC");
      $data = DB::select("SELECT mt.*,
        SUM( CASE WHEN m.jenis_order in (1,2,3,4,5,6,8,9,16,17,20,21,22,23) THEN $sql ELSE 0 END) AS total,
        SUM( CASE WHEN m.jenis_order = 1 THEN $sql ELSE 0 END) AS benjar,
        SUM( CASE WHEN m.jenis_order = 2 THEN $sql ELSE 0 END) AS gamas,
        SUM( CASE WHEN m.jenis_order = 3 THEN $sql ELSE 0 END) AS odp_loss,
        SUM( CASE WHEN m.jenis_order = 4 THEN $sql ELSE 0 END) AS remo,
        SUM( CASE WHEN m.jenis_order = 5 THEN $sql ELSE 0 END) AS utilitas,
        SUM( CASE WHEN m.jenis_order = 6 THEN $sql ELSE 0 END) AS insert_tiang,
        SUM( CASE WHEN m.jenis_order = 9 THEN $sql ELSE 0 END) AS odp_full,
        SUM( CASE WHEN m.jenis_order = 8 THEN $sql ELSE 0 END) AS odp_sehat,
        SUM( CASE WHEN m.jenis_order = 16 THEN $sql ELSE 0 END) AS repair,
        SUM( CASE WHEN m.jenis_order = 17 THEN $sql ELSE 0 END) AS normalisasi,
        SUM( CASE WHEN m.jenis_order = 20 THEN $sql ELSE 0 END) AS odc_sehat,
        SUM( CASE WHEN m.jenis_order = 21 THEN $sql ELSE 0 END) AS valins,
        SUM( CASE WHEN m.jenis_order = 22 THEN $sql ELSE 0 END) AS qe_hem,
        SUM( CASE WHEN m.jenis_order = 23 THEN $sql ELSE 0 END) AS wasaka,
        SUM( CASE WHEN m.jenis_order = 28 THEN $sql ELSE 0 END) AS odp_rusak,
        (SELECT COUNT(*) FROM maintaince m
        LEFT JOIN regu mr ON m.dispatch_regu_id = mr.id_regu
        WHERE m.isHapus = 0 AND tgl_selesai LIKE '$tgl%' AND mr.nama_mitra = mt.mitra AND m.jenis_order = 8) AS tiket_odp_sehat
        FROM maintenance_mitra mt
        LEFT JOIN maintaince m ON m.nama_mitra = mt.mitra
        LEFT JOIN maintaince_mtr mm ON mm.maintaince_id = m.id
        LEFT JOIN khs_maintenance i ON mm.id_item=i.id_item
        WHERE m.isHapus = 0 AND (tgl_selesai LIKE '$tgl%' OR tgl_selesai IS NULL ) AND mt.isAktif=1
        GROUP BY mt.id
        ORDER BY mt.id ASC");

      return view('report.revmitrav2', compact('data'));
    }

    public function ajax_rev($mitra,$id,$tgl){
      if($mitra=="MITRA"){
        $sql = "(qty*jasa_ta)+(qty*material_ta)";
      }else{
        $sql = "(qty*jasa_telkom)+(qty*material_telkom)";
      }
       // $data = DB::select("SELECT mt.*, id_regu as id, mt.uraian as nama_regu,
       //  SUM( CASE WHEN m.jenis_order not in(4,12,13,14,15) THEN $sql ELSE 0 END) as total,
       //  SUM( CASE WHEN m.jenis_order = 1 THEN $sql ELSE 0 END) as benjar,
       //  SUM( CASE WHEN m.jenis_order = 2 THEN $sql ELSE 0 END) as gamas,
       //  SUM( CASE WHEN m.jenis_order = 3 THEN $sql ELSE 0 END) as odp_loss,
       //  SUM( CASE WHEN m.jenis_order = 4 THEN $sql ELSE 0 END) as remo,
       //  SUM( CASE WHEN m.jenis_order = 5 THEN $sql ELSE 0 END) as utilitas,
       //  SUM( CASE WHEN m.jenis_order = 6 THEN $sql ELSE 0 END) as insert_tiang,
       //  SUM( CASE WHEN m.jenis_order = 9 THEN $sql ELSE 0 END) as odp_full,
       //  SUM( CASE WHEN m.jenis_order = 8 THEN $sql ELSE 0 END) as odp_sehat,
       //  SUM( CASE WHEN m.jenis_order = 10 THEN $sql ELSE 0 END) as val_odp,
       //  SUM( CASE WHEN m.jenis_order = 7 THEN $sql ELSE 0 END) as rev_ftm,
       //  SUM( CASE WHEN m.jenis_order = 11 THEN $sql ELSE 0 END) as pihak3,
       //  SUM( CASE WHEN m.jenis_order = 17 THEN $sql ELSE 0 END) as normalisasi,
       //  SUM( CASE WHEN m.jenis_order = 18 THEN $sql ELSE 0 END) as reboundary,
       //  SUM( CASE WHEN m.jenis_order = 19 THEN $sql ELSE 0 END) as tutup_odp,
       //  (SELECT COUNT(*) FROM maintaince m
       //  LEFT JOIN regu mr ON m.dispatch_regu_id = mr.id_regu
       //  WHERE tgl_selesai like '$tgl%' AND mr.nama_mitra = mt.mitra and m.jenis_order = 8) as tiket_odp_sehat
       //  FROM regu mt
       //  LEFT JOIN maintaince m ON m.dispatch_regu_id = mt.id_regu
       //  LEFT JOIN maintaince_mtr mm ON mm.maintaince_id = m.id
       //  LEFT JOIN khs_maintenance i ON mm.id_item=i.id_item
       //  WHERE (tgl_selesai like '$tgl%' OR tgl_selesai IS NULL ) AND mt.nama_mitra= '$id' and mt.uraian != '' AND mt.ACTIVE = 1
       //  GROUP BY mt.id_regu
       //   HAVING total > 0
       //  ORDER BY mt.id_regu ASC");
       $data = DB::select("SELECT mt.*, id_regu as id, mt.uraian as nama_regu,
        SUM( CASE WHEN m.jenis_order in (1,2,3,4,5,6,8,9,16,17,20,21,22,23) THEN $sql ELSE 0 END) as total,
        SUM( CASE WHEN m.jenis_order = 1 THEN $sql ELSE 0 END) as benjar,
        SUM( CASE WHEN m.jenis_order = 2 THEN $sql ELSE 0 END) as gamas,
        SUM( CASE WHEN m.jenis_order = 3 THEN $sql ELSE 0 END) as odp_loss,
        SUM( CASE WHEN m.jenis_order = 4 THEN $sql ELSE 0 END) as remo,
        SUM( CASE WHEN m.jenis_order = 5 THEN $sql ELSE 0 END) as utilitas,
        SUM( CASE WHEN m.jenis_order = 6 THEN $sql ELSE 0 END) as insert_tiang,
        SUM( CASE WHEN m.jenis_order = 8 THEN $sql ELSE 0 END) as odp_sehat,
        SUM( CASE WHEN m.jenis_order = 9 THEN $sql ELSE 0 END) as odp_full,
        SUM( CASE WHEN m.jenis_order = 16 THEN $sql ELSE 0 END) as repair,
        SUM( CASE WHEN m.jenis_order = 17 THEN $sql ELSE 0 END) as normalisasi,
        SUM( CASE WHEN m.jenis_order = 20 THEN $sql ELSE 0 END) as odc_sehat,
        SUM( CASE WHEN m.jenis_order = 21 THEN $sql ELSE 0 END) AS valins,
        SUM( CASE WHEN m.jenis_order = 22 THEN $sql ELSE 0 END) AS qe_hem,
        SUM( CASE WHEN m.jenis_order = 23 THEN $sql ELSE 0 END) AS wasaka,
        (SELECT COUNT(*) FROM maintaince m
        LEFT JOIN regu mr ON m.dispatch_regu_id = mr.id_regu
        WHERE m.isHapus = 0 AND tgl_selesai like '$tgl%' AND mr.nama_mitra = mt.mitra and m.jenis_order = 8) as tiket_odp_sehat
        FROM regu mt
        LEFT JOIN maintaince m ON m.dispatch_regu_id = mt.id_regu
        LEFT JOIN maintaince_mtr mm ON mm.maintaince_id = m.id
        LEFT JOIN khs_maintenance i ON mm.id_item=i.id_item
        WHERE m.isHapus = 0 AND (tgl_selesai like '$tgl%' OR tgl_selesai IS NULL ) AND mt.nama_mitra= '$id' and mt.uraian != ''
        GROUP BY mt.id_regu
         HAVING total > 0
        ORDER BY mt.id_regu ASC");

      return view('report.ajaxrev', compact('data', 'tgl'));
    }
    public function ajaxdetil_rev($regu,$order,$tgl){
      $data = DB::select('select *, (qty*jasa_ta) as hargajasa,(qty*material_ta) as hargamaterial,(select count(*) from maintaince_mtr where maintaince_id=m.id) as countmtr FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and m.jenis_order="'.$order.'" and m.dispatch_regu_id="'.$regu.'" order by m.id');
      return view('report.ajaxdetilrev', compact('data'));
    }
    public function qc($id,$date){
      $foto = $this->photoOdpSehat;
      $data = DB::table('maintaince')->where([
        ['jenis_order', $id],
        ['isHapus', 0]
      ])->where('tgl_selesai', 'like', $date.'%')->get();
      return view('report.qc', compact('data', 'foto'));
    }
    public function toExcelFoto($jenis_order,$tgl){
      Excel::create($jenis_order.'-'.$tgl, function($excel) use($tgl, $jenis_order){
        $mt = DB::table('maintaince')->where([
          ['jenis_order', $jenis_order],
          ['isHapus', 0]
        ])->where('tgl_selesai', 'like', '%'.$tgl.'%')->OrderBy('tgl_selesai', 'ASC')->OrderBy('sto', 'ASC')->get();
        $excel->sheet("evidence", function($sheet) use($tgl, $jenis_order, $mt){
          $sheet->loadView('report.excelFoto', compact('mt'));
        });
      })->download('xlsx');
    }
    public function toExcelFotoHtml($jenis_order,$tgl){
        $mt = DB::table('maintaince')->where([
          ['jenis_order', $jenis_order],
          ['isHapus', 0]
        ])->where('tgl_selesai', 'like', '%'.$tgl.'%')->OrderBy('tgl_selesai', 'ASC')->OrderBy('sto', 'ASC')->get();
        // dd($mt);
        return view('report.excelFoto', compact('mt'));
    }

     public function proc_report(){
      Excel::create('mt', function($excel){
        $excel->sheet('tes', function($sheet){
          $head = DB::table('maintenance_procurement as mp')
          ->select("mp.status")
          ->groupBy('status')
          ->get();

        $count = DB::SELECT("SELECT mitra,
          COUNT(status) as stts_cnt,
          status
          FROM maintenance_procurement GROUP BY status, mitra ");

        foreach($count as $key => $val){
          $mitra[$val->status] = 0;
        }

        $head = [];

        foreach($count as $key => $val){
          $head[$key] = $val->mitra;
        }

        foreach(array_unique($head) as $key2 => $val2){
          $data[$key2]['mitra'] = $val2;
            foreach($count as $key => $val){
              $data[$key2]['status'] = $mitra;
            }
        }

        foreach($data as $key => $val){
          foreach($count as $key2 => $val2){
            foreach($val['status'] as $key3 => $val3){
              if($key3 == $val2->status && $val2->mitra == $val['mitra']){
                $data[$key]['status'][$key3] = $val2->stts_cnt;
              }
            }
          }
        }

          $sheet->loadView('report.reportingProc', compact('data', 'mitra'));
        });
      })->download('xlsx');
      return redirect()->back();
    }

    public function prod_mitra(Request $req) {
      $requestan = $req->all();
      $qmitra = $qsektor = $qorder = '';
      $date = date('Y-m');

      $title = 'Table Produktifitas Mitra';

      if (!empty($req->tgl)) {
        if ($req->tgl == 'all') {
          $date = date('Y-m');
        } else {
          $date = $req->tgl;
        }
      }

      $title .= " Periode <u><b>$date</b></u>";

      if (!empty($req->mitra) && $req->mitra != 'Tidak ada Mitra') {
        if ($req->mitra != 'all') {
          $qmitra = " AND r.nama_mitra = '$req->mitra'";
        }
        $title .= " Mitra <u><b>$req->mitra</b></u>";
      }

      if (!empty($req->sektor) && $req->sektor != 'Tidak ada Sektor') {
        if ($req->sektor != 'all') {
          $qsektor = " AND ms.sector = '$req->sektor'";
        }
        $title .= " Sektor <u><b>$req->sektor</b></u>";
      }

      if (!empty($req->order)) {
        if ($req->order != 'all') {
          $qorder = " AND m.nama_order = '$req->order'";
        }
        $title .= " dengan Jenis Order <u><b>$req->order</b></u>";
      }

      $mi = DB::table('maintenance_mitra')
      ->select('mitra as id', 'mitra as text')->where('isAktif', 1)->get();

      // $data = DB::SELECT(
      // "SELECT dispatch_regu_name, dispatch_regu_id as id_reg,
      // (CASE WHEN nama_sector IS NULL THEN 'Tidak ada Sektor' ELSE nama_sector END) As nama_sector,
      // (CASE WHEN r.nama_mitra IS NULL THEN 'Tidak ada Mitra' ELSE r.nama_mitra END) As nama_mitra,
      // label_sektor,
      // label_regu,
      // m.nama_order,
      // SUM(CASE WHEN m.id IS NOT NULL THEN 1 ELSE 0 END) As order_data,
      // SUM(CASE WHEN m.status = 'Up' OR m.status = 'close' or m.status = 'Redaman Sudah Aman' THEN 1 ELSE 0 END) As up,
      // SUM(CASE WHEN m.status IS NULL OR m.status NOT IN ('cancel order', 'Up', 'ogp', 'close', 'Redaman Sudah Aman') AND m.status NOT LIKE 'kendala%' THEN 1 ELSE 0 END) As sisa,
      // SUM(CASE WHEN m.status = 'ogp' THEN 1 ELSE 0 END) As ogp,
      // SUM(CASE WHEN m.status like 'kendala%' OR m.status = 'cancel order' THEN 1 ELSE 0 END) As kendala,
      // 0 as target,
      // 0 as kekurangan
      // FROM maintaince m
      // LEFT JOIN maintenance_jenis_order mjo ON mjo.id = m.jenis_order
      // LEFT JOIN regu r ON dispatch_regu_id = r.id_regu
      // WHERE modified_at LIKE '$date%' AND mjo.aktif = 1 $qmitra $qsektor $qorder
      // GROUP BY dispatch_regu_name
      // ORDER BY modified_at DESC"
      // );

      $data = DB::SELECT(
      "SELECT uraian, dispatch_regu_id as id_reg,
      (CASE WHEN ms.sector IS NULL THEN 'Tidak ada Sektor' ELSE ms.sector END) As nama_sector,
      (CASE WHEN r.nama_mitra IS NULL THEN 'Tidak ada Mitra' ELSE r.nama_mitra END) As nama_mitra,
      label_sektor,
      label_regu,
      m.nama_order,
      SUM(CASE WHEN m.id IS NOT NULL THEN 1 ELSE 0 END) As order_data,
      SUM(CASE WHEN m.status = 'Up' OR m.status = 'close' or m.status = 'Redaman Sudah Aman' THEN 1 ELSE 0 END) As up,
      SUM(CASE WHEN m.status IS NULL OR m.status NOT IN ('cancel order', 'Up', 'ogp', 'close', 'Redaman Sudah Aman') AND m.status NOT LIKE 'kendala%' THEN 1 ELSE 0 END) As sisa,
      SUM(CASE WHEN m.status = 'ogp' THEN 1 ELSE 0 END) As ogp,
      SUM(CASE WHEN m.status like 'kendala%' OR m.status = 'cancel order' THEN 1 ELSE 0 END) As kendala,
      0 as target,
      0 as kekurangan
      FROM regu r
      LEFT JOIN maintaince m ON dispatch_regu_id = r.id_regu
      LEFT JOIN maintenance_sector ms ON ms.label = r.label_sektor
      LEFT JOIN maintenance_jenis_order mjo ON mjo.id = m.jenis_order
      WHERE m.isHapus = 0 AND label_regu != '' AND (created_at LIKE '$date%' OR tgl_selesai LIKE '$date%') AND mjo.aktif = 1 $qmitra $qsektor $qorder
      GROUP BY uraian
      ORDER BY modified_at DESC"
      );

      $mitra_get_q = DB::SELECT(
      "SELECT
      (CASE WHEN r.nama_mitra IS NULL THEN 'Tidak ada Mitra' ELSE r.nama_mitra END) As nama_mitra
      FROM regu r
      LEFT JOIN maintaince m ON dispatch_regu_id = r.id_regu
      WHERE m.isHapus = 0 AND label_regu != '' AND modified_at LIKE '$date%'
      GROUP BY uraian
      ORDER BY modified_at DESC"
      );

      $sektor_get_q = DB::SELECT(
      "SELECT
      (CASE WHEN ms.sector IS NULL THEN 'Tidak ada Sektor' ELSE ms.sector END) As nama_sector
      FROM regu r
      LEFT JOIN maintaince m ON dispatch_regu_id = r.id_regu
      LEFT JOIN maintenance_sector ms ON ms.label = r.label_sektor
      WHERE m.isHapus = 0 AND label_regu != '' AND modified_at LIKE '$date%' $qmitra
      GROUP BY uraian
      ORDER BY modified_at DESC"
      );

      $jo_get_q = DB::SELECT(
      "SELECT
      m.nama_order
      FROM regu r
      LEFT JOIN maintaince m ON dispatch_regu_id = r.id_regu
      LEFT JOIN maintenance_sector ms ON ms.label = r.label_sektor
      WHERE m.isHapus = 0 AND label_regu != '' AND modified_at LIKE '$date%' $qmitra $qsektor
      GROUP BY uraian
      ORDER BY modified_at DESC"
      );

      $title .= " Sebanyak <b>".count($data)."</b> Buah";

      $mitra = $sektor = $orderjenis = [];

      foreach($mitra_get_q as $keyp => $dta_m) {
        $mitra[$keyp]['id'] = $dta_m->nama_mitra;
        $mitra[$keyp]['text'] = $dta_m->nama_mitra;
      }

      foreach($sektor_get_q as $key => $d) {
        $sektor[$key]['id'] = $d->nama_sector;
        $sektor[$key]['text'] = $d->nama_sector;
      }

      foreach($jo_get_q as $key => $d) {
        $orderjenis[$key]['id'] = $d->nama_order;
        $orderjenis[$key]['text'] = $d->nama_order;
      }

      $mitra = array_unique($mitra, SORT_REGULAR);
      $mitra = json_decode(json_encode(array_values($mitra)));

      $sektor = array_unique($sektor, SORT_REGULAR);
      $sektor = json_decode(json_encode(array_values($sektor)));

      $orderjenis = array_unique($orderjenis, SORT_REGULAR);
      $orderjenis = json_decode(json_encode(array_values($orderjenis)));
      // dd($mitra, $sektor);
      return view('report.mitra_prod', compact('data', 'mitra', 'orderjenis', 'sektor', 'title', 'requestan'));
    }

    public function prod_mitra_searchlist(Request $req) {
      $qmitra = $qsektor = $qorder = '';
      $date = date('Y-m');

      if (!empty($req->tgl)) {
        if ($req->tgl == 'all') {
          $date = date('Y-m');
        } else {
          $date = $req->tgl;
        }
      }

      if (!empty($req->mitra)) {
        if ($req->mitra != 'all') {
          $qmitra = " AND r.nama_mitra = '$req->mitra'";
        }
      }

      if (!empty($req->sektor)) {
        if ($req->sektor != 'all') {
          $qsektor = " AND ms.sector = '$req->sektor'";
        }
      }

      if (!empty($req->order)) {
        if ($req->order != 'all') {
          $qorder = " AND nama_order = '$req->order'";
        }
      }

      $data = DB::SELECT(
      "SELECT nama_order,
      (CASE WHEN ms.sector IS NULL THEN 'Tidak ada Sektor' ELSE ms.sector END) As nama_sector,
      (CASE WHEN r.nama_mitra IS NULL THEN 'Tidak ada Mitra' ELSE r.nama_mitra END) As nama_mitra
      FROM regu r
      LEFT JOIN maintaince m ON dispatch_regu_id = r.id_regu
      LEFT JOIN maintenance_sector ms ON ms.label = r.label_sektor
      WHERE m.isHapus = 0 AND label_regu != '' AND modified_at LIKE '$date%' $qmitra $qsektor $qorder
      GROUP BY uraian
      ORDER BY modified_at DESC"
      );

      $mitra = $sektor = $orderjenis = [];


      foreach($data as $key => $d) {
        $mitra[$key]['id'] = $d->nama_mitra;
        $mitra[$key]['text'] = $d->nama_mitra;

        $sektor[$key]['id'] = $d->nama_sector;
        $sektor[$key]['text'] = $d->nama_sector;

        $orderjenis[$key]['id'] = $d->nama_order;
        $orderjenis[$key]['text'] = $d->nama_order;
      }

      $mitra = array_unique($mitra, SORT_REGULAR);
      $res_dt['mitra'] = json_decode(json_encode(array_values($mitra)));

      $sektor = array_unique($sektor, SORT_REGULAR);
      $res_dt['sektor'] = json_decode(json_encode(array_values($sektor)));

      $orderjenis = array_unique($orderjenis, SORT_REGULAR);
      $res_dt['orderjenis'] = json_decode(json_encode(array_values($orderjenis)));
      return \Response::json($res_dt);
    }

    public function prod_mitra_list($jenis, $reg_nm, $tgl, $mitra, $sektor, $order) {
      $qmitra = $qsektor = $qorder = '';
      $date = date('Y-m');

      if ($tgl != 'all') {
        $date = $tgl;
      }

      if ($mitra != 'all') {
        $qmitra = " AND r.nama_mitra = '$mitra'";
      }

      if ($sektor != 'all') {
        $qsektor = " AND ms.sector = '$sektor'";
      }

      if ($order != 'all') {
        $qorder = " AND m.nama_order = '$order'";
      }

      switch ($jenis) {
      case 'order_data':
        $qjenis = " AND m.id IS NOT NULL";
        break;
      case 'up':
        $qjenis = " AND (m.status = 'Up' OR m.status = 'close' OR m.status = 'Redaman Sudah Aman')";
        break;
      case 'sisa':
        $qjenis = " AND (m.status IS NULL OR m.status NOT IN ('cancel order', 'Up', 'ogp', 'close', 'Redaman Sudah Aman') AND m.status NOT LIKE 'kendala%')";
        break;
      case 'ogp':
        $qjenis = " AND m.status = 'ogp'";
        break;
      case 'kendala':
        $qjenis = " AND( m.status like 'kendala%' or m.status = 'cancel order')";
        break;
      default:
        $qjenis = '';
      }

      $data = DB::SELECT(
      "SELECT m.*,
      (SELECT u_nama As nama FROM user WHERE id_karyawan = m.username1 GROUP BY id_karyawan) AS nama1,
      (SELECT u_nama As nama FROM user WHERE id_karyawan = m.username2 GROUP BY id_karyawan) AS nama2,
      (SELECT count(id) FROM maintaince WHERE refer = m.id) AS jml_anak,
      (select id_regu FROM regu JOIN maintaince mt2 ON id_regu = mt2.dispatch_regu_id WHERE nik1 = '".session('auth')->id_user."' OR nik2 = '".session('auth')->id_user."') As check_regu
      FROM regu r
      LEFT JOIN maintaince m ON dispatch_regu_id = r.id_regu
      LEFT JOIN maintenance_sector ms ON ms.label = r.label_sektor
      LEFT JOIN maintenance_jenis_order mjo ON mjo.id = m.jenis_order
      WHERE isHapus = 0 AND label_regu != '' AND modified_at LIKE '$date%' AND mjo.aktif = 1 AND uraian = '$reg_nm' $qjenis $qmitra $qsektor $qorder
      ORDER BY modified_at DESC"
      );
      // dd($data);
      return view('report.list', compact('data'));
    }
  public function prod_sector($tgl=null)
  {
    if(!$tgl){
      $tgl=date("Y-m-d");
    }
    $data = DB::select("SELECT `sector_id`,nama_sector, count(if(dispatch_regu_id is null,1,null)) as blm_dispatch,count(if(status is null,1,null)) as inbox_teknisi,count(if(status='ogp',1,null)) as ogp,count(if((status='kendala pelanggan' and tgl_selesai like '".$tgl."%'),1,null)) as kendala_pelanggan,count(if((status='kendala teknis' and tgl_selesai like '".$tgl."%') ,1,null)) as kendala_teknis,count(if((status='close' and tgl_selesai like '".$tgl."%'),1,null)) as close FROM `maintaince` where isHapus = 0 GROUP BY `sector_id` ORDER BY `sector_id`");
    // dd($data);
    return view('produktifitas.prod_by_sector', compact('data'));
  }
  public function prod_sektor_detil($tgl)
    {
      $auth = session('auth');
      $query = "";
      $list = array();
      $data[] = ['id' => 'all', 'text' => 'all'];
      $ls = DB::select("SELECT * from maintaince where isHapus = 0 AND ((status = 'kendala teknis' and modified_at like '%".$tgl."%')
        or (status = 'kendala pelanggan' and modified_at like '%".$tgl."%')
        or (status = 'close' and tgl_selesai like '%".$tgl."%')
        or (status is null or status='ogp')) and refer is null ".$query."
        order by dispatch_regu_id asc, created_at asc");
      foreach($ls as $l){
        // if($lastRegu == $l->dispatch_regu_id){
        //   $list[$l->dispatch_regu_id][] = array("tiket"=>$l->no_tiket, 'odp_nama' => $l->nama_odp, "status_laporan"=>$l->status, "created_at" => $l->created_at, "id" => $l->id);
        // }else{
        $list[$l->dispatch_regu_id][] = array("tiket"=>$l->no_tiket, 'odp_nama' => $l->nama_odp, "status_laporan"=>$l->status, "created_at" => $l->created_at, "id" => $l->id, "tgl_dispatch" => $l->tgl_dispatch, "tgl_selesai" => $l->tgl_selesai);
        // }
        // $lastRegu = $l->dispatch_regu_id;
      }
      $regu = array();
      $mitra = "";

      if(!in_array($auth->nama_instansi, ["TELKOM AKSES", "TELKOM"]))
        $mitra = " mr.nama_mitra='".$auth->nama_instansi."' and ";
      foreach(DB::table('maintenance_sector')->orderBy('id', 'asc')->get() as $mr){
        $data = DB::select("SELECT *,
          (select u_mt_absen As mt_absen from user where id_karyawan = mr.nik1 LIMIT 0, 1) as absen1,
          (select u_mt_verif_absen As mt_verif_absen from user where id_karyawan = mr.nik1 LIMIT 0, 1) as verif1,
          (select u_nama As nama from user where id_karyawan = mr.nik1 LIMIT 0, 1) as nama1,
          (select u_mt_absen As mt_absen from user where id_karyawan = mr.nik2 LIMIT 0, 1) as absen2,
          (select u_mt_verif_absen As mt_verif_absen from user where id_karyawan = mr.nik2 LIMIT 0, 1) as verif2,
          (select u_nama As nama from user where id_karyawan = mr.nik2 LIMIT 0, 1) as nama2,
          (select count(*) from maintaince where isHapus = 0 AND dispatch_regu_id = mr.id_regu and refer is null ".$query.") as jumlah,
          (select count(*) from maintaince where isHapus = 0 AND dispatch_regu_id = mr.id_regu and status is null and refer is null ".$query.") as no_update,
          (select count(*) from maintaince where isHapus = 0 AND dispatch_regu_id = mr.id_regu and status = 'kendala teknis' and modified_at like '%".$tgl."%' and refer is null ".$query.") as kendala_teknis,
          (select count(*) from maintaince where isHapus = 0 AND dispatch_regu_id = mr.id_regu and status = 'kendala pelanggan' and modified_at like '%".$tgl."%' and refer is null ".$query.") as kendala_pelanggan,
          (select count(*) from maintaince where isHapus = 0 AND dispatch_regu_id = mr.id_regu and status = 'close' and tgl_selesai like '%".$tgl."%' and refer is null ".$query.") as close,
          (select count(*) from maintaince where isHapus = 0 AND dispatch_regu_id = mr.id_regu and status = 'close' and tgl_selesai like '%".substr($tgl, 0,7)."%' and refer is null ".$query.") as close_bln
          from regu mr left join maintenance_sector ms on mr.label_sektor = ms.label where ".$mitra." mr.ACTIVE = 1 and ms.id = '".$mr->id."' order by ms.id asc, id_regu asc");
          $regu[$mr->id]['data'] = $data;
          $regu[$mr->id]['grup'] = $mr->label;

      }
      //dd($regu);
      return view('produktifitas.prood_by_sector_detil', compact('regu', 'list'));
    }

  public function remaining_saldo($date)
  {
    dd('tes');
  }
  public function saber_list($date){
      // $sql = "(qty*jasa_ta) + (qty*material_ta)";

      // $data_marina = DB::select("SELECT mt.*,
      // SUM( CASE WHEN m.status = 'close' AND m.tgl_selesai LIKE '$date%' THEN $sql ELSE 0 END) AS rev,
      // COUNT(if(m.status = 'close' AND m.tgl_selesai LIKE '$date%', 1, NULL) ) AS close,
      // COUNT(if(m.status LIKE 'kendala%' AND m.tgl_selesai LIKE '$date%', 1, NULL) ) AS kendala,
      // COUNT(if(status IS NULL, 1, NULL) ) AS inbox_teknisi
      // FROM regu mt
      // LEFT JOIN maintaince m ON m.dispatch_regu_id = mt.id_regu
      // LEFT JOIN maintaince_mtr mm ON mm.maintaince_id = m.id
      // LEFT JOIN khs_maintenance i ON mm.id_item=i.id_item
      // WHERE m.isHapus = 0 AND m.dispatch_regu_id IS NOT NULL AND m.dispatch_regu_id != 0 AND mt.spesialis = 'PROV' AND mt.active = 1
      // GROUP BY mt.id_regu
      // HAVING rev > 0
      // ORDER BY mt.id_regu ASC");

      $data_marina_rw = DB::select("SELECT mt.*, mm.qty, mm.id_item, m.status, m.tgl_selesai
      FROM regu mt
      LEFT JOIN maintaince m ON m.dispatch_regu_id = mt.id_regu
      LEFT JOIN maintaince_mtr mm ON mm.maintaince_id = m.id
      WHERE m.isHapus = 0 AND m.dispatch_regu_id IS NOT NULL AND m.dispatch_regu_id != 0 AND mt.spesialis = 'PROV' AND mt.active = 1 AND (m.tgl_selesai LIKE '$date%' OR m.status IS NULL)
      ORDER BY mt.id_regu ASC");

      $get_all_khs = DB::Table('khs_maintenance')->get();
      $data_khs = $data_marina = [];

      foreach($get_all_khs as $k => $v)
      {
        $data_khs[$k]['id_item']     = $v->id_item;
        $data_khs[$k]['material_ta'] = $v->material_ta;
        $data_khs[$k]['jasa_ta']     = $v->jasa_ta;
      }

      foreach($data_marina_rw as $k => $v)
      {
        $find_k_khs = array_search($v->id_item, array_column($data_khs, 'id_item') );

        if(!isset($data_marina[$v->id_regu]) )
        {
          $data_marina[$v->id_regu]                = $v;
          $data_marina[$v->id_regu]->rev           = 0;
          $data_marina[$v->id_regu]->close         = 0;
          $data_marina[$v->id_regu]->kendala       = 0;
          $data_marina[$v->id_regu]->inbox_teknisi = 0;
        }

        if($v->status == null)
        {
          $data_marina[$v->id_regu]->inbox_teknisi += 1;
        }

        if(strpos($v->status, 'kendala') !== false)
        {
          $data_marina[$v->id_regu]->kendala += 1;
        }

        if($find_k_khs !== FALSE)
        {
          $d_khs = $data_khs[$find_k_khs];

          if($v->status == 'close')
          {
            $data_marina[$v->id_regu]->rev   += ($v->qty * $d_khs['material_ta']) + ($v->qty * $d_khs['jasa_ta']);
            $data_marina[$v->id_regu]->close += 1;
          }
        }
      }

      foreach($data_marina as $k => &$v)
      {
        if($v->rev == 0)
        {
          unset($data_marina[$k]);
        }
      }

      $data_psb = DB::SELECT("SELECT
      rr.uraian As tim,
      COUNT(*) As jml_order,
      SUM(CASE WHEN pls.grup = 'UP' THEN 1 ELSE 0 END) As order_up,
      SUM(CASE WHEN pls.grup IN ('KT', 'KP') THEN 1 ELSE 0 END) As order_kendala,
      SUM(CASE WHEN pls.grup IN ('OGP', 'SISA', 'NP', 'HR') THEN 1 ELSE 0 END) As order_sisa
      FROM dispatch_teknisi dt
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN regu rr ON r.spesialis_id_regu = rr.id_regu
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
      pls.grup IS NOT NULL AND
      dt.jenis_order IN ('SC', 'MYIR') AND
      (DATE(dt.tgl) LIKE '$date%') AND
      r.is_saber = 1
      GROUP BY rr.uraian");

      $data_pt2 = DB::SELECT("SELECT pm.regu_name,
      COUNT(*) As jml_order,
      SUM(CASE WHEN lt_status = 'Selesai' THEN 1 ELSE 0 END) As order_up,
      SUM(CASE WHEN lt_status = 'Kendala' THEN 1 ELSE 0 END) As order_kendala,
      SUM(CASE WHEN lt_status NOT IN ('Selesai', 'Kendala') OR lt_status IS NULL THEN 1 ELSE 0 END) As order_sisa
      FROM pt2_master pm
      LEFT JOIN regu r ON pm.regu_id = r.id_regu
      LEFT JOIN pt2_material ptm ON ptm.pt2_id = pm.id
      LEFT JOIN pt2_khs pk On pk.id_item = ptm.id_item
      -- LEFT JOIN procurement_designator pd ON pd.id = pk.id_design_proc
      WHERE pm.tgl_selesai LIKE '$date%' AND pm.delete_clm = 0 AND r.ACTIVE = 1 AND r.is_saber = 1
      GROUP BY pm.regu_name");

      $data_psb = json_decode(json_encode($data_psb), TRUE);
      $data_pt2 = json_decode(json_encode($data_pt2), TRUE);
      $data = [];

      foreach($data_marina as $k => $v)
      {
        if(!isset($data[$v->uraian]) )
        {
          $data[$v->uraian] = new \Stdclass();

          $data[$v->uraian]->uraian            = $v->uraian;
          $data[$v->uraian]->close_M           = $v->close;
          $data[$v->uraian]->kendala_M         = $v->kendala;
          $data[$v->uraian]->inbox_teknisi_M   = $v->inbox_teknisi;
          $data[$v->uraian]->rev_M             = $v->rev;
        }

        $find_k_psb = array_search($v->uraian, array_column($data_psb, 'tim') );

        if($find_k_psb !== FALSE)
        {
          $dm = $data_psb[$find_k_psb];

          $data[$v->uraian]->close_PSB         = $dm['order_up'];
          $data[$v->uraian]->kendala_PSB       = $dm['order_kendala'];
          $data[$v->uraian]->inbox_teknisi_PSB = $dm['order_sisa'];

          unset($data_psb[$find_k_psb]);
          $data_psb = array_values($data_psb);
        }

        $find_k_pt2 = array_search($v->uraian, array_column($data_pt2, 'regu_name') );

        if($find_k_pt2 !== FALSE)
        {
          $dm = $data_pt2[$find_k_pt2];

          $data[$v->uraian]->close_pt2         = $dm['order_up'];
          $data[$v->uraian]->kendala_pt2       = $dm['order_kendala'];
          $data[$v->uraian]->inbox_teknisi_pt2 = $dm['order_sisa'];

          unset($data_psb[$find_k_pt2]);
          $data_pt2 = array_values($data_pt2);
        }
      }

      foreach($data_psb as $v)
      {
        if(!isset($data[$v['tim'] ]) )
        {
          $data[$v['tim'] ] = new \Stdclass();

          $data[$v['tim'] ]->uraian            = $v['tim'];
          $data[$v['tim'] ]->close_PSB         = $v['order_up'];
          $data[$v['tim'] ]->kendala_PSB       = $v['order_kendala'];
          $data[$v['tim'] ]->inbox_teknisi_PSB = $v['order_sisa'];
        }
      }

      foreach($data_pt2 as $v)
      {
        if(!isset($data[$v['regu_name'] ]) )
        {
          $data[$v['regu_name'] ] = new \Stdclass();

          $data[$v['regu_name'] ]->uraian            = $v['regu_name'];
          $data[$v['regu_name'] ]->close_pt2         = $v['order_up'];
          $data[$v['regu_name'] ]->kendala_pt2       = $v['order_kendala'];
          $data[$v['regu_name'] ]->inbox_teknisi_pt2 = $v['order_sisa'];
        }
      }

      $data = array_values($data);
      // dd($data);
      // $data = DB::SELECT("
      //   SELECT dispatch_regu_id, r.uraian,
      //   count(if(status is null,1,null)) as inbox_teknisi,
      //   count(if((status like 'kendala%' and tgl_selesai like '".$date."%'),1,null)) as kendala,
      //     count(if((status='close' and tgl_selesai like '".$date."%'),1,null)) as close,
      //     count(if((status='close' and tgl_selesai like '".$date."%'),1,null)) as sisa
      //     FROM maintaince m left join regu r on m.dispatch_regu_id=r.id_regu where m.isHapus = 0 and m.dispatch_regu_id is NOT null and m.dispatch_regu_id!=0 and r.spesialis='PROV' and r.active = 1 GROUP BY m.dispatch_regu_id ORDER BY m.dispatch_regu_id
      //     ");
      return view('produktifitas.prod_saber', compact('data'));
    }
}
