<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DA\Khs;
use Validator;
use DB;
class KhsController extends Controller
{
    public function setting($id)
    {
      $itemModif = Khs::getByProgram($id);
      $raw_item = Khs::getItemAll();
      $data = DB::table('maintenance_jenis_order')->where('id', $id)->first();
      // dd($itemModif);
      foreach($raw_item as $k => $v)
      {
        $item[$k]['id'] = preg_replace('/[[:^print:]]/', '', $v->id);
        $item[$k]['text'] = preg_replace('/[[:^print:]]/', '', $v->text);
        $item[$k]['uraian'] = preg_replace('/[[:^print:]]/', '', $v->uraian);
      }
      $evident = DB::table('maintenance_evident')->where('program_id', $id)->get();
      $jenis_order = DB::table('maintenance_jenis_order')->where('id', $id)->first();
      // dd($itemModif);
      // dd($bisa_json, $kd_bisa_json, json_encode($kd_bisa_json));
      return view('khs.setting', compact('item', 'itemModif', 'data', 'evident', 'jenis_order'));
    }
    public function grand_setting()
    {
        $data = DB::table('maintenance_jenis_order')->where('aktif', 1)->get();
        return view('setting.grand_setting', compact('data'));
    }

    public function save(Request $req)
    {
      $data = DB::table('maintenance_mtr_program')->where('id_item', $req->id_item)->where('program_id', $req->hid)->first();
      if(!count($data)){
        DB::table('maintenance_mtr_program')->insert([
          'id_item' => $req->id_item,
          'program_id' => $req->hid
          ]);
      }
      return redirect()->back();
    }
    public function index()
    {
      $data = DB::table('khs_maintenance')->select('khs_maintenance.*', 'jenis_khs.jenis_khs')->leftJoin('jenis_khs', 'khs_maintenance.jenis', '=', 'jenis_khs.id')->get();
      return view('khs.list', compact('data'));
    }
    public function input($id)
    {
      $data = DB::table('khs_maintenance')->where('id', $id)->first();
      $jeniskhs = DB::select('select id, jenis_khs as text from jenis_khs where 1');
      return view('khs.input', compact('data', 'jeniskhs'));
    }
    public function saveKhs(Request $req)
    {
      $data = DB::table('khs_maintenance')->where('id', $req->hid)->first();

      $rules = [
        'id_item' => 'required',
        'uraian' => 'required'
      ];

      $messages = [
        'id_item.required' => 'Silahkan isi kolom "id_item" Bang!',
        'uraian.required' => 'Silahkan isi kolom "uraian" Bang!',
      ];

      $validator = Validator::make($req->all(), $rules, $messages);

      if ($validator->fails()) {
        return redirect()->back()
            ->withInput($req->all())
            ->withErrors($validator)
            ->with('alerts', [
              ['type' => 'danger', 'text' => '<strong>GAGAL</strong> isian kurang lengkap']
            ]);
      }

      if(!count($data)){
        DB::table('khs_maintenance')->insert([
          'id_item' => $req->id_item,
          'uraian' => $req->uraian,
          'satuan' => $req->satuan,
          'material_telkom' => $req->material_telkom?:0,
          'jasa_telkom' => $req->jasa_telkom?:0,
          'material_ta' => $req->material_ta?:0,
          'jasa_ta' => $req->jasa_ta?:0,
          'jenis' => $req->jenis,
          'alista_designator' => $req->alista_designator
          ]);
      }else{
        DB::table('khs_maintenance')
        ->where('id', $req->hid)
        ->update([
          'id_item' => $req->id_item,
          'uraian' => $req->uraian,
          'satuan' => $req->satuan,
          'material_telkom' => $req->material_telkom?:0,
          'jasa_telkom' => $req->jasa_telkom?:0,
          'material_ta' => $req->material_ta?:0,
          'jasa_ta' => $req->jasa_ta?:0,
          'jenis' => $req->jenis,
          'alista_designator' => $req->alista_designator
          ]);
      }
      return redirect('/khs')->with('alerts', [
          ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
        ]);
    }
    public function delete_khs(Request $req){
      Khs::delete_data($req->id);
    }
    public function evidentSave($id,Request $req)
    {
      $evident = str_replace(' ', '-', $req->evident);
      $query = ["evident"=>$evident, "hukum"=>$req->hukum, "program_id"=>$id];
      $exists = DB::table('maintenance_evident')->where('evident', $evident)->where('program_id', $id)->first();
      if($exists){
        DB::table('maintenance_evident')->where('id', $exists->id)->update($query);
      }else{
        DB::table('maintenance_evident')->insert($query);
      }
      return redirect('/setting/'.$id)->with('alerts', [
          ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
        ]);
    }
    public function evidentDel(Request $req)
    {
      DB::table('maintenance_evident')->where('id', $req->id)->delete();
    }


    public function jenisOrderSave($id, Request $req)
    {
      $exists = DB::table('maintenance_jenis_order')->where('id', $req->id)->first();
      $query = ["nama_order"=>$req->nama_order, "aktif"=>$req->aktif];
      if($exists){
        DB::table('maintenance_jenis_order')->where('id', $req->id)->update($query);
      }else{
        $id = DB::table('maintenance_jenis_order')->insertGetId($query);
      }
      return redirect('/setting/'.$id)->with('alerts', [
          ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
        ]);
      // DB::table('maintenance_jenis_order')->where('id', $req->id)->delete();
      // dd($id,$req->nama_order,$req->aktif);
    }

    public function setRegisterOrderField($id,Request $req){
      // dd($req->field_register_order);
      DB::table('maintenance_jenis_order')->where('id', $id)->update(['field_register_order'=>$req->field_register_order]);
      return redirect()->back();
    }
    public function setLaporanOrderField($id,Request $req){
      // dd($req->field_laporan_order);
      DB::table('maintenance_jenis_order')->where('id', $id)->update(['field_laporan_order'=>$req->field_laporan_order]);
      return redirect()->back();
    }
}