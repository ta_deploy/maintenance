<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\DA\Nossa;
use DB;
class NossaController extends Controller
{
    public function index()
    {
        $data = Nossa::getMaintenance();
        return view('order.listNossa', compact('data'));
    }
    public function registerTiketNossa($id)
    {
        $data = Nossa::getByNotik($id);
        //$data = null;
        $regu = DB::table('maintenance_regu')->select('*', 'nama_regu as text')->get();
        $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();
        $jenis_order = DB::table('maintenance_jenis_order')->get();
        return view('order.registerNossa', compact('regu','sto','jenis_order', 'data'));
    }
}
