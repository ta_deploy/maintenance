<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\DA\Procurement;
class ProcurementController extends Controller
{
  protected $file = [
    'nde', 'justifikasi', 'boq_real'
  ];
  protected $filePembuatanSp = [
    'kesanggupan'
  ];
  protected $filePemberkasan = [
    'baut', 'ba_rekon', 'boq_rekon', 'rekapitulasi_rekon', 'rekon_material', 'bast_abd', 'bast_pekerjaan'
  ];


  public function adminList(){
    $data = Procurement::getByStatus("Inbox Admin");
    return view('procurement.admin.list', compact('data'));
  }
  public function input($id){
    $data = Procurement::getById($id);
    $mitra = Procurement::get_mitra();
    return view('procurement.admin.input', compact('data', 'mitra'));
  }
  public function adminSave($id, Request $req){
    $data = Procurement::getById($id);
    if($data){
      Procurement::adminUpdate($id, $req);
    }else{
      $id = Procurement::adminInput($req);
    }
    $query_file = $this->handleFileUpload($req,$id,$this->file,public_path().'/upload/proc/admin/'.$id.'/');
    if($query_file){
      Procurement::updateFiles($id, $query_file);
    }
    return redirect('/proc/admin/list');
  }


  public function pembuatanSPList(){
    $data = Procurement::getByStatus("Inbox Pembuatan SP");
    return view('procurement.pembuatanspkontrak.list', compact('data'));
  }
  public function pembuatanSP($id){
    $data = Procurement::getById($id);
    return view('procurement.pembuatanspkontrak.input', compact('data'));
  }
  public function pembuatanSPSave($id, Request $req){
    Procurement::pembuatanSPUpdate($id, $req);
    $query_file = $this->handleFileUpload($req,$id,$this->filePembuatanSp,public_path().'/upload/proc/pembuatansp/'.$id.'/');
    if($query_file){
      Procurement::updateFiles($id, $query_file);
    }
    return redirect('/proc/pembuatansp/list');
  }


  public function pemberkasanList(){
    $data = Procurement::getByStatus("Inbox Pemberkasan");
    return view('procurement.pemberkasan.list', compact('data'));
  }
  public function pemberkasan($id){
    $data = Procurement::getById($id);
    return view('procurement.pemberkasan.input', compact('data'));
  }
  public function pemberkasanSave($id, Request $req){
    Procurement::pemberkasanUpdate($id, $req);
    $query_file = $this->handleFileUpload($req,$id,$this->filePemberkasan,public_path().'/upload/proc/pemberkasan/'.$id.'/');
    if($query_file){
      Procurement::updateFiles($id, $query_file);
    }
    return redirect('/proc/pemberkasan/list');
  }


  public function verifikasiList(){
    $data = Procurement::getByStatus("Inbox Verifikasi");
    return view('procurement.verifikasiproc.list', compact('data'));
  }
  public function verifikasi($id){
    $data = Procurement::getById($id);
    return view('procurement.verifikasiproc.input', compact('data'));
  }
  public function verifikasiSave($id, Request $req){
    Procurement::verifikasiUpdate($id, $req);
    return redirect('/proc/verifikasi/list');
  }


  // pengirimandoc
  public function pengirimandocList(){
    $data = Procurement::getByStatus("Inbox Pengiriman Doc");
    return view('procurement.pengirimandoc.list', compact('data'));
  }
  public function pengirimandoc($id){
    $data = Procurement::getById($id);
    return view('procurement.pengirimandoc.input', compact('data'));
  }
  public function pengirimandocSave($id, Request $req){
    Procurement::pengirimandocUpdate($id, $req);
    return redirect('/proc/pengirimandoc/list');
  }


  // pengirimandoc
  public function financeList(){
    $data = Procurement::getByStatus("Inbox Finance");
    return view('procurement.finance.list', compact('data'));
  }
  public function finance($id){
    $data = Procurement::getById($id);
    return view('procurement.finance.input', compact('data'));
  }
  public function financeSave($id, Request $req){
    Procurement::financeUpdate($id, $req);
    return redirect('/proc/finance/list');
  }

  public function excel_download($id){
    $data = Procurement::getById($id);
    $objPHPExcel =   \PHPExcel_IOFactory::load(public_path('marina_excel.xlsx'));
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('C4', "No")
    ->setCellValue('C5', $data->npwp)
    ->setCellValue('C6', $data->program)
    ->setCellValue('C7', "Phone")
    ->setCellValue('C9', $data->nilai)
    ->setCellValue('C14', $data->no_sp)
    ->setCellValue('C17', $data->nilai)
    ->setCellValue('C18', $data->nilai)
    ->setCellValue('C19', $data->nilai);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="contoh01.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    unset($objPHPExcel);
  }

  private function handleFileUpload($request, $id, $file, $path){
    $nama = array();
    foreach($file as $name) {
      $input = $name;
      // dd($input);
      if ($request->hasFile($input)) {
        // dd("ada");
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true))
            return 'Gagal menyiapkan folder';
        }
        $file = $request->file($input);
        //TODO: path, move, resize
        try {
          $ext = $file->clientExtension();
          $moved = $file->move("$path", "$name.$ext");
          $nama[$name] = "$name.$ext";
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'Gagal upload';
        }
      }
      // dd("takde");
    }
    return $nama;
  }

  public function ajax_hist(Request $req){
    $data = Procurement::get_ajax_hist($req->id);
    return \Response::json($data);
  }
}
