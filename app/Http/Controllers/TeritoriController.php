<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\DA\Teritori;
use App\DA\ODC;
class TeritoriController extends Controller
{
    public function index(){
      $data = Teritori::getAll();
      return view('setting.teritoriList', compact('data'));
    }
    public function form($id){
      $data = Teritori::getById($id);
      return view('setting.teritoriForm', compact('data'));
    }
    public function save($id, Request $req){
      $exists = Teritori::getById($id);
      $query = [
        'sector'=>$req->sector,
        'label'=>$req->label,
        'isAktif'=>$req->isAktif
      ];
      if($exists){
        Teritori::update($id, $query);
      }else{
        Teritori::insert($query);
      }
      return redirect('/teritori');
    }


    public function mapping_odc(){
      $data = ODC::getAll();
      return view('setting.odcList', compact('data'));
    }
    public function mapping_odc_form($id){
      $data = ODC::getById($id);
      
      $sector = Teritori::getAllSelect2();
      return view('setting.odcForm', compact('data', 'sector'));
    }
    public function mapping_odc_save($id, Request $req){
      $exists = ODC::getById($id);
      $query = [
        'sector_id'=>$req->sector_id,
        'alpro'=>$req->alpro,
        'isAktif'=>$req->isAktif
      ];
      if($exists){
        ODC::update($id, $query);
      }else{
        ODC::insert($query);
      }
      return redirect('/mapping_odc');
    }
}
