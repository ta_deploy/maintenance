<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use DB;
use Telegram;
use App\DA\User;
use Validator;

class DashboardController extends Controller
{

  public function revenue($jenis,$tgl){
    if($jenis == "MITRA"){
      $data = DB::select('
        SELECT *,
              (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify is null and m.jenis_order!=4) as tl,
              (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify =1 and m.jenis_order!=4) as sm,
              (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify =2 and m.jenis_order!=4) as telkom,
              (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify =3 and m.jenis_order!=4) as siap_rekon,
              (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.jenis_order!=4) as total,
              (SELECT count(*) FROM maintaince m
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.jenis_order = 6 and m.status = "close" and action_cause!="CANCEL ORDER") as tanam_tiang
              FROM maintenance_mitra mt
              WHERE 1
      ');
    }else{
      $data = DB::select('
        SELECT *,
              (SELECT sum((qty*jasa_telkom)+(qty*material_telkom)) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify is null) as tl,
              (SELECT sum(qty*material_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify is null) as tl_mtr,
              (SELECT sum(qty*jasa_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify is null and m.jenis_order!=4) as tl_jasa,

              (SELECT sum((qty*jasa_telkom)+(qty*material_telkom)) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify =1) as sm,
              (SELECT sum(qty*material_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify =1) as sm_mtr,
              (SELECT sum(qty*jasa_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify =1 and m.jenis_order!=4) as sm_jasa,

              (SELECT sum((qty*jasa_telkom)+(qty*material_telkom)) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify =2) as telkom,
              (SELECT sum(qty*material_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify =2) as telkom_mtr,
              (SELECT sum(qty*jasa_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify =2 and m.jenis_order!=4) as telkom_jasa,

              (SELECT sum((qty*jasa_telkom)+(qty*material_telkom)) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify =3) as siap_rekon,
              (SELECT sum(qty*material_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify =3) as siap_rekon_mtr,
              (SELECT sum(qty*jasa_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra and m.verify =3 and m.jenis_order!=4) as siap_rekon_jasa,

              (SELECT sum((qty*jasa_telkom)+(qty*material_telkom)) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra) as total,
              (SELECT sum(qty*material_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra = mt.mitra) as total_mtr,
              (SELECT sum(qty*jasa_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mr.nama_mitra_rekon = mt.mitra and m.jenis_order!=4) as total_jasa
              FROM maintenance_mitra mt
              WHERE 1
      ');
    }
    return view('dashboard.revenue', compact('data'));
  }
  public function revenuePerProgram($tgl){
    $data = DB::select('
        SELECT *,
              (SELECT sum(qty*jasa_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join maintenance_jenis_order mjo on m.jenis_order = mjo.id
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mjo.rekon = mt.id and m.verify is null) as tl_jasa,
              (SELECT sum(qty*material_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join maintenance_jenis_order mjo on m.jenis_order = mjo.id
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mjo.rekon = mt.id and m.verify is null) as tl_mtr,
              (SELECT sum(qty*jasa_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join maintenance_jenis_order mjo on m.jenis_order = mjo.id
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mjo.rekon = mt.id and m.verify =1) as sm_jasa,
              (SELECT sum(qty*material_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join maintenance_jenis_order mjo on m.jenis_order = mjo.id
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mjo.rekon = mt.id and m.verify =1) as sm_mtr,
              (SELECT sum(qty*jasa_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join maintenance_jenis_order mjo on m.jenis_order = mjo.id
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mjo.rekon = mt.id and m.verify =2) as telkom_jasa,
              (SELECT sum(qty*material_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join maintenance_jenis_order mjo on m.jenis_order = mjo.id
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mjo.rekon = mt.id and m.verify =2) as telkom_mtr,
              (SELECT sum((qty*jasa_telkom)) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join maintenance_jenis_order mjo on m.jenis_order = mjo.id
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mjo.rekon = mt.id and m.verify =3) as siap_rekon_jasa,
              (SELECT sum(qty*material_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join maintenance_jenis_order mjo on m.jenis_order = mjo.id
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mjo.rekon = mt.id and m.verify =3) as siap_rekon_mtr,
              (SELECT sum((qty*jasa_telkom)) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join maintenance_jenis_order mjo on m.jenis_order = mjo.id
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mjo.rekon = mt.id) as total_jasa,
              (SELECT sum(qty*material_telkom) FROM maintaince_mtr mm
              left join khs_maintenance i on mm.id_item=i.id_item
              left join maintaince m on mm.maintaince_id = m.id
              left join maintenance_jenis_order mjo on m.jenis_order = mjo.id
              WHERE m.isHapus = 0 AND m.tgl_selesai like "'.$tgl.'%" and mjo.rekon = mt.id) as total_mtr

              FROM maintenance_rekon mt
              WHERE 1
      ');
    //dd($data);
    return view('dashboard.revenuePerProgram', compact('data'));
  }
  public function rincianRevenue($jenis,$mitra, $verif, $tgl){
    $q = "and verify = ".$verif;
    if($verif == 'null')
      $q = "and verify is ".$verif;
    if($verif == 'all')
      $q = "";
    $q2 = " and mr.nama_mitra = '".$mitra."' ";
    if($mitra == "all")
      $q2 = "";
    if($jenis == "MITRA"){
      $maintenance = DB::select("
        SELECT m.*,m2.*,i.material_ta as material,i.jasa_ta as jasa,i.satuan,
        (SELECT sum(qty*jasa) FROM maintaince_mtr mm
        left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_boq
        FROM maintaince m
        right join maintaince_mtr m2 on m.id = m2.maintaince_id
        LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
        LEFT JOIN regu mr on m.dispatch_regu_id = mr.id_regu
        WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q2." ".$q." order by m2.id_item asc"
      );
    }else{
      $maintenance = DB::select("
        SELECT m.*,m2.*,i.satuan,i.jasa_telkom as jasa, i.material_telkom as material,
        (SELECT sum((qty*jasa)+(qty*material)) FROM maintaince_mtr mm
        left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_boq
        FROM maintaince m
        right join maintaince_mtr m2 on m.id = m2.maintaince_id
        LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
        LEFT JOIN regu mr on m.dispatch_regu_id = mr.id_regu
        WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q2." ".$q." ".$q." order by m2.id_item asc"
      );
    }
    $data = array();
    $lastNama = '';
    $no=0;
    $head = array();
    $title = array();
    foreach ($maintenance as $no => $m){
      if(!empty($m->no_tiket)){
        $head[] = $m->no_tiket;
        $title[$m->no_tiket] = $m->tgl_selesai.'<br/>'.$m->no_tiket.'<br/>'.$m->sto;
      }
    }
    $head = array_unique($head);
    $total = array();
    $totalmaterial = array();
    $totaljasa = array();
    foreach($head as $h){
      $total[$h] = 0;
      $totalmaterial[$h] = 0;
      $totaljasa[$h] = 0;
    }
    foreach ($maintenance as $no => $m){
      if($lastNama == $m->id_item){
        $data[count($data)-1]['no_tiket'][$m->no_tiket] = $m->qty;

        if($jenis == "MITRA"){
          $total[$m->no_tiket] += $m->qty*$m->jasa;
          $totaljasa[$m->no_tiket] += ($m->qty*$m->jasa);
          $totalmaterial[$m->no_tiket] += 0;
        }else{
          $totalmaterial[$m->no_tiket] += ($m->qty*$m->material);
          if($m->jenis_order==4){
            $totaljasa[$m->no_tiket] += 0;
            $total[$m->no_tiket] += ($m->qty*$m->material);
          }
          else{
            $totaljasa[$m->no_tiket] += ($m->qty*$m->jasa);
            $total[$m->no_tiket] += ($m->qty*$m->jasa)+($m->qty*$m->material);
          }

        }
        $no++;
      }else{
        $data[] = array("id_item" => $m->id_item, "satuan" => $m->satuan, "jasa" => $m->jasa, "jenis_order" => $m->jenis_order, "material" => $m->material, "total_boq" => $m->total_boq, "no_tiket" => array(), "total" => array());
        foreach($head as $h){
          if($h == $m->no_tiket){
            $data[count($data)-1]['no_tiket'][$h] = $m->qty;

            if($jenis == "MITRA"){
              $total[$h] += $m->qty*$m->jasa;
              $totalmaterial[$h] += 0;
              $totaljasa[$h] += ($m->qty*$m->jasa);
            }else{
              $totalmaterial[$h] += ($m->qty*$m->material);
              if($m->jenis_order==4){
                $total[$h] += ($m->qty*$m->material);
                $totaljasa[$h] += 0;
              }
              else{
                $total[$h] += ($m->qty*$m->jasa)+($m->qty*$m->material);
                $totaljasa[$h] += ($m->qty*$m->jasa);
              }
            }
          }else{
            $data[count($data)-1]['no_tiket'][$h] = 0;
            $data[count($data)-1]['total'][$h] = 0;
          }
        }
        $no=0;
      }
      $lastNama = $m->id_item;
    }
    $maintenance = DB::select("
      SELECT m.no_tiket, m.tgl_selesai
      FROM maintaince m
        right join maintaince_mtr m2 on m.id = m2.maintaince_id
        LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
        LEFT JOIN regu mr on m.dispatch_regu_id = mr.id_regu
      WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q2." ".$q." order by m.tgl_selesai asc
    ");
    foreach ($maintenance as $no => $m){
      if(!empty($m->no_tiket)){
        $sorthead[] = $m->no_tiket;
      }
    }
    $sorthead = array_unique($sorthead);
    $head = $sorthead;
    return view('dashboard.rincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
  }

  public function rincianRevenueProgram($tgl, $jenis_rekon, $verif){
    $q = "and verify = ".$verif;
    if($verif == 'null')
      $q = "and verify is ".$verif;
    if($verif == 'all')
      $q = "";
    $q2 = " and mjo.rekon = '".$jenis_rekon."' ";
    if($jenis_rekon == "all")
      $q2 = "";

      $maintenance = DB::select("
        SELECT m.*,m2.*,i.satuan,i.jasa_telkom as jasa, i.material_telkom as material, UPPER(m2.id_item) as id_item,
        (SELECT sum((qty*jasa)+(qty*material)) FROM maintaince_mtr mm
        left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_boq
        FROM maintaince m
        right join maintaince_mtr m2 on m.id = m2.maintaince_id
        LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
        LEFT JOIN maintenance_jenis_order mjo on m.jenis_order = mjo.id
        WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q2." ".$q." order by m2.id_item asc"
      );
    $data = array();
    $lastNama = '';
    $no=0;
    $head = array();
    $title = array();
    foreach ($maintenance as $no => $m){
      if(!empty($m->no_tiket)){
        $head[] = $m->no_tiket;
        $title[$m->no_tiket] = $m->tgl_selesai.'<br/>'.$m->no_tiket.'<br/>'.$m->sto;
      }
    }
    $head = array_unique($head);
    $total = array();
    $totalmaterial = array();
    $totaljasa = array();
    foreach($head as $h){
      $total[$h] = 0;
      $totalmaterial[$h] = 0;
      $totaljasa[$h] = 0;
    }
    foreach ($maintenance as $no => $m){
      if($lastNama == $m->id_item){
        $data[count($data)-1]['no_tiket'][$m->no_tiket] = $m->qty;
        $totalmaterial[$m->no_tiket] += ($m->qty*$m->material);
        if($m->jenis_order==4){
          $totaljasa[$m->no_tiket] += 0;
          $total[$m->no_tiket] += ($m->qty*$m->material);
        }
        else{
          $totaljasa[$m->no_tiket] += ($m->qty*$m->jasa);
          $total[$m->no_tiket] += ($m->qty*$m->jasa)+($m->qty*$m->material);
        }
        $no++;
      }else{
        $data[] = array("id_item" => $m->id_item, "satuan" => $m->satuan, "jasa" => $m->jasa, "jenis_order" => $m->jenis_order, "material" => $m->material, "total_boq" => $m->total_boq, "no_tiket" => array(), "total" => array());
        foreach($head as $h){
          if($h == $m->no_tiket){
            $data[count($data)-1]['no_tiket'][$h] = $m->qty;
            $totalmaterial[$h] += ($m->qty*$m->material);
            if($m->jenis_order==4){
              $totaljasa[$h] += 0;
              $total[$h] += ($m->qty*$m->material);
            }
            else{
              $totaljasa[$h] += ($m->qty*$m->jasa);
              $total[$h] += ($m->qty*$m->jasa)+($m->qty*$m->material);
            }
          }else{
            $data[count($data)-1]['no_tiket'][$h] = 0;
            $data[count($data)-1]['total'][$h] = 0;
          }
        }
        $no=0;
      }
      $lastNama = $m->id_item;
    }
    $maintenance = DB::select("
      SELECT m.no_tiket, m.tgl_selesai, UPPER(m2.id_item) as id_item
      FROM maintaince m
        right join maintaince_mtr m2 on m.id = m2.maintaince_id
        LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
        LEFT JOIN maintenance_jenis_order mjo on m.jenis_order = mjo.id
      WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q2." ".$q." order by m.tgl_selesai asc
    ");
    foreach ($maintenance as $no => $m){
      if(!empty($m->no_tiket)){
        $sorthead[] = $m->no_tiket;
      }
    }
    $sorthead = array_unique($sorthead);
    $head = $sorthead;
    return view('dashboard.rincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
  }
  public function revenue_program_ta($tgl){
    $data = DB::select("SELECT mjo.*,
    SUM( CASE WHEN m.verify IS NULL THEN (qty * jasa_telkom) END ) AS tl_jasa,
    SUM( CASE WHEN m.verify IS NULL THEN (qty * material_telkom) END ) AS tl_mtr,
    SUM( CASE WHEN m.verify = 1 THEN (qty * jasa_telkom) END ) AS telkom_jasa,
    SUM( CASE WHEN m.verify = 1 THEN (qty * material_telkom) END ) AS telkom_mtr,
    SUM( CASE WHEN m.verify = 2 THEN (qty * jasa_telkom) END ) AS siap_rekon_jasa,
    SUM( CASE WHEN m.verify = 2 THEN (qty * material_telkom) END ) AS siap_rekon_mtr,
    SUM( CASE WHEN m.verify = 2 AND r.pekerjaan = 1 THEN (qty * jasa_telkom) END ) AS siap_rekon_jasa_ioan,
    SUM( CASE WHEN m.verify = 2 AND r.pekerjaan = 1 THEN (qty * material_telkom) END ) AS siap_rekon_mtr_ioan,
    SUM( CASE WHEN m.verify = 2 AND r.pekerjaan != 1 THEN (qty * jasa_telkom) END ) AS siap_rekon_jasa_non_ioan,
    SUM( CASE WHEN m.verify = 2 AND r.pekerjaan != 1 THEN (qty * material_telkom) END ) AS siap_rekon_mtr_non_ioan,
    SUM( qty * jasa_telkom ) AS total_jasa,
    SUM( qty * material_telkom ) AS total_mtr
    FROM maintaince_mtr mm
    LEFT JOIN khs_maintenance i ON mm.id_item=i.id_item
    LEFT JOIN maintaince m ON mm.maintaince_id = m.id
    LEFT JOIN regu r ON r.id_regu = m.dispatch_regu_id
    LEFT JOIN maintenance_jenis_order mjo ON m.jenis_order = mjo.id
    WHERE m.isHapus = 0 AND m.tgl_selesai like '$tgl%'
    GROUP BY mjo.id");

    return view('dashboard.program', compact('data'));
  }
  public function rincianRevenueProgramTa($tgl, $jenis_rekon, $verif, $stts){
    // dd('tes');
    $q = '';

    if($verif != "all")
    {

      if($verif == 'null')
      {
        $q .= " AND verify is NULL";
      }
      else
      {
        $q .= "AND verify = ".$verif;
      }
    }

    if($jenis_rekon != "all")
    {
      $q .= " AND m.jenis_order = $jenis_rekon";
    }

    if($stts != "all")
    {
      if($stts == 'ioan')
      {
        $q .= " AND r.pekerjaan = 1";
      }
      elseif($stts == 'non_ioan')
      {
        $q .= " AND r.pekerjaan != 1";
      }
    }

    $maintenance = DB::select("SELECT m.*,mm.*,i.satuan,i.jasa_telkom as jasa, i.material_telkom as material, UPPER(mm.id_item) as id_item,
      (SELECT sum( (qty*jasa_telkom)+(qty*material_telkom) ) FROM maintaince_mtr mm
      left join khs_maintenance i on mm.id_item = i.id_item WHERE mm.maintaince_id = m.id) as total_boq
      FROM maintaince_mtr mm
      LEFT JOIN khs_maintenance i ON mm.id_item=i.id_item
      LEFT JOIN maintaince m ON mm.maintaince_id = m.id
      LEFT JOIN regu r ON r.id_regu = m.dispatch_regu_id
      LEFT JOIN maintenance_jenis_order mjo ON m.jenis_order = mjo.id
      WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q." ORDER BY mm.id_item asc
    ");

    $data = array();
    $lastNama = '';
    $no=0;
    $head = array();
    $title = array();

    foreach ($maintenance as $no => $m){
      if(!empty($m->no_tiket)){
        $head[] = $m->no_tiket;
        $title[$m->no_tiket] = $m->tgl_selesai.'<br/>'.$m->no_tiket.'<br/>'.$m->sto;
        //$title[$m->no_tiket] = ["tgl"=>$m->tgl_selesai, "no_tiket"=>$m->no_tiket, "sto"=>$m->sto];
      }
    }
    $head = array_unique($head);
    // dd($head);
    $total = array();
    $totalmaterial = array();
    $totaljasa = array();

    foreach($head as $h){
      $total[$h]         = 0;
      $totalmaterial[$h] = 0;
      $totaljasa[$h]     = 0;
    }

    foreach ($maintenance as $no => $m){
      if($lastNama == $m->id_item){
        $data[count($data)-1]['no_tiket'][$m->no_tiket] = $m->qty;
        $totalmaterial[$m->no_tiket] += ($m->qty*$m->material);
        if($m->jenis_order==4){
          $totaljasa[$m->no_tiket] += 0;
          $total[$m->no_tiket] += ($m->qty*$m->material);
        }
        else{
          $totaljasa[$m->no_tiket] += ($m->qty*$m->jasa);
          $total[$m->no_tiket] += ($m->qty*$m->jasa)+($m->qty*$m->material);
        }
        $no++;
      }else{
        $data[] = array("id_item" => $m->id_item, "satuan" => $m->satuan, "jasa" => $m->jasa, "jenis_order" => $m->jenis_order, "material" => $m->material, "total_boq" => $m->total_boq, "no_tiket" => array(), "total" => array());
        foreach($head as $h){
          if($h == $m->no_tiket){
            $data[count($data)-1]['no_tiket'][$h] = $m->qty;
            $totalmaterial[$h] += ($m->qty*$m->material);
            if($m->jenis_order==4){
              $total[$h] += ($m->qty*$m->material);
              $totaljasa[$h] += 0;
            }
            else{
              $total[$h] += ($m->qty*$m->jasa)+($m->qty*$m->material);
              $totaljasa[$h] += ($m->qty*$m->jasa);
            }
          }else{
            $data[count($data)-1]['no_tiket'][$h] = 0;
            $data[count($data)-1]['total'][$h] = 0;
          }
        }
        $no=0;
      }
      $lastNama = $m->id_item;
    }

    $maintenance = DB::select("SELECT m.no_tiket, m.tgl_selesai
      FROM maintaince m
      LEFT JOIN regu r ON r.id_regu = m.dispatch_regu_id
      right join maintaince_mtr m2 on m.id = m2.maintaince_id
      WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q." order by m.tgl_selesai asc
    ");

    foreach ($maintenance as $no => $m){
      if(!empty($m->no_tiket)){
        $sorthead[] = $m->no_tiket;
      }
    }
    $sorthead = array_unique($sorthead);
    $head = $sorthead;
    // dd($data, $head);
    return view('dashboard.rincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
  }

  public function number_to_alphabet($number) {
    $number = intval($number);
    if($number <= 0)
    {
      return '';
    }

    $alphabet = '';
    while($number != 0)
    {
      $p = ($number - 1) % 26;
      $number = intval( ($number - $p) / 26);
      $alphabet = chr(65 + $p) . $alphabet;
   }
   return $alphabet;
  }

  public function download_rincian_rev($tgl, $jenis_rekon, $verif, $jenis, $generate_evi, $generate_doc){
    Excel::create('Rincian Revenue Program TA - '.$tgl, function($excel) use($tgl, $jenis_rekon, $verif, $jenis, $generate_evi, $generate_doc){
      $q = '';

      if($verif != "all")
      {
        if($verif == 'null')
        {
          $q .= " AND verify is NULL";
        }
        else
        {
          $q .= "AND verify = ".$verif;
        }
      }

      if($jenis_rekon != "all")
      {
        $q .= " AND m.jenis_order = $jenis_rekon";
      }

      if($jenis != "all")
      {
        if($jenis == 'ioan')
        {
          $q .= " AND r.pekerjaan = 1";
        }
        elseif($jenis == 'non_ioan')
        {
          $q .= " AND r.pekerjaan != 1";
        }
      }

      $maintenance = DB::select("SELECT m.*,m2.*,i.satuan,i.jasa_telkom as jasa, i.material_telkom as material, UPPER(i.uraian) as uraian, UPPER(m2.id_item) as id_item,
        dispatch_regu_id, dispatch_regu_name,
        (SELECT CONCAT(u_nama, ' (', username1, ')') FROM user WHERE id_karyawan = username1 GROUP BY id_karyawan)as nik_1,
        (SELECT CONCAT(u_nama, ' (', username2, ')') FROM user WHERE id_karyawan = username2 GROUP BY id_karyawan)as nik_2,
        (SELECT sum((qty * jasa) + (qty * material) ) FROM maintaince_mtr mm
        left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_boq
        FROM maintaince m
        LEFT JOIN regu r ON r.id_regu = m.dispatch_regu_id
        right join maintaince_mtr m2 on m.id = m2.maintaince_id
        LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
        WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q." order by m2.id_item asc"
      );

      $data = $head = $title = array();
      // $lastNama = '';
      // $no=0;

      foreach ($maintenance as $no => $m)
      {
        if(!empty($m->no_tiket)){
          // $head[] = $m->no_tiket;
          $title[$m->no_tiket] = array('tgl'=>$m->tgl_selesai, 'nik1'=>$m->nik_1, 'nik2'=>$m->nik_2, 'regu_name' =>$m->dispatch_regu_name, 'no_tiket'=>$m->no_tiket,'sto'=>'STO-'.$m->sto,'action'=>date("d-m-Y", strtotime($m->tgl_selesai)).' '.$m->nama_odp.' '.$m->action);
        }
      }
      // $head = array_unique($head);
      //sort head per tgl
      $maintenance2 = DB::select("SELECT m.no_tiket, m.tgl_selesai, UPPER(m2.id_item) as id_item
        FROM maintaince m
        LEFT JOIN regu r ON r.id_regu = m.dispatch_regu_id
        right join maintaince_mtr m2 on m.id = m2.maintaince_id
        LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
        WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q." order by m.tgl_selesai asc, m.sto asc
      ");

      foreach ($maintenance2 as $no => $m)
      {
        if(!empty($m->no_tiket) )
        {
          $head[] = $m->no_tiket;
        }
      }

      $head = array_unique($head);
      //endsort head
      $total = $totalmaterial = $totaljasa = array();

      foreach($head as $h){
        $total[$h] = 0;
        $totalmaterial[$h] = 0;
        $totaljasa[$h] = 0;
      }

      // foreach ($maintenance as $no => $m){
      //   if($lastNama == $m->id_item){
      //     $data[count($data)-1]['no_tiket'][$m->no_tiket] = $m->qty;

      //     $totalmaterial[$m->no_tiket] += ($m->qty*$m->material);
      //     if($m->jenis_order == 4){
      //       $totaljasa[$m->no_tiket] += 0;
      //       $total[$m->no_tiket] += ($m->qty*$m->material);
      //     }
      //     else{
      //       $totaljasa[$m->no_tiket] += ($m->qty*$m->jasa);
      //       $total[$m->no_tiket] += ($m->qty*$m->jasa)+($m->qty*$m->material);
      //     }
      //     $no++;
      //   }else{
      //     $data[] = array("id_item" => $m->id_item, "satuan" => $m->satuan, "uraian" => $m->uraian, "jasa" => $m->jasa, "material" => $m->material, "total_boq" => $m->total_boq, "no_tiket" => array() );

      //     foreach($head as $h){
      //       if($h == $m->no_tiket){
      //         $data[count($data)-1]['no_tiket'][$h] = $m->qty;
      //         $totalmaterial[$h] += ($m->qty*$m->material);
      //         if($m->jenis_order == 4){
      //           $total[$h] += ($m->qty*$m->material);
      //           $totaljasa[$h] += 0;
      //         }else{
      //           $total[$h] += ($m->qty*$m->jasa)+($m->qty*$m->material);
      //           $totaljasa[$h] += ($m->qty*$m->jasa);
      //         }

      //       }else{
      //         $data[count($data)-1]['no_tiket'][$h] = 0;
      //       }
      //     }
      //     $no=0;
      //   }
      //   $lastNama = $m->id_item;
      // }

      $renew_head = array_map(function($x){
        return $x = 0;
      }, array_flip($head) );

      foreach ($maintenance as $m)
      {
        if(!isset($data[$m->id_item]) )
        {
          $data[$m->id_item] = [
            "id_item"   => $m->id_item,
            "satuan"    => $m->satuan,
            "uraian"    => $m->uraian,
            "jasa"      => $m->jasa,
            "material"  => $m->material,
            "total_boq" => $m->total_boq,
            "no_tiket"  => []
          ];
        }
        if(empty($data[$m->id_item]['no_tiket']) )
        {
          $data[$m->id_item]['no_tiket'] = $renew_head;
        }

        if(in_array($m->no_tiket, $head) )
        {
          // $tes[$m->no_tiket][] = $m->qty;
          // $tes2[$m->no_tiket]['material'][$m->id_item][] = $m->material;
          // $tes2[$m->no_tiket]['harga'][] = ($m->qty * $m->material);

          $data[$m->id_item]['no_tiket'][$m->no_tiket] += $m->qty;
          $totalmaterial[$m->no_tiket] += ($m->qty * $m->material);


          if($m->jenis_order == 4)
          {
            $total[$m->no_tiket]     += ($m->qty * $m->material);
            $totaljasa[$m->no_tiket] += 0;
          }
          else
          {
            $total[$m->no_tiket]     += ($m->qty * $m->jasa) + ($m->qty * $m->material);
            $totaljasa[$m->no_tiket] += ($m->qty * $m->jasa);
          }
        }
      }

      $data = array_values($data);
      if($generate_doc == 'ok')
      {
        $excel->sheet("boq", function($sheet) use($data, $total, $head, $totalmaterial, $totaljasa, $title){

          $header[0] = ['#', 'Id Item', 'Uraian', 'Jasa', 'Material', 'Satuan'];
          $header[1] = [null, null, null, null, null, null];
          $header[2] = [null, null, null, null, null, null];
          $header[3] = [null, null, null, null, null, null];
          $header[4] = [null, null, null, null, null, null];

          foreach($head as $h)
          {
            $header[0][] = $h;
            $header[1][] = preg_replace("/^=/", "", $title[$h]['action'], 1);
            $header[2][] = $title[$h]['regu_name'];
            $header[3][] = $title[$h]['nik1'] .'&'. $title[$h]['nik2'];
            $header[4][] = $title[$h]['sto'];
          }

          $no_array = 4;

          foreach($data as $no => $list)
          {
            $subtotal = 0;
            ++$no_array;
            $header[$no_array][] = $no;
            $header[$no_array][] = $list['id_item'];
            $header[$no_array][] = $list['uraian'];
            $header[$no_array][] = (int)$list['jasa'];
            $header[$no_array][] = (int)$list['material'];
            $header[$no_array][] = $list['satuan'];

            foreach($list['no_tiket'] as $l2)
            {
              $header[$no_array][] = $l2 ?: '-';
              $subtotal +=  (int)($l2 * $list['jasa']) + ($l2 * $list['material']) ;
            }

            $header[$no_array][] = $subtotal;
          }

          $header[($no_array + 2)] = ['Total Material', null, null, null, null, null];
          $header[($no_array + 3)] = ['Total Jasa', null, null, null, null, null];
          $header[($no_array + 4)] = ['Total', null, null, null, null, null];

          $header[0][] = 'Total';

          $sumtotal = $sumtotaljasa = $sumtotalmaterial = 0;

          foreach($head as $h)
          {
            $header[($no_array + 2)][] = (int)@$totalmaterial[$h];
            $sumtotalmaterial += @$totalmaterial[$h];

            $header[($no_array + 3)][] = (int)@$totaljasa[$h];
            $sumtotaljasa += @$totaljasa[$h];

            $header[($no_array + 4)][] = (int)@$total[$h];
            $sumtotal += @$total[$h];
          }

          $header[($no_array + 2)][] = (int)$sumtotalmaterial;
          $header[($no_array + 3)][] = (int)$sumtotaljasa;
          $header[($no_array + 4)][] = (int)$sumtotal;

          $border_Style = [
            'borders' => [
              'outline' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN
              ],
              'inside' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN
              ],
            ]
          ];

          $sheet->rows($header);

          $sheet->getStyle('A1:F5')->getAlignment()->setWrapText(true);
          $sheet->mergeCells('A1:A5');
          $sheet->mergeCells('B1:B5');
          $sheet->mergeCells('C1:C5');
          $sheet->mergeCells('D1:D5');
          $sheet->mergeCells('E1:E5');
          $sheet->mergeCells('F1:F5');
          $sheet->mergeCells('A'. ($no_array + 2) .':F' . ($no_array + 2) );
          $sheet->mergeCells('A'. ($no_array + 3) .':F' . ($no_array + 3) );
          $sheet->mergeCells('A'. ($no_array + 4) .':F' . ($no_array + 4) );

          $sheet->getStyle('A'. ($no_array + 2) .':F' . ($no_array + 4) )->applyFromArray([
            'alignment' => [
              'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            ]
          ]);

          $sheet->mergeCells(self::number_to_alphabet(count($head) + 7 ) .'1:'. self::number_to_alphabet(count($head) + 7 ) .'5');
          $sheet->getStyle('G1:'. self::number_to_alphabet(count($head) + 7 ) .'5' )->getFont()->setBold(true);
          $sheet->getStyle('A1:'.self::number_to_alphabet(count($head) + 7 ).''. ($no_array + 4) )->applyFromArray($border_Style);
          $sheet->getStyle('A1:'.self::number_to_alphabet(count($head) + 7 ).''. ($no_array + 1) )->applyFromArray([
            'alignment' => [
              'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
              'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ]
          ]);
        });
      }

      if($generate_evi == 'ok')
      {
        $mt = DB::select("SELECT m.*, (select count(*) from maintaince_mtr where maintaince_id=m.id) as mtr
          FROM maintaince m
          LEFT JOIN regu r ON r.id_regu = m.dispatch_regu_id
          WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q." GROUP BY no_tiket having mtr > 0 order by m.tgl_selesai asc, m.sto asc
        ");

        $excel->sheet('evidence', function($sheet) use($tgl, $jenis_rekon, $jenis, $mt) {
          $nomor_in = 0;
          $raw_data = $alphabet = $raw_photo = $raw_photo_link = [];

          for ($i = 'A'; $i !== 'ZZ'; $i++){
            $alphabet[] = $i;
          }

          foreach($mt as $no => $list)
          {
            $raw_data[$no]['judul'] = ++$nomor_in .'.'. $list->no_tiket;
            $raw_photo[$no]['judul'] = null;
            $raw_photo_link[$no]['judul'] = null;

            if(@$jenis == 'rincian revenue' && @$list->jenis_order == 17)
            {
              $template_photo[$list->id][] = $list->id."/Redaman-IN-OUT-Sesudah";
            }
            else
            {
              if ($list->jenis_order == 5)
              {
                $template_photo[$list->id]["Pemasangan-Aksesoris-Tiang-Sesudah(dekat)"] = $list->id."/Pemasangan-Aksesoris-Tiang-Sesudah(dekat)";
              }
              else
              {
                $template_photo[$list->id]["Foto-GRID"] = $list->id."/Foto-GRID";
              }
            }

            foreach ($template_photo as $k => $v)
            {
              if ($k == $list->id)
              {
                foreach($v as $kk => $photo)
                {
                  $path  = "/upload/maintenance/".$photo;
                  $path2 = "/upload2/maintenance/".$photo;
                  $path3 = "/upload3/maintenance/".$photo;
                  $th    = "$path-th.jpg";
                  $th2   = "$path2-th.jpg";
                  $th3   = "$path3-th.jpg";
                  // $th    = "$path.jpg";
                  // $th2   = "$path2.jpg";
                  // $th3   = "$path3.jpg";

                  $raw_data[$no]['id']  = str_replace('-', ' ', $k);
                  $raw_photo[$no]['id'] = null;
                  $raw_photo_link[$no]['id'] = null;

                  if (file_exists(public_path().$th) )
                  {
                    $raw_photo[$no]['path_foto'] = public_path().$th;
                    $raw_photo_link[$no]['url_foto'] = "https://marina.bjm.tomman.app/$path.jpg";
                    $raw_data[$no]['path_foto']  = 'halaman photo';
                  }
                  elseif (file_exists(public_path().$th2) )
                  {
                    $raw_photo[$no]['path_foto'] = public_path().$th2;
                    $raw_photo_link[$no]['url_foto'] = "https://marina.bjm.tomman.app/$path.jpg";
                    $raw_data[$no]['path_foto']  = 'halaman photo';
                  }
                  elseif (file_exists(public_path().$th3) )
                  {
                    $raw_photo[$no]['path_foto'] = public_path().$th3;
                    $raw_photo_link[$no]['url_foto'] = "https://marina.bjm.tomman.app/$path.jpg";
                    $raw_data[$no]['path_foto']  = 'halaman photo';
                  }
                  else
                  {
                    $raw_photo[$no]['path_foto'] = public_path(). "/image/placeholder.gif";
                    $raw_photo_link[$no]['url_foto'] = 'tes' . $no;
                    $raw_data[$no]['path_foto']  = 'halaman photo';
                  }
                }
              }
            }
          }

          $data = $data_photo = $data_photo_url = [];
          foreach($raw_data as $v)
          {
            foreach($v as $vv)
            {
              $data[] = $vv;
            }
          }

          foreach($raw_photo as $v)
          {
            foreach($v as $vv)
            {
              $data_photo[] = $vv;
            }
          }

          foreach($raw_photo_link as $v)
          {
            foreach($v as $vv)
            {
              $data_photo_url[] = $vv;
            }
          }


          if($data)
          {
            $data = array_chunk($data, 36);
          }

          $data_photo = array_chunk($data_photo, 36);
          $data_photo_url = array_chunk($data_photo_url, 36);

          $sheet->rows($data);

          if($data)
          {
            $k = 0;
            foreach($data_photo as $kx => $v)
            {
              ++$k;
              foreach($v as $kk => $vv)
              {
                if(file_exists($vv) )
                {
                  $objDrawing = new \PHPExcel_Worksheet_Drawing;
                  $objDrawing->setPath($vv);
                  $objDrawing->setCoordinates($alphabet[$kk] . '' . $k);
                  $sheet->getHyperlink($alphabet[$kk] . '' . $k)->setUrl($data_photo_url[$kx][$kk]);
                  $objDrawing->setHeight(138);
                  $objDrawing->setWidth(103);
                  $objDrawing->setWorksheet($sheet);
                }
              }
            }

            for ($i = 1 ; $i < count($data_photo) + 3; $i++) {
              $set_height[] = 120;
            }

            $sheet->setHeight($set_height);
            $sheet->getStyle('A1:'.$alphabet[(count($data[0]) - 1)].''. count($data) )->applyFromArray([
              'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
              ]
            ]);
          }
        });

        $c_mt = [];

        foreach($mt as $k => $v)
        {
          $c_mt['data'][$k]['no_tiket'] = $v->no_tiket;
          $c_mt['data'][$k]['sto'] = $v->sto;
          $c_mt['data'][$k]['id'] = $v->id;
          $c_mt['data'][$k]['jenis_order'] = $v->jenis_order;
        }

        if($c_mt)
        {
          $c_mt['data'] = array_chunk($c_mt['data'], 10);
        }

        $excel->sheet('Data Telkom', function($sheet) use($tgl, $jenis_rekon, $c_mt) {
          $data = $raw_photo = $raw_photo_link = $new_data = $arr_data = $new_data_photo = $arr_data_photo = [];
          $nomor = 0;

          for ($i = 'A'; $i !== 'ZZ'; $i++){
            $alphabet[] = $i;
          }

          if($c_mt)
          {
            foreach ($c_mt['data'] as $k => $v )
            {
              foreach ($v as $k => $vv)
              {
                $data[0][] = ++$nomor;
                $data[1][] = $vv['no_tiket'];
                $data[2][] = $vv['sto'];

                $raw_photo[0][] = null;
                $raw_photo[1][] = null;
                $raw_photo[2][] = null;

                $raw_photo_link[0][] = null;
                $raw_photo_link[1][] = null;
                $raw_photo_link[2][] = null;
              }

              foreach ($v as $vv)
              {
                if(@$jenis_rekon == 'rincian revenue' && @$vv['jenis_order'] == 17)
                {
                    $template_photo[$vv['id'] ][] = $vv['id']."/Redaman-IN-OUT-Sesudah";
                }
                else
                {
                  if ($vv['jenis_order'] == 5)
                  {
                    $template_photo[$vv['id'] ]["Pemasangan-Aksesoris-Tiang-Sesudah(dekat)"] = $vv['id']."/Pemasangan-Aksesoris-Tiang-Sesudah(dekat)";
                  }
                  else
                  {
                    $template_photo[$vv['id'] ]["Foto-GRID"] = $vv['id']."/Foto-GRID";
                  }
                }

                foreach ($template_photo as $k => $v)
                {
                  if ($k == $vv['id'] )
                  {
                    foreach ($v as $kk => $photo)
                    {
                      $path  = "/upload/maintenance/".$photo;
                      $path2 = "/upload2/maintenance/".$photo;
                      $path3 = "/upload3/maintenance/".$photo;
                      $th    = "$path-th.jpg";
                      $th2   = "$path2-th.jpg";
                      $th3   = "$path3-th.jpg";
                      // $th    = "$path.jpg";
                      // $th2   = "$path2.jpg";
                      // $th3   = "$path3.jpg";

                      if (file_exists(public_path().$th))
                      {
                        $data[3][]      = 'template photo';
                        $raw_photo[3][] = public_path().$th;
                        $raw_photo_link[3][] = "https://marina.bjm.tomman.app/$path.jpg";
                      }
                      elseif (file_exists(public_path().$th2))
                      {
                        $data[3][]      = 'template photo';
                        $raw_photo[3][] = public_path().$th2;
                        $raw_photo_link[3][] = "https://marina.bjm.tomman.app/$path2.jpg";
                      }
                      elseif (file_exists(public_path().$th3))
                      {
                        $data[3][]      = 'template photo';
                        $raw_photo[3][] = public_path().$th3;
                        $raw_photo_link[3][] = "https://marina.bjm.tomman.app/$path3.jpg";
                      }
                      else
                      {
                        $data[3][]      = 'template photo';
                        $raw_photo[3][] = public_path() . "/image/placeholder.gif";
                        $raw_photo_link[3][] = 'tes';
                      }
                    }
                  }
                }
              }
            }
          }

          foreach($data as $v)
          {
            $new_data[] = array_chunk($v, 12);
          }

          foreach($new_data as $k => $v)
          {
            foreach($v as $kk => $vv)
            {
              $arr_data[$kk][] = $vv;
            }
          }

          $data = [];

          foreach($arr_data as $v)
          {
            foreach($v as $vv)
            {
              $data[] = $vv;
            }
          }

          foreach($raw_photo as $v)
          {
            $new_data_photo[] = array_chunk($v, 12);
          }


          foreach($raw_photo_link as $v)
          {
            $new_data_photo_link[] = array_chunk($v, 12);
          }


          foreach($new_data_photo as $k => $v)
          {
            foreach($v as $kk => $vv)
            {
              $arr_data_photo[$kk][] = $vv;
            }
          }

          foreach($new_data_photo_link as $k => $v)
          {
            foreach($v as $kk => $vv)
            {
              $arr_data_photo_link[$kk][] = $vv;
            }
          }

          $data_pht = $data_pht_link= [];

          foreach($arr_data_photo as $v)
          {
            foreach($v as $vv)
            {
              $data_pht[] = $vv;
            }
          }

          foreach($arr_data_photo_link as $v)
          {
            foreach($v as $vv)
            {
              $data_pht_link[] = $vv;
            }
          }

          $sheet->rows($data);

          $k = 0;
          foreach($data_pht as $kx => $v)
          {
            ++$k;
            foreach($v as $kk => $vv)
            {
              if(file_exists($vv) )
              {
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath($vv);
                $objDrawing->setCoordinates($alphabet[$kk] . '' . $k);
                $sheet->getHyperlink($alphabet[$kk] . '' . $k)->setUrl($data_pht_link[$kx][$kk]);
                $objDrawing->setHeight(138);
                $objDrawing->setWidth(103);
                $objDrawing->setWorksheet($sheet);
              }
            }
          }

          $no_pht = 0;

          if($data)
          {
            foreach ($data_pht as $k => $v)
            {
              ++$no_pht;
              foreach($v as $kk => $vv)
              {
                if($vv)
                {
                  $set_height[$no_pht] = 120;
                }
              }
            }

            $sheet->setHeight($set_height);
            $sheet->getStyle('A1:'.$alphabet[(count($data[0]) - 1)].''. count($data) )->applyFromArray([
              'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
              ]
            ]);
          }
        });

        $excel->sheet("evidence2", function($sheet) use($tgl, $jenis_rekon, $mt){
          $data = $raw_photo = $raw_photo_link = [];
          $nomor = 0;

          for ($i = 'A'; $i !== 'ZZ'; $i++){
            $alphabet[] = $i;
          }

          foreach($mt as $no => $list)
          {
            ++$nomor;

            $path = public_path() . "/upload/maintenance/" . $list->id . "/";
            $path_file = @preg_grep('~^\w(.*-th).*$~', @scandir($path) );
            $path_file_ori = @preg_grep('~^\w(.*).*$~', @scandir($path) );

            $path2 = public_path() . "/upload2/maintenance/" . $list->id . "/";
            $path2_file = @preg_grep('~^\w(.*-th).*$~', @scandir($path2) );
            $path2_file_ori = @preg_grep('~^\w(.*).*$~', @scandir($path2) );

            $path3 = public_path() . "/upload3/maintenance/" . $list->id . "/";
            $path3_file = @preg_grep('~^\w(.*-th).*$~', @scandir($path3) );
            $path3_file_ori = @preg_grep('~^\w(.*).*$~', @scandir($path3) );

            $path_photo_arr = $path_photo_arr_link = [];

            if(count($path_file) != 0)
            {
              $files_path = array_values($path_file);
              if ($files_path) {
                foreach($files_path as $v)
                {
                  $path_photo_arr[$list->id][] = public_path() . "/upload/maintenance/" . $list->id . "/" . $v;
                }
              }
            }
            elseif(count($path2_file) != 0)
            {

              $files_path = array_values($path2_file);
              if ($files_path) {
                foreach($files_path as $v)
                {
                  $path_photo_arr[$list->id][] = public_path() . "/upload2/maintenance/" . $list->id . "/" . $v;
                }
              }
            }
            elseif(count($path3_file) != 0)
            {
              $files_path = array_values($path3_file);
              if ($files_path) {
                foreach($files_path as $v)
                {
                  $path_photo_arr[$list->id][] = public_path() . "/upload3/maintenance/" . $list->id . "/" . $v;
                }
              }
            }
            else
            {
              $path_photo_arr[$list->id][] = public_path() . "/image/placeholder.gif";
            }

            if(count($path_file_ori) != 0)
            {
              $files_path = array_values($path_file_ori);
              if ($files_path) {
                foreach($files_path as $v)
                {
                  $path_photo_arr_link[$list->id][] = "https://marina.bjm.tomman.app/" . $list->id . "/" . $v;
                }
              }
            }
            elseif(count($path2_file_ori) != 0)
            {

              $files_path = array_values($path2_file_ori);
              if ($files_path) {
                foreach($files_path as $v)
                {
                  $path_photo_arr_link[$list->id][] = "https://marina.bjm.tomman.app/" . $list->id . "/" . $v;
                }
              }
            }
            elseif(count($path3_file_ori) != 0)
            {
              $files_path = array_values($path3_file_ori);
              if ($files_path) {
                foreach($files_path as $v)
                {
                  $path_photo_arr_link[$list->id][] = "https://marina.bjm.tomman.app/" . $list->id . "/" . $v;
                }
              }
            }
            else
            {
              $path_photo_arr_link[$list->id][] = 'tes';
            }

            $data[$no]['no'] = $nomor;
            $data[$no]['id'] = $list->no_tiket;

            $raw_photo[$no]['no'] = null;
            $raw_photo[$no]['id'] = null;

            $raw_photo_link[$no]['no'] = null;
            $raw_photo_link[$no]['id'] = null;

            foreach ($path_photo_arr as $k => $v)
            {
              foreach ($v as $kk => $vv)
              {
                if($k == $list->id)
                {
                  $data[$no]['photo'. $kk] = 'Template Photo';
                  $raw_photo[$no]['photo'. $kk] = $vv;
                  $raw_photo_link[$no]['photo'. $kk] = $path_photo_arr_link[$k][$kk];
                }
              }
            }
          }

          $sheet->rows($data);
          $k = 0;

          foreach($raw_photo as $kx => $v)
          {
            ++$k;
            $v = array_values($v);
            foreach($v as $kk => $vv)
            {
              if(file_exists($vv) )
              {
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath($vv);
                $objDrawing->setCoordinates($alphabet[$kk] . '' . $k);

                $rpl = array_values($raw_photo_link[$kx]);

                $sheet->getHyperlink($alphabet[$kk] . '' . $k)->setUrl($rpl[$kk]);
                $objDrawing->setHeight(138);
                $objDrawing->setWidth(103);
                $objDrawing->setWorksheet($sheet);
              }
            }
          }

          if($data)
          {
            $no_pht = 0;

            foreach ($raw_photo as $k => $v)
            {
              ++$no_pht;
              foreach($v as $kk => $vv)
              {
                if($vv)
                {
                  $set_height[$no_pht] = 120;
                }
              }
            }

            $sheet->setHeight($set_height);
            $sheet->getStyle('A1:'.$alphabet[(count($data[0]) - 1)].''. count($data) )->applyFromArray([
              'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
              ]
            ]);
          }
        });
      }
    })->export('xlsx');
  }

  // public function download_rincian_rev_v1($tgl, $jenis_rekon, $verif, $jenis){
  //   // dd('tes');
  //   Excel::create('Rincian Revenue Program TA - '.$tgl, function($excel) use($tgl, $jenis_rekon, $verif, $jenis){
  //     $q = '';

  //     if($verif != "all")
  //     {
  //       if($verif == 'null')
  //       {
  //         $q .= " AND verify is NULL";
  //       }
  //       else
  //       {
  //         $q .= "AND verify = ".$verif;
  //       }
  //     }

  //     if($jenis_rekon != "all")
  //     {
  //       $q .= " AND m.jenis_order = $jenis_rekon";
  //     }

  //     if($jenis != "all")
  //     {
  //       if($jenis == 'ioan')
  //       {
  //         $q .= " AND r.pekerjaan = 1";
  //       }
  //       elseif($jenis == 'non_ioan')
  //       {
  //         $q .= " AND r.pekerjaan != 1";
  //       }
  //     }

  //     $maintenance = DB::select("SELECT m.*,m2.*,i.satuan,i.jasa_telkom as jasa, i.material_telkom as material, UPPER(i.uraian) as uraian, UPPER(m2.id_item) as id_item,
  //       dispatch_regu_id, dispatch_regu_name,
  //         (SELECT CONCAT(u_nama, ' (', username1, ')') FROM user WHERE id_karyawan = username1 GROUP BY id_karyawan)as nik_1,
  //         (SELECT CONCAT(u_nama, ' (', username2, ')') FROM user WHERE id_karyawan = username2 GROUP BY id_karyawan)as nik_2,
  //         (SELECT sum((qty*jasa)+(qty*material)) FROM maintaince_mtr mm
  //       left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as total_boq
  //       FROM maintaince m
  //       LEFT JOIN regu r ON r.id_regu = m.dispatch_regu_id
  //       right join maintaince_mtr m2 on m.id = m2.maintaince_id
  //       LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
  //       WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q." order by m2.id_item asc"
  //     );

  //     $data = array();
  //     $lastNama = '';
  //     $no=0;
  //     $head = array();
  //     $title = array();
  //     foreach ($maintenance as $no => $m){
  //       if(!empty($m->no_tiket)){
  //         $head[] = $m->no_tiket;
  //         $title[$m->no_tiket] = array('tgl'=>$m->tgl_selesai, 'nik1'=>$m->nik_1, 'nik2'=>$m->nik_2, 'regu_name' =>$m->dispatch_regu_name, 'no_tiket'=>$m->no_tiket,'sto'=>'STO-'.$m->sto,'action'=>date("d-m-Y", strtotime($m->tgl_selesai)).' '.$m->nama_odp.' '.$m->action);
  //       }
  //     }
  //     $head = array_unique($head);
  //     //sort head per tgl
  //     $sorthead = array();
  //     $maintenance2 = DB::select("SELECT m.no_tiket, m.tgl_selesai, UPPER(m2.id_item) as id_item
  //       FROM maintaince m
  //       LEFT JOIN regu r ON r.id_regu = m.dispatch_regu_id
  //       right join maintaince_mtr m2 on m.id = m2.maintaince_id
  //       LEFT JOIN khs_maintenance i on m2.id_item=i.id_item
  //       WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q." order by m.tgl_selesai asc, m.sto asc
  //     ");
  //     // dd($maintenance2);
  //     $var_foto =array();
  //     foreach ($maintenance2 as $no => $m){
  //       if(!empty($m->no_tiket)){
  //         $sorthead[] = $m->no_tiket;
  //       }
  //     }
  //     $sorthead = array_unique($sorthead);
  //     $head = $sorthead;
  //     //endsort head
  //     $total = array();
  //     $totalmaterial = array();
  //     $totaljasa = array();
  //     foreach($head as $h){
  //       $total[$h] = 0;
  //       $totalmaterial[$h] = 0;
  //       $totaljasa[$h] = 0;
  //     }
  //     foreach ($maintenance as $no => $m){
  //       if($lastNama == $m->id_item){
  //         $data[count($data)-1]['no_tiket'][$m->no_tiket] = $m->qty;

  //         $totalmaterial[$m->no_tiket] += ($m->qty*$m->material);
  //         if($m->jenis_order == 4){
  //           $totaljasa[$m->no_tiket] += 0;
  //           $total[$m->no_tiket] += ($m->qty*$m->material);
  //         }
  //         else{
  //           $totaljasa[$m->no_tiket] += ($m->qty*$m->jasa);
  //           $total[$m->no_tiket] += ($m->qty*$m->jasa)+($m->qty*$m->material);
  //         }
  //         $no++;
  //       }else{
  //         $data[] = array("id_item" => $m->id_item, "satuan" => $m->satuan, "uraian" => $m->uraian, "jasa" => $m->jasa, "material" => $m->material, "total_boq" => $m->total_boq, "no_tiket" => array(), "total" => array());
  //         foreach($head as $h){
  //           if($h == $m->no_tiket){
  //             $data[count($data)-1]['no_tiket'][$h] = $m->qty;
  //             $totalmaterial[$h] += ($m->qty*$m->material);
  //             if($m->jenis_order == 4){
  //               $total[$h] += ($m->qty*$m->material);
  //               $totaljasa[$h] += 0;
  //             }else{
  //               $total[$h] += ($m->qty*$m->jasa)+($m->qty*$m->material);
  //               $totaljasa[$h] += ($m->qty*$m->jasa);
  //             }

  //           }else{
  //             $data[count($data)-1]['no_tiket'][$h] = 0;
  //             $data[count($data)-1]['total'][$h] = 0;
  //           }
  //         }
  //         $no=0;
  //       }
  //       $lastNama = $m->id_item;
  //     }
  //     // dd($head);
  //     $excel->sheet("boq", function($sheet) use($tgl, $jenis_rekon, $verif, $data, $head, $total, $totalmaterial, $totaljasa, $title){
  //       //dd($title);
  //       //return view('report.excelRincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
  //       $sheet->getStyle('B')->getAlignment()->setWrapText(true);

  //       $sheet->loadView('report.excelRincian', compact('data', 'head', 'total', 'totalmaterial', 'totaljasa', 'title'));
  //     });
  //     $mt = DB::select("SELECT m.*, (select count(*) from maintaince_mtr where maintaince_id=m.id) as mtr
  //       FROM maintaince m
  //       LEFT JOIN regu r ON r.id_regu = m.dispatch_regu_id
  //       WHERE m.isHapus = 0 AND m.tgl_selesai like '".$tgl."%' ".$q." GROUP BY no_tiket having mtr > 0 order by m.tgl_selesai asc, m.sto asc
  //     ");

  //     $jenis_order = $jenis_rekon;
  //     $excel->sheet("evidence", function($sheet) use($tgl, $jenis_order, $mt){
  //       $sheet->loadView('report.excelFoto', compact('mt'), ['jenis' => 'rincian revenue']);
  //     });

  //   foreach($mt as $k => $v)
  //   {
  //     $c_mt['data'][$k]['no_tiket'] = $v->no_tiket;
  //     $c_mt['data'][$k]['sto'] = $v->sto;
  //     $c_mt['data'][$k]['id'] = $v->id;
  //     $c_mt['data'][$k]['jenis_order'] = $v->jenis_order;
  //   }

  //   $c_mt['data'] = array_chunk($c_mt['data'], 10);
  //     $excel->sheet("Data Telkom", function($sheet) use($tgl, $jenis_order, $c_mt){
  //       $sheet->loadView('report.excelFoto_telkom', compact('c_mt'), ['jenis' => 'rincian revenue']);
  //     });
  //     $excel->sheet("evidence", function($sheet) use($tgl, $jenis_order, $mt){
  //       $sheet->loadView('report.excelFotoHorizon', compact('mt'));
  //     });
  //   })->download('xlsx');
  // }

}