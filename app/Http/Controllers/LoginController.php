<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\LoginModel;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\DA\User;
use App\DA\SSO;
use DB;

class LoginController extends Controller
{
    public function loginPage()
    {
        return view('newlogin');
    }

    public function login(Request $request)
    {
        $username  = $request->input('login');
        $password = $request->input('password');


        $result = DB::select('
          SELECT u.id_user, u.password, u.id_karyawan, u.u_nama As nama, u.level, u.maintenance_level, u.u_nama_instansi As nama_instansi,  u.jenis_verif, u.pixeltheme
          FROM user u
          WHERE id_user = ? AND password = MD5(?) and u.status = 0
        ', [
          $username,
          $password
        ]);
        // dd($result);
        if (count($result) == 1) {
            // $this->insertIfNoExist($username);
            $user = User::userExists($username);
            // $result[0]->witel = @$user->witel;
            // $result[0]->fiberzone = @$user->fiberzone;
            // $result[0]->level = $result[0]->level;
            // $result[0]->loginMethod = "WASAKA";
            Session::put('auth', $user);
            $this->ensureLocalUserHasRememberToken($user);
            return $this->successResponse($user->psb_remember_token);
        }
        return $this->failureResponse($request, $result);
    }
    public function loginAPI(Request $request)
    {
        $username  = $request->input('login');
        $password = $request->input('password');


        $result = DB::select('
          SELECT *
          FROM user u
          WHERE id_user = ? AND password = MD5(?) and u.status = 0
        ', [
          $username,
          $password
        ]);
        if (count($result) == 1) {
            $user = User::userExists($username);
            return response()->json(["state"=>"success","msg"=>"success!", "data"=>$user]);
        }
        return response()->json(["state"=>"failed","msg"=>"Login Gagal!", "data"=>null]);
    }
    public function getAkun(Request $request)
    {
        $user  = $request->input('user');
        $result = DB::table('akun')->where('user',$user)->first();
        if ($result) {
            return response()->json(["state"=>"success","msg"=>"success!", "data"=>$result]);
        }else{
            return response()->json(["state"=>"failed","msg"=>"Errorl!", "data"=>null]);
        }
        
    }
    // public function insertIfNoExist($nik){
    //   $user = User::userExists($nik);
    //   if(!count($user)){
    //     User::storeProfile([
    //       'nik' => $nik
    //     ]);
    //   }
    // }
    public function logout()
    {
        Session::forget('auth');
        return redirect('/login')->withCookie(cookie()->forever('presistent-token', ''));
    }

    private function ensureLocalUserHasRememberToken($localUser)
    {
        $token = $localUser->psb_remember_token;

        if (!$localUser->psb_remember_token) {
            $token = $this->generateRememberToken($localUser->id_user);
            User::updateProfileByNik($localUser->id_user, [
                'psb_remember_token' => $token
            ]);
            $localUser->psb_remember_token = $token;
        }

        return $token;
    }

    private function generateRememberToken($nik)
    {
        return md5($nik . microtime());
    }

    private function successResponse($rememberToken)
    {
        if (Session::has('auth-originalUrl')) {
            $url = Session::pull('auth-originalUrl');
        } else {
            $url = '/';
        }

        $response = redirect($url);
        if ($rememberToken) {
            $response->withCookie(cookie()->forever('presistent-token', $rememberToken));
            //$response->cookie('persistent-token', $rememberToken, 0, '', '', true, true);
        }

        return $response;
    }

    private function failureResponse(Request $request, $ssoResult)
    {
        // flash old input
        $request->flash();
        return redirect()->back()->with('alerts', [
                ['type' => 'danger', 'text' => 'GAGAL LOGIN']
            ]);
    }
    public static function cek_all_login_sso()
    {
        //ambil semua user
        $akuns = SSO::getAll();
        //cek semua user
        foreach($akuns as $no => $akun){
            $result = SSO::do_login($akun->user,$akun->pwd);
            SSO::updateStatus($akun->user, $result);
        }
        $data = SSO::getByUser('17920262');
        $data2 = SSO::getByUser('18940469');
        if($data->isExpired){
            self::sso_renewal($data->user);
        }

        if($data2->isExpired){
            self::sso_renewal($data2->user);
        }
    }
    public static function cek_login_sso_by_user($user)
    {
        $akun = SSO::getByUser($user);
        $result = SSO::do_login($akun->user,$akun->pwd);
        // dd($result);
        SSO::updateStatus($akun->user, $result);

    }
    public static function sso_activation(){
        $akuns = SSO::getByStatus(2);
        foreach($akuns as $no => $akun){
            if($akun->auto_relock){
                $url = 'https://apps.telkomakses.co.id/sso/unlock_user_sso.php';
                $data = 'nik=' . $akun->user . '&btn_unlock=AKTIFKAN';
                $ch = curl_init( $url );
                curl_setopt( $ch, CURLOPT_POST, 1);
                curl_setopt( $ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt( $ch, CURLOPT_HEADER, 0);
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                $response = curl_exec( $ch );
                curl_close($ch);
                // dd($response);
                self::cek_login_sso_by_user($akun->user);
            }
        }
        // https://apps.telkomakses.co.id/sso/unlock_user_sso.php
    }
    public static function sso_renewal($user){
        // https://apps.telkomakses.co.id/portal/login.php
        // https://apps.telkomakses.co.id/sso/change_password_.php
        $akun = SSO::getByUser($user);
        // dd($akun);
        $url = 'https://apps.telkomakses.co.id/portal/proses_login.php';
        $data = 'nik=' . $akun->user . '&password='.$akun->pwd.'&id_captcha=211&answer=integrity&submit=';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'sso.jar');
        curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec( $ch );

        $dom = @\DOMDocument::loadHTML(trim($response));
        $input = $dom->getElementsByTagName('input')->item(0)->getAttribute("value");
        curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/sso/change_password_.php');
        $data = 'changepwd='.$input;
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec( $ch );

        curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/sso/index.php');
        curl_setopt($ch, CURLOPT_POST, false);
        $response = curl_exec( $ch );

        curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/sso/change_password_.php');
        $data = 'changepwd=changepwd';
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec( $ch );

        $dom = @\DOMDocument::loadHTML(trim($response));
        $id = $dom->getElementsByTagName('input')->item(0)->getAttribute("value");
        $emp_id = $dom->getElementsByTagName('input')->item(1)->getAttribute("value");
        $passwd = $dom->getElementsByTagName('input')->item(2)->getAttribute("value");
        $seq = $dom->getElementsByTagName('input')->item(3)->getAttribute("value");
        $change_from_telkomakses = $dom->getElementsByTagName('input')->item(7)->getAttribute("value");
        $new_pwd = '@'.date('ymd').'wandiy';
        if ($user == '18940469') {
            $new_pwd = '@'.date('ymd').'rendy';
        }
        $data = 'id='.$id.'&emp_id='.$emp_id.'&passwd='.$passwd.'&seq='.$seq.'&old_pwd='.$akun->pwd.'&new_pwd='.$new_pwd.'&retype_pwd='.$new_pwd.'&change_from_telkomakses=change_from_telkomakses&btn_update=Update';
        curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/sso/change_password_.php');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec( $ch );

        curl_close($ch);
        SSO::setNewPwd($akun->user, $new_pwd);
        // dd($data);
    }
}
