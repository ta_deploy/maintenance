<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use DB;
use Telegram;
use App\DA\User;
use App\DA\Saldo;
use Validator;
use App\Http\Controllers\GraberController;

class OrderController extends Controller
{
    protected static $sendPhoto = [ 'RFC', 'ODP', 'Hasil_Ukur_OPM'];
    protected $photoGamas = ['RFC', 'Sebelum', 'Progress', 'Sesudah', 'Foto-GRID', 'Denah-Lokasi', 'Foto-Pemasangan-Slack-Baru_(Sisi_Pertama)', 'Foto-Pemasangan-Slack-Baru-(Sisi_Kedua)', 'Foto-Penyelekan-Kabel-Stang-Slack-Gamas-(Sisi_Pertama)', 'Foto-Penyelekan-Kabel-Stang-Slack-Gamas-(Sisi_Kedua)', 'Foto-Penggalian-Penanaman-Closure-Gamas_(Kabel_Tanah)', 'Foto-Penanda-Letak-Galian-Closure-Tanah'];
    protected static $sendPhotoPsb = [ 'RFC', 'ODP', 'Redaman_ODP'];
    protected $photoInputs = [ 'RFC',
      'ODP', 'Sebelum','Progress', 'Sesudah', 'Foto-GRID', 'BA-Remo', 'Denah-Lokasi', 'Pengukuran', 'Pelanggan_GRID', 'ODP-dan-Redaman-GRID'
    ];
    protected $photoBenjar = [ 'RFC',
      'ODC-Sebelum', 'ODC-Progress','ODC-Sesudah',
      'ODP-Sebelum', 'ODP-Progress','ODP-Sesudah',
      'Redaman-Sebelum', 'Redaman-Progress','Redaman-Sesudah',
      'Saluran-Penanggal-Sebelum', 'Saluran-Penanggal-Progress','Saluran-Penanggal-Sesudah',
      'Tiang-Sebelum', 'Tiang-Progress','Tiang-Sesudah','Foto-GRID'
    ];
    protected $photoIXSA = [
    'Foto-Peremajaan-Update-Layout-Node',
    'Foto-Pemeliharaan-Lantai-dan-Patok',
    'Foto-Kebersihan-Kerapian-Cabinet-ODC',
    'Foto-Pemeliharaan-Pemasangan-Core',
    'Foto-Pemeliharaan-Pemasangan-Parafin',
    'Foto-Pelabelan-Core',
    'Foto-Pengukuran-Core-Idle',
    'Foto-Melengkapi-Logbook-Node',
    'Foto-Gembok-Kunci-Pengaman-ODC',
    'Foto-Pelabelan-Nama-ODC',
    'Foto-Tampilan-ODC-Bagian-Luar',
    'Foto-Tampilan-ODC-Bagian-Dalam',
    ];
    protected static $photoIXSAst = [
    'Foto-Peremajaan-Update-Layout-Node',
    'Foto-Pemeliharaan-Lantai-dan-Patok',
    'Foto-Kebersihan-Kerapian-Cabinet-ODC',
    'Foto-Pemeliharaan-Pemasangan-Core',
    'Foto-Pemeliharaan-Pemasangan-Parafin',
    'Foto-Pelabelan-Core',
    'Foto-Pengukuran-Core-Idle',
    'Foto-Melengkapi-Logbook-Node',
    'Foto-Gembok-Kunci-Pengaman-ODC',
    'Foto-Pelabelan-Nama-ODC',
    'Foto-Tampilan-ODC-Bagian-Luar',
    'Foto-Tampilan-ODC-Bagian-Dalam',
    ];
    protected $photoReboundary = [ 'RFC',
    'Foto-Odp-Depan-Eksisting',
    'Foto-Odp-Dalam-Eksisting-Sebelum',
    'Foto-Odp-Dalam-Eksisting-Sesudah',
    'Foto-Redaman-Odp-Eksisting',
    'Foto-Progress',
    'Foto-Odp-Depan-(Rebondary)',
    'Foto-Odp-Dalam-Sebelum-(Rebondary)',
    'Foto-Odp-Dalam-Sesudah-(Rebondary)',
    'Foto-Redaman-Odp-(Rebondary)',
    'Pemasangan-Aksesoris',
    'Capture-Id-Valins-Awal',
    'Capture-Id-Valins-Rebondary',
    'Foto-GRID'
    ];
    protected static $photoGamasSt = ['RFC', 'Sebelum', 'Progress', 'Sesudah', 'Foto-GRID', 'Denah-Lokasi', 'Foto-Pemasangan-Slack-Baru_(Sisi_Pertama)', 'Foto-Pemasangan-Slack-Baru-(Sisi_Kedua)', 'Foto-Penyelekan-Kabel-Stang-Slack-Gamas-(Sisi_Pertama)', 'Foto-Penyelekan-Kabel-Stang-Slack-Gamas-(Sisi_Kedua)', 'Foto-Penggalian-Penanaman-Closure-Gamas_(Kabel_Tanah)', 'Foto-Penanda-Letak-Galian-Closure-Tanah'];
    protected static $photoReboundarySt = [ 'RFC',
    'Foto-Odp-Depan-Eksisting',
    'Foto-Odp-Dalam-Eksisting-Sebelum',
    'Foto-Odp-Dalam-Eksisting-Sesudah',
    'Foto-Redaman-Odp-Eksisting',
    'Foto-Progress',
    'Foto-Odp-Depan-(Rebondary)',
    'Foto-Odp-Dalam-Sebelum-(Rebondary)',
    'Foto-Odp-Dalam-Sesudah-(Rebondary)',
    'Foto-Redaman-Odp-(Rebondary)',
    'Pemasangan-Aksesoris',
    'Capture-Id-Valins-Awal',
    'Capture-Id-Valins-Rebondary',
    'Foto-GRID'
    ];
    protected $photophotoUtilitas = [ 'RFC',
      'Kondisi-Sebelum(dekat)',
      'Kondisi-Sebelum(jauh)',
      'Progres-1',
      'Progres-2',
      'Progres-3',
      'Progres-4',
      'Pengecoran-Sebelum(dekat)',
      'Pengecoran-Sebelum(jauh)',
      'Pemasangan-Aksesoris-Tiang-Sebelum(dekat)',
      'Pemasangan-Aksesoris-Tiang-Sebelum(jauh)',
      'Pengecoran-Sesudah(dekat)',
      'Pengecoran-Sesudah(jauh)',
      'Pemasangan-Aksesoris-Tiang-Sesudah(dekat)',
      'Pemasangan-Aksesoris-Tiang-Sesudah(jauh)',
      'Kondisi-Sesudah(dekat)',
      'Kondisi-Sesudah(jauh)'
    ];
    protected $photophotoUtilitasSt = [ 'RFC',
      'Kondisi-Sebelum(dekat)',
      'Kondisi-Sebelum(jauh)',
      'Progres-1',
      'Progres-2',
      'Progres-3',
      'Progres-4',
      'Pengecoran-Sebelum(dekat)',
      'Pengecoran-Sebelum(jauh)',
      'Pemasangan-Aksesoris-Tiang-Sebelum(dekat)',
      'Pemasangan-Aksesoris-Tiang-Sebelum(jauh)',
      'Pengecoran-Sesudah(dekat)',
      'Pengecoran-Sesudah(jauh)',
      'Pemasangan-Aksesoris-Tiang-Sesudah(dekat)',
      'Pemasangan-Aksesoris-Tiang-Sesudah(jauh)',
      'Kondisi-Sesudah(dekat)',
      'Kondisi-Sesudah(jauh)'
    ];
    protected $photoNormalisasi = [ 'RFC',
    'ODP-Gendong',
    'Progress',
    'Dalam-ODP-Gendong',
    'Redaman-IN-OUT-Sebelum',
    'Redaman-IN-OUT-Sesudah',
    'ODP-New',
    'Dalam-ODP-New',
    'Penurunan-DC',
    'Foto-Rumah-Pelanggan-GRID',
    'Redaman-OUT',
    'Redaman-OUT-dari-ODP',
    'Redaman-DC-Pelanggan-Sebelum',
    'Redaman-DC-Pelanggan-Sesudah',
    'Foto-Splitter-Diturunkan',
    'Foto-DC-Tambah-Klem-Spiral',
    'Foto-DC-Masuk-Pipa-PVC',
    'Foto-Belakang-ODP'
    ];
    protected static $photoNormalisasiSt = [ 'RFC',
    'ODP-Gendong',
    'Progress',
    'Dalam-ODP-Gendong',
    'Redaman-IN-OUT-Sebelum',
    'Redaman-IN-OUT-Sesudah',
    'ODP-New',
    'Dalam-ODP-New',
    'Penurunan-DC',
    'Foto-Rumah-Pelanggan-GRID',
    'Redaman-OUT',
    'Redaman-OUT-dari-ODP',
    'Redaman-DC-Pelanggan-Sebelum',
    'Redaman-DC-Pelanggan-Sesudah',
    'Foto-Splitter-Diturunkan',
    'Foto-DC-Tambah-Klem-Spiral',
    'Foto-DC-Masuk-Pipa-PVC',
    'Foto-Belakang-ODP'
    ];
    protected $photoValidasi = [ 'RFC',
      'ODP-Sebelum', 'ODP-Progress','ODP-Sesudah',
      'Redaman-Sebelum', 'Redaman-Progress','Redaman-Sesudah',
      'Saluran-Penanggal-Sebelum', 'Saluran-Penanggal-Progress','Saluran-Penanggal-Sesudah',
      'Tiang-Sebelum', 'Tiang-Progress','Tiang-Sesudah', 'Foto-GRID'
    ];
    protected static $photoValidasiSt = [ 'RFC',
      'ODP-Sebelum', 'ODP-Progress','ODP-Sesudah',
      'Redaman-Sebelum', 'Redaman-Progress','Redaman-Sesudah',
      'Saluran-Penanggal-Sebelum', 'Saluran-Penanggal-Progress','Saluran-Penanggal-Sesudah',
      'Tiang-Sebelum', 'Tiang-Progress','Tiang-Sesudah', 'Foto-GRID'
    ];
    protected $photoOdpSehat = [ 'RFC',
      'ODP-Sebelum', 'ODP-Progress','ODP-Sesudah',
      'Redaman-Sebelum', 'Redaman-Progress','Redaman-Sesudah',
      'Saluran-Penanggal-Sebelum', 'Saluran-Penanggal-Progress','Saluran-Penanggal-Sesudah',
      'Tiang-Sebelum', 'Tiang-Progress','Tiang-Sesudah', 'Foto-GRID'
    ];
    protected static $photoOdpSehatst = [ 'RFC',
      'ODP-Sebelum', 'ODP-Progress','ODP-Sesudah',
      'Redaman-Sebelum', 'Redaman-Progress','Redaman-Sesudah',
      'Saluran-Penanggal-Sebelum', 'Saluran-Penanggal-Progress','Saluran-Penanggal-Sesudah',
      'Tiang-Sebelum', 'Tiang-Progress','Tiang-Sesudah', 'Foto-GRID'
    ];
    protected $photoRemo = [ 'RFC', 'ODP-dan-Redaman-GRID', 'Sebelum', 'Progress', 'Sesudah', 'Pengukuran', 'Pelanggan_GRID', 'Foto-GRID', 'Redaman-OPM-Dropcore', 'Foto-Dalam-ODP'];
    protected $photoCommon = [ 'RFC', 'Sebelum', 'Progress', 'Sesudah', 'Foto-GRID', 'Denah-Lokasi'];
    protected $photoOdpLoss = [ 'RFC', 'ODP', 'Sebelum', 'Progress', 'Sesudah', 'Foto-GRID'];
    protected $photoODCSehat = [ 'RFC',
      'Foto_ODC_Depan_Sebelum', 'Foto_ODC_Depan_Sesudah','Foto_ODC_Dalam_Sebelum',
      'Foto_ODC_Dalam_Sesudah', 'Sebelum', 'Progress', 'Sesudah', 'Foto-GRID'
    ];
    protected static $photoInputst = [ 'RFC',
      'ODP', 'Sebelum', 'Progress', 'Sesudah', 'Foto-GRID', 'BA-Remo', 'Denah-Lokasi', 'Pengukuran', 'Pelanggan_GRID', 'ODP-dan-Redaman-GRID'
    ];
    protected static $photoBenjarSt = [ 'RFC',
      'ODC-Sebelum', 'ODC-Progress','ODC-Sesudah',
      'ODP-Sebelum', 'ODP-Progress','ODP-Sesudah',
      'Redaman-Sebelum', 'Redaman-Progress','Redaman-Sesudah',
      'Saluran-Penanggal-Sebelum', 'Saluran-Penanggal-Progress','Saluran-Penanggal-Sesudah',
      'Tiang-Sebelum', 'Tiang-Progress','Tiang-Sesudah','Foto-GRID'
    ];
    public function refer(Request $req){
      //dd($req->all());
      DB::table('maintaince')->where('id', $req->id_maint)->update([
        "refer" => $req->refer
      ]);
      return redirect()->back();
    }
    public function filter($sd, $ed, $sts, $jns)
    {
      $where = '';
      $tgl = '';
      if($sts)
        $where .= 'mt.status = "'.$sts.'" and ';
      if($sd){
        $tgl = 'mt.created_at like "%'.$sd.'%"';
      }
      if($ed){
        $tgl = '(mt.created_at between "'.$sd.' 00:00:01" and "'.$ed.' 23:59:59")';
      }
      $data = DB::select('SELECT mt.*, mt.id as id_mt, mt.nama_odp as nama_dp,
      mr.nama1,
      mr.nama2,
      (select COUNT(id_regu) FROM regu WHERE mt.dispatch_regu_id = id_regu AND id_regu = mt.dispatch_regu_id (nik1 = "'.session('auth')->id_user.'" OR nik2 = "'.session('auth')->id_user.'") ) As check_regu
      from maintaince mt
      left join regu mr on mt.dispatch_regu_id = mr.id_regu
      where '.$where.''.$tgl.' and mt.jenis_order='.$jns.' AND mt.isHapus = 0 order by tgl_selesai desc');
      return view('order.filter', compact('data'));
    }
    public function input($id)
    {
      $data = null;
      $regu = DB::table('regu')
      ->select('*','id_regu as id', 'uraian as text')
      ->whereNotNull('uraian')
      ->where('label_regu', '!=', '')
      ->get();
      // dd(json_encode($regu));
      $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();
      $sektor = DB::table('maintenance_sector')->select('label as id', 'sector as text')->get();
      $jenis_order = DB::table('maintenance_jenis_order')->where('aktif',1)->get();

      $tim_jointer = DB::table('user As u')
      ->Leftjoin('1_2_employee As emp', 'u.id_user', '=', 'emp.nik')
      ->select('u.id_user', 'emp.nama')
      ->where('is_jointer', 1)
      ->whereNotNull('emp.mitra_amija')
      ->get();

      return view('order.input', compact('data', 'regu', 'jenis_order', 'sto', 'sektor', 'tim_jointer') );
    }
    public function input_Nog($id)
    {
      $data = DB::table('nog_master')->where('id', $id)->first();

      if(!in_array($data->tindak_lanjut, ['PT-1']) )
      {
        return redirect()->back()->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Tidak Bisa Diakses']
        ]);
      }
      $regu = DB::table('regu')
      ->select('*','id_regu as id', 'uraian as text')
      ->whereNotNull('uraian')
      ->where('label_regu', '!=', '')
      ->get();
      // dd(json_encode($regu));
      $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();
      $sektor = DB::table('maintenance_sector')->select('label as id', 'sector as text')->get();
      $jenis_order = DB::table('maintenance_jenis_order')->where('aktif',1)->get();
      // dd($jenis_order);
      return view('order.input', compact('data', 'regu', 'jenis_order', 'sto', 'sektor') );
    }
    public function listRemo($id)
    {
      if($id == "sisa"){
        $data = DB::select('SELECT *,m.id as id_mt, (select COUNT(id_regu) FROM regu WHERE m.dispatch_regu_id = id_regu AND id_regu = mt.dispatch_regu_id (nik1 = "'.session('auth')->id_user.'" OR nik2 = "'.session('auth')->id_user.'") ) As check_regu from remo_servo rs left join maintaince m on rs.NO_SPEEDY=m.no_tiket where m.isHapus = 0 AND m.status is null');
      }else if($id == "all"){
        $data = DB::select('SELECT *,m.id as id_mt, (select COUNT(id_regu) FROM regu WHERE m.dispatch_regu_id = id_regu AND id_regu = mt.dispatch_regu_id (nik1 = "'.session('auth')->id_user.'" OR nik2 = "'.session('auth')->id_user.'") ) As check_regu from remo_servo rs left join maintaince m on rs.NO_SPEEDY=m.no_tiket where m.isHapus = 0 AND 1');
      }else{
        $data = DB::select('SELECT *,m.id as id_mt, (select COUNT(id_regu) FROM regu WHERE m.dispatch_regu_id = id_regu AND id_regu = mt.dispatch_regu_id (nik1 = "'.session('auth')->id_user.'" OR nik2 = "'.session('auth')->id_user.'") ) As check_regu from maintaince m left join remo_servo rs on m.no_tiket=rs.NO_SPEEDY where m.isHapus = 0 AND m.jenis_order=4 and m.status = "'.$id.'" and m.tgl_selesai like "%'.date('Y-m').'%"');
      }
      $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();
      $regu = DB::table('regu')->select('*', 'id_regu as id', 'uraian as text')->get();
      return view('order.listRemo', compact('data', 'regu', 'sto'));
    }
    public function listRemoClose()
    {
      $data = DB::table('maintaince')
      ->where('status', 'close')
      ->where('verifHd', 0)
      ->where('jenis_order', 4)
      ->where('isHapus', 0)
      ->get();
      return view('order.listverifremo', compact('data'));
    }
    public function verifRemo(Request $req)
    {
      $auth = session('auth');
      DB::table('maintaince')->where('id', $req->id_maint)->update([
                "verifHd" => $req->redaman
      ]);
      DB::table('maintenance_verifhd_history')->insert([
                "maintenance_id" => $req->id_maint,
                "verif" => $req->redaman,
                "updated_nik" => $auth->id_karyawan,
                "updated_name" => $auth->nama
      ]);
      return redirect()->back();
    }
    public function getJsonMaint($id)
    {
       return json_encode(DB::table('maintaince')
          ->select('maintaince.*', 'psb_laporan.kordinat_odp', 'psb_laporan.nama_odp', 'maintaince.nama_odp as nama_dp')
          ->leftJoin('psb_laporan', 'maintaince.psb_laporan_id', '=', 'psb_laporan.id')
          ->where([
            ['maintaince.id', $id],
            ['maintaince.isHapus', 0]
          ])->first());
    }
    public function getJsonRemo($id)
    {
      $mt = DB::table('maintaince')
          ->select('maintaince.*', 'psb_laporan.kordinat_odp', 'psb_laporan.nama_odp', 'maintaince.nama_odp as nama_dp')
          ->leftJoin('psb_laporan', 'maintaince.psb_laporan_id', '=', 'psb_laporan.id')
          ->where([
            ['maintaince.no_tiket', $id],
            ['maintaince.isHapus', 0]
          ])->first();
      return json_encode($mt);
    }
    public function dispatchOrderRemo(Request $req)
    {
      $sto = DB::table('maintenance_datel')->where('sto', $req->sto)->first();
      $auth = session('auth');
      $regu = DB::table('regu')->where('id_regu', $req->regu)->first();
      $maint = DB::table('maintaince')->where([
        ['no_tiket', $req->no_tiket],
        ['isHapus', 0]
      ])->first();
      if(count($maint)){
        DB::table('maintaince')->where([
          ['no_tiket', $req->no_tiket],
          ['isHapus', 0]
        ])->update([
                  "dispatch_regu_id"  =>$req->regu,
                  "dispatch_regu_name"=>$regu->uraian,
                  "username1"         =>$regu->nik1,
                  "username2"         =>$regu->nik2,
                  "username3"         =>$regu->nik3,
                  "username4"         =>$regu->nik4,
                  "modified_at"       =>DB::raw('now()'),
                  "modified_by"       =>$auth->id_karyawan,
                  "sto"               =>$sto->sto,
                  "kandatel"          =>$sto->datel
        ]);
        $idmt = $req->id_mt;
      }else{
        $idremo = DB::table('maintaince')->insertGetId([
                  "no_tiket"          =>$req->no_tiket,
                  "pic"               =>$req->pic,
                  "alamat"            =>$req->alamat,
                  "koordinat"         =>$req->koordinatodp,
                  "jenis_order"       =>4,
                  "nama_order"        =>"REMO",
                  "created_at"        =>DB::raw('now()'),
                  "created_by"        =>$auth->id_karyawan,
                  "dispatch_regu_id"  =>$req->regu,
                  "dispatch_regu_name"=>$regu->uraian,
                  "username1"         =>$regu->nik1,
                  "username2"         =>$regu->nik2,
                  "username3"         =>$regu->nik3,
                  "username4"         =>$regu->nik4,
                  "modified_at"       =>DB::raw('now()'),
                  "modified_by"       =>$auth->id_karyawan,
                  "sto"               =>$sto->sto,
                  "kandatel"          =>$sto->datel
                  ]);
        $idmt = $idremo;
      }
      // DB::table('maintenance_dispatch_log')->insert([
      //           "maintenance_id"  =>$idmt,
      //           "regu_id"         =>$req->regu,
      //           "regu_name"       =>$regu->uraian,
      //           "ts_dispatch"     =>DB::raw('now()'),
      //           "dispatch_by"     =>$auth->id_karyawan,
      //           "action"     =>"dispatch"
      // ]);
      DB::table('maintenance_note')
      ->insert([
        'mt_id'     => $idmt,
        'catatan'   => 'DISPATCH REMO',
        'status'   => 'DISPATCH REMO',
        'pelaku'    => $auth->id_karyawan,
        'ts'     => DB::raw('NOW()')
      ]);
      exec('cd ..;php artisan sendDispatch '.$idmt.' > /dev/null &');
      return "<span class='label label-success'>success</span>";
    }
    public function getJsonPsb($id){
        return json_encode(DB::table('psb_laporan')
          ->where('id', $id)->first());
    }
    public function dispatchOrder(Request $req)
    {
      //$req->id_mt
      //$req->regu
      $sto = DB::table('maintenance_datel')->where('sto', $req->sto)->first();
      $auth = session('auth');
      $regu = DB::table('regu')->where('id_regu', $req->regu)->first();

      DB::table('maintaince')->where('id', $req->id_mt)->update([
                "dispatch_regu_id"  =>$req->regu,
                "dispatch_regu_name"=>$regu->uraian,
                "username1"         =>$regu->nik1,
                "username2"         =>$regu->nik2,
                "modified_at"       =>DB::raw('now()'),
                "modified_by"       =>$auth->id_karyawan,
                "sto"               =>$sto->sto,
                "kandatel"          =>$sto->datel
      ]);
      // DB::table('maintenance_dispatch_log')->insert([
      //           "maintenance_id"  =>$req->id_mt,
      //           "regu_id"         =>$req->regu,
      //           "regu_name"       =>$regu->uraian,
      //           "ts_dispatch"     =>DB::raw('now()'),
      //           "dispatch_by"     =>$auth->id_karyawan,
      //           "action"     =>"dispatch"
      // ]);
      DB::table('maintenance_note')
      ->insert([
        'mt_id'     => $req->id_mt,
        'catatan'   => 'DISPATCH',
        'status'   => 'DISPATCH',
        'pelaku'    => $auth->id_karyawan,
        'ts'     => DB::raw('NOW()')
      ]);

      $main = DB::table('maintaince')->where('id','=',$req->id_mt)->first();
      /*
      DB::table('dispatch_teknisi')
        ->where('Ndem',$main->no_tiket)
        ->whereNull('id_regu')
        ->update([
          'updated_at'          => DB::raw('now()'),
          'tgl'                 => DB::raw('now()'),
          'updated_by'          => $auth->id_karyawan,
          'step_id'             => '1.0',
          'maintenance_regu_id' => @$regu->id
      ]);
      */
      exec('cd ..;php artisan sendDispatch '.$req->id_mt.' > /dev/null &');
      return "<span class='label label-success'>success</span>";
    }
    public function deleteOrder(Request $req){
      $mt = DB::table('maintaince')->where('id', $req->id_mt)->first();
      if($mt->status != "close"){
        if($mt->psb_laporan_id){
          DB::table('psb_laporan')->where('id', $mt->psb_laporan_id)->update([
            "checked_at" => "",
            "checked_by" => ""
            ]);
        }
        DB::table('maintaince')->where('id', $req->id_mt)->update([
          'isHapus' => 1
        ]);
        // DB::table('maintenance_dispatch_log')->insert([
        //         "maintenance_id"  =>$req->id_mt,
        //         "ts_dispatch"     =>DB::raw('now()'),
        //         "dispatch_by"     =>session('auth')->id_karyawan,
        //         "action"     =>  "delete ".$mt->no_tiket
        //   ]);
        DB::Table('nog_master')->where('mt_id', $req->id_mt)->delete();
        DB::table('maintenance_note')
        ->insert([
          'mt_id'     => $req->id_mt,
          'catatan'   => "DELETE ".$mt->no_tiket,
          'status'   => "DELETE ".$mt->no_tiket,
          'pelaku'    => session('auth')->id_karyawan,
          'ts'     => DB::raw('NOW()')
        ]);
        return "success mendelete!";
      }else{
        return "gagal mendelete, order sudah close!";
      }
    }
    public function ajax_matrik($id, $datel, $tgl){
      if($datel == 'all'){
        $data = DB::select("select * from (
          select md.*,
            (select count(mt.id) from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where mt.isHapus = 0 AND mt.jenis_order = ".$id." and mt.kandatel = md.datel and mt.status = 'close' and refer is null and mt.tgl_selesai like '%".$tgl."%') as close,
            (select count(mt.id) from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where mt.isHapus = 0 AND mt.jenis_order = ".$id." and mt.kandatel = md.datel and mt.status = 'kendala pelanggan' and refer is null and mt.tgl_selesai like '%".$tgl."%') as kendala_pelanggan,
            (select count(mt.id) from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where mt.isHapus = 0 AND mt.jenis_order = ".$id." and mt.kandatel = md.datel and mt.status = 'kendala teknis' and refer is null and mt.tgl_selesai like '%".$tgl."%') as kendala_teknis,
            (select count(mt.id) from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where mt.isHapus = 0 AND mt.jenis_order = ".$id." and mt.kandatel = md.datel and mt.status is null and mt.dispatch_regu_id is not null and mt.dispatch_regu_id !=0 and refer is null) as no_update,
            (select count(mt.id) from maintaince mt where mt.isHapus = 0 AND mt.jenis_order = ".$id." and mt.kandatel = md.datel and (mt.dispatch_regu_id is null or mt.dispatch_regu_id=0) and refer is null) as undisp
          from maintenance_so md where 1 order by datel asc) subq
          where kendala_pelanggan>0 or kendala_teknis>0 or no_update>0 or undisp>0
        ");
      }else{
        $data = DB::select("select * from (
          select md.*,
            (select count(mt.id) from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where mt.isHapus = 0 AND mt.jenis_order = ".$id." and mt.sto = md.sto and mt.status = 'close' and refer is null and mt.tgl_selesai like '%".$tgl."%') as close,
            (select count(mt.id) from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where mt.isHapus = 0 AND mt.jenis_order = ".$id." and mt.sto = md.sto and mt.status = 'kendala pelanggan' and refer is null and mt.tgl_selesai like '%".$tgl."%') as kendala_pelanggan,
            (select count(mt.id) from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where mt.isHapus = 0 AND mt.jenis_order = ".$id." and mt.sto = md.sto and mt.status = 'kendala teknis' and refer is null and mt.tgl_selesai like '%".$tgl."%') as kendala_teknis,
            (select count(mt.id) from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where mt.isHapus = 0 AND mt.jenis_order = ".$id." and mt.sto = md.sto and mt.status is null and mt.dispatch_regu_id is not null and mt.dispatch_regu_id !=0 and refer is null) as no_update,
            (select count(mt.id) from maintaince mt where mt.isHapus = 0 AND mt.jenis_order = ".$id." and mt.sto = md.sto and (mt.dispatch_regu_id is null or mt.dispatch_regu_id=0) and refer is null) as undisp
          from maintenance_datel md where 1 order by datel asc) subq
          where kendala_pelanggan>0 or kendala_teknis>0 or no_update>0 or undisp>0
        ");
      }
      // $psb = DB::select("SELECT m.id, m.status_psb, m.nama_order as jenis_order,
      //   (select count(d.id) from dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket left join psb_laporan pl on d.id=pl.id_tbl_mj where mc.id is null and (pl.status_laporan = m.status_psb OR pl.status_laporan IN(112) ) and pl.isCutOff is null) as xcheck
      //   from maintenance_jenis_order m where m.id = 3")[0];
      // $odfullpsb = DB::select("SELECT m.id, m.status_psb, m.nama_order as jenis_order,
      //   (select count(d.id) from dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket left join psb_laporan pl on d.id=pl.id_tbl_mj where mc.id is null and (pl.status_laporan = m.status_psb OR pl.status_laporan IN(113) ) and pl.isCutOff is null) as xcheck
      //   from maintenance_jenis_order m where m.id = 9")[0];
      // $insertpsb = DB::select("SELECT m.id, m.status_psb, m.nama_order as jenis_order,
      //   (select count(d.id) from dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket left join psb_laporan pl on d.id=pl.id_tbl_mj where mc.id is null and pl.status_laporan = m.status_psb and pl.isCutOff is null) as xcheck
      //   from maintenance_jenis_order m where m.id = 6")[0];
      return view('order.ajaxMatrik', compact('data', 'id'));
    }
    public function orderList($tgl, Request $req)
    {
      $datel = DB::table('maintenance_datel')->select('datel as id', 'datel as text')->groupBy('datel')->get();
      //untuk matrix
      $data = DB::select("SELECT m.id, m.nama_order AS jenis_order,
      SUM(CASE WHEN mt.status = 'close' AND refer IS NULL AND mt.tgl_selesai LIKE '%$tgl%' THEN 1 ELSE 0 END) AS close,
      SUM(CASE WHEN mt.status = 'kendala pelanggan' AND mt.tgl_selesai LIKE '%$tgl%' AND refer IS NULL THEN 1 ELSE 0 END) AS kendala_pelanggan,
      SUM(CASE WHEN mt.status = 'kendala teknis' AND mt.tgl_selesai LIKE '%$tgl%' AND refer IS NULL THEN 1 ELSE 0 END) AS kendala_teknis,
      SUM(CASE WHEN mt.status IS NULL AND mt.dispatch_regu_id IS NOT NULL AND mt.dispatch_regu_id != 0 AND refer IS NULL THEN 1 ELSE 0 END) AS no_update,
      SUM(CASE WHEN refer IS NULL AND (mt.dispatch_regu_id IS NULL OR mt.dispatch_regu_id = 0) THEN 1 ELSE 0 END) AS undisp
      FROM maintaince mt
      LEFT JOIN maintaince_progres mp ON mt.id = mp.maintaince_id
      LEFT JOIN maintenance_jenis_order m ON mt.jenis_order = m.id
      WHERE mt.isHapus = 0 AND m.id IS NOT NULL AND m.id NOT IN (12, 13) AND m.aktif = 1
      GROUP BY m.nama_order");
      // $psb = DB::select("SELECT m.id, m.status_psb, m.nama_order as jenis_order,
      //   (select count(d.id) from dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket left join psb_laporan pl on d.id=pl.id_tbl_mj where mc.id is null and (pl.status_laporan = m.status_psb OR pl.status_laporan IN(112) ) and pl.isCutOff is null) as xcheck
      //   from maintenance_jenis_order m where m.id = 3
      //   ")[0];
      // $odpfullpsb = DB::select("SELECT m.id, m.status_psb, m.nama_order as jenis_order,
      //   (select count(d.id) from dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket left join psb_laporan pl on d.id=pl.id_tbl_mj where mc.id is null and pl.status_laporan IN (24, 113) and pl.isCutOff is null) as xcheck
      //   from maintenance_jenis_order m where m.id = 9
      //   ")[0];
      // $insertpsb = DB::select("SELECT m.id, m.status_psb, m.nama_order as jenis_order,
      //   (select count(d.id) from dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket left join psb_laporan pl on d.id=pl.id_tbl_mj where mc.id is null and pl.status_laporan = m.status_psb and pl.isCutOff is null) as xcheck
      //   from maintenance_jenis_order m where m.id = 6
      //   ")[0];
      return view('order.matrix', compact('data', 'datel'));
    }

    public function orderdetail($jns, $sts, $tgl, $kandatel, Request $req)
    {
      $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();
      $datel = DB::table('maintenance_datel')->select('datel as id', 'datel as text')->groupBy('datel')->get();
      //untuk search search
      if ($req->has('q')) {
        $regu = DB::table('regu')
          ->select('id_regu as id', 'uraian as text', 'nama1', 'nama2')
          ->where([
              ['jenis_regu', 'marina'],
              ['uraian', '!=', null],
              ['label_regu', '!=', '']
            ])
          ->get();

        $data = DB::select("SELECT mt.*, mt.id as id_mt, mt.nama_odp, mt.nama_odp as nama_dp, mt.koordinat as kordinat_odp,
          mr.nama1,
          mr.nama2,
          (select count(id) from maintaince where refer = mt.id) as jml_anak,
          (select id_regu FROM regu WHERE mt.dispatch_regu_id = id_regu and( nik1 = '".session('auth')->id_user."' OR nik2 = '".session('auth')->id_user."') ) As check_regu
          from maintaince mt
          left join regu mr on mt.dispatch_regu_id = mr.id_regu
          where mt.isHapus = 0 AND( (refer is null and mt.no_tiket like '%".$req->q."') or (mt.no_tiket like '%".$req->q."' OR mt.nama_odp like '%".
          $req->q."')) order by tgl_selesai desc");
        if(count($data)>0 && $data[0]->refer != null){

          $refer=$data[0]->refer;
          $data = DB::select("SELECT mt.*, mt.id as id_mt, mt.nama_odp as nama_dp, mt.nama_odp, mt.koordinat as kordinat_odp,
          mr.nama1,
          mr.nama2,
          (select count(id) from maintaince where refer = mt.id) as jml_anak,
          (select id_regu FROM regu WHERE mt.dispatch_regu_id = id_regu AND nik1 = '".session('auth')->id_user."' OR nik2 = '".session('auth')->id_user."') As check_regu
          from maintaince mt
          left join regu mr on mt.dispatch_regu_id = mr.id_regu
          where mt.isHapus = 0 AND mt.id='".$refer."'");
        }
      }else{
        $regu = DB::table('regu')
          ->select('id_regu as id', 'uraian as text', 'nama1', 'nama2')
          ->where([
              ['jenis_regu', 'marina'],
              ['uraian', '!=', null],
              ['label_regu', '!=', '']
            ])
          ->get();

            //untuk list
            $sql = "";
            $jenis_order = "";
            $sql = " mt.status = '".$sts."'";
            if($jns != 'all')
              $jenis_order = " and (mt.jenis_order = '".$jns."')";
            if($sts == 'null')
              $sql = " mt.status is ".$sts." and mt.dispatch_regu_id is not null and mt.dispatch_regu_id !=0";
            else if($sts == 'undisp')
              $sql = " (mt.dispatch_regu_id is null or mt.dispatch_regu_id=0)";
            else if($sts == 'all')
              $sql = " 1 ";
            if ($sts == 'close'){
              $sql .= " and mt.tgl_selesai like '%".$tgl."%'";
            }
            $qdatel = "";
            if($kandatel != "all")
              $qdatel = " and mt.kandatel='".$kandatel."' ";
            $qtgl = "";
            if($tgl!="all")
              $qtgl = " and mt.tgl_selesai like '%".$tgl."%'";
            $data = DB::select("SELECT mt.*, mt.id as id_mt, mt.nama_odp as nama_dp, mt.nama_odp, mt.koordinat kordinat_odp,
              mr.nama1,
              mr.nama2,
              (select count(id) from maintaince where refer = mt.id) as jml_anak,
              (select id_regu FROM regu WHERE mt.dispatch_regu_id = id_regu AND nik1 = '".session('auth')->id_user."' OR nik2 = '".session('auth')->id_user."') As check_regu
              from maintaince mt
              left join regu mr on mt.dispatch_regu_id = mr.id_regu
              where mt.isHapus = 0 AND refer is null and ".$sql." ".$jenis_order." ".$qtgl." ".$qdatel." order by tgl_selesai desc");
        }

      return view('order.list', compact('data', 'regu', 'sto', 'datel'));
    }

    public function orderdetail_crossover($jns, $sts, $tgl, $from)
    {
      $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();
      $datel = DB::table('maintenance_datel')->select('datel as id', 'datel as text')->groupBy('datel')->get();
        $regu = DB::table('regu')
          ->select('uraian as id', 'uraian as text', DB::raw('(select u_nama As nama from user where id_karyawan = regu.nik1) as nama1, (select u_nama As nama from user where id_karyawan = regu.nik2) as nama2'))
          ->where([
              ['jenis_regu', 'marina'],
              ['uraian', '!=', null]
            ])
          ->get();

            //untuk list
            $jenis_order = " AND (mt.jenis_order IN (3, 6) )";
            if($jns != 'all'){
              $jenis_order = " AND (mt.jenis_order = '$jns')";
            }

            $qtgl ='';

            if(!in_array($tgl,['all', 'null'])){
              $qtgl = " AND mt.tgl_selesai like '$tgl%'";
            }

            if ($from != 'null') {
              $qfrom = "AND order_from = '$from'";
            } else {
              $qfrom = "AND order_from IS NULL";
            }

            if ($sts == 'close_val' ) {
              $qsts = "AND mt.status = 'close' AND mv.maintenance_id IS NOT NULL AND (mv.sisa_port != 0 OR port_idle IS NOT NULL AND port_idle != 0)";
            } elseif ($sts == 'close_notval') {
              $qsts = "AND mt.status = 'close' AND mv.maintenance_id IS NULL";
            }

            $group_valins = $kaps_valins = $moresql = '';

            if (!in_array($sts, ['close_val', 'close_notval'])) {
              if ($sts != 'null') {
                $qsts = "AND mt.status = '$sts'";
              } else {
                $qsts = "AND mt.status IS NULL AND mt.dispatch_regu_id IS NOT NULL AND mt.dispatch_regu_id != 0";
              }
            } else {
              $moresql = "LEFT JOIN maintenance_valins mv ON mt.id = mv.maintenance_id";
            }

            if (in_array($sts, ['close', 'close_val'])){
              $moresql = "LEFT JOIN maintenance_valins mv ON mt.id = mv.maintenance_id";
              $group_valins =' GROUP BY mt.id';
              $kaps_valins ='SUM(kapasitas) as kaps_vals,';
            }

            $data = DB::select("SELECT mt.*, mt.koordinat as kordinat_odp, mt.id AS id_mt, mt.nama_odp,mt.nama_odp AS nama_dp,
              (SELECT u_nama As nama FROM user WHERE id_karyawan = mt.username1) AS nama1,
              (SELECT u_nama As nama FROM user WHERE id_karyawan = mt.username2) AS nama2,
              (select id_regu FROM regu WHERE mt.dispatch_regu_id = id_regu AND nik1 = '".session('auth')->id_user."' OR nik2 = '".session('auth')->id_user."') As check_regu,
              $kaps_valins
              (SELECT count(id) FROM maintaince WHERE refer = mt.id) AS jml_anak
              FROM maintaince mt
              LEFT JOIN maintaince_progres mp ON mt.id = mp.maintaince_id
              $moresql
              WHERE mt.isHapus = 0 AND refer IS NULL $qfrom $qsts $jenis_order $qtgl $group_valins ORDER BY tgl_selesai DESC");

      return view('order.list', compact('data', 'regu', 'sto', 'datel'));
    }

    public function ajax_refer(Request $req){
    $term = trim($req->searchTerm);

    if (empty($term)) {
      return \Response::json([]);
    }

    $fetchData = DB::table('maintaince')
    ->Where('no_tiket', 'like', '%' .$term. '%')
    ->where('isHapus', 0)
    ->whereIn('jenis_order', [2,3,4,9])
    ->whereNull('refer')
    ->get();
    foreach ($fetchData as $row) {
      $formatted_tags[] = [
      "id"=> $row->id,
      "text"=> "<div><span class='label label-default'>$row->no_tiket</span>$row->nama_order<br><span class='label label-info'>$row->status</span><br><span class='label label-info'>$row->nama_odp</span></div>",
      "title"=> $row->no_tiket
    ];
    }

    return \Response::json($formatted_tags);
    }
    public function listodpfulltommanpsb(){
      $data = DB::select("select pls.laporan_status,pl.nama_odp, pl.kordinat_odp, pl.kordinat_pelanggan, d.id as id_dispatch, d.Ndem as no_tiket,
                 pl.kordinat_odp as nama_order, pl.catatan as headline, pl.created_at, pl.created_by as dispatch_regu_name, redaman from
                 dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket
                 left join psb_laporan pl on d.id=pl.id_tbl_mj
                 left join psb_laporan_status pls on pl.status_laporan=pls.laporan_status_id
                 where mc.isHapus = 0 AND pl.isCutOff is null and mc.id is null and (pl.status_laporan = '24')");
      //dd($data);
      return view('order.tommanpsblist', compact('data'));
    }
    public function tpsbcutoff($id,$jns){
      DB::table('psb_laporan')->where('id_tbl_mj', $id)->update(["isCutOff"=>1]);
      return redirect('/listtommanpsb/'.$jns);
    }
    public function tpsbfixdispatch($id,$jns){
      DB::table('psb_laporan')->where('id_tbl_mj', $id)->update(["checked_by"=>null, "checked_at"=>null]);
      return redirect('/listtommanpsb/'.$jns);
    }
    public function listtommanpsb($jns){
      //loss=2,full=24,insert=11
      $sql = "(pl.status_laporan = ".$jns.")";

      if($jns == 2 )
      {
      $sql = "(pl.status_laporan IN (".$jns.", 112) )";
      }

      if($jns == 24 )
      {
      $sql = "(pl.status_laporan IN (".$jns.", 113) )";
      }

      $data = DB::select(
      "SELECT pls.laporan_status,pl.nama_odp, pl.kordinat_odp, pl.kordinat_pelanggan, d.id AS id_dispatch, d.Ndem AS no_tiket, pl.kordinat_odp AS nama_order, pl.catatan AS headline, pl.created_at, pl.created_by AS dispatch_regu_name, redaman, pl.isCutOff, pl.checked_at, pl.checked_by,
      (CASE WHEN d.jenis_order IN('IN', 'INT') THEN 'Assurance'
      WHEN d.jenis_order IN('SC', 'MYIR') THEN 'PSB'
      ELSE 'None' END ) as jenis_psb
      FROM dispatch_teknisi d
      LEFT JOIN maintaince mc ON d.Ndem = mc.no_tiket
      LEFT JOIN psb_laporan pl ON d.id=pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
      WHERE pl.isCutOff IS NULL AND mc.id IS NULL AND $sql");
      return view('order.tommanpsblist', compact('data'));
    }
    public function listtommanpsb_crossover($jns, $stts){
      //loss=2,full=24,insert=11
      $qstts = '';
      if ($stts == 'ioan') {
        $qstts = "AND d.jenis_order IN ('IN', 'INT')";
      } elseif ($stts == 'psb') {
        $qstts = "AND d.jenis_order = 'SC'";
      }
      $data = DB::select(
        "SELECT pls.laporan_status,pl.nama_odp, pl.kordinat_odp, pl.kordinat_pelanggan, d.id AS id_dispatch, d.Ndem AS no_tiket, pl.kordinat_odp AS nama_order, pl.catatan AS headline, pl.created_at, pl.created_by AS dispatch_regu_name, redaman, pl.isCutOff, pl.checked_at, pl.checked_by,
        (CASE WHEN d.jenis_order IN('IN', 'INT') THEN 'Assurance'
        WHEN d.jenis_order IN('SC', 'MYIR') THEN 'PSB'
        ELSE 'None' END ) as jenis_psb
        FROM dispatch_teknisi d LEFT JOIN maintaince mc ON d.Ndem = mc.no_tiket
        LEFT JOIN psb_laporan pl ON d.id=pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
        WHERE pl.isCutOff IS NULL AND mc.id IS NULL AND (pl.status_laporan = '$jns') $qstts"
      );
      return view('order.tommanpsblist', compact('data'));
    }
    public function registertommanpsb($jns,$id){
      $data = DB::table('psb_laporan')
        ->select('psb_laporan.nama_odp', 'psb_laporan.catatan','psb_laporan.id','psb_laporan.checked_by','psb_laporan.kordinat_odp','psb_laporan.catatan', 'maintenance_jenis_order.nama_order', 'maintenance_jenis_order.id as jenis_order', 'dispatch_teknisi.Ndem')
        ->leftJoin('dispatch_teknisi', 'psb_laporan.id_tbl_mj', '=', 'dispatch_teknisi.id')
        ->leftJoin('maintenance_jenis_order', 'maintenance_jenis_order.status_psb', '=', 'psb_laporan.status_laporan')
        ->where('id_tbl_mj', $id)->first();
        $jenis_order = DB::table('maintenance_jenis_order')->get();
        $regu = DB::table('regu')->select('*', 'id_regu as id', 'uraian as text')->whereNotNull('uraian')->get();
        $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();

      return view('order.tommanpsbregister', compact('regu', 'sto', 'jenis_order', 'data'));
    }
    public function saveregistertommanpsb($jns,$id, Request $req){
      $rules = array(
        'headline' => 'required',
        'koordinat' => 'required',
        'sto' => 'required'
      );
      $messages = [
        'headline.required' => 'Silahkan isi kolom "Headline" Bang!',
        'koordinat.required' => 'Silahkan isi kolom "Koordinat" Bang!',
        'sto.required' => 'Silahkan Pilih "sto" Bang!'
      ];
      $validator = Validator::make($req->all(), $rules, $messages);
      if ($validator->fails()) {
        return redirect()->back()
            ->withInput($req->all())
            ->withErrors($validator)
            ->with('alerts', [
              ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan order']
            ]);
      }
      $auth = session('auth');
      $pl = DB::table('psb_laporan')
        ->select('psb_laporan.id','psb_laporan.checked_by','psb_laporan.kordinat_odp','psb_laporan.catatan', 'maintenance_jenis_order.nama_order', 'maintenance_jenis_order.id as jenis_order', 'dispatch_teknisi.Ndem')
        ->leftJoin('dispatch_teknisi', 'psb_laporan.id_tbl_mj', '=', 'dispatch_teknisi.id')
        ->leftJoin('maintenance_jenis_order', 'maintenance_jenis_order.status_psb', '=', 'psb_laporan.status_laporan')
        ->where('id_tbl_mj', $id)->first();
      //dd($pl);
      // if($pl->checked_by){
      //   return redirect('/listtommanpsb/{{ $jns }}')->with('alerts', [
      //           ['type' => 'danger', 'text' => '<strong>Order Sudah Di Lempar HD Consumer</strong>']
      //         ]);
      // }
      // else{
      // }
        if(substr($pl->Ndem,0,2) == "IN"){
          $order_from = "ASSURANCE";
        }else{
          $order_from = "PSB";
        }
        $sector_id = $sector_name = NULL;
        if ($req->nama_odp) {
            $alpro = explode("/", $req->nama_odp);
            $alpro = explode("-", $alpro[0]);
            $check_alpro = DB::table('maintenance_sector As ms')
                  ->leftJoin('maintenance_sector_alpro As msa', 'ms.id', '=', 'msa.sector_id')
                  ->select('ms.*', 'msa.alpro')
                  ->where('msa.alpro', $alpro[1].'-'.$alpro[2])->first();
            if ($check_alpro) {
                $sector_id = $check_alpro->id;
                $sector_name = $check_alpro->sector;
            }
        }

        $stts = DB::table('maintenance_jenis_order')->where('id', $req->jenis_order)->first();
        $datasto = DB::table('maintenance_datel')->where('sto', $req->sto)->first();
        $regu = DB::table('regu')->where('id_regu', $req->regu)->first();
        if(DB::table('maintaince')->where([
          ['no_tiket', $pl->Ndem],
          ['isHapus', 0]
        ])->first()){
          return redirect('/listtommanpsb/'.$jns)->with('alerts', [
                ['type' => 'danger', 'text' => '<strong>Nomor Tiket Sudah ada di Databas Marina</strong>']
              ]);
        }else{
          $id_mt = DB::table('maintaince')->insertGetId([
                    "no_tiket"          =>$pl->Ndem,
                    "jenis_order"       =>$req->jenis_order,
                    "nama_order"        =>$stts->nama_order,
                    "headline"          =>$req->headline,
                    "pic"               =>$auth->id_karyawan."/".$auth->nama,
                    "koordinat"         =>$req->koordinat,
                    "created_at"        =>DB::raw('now()'),
                    "created_by"        =>$auth->id_karyawan,
                    "psb_laporan_id"    =>$pl->id,
                    "order_from"        =>$order_from,
                    "kandatel"          =>$datasto->datel,
                    "sto"               =>$req->sto,
                    "sector_id"         =>$sector_id,
                    "nama_sector"       =>$sector_name,
                    "nama_odp"          =>$req->nama_odp,
                    "dispatch_regu_id"  =>@$regu->id_regu,
                    "dispatch_regu_name"=>@$regu->uraian,
                    "username1"         =>@$regu->nik1,
                    "username2"         =>@$regu->nik2
          ]);

          DB::table('psb_laporan')->where('id_tbl_mj', $id)->update([
            'checked_by' => $auth->id_karyawan,
            'checked_at' => DB::raw('now()')
          ]);

          if($req->input('regu')){
            // DB::table('maintenance_dispatch_log')->insert([
            //       "maintenance_id"  =>$id_mt,
            //       "regu_id"         =>@$regu->id_regu,
            //       "regu_name"       =>@$regu->uraian,
            //       "ts_dispatch"     =>DB::raw('now()'),
            //       "dispatch_by"     =>$auth->id_karyawan,
            //       "action"     =>"dispatch"
            // ]);
            DB::table('maintenance_note')
            ->insert([
              'mt_id'     => $id_mt,
              'catatan'   => "DISPATCH TOMMAN PSB",
              'status'   => "DISPATCH",
              'pelaku'    => session('auth')->id_karyawan,
              'ts'     => DB::raw('NOW()')
            ]);
            exec('cd ..;php artisan sendDispatch '.$id_mt.' > /dev/null &');
          }
          return redirect('/listtommanpsb/'.$jns);
        }
        //exec('cd ..;php artisan sendMt '.$id.' > /dev/null &');
    }
    public function get_info_odc($odc = 'all',  $sto = 'all', $datel= 'all')
    {
      $curl = curl_init();

      $post = [
        'datel' => $datel,
        'sto'   => $sto,
        'odc'   => $odc,
      ];

      curl_setopt_array($curl, [
        CURLOPT_URL            => 'https://promitos.tomman.app/API_ODC',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POSTFIELDS     => $post,
        CURLOPT_ENCODING       => '',
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_TIMEOUT        => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST  => 'POST',
      ]);

      $response = curl_exec($curl);
      curl_close($curl);

      $data = json_decode($response);

      return $data;
    }
    public function saveOrder($id, Request $req)
    {
      $auth = session('auth');
      //validator
      // if($auth->maintenance_level = 1){
        $rules = array(
          'headline' => 'required',
          'koordinat' => 'required',
          'sto' => 'required',
        );

        $messages = [
          'order_id.required' => 'Silahkan isi kolom "No Tiket" Bang!',
          'headline.required' => 'Silahkan isi kolom "Headline" Bang!',
          'koordinat.required' => 'Silahkan isi kolom "Koordinat" Bang!',
          'sto.required' => 'Silahkan Pilih "sto" Bang!',
          // 'no_inet.required' => 'Silahkan isi "no_inet" Bang!'
        ];

        $sector_id = $sector_name = NULL;

        if ($req->nama_odp) {
          $alpro = explode("/", $req->nama_odp);
          $alpro = explode("-", $alpro[0]);
          $check_alpro = DB::table('maintenance_sector As ms')
          ->leftJoin('maintenance_sector_alpro As msa', 'ms.id', '=', 'msa.sector_id')
          ->select('ms.*', 'msa.alpro')
          ->where('msa.alpro', $alpro[1].'-'.$alpro[2])->first();
          if ($check_alpro) {
            $sector_id = $check_alpro->id;
            $sector_name = $check_alpro->sector;
          }
        }

        if($req->jenis_order == 4){
          // $rules['no_inet'] = 'required';
          unset($rules['koordinat']);
          unset($messages['koordinat.required']);
          unset($rules['headline']);
          unset($messages['headline.required']);
        }
        // if($req->jenis_order != 1 && $req->jenis_order != 10){
        //   $rules['order_id'] = 'required';
        // }
        $validator = Validator::make($req->all(), $rules, $messages);

        if ($validator->fails()) {
          return redirect()->back()
              ->withInput($req->all())
              ->withErrors($validator)
              ->with('alerts', [
                ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan order']
              ]);
        }
        if($req->jenis_order == 4){
          //validasi duplikasi no inet yg blm close
          $cekinet = DB::table('maintaince')
          ->where('jenis_order', 4)
          ->where('no_inet', $req->no_inet)
          ->whereNull('status')
          ->where('isHapus', 0)
          ->first();
          // if($cekinet){
          //   return redirect()->back()
          //     ->withInput($req->all())
          //     ->with('alerts', [
          //       ['type' => 'danger', 'text' => '<strong>GAGAL</strong>, Duplikasi No Inet']
          //     ]);
          // }
          $data = DB::table('maintaince')->where([
            ['no_inet', $req->inet ],
            ['isHapus', 0],
            ['jenis_order', $req->jenis_order ],
            [DB::raw('DATE_FORMAT(created_at, "%Y-%m")'), '=', DB::raw('DATE_FORMAT(NOW(), "%Y-%m")')]
          ])->get();

          // if(count($data))
          // {
          //   return redirect()->back()
          //     ->withInput($req->all())
          //     ->with('alerts', [
          //       ['type' => 'danger', 'text' => '<strong>GAGAL</strong>, Nomor Internet Sudah Pernah Digunakan Bulan Ini']
          //     ]);
          // }
        }
      // }
      //end validator
        if($req->nog_order_id){
          $check_id_mt = DB::table('nog_master')->leftjoin('maintaince', 'nog_master.id', '=', 'maintaince.nog_order_id')->select('maintaince.id')->where('nog_master.id', $req->nog_order_id)->first();
          $id = $check_id_mt->id;
        }

        $order = DB::table('maintaince')->where([
          ['id', $id],
          ['isHapus', 0]
        ])->first();

        $jenis = DB::table('maintenance_jenis_order')->where('id', $req->jenis_order)->first();
        $sto = DB::table('maintenance_datel')->where('sto', $req->sto)->first();
        if(in_array($auth->maintenance_level, [1, 2, 3, 7]))
          $regu = DB::table('regu')->where('id_regu', $req->regu)->first();
        else
          $regu = DB::table('regu')->where('nik1', $auth->id_karyawan)->orWhere('nik2', $auth->id_karyawan)->first();

        if($order)
        {
          $id_mt = $id;
          $data = [
            "no_tiket"          =>$req->order_id,
            "jenis_order"       =>$req->jenis_order,
            "jenis_hvc"         =>$req->kategori,
            "nama_order"        =>$jenis->nama_order,
            "headline"          =>$req->headline,
            "pic"               =>$req->pic,
            "koordinat"         =>$req->koordinat,
            "sektor"            =>$req->sektor,
            "timeplan"          =>$req->timeplan,
            "dispatch_regu_id"  =>@$regu->id_regu,
            "dispatch_regu_name"=>@$regu->uraian,
            "username1"         =>@$regu->nik1,
            "username2"         =>@$regu->nik2,
            "modified_at"       =>DB::raw('now()'),
            "modified_by"       =>$auth->id_karyawan,
            "sto"               =>$sto->sto,
            "sector_id"         =>$sector_id,
            "nama_sector"       =>$sector_name,
            "kandatel"          =>$sto->datel,
            "nama_odp"          =>$req->nama_odp,
            "no_inet"          =>$req->no_inet
          ];

          if($req->nog_order_id)
          {
            $data['nog_order_id'] = $req->nog_order_id;
          }

          DB::table('maintaince')->where('id', $id)->update($data);
        }
        else
        {
          $no_tiket = $req->order_id;
          $data = [
            "no_tiket"          =>$no_tiket,
            "jenis_order"       =>$req->jenis_order,
            "nama_order"        =>$jenis->nama_order,
            "jenis_hvc"         =>$req->kategori,
            "headline"          =>$req->headline,
            "pic"               =>$req->pic,
            "koordinat"         =>$req->koordinat,
            "sektor"            =>$req->sektor,
            "timeplan"          =>$req->timeplan,
            "dispatch_regu_id"  =>@$regu->id_regu,
            "dispatch_regu_name"=>@$regu->uraian,
            "username1"         =>@$regu->nik1,
            "username2"         =>@$regu->nik2,
            "created_at"        =>DB::raw('now()'),
            "created_by"        =>$auth->id_karyawan,
            "sto"               =>$sto->sto,
            "sector_id"         =>$sector_id,
            "nama_sector"       =>$sector_name,
            "kandatel"          =>$sto->datel,
            "nama_odp"          =>$req->nama_odp,
            "no_inet"           =>$req->no_inet
          ];

          if($req->nog_order_id)
          {
            $data['nog_order_id'] = $req->nog_order_id;
          }

          $id_mt = DB::table('maintaince')->insertGetId($data);
        }

        if($req->jenis_order == 2)
        {
          if($req->pilih_jointer == 'freelance')
          {
            DB::table('pt2_ready_jointer')->insert([
              'id_wo'      => $id_mt,
              'jenis'      => 'marina',
              "created_by" => $auth->id_karyawan,
            ]);
          }
          else
          {
            $order['nik1'] = $req->tim_jointer;
            $nama_jointer = DB::table('1_2_employee')->where('nik', $req->tim_jointer)->first();

            $order['jointer_name'] = $nama_jointer->nama;
            $order['jenis_wo']     = 'PT-2 SIMPLE';
            $order['id_maint']     = $id_mt;
            $order['odp_nama']     = $data['nama_odp'];
            $order['odp_koor']     = $data['koordinat'];
            $order['catatan_HD']   = $data['headline'];
            $order['nomor_sc']     = $data['no_tiket'];
            $order['sto']          = $data['sto'];
            $order['project_name'] = $data['no_tiket'];

            $get_odc = explode('/', $data['nama_odp'])[0];
            $get_odc = explode('-', $get_odc);
            $get_odc = 'ODC-'.$get_odc[1].'-'.$get_odc[2];

            $order['odc_nama'] = $get_odc;

            $split_odc = explode('-', $order['odc_nama']);

            $get_info_odc = $this->get_info_odc($split_odc[2], $split_odc[1]);

            if(count($get_info_odc) )
            {
              $order['odc_koor'] = str_replace(',', '.', $get_info_odc[0]->latitude) .','. str_replace(',', '.', $get_info_odc[0]->longitude);
            }

            $order['kategory_non_unsc'] = 1;
            $order['delete_clm']        = 0;
            $order['tgl_pengerjaan']    = date('Y-m-d H:i:s');

            $order['status'] = 'Construction';
            $order['tgl_buat'] = date('Y-m-d H:i:s');

            $id = DB::table('pt2_master')->insertGetId($order);

            DB::table('pt2_dispatch')->insert([
              'id_order'           => $id,
              'nik1'               => $order['nik1'],
              'keterangan'         => 'Pickup Order Jointer',
              'tgl_kerja_dispatch' => $order['tgl_pengerjaan'],
              'updated_by'         => $auth->id_karyawan
            ]);
          }
        }

        if($req->nog_order_id)
        {
          DB::table('nog_master')->where('id', $req->nog_order_id)->update([
            'mt_id'            => $id_mt,
            'dispatch_regu_id' => @$regu->id_regu,
            'regu_nama'        => @$regu->uraian,
          ]);
        }

        if($req->input('regu') != @$order->dispatch_regu_id){
          // DB::table('maintenance_dispatch_log')->insert([
          //       "maintenance_id"  =>$id_mt,
          //       "regu_id"         =>@$regu->id_regu,
          //       "regu_name"       =>@$regu->uraian,
          //       "ts_dispatch"     =>DB::raw('now()'),
          //       "dispatch_by"     =>$auth->id_karyawan,
          //       "action"     =>"dispatch"
          // ]);
          DB::table('maintenance_note')
            ->insert([
              'mt_id'   => $id_mt,
              'catatan' => "DISPATCH REGU LAIN ($regu->uraian)",
              'status'  => "DISPATCH REGU LAIN",
              'pelaku'  => session('auth')->id_karyawan,
              'ts'      => DB::raw('NOW()')
            ]);
          exec('cd ..;php artisan sendDispatch '.$id_mt.' > /dev/null &');
        }
        return redirect('/order/none/none/'.date('Y-m').'/all');
    }

    // public function tes_bulk()
    // {
    //   $data = [
    //     ['no_tiket' => 517213327, 'regu'=>'AGB-BJM1-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FAU/167'],
    //     ['no_tiket' => 516825716, 'regu'=>'AGB-BJM1-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FAJ/182'],
    //     ['no_tiket' => 516083067, 'regu'=>'AGB-BJM1-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FL/053'],
    //     ['no_tiket' => 515680879, 'regu'=>'AGB-BJM1-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FA/020'],
    //     ['no_tiket' => 512300596, 'regu'=>'AGB-BJM1-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FY/027'],
    //     ['no_tiket' => 511807532, 'regu'=>'AGB-BJM1-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FAA/010'],
    //     ['no_tiket' => 511666093, 'regu'=>'AGB-BJM1-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FAS/054'],
    //     ['no_tiket' => 517732683, 'regu'=>'AGB-BJM1-SODP/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FAB/032'],
    //     ['no_tiket' => 517995910, 'regu'=>'AGB-BJM1-SODP/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FAB/116'],
    //     ['no_tiket' => 517942434, 'regu'=>'AGB-BJM1-SODP/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FU/002'],
    //     ['no_tiket' => 517852829, 'regu'=>'AGB-BJM1-SODP/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FL/002'],
    //     ['no_tiket' => 517719165, 'regu'=>'AGB-BJM1-SODP/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FAS/097'],
    //     ['no_tiket' => 516834316, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-KIP-FE/086'],
    //     ['no_tiket' => 510555604, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FD/026'],
    //     ['no_tiket' => 517537552, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FD/026'],
    //     ['no_tiket' => 517539539, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FD/028'],
    //     ['no_tiket' => 517125468, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FD/029'],
    //     ['no_tiket' => 515164541, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FA/026'],
    //     ['no_tiket' => 514683019, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FD/028'],
    //     ['no_tiket' => 514478989, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FA/002'],
    //     ['no_tiket' => 514330262, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FA/002'],
    //     ['no_tiket' => 513843322, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FA/008'],
    //     ['no_tiket' => 513854121, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FA/011'],
    //     ['no_tiket' => 513787539, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FA/008'],
    //     ['no_tiket' => 513639758, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FF/034'],
    //     ['no_tiket' => 512857713, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FB/005'],
    //     ['no_tiket' => 512545446, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FF/034'],
    //     ['no_tiket' => 512409668, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FA/008'],
    //     ['no_tiket' => 512131950, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FA/025'],
    //     ['no_tiket' => 511602219, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FA/032'],
    //     ['no_tiket' => 511377783, 'regu'=>'CUI-BLC-PT2/03', 'jenis_order' =>	8, 'odp' =>	'ODP-STI-FD/028'],
    //     ['no_tiket' => 513837086, 'regu'=>'CUI-KPL-ODP_SEHAT/03', 'jenis_order' =>	8, 'odp' =>	'ODP-KPL-FL/002'],
    //     ['no_tiket' => 513578012, 'regu'=>'CUI-KPL-ODP_SEHAT/03', 'jenis_order' =>	8, 'odp' =>	'ODP-KPL-FL/001'],
    //     ['no_tiket' => 512643941, 'regu'=>'CUI-KPL-ODP_SEHAT/03', 'jenis_order' =>	8, 'odp' =>	'ODP-KPL-FB/002'],
    //     ['no_tiket' => 510653959, 'regu'=>'CUI-SODP-BBR2-01', 'jenis_order' =>	8, 'odp' =>	'ODP-MTP-FK/003'],
    //     ['no_tiket' => 517143029, 'regu'=>'CUI-SODP-BBR2-01', 'jenis_order' =>	8, 'odp' =>	'ODP-MTP-FC/054'],
    //     ['no_tiket' => 515245530, 'regu'=>'CUI-SODP-BBR2-01', 'jenis_order' =>	8, 'odp' =>	'ODP-MTP-FC/054'],
    //     ['no_tiket' => 515161367, 'regu'=>'CUI-SODP-BBR2-01', 'jenis_order' =>	8, 'odp' =>	'ODP-MTP-FC/060'],
    //     ['no_tiket' => 512997491, 'regu'=>'CUI-SODP-BBR2-01', 'jenis_order' =>	8, 'odp' =>	'ODP-MTP-FB/102'],
    //     ['no_tiket' => 511564772, 'regu'=>'CUI-SODP-BBR2-01', 'jenis_order' =>	8, 'odp' =>	'ODP-MTP-FC/026'],
    //     ['no_tiket' => 511279282, 'regu'=>'CUI-SODP-BBR2-01', 'jenis_order' =>	8, 'odp' =>	'ODP-MTP-FC/063'],
    //     ['no_tiket' => 511062237, 'regu'=>'CUI-SODP-BBR2-01', 'jenis_order' =>	8, 'odp' =>	'ODP-MTP-FC/062'],
    //     ['no_tiket' => 511022634, 'regu'=>'CUI-SODP-BBR2-01', 'jenis_order' =>	8, 'odp' =>	'ODP-MTP-FE/023'],
    //     ['no_tiket' => 510944342, 'regu'=>'CUI-SODP-BBR2-01', 'jenis_order' =>	8, 'odp' =>	'ODP-MTP-FB/093'],
    //     ['no_tiket' => 510505171, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FE/07'],
    //     ['no_tiket' => 510599540, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FH/008'],
    //     ['no_tiket' => 510948702, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FF/035'],
    //     ['no_tiket' => 510911048, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FU/021'],
    //     ['no_tiket' => 510994550, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FD/033'],
    //     ['no_tiket' => 511096551, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-AMT-FM/034'],
    //     ['no_tiket' => 511178035, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FD/023'],
    //     ['no_tiket' => 511278947, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-TJL-FV/048'],
    //     ['no_tiket' => 511380612, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-AMT-FM/010'],
    //     ['no_tiket' => 511554375, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FF/026'],
    //     ['no_tiket' => 511639538, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FH/057'],
    //     ['no_tiket' => 511789902, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FF/036'],
    //     ['no_tiket' => 511867331, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FE/062'],
    //     ['no_tiket' => 511911326, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FD/032'],
    //     ['no_tiket' => 511966859, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FU/014'],
    //     ['no_tiket' => 512198743, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-AMT-FM/009'],
    //     ['no_tiket' => 512429175, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FE/030'],
    //     ['no_tiket' => 512463142, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FH/056'],
    //     ['no_tiket' => 512438527, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FH/057'],
    //     ['no_tiket' => 512582366, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FU/021'],
    //     ['no_tiket' => 513138208, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FF/036'],
    //     ['no_tiket' => 513641909, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FS/002'],
    //     ['no_tiket' => 513720345, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-AMT-FJ/070'],
    //     ['no_tiket' => 514042399, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FE/024'],
    //     ['no_tiket' => 514095065, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FN/006'],
    //     ['no_tiket' => 514282713, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FF/035'],
    //     ['no_tiket' => 514283997, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-TJL-FA/085'],
    //     ['no_tiket' => 514482277, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FN/008'],
    //     ['no_tiket' => 514820801, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-TJL-FQ/132'],
    //     ['no_tiket' => 515495141, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-AMT-FAC/046'],
    //     ['no_tiket' => 512614425, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FF/020'],
    //     ['no_tiket' => 517979236, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-TJL-FQ/050'],
    //     ['no_tiket' => 517918855, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-TJL-FF/002'],
    //     ['no_tiket' => 517413105, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FS/001'],
    //     ['no_tiket' => 517372794, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-TJL-FA/085'],
    //     ['no_tiket' => 517348109, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-TJL-FQ/043'],
    //     ['no_tiket' => 517023549, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-AMT-FM/005'],
    //     ['no_tiket' => 516610191, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-TJL-FQ/039'],
    //     ['no_tiket' => 515480592, 'regu'=>'CUI-TJL-PROV-01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGN-FF/024'],
    //     ['no_tiket' => 515901192, 'regu'=>'MAP-BRI-PROV/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BRI-FO/003'],
    //     ['no_tiket' => 514756968, 'regu'=>'MAP-BRI-PROV/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BRI-FJ/135'],
    //     ['no_tiket' => 513683177, 'regu'=>'MAP-BRI-PROV/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BRI-FO1/001'],
    //     ['no_tiket' => 512791358, 'regu'=>'MAP-BRI-PROV/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BRI-FJ/019'],
    //     ['no_tiket' => 512145415, 'regu'=>'MAP-BRI-PROV/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BRI-FJ/009'],
    //     ['no_tiket' => 511903406, 'regu'=>'MAP-BRI-PROV/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BRI-FJ/017'],
    //     ['no_tiket' => 514575781, 'regu'=>'MAP-KDG-SODP/10', 'jenis_order' =>	8, 'odp' =>	'ODP-KDG-FA/030'],
    //     ['no_tiket' => 514801981, 'regu'=>'MAP-KDG-SODP/10', 'jenis_order' =>	8, 'odp' =>	'ODP-KDG-FJ/080'],
    //     ['no_tiket' => 515181012, 'regu'=>'MAP-KDG-SODP/10', 'jenis_order' =>	8, 'odp' =>	'ODP-KDG-FC/085'],
    //     ['no_tiket' => 513822379, 'regu'=>'MAP-KDG-SODP/10', 'jenis_order' =>	8, 'odp' =>	'ODP-KDG-FF/072'],
    //     ['no_tiket' => 513228722, 'regu'=>'MAP-KDG-SODP/10', 'jenis_order' =>	8, 'odp' =>	'ODP-KDG-FA/062'],
    //     ['no_tiket' => 513121419, 'regu'=>'MAP-KDG-SODP/10', 'jenis_order' =>	8, 'odp' =>	'ODP-RTA-FB/005'],
    //     ['no_tiket' => 510607328, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FB/007'],
    //     ['no_tiket' => 513071389, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAF/305'],
    //     ['no_tiket' => 517267087, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAH/016'],
    //     ['no_tiket' => 517553907, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAJ/709'],
    //     ['no_tiket' => 514045660, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FBL/071'],
    //     ['no_tiket' => 515025922, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAN/099'],
    //     ['no_tiket' => 517991978, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FT/014'],
    //     ['no_tiket' => 517660130, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAL/013'],
    //     ['no_tiket' => 517477272, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAJ/079'],
    //     ['no_tiket' => 517236832, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAL/008'],
    //     ['no_tiket' => 517237990, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAG/037'],
    //     ['no_tiket' => 517211879, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FY/070'],
    //     ['no_tiket' => 517143199, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAL/017'],
    //     ['no_tiket' => 517215039, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAH/016'],
    //     ['no_tiket' => 517164384, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAU/112'],
    //     ['no_tiket' => 516973850, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAZ/060'],
    //     ['no_tiket' => 517049316, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAN/003'],
    //     ['no_tiket' => 516939079, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FK/037'],
    //     ['no_tiket' => 516700558, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAH/208'],
    //     ['no_tiket' => 516556805, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAL/015'],
    //     ['no_tiket' => 516464848, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FBD/036'],
    //     ['no_tiket' => 516416004, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAL/002'],
    //     ['no_tiket' => 516224399, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAM/136'],
    //     ['no_tiket' => 515841784, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAN/136'],
    //     ['no_tiket' => 515482234, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FBG/069'],
    //     ['no_tiket' => 515513624, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAA/202'],
    //     ['no_tiket' => 515423179, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FK/040'],
    //     ['no_tiket' => 515180976, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAJ/005'],
    //     ['no_tiket' => 515163417, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FN/212'],
    //     ['no_tiket' => 515152700, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FBE/010'],
    //     ['no_tiket' => 514929522, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FT/014'],
    //     ['no_tiket' => 514931996, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAJ/701'],
    //     ['no_tiket' => 514881298, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAQ/044'],
    //     ['no_tiket' => 514709458, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FK/006'],
    //     ['no_tiket' => 514496249, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FY/071'],
    //     ['no_tiket' => 514249160, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FY/023'],
    //     ['no_tiket' => 514219815, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FW/006'],
    //     ['no_tiket' => 514218853, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FG/003'],
    //     ['no_tiket' => 514094580, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FAC/031'],
    //     ['no_tiket' => 514041671, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FW/006'],
    //     ['no_tiket' => 513875239, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FAC/028'],
    //     ['no_tiket' => 513715627, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAL/012'],
    //     ['no_tiket' => 513539043, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FY/051'],
    //     ['no_tiket' => 513481184, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FU/009'],
    //     ['no_tiket' => 513404980, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAJ/061'],
    //     ['no_tiket' => 513306298, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FBC/003'],
    //     ['no_tiket' => 513317080, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FBT/148'],
    //     ['no_tiket' => 513279262, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FBC/003'],
    //     ['no_tiket' => 513225497, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FAC/028'],
    //     ['no_tiket' => 513178511, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FBU/021'],
    //     ['no_tiket' => 513165222, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FBE/007'],
    //     ['no_tiket' => 513041578, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FB/002'],
    //     ['no_tiket' => 512929553, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAZ/027'],
    //     ['no_tiket' => 512857427, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FG/091'],
    //     ['no_tiket' => 512668496, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FL/003'],
    //     ['no_tiket' => 512455317, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FBT/146'],
    //     ['no_tiket' => 512308118, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FK/033'],
    //     ['no_tiket' => 512104082, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAM/101'],
    //     ['no_tiket' => 512069883, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAF/312'],
    //     ['no_tiket' => 511547172, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAQ/106'],
    //     ['no_tiket' => 511522715, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAQ/106'],
    //     ['no_tiket' => 511477053, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FBG/149'],
    //     ['no_tiket' => 511351043, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAJ/002'],
    //     ['no_tiket' => 511123764, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAL/015'],
    //     ['no_tiket' => 511108520, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAQ/031'],
    //     ['no_tiket' => 511065381, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FAL/014'],
    //     ['no_tiket' => 511078359, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FBE/009'],
    //     ['no_tiket' => 511021203, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FU/017'],
    //     ['no_tiket' => 511023448, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FU/002'],
    //     ['no_tiket' => 510985019, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FG/091'],
    //     ['no_tiket' => 510898598, 'regu'=>'MAP-SODP-BBR1-GHIFARI&RAMADHANI', 'jenis_order' =>	8, 'odp' =>	'ODP-BBR-FK/403'],
    //     ['no_tiket' => 510443865, 'regu'=>'SODP-AGB-BJM-FERLI', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FAB/034'],
    //     ['no_tiket' => 510592997, 'regu'=>'SODP-AGB-BJM-FERLI', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FAA/414'],
    //     ['no_tiket' => 517519087, 'regu'=>'SODP-AGB-BJM-FERLI', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FAB/030'],
    //     ['no_tiket' => 517981907, 'regu'=>'SODP-AGB-BJM-FERLI', 'jenis_order' =>	8, 'odp' =>	'ODP-BJM-FA/020'],
    //     ['no_tiket' => 510729206, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FE/002'],
    //     ['no_tiket' => 518049419, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FJ/038'],
    //     ['no_tiket' => 514818368, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FAM/035'],
    //     ['no_tiket' => 517990706, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FE/008'],
    //     ['no_tiket' => 517656157, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FW/062'],
    //     ['no_tiket' => 517416254, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FL/048'],
    //     ['no_tiket' => 517300788, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FAC/018'],
    //     ['no_tiket' => 517212449, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FZ/076'],
    //     ['no_tiket' => 516713788, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FP/014'],
    //     ['no_tiket' => 516474645, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FZ/076'],
    //     ['no_tiket' => 516457247, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FC/001'],
    //     ['no_tiket' => 516299897, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FF/025'],
    //     ['no_tiket' => 516270832, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FZ/076'],
    //     ['no_tiket' => 516176787, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FF/025'],
    //     ['no_tiket' => 515404892, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FJ/024'],
    //     ['no_tiket' => 515405106, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FZ/047'],
    //     ['no_tiket' => 515029257, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FP/041'],
    //     ['no_tiket' => 514958260, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FZ/076'],
    //     ['no_tiket' => 514494176, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FY/002'],
    //     ['no_tiket' => 514202971, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FJ/025'],
    //     ['no_tiket' => 514140861, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FF/025'],
    //     ['no_tiket' => 514132277, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FP/102'],
    //     ['no_tiket' => 514045915, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FAM/029'],
    //     ['no_tiket' => 514029979, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FP/003'],
    //     ['no_tiket' => 513975403, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FF/034'],
    //     ['no_tiket' => 513809905, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FAM/029'],
    //     ['no_tiket' => 513792497, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FZ/056'],
    //     ['no_tiket' => 513514051, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FP/043'],
    //     ['no_tiket' => 513339320, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FG/001'],
    //     ['no_tiket' => 513330239, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FP/043'],
    //     ['no_tiket' => 512609104, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FAC/011'],
    //     ['no_tiket' => 512456976, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FAM/029'],
    //     ['no_tiket' => 512393481, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FC/002'],
    //     ['no_tiket' => 511957609, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FP/102'],
    //     ['no_tiket' => 511954214, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FD/020'],
    //     ['no_tiket' => 511823629, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FW/422'],
    //     ['no_tiket' => 511519082, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FAM/021'],
    //     ['no_tiket' => 511135604, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FX/042'],
    //     ['no_tiket' => 511065574, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FY/003'],
    //     ['no_tiket' => 511039532, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FP/041'],
    //     ['no_tiket' => 511013505, 'regu'=>'SODP-AGB-LUL/01', 'jenis_order' =>	8, 'odp' =>	'ODP-LUL-FN/023'],
    //     ['no_tiket' => 516225238, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BLC-FM/066'],
    //     ['no_tiket' => 516461207, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BLC-FL/016'],
    //     ['no_tiket' => 516225226, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BLC-FD)015'],
    //     ['no_tiket' => 515579027, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BLC-FH/201'],
    //     ['no_tiket' => 515162478, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BLC-FS/018'],
    //     ['no_tiket' => 513814767, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BLC-FL/002'],
    //     ['no_tiket' => 513699914, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BLC-FE/046'],
    //     ['no_tiket' => 513646600, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BLC-FM/115'],
    //     ['no_tiket' => 511844302, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BLC-FK/034'],
    //     ['no_tiket' => 516783625, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGT-FB/029'],
    //     ['no_tiket' => 515761351, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGT-FB/057'],
    //     ['no_tiket' => 513459031, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGT-FB/060'],
    //     ['no_tiket' => 511146696, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-PGT-FB/060'],
    //     ['no_tiket' => 516650087, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-SER-FV/002'],
    //     ['no_tiket' => 513271615, 'regu'=>'SODP-CUI-BLC/01', 'jenis_order' =>	8, 'odp' =>	'ODP-BLC-FU/043'],
    //     ['no_tiket' => 510899230, 'regu'=>'SODP-UT-ULI1/15', 'jenis_order' =>	8, 'odp' =>	'ODP-GMB-FB/018'],
    //     ['no_tiket' => 512463199, 'regu'=>'SODP-UT-ULI1/15', 'jenis_order' =>	8, 'odp' =>	'ODP-GMB-FD/023'],
    //     ['no_tiket' => 512912140, 'regu'=>'SODP-UT-ULI1/15', 'jenis_order' =>	8, 'odp' =>	'ODP-GMB-FA/409'],
    //     ['no_tiket' => 512998776, 'regu'=>'SODP-UT-ULI1/15', 'jenis_order' =>	8, 'odp' =>	'ODP-GMB-FF/012'],
    //     ['no_tiket' => 514166923, 'regu'=>'SODP-UT-ULI1/15', 'jenis_order' =>	8, 'odp' =>	'ODP-GMB-FA/024'],
    //     ['no_tiket' => 515247955, 'regu'=>'SODP-UT-ULI1/15', 'jenis_order' =>	8, 'odp' =>	'ODP-GMB-FG/018'],
    //     ['no_tiket' => 513570232, 'regu'=>'SODP-UT-ULI1/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FAF/205'],
    //     ['no_tiket' => 513579208, 'regu'=>'SODP-UT-ULI1/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FBJ/050'],
    //     ['no_tiket' => 513691703, 'regu'=>'SODP-UT-ULI1/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FAN/071'],
    //     ['no_tiket' => 513783666, 'regu'=>'SODP-UT-ULI1/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FBK/080'],
    //     ['no_tiket' => 513848683, 'regu'=>'SODP-UT-ULI1/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FBW/009'],
    //     ['no_tiket' => 513920603, 'regu'=>'SODP-UT-ULI1/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FX/002'],
    //     ['no_tiket' => 514057211, 'regu'=>'SODP-UT-ULI1/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FV/026'],
    //     ['no_tiket' => 511353601, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FAD/103'],
    //     ['no_tiket' => 511640970, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FBQ/065'],
    //     ['no_tiket' => 511682684, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FQ/001'],
    //     ['no_tiket' => 511815800, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FAM/008'],
    //     ['no_tiket' => 511867830, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FE/013'],
    //     ['no_tiket' => 512109835, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FCA/001'],
    //     ['no_tiket' => 512282250, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FBQ/004'],
    //     ['no_tiket' => 512299669, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FBF/046'],
    //     ['no_tiket' => 513011647, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FL/042'],
    //     ['no_tiket' => 512973424, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FAC/219'],
    //     ['no_tiket' => 513180553, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FAL/010'],
    //     ['no_tiket' => 513235727, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FL/024'],
    //     ['no_tiket' => 513465690, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FBK/007'],
    //     ['no_tiket' => 513485746, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FAC/09'],
    //     ['no_tiket' => 517749706, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FE/039'],
    //     ['no_tiket' => 514147571, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FCC/043'],
    //     ['no_tiket' => 515468956, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FCC/031'],
    //     ['no_tiket' => 516285329, 'regu'=>'SODP-UT-ULI1/16', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FAL/010'],
    //     ['no_tiket' => 510604555, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FQ/038'],
    //     ['no_tiket' => 518021237, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FK/009'],
    //     ['no_tiket' => 517327140, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FF/007'],
    //     ['no_tiket' => 516043583, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FA/027'],
    //     ['no_tiket' => 515841800, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FM/008'],
    //     ['no_tiket' => 513845250, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FA/060'],
    //     ['no_tiket' => 513619196, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FP/020'],
    //     ['no_tiket' => 513448575, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FK/016'],
    //     ['no_tiket' => 513054199, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-BTB-FE/010'],
    //     ['no_tiket' => 512663286, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FN/067'],
    //     ['no_tiket' => 511875564, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FXA/008'],
    //     ['no_tiket' => 511837115, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FC/024'],
    //     ['no_tiket' => 511700033, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FXA/033'],
    //     ['no_tiket' => 511581537, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FE/021'],
    //     ['no_tiket' => 511181848, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FM/011'],
    //     ['no_tiket' => 511021406, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FK/055'],
    //     ['no_tiket' => 510981884, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FN/080'],
    //     ['no_tiket' => 510984335, 'regu'=>'TBN-PLE-SODP/08', 'jenis_order' =>	8, 'odp' =>	'ODP-PLE-FQ/037'],
    //     ['no_tiket' => 514336090, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FCA/001'],
    //     ['no_tiket' => 514434661, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FA/001'],
    //     ['no_tiket' => 514665748, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FCA/001'],
    //     ['no_tiket' => 515161537, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FN/208'],
    //     ['no_tiket' => 515022646, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FN/038'],
    //     ['no_tiket' => 516145040, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FAZ/073'],
    //     ['no_tiket' => 516269779, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FH/201'],
    //     ['no_tiket' => 516335949, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FAC/048'],
    //     ['no_tiket' => 516330727, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FAM/005'],
    //     ['no_tiket' => 516538309, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FAS/040'],
    //     ['no_tiket' => 516569521, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FAY/013'],
    //     ['no_tiket' => 516599938, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FE/140'],
    //     ['no_tiket' => 516700069, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FCC/058'],
    //     ['no_tiket' => 517203403, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FAS/522'],
    //     ['no_tiket' => 517276422, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FH/013'],
    //     ['no_tiket' => 517415502, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FCC/063'],
    //     ['no_tiket' => 516610634, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FN/020'],
    //     ['no_tiket' => 517611170, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FL/013'],
    //     ['no_tiket' => 517710611, 'regu'=>'UT-ULI1-SODP/15', 'jenis_order' =>	8, 'odp' =>	'ODP-ULI-FE/703'],
    //     ['no_tiket' => 510605208, 'regu'=>'STM-KYG-PROV/02', 'jenis_order' =>	8, 'odp' =>	'ODP-KYG-FT/039'],
    //     ['no_tiket' => 510687323, 'regu'=>'STM-KYG-PROV/02', 'jenis_order' =>	8, 'odp' =>	'ODP-KYG-FV/052'],
    //     ['no_tiket' => 514341072, 'regu'=>'STM-KYG-PROV/02', 'jenis_order' =>	8, 'odp' =>	'ODP-KYG-FN/106'],
    //     ['no_tiket' => 514798080, 'regu'=>'STM-KYG-PROV/02', 'jenis_order' =>	8, 'odp' =>	'ODP-KYG-FG/008'],
    //     ['no_tiket' => 516277329, 'regu'=>'STM-KYG-PROV/02', 'jenis_order' =>	8, 'odp' =>	'ODP-KYG-FN/008'],
    //     ['no_tiket' => 517016925, 'regu'=>'STM-KYG-PROV/02', 'jenis_order' =>	8, 'odp' =>	'ODP-KYG-FT/060'],
    //     ['no_tiket' => 517166191, 'regu'=>'STM-KYG-PROV/02', 'jenis_order' =>	8, 'odp' =>	'ODP-KYG-FG/005'],
    //     ['no_tiket' => 517518147, 'regu'=>'STM-KYG-PROV/02', 'jenis_order' =>	8, 'odp' =>	'ODP-KYG-FX/063'],
    //     ['no_tiket' => 517574229, 'regu'=>'STM-KYG-PROV/02', 'jenis_order' =>	8, 'odp' =>	'ODP-KYG-FV/045'],
    //     ['no_tiket' => 512330871, 'regu'=>'STM-KYG-PROV/02', 'jenis_order' =>	8, 'odp' =>	'ODP-KYG-FV/065'],
    //     ['no_tiket' => 514818443, 'regu'=>'STM-KYG-PROV/02', 'jenis_order' =>	8, 'odp' =>	'ODP-KYG-FT/016'],
    //     ['no_tiket' => 517673477, 'regu'=>'STM-KYG-PROV/02', 'jenis_order' =>	8, 'odp' =>	'ODP-KYG-FT/010'],
    //     ['no_tiket' => 512712178, 'regu'=>'STM-KYG-PROV/02', 'jenis_order' =>	8, 'odp' =>	'ODP-KYG-FV/017'],
    //     ['no_tiket' => 514760861, 'regu'=>'STM-KYG-PROV/02', 'jenis_order' =>	8, 'odp' =>	'ODP-KYG-FN/009'],
    //     ['no_tiket' => 511110942,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAG/018'],
    //     ['no_tiket' => 515842738,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAH/007'],
    //     ['no_tiket' => 516825458,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAH/008'],
    //     ['no_tiket' => 511373465,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAH/131'],
    //     ['no_tiket' => 515628592,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAJ/017'],
    //     ['no_tiket' => 511323864,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAJ/052'],
    //     ['no_tiket' => 517519103,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAJ/056'],
    //     ['no_tiket' => 512920479,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAJ/094'],
    //     ['no_tiket' => 517267377,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAJ/132'],
    //     ['no_tiket' => 515416272,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAK/017'],
    //     ['no_tiket' => 511699002,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAL/028'],
    //     ['no_tiket' => 516842948,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAL/028'],
    //     ['no_tiket' => 516845555,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAL/042'],
    //     ['no_tiket' => 513011976,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAM/060'],
    //     ['no_tiket' => 516950554,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAM/060'],
    //     ['no_tiket' => 511642339,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAM/153'],
    //     ['no_tiket' => 513735438,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAQ/016'],
    //     ['no_tiket' => 512723577,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAQ/106'],
    //     ['no_tiket' => 512232306,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAS/041'],
    //     ['no_tiket' => 510760072,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAU/023'],
    //     ['no_tiket' => 511240586,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAV/113'],
    //     ['no_tiket' => 510431794,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAV/169'],
    //     ['no_tiket' => 516524685,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAV/173'],
    //     ['no_tiket' => 511860616,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAZ/012'],
    //     ['no_tiket' => 514960725,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FAZ/060'],
    //     ['no_tiket' => 517494282,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FBG/058'],
    //     ['no_tiket' => 517822772,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FBM/005'],
    //     ['no_tiket' => 512754322,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FBM/010'],
    //     ['no_tiket' => 513502920,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FBM/010'],
    //     ['no_tiket' => 513972034,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FBU/005'],
    //     ['no_tiket' => 517611489,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FBU/044'],
    //     ['no_tiket' => 513889652,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FG/052'],
    //     ['no_tiket' => 513677022,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FG/077'],
    //     ['no_tiket' => 513654765,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FG/091'],
    //     ['no_tiket' => 512351370,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FK/010'],
    //     ['no_tiket' => 516893895,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FL/005'],
    //     ['no_tiket' => 514535589,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FL/005'],
    //     ['no_tiket' => 516692619,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FV/005'],
    //     ['no_tiket' => 513910266,	'regu' => 'MAP-SODP-BBR1-GHIFARI&RAMADHANI',	'jenis_order' => 8, 'odp' =>	'ODP-BBR-FW/027'],
    //     ['no_tiket' => 517586148,	'regu' => 'AGB-BJM1-PROV-01',	'jenis_order' => 8, 'odp' =>	'ODP-BJM-FAB/037'],
    //     ['no_tiket' => 516292822,	'regu' => 'AGB-BJM1-PROV-01',	'jenis_order' => 8, 'odp' =>	'ODP-BJM-FBN/076'],
    //     ['no_tiket' => 511557809,	'regu' => 'AGB-BJM1-PROV-01',	'jenis_order' => 8, 'odp' =>	'ODP-BJM-FY/018'],
    //     ['no_tiket' => 515361742,	'regu' => 'CUI-BLC-PT2/03',	'jenis_order' => 8, 'odp' =>	'ODP-BLC-FB/016'],
    //     ['no_tiket' => 511734497,	'regu' => 'CUI-BLC-PT2/03',	'jenis_order' => 8, 'odp' =>	'ODP-BLC-FQ/015'],
    //     ['no_tiket' => 511111065,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-GMB-FD/013'],
    //     ['no_tiket' => 515026313,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-GMB-FE/052'],
    //     ['no_tiket' => 511412353,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-GMB-FG/053'],
    //     ['no_tiket' => 513258776,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-GMB-FG/053'],
    //     ['no_tiket' => 515696167,	'regu' => 'STM-KYG-PROV/02',	'jenis_order' => 8, 'odp' =>	'ODP-KYG-FAF/005'],
    //     ['no_tiket' => 510961658,	'regu' => 'STM-KYG-PROV/02',	'jenis_order' => 8, 'odp' =>	'ODP-KYG-FB/012'],
    //     ['no_tiket' => 512752341,	'regu' => 'STM-KYG-PROV/02',	'jenis_order' => 8, 'odp' =>	'ODP-KYG-FX/004'],
    //     ['no_tiket' => 512859968,	'regu' => 'SODP-AGB-LUL/01',	'jenis_order' => 8, 'odp' =>	'ODP-LUL-FAH/015'],
    //     ['no_tiket' => 514082659,	'regu' => 'SODP-AGB-LUL/01',	'jenis_order' => 8, 'odp' =>	'ODP-LUL-FAH/015'],
    //     ['no_tiket' => 513401527,	'regu' => 'SODP-AGB-LUL/01',	'jenis_order' => 8, 'odp' =>	'ODP-LUL-FAM/002'],
    //     ['no_tiket' => 513847651,	'regu' => 'SODP-AGB-LUL/01',	'jenis_order' => 8, 'odp' =>	'ODP-LUL-FL/067'],
    //     ['no_tiket' => 513029044,	'regu' => 'SODP-AGB-LUL/01',	'jenis_order' => 8, 'odp' =>	'ODP-LUL-FX/041'],
    //     ['no_tiket' => 511934820,	'regu' => 'SODP-AGB-LUL/01',	'jenis_order' => 8, 'odp' =>	'ODP-LUL-FZ/047'],
    //     ['no_tiket' => 517204422,	'regu' => 'CUI-BLC-PT2/03',	'jenis_order' => 8, 'odp' =>	'ODP-PGT-FB/024'],
    //     ['no_tiket' => 516886292,	'regu' => 'CUI-BLC-PT2/03',	'jenis_order' => 8, 'odp' =>	'ODP-PGT-FF/006'],
    //     ['no_tiket' => 511450367,	'regu' => 'TBN-PLE-SODP/08',	'jenis_order' => 8, 'odp' =>	'ODP-PLE-FA/008'],
    //     ['no_tiket' => 512160062,	'regu' => 'TBN-PLE-SODP/08',	'jenis_order' => 8, 'odp' =>	'ODP-PLE-FH/009'],
    //     ['no_tiket' => 513010323,	'regu' => 'TBN-PLE-SODP/08',	'jenis_order' => 8, 'odp' =>	'ODP-PLE-FH/009'],
    //     ['no_tiket' => 511115808,	'regu' => 'TBN-PLE-SODP/08',	'jenis_order' => 8, 'odp' =>	'ODP-PLE-FJ/014'],
    //     ['no_tiket' => 515649762,	'regu' => 'CUI-BLC-PT2/03',	'jenis_order' => 8, 'odp' =>	'ODP-STI-FA/044'],
    //     ['no_tiket' => 517918261,	'regu' => 'CUI-BLC-PT2/03',	'jenis_order' => 8, 'odp' =>	'ODP-STI-FA/044'],
    //     ['no_tiket' => 517998373,	'regu' => 'CUI-BLC-PT2/03',	'jenis_order' => 8, 'odp' =>	'ODP-STI-FA/044'],
    //     ['no_tiket' => 512059211,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FAC/16'],
    //     ['no_tiket' => 515443979,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FAF/023'],
    //     ['no_tiket' => 514214109,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FAM/018'],
    //     ['no_tiket' => 513400070,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FAN/018'],
    //     ['no_tiket' => 510970508,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FAQ/014'],
    //     ['no_tiket' => 513641060,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FAT/122'],
    //     ['no_tiket' => 517207901,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FAW/042'],
    //     ['no_tiket' => 514199219,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FBB/042'],
    //     ['no_tiket' => 511883577,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FBE/134'],
    //     ['no_tiket' => 516151691,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FBK/013'],
    //     ['no_tiket' => 511965096,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FBK/075'],
    //     ['no_tiket' => 517573863,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FBS/014'],
    //     ['no_tiket' => 517164205,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FBY/034'],
    //     ['no_tiket' => 511699086,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FCC/014'],
    //     ['no_tiket' => 517517547,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FE/140'],
    //     ['no_tiket' => 512275959,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FH/013'],
    //     ['no_tiket' => 513470452,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FL/003'],
    //     ['no_tiket' => 512690484,	'regu' => 'SODP-UT-ULI1/15',	'jenis_order' => 8, 'odp' =>	'ODP-ULI-FL/03']
    //     ];

    //     DB::transaction(function() use($data) {
    //       foreach($data as $d)
    //       {
    //         $alpro = explode("/", $d['odp']);
    //         $alpro = explode("-", $alpro[0]);

    //         $sector_id = $sector_name = null;

    //         $check_alpro = DB::table('maintenance_sector As ms')
    //         ->leftJoin('maintenance_sector_alpro As msa', 'ms.id', '=', 'msa.sector_id')
    //         ->select('ms.*', 'msa.alpro')
    //         ->where('msa.alpro', $alpro[1].'-'.$alpro[2])->first();
    //         if ($check_alpro) {
    //           $sector_id = $check_alpro->id;
    //           $sector_name = $check_alpro->sector;
    //         }
    //         $auth = session('auth');
    //         $no_tiket=$d['no_tiket'];
    //         $jenis = DB::table('maintenance_jenis_order')->where('id', $d['jenis_order'])->first();
    //         $sto = DB::table('maintenance_datel')->where('sto', $alpro[1])->first();
    //         $regu = DB::table('regu')->where('uraian', $d['regu'])->first();
    //         // dd($alpro, $sector_id, $sector_name, $jenis, $sto, $regu, $no_tiket);
    //         $id_mt = DB::table('maintaince')->insertGetId([
    //           "no_tiket"          =>$no_tiket,
    //           "jenis_order"       =>$d['jenis_order'],
    //           "nama_order"        =>$jenis->nama_order,
    //           "dispatch_regu_id"  =>@$regu->id_regu,
    //           "dispatch_regu_name"=>@$regu->uraian,
    //           "username1"         =>@$regu->nik1,
    //           "username2"         =>@$regu->nik2,
    //           "created_at"        =>DB::raw('now()'),
    //           "created_by"        =>$auth->id_karyawan,
    //           "sto"               =>@$sto->sto,
    //           "sector_id"         =>$sector_id,
    //           "nama_sector"       =>$sector_name,
    //           "kandatel"          =>@$sto->datel,
    //           "nama_odp"          =>$d['odp'],
    //         ]);

    //         DB::table('maintenance_dispatch_log')->insert([
    //           "maintenance_id"  =>$id_mt,
    //           "regu_id"         =>@$regu->id_regu,
    //           "regu_name"       =>@$regu->uraian,
    //           "ts_dispatch"     =>DB::raw('now()'),
    //           "dispatch_by"     =>$auth->id_karyawan,
    //           "action"     =>"dispatch"
    //         ]);
    //       }
    //     });
    // }

    //tech list order

     public function hometech()
    {
      $auth = session('auth');
      $cekabsen = DB::table('user')->where('id_user', $auth->id_karyawan)->first();
      $cektech = DB::table('regu')->select('regu.*', 'user.u_nama as nama_TL')->leftjoin('user', 'regu.TL','=', 'user.id_karyawan')
      ->where([
        ['regu.ACTIVE', 1],
        ['regu.jenis_regu', 'MARINA']
      ])
      ->where(function($q) use($auth) {
        $q->where('nik1', $auth->id_karyawan)
        ->orWhere('nik2', $auth->id_karyawan);
      })
      ->first();

      // dd($cektech);
      // dd($cekabsen);
      if(empty($cekabsen) || in_array('',[$cekabsen->u_no_ktp, $cekabsen->u_alamat, $cekabsen->u_no_gsm1])){
          $msg = "DATA DIRI ANDA TIDAK LENGKAP, SILAHKAN MELENGKAPI DIRI DI MENU PROFILE ";
        return view('tech.absen', compact('msg'));
      }elseif($cekabsen){
        //cek absen teknisi maintenance
        if(!count($cektech) || substr($cekabsen->u_mt_verif_absen, 0, 10)==date('Y-m-d')){
          $order_rw=DB::table('maintaince')
          ->where(function($query){
            $query->whereNull('status')
            ->orWhere('status', 'ogp');
          })
          ->Where('isHapus', 0)
          ->where(function($query) use($auth) {
            $query->where('username1', $auth->id_user)->orWhere('username2', $auth->id_user)
            ->orWhere('username3', $auth->id_user)->orWhere('username4', $auth->id_user);
          })->whereNull('refer')
          ->get();

            $order = 0;

            foreach($order_rw as $v)
            {
              if( (substr($v->timeplan, 0, 10) != '0000-00-00' && strtotime($v->timeplan) <= strtotime(date('Y-m-d') ) ) || substr($v->timeplan, 0, 10) == '0000-00-00')
              {
                $order += 1;
              }
            }

          $closed=DB::table('maintaince')->where('status', 'close')->where(function($query) use($auth) {
                $query->where('username1', $auth->id_user)->orWhere('username2', $auth->id_user)
                    ->orWhere('username3', $auth->id_user)->orWhere('username4', $auth->id_user);
            })->whereNull('refer')->where('tgl_selesai', 'like', date('Y-m').'%')->count();

          $kendala=DB::table('maintaince')->where('status', 'like', 'kendala%')->where(function($query) use($auth) {
                $query->where('username1', $auth->id_user)->orWhere('username2', $auth->id_user)
                    ->orWhere('username3', $auth->id_user)->orWhere('username4', $auth->id_user);
            })->whereNull('refer')->where('tgl_selesai', 'like', date('Y-m').'%')->count();

          $cancel=DB::table('maintaince')->where('status', 'cancel order')->where(function($query) use($auth) {
                $query->where('username1', $auth->id_user)->orWhere('username2', $auth->id_user)
                    ->orWhere('username3', $auth->id_user)->orWhere('username4', $auth->id_user);
            })->whereNull('refer')->where('tgl_selesai', 'like', date('Y-m').'%')->count();

          $decline = DB::table('maintaince')->where(function($query) use($auth) {
            $query->where('username1', $auth->id_user)->orWhere('username2', $auth->id_user)
                ->orWhere('username3', $auth->id_user)->orWhere('username4', $auth->id_user);
        })->whereIN('verify', [4, 5])
        ->where('isHapus', 0)
        ->count();
          return view('hometeknisi', compact('order', 'closed', 'kendala', 'cancel', 'decline'));
        }else{
          $msg = 0;
          if(substr($cekabsen->u_mt_absen, 0, 10) == date('Y-m-d')){
            $msg = "SILAHKAN MENUNGGU VERIFIKASI TL ANDA $cektech->TL/$cektech->nama_TL";
          }
          return view('tech.absen', compact('msg'));
        }
      }else{
        return view('hometeknisi');
      }
    }
    public function listabsen()
    {
//       {#305 ▼
//   +"id_user": "16000538"
//   +"password": "7bcffcbd59a2f6194853d4de10143c8e"
//   +"id_karyawan": "16000538"
//   +"nama": "MUHAMMAD RIZKI"
//   +"level": 10
//   +"maintenance_level": 0
//   +"nama_instansi": "SPM"
//   +"jenis_verif": 0
//   +"witel": null
//   +"fiberzone": null
//   +"loginMethod": "WASAKA"
// }
      $auth = session('auth');
      $data = DB::table('maintenance_absen_log as a')->select('*', DB::raw('(select count(*) from maintenance_absen_log where id_user=a.id_user and SUBSTRING(created_at, 1, 10)=SUBSTRING(a.created_at, 1, 10)) as count'))->where('created_at', 'like', date('Y-m').'%')->where('id_user', $auth->id_user)->where('status',2)->orderBy('created_at', 'desc')->get();
      // dd($data);
      return view('listabsen', compact('data'));
    }

    public function listorder($jenis)
    {
      $auth = session('auth');
      if($jenis == 'order'){
        $status = " and (m.status is NULL or m.status='ogp') AND refer is null ";
      }elseif($jenis == 'decline'){
        $status = " and verify IN(4, 5) ";
      }else{
        $status = " and m.status like '".$jenis."%' and refer is null and tgl_selesai like '".date('Y-m')."%'";
      }
      // $status = " and m.status is NULL and refer is null";
      $condition = "m.isHapus = 0 AND (m.username1 = '".$auth->id_user."' OR m.username2 = '".$auth->id_user."' OR m.username3 = '".$auth->id_user."' OR m.username4 = '".$auth->id_user."')";

      $list_raw = DB::select('SELECT m.*, m.nama_odp as nama_odp_m,concat("DECLINE") as last_stts
        FROM maintaince m
        LEFT JOIN regu mr on m.dispatch_regu_id = mr.id_regu
        WHERE '.$condition.' '.$status.' order by m.id desc
      ');

      $list = $waiting_list = [];

      foreach($list_raw as $k => $v)
      {
        if($jenis == 'order')
        {
          if( (substr($v->timeplan, 0, 10) != '0000-00-00' && strtotime($v->timeplan) <= strtotime(date('Y-m-d') ) ) || substr($v->timeplan, 0, 10) == '0000-00-00')
          {
            $list[$k] = $v;
          }
          else
          {
            $waiting_list[$k] = $v;
          }

          $count_total = count($list);

          if($count_total == 0)
          {
            foreach($list as $k => $v)
            {
              if(!isset($list[$k]) )
              {
                if(substr($v->timeplan, 0, 10) != '0000-00-00' && strtotime($v->timeplan) <= strtotime(date('Y-m-d') ) )
                {
                  $date1 = date_create($v->timeplan);
                  $date2 = date_create(date('Y-m-d'));
                  $diff  = date_diff($date1, $date2);

                  $diff_day =  $diff->format("%a");

                  if($diff_day == 1 && $v->jenis_order == 17)
                  {
                    $list[$k] = $v;
                  }
                  else
                  {
                    $waiting_list[$k] = $v;
                  }
                }
              }
            }
          }
        }
        else
        {
          $list[$k] = $v;
        }
      }

      return view('tech.list', compact('list', 'waiting_list') );
    }

    public function tech()
    {
      /*
      $auth = session('auth');
      $status = " and (m.status is NULL OR m.status != 'close')";
      $condition = "(m.username1 = '".$auth->id_karyawan."' OR m.username2 = '".$auth->id_karyawan."')";
      $list = DB::select('
        SELECT m.*, pl.nama_odp, pl.kordinat_odp, m.nama_odp as nama_odp_m
        FROM maintaince m left join psb_laporan pl on m.psb_laporan_id = pl.id
        WHERE '.$condition.' '.$status.' and refer is null order by m.id desc
      ');
      return view('tech.list', compact('list'));
      */
      $auth = session('auth');
      $cektl = DB::table('regu')->where('TL', $auth->id_karyawan)->first();
      if(count($cektl)){
        $data = DB::select('select mr.*,
          k.id_user as idt1,
          k.u_mt_absen as absent1,
          k.u_nama as namat1,
          k.u_mt_verif_absen as verift1,
          kk.id_user as idt2,
          kk.u_mt_absen as absent2,
          kk.u_nama as namat2,
          kk.u_mt_verif_absen as verift2 from regu mr
          left join user k on mr.nik1 = k.id_user
          left join user kk on mr.nik2 = kk.id_user where mr.ACTIVE=1 and mr.TL="'.$auth->id_karyawan.'"
        ');
        return view('tech.absen', compact('msg', 'data'));
      }else{
        $cekabsen = DB::table('user')->where('id_karyawan', $auth->id_karyawan)->first();
        if(!$cekabsen){
          //cek absen teknisi maintenance
          $cektech = DB::table('regu')->select('regu.*', 'user.u_nama as nama_TL')->leftjoin('user', 'regu.TL','=', 'user.id_karyawan')
          ->where([
            ['regu.ACTIVE', 1],
            ['regu.jenis_regu', 'MARINA']
            ])
          ->where(function($q) use($auth) {
            $q->where('nik1', $auth->id_karyawan)
            ->orWhere('nik2', $auth->id_karyawan);
          })
          ->first();
          if(!count($cektech) || substr($cekabsen->u_mt_verif_absen, 0, 10)==date('Y-m-d')){
            $status = " AND ( (m.status is NULL OR m.status = 'ogp') and refer is null OR verify = 4 )";
            // $status = " and m.status is NULL and refer is null";
            $condition = "m.isHapus = 0 AND (m.username1 = '".$auth->id_karyawan."' OR m.username2 = '".$auth->id_karyawan."' OR m.username3 = '".$auth->id_karyawan."' OR m.username4 = '".$auth->id_karyawan."')";
            $list = DB::select(
              'SELECT CONCAT("DECLINE") as last_stts, m.*, m.nama_odp as nama_odp_m
              FROM maintaince m
              LEFT JOIN regu mr on m.dispatch_regu_id = mr.id_regu
              WHERE '.$condition.' '.$status.' order by m.id desc
            ');
            return view('tech.list', compact('list'));
          }else{
            $msg = 0;
            if(substr($cekabsen->u_mt_absen, 0, 10) == date('Y-m-d'))
              $msg = "SILAHKAN MENUNGGU VERIFIKASI TL ANDA $cektech->TL/$cektech->nama_TL";

            return view('tech.absen', compact('msg'));
          }

        }else{
          //teknisi prov
          $status = " AND ( (m.status is NULL OR m.status = 'ogp') and refer is null OR verify = 4 )";
          // $status = " and m.status is NULL and refer is null";
          $condition = "m.isHapus = 0 AND ( mr.nik1 = '".$auth->id_karyawan."' OR mr.nik2 = '".$auth->id_karyawan."')";
          $list = DB::select(
            'SELECT CONCAT("DECLINE") as last_stts, m.*,  m.nama_odp as nama_odp_m
            FROM maintaince m
            LEFT JOIN regu mr on m.dispatch_regu_id = mr.id_regu
            WHERE '.$condition.' '.$status.' order by m.id desc
          ');
          // dd($list);
          return view('tech.list', compact('list'));
        }
      }
    }
    public function updated()
    {
        $auth = session('auth');
        $status = " and m.status = 'close' and m.tgl_selesai like '%".date('Y-m')."%'";
        $condition = "m.isHapus = 0 AND (m.username1 = '".$auth->id_karyawan."' OR m.username2 = '".$auth->id_karyawan."' OR m.username3 = '".$auth->id_karyawan."' OR m.username4 = '".$auth->id_karyawan."')";
        $list = DB::select('
          SELECT m.*, m.nama_odp as nama_odp_m
          FROM maintaince m
          LEFT JOIN regu mr on m.dispatch_regu_id = mr.id_regu
          WHERE '.$condition.' '.$status.' order by m.id desc
        ');
        return view('tech.list', compact('list'));
    }

    //to form progress
    public function update($id)
    {
      // $foto = $this->photoCommon;
      $exists = DB::select('
        SELECT m.*,m.nama_odp as odp_nama, mr.TL, pl.id_tbl_mj, u.u_nama as psbcreated
        FROM maintaince m
        left join psb_laporan pl on m.psb_laporan_id = pl.id
        LEFT JOIN regu mr on m.dispatch_regu_id=mr.id_regu
        LEFT JOIN user u on m.created_by = u.id_karyawan
        WHERE m.id = ?
      ', [
        $id
      ]);

      if($exists[0]->isHapus == 1)
      {
        return redirect()->back()
        ->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Order Sudah Terhapus']
        ]);
      }

      if( (substr($exists[0]->timeplan, 0, 10) != '0000-00-00' && strtotime($exists[0]->timeplan) > strtotime(date('Y-m-d') ) ) )
      {
        $now = time(); // or your date as well
        $your_date = strtotime($exists[0]->timeplan);
        $datediff = $your_date - $now;
        $result = round($datediff / (60 * 60 * 24) );

        return redirect()->back()
        ->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Tunggu '.$result.' Hari Untuk Mengerjakan ('.$exists[0]->timeplan.')']
        ]);
      }

      $log = DB::table('maintenance_note')
      ->leftjoin('user', 'pelaku', '=', 'id_karyawan')
      ->select('maintenance_note.*', 'user.u_nama as nama', 'maintenance_note.ts as ts_dispatch', 'maintenance_note.pelaku as dispatch_by', 'maintenance_note.status as action', 'maintenance_note.catatan')
      ->where('mt_id', $id)
      ->orderBy('id', 'asc')
      ->get();

      $action_cause = DB::table('maintenance_cause_action')->select('action_cause as text', 'action_cause as id', 'status')->get();
      $gamas_cause = DB::table('maintenance_gamas_cause')->select('gamas_cause as text', 'id as id')->get();
      if (count($exists)) {
        $data = $exists[0];
        if( in_array($data->jenis_order, [8, 10, 17])){
          if(!$data->step_id){
            $data = DB::table('maintaince')->where([
              ['id', $id],
              ['isHapus', 0]
            ])->first();
            $list = DB::table('maintenance_validasi_port')->where('maintenance_id', $id)->get();
            return view('tech.formValidasiPort', compact('data', 'list'));
          }else if($data->step_id== 1){
            $data = DB::table('maintaince')->where([
              ['id', $id],
              ['isHapus', 0]
            ])->first();
            $list = DB::table('maintenance_reboundary')->where('maintenance_id', $id)->get();
            return view('tech.reboundary', compact('data', 'list'));
          }else if($data->step_id== 2){
            $data = DB::table('maintaince')->where([
              ['id', $id],
              ['isHapus', 0]
            ])->first();
            $list = DB::table('maintenance_validasi_port')->where('maintenance_id', $id)->get();
            return view('tech.formValidasiPort', compact('data', 'list'));
          }
        }
        // if($data->jenis_order == 3 ){
        //   $foto = $this->photoOdpLoss;
        // }else if($data->jenis_order == 4){
        //   $foto = $this->photoRemo;
        // }else if($data->jenis_order == 5){
        //   $foto = $this->photophotoUtilitas;
        // }else if($data->jenis_order == 8){
        //   $foto = $this->photoOdpSehat;
        // }else if($data->jenis_order == 1 || $data->jenis_order == 19){
        //   $foto = $this->photoBenjar;
        // }else if($data->jenis_order == 10){
        //   $foto = $this->photoValidasi;
        // }else if($data->jenis_order == 17){
        //   $foto = $this->photoNormalisasi;
        // }else if($data->jenis_order == 9){
        //   $foto = $this->photoReboundary;
        // }else if($data->jenis_order == 20){
        //   $foto = $this->photoODCSehat;
        // }else if($data->jenis_order == 24){
        //   $foto = $this->photoIXSA;
        // }else if($data->jenis_order == 2){
        //   $foto = $this->photoGamas;
        // }

        if($data->action_cause){
          $action_cause = DB::table('maintenance_cause_action')
            ->select('action_cause as text', 'action_cause as id', 'status')
            ->where('status', $data->status)->get();
        }
      }
      else{
        $data = new \stdClass();
      }
      // dd($data);
      /*
      $materials = DB::select('
          SELECT
            i.*, i.uraian as nama_item,
            COALESCE(m.qty, 0) AS qty
          FROM khs_maintenance i
          LEFT JOIN
            maintaince_mtr m
            ON i.id_item = m.id_item
            AND m.maintaince_id = ?
          ORDER BY m.id_item
        ', [
          $id
        ]);
        */
      $saldos = Saldo::getSubmitedMaterial(session('auth')->id_karyawan, $id);
      $saldo = Saldo::getSisaMaterial(session('auth')->id_karyawan, $id);

      foreach($saldo as $s){
        foreach($saldos as $ss){
          // echo $s->rfc."-".$s->id_item." vs ".$ss->rfc."-".$ss->id_item;
          if($s->bantu != $ss->bantu){
            $saldo[] = $ss;
          }
        }
      }

      $materials = DB::select('SELECT mmp.id as id_mmp, program_id, i.*, i.uraian as nama_item, COALESCE(m.qty, 0) AS qty
      FROM maintenance_mtr_program mmp
      LEFT JOIN khs_maintenance i ON i.id_item = mmp.id_item
      LEFT JOIN maintaince_mtr m ON m.id_item = i.id_item AND m.maintaince_id = ?
      WHERE i.id IS NOT NULL AND program_id = ? GROUP BY i.id', [
        $id, $data->jenis_order
      ]);
      // dd($saldo);
      // dd($materials);
      foreach($data as $key => $new_data)
      {
        if(json_encode($new_data) != false){
          $renew_data[$key] = $new_data;
        }else{
          $wrong[] = $new_data;
          $renew_data[$key] = preg_replace('/[[:^print:]]/', '', $new_data);
        }
      }

      $nte= [];
      if(in_array($data->jenis_order, [6, 9]))
      {
        $check_nte = DB::table('maintaince')
        ->leftJoin('psb_laporan', 'maintaince.psb_laporan_id', '=', 'psb_laporan.id')
        ->leftJoin('nte', 'psb_laporan.id_tbl_mj', '=', 'nte.id_dispatch_teknisi')
        ->select('nte.id_dispatch_teknisi')
        ->where([
          ['psb_laporan.id', $data->psb_laporan_id],
          ['maintaince.isHapus', 0]
        ])
        ->first();
        // dd($check_nte);
        $auth = session('auth');
        if (count($check_nte)>0) {
          $nte = DB::SELECT('SELECT nte.sn as id, nte.sn as text, COALESCE(nte.qty, 0) AS qty, nte.jenis_nte, nte_jenis.nte_jenis_kat FROM nte LEFT JOIN nte_jenis ON nte.jenis_nte = nte_jenis.nte_jenis WHERE (nte.id_dispatch_teknisi = "'.$check_nte->id_dispatch_teknisi.'" OR nte.status = 2) AND (nte.nik1 = "'.$auth->id_karyawan.'" OR nte.nik2 = "'.$auth->id_karyawan.'")');
        } else {
          $nte = DB::SELECT('SELECT nte.sn as id, nte.sn as text, COALESCE(nte.qty, 0) AS qty, nte.jenis_nte, nte_jenis.nte_jenis_kat FROM nte LEFT JOIN nte_jenis ON nte.jenis_nte = nte_jenis.nte_jenis WHERE nte.status = 2 AND (nte.nik1 = "'.$auth->id_karyawan.'" OR nte.nik2 = "'.$auth->id_karyawan.'")');
        }
      }

      $get_field_photo = DB::table('maintenance_evident')->where('program_id', $data->jenis_order)->get();
      $get_field_photo = json_decode(json_encode($get_field_photo), TRUE);

      $penyebab = DB::table('maintenance_penyebab')->select('*', 'penyebab as text')->get();

      if(session('auth')->nama_instansi == 'TELKOM')
      {
        foreach($get_field_photo as $k => &$v)
        {
          if($v['evident'] == 'RFC')
          {
            unset($get_field_photo[$k]);
          }
        }
        unset($v);
      }
      // dd($renew_data, $data, $wrong);
      // $log = array_merge(json_decode($komen, true), json_decode($dispatch_log, true));

      // $col = array_column( $log, "ts_dispatch" );
      // array_multisort( $col, SORT_ASC, $log );
      // dd($log);
      $username1 = DB::Table('user')->Select(DB::RAW("CONCAT(u_nama, ' (', id_karyawan, ')') As title"))->where('id_karyawan', $data->username1)->Orwhere('id_karyawan', $data->username2)->get();

      foreach($username1 as $v)
      {
        $un[] = $v->title;
      }

      $get_field = DB::table('maintenance_jenis_order')->select('field_laporan_order')->where('nama_order', $data->nama_order)->first();

      $get_field = json_decode($get_field->field_laporan_order);

      if($get_field)
      {
        foreach($get_field as $k => $v)
        {
          if(@$v->visible != true)
          {
            unset($get_field[$k]);
          }
        }
      }

      $data_bot_odp_alpro = DB::select("SELECT * FROM master_OdpCare WHERE maintenance_id = $id");
      $foto_bot = [];

      if($data_bot_odp_alpro)
      {
        $token = '5774572588:AAF4JN_QEtSdCLgPchuzX0FMVgmI2dm1A5w';

        foreach($data_bot_odp_alpro as $v)
        {
          $generate_foto = json_decode($v->file_id);

          $data_foto = http_build_query([
          'file_id'    => $generate_foto->file_id,
          ]);

          $url = "https://api.telegram.org/bot".$token."/getFile?".$data_foto;

          $res = @file_get_contents($url);
          $res = json_decode($res)->result;
          $foto_bot[$token]['foto'][] = $res->file_path;
          $foto_bot[$token]['base64'][] = base64_encode(@file_get_contents("https://api.telegram.org/file/bot".$token.'/'.$res->file_path) );
        }
      }

      $check_regu = null;

      if(session('auth')->maintenance_level == 0)
      {
        $check_regu = DB::table('regu')
        ->Where('id_regu', $renew_data['dispatch_regu_id'])
        ->Where(function($join){
          $join->Where('nik1', session('auth')->id_user)
          ->OrWhere('nik2', session('auth')->id_user);
        })
        ->first();
      }

      return view('tech.progress2', ['data2' => $renew_data], compact('data', 'materials', 'action_cause', 'gamas_cause', 'log', 'penyebab', 'saldo', 'nte', 'un', 'get_field', 'get_field_photo', 'foto_bot', 'check_regu') );
    }
      private function handleFileUpload($request, $id, $foto)
      {
        foreach($foto as $name) {
          $input = 'photo-'.$name;
          if ($request->hasFile($input)) {
            //dd($input);
            $path = public_path().'/upload/maintenance/'.$id.'/';
            if (!file_exists($path)) {
              if (!mkdir($path, 0770, true))
                return 'gagal menyiapkan folder foto evidence';
            }
            $file = $request->file($input);
            $ext = 'jpg';
            //TODO: path, move, resize
            try {
              $moved = $file->move("$path", "$name.$ext");
              $img = new \Imagick($moved->getRealPath());
              $img->scaleImage(100, 150, true);
              $img->writeImage("$path/$name-th.$ext");
            }
            catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
              return 'gagal menyimpan foto evidence '.$name;
            }
          }
        }
      }

      public function saveProgres(Request $request, $id){
        $auth = session('auth');
        $rules = array(
          'status' => 'required'
        );
        $messages = [
          'status.required' => 'Silahkan pilih "Status" Bang!',
          'action_cause.required' => 'Silahkan pilih "Action/Cause" Bang!',
          'action.required' => 'Silahkan isi "Action" Bang!',
          'splitter.required' => 'Silahkan pilih "Jenis Splitter" Bang!',
          'flag_Sebelum.required' => 'Silahkan Isi Foto "Sebelum" Bang!',
          'flag_Progress.required' => 'Silahkan Isi Foto "Progress" Bang!',
          'flag_Sesudah.required' => 'Silahkan Isi Foto "Sesudah" Bang!',

          'flag_ODP.required' => 'Silahkan Isi Foto "ODP" Bang!',
          'flag_ODP-dan-Redaman-GRID.required' => 'Silahkan Isi Foto "ODP-dan-Redaman-GRID" Bang!',
          'flag_Pengukuran.required' => 'Silahkan Isi Foto "Pengukuran" Bang!',
          'flag_Pelanggan_GRID.required' => 'Silahkan Isi Foto "Pelanggan_GRID" Bang!',
          'flag_BA-Remo.required' => 'Silahkan Isi Foto "BA-Remo" Bang!',
          'flag_Foto-GRID.required' => 'Silahkan Isi Foto "Foto-GRID" Bang!',
          'flag_Denah-Lokasi.required' => 'Silahkan Isi Foto "Denah-Lokasi" Bang!',
          'redaman_awal.required' => 'Silahkan Isi kolom "Redaman Sebelum" Bang!',
          'redaman_akhir.required' => 'Silahkan Isi kolom "Redaman Sesudah" Bang!',
          'penyebab_id.required' => 'Silahkan Pilih "Penyebab" Bang!'
        ];


        $exists = DB::select('
          SELECT *
          FROM maintaince
          WHERE isHapus = 0 AND id = ?
        ',[
          $id
        ]);

        $saldo = json_decode($request->input('saldo'));
        $materials = json_decode($request->input('materials'));
        if (count($exists)) {
          $data = $exists[0];
          //cek ogp gak boleh lebih dr 1
          if($request->input('status') == "ogp" &&  session('auth')->maintenance_level == 0){
            $cekogp = DB::select('select * from maintaince where isHapus = 0 AND status = "ogp" and (dispatch_regu_id = ?)', [$data->dispatch_regu_id]);
            if (count($cekogp)) {
              return redirect()->back()
              ->withInput($request->all())
              ->with('alerts', [
                ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan (Tidak boleh update OGP/selesaikan orderan OGP yg ada)']
              ]);
            }
          }
          //apabila jenis order = 19 / Penutupan ODP
          // if(in_array($data->jenis_order, ['8','10','17','9','19']) && $request->status=="close"){
          //   $cekvalin = DB::table('maintenance_valins')->where('maintenance_id', $id)->first();
          //   if(!$cekvalin){
          //     return redirect()->back()
          //       ->withInput($request->all())
          //       ->with('alerts', [
          //         ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan (Silahkan input ID Valins)']
          //       ]);
          //   }
          // }

          if($request->valins_awal)
          {
            $fitur = DB::table('fitur')->first();
            if($fitur->check_valins == 1)
            {
              $check_valins = GraberController::grab_valins($request->valins_awal);
              $jenis_valins = 0;

              if($check_valins)
              {
                DB::Table('bank_valins')->where([
                  ['app', 'MARINA'],
                  ['id_wo', $data->id],
                  ['jenis_valins', $jenis_valins]
                ])->delete();

                if($check_valins && $check_valins[1]['odp'] == $data->nama_odp)
                {
                  foreach ($check_valins as $k => $v_val)
                  {
                    DB::table('bank_valins')->insert([
                      'app'          => 'MARINA',
                      'id_wo'        => $data->id,
                      'jenis_valins' => $jenis_valins,
                      'port_odp'     => $v_val['port_odp'],
                      'id_valins'    => $v_val['id_valins'],
                      'time_valins'  => $v_val['time_valins'],
                      'odp'          => $v_val['odp'],
                      'onu_id'       => $v_val['onu_id'],
                      'onu_sn'       => $v_val['onu_sn'],
                      'sip1'         => $v_val['sip1'],
                      'sip2'         => $v_val['sip2'],
                      'no_hsi1'      => $v_val['no_hsi1'],
                      'created_by'   => session('auth')->id_karyawan
                    ]);

                    DB::table('bank_valins_log')->insert([
                      'app'          => 'MARINA',
                      'id_wo'        => $data->id,
                      'jenis_valins' => $jenis_valins,
                      'port_odp'     => $v_val['port_odp'],
                      'id_valins'    => $v_val['id_valins'],
                      'time_valins'  => $v_val['time_valins'],
                      'odp'          => $v_val['odp'],
                      'onu_id'       => $v_val['onu_id'],
                      'onu_sn'       => $v_val['onu_sn'],
                      'sip1'         => $v_val['sip1'],
                      'sip2'         => $v_val['sip2'],
                      'no_hsi1'      => $v_val['no_hsi1'],
                      'created_by'   => session('auth')->id_karyawan
                    ]);
                  }
                }
                else
                {
                  return redirect()->back()
                  ->withInput($request->all())
                  ->with('alerts', [
                    ['type' => 'danger', 'text' => '<strong>GAGAL</strong> ODP Tidak Sama Dengan ODP Valins)']
                  ]);
                }
              }
            }
          }

          if($request->valins_akhir)
          {
            $fitur = DB::table('fitur')->first();
            if($fitur->check_valins == 1)
            {
              $check_valins = GraberController::grab_valins($request->valins_akhir);
              $jenis_valins = 1;

              if($check_valins)
              {
                DB::Table('bank_valins')->where([
                  ['app', 'MARINA'],
                  ['id_wo', $data->id],
                  ['jenis_valins', $jenis_valins]
                ])->delete();

                if($check_valins && $check_valins[1]['odp'] == $data->nama_odp)
                {
                  foreach ($check_valins as $k => $v_val)
                  {
                    DB::table('bank_valins')->insert([
                      'app'          => 'MARINA',
                      'id_wo'        => $data->id,
                      'jenis_valins' => $jenis_valins,
                      'port_odp'     => $v_val['port_odp'],
                      'id_valins'    => $v_val['id_valins'],
                      'time_valins'  => $v_val['time_valins'],
                      'odp'          => $v_val['odp'],
                      'onu_id'       => $v_val['onu_id'],
                      'onu_sn'       => $v_val['onu_sn'],
                      'sip1'         => $v_val['sip1'],
                      'sip2'         => $v_val['sip2'],
                      'no_hsi1'      => $v_val['no_hsi1'],
                      'created_by'   => session('auth')->id_karyawan
                    ]);

                    DB::table('bank_valins_log')->insert([
                      'app'          => 'MARINA',
                      'id_wo'        => $data->id,
                      'jenis_valins' => $jenis_valins,
                      'port_odp'     => $v_val['port_odp'],
                      'id_valins'    => $v_val['id_valins'],
                      'time_valins'  => $v_val['time_valins'],
                      'odp'          => $v_val['odp'],
                      'onu_id'       => $v_val['onu_id'],
                      'onu_sn'       => $v_val['onu_sn'],
                      'sip1'         => $v_val['sip1'],
                      'sip2'         => $v_val['sip2'],
                      'no_hsi1'      => $v_val['no_hsi1'],
                      'created_by'   => session('auth')->id_karyawan
                    ]);
                  }
                }
                else
                {
                  return redirect()->back()
                  ->withInput($request->all())
                  ->with('alerts', [
                    ['type' => 'danger', 'text' => '<strong>GAGAL</strong> ODP Tidak Sama Dengan ODP Valins)']
                  ]);
                }
              }
            }
          }

          // if($request->status=="close")
          // {
          //   $cekvalin = DB::table('bank_valins')->where([
          //     ['id_wo', $id],
          //     ['app', 'MARINA']
          //   ])->first();

          //   $fitur = DB::table('fitur')->first();

          //   if($fitur->check_valins == 1 && ($request->valins_awal || $request->valins_akhir) )
          //   {
          //     if(!$cekvalin){
          //       return redirect()->back()
          //       ->withInput($request->all())
          //       ->with('alerts', [
          //         ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan (Silahkan input ID Valins)']
          //       ]);
          //     }
          //   }
          // }
          // $foto = $this->photoCommon;
          // if($data->jenis_order == 3){
          //   $foto = $this->photoOdpLoss;
          // }else if($data->jenis_order == 4){
          //   $foto = $this->photoRemo;
          // }else if($data->jenis_order == 5){
          //   $foto = $this->photophotoUtilitas;
          // }else if($data->jenis_order == 8){
          //   $foto = $this->photoOdpSehat;
          // }else if($data->jenis_order == 1 || $data->jenis_order == 19){
          //   $foto = $this->photoBenjar;
          // }else if($data->jenis_order == 10){
          //   $foto = $this->photoValidasi;
          // }else if($data->jenis_order == 17){
          //   $foto = $this->photoNormalisasi;
          // }else if($data->jenis_order == 9){
          //   $foto = $this->photoReboundary;
          // }else if($data->jenis_order == 20){
          //   $foto = $this->photoODCSehat;
          // }else if($data->jenis_order == 24){
          //   $foto = $this->photoIXSA;
          // }else if($data->jenis_order == 2){
          //   $foto = $this->photoGamas;
          // }
          // $this->handleFileUpload($request, $id, $foto);

          // if($request->status == 'close' && $data->verify == null && $request->action_cause != "CANCEL ORDER"){
          //   $rules = array_merge($rules, array('splitter' => 'required', 'port_idle' => 'required', 'port_used' => 'required') );

          //   if($data->jenis_order != 8 && $data->jenis_order != 1 && $data->jenis_order != 10 && $data->jenis_order != 9 && $data->jenis_order != 17 && $data->jenis_order != 19 && $data->jenis_order != 5){
          //     $rules = array_merge($rules, array('action_cause' => 'required','action' => 'required','flag_Sebelum' => 'required','flag_Progress' => 'required','flag_Sesudah' => 'required'));
          //   }
          //   if($data->jenis_order == 4){
          //     $rules = array_merge($rules, array('flag_ODP-dan-Redaman-GRID' => 'required','flag_Pengukuran' => 'required','flag_Pelanggan_GRID' => 'required', 'redaman_awal' => 'required','redaman_akhir' => 'required'));
          //   }else if ($data->jenis_order == 5){
          //     $fotonya = $this->photophotoUtilitas;
          //     foreach($fotonya as $val)
          //     {
          //       $req_foto_ul['flag_'.$val] = 'required';
          //     }
          //     $rules = array_merge($rules, $req_foto_ul);
          //   }else if($data->jenis_order == 3){
          //     $rules = array_merge($rules,array('penyebab_id'=>'required','flag_ODP'=>'required','flag_Foto-GRID'=>'required'));
          //   }else if($data->jenis_order == 2){
          //     $rules = array_merge($rules,array('gamas_cause'=>'required'));
          //   }else if($data->jenis_order != 8 && $data->jenis_order != 1 && $data->jenis_order != 10 && $data->jenis_order != 9 && $data->jenis_order != 17 && $data->jenis_order != 19){
          //     $rules = array_merge($rules,array('flag_Foto-GRID'=>'required','flag_Denah-Lokasi'=>'required'));
          //   }
          //   if($data->jenis_order == 1){
          //     $rules = array_merge($rules, array('flag_ODC-Sebelum' => 'required',
          //             'flag_ODC-Progress' => 'required',
          //             'flag_ODC-Sesudah' => 'required',
          //             'flag_ODP-Sebelum' => 'required',
          //             'flag_ODP-Progress' => 'required',
          //             'flag_ODP-Sesudah' => 'required',
          //             'flag_Redaman-Sebelum' => 'required',
          //             'flag_Redaman-Progress' => 'required',
          //             'flag_Redaman-Sesudah' => 'required',
          //             'flag_Saluran-Penanggal-Sebelum' => 'required',
          //             'flag_Saluran-Penanggal-Progress' => 'required',
          //             'flag_Saluran-Penanggal-Sesudah' => 'required',
          //             'flag_Tiang-Sebelum' => 'required',
          //             'flag_Tiang-Progress' => 'required',
          //             'flag_Tiang-Sesudah' => 'required',
          //             'flag_Foto-GRID' => 'required'));
          //   }
          //   if($data->jenis_order == 3 || $data->jenis_order == 4 || $data->jenis_order == 9 || $data->jenis_order == 8 || $data->jenis_order == 10){
          //     $rules['splitter'] = 'required';
          //   }
          //   if($data->jenis_order == 3 || $data->jenis_order == 9 || $data->jenis_order == 8 || $data->jenis_order == 10){
          //     $rules = array_merge($rules,array('port_idle'=>'required','port_used'=>'required'));
          //   }
          //   //validasi reboundary
          //   if($data->jenis_order == 9){
          //       $rules = array_merge($rules,array('flag_Foto-Odp-Depan-Eksisting' => 'required',
          //       'flag_Foto-Odp-Dalam-Eksisting-Sebelum' => 'required',
          //       'flag_Foto-Odp-Dalam-Eksisting-Sesudah' => 'required',
          //       'flag_Foto-Redaman-Odp-Eksisting' => 'required',
          //       'flag_Foto-Progress' => 'required',
          //       'flag_Foto-Odp-Depan-(Rebondary)' => 'required',
          //       'flag_Foto-Odp-Dalam-Sebelum-(Rebondary)' => 'required',
          //       'flag_Foto-Odp-Dalam-Sesudah-(Rebondary)' => 'required',
          //       'flag_Foto-Redaman-Odp-(Rebondary)' => 'required',
          //       'flag_Pemasangan-Aksesoris' => 'required',
          //       'flag_Capture-Id-Valins-Awal' => 'required',
          //       'flag_Capture-Id-Valins-Rebondary' => 'required',
          //       'flag_Foto-GRID' => 'required'));
          //   }

          //   //validasi normalisasi
          //   if($data->jenis_order == 17){
          //       $rules = array_merge($rules, array('flag_ODP-Gendong' => 'required',
          //       'flag_Progress' => 'required',
          //       'flag_Dalam-ODP-Gendong' => 'required',
          //       'flag_Redaman-IN-OUT-Sebelum' => 'required',
          //       'flag_Redaman-IN-OUT-Sesudah' => 'required',
          //       'flag_ODP-New' => 'required',
          //       'flag_Dalam-ODP-New' => 'required',
          //       'flag_Redaman-OUT' => 'required'));
          //   }
          //   // if($data->jenis_order == 8){
          //   //   $rules = array_merge($rules,array('flag_GRID_TIANG'=>'required','flag_GRID_PROGRES_TIANG'=>'required','flag_GRID_AKSESORIS'=>'required','flag_GRID_PROGRES_AKSESORIS'=>'required','flag_GRID_ODP'=>'required','flag_GRID_PROGRES_ODP'=>'required','flag_GRID_REBOUNDARY'=>'required','flag_GRID_PROGRES_REBOUNDARY'=>'required','flag_GRID_DENAH'=>'required'));
          //   // }
          //   if($data->jenis_order == 10 || $data->jenis_order == 8){
          //     $rules = array_merge($rules,array('flag_ODP-Sebelum'=>'required',
          //       'flag_ODP-Progress'=>'required',
          //       'flag_ODP-Sesudah'=>'required',
          //       'flag_Redaman-Sebelum'=>'required',
          //       'flag_Redaman-Progress'=>'required',
          //       'flag_Redaman-Sesudah'=>'required',
          //       'flag_Saluran-Penanggal-Sebelum'=>'required',
          //       'flag_Saluran-Penanggal-Progress'=>'required',
          //       'flag_Saluran-Penanggal-Sesudah'=>'required',
          //       'flag_Tiang-Sebelum'=>'required',
          //       'flag_Tiang-Progress'=>'required',
          //       'flag_Tiang-Sesudah'=>'required'));
          //   }
          //   //odc sehat
          //   if($data->jenis_order == 20){
          //     $rules = array_merge($rules,array(
          //       'flag_Foto_ODC_Depan_Sebelum'=>'required',
          //       'flag_Foto_ODC_Depan_Sesudah'=>'required',
          //       'flag_Foto_ODC_Dalam_Sebelum'=>'required',
          //       'flag_Foto_ODC_Dalam_Sesudah'=>'required'));

          //       unset($rules['flag_Denah-Lokasi']);
          //   }
          // }

          if($request->status == 'close' && $data->verify == null && $request->action_cause != "CANCEL ORDER"){
            // if($data->jenis_order == 3){
            //   $check_valins = DB::table('maintenance_valins')->where('maintenance_id', $id)->first();
            //   if(!$check_valins)
            //   {
            //     return redirect()->back()
            //     ->withInput($request->all())
            //     ->with('alerts', [
            //       ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Tidak ditemukan ID Valins!']
            //     ]);
            //   }
            // }
            // $rules = array_merge($rules, array('splitter' => 'required', 'port_idle' => 'required', 'port_used' => 'required') );

            $get_field_photo = DB::table('maintenance_evident')->select('evident')->where([
              ['program_id', $data->jenis_order],
              ['hukum', 'require']
            ])->get();

            $get_field_photo = json_decode(json_encode($get_field_photo), TRUE);
            $fotonya = array_map(function($x)
            {
              return $x['evident'];
            }, $get_field_photo);

            foreach($fotonya as $val)
            {
              $req_foto_ul['flag_'.$val] = 'required';
            }

            $rules = array_merge($rules, $req_foto_ul);

            $get_field = DB::table('maintenance_jenis_order')->select('field_laporan_order')->where('nama_order', $data->nama_order)->first();
            $get_field = json_decode($get_field->field_laporan_order);

            if($get_field)
            {
              foreach($get_field as $k => $v)
              {
                if(@$v->visible != true)
                {
                  unset($get_field[$k]);
                }
              }
            }

            $get_field = array_map(function($x){
              if(@$x->require == true) return $x->id;
            }, $get_field);

            $get_field = array_filter($get_field);

            foreach($get_field as $val)
            {
              $req_field[$val] = 'required';
            }

            $rules = array_merge($rules, $req_field);
          }

          if($request->status != 'close')
          {
            unset($rules['flag_RFC']);
          }
          $materialsNte = json_decode($request->input('materialsNte'));
          // dd('stop', $materials);
          $validator = Validator::make($request->all(), $rules, $messages);
          // dd($validator, $validator->fails(), $rules);
          if ($validator->fails()) {
            return redirect()->back()
                ->withInput($request->all())
                ->withErrors($validator)
                ->with('alerts', [
                  ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan (Dokumen atau Isian kurang lengkap)']
                ]);
          }
          // dd('ready to go bitch');
          $status_mt = $request->input('status');
          DB::transaction(function() use($request, $data, $auth, $materials, &$status_mt, $id, $saldo, $materialsNte) {

            if($saldo){
              foreach($saldo as $s){
                $s->id_pemakaian = $id;
                $s->action = 2;
                $s->value = $s->pemakaian*-1;
                $s->created_at = date('Y-m-d H:i:s');
                $s->created_by = $auth->id_karyawan;
                unset($s->id);
                unset($s->tpemakaian);
                unset($s->pemakaian);
                unset($s->kembalian);
                unset($s->sisa);
              }
              Saldo::delete($id,2);
              //to array
              Saldo::insert(json_decode(json_encode($saldo), true));
              // dd($saldo);
            }

            DB::table('maintaince_mtr')
              ->where('maintaince_id', $id)
              ->delete();
            $mtrcount = 0;

            foreach($materials as $material) {
              DB::table('maintaince_mtr')->insert([
                'maintaince_id' => $id,
                'id_item'       => $material->id_item,
                'qty'           => $material->qty,
              ]);
              $mtrcount++;
            }

            $total = DB::select('
              SELECT
              (SELECT sum((qty*material_ta)) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as material,
              (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as jasa
              FROM maintaince m
              WHERE isHapus = 0 AND id = ?
            ', [$id])[0];

            $hasil_by_qc = $data->hasil_by_qc;
            $hasil_by_user = $data->hasil_by_user;

            if($request->input('status')=="close"){
              $hasil_by_user = $total->jasa + $total->material;
            }else if($request->input('status')=="qqc"){
              $hasil_by_qc = $total->jasa + $total->material;
            }

            $tgl_selesai = null;
            $tgl_ogp = $data->tgl_ogp;

            if(in_array($request->input('status'), ['Redaman Sudah Aman', 'ogp']) )
            {
              $tgl_ogp = date('Y-m-d H:i:s');
            }
            else
            {
              $tgl_selesai = ($data->tgl_selesai == '' || is_null($data->tgl_selesai) ? date('Y-m-d H:i:s') : $data->tgl_selesai);
            }

            $verify = $data->verify;
            //if($data->status!=$request->input('status') && $request->input('status')=='close'){
            /*
              null = in TL
              1 = in SM
              2 = in TELKOM
              3 = siap rekon
            */

            $penyebab_id = null;
            $penyebab_name = null;
            if($data->jenis_order == 3){
              $penyebab = DB::table('maintenance_penyebab')->where('id', $request->penyebab_id)->first();
              $penyebab_id = $request->penyebab_id;
              $penyebab_name = @$penyebab->penyebab;
            }

            // $check_nte = DB::table('maintaince')
            // ->leftJoin('psb_laporan', 'maintaince.psb_laporan_id', '=', 'psb_laporan.id')
            // ->leftJoin('nte', 'psb_laporan.id_tbl_mj', '=', 'nte.id_dispatch_teknisi')
            // ->select('nte.id_dispatch_teknisi')
            // ->where('psb_laporan.id', $data->psb_laporan_id)
            // ->first();

            // if(@$check_nte->id_dispatch_teknisi)
            // {
            //   foreach($materialsNte as $mNte) {
            //     if ($mNte->kat == "typeont") {
            //       DB::table('psb_laporan')->where('id', $data->psb_laporan_id)->update([
            //         'typeont'             => $mNte->jenis,
            //         'snont'               => $mNte->id
            //       ]);
            //     } elseif ($mNte->kat == "typestb") {
            //       DB::table('psb_laporan')->where('id', $data->psb_laporan_id)->update([
            //         'typestb'             => $mNte->jenis,
            //         'snstb'               => $mNte->id
            //       ]);
            //     }
            //     DB::table('nte')->where('sn', $mNte->id)->update([
            //       'status'              => 1,
            //       'qty'                 => $mNte->qty,
            //       'id_dispatch_teknisi' => $check_nte->id_dispatch_teknisi ,
            //       'position_key'        => $auth->id_karyawan
            //     ]);
            //   }
            // }
            // pindah ke table baru unspec_ganti_nte jgn update laporan psb, nanti hilang historical laporan teknisi psb


            $nama_note = 'SUBMIT PEKERJAAN';
            $verify_me = $data->verify;

            switch ($data->verify ) {
              case (in_array($data->verify, [4, 5]) ? TRUE : FALSE):
                $nama_note = 'SUBMIT PEKERJAAN REVISI';
                $verify_me = NULL;
              break;
              case 3:
                $nama_note = 'SIAP REKON';
              break;
            }
            // dd($verify_me, $nama_note);

            DB::table('maintenance_note')
            ->insert([
              'mt_id'     => $id,
              'catatan'   => $nama_note,
              'status'   => $nama_note,
              'mtr_json'   => json_encode($materials),
              'pelaku'    => session('auth')->id_karyawan,
              'ts'     => DB::raw('NOW()')
            ]);

            $this->handleUploadDataAbd($request, $id);

            $regu = DB::table('regu')->where('id_regu', $data->dispatch_regu_id)->first();

            $date1 = new \DateTime($data->created_at);
            $date2 = new \DateTime(date('Y-m-d H:i:s'));

            $diff = $date1->diff($date2);
            $month = $diff->format('%m');

            // if($data->jenis_order == 4 && $request->no_tiket)
            // {
            //   $array_save['no_tiket'] = $request->no_tiket;
            // }

            $koordinat_tiang = null;

            if($request->koordinat_tiang)
            {
              $koordinat_tiang = implode(';', array_map(function($x){
                return preg_replace('/\s+/', '', $x);
              }, $request->koordinat_tiang) );

              if($data->jenis_order == 6)
              {
                $koordinat_tiang_exp = explode(';', $koordinat_tiang);

                $check_material = DB::table('maintaince_mtr')
                ->select('maintaince_mtr.id_item', 'maintaince_mtr.qty', 'khs_maintenance.uraian', 'khs_maintenance.satuan')
                ->leftJoin('khs_maintenance', 'maintaince_mtr.id_item', '=', 'khs_maintenance.id_item')
                ->where('maintaince_id', $id)
                ->get()
                ->toArray();

                if(in_array('PU-S7.0-140', array_column($check_material, 'id_item') ) )
                {
                  $material_mtng = 'PU-S7.0-140';
                }

                if(in_array('PU-S9.0-140', array_column($check_material, 'id_item') ) )
                {
                  $material_mtng = 'PU-S9.0-140';
                }

                foreach($koordinat_tiang_exp as $kte)
                {
                  DB::table('master_tiang')->insert([
                    'koordinat' => $kte,
                    'odc' => explode('-', $data->nama_odp)[1],
                    'dist' => 'MARINA',
                    'designator' => $material_mtng
                  ]);
                }
              }
            }

            $array_save = [
              'status_name'     => $status_mt,
              'status'          => $status_mt,
              'status_detail'   => $request->input('status_detail'),
              'nog_project'     => $request->input('nog_project'),
              'tgl_selesai'     => $tgl_selesai,
              'mtrcount'        => $mtrcount,
              'action'          => $request->input('action'),
              'koordinat'       => $request->input('koordinat'),
              'modified_at'     => DB::raw('NOW()'),
              'modified_by'     => $auth->id_karyawan,
              'verify'          => $verify_me,
              'koordinat_tiang' => $koordinat_tiang,
              'tgl_ogp'         => $tgl_ogp,
              'hasil_by_user'   => $hasil_by_user,
              'hasil_by_qc'     => $hasil_by_qc,
              'action_cause'    => $request->input('action_cause'),
              'gamas_cause'     => $request->input('gamas_cause'),
              'splitter'        => $request->input('splitter'),
              'port_idle'       => $request->input('port_idle'),
              'port_used'       => $request->input('port_used'),
              'redaman_awal'    => $request->input('redaman_awal'),
              'redaman_akhir'   => $request->input('redaman_akhir'),
              'penyebab_id'     => $penyebab_id,
              'penyebab_name'   => $penyebab_name,
              'discovery'       => $request->input('discovery'),
              'nama_mitra'      => $regu->nama_mitra
            ];

            if($request->nama_odp)
            {
              $array_save['nama_odp'] = $request->nama_odp;
            }

            if($request->headline)
            {
              $array_save['headline'] = $request->headline;
            }

            if($request->valins_awal)
            {
              $array_save['valins_awal'] = $request->valins_awal;
              $array_save['tgl_valins_awal'] = $request->tgl_valins_awal;
            }

            if($request->valins_akhir)
            {
              $array_save['valins_akhir'] = $request->valins_akhir;
              $array_save['tgl_valins_akhir'] = $request->tgl_valins_akhir;
            }

            if($request->valin_bot)
            {
              $string = htmlentities($request->valin_bot, ENT_QUOTES, 'UTF-8', false);
              $strng  = htmlentities($string, ENT_QUOTES, 'UTF-8', false);

              $array_save['valin_bot'] = $strng;
            }

            if ($month > 1)
            {
              if(!$data->modified_at)
              {
                $array_save['modified_at'] = date("Y-m-d H:i:s", strtotime($data->created_at));
              }
            }
            else
            {
              $array_save['modified_at'] = date('Y-m-d H:i:s');
            }

            DB::table('maintaince')
            ->where('id', $id)
            ->update($array_save);

            if($data->nog_order_id == 0 || is_null($data->nog_order_id) )
            {
              if($request->splitter && $request->splitter != '1:8')
              {
                $data_nog = DB::Table('nog_master')->where('nama_odp', 'like', '%'. $request->nama_odp .'%')->first();
                // $data_nog = DB::Table('nog_master')->where('mt_id', $id)->first();
                if(count($data_nog) == 0)
                {
                  DB::Table('nog_master')->insert([
                    'nama_odp'      => $request->nama_odp,
                    'koordinat_odp' => $request->koordinat,
                    'mt_id'         => $id,
                    'sto_nog'       => $data->sto,
                    'jml_port'      => $request->splitter,
                    'jenis_order'   => 'MARINA',
                    'id_valins'     => $data->valins_id,
                    'created_by'    => session('auth')->id_karyawan
                  ]);
                }
                // else
                // {
                //   DB::Table('nog_master')->where('mt_id', $id)->update([
                //     'nama_odp' => $request->nama_odp,
                //     'jml_port' => $request->splitter,
                //     'id_valins' => $data->valins_id,
                //     'created_by' => session('auth')->id_karyawan
                //   ]);
                // }
              }
            }
            else
            {
              $so = null;

              if($status_mt == 'close')
              {
                $so = 'DONE';
              }
              elseif(preg_match('/(?i)kendala/', $status_mt) || $status_mt == 'cancel order')
              {
                $so = 'KENDALA';
              }

              DB::table('nog_master')->where('id', $data->nog_order_id)->update([
                'status_order' => $so
              ]);
            }
          });
        }

        if($status_mt != $data->status){
          exec('cd ..;php artisan sendReport '.$id.' > /dev/null &');
        }
        return redirect('/tech')->with('alerts', [
          ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
        ]);
      }

    public function change_ticket(Request $req, $id)
    {
      DB::table('maintaince')->where('id', $id)->update([
        'no_tiket' => $req->notiket
      ]);

      return redirect('/tech/'.$id)->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengganti Nomor Tiket']
      ]);
    }

    private function handleUploadDataAbd($req, $id)
    {
      $path = public_path() . '/upload/maintenance/abd/'.$id.'/';

      if (!file_exists($path)) {
        if (!mkdir($path, 0770, true)) {
          return 'Gagal Menyimpan File';
        }
      }

      if ($req->hasFile('abd')) {
        $file = $req->file('abd');
        try {
        $filename = 'File ABD.'. strtolower( $file->getClientOriginalExtension() );
        $file->move("$path", "$filename");
        } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'Gagal Menyimpan '. $path;
        }
      }
    }

    public function download_file_abd($id)
    {
      $check_path = public_path() . '/upload/maintenance/abd/' . $id . '/';
      $files = preg_grep('~^File ABD.*$~', scandir($check_path));

      if(count($files) != 0)
      {
        $files = array_values($files);
        $file = $check_path.'/'.$files[0];
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($file));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        readfile($file);
        exit;
      }
    }

    public function upload($id){
      $path2 = public_path().'/upload/maintenance/'.$id.'th';
      $th = array();
      if (file_exists($path2)) {
        $photos = File::allFiles($path2);
        foreach ($photos as $photo){
          array_push($th,(string) $photo);
        }
      }
      return view('tech.photos', compact('id','th'));
    }
    public function uploadSave(Request $request, $id){
      $files = $request->file('photo');
      $path = public_path().'/upload/maintenance/'.$id.'/';
      $th = public_path().'/upload/maintenance/'.$id.'th/';
      if (!file_exists($path)) {
        if (!mkdir($path, 0755, true))
          return 'gagal menyiapkan folder foto evidence';
      }
      if (!file_exists($th)) {
        if (!mkdir($th, 0755, true))
          return 'gagal menyiapkan folder foto evidence';
      }
      foreach ($files as $file) {
          $name = $file->getClientOriginalName();
          try {
            $moved = $file->move("$path", "$name");
            $img = new \Imagick($moved->getRealPath());
            $img->scaleImage(100, 150, true);
            $img->writeImage("$th/$name");
          }
          catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            return 'gagal menyimpan foto evidence '.$name;
          }
      }
      return redirect('/tech')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> Upload Photo']
      ]);
    }
    private static function getGrupTech($id){
      $nama = DB::select('select *, (select u_nama As nama from user where id_karyawan = mr.nik1) as nama1, (select u_nama As nama from user where id_karyawan = mr.nik2) as nama2 from regu mr where id_regu = '.$id);
      if(count($nama)){
        return $nama[0]->nama1."&".$nama[0]->nama2;
      }
    }
    public static function sendtotelegram($id){
      $auth = session('auth');
      $chat_id = "-285944800";
      $data = DB::table('maintaince')
        ->select('maintaince.*', 'regu.mainsector', 'regu.mainsector_marina', 'psb_laporan.kordinat_odp', 'psb_laporan.nama_odp')
        ->leftJoin('regu', 'maintaince.dispatch_regu_id', '=', 'regu.id_regu')
        ->leftJoin('psb_laporan', 'maintaince.psb_laporan_id', '=', 'psb_laporan.id')
        ->where('maintaince.id', $id)->first();
      $chatid_marina = $data->mainsector_marina;
      $nama = self::getGrupTech($data->dispatch_regu_id);
      $nm = User::karyawanExists($data->modified_by);
       $dteStart = new \DateTime($data->created_at);
        if($data->tgl_selesai){
          $endds = $data->tgl_selesai;
          $dteEnd   = new \DateTime($data->tgl_selesai);
        }else{
          $endds = date('Y-m-d H:i:s');
          $dteEnd   = new \DateTime(date('Y-m-d H:i:s'));
        }

        $dteDiff  = $dteStart->diff($dteEnd);
        $durasi   = $dteDiff->format("%Dd %Hh %im");

      $msg = "<b>Laporan ".$data->nama_order."</b>\n";
      $msg .= "=============\n";
      $msg .= "<b>Tanggal Order</b> : <i>".$data->created_at."</i>\n";
      $msg .= "<b>Tanggal Close</b> : <i>".$endds."</i>\n";
      $msg .= "<b>Durasi Pekerjaan</b> : <i> $durasi </i>\n";
      $msg .= "<b>Nomor Order</b> : <i>".$data->no_tiket."</i>\n";
      $msg .= "<b>Status</b> : <i>".$data->status."</i>\n";
      $msg .= "<b>Jenis Splitter</b> : <i>".$data->splitter."</i>\n";
      if($data->jenis_order == 3){
        $msg .= "<b>Penyebab</b> : <i>".$data->penyebab_name."</i>\n";
      }
      $msg .= "<b>Action</b> : <i>".$data->action_cause."</i>\n";
      $msg .= "<b>Action Detil</b> : <i>".$data->action."</i>\n";
      if($data->jenis_order == 3){
        $msg .= "<b>Detail ODP</b> : <i>".$data->nama_odp."( ".$data->kordinat_odp." )</i>\n";
      }
      if($data->jenis_order == 4){
        $msg .= "<b>Redaman Sebelum</b> : <i>".$data->redaman_awal."</i>\n";
        $msg .= "<b>Redaman Sesudah</b> : <i>".$data->redaman_akhir."</i>\n";
      }
      $msg .= "<b>Headline</b> : <i>".$data->headline."</i>\n";
      $msg .= "<b>Koordinat</b> : <i>".$data->koordinat."</i>\n";
      $msg .= "<b>Dilaporkan Oleh</b> : <i>".@$data->modified_by."/".@$nm->nama."</i>\n";
      $msg .= "<b>Dikerjakan Oleh</b> : <i>".$data->dispatch_regu_name."( ".$nama." )</i>\n\n";

      $msg .= "<b>Material dan Jasa</b>\n";
      $msg .= "=============\n";
      $mtr = DB::table('maintaince_mtr')
        ->select('maintaince_mtr.id_item', 'maintaince_mtr.qty', 'khs_maintenance.uraian', 'khs_maintenance.satuan')
        ->leftJoin('khs_maintenance', 'maintaince_mtr.id_item', '=', 'khs_maintenance.id_item')
        ->where('maintaince_id', $id)->get();
        //dd($mtr);
      foreach($mtr as $m){
        $msg .= " -<code>".$m->id_item."</code> ( <b>".$m->qty." ".$m->satuan."</b> )\n   * <i>".$m->uraian."</i>\n";
      }

      if ($data->status=='close' && $data->jenis_order == '3') {
        Telegram::sendMessage([
          'chat_id' => '-467693154',
          'parse_mode' => 'html',
          'text' => $msg
        ]);
      }

      $no_tikett = substr($data->no_tiket, 0, 2);
      if ($no_tikett<>'IN'){
        if ($data->status =='close' && ($data->jenis_order=='3' or $data->jenis_order=='6' or $data->jenis_order=='9')){
            Telegram::sendMessage([
              'chat_id' => '-267883773',
              'parse_mode' => 'html',
              'text' => $msg
            ]);

            $nameFoto = 'Foto-GRID';
            $path = public_path().'/upload/maintenance/'.$id.'/'.$nameFoto.'.jpg';
            if (file_exists($path)){
              try {
                Telegram::sendPhoto([
                  'chat_id' => '-267883773',
                  'caption' => $nameFoto,
                  'photo' => $path
                ]);
              }catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                Telegram::sendMessage([
                  'chat_id' => '-267883773',
                  'parse_mode' => 'html',
                  'text' => $e
                ]);
              }
            }
        }
      }

      Telegram::sendMessage([
        'chat_id' => $chatid_marina,
        'parse_mode' => 'html',
        'text' => $msg
      ]);
      // if($data->jenis_order == 1 || $data->jenis_order == 10){
      //   Telegram::sendMessage([
      //     'chat_id' => $chatid_marina,
      //     'text' => "Dicovery info : ".$data->discovery
      //   ]);
      // }
      // $foto = self::$photoInputst;
      // if($data->jenis_order == 8){
      //   $foto = self::$photoOdpSehatst;
      // }
      // if($data->jenis_order == 1){
      //   $foto = self::$photoBenjarSt;
      // }
      // if($data->jenis_order == 10){
      //   $foto = self::$photoValidasiSt;
      // }
      // if($data->jenis_order == 17){
      //   $foto = self::$photoNormalisasiSt;
      // }
      // if($data->jenis_order == 9){
      //   $foto = self::$photoReboundarySt;
      // }
      // if($data->jenis_order == 5){
      //   $foto = self::$photophotoUtilitasSt;
      // }
      // if($data->jenis_order == 24){
      //   $foto = self::$photoIXSAst;
      // }
      // else if($data->jenis_order == 2){
      //   $foto = self::$photoGamasSt;
      // }
      $get_field_photo = DB::table('maintenance_evident')->where('program_id', $data->jenis_order)->get();
      foreach($get_field_photo as $v) {
        $name  = $v->evident;
        $path  = public_path().'/upload/maintenance/'.$id.'/'.$name.'.jpg';
        $path2 = public_path().'/upload1/maintenance/'.$id.'/'.$name.'.jpg';
        $path3 = public_path().'/upload2/maintenance/'.$id.'/'.$name.'.jpg';
        if (file_exists($path)){
          echo"ada";
          try {
          if($data->status =='close'){
            $name = "$data->no_tiket \n $name \n #".strtolower(preg_replace('/[^A-Za-z0-9]/', "", $data->dispatch_regu_name));
          }
            Telegram::sendPhoto([
              'chat_id' => $chatid_marina,
              'caption' => $name,
              'photo' => $path
            ]);
          }catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            Telegram::sendMessage([
              'chat_id' => $chatid_marina,
              'parse_mode' => 'html',
              'text' => $e
            ]);
          }
        }else if (file_exists($path2)){
          try {
          echo"ada2";
            Telegram::sendPhoto([
              'chat_id' => $chatid_marina,
              'caption' => $name,
              'photo' => $path2
            ]);
          }catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            Telegram::sendMessage([
              'chat_id' => $chatid_marina,
              'parse_mode' => 'html',
              'text' => $e
            ]);
          }
        }else if (file_exists($path3)){
          try {
          echo"ada3";
            Telegram::sendPhoto([
              'chat_id' => $chatid_marina,
              'caption' => $name,
              'photo' => $path3
            ]);
          }catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            Telegram::sendMessage([
              'chat_id' => $chatid_marina,
              'parse_mode' => 'html',
              'text' => $e
            ]);
          }
        }
      }
    }

    public static function senddispatchtotelegram($id){
      //$auth = session('auth');
      //$chat_id = "-285944800";
      $data = DB::table('maintaince')
        ->select('maintaince.*', 'regu.mainsector', 'regu.mainsector_marina', 'psb_laporan.kordinat_odp', 'psb_laporan.nama_odp', 'psb_laporan.id_tbl_mj')
        ->leftJoin('regu', 'maintaince.dispatch_regu_id', '=', 'regu.id_regu')
        ->leftJoin('psb_laporan', 'maintaince.psb_laporan_id', '=', 'psb_laporan.id')
        ->where('maintaince.id', $id)->first();
      $chatid_marina =$data->mainsector_marina;
      $nama = self::getGrupTech($data->dispatch_regu_id);
      $msg = "<b>Mohon Bantuan WO ".$data->nama_order."</b>\n";
      $msg .= "===========\n";
      $msg .= " <b>Tanggal</b> : <i>".$data->created_at."</i>\n";
      $msg .= " <b>Ditugaskan Kepada</b> : <i>".$data->dispatch_regu_name."( ".$nama." )</i>\n";
      $msg .= " <b>Nomor Order</b> : <i>".$data->no_tiket."</i>\n";
      if($data->jenis_order == 3){
        $msg .= " <b>Detail ODP</b> : <i>".$data->nama_odp."( ".$data->kordinat_odp." )</i>\n";
      }
      $msg .= " <b>Headline</b> : <i>".$data->headline."</i>\n";
      $msg .= " <b>Koordinat</b> : <i>".$data->koordinat."</i>\n";
      Telegram::sendMessage([
        'chat_id' => $chatid_marina,
        'parse_mode' => 'html',
        'text' => $msg
      ]);
      if($data->koordinat){
        $koor = explode(",", $data->koordinat);
        if(count($koor)){
          Telegram::sendLocation([
            'chat_id' => $chatid_marina,
            'latitude' => $koor[0],
            'longitude' => $koor[1]
          ]);
        }
      }
      if($data->id_tbl_mj){
        if($data->order_from == 'ASSURANCE'){
          $folder = 'asurance';
          $photos = self::$sendPhoto;
        }else if($data->order_from == 'PSB'){
          $folder = "evidence";
          $photos = self::$sendPhotoPsb;
        }
        foreach($photos as $photo){
          $file = "/srv/htdocs/tomman1_psb/public/upload/".$folder."/".$data->id_tbl_mj."/".$photo.".jpg";
          $file2 = "/srv/htdocs/tomman1_psb/public/upload2/".$folder."/".$data->id_tbl_mj."/".$photo.".jpg";
          $file3 = "/srv/htdocs/tomman1_psb/public/upload3/".$folder."/".$data->id_tbl_mj."/".$photo.".jpg";
          $file4 = "/srv/htdocs/tomman1_psb/public/upload4/".$folder."/".$data->id_tbl_mj."/".$photo.".jpg";
          if (file_exists($file)){
            Telegram::sendPhoto([
              'chat_id' => $chatid_marina,
              'caption' => "FOTO ".$photo,
              'photo' => $file
            ]);
          }else if (file_exists($file2)){
            Telegram::sendPhoto([
              'chat_id' => $chatid_marina,
              'caption' => "FOTO ".$photo,
              'photo' => $file2
            ]);
          }else if (file_exists($file3)){
            Telegram::sendPhoto([
              'chat_id' => $chatid_marina,
              'caption' => "FOTO ".$photo,
              'photo' => $file3
            ]);
          }else if (file_exists($file4)){
            Telegram::sendPhoto([
              'chat_id' => $chatid_marina,
              'caption' => "FOTO ".$photo,
              'photo' => $file4
            ]);
          }else{
            Telegram::sendMessage([
              'chat_id' => $chatid_marina,
              'parse_mode' => 'html',
              'text' => "tidak ada foto ".$photo."\n".$file
            ]);
          }
        }
      }

    }
  public static function pleaseUpdate(){
    $regu = DB::table('maintenance_grup')->get();
      //$data = DB::select("select * from maintaince mt left join maintenance_regu mr on mt.dispatch_regu_id = mr.id where status is null");
      //  dd($data);
    foreach($regu as $r){
      $msg = "<b>Please Report Progress! \n".$r->grup."</b> \n";
      $msg .= date('Y-m-d h:i:s')."\n";
      $data = DB::table('maintaince')
        ->leftJoin('regu', 'maintaince.dispatch_regu_id', '=', 'regu.id_regu')
        ->where('mainsector', $r->chat_id)
        ->whereNull('status')
        ->whereNull('refer')
        ->where('maintaince.isHapus', 0)
        ->where('jenis_order', '!=', '1')
        ->orderBy('dispatch_regu_id', 'asc')
        ->get();
      $regu = '';
      foreach($data as $d){
        $nama = self::getGrupTech($d->dispatch_regu_id);
        if ($regu<>$d->dispatch_regu_id){
          $msg .= "\n";
          $msg .= "<b>".$d->dispatch_regu_name." ( ".$nama." )</b>\n";
        }
        if (!$d->status){ $laporan = "NO UPDATE"; } else { $laporan = $d->status_name; }
        $startDate = new \DateTime($d->created_at);
        $endDate = new \DateTime();
        $since_start = $startDate->diff($endDate);
        if ($since_start->h>0) $h = $since_start->h."h "; else $h = '';
        if ($since_start->i>0) $i = $since_start->i."min "; else $i = '';
        $result = $h. $since_start->i."min";
        $msg .= $result." | ";
        if (substr($d->no_tiket,0,2)<>"IN" || substr($d->no_tiket,0,2)<>"SC"){
          $msg .= $d->no_tiket." | ".$laporan."\n";
        } else {
          $msg .= "SC".$d->no_tiket." | ".$laporan."\n";
        }
        $regu = $d->dispatch_regu_id;
      }

      Telegram::sendMessage([
        'chat_id' => $r->mainsector,
        'parse_mode'=> 'HTML',
        'text' => $msg
      ]);
    }
  }
  public function action_cause($id){
    return json_encode(DB::table('maintenance_cause_action')
                  ->select('action_cause as text', 'action_cause as id', 'status')
                  ->where('status', $id)->get());
  }
  public function chart_status($id){
    /*
    $data = DB::select('select *,
      (select count(id) from maintaince where status = mca.status and tgl_selesai like "'.$id.'%") as status_val
      from maintenance_cause_action mca group by mca.status');
    $array = json_decode(json_encode($data), true);
    $status = array("sts"=>array_column($array, 'status'), "nilai" => array_column($array, 'status_val'));
    $data = DB::select('SELECT COUNT( * ) AS  rows ,  action_cause
      FROM  maintaince where tgl_selesai like "'.$id.'%" and action_cause is not null
      GROUP BY  action_cause
      ORDER BY  action_cause');
    $array = json_decode(json_encode($data), true);
    $action = array("sts"=>array_column($array, 'action_cause'), "nilai" => array_column($array, 'rows'));
    $dataChart = array("sts"=> $status, "act" => $action);
    return $dataChart;
    */
    $data = DB::select('select *,
      (select count(id) from maintaince where isHapus = 0 AND jenis_order = mjo.id and tgl_selesai like "'.$id.'%" and status = "close") as close,
      (select count(id) from maintaince where isHapus = 0 AND jenis_order = mjo.id and tgl_selesai like "'.$id.'%" and status = "kendala pelanggan") as kendala_pelanggan,
      (select count(id) from maintaince where isHapus = 0 AND jenis_order = mjo.id and tgl_selesai like "'.$id.'%" and status = "kendala teknis") as kendala_teknis
      from maintenance_jenis_order mjo');
    return json_encode($data);
  }
  public function dashboard_verify($tgl){
    //  $data = DB::select('select *,
    //   (select count(m.id) from maintaince m where refer is null and m.status = "close" and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = mjo.id and verify is null and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as TA,
    //   (select count(m.id) from maintaince m where refer is null and m.status = "close" and m.tgl_selesai like "'.$tgl.'%" and m.jenis_order = mjo.id and verify = 1 and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as TELKOM
    //   from maintenance_jenis_order mjo where 1');

    $data = DB::select("SELECT mjo.*,
      (CASE
      WHEN verify is null THEN 'Submit Teknisi'
      WHEN verify = 1 THEN 'Verifikasi TA'
      WHEN verify = 2 THEN 'Verifikasi TELKOM'
      WHEN verify = 4 THEN 'DECLINE TA'
      WHEN verify = 5 THEN 'DECLINE TELKOM'
      ELSE 'SIAP REKON' END) as last_stts,
      SUM(CASE WHEN verify IS NULL THEN 1 ELSE 0 END ) as TA_mat,
      SUM(CASE WHEN verify = 4 THEN 1 ELSE 0 END) as Decline_ta_mat,
      SUM(CASE WHEN verify = 1  THEN 1 ELSE 0 END ) as TELKOM_mat,
      SUM(CASE WHEN verify = 5 THEN 1 ELSE 0 END) as Decline_t,
      SUM(CASE WHEN m.verify = 2 AND m.mtrcount != 0 THEN 1 ELSE 0 END) as siap_rekon_mat,
      SUM(CASE WHEN m.verify = 2 AND m.mtrcount = 0 THEN 1 ELSE 0 END) as siap_rekon_non_mat
      FROM maintaince m
      LEFT JOIN maintenance_jenis_order mjo ON m.jenis_order = mjo.id
      WHERE m.isHapus = 0 AND refer IS NULL AND m.status = 'close' AND m.tgl_selesai LIKE '$tgl%' GROUP BY m.jenis_order");
    return view('order.verify', compact('data'));
  }

  public function ajax_verify($ds, $dj, $dt){
    if(is_numeric($ds) )
    {
      $q = "AND verify = $ds";
    }

    if($ds == 'null')
    {
      $q = "AND verify is $ds";
    }

    if($ds == 'siap_rekon_material')
    {
      $q = "AND verify = '2' AND m.mtrcount != 0 ";
    }

    if($ds == 'siap_rekon_non_material')
    {
      $q = "AND verify = '2' AND m.mtrcount = 0 ";
    }

    $q2 = "";

    if($dj != "all")
    {
      $q2 = "and m.jenis_order = ".$dj;
    }

    $rd = DB::select("SELECT m.*
      FROM maintaince m
      LEFT JOIN maintenance_jenis_order mjo ON m.jenis_order = mjo.id
      WHERE m.isHapus = 0 AND refer IS NULL AND m.status = 'close' AND m.tgl_selesai LIKE '$dt%' $q $q2 AND mjo.id = $dj
      GROUP BY m.id
      ORDER BY mjo.id DESC");

    foreach($rd as $k => $v)
    {
      $data[$k] = $v;
      $log = DB::table('maintenance_note')->where('mt_id', $v->id)->orderBy('ts','DESC')->limit(1)->first();

      $data[$k]->ts = null;

      if($log)
      {
        $data[$k]->ts = $log->ts;
      }
    }
    return view('order.ajaxVerify', compact('data'));
  }

  public function ajax_anak_tiket($id){
    $data = DB::select('select * from maintaince where isHapus = 0 AND refer='.$id);
    return json_encode($data);
  }

  public function list_verify($tgl){
    $auth = session('auth');
    if($auth->jenis_verif == 1 || $auth->jenis_verif == 3){
      $query = " ";
      if($auth->jenis_verif == 1){
        $query = ' and mt.verify is null OR mt.verify IN (4, 5)';
      }else if($auth->jenis_verif == 3){
        $query = ' and mt.verify IN (1, 5, 2)';
      }
      // else if($auth->jenis_verif == 3){
      //   $query = ' and mt.verify=2';
      // }
      $qtgl = "";
      if($tgl != "all")
        $qtgl = ' and mt.tgl_selesai like "'.$tgl.'%" ';
      $data = DB::select('SELECT mt.*, mt.id as id_mt, mt.nama_odp as nama_dp,
      mr.nama1,
      mr.nama2,
      (select count(id) from maintaince where refer = mt.id) as jml_anak,
      (select COUNT(id_regu) FROM regu JOIN maintaince mt2 ON id_regu = mt2.dispatch_regu_id WHERE id_regu = mt.dispatch_regu_id (nik1 = "'.session('auth')->id_user.'" OR nik2 = "'.session('auth')->id_user.'") ) As check_regu
      from maintaince mt
      left join regu mr on mt.dispatch_regu_id = mr.id_regu
      where  mt.isHapus = 0 AND refer is null and mt.status = "close" '.$qtgl.'
    '.$query.' and (mt.mtrcount>0 or mt.action NOT IN ("CANCEL ORDER","REDAMAN AMAN")) order by tgl_selesai desc');
    }else{
      $data = new \stdClass();
    }
    // dd('tes')
    return view('order.listverify', compact('data'));
  }
  public static function pleaseVerif(){
    $tgl = date('Y-m');
    $msg = "<b>Please Verifikasi WO!</b> \n";
    $msg .= date('Y-m-d h:i:s')."\n";
    // $data = DB::select('select mt.tl,k.nama,
    //   (select count(m.id) from maintaince m left join maintenance_regu mr on m.dispatch_regu_id = mr.id where refer is null and m.status = "close" and mr.niktl = mt.tl and m.tgl_selesai like "'.$tgl.'%" and verify is null and (m.mtrcount>0 or m.action NOT IN ("CANCEL ORDER","REDAMAN AMAN"))) as tl
    //   from maintenance_tl mt left join karyawan k on mt.tl = k.id_karyawan where 1 order by tl desc');
    $data = DB::select('SELECT SUM(CASE WHEN verify is null and action NOT IN ("CANCEL ORDER","REDAMAN AMAN")
         THEN 1 ELSE 0
    END) AS TA,SUM(CASE WHEN verify = 1
         THEN 1 ELSE 0
    END) AS TELKOM FROM maintaince WHERE isHapus = 0');

    foreach($data as $no => $d){
        $msg .= "<b>1. TA ( ".$d->TA." WO )</b>\n";
        $msg .= "<b>2. TELKOM ( ".$d->TELKOM." WO )</b>\n";
    }

    Telegram::sendMessage([
      'chat_id' => "-200657991",
      'parse_mode'=> 'HTML',
      'text' => $msg
    ]);
  }
  public function logistik(){
    $data = new \stdClass();
    $project = DB::select('SELECT project as id, project as text FROM alista_material_keluar GROUP BY project ORDER BY project');
    $gudang = DB::select('SELECT nama_gudang as id, nama_gudang as text FROM alista_material_keluar GROUP BY nama_gudang ORDER BY nama_gudang');
    $mitra = DB::select('SELECT mitra as id, mitra as text FROM alista_material_keluar GROUP BY mitra ORDER BY mitra');
    return view('logistik.logistik', compact('project', 'gudang', 'mitra', 'data'));
  }
  public function listLogistik(Request $req){
    $sql = '';
      if($req->proaktif_id)
          $sql.='project = "'.$req->proaktif_id.'"';

      if($req->nama_gudang)
          $sql.='and nama_gudang = "'.$req->nama_gudang.'"';

      if($req->mitra)
          $sql.=' and mitra = "'.$req->mitra.'"';

      if($req->tgl)
          $sql.=' and tgl like "%'.$req->tgl.'%"';
    $data = DB::select('select * from alista_material_keluar where '.$sql);
    $project = DB::select('SELECT project as id, project as text FROM alista_material_keluar GROUP BY project ORDER BY project');
    $gudang = DB::select('SELECT nama_gudang as id, nama_gudang as text FROM alista_material_keluar GROUP BY nama_gudang ORDER BY nama_gudang');
    $mitra = DB::select('SELECT mitra as id, mitra as text FROM alista_material_keluar GROUP BY mitra ORDER BY mitra');
    return view('logistik.logistik', compact('data', 'project', 'gudang', 'mitra'));
  }
  public function formValidasi($id){
    $data = DB::table('maintaince')->where('id', $id)->first();
    $list = DB::table('maintenance_validasi_port')->where('maintenance_id', $id)->get();
    return view('tech.formValidasiPort', compact('data', 'list'));
  }
  public function formValidasiAwal($id){
    $data = DB::table('maintaince')->where('id', $id)->first();
    $list = DB::table('maintenance_validasi_port_awal')->where('maintenance_id', $id)->get();
    return view('tech.formValidasiPortAwal', compact('data', 'list'));
  }
  public function getJsonPort($id){
    $json = DB::table('maintenance_validasi_port')->where('id', $id)->first();
    return json_encode($json);
  }
  public function getJsonPortAwal($id){
    $json = DB::table('maintenance_validasi_port_awal')->where('id', $id)->first();
    return json_encode($json);
  }
  public function getJsonReboundary($id){
    $json = DB::table('maintenance_reboundary')->where('id', $id)->first();
    return json_encode($json);
  }

  public function saveJumlahPort($id, Request $req){
    $count = DB::table('maintenance_validasi_port')->where('maintenance_id', $id)->count();
    if($req->kapasitas != $count){

      $insert=array();
      for($i=1;$i<=$req->kapasitas;$i++){
        $insert[] = ["maintenance_id"=>$id, "port"=>$i];
      }
      DB::table('maintenance_validasi_port')->where('maintenance_id',$id)->delete();
      DB::table('maintenance_validasi_port')->insert($insert);
      DB::table('maintaince')->where('id', $id)->update(['kapasitas_odp' => $req->kapasitas]);
    }
    return back();
  }
  public function saveValidasi($id, Request $req){
    DB::table('maintenance_validasi_port')->where('id', $req->id)->update(["jenis_layanan"=>$req->jenis_layanan,"no_layanan"=>$req->no_layanan,"qrcode_spl"=>$req->qrcode_spl,"qrcode_dc"=>$req->qrcode_dc,"status"=>$req->status]);
    return back();
  }
  public function saveValidasiAwal($id, Request $req){
    DB::table('maintenance_validasi_port_awal')->where('id', $req->id)->update(["jenis_layanan"=>$req->jenis_layanan,"no_layanan"=>$req->no_layanan,"qrcode_spl"=>$req->qrcode_spl,"qrcode_dc"=>$req->qrcode_dc,"status"=>$req->status]);
    return back();
  }
  public function saveJumlahPortAwal($id, Request $req){
    $count = DB::table('maintenance_validasi_port_awal')->where('maintenance_id', $id)->count();
    if($req->kapasitas != $count){
      $insert=array();
      for($i=1;$i<=$req->kapasitas;$i++){
        $insert[] = ["maintenance_id"=>$id, "port"=>$i];
      }
      DB::table('maintenance_validasi_port_awal')->where('maintenance_id',$id)->delete();
      DB::table('maintenance_validasi_port_awal')->insert($insert);
      DB::table('maintaince')->where('id', $id)->update(['kapasitas_odp' => $req->kapasitas]);
    }
    return back();
  }
  public function saveReboundary($id, Request $req){
    if($req->vid){
      DB::table('maintenance_reboundary')->where('id', $req->vid)->update(["no_layanan"=>$req->no_layanan,"odp_lama"=>$req->odp_lama,"qrcode_dc"=>$req->qrcode_dc,"odp_baru"=>$req->odp_baru,"tarikan_lama"=>$req->tarikan_lama,"tarikan_baru"=>$req->tarikan_baru]);
    }else{
      DB::table('maintenance_reboundary')->insert(["maintenance_id"=>$id,"no_layanan"=>$req->no_layanan,"odp_lama"=>$req->odp_lama,"qrcode_dc"=>$req->qrcode_dc,"odp_baru"=>$req->odp_baru,"tarikan_lama"=>$req->tarikan_lama,"tarikan_baru"=>$req->tarikan_baru]);
    }
    return back();
  }
  public function nextstep($id, Request $req){
    DB::table('maintaince')->where('id', $req->id)->update(["step_id"=>$req->nextstep]);
    if($req->nextstep == 1){
      DB::statement('INSERT INTO maintenance_validasi_port_awal(maintenance_id, port, jenis_layanan, no_layanan, qrcode_spl, qrcode_dc, status) SELECT maintenance_id, port, jenis_layanan, no_layanan, qrcode_spl, qrcode_dc, status FROM maintenance_validasi_port WHERE maintenance_id = '.$req->id);
    }
    return back();
  }
  public function reboundaryForm($id){
    $data = DB::table('maintaince')->where('id', $id)->first();
    $list = DB::table('maintenance_reboundary')->where('maintenance_id', $id)->get();
    return view('tech.reboundary', compact('data', 'list'));
  }
  public function registerODPTerbuka($id){
    $user = session('auth')->id_user;
    $regu = DB::table('regu')->select('*', 'id_regu as id', 'uraian as text')->where('nik1', $user)->orWhere('nik2', $user)->get();
    $sto = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();
    return view('tech.registerODPTerbuka', compact('regu', 'sto'));
  }

  public function saveRegisterODPTerbuka($id, Request $req){
    $rules = array(
      'nama_odp' => 'required',
      'koordinat' => 'required',
      'sto' => 'required'
    );
    $messages = [
      'nama_odp.required' => 'Silahkan isi kolom "Nama ODP" Bang!',
      'koordinat.required' => 'Silahkan isi kolom "Koordinat" Bang!',
      'sto.required' => 'Silahkan Pilih "sto" Bang!'
    ];
    $validator = Validator::make($req->all(), $rules, $messages);
    if ($validator->fails()) {
      return redirect()->back()
          ->withInput($req->all())
          ->withErrors($validator)
          ->with('alerts', [
            ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan order']
          ]);
    }
    $auth = session('auth');

    $stts = DB::table('maintenance_jenis_order')->where('id', $req->jenis_order)->first();
    $datasto = DB::table('maintenance_datel')->where('sto', $req->sto)->first();
    $regu = DB::table('regu')->where('id_regu', $req->regu)->first();
    if(DB::table('maintaince')->where([
      ['no_tiket', $req->nama_odp],
      ['isHapus', 0]
    ])->whereNull('status')->first()){
      return redirect()->back()
        ->withInput($req->all())
        ->with('alerts', [
            ['type' => 'danger', 'text' => '<strong>Nomor Tiket Sudah ada di Databas Marina</strong>']
          ]);
    }else{
      $id_mt = DB::table('maintaince')->insertGetId([
                "no_tiket"          =>$req->nama_odp,
                "jenis_order"       =>$req->jenis_order,
                "nama_order"        =>$stts->nama_order,
                "headline"          =>$req->nama_odp.'|'.date('ymd'),
                "pic"               =>$auth->id_karyawan."/".$auth->nama,
                "koordinat"         =>$req->koordinat,
                "created_at"        =>DB::raw('now()'),
                "created_by"        =>$auth->id_karyawan,
                "kandatel"          =>$datasto->datel,
                "sto"               =>$req->sto,
                "nama_odp"          =>$req->nama_odp,
                "dispatch_regu_id"  =>@$regu->id_regu,
                "dispatch_regu_name"=>@$regu->uraian,
                "username1"         =>@$regu->nik1,
                "username2"         =>@$regu->nik2
      ]);
      if($req->input('regu')){
        // DB::table('maintenance_dispatch_log')->insert([
        //       "maintenance_id"  =>$id_mt,
        //       "regu_id"         =>@$regu->id_regu,
        //       "regu_name"       =>@$regu->uraian,
        //       "ts_dispatch"     =>DB::raw('now()'),
        //       "dispatch_by"     =>$auth->id_karyawan,
        //       "action"     =>"dispatch"
        // ]);
        DB::table('maintenance_note')
        ->insert([
          'mt_id'     => $id_mt,
          'catatan'   => "DISPATCH ODP TERBUKA",
          'status'   => "DISPATCH ODP TERBUKA",
          'pelaku'    => session('auth')->id_karyawan,
          'ts'     => DB::raw('NOW()')
        ]);
        exec('cd ..;php artisan sendDispatch '.$id_mt.' > /dev/null &');
      }
      // if user/tekisi maka ke home mereka
      if($auth->maintenance_level){
        return redirect()->back()->with('alerts', [
            ['type' => 'success', 'text' => '<strong>sukses menginput order</strong>']
          ]);
      }
      else{
        return redirect('/hometech')->with('alerts', [
            ['type' => 'success', 'text' => '<strong>sukses menginput order</strong>']
          ]);
      }
    }
  }
  public function formValins($id){
    $data = DB::table('maintaince')->where('id', $id)->first();
    // $valins = DB::table('valins')->where('VALINS_ID', $data->valins_id)->whereNotNull('VALINS_ID')->where('VALINS_ID', '!=', '0')->get();
    $valins = DB::select('SELECT valins.*, mv.maintenance_id,
    (CASE WHEN mv.jenis = "sebelum" THEN "ID Valins Gendong Sebelum"
    WHEN mv.jenis = "sesudah" THEN "ID Valins Gendong Sesudah"
    WHEN mv.jenis = "reboundary" THEN "ID Valins ODP Reboundary"
    ELSE "Tidak Ada!" END) as Jenis_mv, mv.koordinat_odp, mv.koor_pelanggan
    from valins
    left join maintenance_valins mv on valins.valins_id=mv.valins_id
    where mv.maintenance_id='.$id);
    // dd($valins);
    return view('tech.formValins', compact('data', 'valins'));
  }

  public function saveFormValins($id, Request $req){
    $rules = array(
      'valins_id' => 'required|numeric|min:0|not_in:0',
      'kapasitas' => 'required|numeric|min:0|not_in:0',
      'odp_koor' => 'required'
    );
    $messages = [
      'valins_id.required' => 'Silahkan isi "ID VALINS" Bang!',
      'kapasitas.required' => 'Silahkan pilih "KAPASITAS ODP" Bang!',
      'odp_koor.required' => 'Silahkan Masukkan Koordinat ODP!'
    ];
    $validator = Validator::make($req->all(), $rules, $messages);
    if ($validator->fails()) {
      return redirect()->back()
        ->withInput($req->all())
        ->withErrors($validator)
        ->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Isian kurang lengkap']
        ]);
    }
    GraberController::valinsphp($req->valins_id, $req->kapasitas);
    // DB::table('maintaince')->where('id', $id)->update([
    //   'valins_id'     =>$req->valins_id,
    //   'kapasitas_odp' =>$req->kapasitas
    // ]);
    $get_port = DB::SELECT(
      "SELECT
        SUM(CASE WHEN ONU_SN = '' THEN 1 ELSE 0 END) as OS,
        SUM(CASE WHEN ONU_SN != '' THEN 1 ELSE 0 END) as OS_avail
        FROM maintenance_valins mv LEFT JOIN valins v ON mv.valins_id = v.VALINS_ID
        WHERE maintenance_id = $id"
    )[0];

    $sisa = $used = 0;

    if ($get_port) {
      $sisa = $get_port->OS;
      $used = $get_port->OS_avail;
    }
    DB::table('maintenance_valins')->insert([
      'valins_id'       =>$req->valins_id,
      'maintenance_id'  =>$id,
      'kapasitas'       =>$req->kapasitas,
      'koordinat_odp'   =>$req->odp_koor,
      'barcode_kode'    =>$req->barcode_kode,
      'koor_pelanggan'  =>$req->koor_pelanggan,
      'jenis'           =>$req->jenis_valins,
      'sisa_port'       =>$sisa,
      'used_port'       =>$used,
    ]);
    return redirect('/tech/'.$id)->with('alerts', [
      ['type' => 'success', 'text' => '<strong>Berhasil</strong> data valin sudah disimpan']
    ]);
  }

  public function list_odc_alpro()
  {
    $data = DB::table('maintenance_sector As ms')->get();
    return view('order.listOdcAlpro', compact('data'));
  }

  public function detail_list_sector($id)
  {
    $data = DB::table('maintenance_sector As ms')
    ->leftJoin('maintenance_sector_alpro As msa', 'ms.id', '=', 'msa.sector_id')
    ->select('ms.*', 'msa.alpro')
    ->where('ms.id', $id)
    ->get();

    $title = DB::table('maintenance_sector As ms')
    ->where('ms.id', $id)
    ->first();

    return view('order.detailListSector', compact('data', 'title'));
  }

  public function ibooster_search(Request $req)
  {
    $data = [];
    $rq = [];
    if($req->all()) {
      $rq = $req->all();
      if($req->jenis_srch == 0){
        $search = "no_inet";
      } else{
        $search = "no_tiket";
      }

      $rawd = DB::table('maintaince')
      ->where([
        [$search, $req->order_id],
        ['jenis_order', 4],
        ['isHapus', 0]
      ])
      ->where(function($join) {
        $join->where([
          ['status', 'not like', 'kendala%']
        ])
        ->whereNotIn('status', ['cancel order', 'close', 'UP'])
        ->OrWhereNull('status');
      })->get();

      foreach($rawd as $key => $d){
        $ibooster = GraberController::grabIbooster($d->no_inet);
        $ibos = json_decode($ibooster);

        if($ibos[0]->ONU_Rx){
          $data[$key] = $d;
          $data[$key]->iboost = $ibos[0]->ONU_Rx;
        }
      }
    }
    // dd($data);
    return view('order.ibooster_search', compact('data', 'rq'));
  }

  public function update_ibooster($jns, $id)
  {
    DB::table('maintaince')->where('id', $id)->update([
      'status' => $jns
    ]);
  }

  public function update_photo_ajax(Request $req)
  {
    // dd($req->all());

    $exists = DB::select('
      SELECT *
      FROM maintaince
      WHERE id = ?
    ',[
      $req->id
    ]);

    $data = $exists[0];

    // $foto = $this->photoCommon;
    // if($data->jenis_order == 3){
    //   $foto = $this->photoOdpLoss;
    // }else if($data->jenis_order == 4){
    //   $foto = $this->photoRemo;
    // }else if($data->jenis_order == 5){
    //   $foto = $this->photophotoUtilitas;
    // }else if($data->jenis_order == 8){
    //   $foto = $this->photoOdpSehat;
    // }else if($data->jenis_order == 1 || $data->jenis_order == 19){
    //   $foto = $this->photoBenjar;
    // }else if($data->jenis_order == 10){
    //   $foto = $this->photoValidasi;
    // }else if($data->jenis_order == 17){
    //   $foto = $this->photoNormalisasi;
    // }else if($data->jenis_order == 9){
    //   $foto = $this->photoReboundary;
    // }else if($data->jenis_order == 20){
    //   $foto = $this->photoODCSehat;
    // }else if($data->jenis_order == 24){
    //   $foto = $this->photoIXSA;
    // }else if($data->jenis_order == 2){
    //   $foto = $this->photoGamas;
    // }

    $get_field_photo = DB::table('maintenance_evident')->select('evident')->where('program_id', $data->jenis_order)->get();
    $get_field_photo = json_decode(json_encode($get_field_photo), TRUE);

    $get_field_photo = array_map(function($x)
    {
      return $x['evident'];
    }, $get_field_photo);

    return $this->handleFileUpload($req, $req->id, $get_field_photo);
  }

  public function ajax_umur_speedy(Request $req)
  {
    $data = DB::table('maintaince')->where([
      ['no_inet', $req->inet ],
      ['jenis_order', $req->jenis_order ],
      ['isHapus', 0],
      [DB::raw('DATE_FORMAT(created_at, "%Y-%m")'), '=', DB::raw('DATE_FORMAT(NOW(), "%Y-%m")')]
    ]);

    if($req->id != 'all')
    {
      $sql = $data->where('id', '!=', $req->id)->first();
    }
    else
    {
      $sql = $data->first();
    }

    return \Response::json($sql);
  }

	public function add_log_manual(Request $request, $id)
	{
    $data = DB::select("SELECT * FROM maintaince WHERE id = $id")[0];
    $auth = session('auth');
    $status_mt = $request->status;
    // dd($request->all(), $request->count_me);

    return DB::transaction( function() use($request, $data, $auth, $status_mt, $id)
    {
      $count_me = $request->count_me;
      if(!in_array($count_me, [4, 5]) )
      {
        Session::flash('alerts',[['type' => 'success', 'text' => '<strong>Approve</strong> Berhasil Disimpan']]);
      }
      else
      {
        Session::flash('alerts',[['type' => 'danger', 'text' => '<strong>Deline</strong> Berhasil Disimpan, WO Akan Dikembalikan Ke Teknisi']]);
        // $status_mt = NULL;
      }

      switch ($count_me) {
        case 1:
          $nama_note = 'VERIF TA';
        break;
        case 2:
          $nama_note = 'VERIF TELKOM';
        break;
        case 3:
          $nama_note = 'SIAP REKON';
        break;
        case 4:
          $nama_note = 'DECLINE TA';
        break;
        case 5:
          $nama_note = 'DECLINE TELKOM';
        break;
      }

      if($request->komen)
      {
        $komen = $request->komen;
      }
      else
      {
        $komen = $nama_note;
      }

      DB::table('maintenance_note')
      ->insert([
        'mt_id'     => $data->id,
        'catatan'   => $komen,
        'status'   => $nama_note,
        'mtr_json'   => $request->materials,
        'pelaku'    => $auth->id_karyawan,
        'ts'     => DB::raw('NOW()')
      ]);

      $saldo = json_decode($request->saldo);
      $materials = json_decode($request->materials);
      // dd($materials);
      $materialsNte = json_decode($request->materialsNte);

      $check_nte = DB::table('maintaince')
      ->leftJoin('psb_laporan', 'maintaince.psb_laporan_id', '=', 'psb_laporan.id')
      ->leftJoin('nte', 'psb_laporan.id_tbl_mj', '=', 'nte.id_dispatch_teknisi')
      ->select('nte.id_dispatch_teknisi')
      ->where('psb_laporan.id', $data->psb_laporan_id)
      ->first();

      if(@$check_nte->id_dispatch_teknisi)
      {
        foreach($materialsNte as $mNte)
        {
          if ($mNte->kat == "typeont")
          {
            DB::table('psb_laporan')->where('id', $data->psb_laporan_id)->update([
              'typeont'             => $mNte->jenis,
              'snont'               => $mNte->id
            ]);
          }
          elseif ($mNte->kat == "typestb")
          {
            DB::table('psb_laporan')->where('id', $data->psb_laporan_id)->update([
              'typestb'             => $mNte->jenis,
              'snstb'               => $mNte->id
            ]);
          }

          DB::table('nte')->where('sn', $mNte->id)->update([
            'status'              => 1,
            'qty'                 => $mNte->qty,
            'id_dispatch_teknisi' => $check_nte->id_dispatch_teknisi,
            'position_key'        => $auth->id_karyawan
          ]);
        }
      }

      if($saldo)
      {
        foreach($saldo as $s)
        {
          $s->id_pemakaian = $id;
          $s->action = 2;
          $s->value = $s->pemakaian*-1;
          $s->created_at = date('Y-m-d H:i:s');
          $s->created_by = $auth->id_karyawan;
          unset($s->id);
          unset($s->tpemakaian);
          unset($s->pemakaian);
          unset($s->kembalian);
          unset($s->sisa);
        }

        Saldo::delete($id,2);
        Saldo::insert(json_decode(json_encode($saldo), true));
      }

      DB::table('maintaince_mtr')
      ->where('maintaince_id', $data->id)
      ->delete();

      $mtrcount = 0;

      foreach($materials as $material) {
        DB::table('maintaince_mtr')->insert([
          'maintaince_id' => $data->id,
          'id_item'       => $material->id_item,
          'qty'           => $material->qty,
        ]);
        $mtrcount++;
      }

      DB::TABLE('maintaince')->where('id', $id)->update([
        'status' => $status_mt,
        'status_name' => $status_mt,
        'action' => $request->action,
        'mtrcount' => $mtrcount,
        'modified_by' => session('auth')->id_karyawan,
        'verify' => $count_me
      ]);

      $redirect = [
        'url' => '/tech'
      ];

      return $redirect;
    });
	}

	public function list_decline(Request $req)
	{
    $sql_strict = '';

    if(in_array(session('auth')->maintenance_level, [5, 6]))
    {
      $sql_strict .= ' mr.nama_mitra = "'.session('auth')->nama_instansi .'" AND';
    }

    $tgl = date('Y-m-d');

    if($req->all())
    {
      $tgl = $req->tgl. date('d');
    }

    $sql_strict .= " DATE_FORMAT(m.created_at, '%Y-%m-%d') BETWEEN '$tgl' - INTERVAL 1 MONTH AND '$tgl' AND";

    $list = DB::select("SELECT m.*, m.nama_odp as nama_odp_m, concat('DECLINE') as last_stts, mr.nama_mitra, sub1.pelaku, sub1.catatan as cttn_decline, sub1.id as id_sub
    FROM maintaince m
    LEFT JOIN regu mr on m.dispatch_regu_id = mr.id_regu
    LEFT JOIN maintenance_note sub1 ON sub1.mt_id = m.id
    WHERE $sql_strict sub1.status = 'DECLINE' AND m.isHapus = 0 AND verify IN(4, 5) AND LENGTH(mr.nama_mitra) > 9 ORDER BY m.id DESC, sub1.id DESC");
    $data = $final_data = $raw_data1 = [];

    foreach($list as $k => $d)
    {
      if(json_encode($d) != false){
        $data[$k] = $d;
      }else{
        $wrong[] = $d;
        $data[$k] = (object)preg_replace('/[[:^print:]]/', '', (array)$d);
      }
    }

    foreach($data as $key => $val)
    {
      $raw_data1[$key] = $val;
    }

    // foreach($raw_data1 as $val)
    // {
    //   $get_max = max( array_column( (array)$val, 'id_sub' ) );

    //   foreach($val as $val_c1)
    //   {
    //     if($val_c1->id_sub == $get_max)
    //     {
    //       $final_data[] = $val_c1;
    //     }
    //   }
    // }
    // dd($lis  t, $raw_data1);
    return view('order.decline', ['data' => $raw_data1], compact('tgl') )->withInput($req->all());
	}

	public function ajax_order_id(Request $req)
	{
		$sql = DB::table('maintaince')->where([
      ['no_tiket', trim($req->order_id) ],
      ['id', '!=', $req->id],
      ['isHapus', 0],
    ]);

    if($req->jenis_order == 28)
    {
      $sql->Where('jenis_order', '!=', 1);
    }

    $sql = $sql->first();

    return \Response::json($sql);
	}

	public function get_valins_data(Request $req)
	{
		$sql = DB::table('bank_valins')->where([
      ['app', 'MARINA'],
      ['id_wo', $req->id_wo],
      ['id_valins', $req->id_valins],
    ])->get();

    if(count($sql) == 0)
    {
      $check_valins = GraberController::grab_valins($req->id_valins);
      $jenis_valins = $req->jenis_valins;

      if($check_valins)
      {
        DB::Table('bank_valins')->where([
          ['app', 'MARINA'],
          ['id_wo', $req->id_wo],
          ['jenis_valins', $jenis_valins]
        ])->delete();

        foreach ($check_valins as $k => $v_val)
        {
          DB::table('bank_valins')->insert([
            'app'          => 'MARINA',
            'id_wo'        => $req->id_wo,
            'jenis_valins' => $jenis_valins,
            'port_odp'     => $v_val['port_odp'],
            'id_valins'    => $v_val['id_valins'],
            'time_valins'  => $v_val['time_valins'],
            'odp'          => $v_val['odp'],
            'onu_id'       => $v_val['onu_id'],
            'onu_sn'       => $v_val['onu_sn'],
            'sip1'         => $v_val['sip1'],
            'sip2'         => $v_val['sip2'],
            'no_hsi1'      => $v_val['no_hsi1'],
            'created_by'   => session('auth')->id_karyawan
          ]);

          DB::table('bank_valins_log')->insert([
            'app'          => 'MARINA',
            'id_wo'        => $req->id_wo,
            'jenis_valins' => $jenis_valins,
            'port_odp'     => $v_val['port_odp'],
            'id_valins'    => $v_val['id_valins'],
            'time_valins'  => $v_val['time_valins'],
            'odp'          => $v_val['odp'],
            'onu_id'       => $v_val['onu_id'],
            'onu_sn'       => $v_val['onu_sn'],
            'sip1'         => $v_val['sip1'],
            'sip2'         => $v_val['sip2'],
            'no_hsi1'      => $v_val['no_hsi1'],
            'created_by'   => session('auth')->id_karyawan
          ]);
        }
      }
    }

    $sql = DB::table('bank_valins')->where([
      ['app', 'MARINA'],
      ['id_wo', $req->id_wo],
      ['id_valins', $req->id_valins],
    ])->get();

    return \Response::json($sql);
	}
}
