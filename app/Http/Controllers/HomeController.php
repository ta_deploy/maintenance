<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\DA\User;
use Telegram;
class HomeController extends Controller
{
  public function index()
  {
    $id = date('Y-m-d');
    $auth = session('auth');
    if($auth->maintenance_level){
      $data = DB::select("SELECT mjo.*,
        SUM(CASE WHEN status = 'close' THEN 1 ELSE 0 END) as close,
        SUM(CASE WHEN status = 'kendala pelanggan' THEN 1 ELSE 0 END) as kendala_pelanggan,
        SUM(CASE WHEN status = 'kendala teknis' THEN 1 ELSE 0 END) as kendala_teknis
        from maintenance_jenis_order mjo LEFT JOIN maintaince ON jenis_order = mjo.id WHERE isHapus = 0 AND mjo.nama_order NOT IN('GAMAS', 'DEDICATED', 'IOAN-UPATEK', 'IOAN-CUI', 'IOAN-TA') AND (tgl_selesai IS NULL OR tgl_selesai like '$id%') GROUP BY mjo.id");

      $action = DB::select("
        SELECT SUM(CASE WHEN tgl_selesai IS NOT NULL THEN 1 ELSE 0 END) as rows,
        action_cause
        FROM  maintaince where isHapus = 0 AND tgl_selesai like '$id%'
        GROUP BY  action_cause
        ORDER BY  action_cause");

      $gamas = DB::SELECT("
        SELECT mgc.gamas_cause,
        SUM(CASE WHEN tgl_selesai IS NOT NULL THEN 1 ELSE 0 END) as rows
        FROM maintenance_gamas_cause mgc LEFT JOIN maintaince m ON mgc.id = m.gamas_cause WHERE m.isHapus = 0 AND tgl_selesai LIKE '$id%' or tgl_selesai IS NULL
        GROUP BY mgc.gamas_cause
        ORDER BY mgc.gamas_cause
        ");
      $remo = new \stdClass;
            // $remo = DB::select('select *,
            //   (select count(rs.id) from remo_servo rs left join maintaince m on rs.NO_SPEEDY = m.no_tiket where NO_SPEEDY != "" and m.status is null) as order_remo,
            //   (select count(id) from maintaince where jenis_order = mjo.id and tgl_selesai like "'.$id.'%" and status = "close") as close,
            //   (select count(id) from maintaince where jenis_order = mjo.id and tgl_selesai like "'.$id.'%" and status = "kendala pelanggan") as kendala_pelanggan,
            //   (select count(id) from maintaince where jenis_order = mjo.id and tgl_selesai like "'.$id.'%" and status = "kendala teknis") as kendala_teknis
            //   from maintenance_jenis_order mjo where mjo.id = 4')[0];
      $line = new \stdClass;
    //         $line = DB::select("select *,
    // (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm
    //           left join khs_maintenance i on mm.id_item=i.id_item
    //           left join maintaince m on mm.maintaince_id = m.id
    //           left join maintenance_regu mr on m.dispatch_regu_id = mr.id
    //           WHERE DATE_FORMAT(m.tgl_selesai,'%m-%Y')=bulan and mr.mitra = 'PT UPATEK' and m.jenis_order!=4) as upatek,
    //     (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm
    //           left join khs_maintenance i on mm.id_item=i.id_item
    //           left join maintaince m on mm.maintaince_id = m.id
    //           left join maintenance_regu mr on m.dispatch_regu_id = mr.id
    //           WHERE DATE_FORMAT(m.tgl_selesai,'%m-%Y')=bulan and mr.mitra = 'SPM' and m.jenis_order!=4) as spm,
    //     (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm
    //           left join khs_maintenance i on mm.id_item=i.id_item
    //           left join maintaince m on mm.maintaince_id = m.id
    //           left join maintenance_regu mr on m.dispatch_regu_id = mr.id
    //           WHERE DATE_FORMAT(m.tgl_selesai,'%m-%Y')=bulan and mr.mitra = 'CUI' and m.jenis_order!=4) as cui,
    //     (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm
    //           left join khs_maintenance i on mm.id_item=i.id_item
    //           left join maintaince m on mm.maintaince_id = m.id
    //           left join maintenance_regu mr on m.dispatch_regu_id = mr.id
    //           WHERE DATE_FORMAT(m.tgl_selesai,'%m-%Y')=bulan and mr.mitra = 'AGB' and m.jenis_order!=4) as agb from revbulan where bulan like '%".date('Y')."'");
            // dd($action);
      return view('home', compact('data', 'action', 'remo', 'line', 'gamas'));
    }
    else
      return redirect('/hometech');
  }
  public function ajaxHome($id, $jenis_order){
    $data = DB::select("SELECT mjo.*,
        SUM(CASE WHEN status = 'close' THEN 1 ELSE 0 END) as close,
        SUM(CASE WHEN status = 'kendala pelanggan' THEN 1 ELSE 0 END) as kendala_pelanggan,
        SUM(CASE WHEN status = 'kendala teknis' THEN 1 ELSE 0 END) as kendala_teknis
        from maintenance_jenis_order mjo LEFT JOIN maintaince ON jenis_order = mjo.id WHERE isHapus = 0 AND mjo.nama_order NOT IN('GAMAS') AND (tgl_selesai IS NULL OR tgl_selesai like '$id%') GROUP BY mjo.id");

    $action = DB::select("SELECT SUM(CASE WHEN tgl_selesai IS NOT NULL THEN 1 ELSE 0 END) as rows,
      action_cause
      FROM  maintaince where isHapus = 0 AND tgl_selesai like '$id%'
      GROUP BY  action_cause
      ORDER BY  action_cause");

    $gamas = DB::SELECT("
      SELECT mgc.gamas_cause,
      SUM(CASE WHEN tgl_selesai IS NOT NULL THEN 1 ELSE 0 END) as rows
      FROM maintenance_gamas_cause mgc LEFT JOIN maintaince m ON mgc.id = m.gamas_cause WHERE m.isHapus = 0 AND tgl_selesai LIKE '$id%' or tgl_selesai IS NULL
      GROUP BY mgc.gamas_cause
      ORDER BY mgc.gamas_cause
      ");
    $remo = new \stdClass;
      // $remo = DB::select('select *,
      //   (select count(rs.id) from remo_servo rs left join maintaince m on rs.NO_SPEEDY = m.no_tiket where NO_SPEEDY != "" and m.status is null) as order_remo,
      //   (select count(id) from maintaince where jenis_order = mjo.id and tgl_selesai like "'.$id.'%" and status = "close") as close,
      //   (select count(id) from maintaince where jenis_order = mjo.id and tgl_selesai like "'.$id.'%" and status = "kendala pelanggan") as kendala_pelanggan,
      //   (select count(id) from maintaince where jenis_order = mjo.id and tgl_selesai like "'.$id.'%" and status = "kendala teknis") as kendala_teknis
      //   from maintenance_jenis_order mjo where mjo.id = 4')[0];
    $line = DB::select("select *,
      (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm
      left join khs_maintenance i on mm.id_item=i.id_item
      left join maintaince m on mm.maintaince_id = m.id
      left join maintenance_regu mr on m.dispatch_regu_id = mr.id
      WHERE m.isHapus = 0 AND DATE_FORMAT(m.tgl_selesai,'%m-%Y')=bulan and mr.mitra = 'PT UPATEK' and m.jenis_order!=4) as upatek,
      (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm
      left join khs_maintenance i on mm.id_item=i.id_item
      left join maintaince m on mm.maintaince_id = m.id
      left join maintenance_regu mr on m.dispatch_regu_id = mr.id
      WHERE m.isHapus = 0 AND DATE_FORMAT(m.tgl_selesai,'%m-%Y')=bulan and mr.mitra = 'SPM' and m.jenis_order!=4) as spm,
      (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm
      left join khs_maintenance i on mm.id_item=i.id_item
      left join maintaince m on mm.maintaince_id = m.id
      left join maintenance_regu mr on m.dispatch_regu_id = mr.id
      WHERE m.isHapus = 0 AND DATE_FORMAT(m.tgl_selesai,'%m-%Y')=bulan and mr.mitra = 'CUI' and m.jenis_order!=4) as cui,
      (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm
      left join khs_maintenance i on mm.id_item=i.id_item
      left join maintaince m on mm.maintaince_id = m.id
      left join maintenance_regu mr on m.dispatch_regu_id = mr.id
      WHERE m.isHapus = 0 AND DATE_FORMAT(m.tgl_selesai,'%m-%Y')=bulan and mr.mitra = 'AGB' and m.jenis_order!=4) as agb from revbulan");
    return json_encode(['datachart' => $data, 'action' => $action, 'remo' => $remo, 'line' => $line, 'gamas' => $gamas]);
//      return view('ajaxHome', compact('data', 'action', 'remo'));
  }
  public function absen()
  {
    $auth = session('auth');

  }
  public function absenTech()
  {
    $auth = session('auth');
    User::absen($auth->id_karyawan);
    // $chat_id = "-200657991";
    // $msg = "User ".$auth->id_karyawan."/".$auth->nama." sudah absen, silahkan TL yg bersangkutan memverifikasi kehadiran nya.tks";
    // Telegram::sendMessage([
    //   'chat_id' => $chat_id,
    //   'parse_mode' => 'html',
    //   'text' => $msg
    // ]);
    return redirect()->back();
  }
  public function absenVerif(Request $req)
  {
    $auth = session('auth');
    User::verifAbsen($req->id_user, $auth->id_karyawan);
    // $chat_id = "-200657991";
    // $msg = "User ".$req->id_user."/".$req->nama." sudah di verifikasi kehadiran nya by ".$auth->id_karyawan."/".$auth->nama.".tks";
    // Telegram::sendMessage([
    //   'chat_id' => $chat_id,
    //   'parse_mode' => 'html',
    //   'text' => $msg
    // ]);
    return redirect()->back();
  }


  public function formPwd($nik)
  {
    $data = DB::table('akun')->where('user', $nik)->first();
    return view('changePwd', compact('data'));
  }
  public function savePwd($nik, Request $req)
  {
    DB::table('akun')->where('user', $nik)->update(['pwd'=>$req->pwd]);

    return redirect()->back();

  }
}
