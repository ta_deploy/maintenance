<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\DA\Commerce;
class CommerceController extends Controller
{
  protected $file = [
      'nde', 'justifikasi'
    ];
  public function adminList(){
    $data = Commerce::getByStatus("Inbox Admin");
    return view('commerce.admin.list', compact('data'));
  }
  public function input($id){
    $data = Commerce::getById($id);
    return view('commerce.admin.input', compact('data'));
  }
  public function adminSave($id, Request $req){
    $data = Commerce::getById($id);
    if($data){
      Commerce::adminUpdate($id, $req);
    }else{
      $id = Commerce::adminInput($req);
    }
    $query_file = $this->handleFileUpload($req,$id,$this->file,public_path().'/upload/commerce/admin/'.$id.'/');
    if($query_file){
      Commerce::updateFiles($id, $query_file);
    }
    return redirect('/commerce/admin/list');
  }
  public function approvalList(){
    $data = Commerce::getByStatus("Inbox Approval");
    return view('commerce.approval.list', compact('data'));
  }
  public function approval($id){
    $data = Commerce::getById($id);
    return view('commerce.approval.input', compact('data'));
  }
  public function approvalSave($id, Request $req){
    Commerce::approvalUpdate($id, $req);
    return redirect('/commerce/approval/list');
  }

  public function submit_cutoff(Request $req)
  {
    $data = Commerce::submitcutoff($req);
    return $data;
  }

  public function undo_cutoff($id)
  {
    Commerce::undo_cutoff($id);
    return redirect('/commerce/admin/list');
  }

  private function handleFileUpload($request, $id, $file, $path){
    $nama = array();
    foreach($file as $name) {
      $input = $name;
      // dd($input);
      if ($request->hasFile($input)) {
        // dd("ada");
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true))
            return 'Gagal menyiapkan folder';
        }
        $file = $request->file($input);
        //TODO: path, move, resize
        try {
          $ext = $file->clientExtension();
          $moved = $file->move("$path", "$name.$ext");
          $nama[$name] = "$name.$ext";
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'Gagal upload';
        }
      }
      // dd("takde");
    }
    return $nama;
  }

  public function get_cutoff()
  {
    $data = Commerce::data_cutoff();
    return \Response::json($data);
  }
}
