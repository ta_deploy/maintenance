<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use DB;
use Telegram;

class GraberController extends Controller
{
  public function grabWMTA(){
    $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://speakup.telkomakses.co.id/wtmta/index.php?r=site/login');
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
        curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
        curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=97150038&LoginForm%5Bpassword%5D=banjarmasin%4012345&yt0=");
        $result = curl_exec($ch);

        curl_setopt($ch, CURLOPT_URL, 'https://speakup.telkomakses.co.id/wtmta/index.php?r=masteraset/downloadexcelsn&gudang=50&productseries=&jenis=');
        $result = curl_exec($ch);
        curl_close($ch);
        $dom = @\DOMDocument::loadHTML(trim($result));
        $table = $dom->getElementsByTagName('table')->item(0);
        $rows = $table->getElementsByTagName('tr');
        $columns = array(
                1 =>'serial_number',
                'product_series',
                'jenis_barang',
                'merk_barang',
                'nama_product',
                'spesifikasi',
                'kategori_asset',
                'nama_gudang',
                'regional',
                'fiberzone',
                'kondisi',
                'status',
                'nik_pemakai',
                'nama_pemakai',
                'position_name',
                'group_fungsi',
                'psa',
                'create_date',
                'creator',
                'no_do',
                'no_po',
                'peruntukan',
                'owner',
                'id_permintaan',
                'status_ba',
                'jenis_asset'
            );
        $result = array();
        for ($i = 2, $count = $rows->length; $i < $count; $i++)
        {
            $cells = $rows->item($i)->getElementsByTagName('td');
            $data = array();
            for ($j = 1, $jcount = count($columns); $j < $jcount; $j++)
            {
                $td = $cells->item($j);
                $data[$columns[$j]] =  $td->nodeValue;
            }
            $result[] = $data;
            $dataarray[] = $data;
        }
        $srcarr=array_chunk($result,500);
        DB::table('aset_wmta')->truncate();
        foreach($srcarr as $item) {
            DB::table('aset_wmta')->insert($item);
        }
        dd($result);
  }
  public function grab_kpro_tot(){
    //perform header
    $postdata = http_build_query(
        array(
            "sql" => "SELECT distinct od.ORDER_ID, JENISPSB, REGIONAL, WITEL, DATEL, STO, LOC_ID, NCLI, POTS, SPEEDY, TYPE_TRANS, TYPE_LAYANAN, CUSTOMER_NAME, CONTACT_HP, INS_ADDRESS, GPS_LONGITUDE, GPS_LATITUDE, KCONTACT, STATUS_RESUME, STATUS_MESSAGE, to_char(order_date,'yyyy-mm-dd hh24:mi:ss') as order_date, to_char(last_updated_date,'yyyy-mm-dd hh24:mi:ss') as last_updated_date, STATUS_VOICE, STATUS_INET, STATUS_ONU, STATUS_REDAMAN, OLT_RX, ONU_RX, SNR_UP, SNR_DOWN, UPLOAD, DOWNLOAD, LAST_PROGRAM, CLID, to_char(last_start,'dd-mm-yyyy hh24:mi:ss') last_start, to_char(last_view,'dd-mm-yyyy hh24:mi:ss') last_view , to_char(ukur_time,'dd-mm-yyyy hh24:mi:ss') ukur_time, TINDAK_LANJUT, ISI_COMMENT, USER_ID_TL, to_char(TL_DATE,'dd-mm-yyyy hh24:mi:ss') as tgl_comment, to_char(SCHEDULE_LABOR,'dd-mm-yyyy hh24:mi:ss') as SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task, tek.device_id
            from KPRO_DETAIL_ORDER od LEFT OUTER JOIN kpro_hasil_ukur uk on od.order_id=uk.order_id
            LEFT OUTER JOIN (select order_id, SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task,device_id from kpro_detail_wfm where wo_parent is not null) tek on od.order_id=tek.order_id where ((status_resume not like 'Completed (PS)' and status_resume not like 'UN%' and lower(status_resume) not like ('%cancel%') and lower(status_resume) not like ('%revoke%')) or (status_resume like 'Completed (PS)' and TO_CHAR(last_updated_date,'yyyy-mm-dd') = TO_CHAR(CURRENT_DATE,'yyyy-mm-dd')))  and  regional='6' and  witel='BANJARMASIN' and  1=1  and  1=1  and  1=1  and  1=1  and  1=1  order by order_date",
            "tombol" => "Download to Excel"
        )
    );
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );
    $context  = stream_context_create($opts);

    // get excel content
    $excel = file_get_contents('http://10.62.170.27/downloadreport.php', false, $context);

    //performe convert to variable
    $dom = @\DOMDocument::loadHTML(trim($excel));
    $columns = array("no", "regional", "witel", "datel", "sto", "order_id", "type_transaksi", "jenis_layanan", "alpro", "ncli", "pots","speedy", "status_resume", "status_message", "order_date", "last_update_sts", "nama_cust", "no_hp", "alamat", "kcontact", "lng", "lat","wfm_id", "status_wfm", "desk_task", "status_task", "tgl_install", "amcrew", "teknisi1", "hp_teknisi1", "teknisi2", "hp_teknisi2", "tinjut","ket","user","tgl_tinjut");

    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $data['sts'] = "tot";
        $result[] = $data;
    }
    DB::table("kpro_pi")->insert($result);
  }

  public function grab_kpro_val(){
    //perform header
    $postdata = http_build_query(
        array(
            "sql" => "SELECT distinct od.ORDER_ID, JENISPSB, REGIONAL, WITEL, DATEL, STO, LOC_ID, NCLI, POTS, SPEEDY, TYPE_TRANS, TYPE_LAYANAN, CUSTOMER_NAME, CONTACT_HP, INS_ADDRESS, GPS_LONGITUDE, GPS_LATITUDE, KCONTACT, STATUS_RESUME, STATUS_MESSAGE, to_char(order_date,'yyyy-mm-dd hh24:mi:ss') as order_date, to_char(last_updated_date,'yyyy-mm-dd hh24:mi:ss') as last_updated_date, STATUS_VOICE, STATUS_INET, STATUS_ONU, STATUS_REDAMAN, OLT_RX, ONU_RX, SNR_UP, SNR_DOWN, UPLOAD, DOWNLOAD, LAST_PROGRAM, CLID, to_char(last_start,'dd-mm-yyyy hh24:mi:ss') last_start, to_char(last_view,'dd-mm-yyyy hh24:mi:ss') last_view , to_char(ukur_time,'dd-mm-yyyy hh24:mi:ss') ukur_time, TINDAK_LANJUT, ISI_COMMENT, USER_ID_TL, to_char(TL_DATE,'dd-mm-yyyy hh24:mi:ss') as tgl_comment, to_char(SCHEDULE_LABOR,'dd-mm-yyyy hh24:mi:ss') as SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task, tek.device_id
from KPRO_DETAIL_ORDER od LEFT OUTER JOIN kpro_hasil_ukur uk on od.order_id=uk.order_id
LEFT OUTER JOIN (select order_id, SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task,device_id from kpro_detail_wfm where wo_parent is not null) tek on od.order_id=tek.order_id where status_resume = 'Process OSS (Provision Issued)' and (tindak_lanjut like 'Management Janji' or tindak_lanjut is null)  and  regional='6' and  witel='BANJARMASIN' and  1=1  and  1=1  and  1=1  and  1=1  and  1=1  order by order_date",
            "tombol" => "Download to Excel"
        )
    );
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );
    $context  = stream_context_create($opts);

    // get excel content
    $excel = file_get_contents('http://10.62.170.27/downloadreport.php', false, $context);

    //performe convert to variable
    $dom = @\DOMDocument::loadHTML(trim($excel));
    $columns = array("no", "regional", "witel", "datel", "sto", "order_id", "type_transaksi", "jenis_layanan", "alpro", "ncli", "pots","speedy", "status_resume", "status_message", "order_date", "last_update_sts", "nama_cust", "no_hp", "alamat", "kcontact", "lng", "lat","wfm_id", "status_wfm", "desk_task", "status_task", "tgl_install", "amcrew", "teknisi1", "hp_teknisi1", "teknisi2", "hp_teknisi2", "tinjut","ket","user","tgl_tinjut");

    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $data['sts'] = "val";
        $result[] = $data;
    }
    DB::table("kpro_pi")->insert($result);
  }
  public static function getQuestionPortal(){
    $html = file_get_contents('https://apps.telkomakses.co.id/portal/login.php');
    $html = substr_replace($html, '', 0, strpos($html, '<div id="captcha_part"'));
    $html = substr_replace($html, '', strpos($html, '<div class="footer text-center">'));
    echo $html;
    $dom = @\DOMDocument::loadHTML(trim($html));
    $id = $dom->getElementsByTagName("input")[0]->getAttribute("value");
    $exist = DB::table('QPortal')->where('id', $id)->first();
    if($exist){
      DB::table('QPortal')->where('id', $id)->update([
        'question'  => $html
      ]);
    }else{
      DB::table('QPortal')->insert([
        'id'        => $id,
        'question'  => $html
      ]);
    }

  }
  public static function grabIbooster($spd){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login1.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'ibooster.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $login = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($login));
    // dd($login);
    $input = $dom->getElementsByTagName('input')->item(0)->getAttribute("value");
    //$value = $input->item($i)->getAttribute("value");
    // dd($input);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login_code_a.php');
    // curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/login1.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $akun = DB::table('akun')->where('id', '7')->first();
    $username = $akun->user;
    $password = $akun->pwd;
    curl_setopt($ch, CURLOPT_POSTFIELDS, "sha256=".$input."&username=".$username."&password=".$password."&dropdown_login=sso");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    $rough_content = curl_exec($ch);

    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    //print_r($matches['cookie']);
    $cookiesOut = implode("; ", $matches['cookie']);
    dd($cookiesOut);
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    $cookie = null;
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/ibooster/ukur_massal_speedy.php');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "nospeedy=".$spd."&analis=ANALISA");
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    $result = curl_exec($ch);
    // dd($result);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($result, 0, $header_size);
    $result = substr($result, $header_size);
    $columns = array(
      1=> "ND",
      "IP_Embassy",
      "Type",
      "Calling_Station_Id",
      "IP_NE",
      "ADSL_Link_Status",
      "Upstream_Line_Rate",
      "Upstream_SNR",
      "Upstream_Attenuation",
      "Upstream_Attainable_Rate",
      "Downstream_Line_Rate",
      "Downstream_SNR",
      "Downstream_Attenuation",
      "Downstream_Attainable_Rate",
      "ONU_Link_Status",
      "ONU_Serial_Number",
      "Fiber_Length",
      "OLT_Tx",
      "OLT_Rx",
      "ONU_Tx",
      "ONU_Rx",
      "Framed_IP_Address",
      "MAC_Address",
      "Last_Seen",
      "AcctStartTime",
      "AccStopTime",
      "AccSesionTime",
      "Up",
      "Down",
      "Status_Koneksi",
      "Nas_IP_Address"
      );

    $dom = @\DOMDocument::loadHTML(trim($result));
    // print_r($result);
    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 3, $count = $rows->length; $i < $count; $i++)
    {
      $cells = $rows->item($i)->getElementsByTagName('td');
      $data = array();
      for ($j = 1, $jcount = count($columns); $j < $jcount; $j++)
      {
          //echo $j." ";
          $td = $cells->item($j);
          if (is_object($td)) {
            $node = $td->nodeValue;
          } else {
            $node = "empty";
          }
          $data[$columns[$j]] = $node;
      }
      $result[] = $data;
    }
    return json_encode($result);
    curl_close($ch);
  }

    public static function grabNosa(){
      $ch = curl_init();
      echo "login...\n";
      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/webclient/login/login.jsp');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
      curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
      $login = curl_exec($ch);


      $user_nossa = DB::table('user_nossa')->where('id', 2)->first();

      $dom = @\DOMDocument::loadHTML(trim($login));
      $input = $dom->getElementsByTagName('input')->item(3)->getAttribute("value");
      // dd($input);
      //$value = $input->item($i)->getAttribute("value");

      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/login');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "allowinsubframe=null&mobile=false&login=jsp&loginstamp=".$input."&username=".$user_nossa->username."&password=".$user_nossa->password);
      $result = curl_exec($ch);

      echo "filter owner dan status...\n";
      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/?event=loadapp&value=incident');
      curl_setopt($ch, CURLOPT_POST, false);
      $result = curl_exec($ch);
      $dom = @\DOMDocument::loadHTML(trim($result));
      $uisesid = $dom->getElementById('uisessionid')->getAttribute("value");
      $csrftokenholder = $dom->getElementById('csrftokenholder')->getAttribute("value");
      //asr
      // if($div=='asr')
        $event = json_encode(array((object)array("type"=>"setvalue","targetId"=>"mx".$user_nossa->owner,"value"=>"TA HD WITEL KALSEL","requestType"=>"ASYNC","csrftokenholder"=>$csrftokenholder),(object)array("type"=>"setvalue","targetId"=>"mx".$user_nossa->statusx,"value"=>"=BACKEND,=QUEUED,=SLAHOLD","requestType"=>"ASYNC","csrftokenholder"=>$csrftokenholder)));

      //maintenance
      // if($div=='mt')
      //   $event = json_encode(array((object)array("type"=>"setvalue","targetId"=>"mx".$user_nossa->owner,"value"=>"ACCESS MAINTENANCE WITEL KALSEL (BANJARMASIN)","requestType"=>"ASYNC","csrftokenholder"=>$csrftokenholder),(object)array("type"=>"setvalue","targetId"=>"mx".$user_nossa->statusx,"value"=>"=BACKEND,=QUEUED,=SLAHOLD","requestType"=>"ASYNC","csrftokenholder"=>$csrftokenholder)));
      // $event = json_encode(array((object)array("type"=>"setvalue","targetId"=>"mx1048","value"=>"=ACCESS MAINTENANCE WITEL KALSEL (BANJARMASIN)","requestType"=>"ASYNC","csrftokenholder"=>$csrftokenholder),(object)array("type"=>"setvalue","targetId"=>"mx1364","value"=>"=BACKEND,=QUEUED","requestType"=>"ASYNC","csrftokenholder"=>$csrftokenholder),(object)array("type"=>"filterrows","targetId"=>"mx444","value"=>"","requestType"=>"SYNC","csrftokenholder"=>$csrftokenholder)));
      $postdata = http_build_query(
        array(
            "uisessionid" => $uisesid,
            "csrftoken" => $csrftokenholder,
            "currentfocus" => "mx".$user_nossa->owner,
            "scrollleftpos" => "0",
            "scrolltoppos" => "0",
            "requesttype" => "SYNC",
            "responsetype" => "text/xml",
            "events" => $event
        )
      );
      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
      $result = curl_exec($ch);
      //
      $link = "https://nossa.telkom.co.id/maximo/ui/".$input."?_tbldnld=results_showlist&uisessionid=".$uisesid."&csrftoken=".$csrftokenholder;
      // $posisi = strpos($result,'<component vis="true" id="mx310_holder" compid="mx310"')+119;
      // $asd = substr_replace($result, "", 0, $posisi);
      // $asd = substr_replace($asd, "", strpos($asd,'"   noclick="1"'), strlen($asd));
      // echo "test ".($asd)." #";
      // dd($link);

      curl_setopt($ch, CURLOPT_URL, $link);
      $result = curl_exec($ch);
      curl_close($ch);
      // dd($result);

      echo "prepare variable...\n";
      $result=str_replace('&nbsp;', ' ', $result);
      $columns = array(
        "Incident",
        "Customer_Name",
        "Contact_Name",
        "Contact_Phone",
        "Contact_Email",
        "Summary",
        "Owner_Group",
        "Owner",
        "Last_Updated_Work_Log",
        "Last_Work_Log_Date",
        "Count_CustInfo",
        "Last_CustInfo",
        "Assigned_to",
        "Booking_Date",
        "Assigned_by",
        "Reported_Priority",
        "Source",
        "Subsidiary",
        "External_Ticket_ID",
        "External_Ticket_Status",
        "Segment",
        "Channel",
        "Customer_Segment",
        "Closed_By",
        "Customer_ID",
        "Service_ID",
        "Service_No",
        "Service_Type",
        "Top_Priority",
        "SLG",
        "Technology",
        "Datek",
        "RK_Name",
        "Induk_Gamas",
        "Reported_Date",
        "LAPUL",
        "GAUL",
        "TTR_Customer",
        "TTR_Nasional",
        "TTR_Regional",
        "TTR_Witel",
        "TTR_Mitra",
        "TTR_Agent",
        "TTR_Pending",
        "pending_reason",
        "Status",
        "Hasil_Ukur",
        "OSM_Resolved_Code",
        "Last_Update_Ticket",
        "Status_Date",
        "Resolved_By",
        "Workzone",
        "Witel",
        "Regional",
        "Incident_Symptom",
        "Solution_Segment",
        "Actual_Solution",
        "ID");
      //$result = ;
      $dom = @\DOMDocument::loadHTML(trim($result));

      $table = $dom->getElementsByTagName('table')->item(0);
      $rows = $table->getElementsByTagName('tr');
      $result = array();
      for ($i = 2, $count = $rows->length; $i < $count; $i++)
      {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns)-1; $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  str_replace('&nbsp;', '', $td->nodeValue);
            if($j==0)
              $data['ID'] =  substr($td->nodeValue,2);
        }
        $result[] = $data;
      }

      echo "inserting...\n";
      $srcarr=array_chunk($result,500);
      DB::table('data_nossa')->truncate();
      foreach($srcarr as $item) {
          DB::table('data_nossa')->insert($item);
      }
      // dd($result);
      // if($div == 'asr')
      //   DB::table('data_nossa')->where('Owner_Group', 'TA HD WITEL KALSEL')->delete();
      // if($div == 'mt')
      //   DB::table('data_nossa')->where('Owner_Group', 'ACCESS MAINTENANCE WITEL KALSEL (BANJARMASIN)')->delete();
      // DB::table('data_nossa')->insert($result);
    }
    public static function grabAlistaByRfc($rfc){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');
      curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
      $data = DB::table('akun')->where('id', '8')->first();
      curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$data->user."&LoginForm%5Bpassword%5D=".$data->pwd."&yt0=");
      // curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=850056&LoginForm%5Bpassword%5D=KalselHibat188&yt0=");
      $result = curl_exec($ch);
      //dd($result);
      echo"logged in \n";
      $result = array();


      // curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/historypengeluaranproject&Permintaanpengambilanbarang%5Bid_permintaan_ambil%5D=&Permintaanpengambilanbarang%5Bproject_id%5D=&Permintaanpengambilanbarang%5Bnama_gudang%5D=&Permintaanpengambilanbarang%5Btgl_permintaan%5D=&Permintaanpengambilanbarang%5Bnama_requester%5D=&Permintaanpengambilanbarang%5Bpengambil%5D=&Permintaanpengambilanbarang%5Bno_rfc%5D='.$rfc.'&Permintaanpengambilanbarang%5Bnik_pemakai%5D=&Permintaanpengambilanbarang%5Bnama_mitra%5D=&Permintaanpengambilanbarang_page=1');

      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?Permintaanpengambilanbarang%5Bid_permintaan_ambil%5D=&Permintaanpengambilanbarang%5Bproject_id%5D=&Permintaanpengambilanbarang%5Bnama_gudang%5D=&Permintaanpengambilanbarang%5Btgl_permintaan%5D=&Permintaanpengambilanbarang%5Bnama_requester%5D=&Permintaanpengambilanbarang%5Bpengambil%5D=&Permintaanpengambilanbarang%5Bno_rfc%5D='.urlencode($rfc).'&Permintaanpengambilanbarang%5Bnik_pemakai%5D=&Permintaanpengambilanbarang%5Bnama_mitra%5D=&Permintaanpengambilanbarang_page=1&r=gudang%2Fhistorypengeluaranproject');
      $als = curl_exec($ch);
      // dd($als);
      if(!strpos($als, 'No results found.')){
        $columns = array(
          'alista_id',
          'project',
          'nama_gudang',
          'tgl',
          'requester',
          'pengambil',
          'no_rfc',
          'nik_pemakai',
          'mitra',
          'id_permintaan'
        );
        $cals = str_replace('<a class="view" title="Lihat Detail Permintaan" href="javascript:detailpermintaan(&quot;', '', $als);
        $als = str_replace('&quot;)"><img src="images/find.png" alt="Lihat Detail Permintaan" /></a>', '', $cals);
        $dom = @\DOMDocument::loadHTML(trim($als));
        // dd($als);
        echo"convert string to variable \n";
        $table = $dom->getElementsByTagName('table')->item(0);
        $rows = $table->getElementsByTagName('tr');
        for ($i = 2, $count = $rows->length; $i < $count; $i++)
        {
          $cells = $rows->item($i)->getElementsByTagName('td');
          $data = array();
          for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
          {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
          }
          $result[] = $data;
        }
      }
      // dd($result);
      $material = array();
      foreach($result as $noooooo => $r){
        echo "\nambil data ".$noooooo;
        curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/showdetailpermintaanoperation&id='.$r['id_permintaan']);

        $mtr = curl_exec($ch);
        curl_close($ch);
        $columnsx= array(2=>"id_barang", "nama_barang", "jumlah");
        $dom = @\DOMDocument::loadHTML(trim($mtr));
        $table = $dom->getElementsByTagName('table')->item(1);
        $rows = $table->getElementsByTagName('tr');

        for ($i = 1, $count = $rows->length; $i < $count; $i++)
        {
          $cells = $rows->item($i)->getElementsByTagName('td');
          for ($j = 2, $jcount = 4; $j <= $jcount; $j++)
          {
            $td = $cells->item($j);
            if($j==4){
              $data[$columnsx[$j]] =  explode(' ', $td->nodeValue)[0];
            }else{
              $data[$columnsx[$j]] =  $td->nodeValue;
            }
          }
          $r['id_item_bantu'] = $data['id_barang']."_".$r['no_rfc'];
          $material[] = array_merge($data,$r);
          // dd($material);
        }
      }
      // dd($material);
      DB::table('alista_material_keluar')->where('no_rfc', $rfc)->delete();
      DB::table('alista_material_keluar')->insert($material);
  }
  public function grabKpiIoan(){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://access-servo.telkom.co.id:8081/admin/login/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'servo.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=TA9999&password=telkom135");
    $result = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'http://access-servo.telkom.co.id:8081/admin/misc/getquery/api_id/4103/xin/thnbln:'.date("Ym").'~reg:6~witel:44~regnew:~newwitel:~formid:2107~appid:22');
    $result = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($result)->data;
    $insert = array();
    foreach($data as $d){
      $insert[] = json_decode(json_encode($d), true);
    }
    DB::table('kpi_sector_ioan')->insert($insert);
    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "sukses ngambil data kpi servo ".date("Y-m-d H:i:s")
    ]);
  }
  public function grabSaldoUnspec(){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_URL, 'https://access-quality.telkom.co.id/rekap_unspec_sektor/dashboard_semesta/sektor.php?regional=6&witel=KALSEL&tanggal='.date("Y-m-d").'&day=');
    $result = curl_exec($ch);
    curl_close($ch);
    $result = str_replace("</tr><td","<tr><td",$result);
    $result = str_replace("</tr></tr>","</tr><tr>",$result);
    $dom = @\DOMDocument::loadHTML(trim($result));

    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
            1 =>'SEKTOR',
            'TOTAL_SALDO',
            'P1',
            'P2',
            'P3',
            'SALDO_PELANGGAN_H1',
            'SALDO_PELANGGAN_H',
            'SALDO_PELANGGAN_PERBAIKAN',
            'SALDO_WARANTY_H1',
            'SALDO_WARANTY_H',
            'SALDO_WARANTY_PERBAIKAN',
            'TOTAL_PERBAIKAN',
            'TARGET',
            'GAP',
            'PERNAH_CLOSE'
        );
    $result = array();
    for ($i = 2, $count = $rows->length-1; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $data['TGL_UPDATE']=date('Y-m-d H:i:s');
        $result[] = $data;
    }
    DB::table('saldo_unspec_sektor')->insert($result);
    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "sukses ngambil data saldo_unspec_sektor semesta ".date("Y-m-d H:i:s")
    ]);
  }

  public function grabprodunspec(){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_URL, 'https://access-quality.telkom.co.id/rekap_unspec_sektor/dashboard_semesta/monitoring_tiket/bulanan/summary_witelxproman.php?witel=KALSEL');
    $result = curl_exec($ch);
    curl_close($ch);
    $dom = @\DOMDocument::loadHTML(trim($result));

    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
            1 =>'SEKTOR',
            'CLOSE_SEMESTA',
            'CLOSE_PROMAN',
            'CLOSE_CUR_MONTH_SEMESTA',
            'CLOSE_CUR_MONTH_PROMAN',
            'CLOSE_SPEC_SEMESTA',
            'CLOSE_SPEC_PROMAN',
            'CLOSE_MYTECH_SEMESTA',
            'CLOSE_MYTECH_PROMAN',
            'GAUL_UNSPEC',
            'REAL_CLOSE',
            'TARGET',
            'KINERJA_SEMESTA'
        );
    $result = array();
    for ($i = 2, $count = $rows->length-1; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $data['TGL_UPDATE']=date('Y-m-d H:i:s');
        $result[] = $data;
    }
    // dd($result);
    DB::table('saldo_unspec_semesta')->insert($result);
    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "sukses ngambil data saldo_unspec_semesta summary_witelxproman.php ".date("Y-m-d H:i:s")
    ]);
  }
  public static function MaterialAlistaBaDigital($date){
    $ch = curl_init();
    if($date == 'today'){
      $date = date('Y-m-d');
    }
    if(strlen($date)<10){
      echo "parameter salah, harus YYYY-mm-dd";
      Telegram::sendMessage([
        'chat_id' => '-306306083',
        'parse_mode' => 'html',
        'text' => "parameter salah, harus YYYY-mm-dd"
      ]);
    }
    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/report_amalia/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'amalia.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $akun = DB::table('akun')->where('app', 'alista')->first();
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm[username]=".$akun->user."&LoginForm[password]=".$akun->pwd);
    $result = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/report_amalia/index.php?r=report/RekapBaNew&regional=REG%206&witel=KALSEL&date1='.$date.'&date2='.$date.'&material=1');
    $result = curl_exec($ch);
    curl_close($ch);
    $dom = @\DOMDocument::loadHTML(trim($result));

    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
            1 =>'REG',
            'WITEL',
            'NO_SC',
            'NAMA_BARANG',
            'DESIGNATOR',
            'QTY',
            'JENIS_KABEL'
        );
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $data['TGL_UPDATE']=$date;
        $result[] = $data;
    }
    DB::table('MaterialAlistaBaDigital')->where('TGL_UPDATE', 'like', $date."%")->delete();
    DB::table('MaterialAlistaBaDigital')->insert($result);
    // dd($result);

    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "sukses ngambil data MaterialAlistaBaDigital ".$date
    ]);
  }
  private static function generate_string(){
    $input = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $strength=4;
    $input_length = strlen($input);
    $random_string = '';
    for($i = 0; $i < $strength; $i++) {
        $random_character = $input[mt_rand(0, $input_length - 1)];
        $random_string .= $random_character;
    }

    return $random_string;
  }
  public static function loginvalins(){
    $akun = DB::table('akun')->where('user', 18900106)->where('status',1)->where('isExpired', 0)->first();
    if(count($akun)){
      $captcha = self::generate_string();
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.36/valins/config/action.php');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'valins.jar');  //could be empty, but cause problems on some hosts
      curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
      curl_setopt($ch, CURLOPT_POSTFIELDS, "username=".$akun->user."&password=".$akun->pwd."&dropdown_login=sso&captcha=".$captcha."&generate=".$captcha);
      $result = curl_exec($ch);
      curl_close($ch);
    }
  }
  public static function valinsphp($idval, $jumlah_port){
    $result = self::valinsgrab($idval, $jumlah_port);
    // dd($result);
    DB::table('valins')->where('VALINS_ID', $idval)->delete();
    DB::table('valins')->insert($result);
  }
  public static function valinsgrab($idval, $jumlah_port){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'valins.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $url = 'http://10.62.165.36/valins/home.php?page=PG'.date('ymd').'0018&column=port_isi&treg=6&jml_port='.$jumlah_port.'&summary='.$idval.'&column=odp&start=2019-08-01&end='.date('Y-m-d');
    curl_setopt($ch, CURLOPT_URL, $url);
    $result = curl_exec($ch);
    curl_close($ch);
    if(strpos($result, 'config/action.php')){
      self::loginvalins();
    }
    $dom = @\DOMDocument::loadHTML($result);

    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
            0 =>'Regional','Witel','STO','IP_OLT','Slot','Port','ONU_ID','ONU_SN','Version_ONT','Nama_ODP','QR_Code_ODP','Port_ODP','QR_Code_Dropcore','ONU_Status','Offline_at','Online_at','Duration','OLT_RX_LEVEL_OLD','ONU_RX_LEVEL_OLD','OLT_RX_LEVEL_NEW','ONU_RX_LEVEL_NEW','SIP_1','SIP_2','Inet'
        );
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        $data['VALINS_ID']=$idval;
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $data['TGL_UPDATE']=date('Y-m-d H:i:s');
        $result[] = $data;
    }
    return $result;
  }
  public static function getnasionaltactical(){
    $akun = DB::table('akun')->where('user', 17920262)->first();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/tactical/index.php/login/act_login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'tactical.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "user_id=".$akun->user."&password=".$akun->pwd);
    $result = curl_exec($ch);
    $tablewitel="";$headtable = "";
    $param = ["REG 1", "REG 2", "REG 3", "REG 4", "REG 5", "REG 6", "REG 7"];
    $css='<html class="loading" lang="en" data-textdirection="ltr">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
        <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
        <meta name="author" content="ThemeSelect">
        <title>Tactical</title>
        <link rel="apple-touch-icon" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/images/ico/favicon.ico">
        <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
        <!--<link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">-->
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/line-awesome.min.css">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/vendors/css/charts/chartist.css">
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/vendors/css/charts/chartist-plugin-tooltip.css">
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN CHAMELEON  CSS-->
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/app.css">
        <!-- END CHAMELEON  CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/core/colors/palette-gradient.css">
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/core/colors/palette-gradient.css">
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/pages/chat-application.css">
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/pages/dashboard-analytics.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/assets/css/style.css">
        <!-- END Custom CSS-->
      </head>';
      $js='<script src="https://apps.telkomakses.co.id/tactical/assets/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="https://apps.telkomakses.co.id/tactical/assets/app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="https://apps.telkomakses.co.id/tactical/assets/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="https://apps.telkomakses.co.id/tactical/assets/app-assets/vendors/js/charts/chart.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN CHAMELEON  JS-->
    <script src="https://apps.telkomakses.co.id/tactical/assets/app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="https://apps.telkomakses.co.id/tactical/assets/app-assets/js/core/app.js" type="text/javascript"></script>
    <!-- END CHAMELEON  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="https://apps.telkomakses.co.id/tactical/assets/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script><script type="text/javascript">
      $(document).ready(function() {
                  /* Data Tables */
          $(".dt_rekap_witel").DataTable( {
              "columnDefs": [ {
                  "searchable": false,
                  "orderable": false,
                  "targets": 0
              } ],
              "order": [[ 46, "desc" ]],
              "searching": false,
              "paging": false,
              "info": true,

              "drawCallback": function( settings ) {
                  $(".dt_rekap_witel tbody tr").each(function(i){
                    $($(this).find("th")[0]).html(i+1);
                    if($($(this).find("th")[1]).html()==`<font color="#5E66E5">KALSEL</font>` || $($(this).find("th")[1]).html()==`<font color="#5E66E5">BALIKPAPAN</font>`){
                      $(this).css("outline-style", "solid");
                    }
                  });
              }
          } );

        });</script>';
    foreach($param as $p){
      curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/tactical/dashboard/get_kpi_ioan_witel');
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'tactical.jar');  //could be empty, but cause problems on some hosts
      curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "periode=".date('Ym')."&regional=".$p);
      $result = curl_exec($ch);
      if(!$headtable){
        $posa=strpos($result, '<table class="table-sm table-bordered table-responsive-sm text-nowrap table-fixed dt_rekap_witel"');
        $headtable = $css.substr($result, $posa,(strpos($result, 'btnwitel')-151)-$posa);
        $headtable = str_replace("Performansi I-OAN REG 1", "Performansi I-OAN all WITEL", $headtable);
        // dd($headtable);
      }
      $tbl2 = strpos($result, 'btnwitel')-151;
      $result = substr_replace($result, "", 0,$tbl2);
      $result = substr_replace($result, "", strpos($result, '</tbody>'),strlen($result));
      $tablewitel .= $result;
    }
    curl_close($ch);
    echo($headtable.$tablewitel.$js);
  }

  public static function gettacticalhmtl(){
    $akun = DB::table('akun')->where('user', 17920262)->first();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/tactical/index.php/login/act_login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'tactical.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "user_id=".$akun->user."&password=".$akun->pwd);
    $result = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/tactical/dashboard/get_kpi_ioan_witel');
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'tactical.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "periode=".date('Ym')."&regional=REG 6");
    $witel = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/tactical/dashboard/get_kpi_ioan_sektor');
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'tactical.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "periode=".date('Ym')."&witel=KALSEL");
    $sektor = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/tactical/dashboard/get_kpi_ioan_sektor');
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'tactical.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "periode=".date('Ym')."&witel=BALIKPAPAN");
    $sektorbpp = curl_exec($ch);
    // echo $sektor;
    curl_close($ch);

    $myfile = fopen("tactical/SB".date('YmdH').".html", "w") or die("Unable to open file!");
    fwrite($myfile, $sektorbpp);
    fclose($myfile);

    $myfile = fopen("tactical/S".date('YmdH').".html", "w") or die("Unable to open file!");
    fwrite($myfile, $sektor);
    fclose($myfile);
    $myfile = fopen("tactical/W".date('YmdH').".html", "w") or die("Unable to open file!");
    fwrite($myfile, $witel);
    fclose($myfile);
  }
  public static function kpiperbandinganioanwitel(){
    $yesterday = date("Ymd", strtotime( '-1 days' ));
    $today = date("Ymd");
    $file = array($yesterday."10",$yesterday."14",$yesterday."19",$today."10",$today."14",$today."19");
    // dd($file);
    $return = array();
    foreach($file as $f){
      if(file_exists("tactical/W".$f.".html")){
        $h = file_get_contents("tactical/W".$f.".html");
        $h = str_replace("\t", "", $h);
        $h = str_replace("\n", "", $h);
        $h = str_replace('th', "td", $h);
        $h = str_replace('                                                        ', "", $h);
        $dom = @\DOMDocument::loadHTML($h);
        $table = $dom->getElementsByTagName('table')->item(1);
        $rows = $table->getElementsByTagName('tr');
        $columns = array(
                1 =>'WITEL','QT','QR','QA','QNK','SUGART','SUGARR','SUGARA','SUGARNK','HVCT','HVCR','HVCA','HVCNK','TTR3T','TTR3R','TTR3A','TTR3NK','TTR12T','TTR12R','TTR12A','TTR12NK','SALDOUNSPECT','SALDOUNSPECR','SALDOUNSPECA','SALDOUNSPECNK','PRODUNSPECT','PRODUNSPECR','PRODUNSPECA','PRODUNSPECNK','TGBKUANT','TGBKUANR','TGBKUANA','TGBKUANNK','TGBQUALT','TGBQUALR','TGBQUALA','TGBQUALNK','VALINST','VALINSR','VALINSA','VALINSNK','SQMT','SQMR','SQMA','SQMNK','TNK'
            );
        $result = array();
        for ($i = 3, $count = $rows->length-1; $i < $count; $i++)
        {
            $cells = $rows->item($i)->getElementsByTagName('td');
            $data = array();
            for ($j = 1, $jcount = count($columns)+1; $j < $jcount; $j++)
            {
                $td = $cells->item($j);
                $data[$columns[$j]] =  $td->nodeValue;
            }
            $result[] = $data;
        }
        // dd($result);
        $return[$f]=$result;
      }else{
        $return[$f]=null;
      }
    }
    $witel = array("BALIKPAPAN","KALSEL");
    return view("kpi.perbandingan", compact('return', 'witel'));
  }
  public function rekapracingpoin($bln){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://10.128.16.65/comparin_tech/masuk');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'comparin.jar');
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML($result);
    $token = $dom->getElementsByTagName('input')[0]->getAttribute("value");

    curl_setopt($ch, CURLOPT_URL, 'http://10.128.16.65/comparin_tech/signin');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=17920262&password=kamaeltroper&_token=".$token);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'comparin.jar');
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $result = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'http://10.128.16.65/comparin_tech/racingTeknisi?witel=ALL&month='.$bln);
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'comparin.jar');
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $result = curl_exec($ch);
    curl_close($ch);
    $dom = @\DOMDocument::loadHTML($result);
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
            1 =>'ID','NAMA','WITEL','PS','DIG_CHANNEL','NON_DIG_CHANNEL','SOBI','GARANSI_PSB','POIN'
        );
    $result = array();
    for ($i = 2, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        $sto='';
        for ($j = 1, $jcount = count($columns)+1; $j < $jcount; $j++)
        {
            $td = $cells->item($j);

            if($columns[$j]=='WITEL'){
              $witel = explode('/', $td->nodeValue);
              $sto = $witel[1];
              $data[$columns[$j]] = $witel[0];
            }else{
              $data[$columns[$j]] = trim($td->nodeValue);
            }
        }
        $data['STO']=$sto;
        $result[] = $data;
    }
    DB::statement('INSERT INTO racing_point_log SELECT * FROM racing_point;');
    DB::table('racing_point')->delete();
    DB::table('racing_point')->insert($result);

    $data = DB::select('SELECT COUNT(*) as TEKNISI,sum(PS) AS `PS`,sum(DIG_CHANNEL) AS `DIG_CHANNEL`,sum(NON_DIG_CHANNEL) AS `NON_DIG_CHANNEL`,sum(SOBI) AS `SOBI`,sum(GARANSI_PSB) AS `GARANSI_PSB`,sum(POIN) AS `POIN`, `WITEL` FROM `racing_point` GROUP BY `WITEL` ORDER BY `POIN` DESC');
    return view('racing_point',compact('data'));

  }

  public static function grab_valins2()
  {
    $arr = ['15996043'];

    foreach($arr as $v)
    {
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://ops.tomman.app/api/checkValins/'.$v,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
      ));

      $response = curl_exec($curl);
      if (curl_errno($curl)) {
        echo 'Error:' . curl_error($curl);
        curl_close($curl);
      } else {
        curl_close($curl);

        $response = json_decode($response);

        if($response)
        {
          $main_data = $response->message;
          $get_data = substr($main_data, (strpos($main_data, 'port_odp') + 9) );

          $get_time = trim(substr($main_data, (strpos($main_data, 'Time:') + 5) ) );
          $get_time = substr($get_time, 0, 19);

          $get_odp = trim(substr($main_data, (strpos($main_data, 'ODP:') + 5) ) );
          $split_get_odp = explode(' ', $get_odp)[0];
          $split_get_odp = explode('/', $split_get_odp);
          $odp = '';

          if(count($split_get_odp) == 2)
          {
            $odp = $split_get_odp[0] .'/'. sprintf("%03d", (int)$split_get_odp[1]);
          }
          // preg_match_all("/(.*?)Panel/i", $get_odp, $match, PREG_OFFSET_CAPTURE);
          // dd($match, $get_odp);
          $explode_new_line = explode(PHP_EOL, $get_data)[0];
          $explode_new_line2 = explode('ODP Power Checking', $get_data)[0];
          $explode_new_line3 = explode('|', $explode_new_line2);
          $result_new = array_chunk($explode_new_line3, 5);

          unset($result_new[0]);

          foreach($result_new as $k => $vx)
          {
            // $fd[$k]['port_odp']   = $k;
            // $fd[$k]['id_valins']  = $v;
            // $fd[$k]['time_valins'] = $get_time;
            // $fd[$k]['odp']        = $odp;
            // $fd[$k]['onu_id']     = trim($v[0]);
            // $fd[$k]['onu_sn']     = trim($v[1]);
            // $fd[$k]['sip1']       = trim($v[2]);
            // $fd[$k]['sip2']       = trim($v[3]);
            // $fd[$k]['no_hsi1']    = trim($v[4]);

            DB::table('bank_valins')->insert([
              'app'          => 'kosong',
              'id_wo'        => 'kosong',
              'jenis_valins' => 'kosong',
              'port_odp'     => $k,
              'id_valins'    => $v,
              'time_valins'  => $get_time,
              'odp'          => $odp,
              'onu_id'       => trim($vx[0]),
              'onu_sn'       => trim($vx[1]),
              'sip1'         => trim($vx[2]),
              'sip2'         => trim($vx[3]),
              'no_hsi1'      => trim($vx[4]),
              'created_by'   => 'KOSONG'
            ]);

            print_r($v);

            ++$k;
          }
        }
      }
    }
  }

  public static function grab_valins($v)
  {
    $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://ops.tomman.app/api/checkValins/'.$v,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
      ));

      $response = curl_exec($curl);

      if (curl_errno($curl)) {
        echo 'Error:' . curl_error($curl);
        curl_close($curl);
      } else {
        curl_close($curl);

        $response = json_decode($response);

        $fd = [];

        if($response->status)
        {
          $main_data = $response->message;

          $get_data = substr($main_data, (strpos($main_data, 'port_odp') + 9) );

          $get_time = trim(substr($main_data, (strpos($main_data, 'Time:') + 5) ) );
          $get_time = substr($get_time, 0, 19);
          $get_odp = trim(substr($main_data, (strpos($main_data, 'ODP:') + 5) ) );
          $split_get_odp = explode(' ', $get_odp)[0];
          $split_get_odp = explode('/', $split_get_odp);
          $odp = '';

          if(count($split_get_odp) == 2)
          {
            $odp = $split_get_odp[0] .'/'. sprintf("%03d", (int)$split_get_odp[1]);
          }
          // preg_match_all("/(.*?)Panel/i", $get_odp, $match, PREG_OFFSET_CAPTURE);
          // dd($match, $get_odp);
          $explode_new_line = explode(PHP_EOL, $get_data)[0];
          $explode_new_line2 = explode('ODP Power Checking', $explode_new_line)[0];
          $explode_new_line3 = explode('|', $explode_new_line2);
          $result_new = array_chunk($explode_new_line3, 5);

          unset($result_new[0]);

          foreach($result_new as $k => $vx)
          {
            $fd[$k]['port_odp']   = $k;
            $fd[$k]['id_valins']  = $v;
            $fd[$k]['time_valins'] = $get_time;
            $fd[$k]['odp']        = $odp;
            $fd[$k]['onu_id']     = trim($vx[0]);
            $fd[$k]['onu_sn']     = trim($vx[1]);
            $fd[$k]['sip1']       = trim($vx[2]);
            $fd[$k]['sip2']       = trim($vx[3]);
            $fd[$k]['no_hsi1']    = trim($vx[4]);

            ++$k;
          }
        }
        return $fd;
      }
  }
}
