<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
class Commerce
{
    const TABLE = 'maintenance_commerce';

    public static function getById($id)
    {
        return DB::table(self::TABLE)->where('id', $id)->first();
    }
    public static function getByStatus($status)
    {
        return DB::table(self::TABLE)->where('status', $status)->get();
    }
    public static function adminInput($req)
    {
        $id = DB::table(self::TABLE)->insertGetId([
            "bulan" => $req->bulan,
            "judul" => $req->judul,
            "jenis" => $req->jenis,
            "nilai" => $req->nilai,
            "status" => "Inbox Approval"
        ]);
        DB::table("maintenance_commerce_log")->insert([
            "mcl_id" => $id,
            "catatan" => $req->catatan,
            "updater" => session('auth')->id_karyawan,
            "status" => "Inbox Approval"
        ]);
        return $id;
    }
    public static function adminUpdate($id, $req)
    {
        DB::table(self::TABLE)->where('id', $id)->update([
            "bulan" => $req->bulan,
            "judul" => $req->judul,
            "jenis" => $req->jenis,
            "nilai" => $req->nilai,
            "status" => "Inbox Approval"
        ]);
        DB::table("maintenance_commerce_log")->insert([
            "mcl_id" => $id,
            "catatan" => $req->catatan,
            "updater" => session('auth')->id_karyawan,
            "status" => "Inbox Approval"
        ]);
    }
    public static function approvalUpdate($id, $req)
    {
        $status = $req->status=="Approve"?"In-progress Commerce":$req->status=="Reject"?"Inbox Admin":"Inbox Approval";
        DB::table(self::TABLE)->where('id', $id)->update([
            "pid" => $req->pid,
            "approval" => $req->status,
            "status" => $status,
            "catatan" => $req->catatan
        ]);
        DB::table("maintenance_commerce_log")->insert([
            "mcl_id" => $id,
            "catatan" => $req->catatan,
            "updater" => session('auth')->id_karyawan,
            "status" => $status
        ]);
    }
    public static function updateFiles($id,$query)
    {
        DB::table(self::TABLE)->where('id', $id)->update($query);
    }

    public static function submitcutoff($req)
    {
        return DB::transaction(function() use ($req){
            // dd($req->all());
            $data = DB::table('maintenance_mitra As mt')
            ->leftjoin('regu As mr', 'mr.nama_mitra', '=', 'mt.mitra')
            ->leftjoin('maintaince As m', 'm.dispatch_regu_id', '=', 'mr.id_regu')
            ->leftjoin('maintaince_mtr As mm', 'mm.maintaince_id', '=', 'm.id')
            ->leftjoin('khs_maintenance As i', 'mm.id_item', '=', 'i.id_item')
            ->select(
                'mr.nama_mitra',
                DB::raw("SUM(CASE WHEN m.jenis_order IN (1, 3, 4, 8, 9, 17, 20) THEN (qty*jasa_ta) + (qty*material_ta) ELSE 0 END) as qe_akses"),
                DB::RAW("SUM(CASE WHEN m.jenis_order IN (2, 16) THEN (qty*jasa_ta) + (qty*material_ta) ELSE 0 END) as qe_recover"),
                DB::RAW("SUM(CASE WHEN m.jenis_order IN (5) THEN (qty*jasa_ta) + (qty*material_ta) ELSE 0 END) as qe_relok")
             )
            ->where([
                ["mt.isAktif", 1],
                ['isHapus', 0],
                ['status', '=', 'close']
            ])
            ->whereBetween(DB::RAW("(DATE_FORMAT(tgl_selesai, '%Y-%m-%d') )"), [$req->start_date, $req->end_date])
            ->groupBY('mt.id')
            ->get();
            // dd($data, $req->all());

            // $get_commerce_data = DB::table('maintenance_commerce')->where('id', $req->id_comp)->first();

            $id = DB::table('maintenance_cutoff_rekon')->insertGetId([
                'start_date' => $req->start_date,
                'end_date' => $req->end_date,
                'created_by' => session('auth')->id_user,
            ]);

            DB::table('maintaince')->where([
                ['status', '=', 'close'],
                ['isHapus', 0],
                ['jenis_order', '!=', 6]
            ])
            ->whereBetween(DB::RAW("(DATE_FORMAT(tgl_selesai, '%Y-%m-%d') )"), [$req->start_date, $req->end_date])
            ->update([
                'cutoff_id' => $id,
            ]);

            $proc = $comm = [];
            $sum_akses = $sum_recover = $sum_relok = 0;

            foreach($data as $d)
            {
                if($d->qe_akses != 0)
                {
                    $proc[] = [
                        'cutoff_id' => $id,
                        'status' => 'Inbox Approval',
                        'Judul' => 'QE AKSES',
                        'program' => 'QE AKSES',
                        'nilai' => $d->qe_akses,
                        'mitra' => $d->nama_mitra
                    ];
                }

                if($d->qe_recover != 0)
                {
                    $proc[] = [
                        'cutoff_id' => $id,
                        'status' => 'Inbox Approval',
                        'Judul' => 'QE RECOVERY',
                        'program' => 'QE RECOVERY',
                        'nilai' => $d->qe_recover,
                        'mitra' => $d->nama_mitra
                    ];
                }

                if($d->qe_relok != 0)
                {
                    $proc[] = [
                        'cutoff_id' => $id,
                        'status' => 'Inbox Approval',
                        'Judul' => 'RELOKASI UTILITAS',
                        'program' => 'RELOKASI UTILITAS',
                        'nilai' => $d->qe_relok,
                        'mitra' => $d->nama_mitra
                    ];
                }

                $sum_akses += $d->qe_akses;
                $sum_recover += $d->qe_recover;
                $sum_relok += $d->qe_relok;
            }

            if($sum_akses != 0)
            {
                $comm[] = [
                    'cutoff_id' => $id,
                    'status' => 'Inbox Approval',
                    'Judul' => 'QE AKSES',
                    'jenis' => 'QE AKSES',
                    'nilai' => $sum_akses,
                ];
            }

            if($sum_recover != 0)
            {
                $comm[] = [
                    'cutoff_id' => $id,
                    'status' => 'Inbox Approval',
                    'Judul' => 'QE RECOVERY',
                    'jenis' => 'QE RECOVERY',
                    'nilai' => $sum_recover,
                ];
            }

            if($sum_relok != 0)
            {
                $comm[] = [
                    'cutoff_id' => $id,
                    'status' => 'Inbox Approval',
                    'Judul' => 'RELOKASI UTILITAS',
                    'jenis' => 'RELOKASI UTILITAS',
                    'nilai' => $sum_relok,
                ];
            }


            DB::table('maintenance_commerce')->insert($comm);

            $data = DB::table('maintenance_commerce')->where('cutoff_id', $id)->get();

            foreach($data as $d)
            {
                if($d->jenis == 'QE AKSES')
                {
                    $in = [1, 3, 4, 8, 9, 17, 20];
                }
                elseif($d->jenis == 'QE RECOVERY')
                {
                    $in = [2, 16];
                }
                elseif($d->jenis == 'RELOKASI UTILITAS')
                {
                    $in = [5];
                }

                DB::table('maintaince')
                ->where([
                    ['cutoff_id', $id],
                    ['isHapus', 0]
                ])
                ->whereIn('m.jenis_order', $in)
                ->update([
                    'mcommerce_id' => $d->id
                ]);
            }

            DB::table('maintenance_procurement')->insert($proc);

            $data = DB::table('maintenance_procurement')->where('cutoff_id', $id)->get();

            foreach($data as $d)
            {
                if($d->program == 'QE AKSES')
                {
                    $in = [1, 3, 4, 8, 9, 17, 20];
                }
                elseif($d->program == 'QE RECOVERY')
                {
                    $in = [2, 16];
                }
                elseif($d->program == 'RELOKASI UTILITAS')
                {
                    $in = [5];
                }

                DB::table('maintenance_mitra As mt')
                ->leftjoin('regu As mr', 'mr.nama_mitra', '=', 'mt.mitra')
                ->leftjoin('maintaince As m', 'm.dispatch_regu_id', '=', 'mr.id_regu')
                ->where([
                    ['mr.nama_mitra', $d->mitra],
                    ['isHapus', 0],
                    ['cutoff_id', $id],
                ])
                ->whereIn('m.jenis_order', $in)
                ->update([
                    'mpl_id' => $d->id
                ]);
            }
            // dd('try me b');
            Session::flash('alerts',[['type' => 'success',
            'text' => '<strong>Berhasil</strong>Menambah CutOff']]);

            $redirect = [
                'url' => '/commerce/admin/list'
            ];

            return $redirect;
        });
    }

    public static function undo_cutoff($id)
    {
        DB::table('maintenance_commerce')->where('cutoff_id', $id)->delete();
        DB::table('maintenance_cutoff_rekon')->where('id', $id)->delete();
        DB::table('maintenance_procurement')->where('cutoff_id', $id)->delete();

        DB::table('maintaince')->where([
            ['cutoff_id', $id],
            ['isHapus', 0]
        ])->update([
            'mpl_id' => 0,
            'cutoff_id' => 0,
            'mcommerce_id' => 0
        ]);
    }

    public static function data_cutoff()
    {
        return DB::Table('maintenance_cutoff_rekon')
        ->orderBy('tgl_cut_off', 'ASC')
        ->get();
    }
}