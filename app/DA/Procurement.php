<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
class Procurement
{
    const TABLE = 'maintenance_procurement';

    public static function getById($id)
    {
        return DB::table(self::TABLE)->where('id', $id)->first();
    }
    public static function getByStatus($status)
    {
        return DB::table(self::TABLE)->where('status', $status)->get();
    }
    public static function get_mitra(){
        return DB::table('maintenance_mitra')->where('isAktif', '=', 1)->get();
    }
    public static function adminInput($req)
    {
        $id = DB::table(self::TABLE)->insertGetId([
            "mitra"   => $req->mitra,
            "program" => $req->program,
            "bulan"   => $req->bulan,
            "judul"   => $req->judul,
            "npwp"    => $req->npwp,
            "no_sp"   => $req->sp_no,
            "status"  => "Inbox Pembuatan SP"
        ]);

        $sql = DB::table('maintaince As m')
        ->leftjoin('maintenance_jenis_order As mjo', 'm.jenis_order', '=', 'mjo.id')
        ->leftjoin('regu As mr', 'm.dispatch_regu_id', '=', 'mr.id_regu')
        ->leftjoin('maintenance_mitra As mt', 'mr.nama_mitra', '=', 'mt.mitra')
        ->whereNull('mpl_id')
        ->where([
            ['mt.id', $req->mitra],
            ['m.isHapus', 0]
        ]);

        if($req->program == "Insert Tiang"){
            $sql->where('mjo.id', '=', 6)
            ->update([
                'mpl_id' => $id
            ]);
        }else{
            $sql->where('mjo.id', '!=', 6)
            ->update([
                'mpl_id' => $id
            ]);
        }

        $sum = DB::SELECT("SELECT SUM(qty*jasa_ta + (qty*jasa_ta * 0.1)) as harga FROM maintaince_mtr mm
            LEFT JOIN khs_maintenance i ON mm.id_item=i.id_item
            LEFT JOIN maintaince m ON mm.maintaince_id = m.id
            LEFT JOIN regu mr ON m.dispatch_regu_id = mr.id_regu
            LEFT JOIN maintenance_mitra mt ON mr.nama_mitra = mt.mitra
            WHERE m.isHapus = 0 AND m.mpl_id = $id AND mt.id = $req->mitra
            ")[0];

        DB::table(self::TABLE)->where('id', '=', $id)->update([
            'nilai' => $sum->harga
        ]);

        DB::table("maintenance_procurement_log")->insert([
            "mpl_id" => $id,
            "catatan" => $req->catatan,
            "updater" => session('auth')->id_karyawan,
            "status" => "Inbox Pembuatan SP"
        ]);
        return $id;
    }
    public static function adminUpdate($id, $req)
    {
        DB::table(self::TABLE)->where('id', $id)->update([
            "mitra"   => $req->mitra,
            "program" => $req->program,
            "bulan"   => $req->bulan,
            "judul"   => $req->judul,
            "npwp"    => $req->npwp,
            "no_sp"   => $req->sp_no,
            "status"  => "Inbox Pembuatan SP"
        ]);
        DB::table("maintenance_procurement_log")->insert([
            "mpl_id" => $id,
            "catatan" => $req->catatan,
            "updater" => session('auth')->id_karyawan,
            "status" => "Inbox Pembuatan SP"
        ]);
    }

    public static function pembuatanSPUpdate($id, $req)
    {
        $status = "Inbox Pemberkasan";
        DB::table(self::TABLE)->where('id', $id)->update([
            "no_khs" => $req->no_khs,
            "no_sp" => $req->no_sp,
            "toc" => $req->toc,
            "status" => $status
        ]);
        DB::table("maintenance_procurement_log")->insert([
            "mpl_id" => $id,
            "catatan" => $req->catatan,
            "updater" => session('auth')->id_karyawan,
            "status" => $status
        ]);
    }

    public static function pemberkasanUpdate($id, $req)
    {
        $status = "Inbox Verifikasi";
        DB::table(self::TABLE)->where('id', $id)->update([
            "status" => $status
        ]);
        DB::table("maintenance_procurement_log")->insert([
            "mpl_id" => $id,
            "catatan" => $req->catatan,
            "updater" => session('auth')->id_karyawan,
            "status" => $status
        ]);
    }
    public static function verifikasiUpdate($id, $req)
    {
        $status = $req->status=="Diterima"?"Inbox Pengiriman Doc":($req->status=="Ditolak"?"Inbox Pemberkasan":"Inbox Verifikasi");
        // dd($status);
        DB::table(self::TABLE)->where('id', $id)->update([
            "status" => $status,
            "catatan" => $req->catatan
        ]);
        DB::table("maintenance_procurement_log")->insert([
            "mpl_id" => $id,
            "catatan" => $req->catatan,
            "updater" => session('auth')->id_karyawan,
            "status" => $status
        ]);
    }

    public static function pengirimandocUpdate($id, $req)
    {
        $status = "Inbox Finance";
        DB::table(self::TABLE)->where('id', $id)->update([
            "status" => $status,
            "tgl_kirim" => $req->tgl_kirim,
            "no_resi" => $req->no_resi,
            "kurir" => $req->kurir,
            "catatan" => $req->catatan
        ]);
        DB::table("maintenance_procurement_log")->insert([
            "mpl_id" => $id,
            "catatan" => $req->catatan,
            "updater" => session('auth')->id_karyawan,
            "status" => $status
        ]);
    }

    public static function financeUpdate($id, $req)
    {
        $status = $req->status_pembayaran=="Cash&Bank"?"Finish":"Inbox Finance";
        DB::table(self::TABLE)->where('id', $id)->update([
            "status" => $status,
            "posisi_doc" => $req->posisi_doc,
            "status_pembayaran" => $req->no_resi,
            "status_doc" => $req->kurir
        ]);
        DB::table("maintenance_procurement_log")->insert([
            "mpl_id" => $id,
            "catatan" => $req->catatan,
            "updater" => session('auth')->id_karyawan,
            "status" => $status
        ]);
    }
    public static function updateFiles($id, $query)
    {
        DB::table(self::TABLE)->where('id', $id)->update($query);
    }

    public static function get_ajax_hist($dta){
        return DB::table('maintenance_procurement_log')
        ->where('mpl_id', $dta)
        ->orderBy('id', 'DESC')
        ->get();
    }
}