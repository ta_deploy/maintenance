<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
class OrderManual
{
    const TABLE = 'maintenance_order_manual';
    private static function table(){
        return DB::table(self::TABLE);
    }
    public static function list()
    {
        return self::table()->get();
    }
    public static function getById($id)
    {
        return self::table()->where('ID', $id)->get();
    }
    public static function input($sqldata)
    {
        return self::table()->insert($sqldata);
    }
    public static function update($id, $sqldata)
    {
        return self::table()->where('ID', $id)->update($sqldata);
    }
}