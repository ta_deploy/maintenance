<?php

namespace App\DA;

use cURL;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;

Class SSO{
	
	public static function do_login($username,$password){
		
	  $url = 'http://api.telkomakses.co.id/API/sso/auth_sso_post.php';
		$data = 'username=' . $username . '&password=' . $password;	
		$ch = curl_init( $url );
		curl_setopt( $ch, CURLOPT_POST, 1);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt( $ch, CURLOPT_HEADER, 0);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec( $ch );
        curl_close($ch);
		$hasil = json_decode($response, true);	
		return $hasil;	
	} 
	public static function updateStatus($user, $result){
		DB::table('akun')->where('user', $user)->update([
			"status" => $result['status'],
			"isExpired" => $result['exp']
		]);
	}
	public static function getAll(){
		return DB::table('akun')->get();
	}
	public static function setNewPwd($user,$pwd){
		DB::table('akun')->where('user', $user)->update([
			"pwd" 			=> $pwd,
			"isExpired"	=> 0
		]);
	}
	public static function getByStatus($sts){
		return DB::table('akun')->where('status', $sts)->get();
	}
	public static function getByUser($user){
		return DB::table('akun')->where('user', $user)->first();
	}

}