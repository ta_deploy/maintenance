<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
class Mitra
{
    const TABLE = 'maintenance_mitra';

    public static function getById($id)
    {
        return DB::table(self::TABLE)
        ->where('id', $id)->first();
    }
    public static function getAll()
    {
        return DB::table(self::TABLE)->get();
    }
    public static function getAllSelect2()
    {
        return DB::table(self::TABLE)
        ->select('id', 'mitra as text')->where('isAktif', 1)->get();
    }
    public static function insert($query)
    {
        DB::table(self::TABLE)->insert($query);
    }
    public static function update($id, $query)
    {
        DB::table(self::TABLE)->where('id', $id)->update($query);
    }
}