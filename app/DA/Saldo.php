<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
class Saldo
{
    public static function getAll()
    {
        return DB::table('maintenance_saldo_rfc')
            ->get();
    }
    public static function getKeluar($req)
    {
        return DB::table('alista_material_keluar')
            ->where('no_rfc', $req->rfc)->orWhere('alista_id', $req->rfc)
            ->get();
    }
    public static function getKembali($req)
    {
        return DB::table('alista_material_kembali')
            ->where('NO_RFC', $req->rfc)->orWhere('ID_Pengembalian', $req->rfc)
            ->get();
    }
    public static function cekRfc($rfc)
    {
        return DB::table('maintenance_saldo_rfc')
            ->where('rfc', $rfc)->where('action', 1)
            ->first();
    }
    public static function insert($insert)
    {
        return DB::table('maintenance_saldo_rfc')
            ->insert($insert);
    }
    public static function delete($id,$action)
    {
        return DB::table('maintenance_saldo_rfc')
            ->where('id_pemakaian', $id)->where('action', $action)->delete();
    }

    public static function getListMatrik()
    {
        $query = DB::table('alista_material_keluar')
            ->select('project', DB::raw('1 as dev,COUNT(*) AS baris'))
            ->where('tgl', 'like', date('Y-m').'%')
            ->groupBy('project');
        return $query->get();
    }
    public static function getListOutstanding($tl = "ALL")
    {
        $query = DB::table('maintenance_saldo_rfc')
            ->select('*', DB::raw('sum(value) as dev'))
            ->where('action', 1)
            ->whereNotNull('rfc')
            ->whereNull('transaksi')
            ->having('dev', '>', 0)
            ->orderBy('created_at', 'desc')
            ->groupBy('rfc');
        if($tl != 'ALL'){
            $query->where('niktl', $tl);
        }

        return $query->get();
    }
    public static function listbyrfc($rfc)
    {
        $query = DB::table('maintenance_saldo_rfc')
            ->select('*', DB::raw('(select sum(value) from maintenance_saldo_rfc msr where msr.rfc = maintenance_saldo_rfc.rfc and msr.id_item = maintenance_saldo_rfc.id_item and msr.action = 2) as pemakaian')
            , DB::raw('(select sum(value) from maintenance_saldo_rfc msr where msr.rfc = maintenance_saldo_rfc.rfc and msr.id_item = maintenance_saldo_rfc.id_item and msr.action = 3) as kembalian'))
            ->where('action', 1)
            ->where('rfc', $rfc);
        return $query->get();
    }
    public static function getSisaMaterial($nik,$id)
    {
        $query = DB::table('maintenance_saldo_rfc')
            ->select('*', DB::raw('COALESCE((select sum(value) from maintenance_saldo_rfc msr where msr.rfc = maintenance_saldo_rfc.rfc and msr.id_item = maintenance_saldo_rfc.id_item and msr.action = 2)*-1, 0) as tpemakaian')
            , DB::raw('COALESCE((select sum(value) from maintenance_saldo_rfc msr where msr.rfc = maintenance_saldo_rfc.rfc and msr.id_item = maintenance_saldo_rfc.id_item and msr.action = 2 and msr.id_pemakaian = '.$id.')*-1, 0) as pemakaian')
            , DB::raw('COALESCE((select sum(value) from maintenance_saldo_rfc msr where msr.rfc = maintenance_saldo_rfc.rfc and msr.id_item = maintenance_saldo_rfc.id_item and msr.action = 3), 0) as kembalian')
            , DB::raw('COALESCE((select sum(value) from maintenance_saldo_rfc msr where msr.rfc = maintenance_saldo_rfc.rfc and msr.id_item = maintenance_saldo_rfc.id_item), 0) as sisa'))
           
            ->where('action', 1)
            ->where(function ($q) use($id) {
                $q->whereRaw('(select sum(value) from maintenance_saldo_rfc msr where msr.rfc = maintenance_saldo_rfc.rfc and msr.id_item = maintenance_saldo_rfc.id_item)>0');
            })
            ->where(function ($x) use($nik) {
                $x->where('nik1','=', $nik)
                    ->orWhere('nik2', '=', $nik);
            });
        return $query->get();
    }
    public static function getSubmitedMaterial($nik,$id)
    {
        $query = DB::table('maintenance_saldo_rfc')
            ->select('*', DB::raw('COALESCE((select sum(value) from maintenance_saldo_rfc msr where msr.rfc = maintenance_saldo_rfc.rfc and msr.id_item = maintenance_saldo_rfc.id_item and msr.action = 2)*-1, 0) as tpemakaian')
            , DB::raw('COALESCE((select sum(value) from maintenance_saldo_rfc msr where msr.rfc = maintenance_saldo_rfc.rfc and msr.id_item = maintenance_saldo_rfc.id_item and msr.action = 2 and msr.id_pemakaian = '.$id.')*-1, 0) as pemakaian')
            , DB::raw('COALESCE((select sum(value) from maintenance_saldo_rfc msr where msr.rfc = maintenance_saldo_rfc.rfc and msr.id_item = maintenance_saldo_rfc.id_item and msr.action = 3), 0) as kembalian')
            , DB::raw('COALESCE((select sum(value) from maintenance_saldo_rfc msr where msr.rfc = maintenance_saldo_rfc.rfc and msr.id_item = maintenance_saldo_rfc.id_item), 0) as sisa'))
           
            ->where('action', 2)
            ->where('id_pemakaian', $id)
            ->where(function ($x) use($nik) {
                $x->where('nik1','=', $nik)
                    ->orWhere('nik2', '=', $nik);
            });
        return $query->get();
    }
}