<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Validator;

class Nog
{
    public static function list_All($list_data, $tgl)
    {
       $sql =  DB::Table('nog_master As nm')
       ->select('nm.*')
       ->Where('nm.created_at', 'LIKE', $tgl.'%');

       if($list_data)
       {
           $list_data = array_map(function($x){
               return str_replace(' ', '_', $x);
           }, $list_data);

           $sql->whereIn('nm.jenis_order', $list_data);

           if(in_array('NO_STATUS', $list_data) )
           {
                $sql->OrwhereNull('nm.jenis_order');
           }
       }

       return $sql->get();
    }

    public static function all_stat()
    {
       $sql =  DB::Table('nog_master As nm')->GroupBy('jenis_order');

       return $sql->get();
    }

    public static function list_param($sto, $jenis, $list_data, $list_kendala_m, $date = null)
    {
        $sql = '';
        $data = $moc = [];

        switch ($jenis) {
            case 'pt_1':
                $sql .= "tindak_lanjut = 'PT-1' AND status_order IS NULL";
            break;
            case 'pt_2':
                $sql .= "tindak_lanjut = 'PT-2' AND status_order IS NULL";
            break;
            case 'pt_3':
                $sql .= "tindak_lanjut = 'PT-3' AND status_order IS NULL";
            break;
            case 'In_Progress':
                $sql .= "(tindak_lanjut NOT IN ('PT-1', 'PT-2', 'PT-3') OR tindak_lanjut IS NULL)";
            break;
            case 'hd_mt':
                $sql .= "tindak_lanjut = 'PT-1' AND dispatch_regu_id IS NULL AND status_order IS NULL";
            break;
            case 'hd_pt2':
                $sql .= "tindak_lanjut = 'PT-2' AND dispatch_regu_id IS NULL AND status_order IS NULL";
            break;
            case 'progress':
                $sql .= " dispatch_regu_id IS NULL AND status_order IS NULL";
            break;
            case 'kendala':
                $sql .= " (status_order = 'KENDALA' OR mo.status_dalapa = 'Kedala') ";
            break;
            case 'done':
                $sql .= " (status_order = 'DONE' OR mo.status_dalapa = 'Selesai') ";
            break;
            case 'total':
                $sql .= "( (dispatch_regu_id IS NULL OR dispatch_regu_id IS NOT NULL) AND status_order IS NULL)";
            break;
        }

        if($list_data)
        {
            $val = "'". implode("', '", $list_data) . "'";
            $sql .= " AND nm.jenis_order IN ($val)";
        }

        $get_sto = DB::Table('maintenance_datel')->select('sto')->GroupBy('sto')->get();

        $get_sto = array_map(function($x){
            return $x['sto'];
        }, json_decode(json_encode($get_sto), TRUE) );

        if(strcasecmp($sto, 'All') != 0)
        {
            $sto = strtoupper($sto);

            if(in_array($sto, $get_sto) )
            {
                $sql .= " AND sto_nog = '$sto'";
            }
            elseif(strcasecmp($sto, 'NA') == 0)
            {
                $sto_imp = "'". implode("','", $get_sto) ."'";
                $sql .= " AND (sto_nog NOT IN ($sto_imp) OR sto_nog IS NULL)";
            }
        }

        if($date)
        {
            $sql .= " AND nm.created_at LIKE '$date%'";
        }

        // $data = DB::SELECT("SELECT nm.*
        //  FROM nog_master nm
        //  WHERE $sql");

        $master_odpCare = DB::table('master_OdpCare As mo')
        ->leftjoin('maintaince As m', 'mo.maintenance_id', '=', 'm.id')
        ->select('m.*', 'mo.flag_kondisi_alpro', DB::RAW("COALESCE(m.nama_odp, mo.odp) As nama_odp, COALESCE(m.created_at, mo.created_at) As created_at, COALESCE(m.headline, mo.catatan) As headline, COALESCE(m.koordinat, CONCAT(mo.latt, ', ', mo.longt) ) As koordinat, COALESCE(m.sto, SUBSTRING_INDEX(SUBSTRING_INDEX(mo.odp, '-', 2), '-', -1) ) As sto"), DB::RAW("(CASE WHEN mo.status_dalapa IS NOT NULL THEN mo.status_dalapa ELSE m.status END) As status"), 'mo.id As id_mo', 'mo.jenis_kerusakan', 'mo.file_id', 'mo.maintenance_id')
        ->GroupBy('uniq_id')
        ->OrderBy('mo.id', 'DESC');

        if($date)
        {
            $master_odpCare->Where('mo.created_at', 'LIKE', $date.'%');
        }

        if(strcasecmp($sto, 'All') != 0)
        {
            $sto = strtoupper($sto);

            if(in_array($sto, $get_sto) )
            {
                $master_odpCare->Where(DB::RAW("COALESCE(m.sto, SUBSTRING_INDEX(SUBSTRING_INDEX(mo.odp, '-', 2), '-', -1) )"), 'LIKE', '%'. $sto.'%');
            }
            elseif(strcasecmp($sto, 'NA') == 0)
            {
                $master_odpCare->Where(function($join) use($get_sto){
                    $join->WhereNotIn(DB::RAW("COALESCE(m.sto, SUBSTRING_INDEX(SUBSTRING_INDEX(mo.odp, '-', 2), '-', -1) )"), $get_sto)
                    ->OrWhereNull(DB::RAW("COALESCE(m.sto, SUBSTRING_INDEX(SUBSTRING_INDEX(mo.odp, '-', 2), '-', -1) )") );
                });
            }
        }

        switch ($jenis) {
            case 'progress':
                $master_odpCare->Where(function($join){
                    $join->Where('mo.maintenance_id', '!=', 0);
                });
            break;
            case 'kendala':
                $master_odpCare->Where(function($join){
                    $join->where('mo.status_dalapa', 'Kendala')
                    ->OrWhere('m.status', 'like', '%kendala%');
                });
            break;
            case 'done':
                $master_odpCare->Where(function($join){
                    $join->where('mo.status_dalapa', 'Selesai')
                    ->OrWhere('m.status', 'like', '%selesai%');
                });
            break;
            case 'total':
                $master_odpCare->Where(function($join){
                    $join->where('mo.maintenance_id', 0)
                    ->WhereNull('m.status')
                    ->WhereNull('mo.status_dalapa');
                });
            break;
        }

        $master_odpCare = $master_odpCare->get();

        foreach($master_odpCare as $k => $v)
        {
            $jk = json_decode($v->jenis_kerusakan);

            if($list_kendala_m)
            {
                if(count(array_intersect($list_kendala_m, $jk) ) != 0)
                {
                    $moc[$k] = $v;
                    $moc[$k]->tindak_lanjut = 'Maintenance';
                    $moc[$k]->jenis_kerusakan = json_decode($v->jenis_kerusakan);
                }
            }
            else
            {
                $moc[$k] = $v;
                $moc[$k]->tindak_lanjut = 'Maintenance';
                $moc[$k]->jenis_kerusakan = json_decode($v->jenis_kerusakan);
            }
        }

        foreach($moc as $k => $v)
        {
            if(strcasecmp($sto, 'All') != 0)
            {
                if($sto == $v->sto || strcasecmp($sto, 'NA') == 0)
                {
                    $data[] = $v;
                }
            }
            else
            {
                $data[] = $v;
            }
        }

        return $data;
    }

    public static function list_kondisi_alpro($sto, $jenis, $date = null)
    {
        $get_sto = DB::Table('maintenance_datel')->select('sto')->GroupBy('sto')->get();

        $get_sto = array_map(function($x){
            return $x['sto'];
        }, json_decode(json_encode($get_sto), TRUE) );

        $master_odpCare = DB::table('master_OdpCare As mo')
        ->leftjoin('maintaince As m', 'mo.maintenance_id', '=', 'm.id')
        ->select('m.*', 'mo.flag_kondisi_alpro', DB::RAW("COALESCE(m.nama_odp, mo.odp) As nama_odp, COALESCE(m.created_at, mo.created_at) As created_at, COALESCE(m.headline, mo.catatan) As headline, COALESCE(m.koordinat, CONCAT(mo.latt, ', ', mo.longt) ) As koordinat, COALESCE(m.sto, SUBSTRING_INDEX(SUBSTRING_INDEX(mo.odp, '-', 2), '-', -1) ) As sto"), 'mo.id As id_mo', 'mo.jenis_kerusakan', 'mo.file_id', 'mo.maintenance_id')
        ->GroupBy('uniq_id')
        ->OrderBy('mo.id', 'DESC');

        if($date)
        {
            $master_odpCare->Where('mo.created_at', 'LIKE', $date.'%');
        }

        if(strcasecmp($sto, 'All') != 0)
        {
            $sto = strtoupper($sto);

            if(in_array($sto, $get_sto) )
            {
                $master_odpCare->Where(DB::RAW("COALESCE(m.sto, SUBSTRING_INDEX(SUBSTRING_INDEX(mo.odp, '-', 2), '-', -1) )"), 'LIKE', '%'. $sto.'%');
            }
            elseif(strcasecmp($sto, 'NA') == 0)
            {
                $master_odpCare->Where(function($join) use($get_sto){
                    $join->WhereNotIn(DB::RAW("COALESCE(m.sto, SUBSTRING_INDEX(SUBSTRING_INDEX(mo.odp, '-', 2), '-', -1) )"), $get_sto)
                    ->OrWhereNull(DB::RAW("COALESCE(m.sto, SUBSTRING_INDEX(SUBSTRING_INDEX(mo.odp, '-', 2), '-', -1) )") );
                });
            }
        }

        $master_odpCare = $master_odpCare->get();


        foreach($master_odpCare as $k => $v)
        {
            $jk = json_decode($v->jenis_kerusakan);

            if($jenis && strcasecmp($jenis, 'All') != 0)
            {
                if(in_array($jenis, $jk) )
                {
                    $moc[$k] = $v;
                    $moc[$k]->tindak_lanjut = 'Maintenance';
                    $moc[$k]->jenis_kerusakan = json_decode($v->jenis_kerusakan);
                }
            }
            else
            {
                $moc[$k] = $v;
                $moc[$k]->tindak_lanjut = 'Maintenance';
                $moc[$k]->jenis_kerusakan = json_decode($v->jenis_kerusakan);
            }
        }

        foreach($moc as $k => $v)
        {
            if(strcasecmp($sto, 'All') != 0)
            {
                if($sto == $v->sto || strcasecmp($sto, 'NA') == 0)
                {
                    $data[] = $v;
                }
            }
            else
            {
                $data[] = $v;
            }
        }

        return $data;
    }

    public static function get_id($id)
    {
        return DB::table('nog_master')
        ->where('id', $id)
        ->first();
    }

    public static function update_or_ins_nog($req, $id)
    {
        $auth = session('auth');
        $rules = array(
            'tindak_lanjut' => 'required',
        );

        $messages = [
            'tindak_lanjut.required' => 'Silahkan isi kolom "Tindak Lanjut" Bang!',
        ];

        $validator = Validator::make($req->all(), $rules, $messages);

        if ($validator->fails() ) {
            return redirect()->back()->withInput($req->all() )->withErrors($validator)->with('alerts', [
                ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan order']
            ]);
        }

        if($id == 0)
        {
            $check = DB::table('nog_master')->where('nama_odp', trim($req->nama_odp) )->first();

            if($check)
            {
                if ($validator->fails() ) {
                    return redirect()->back()->withInput($req->all() )->withErrors($validator)->with('alerts', [
                        ['type' => 'danger', 'text' => '<strong>GAGAL</strong> ODP ' . $req->nama_odp . ' Pernah Diiinput!']
                    ]);
                }
            }
            else
            {
                DB::table('nog_master')->insert([
                    'nama_odp'      => $req->nama_odp,
                    'jml_port'      => $req->jml_port,
                    'koordinat_odp' => $req->odp_koor,
                    'jenis_order'   => 'INPUT_MANUAL',
                    'tindak_lanjut' => $req->tindak_lanjut,
                    'status_order'  => $req->status_order,
                    'jenis_order'   => $req->source,
                    'note'          => $req->note,
                    'sto_nog'       => $req->sto,
                    'created_by'    => $auth->id_karyawan
                ]);
            }
        }
        else
        {
            $check = DB::table('nog_master')->where([
                ['nama_odp', trim($req->nama_odp)],
                ['id', '!=', $id]
            ])->first();

            if($check)
            {
                if ($validator->fails() ) {
                    return redirect()->back()->withInput($req->all() )->withErrors($validator)->with('alerts', [
                        ['type' => 'danger', 'text' => '<strong>GAGAL</strong> ODP ' . $req->nama_odp . ' Pernah Diiinput!']
                    ]);
                }
            }
            else
            {
                DB::table('nog_master')->where('id', $id)->update([
                    'tindak_lanjut' => $req->tindak_lanjut,
                    'status_order'  => $req->status_order,
                    'jenis_order'   => $req->source,
                    'note'          => $req->note,
                    'updated_by'    => $auth->id_karyawan
                ]);
            }
        }

        return redirect('/list_saldo/nog/'.date('Y-m') )
        ->with('alerts', [
            ['type' => 'success', 'text' => '<strong>BERHASIL</strong> NOG Berhasil Diupdate!!']
        ]);
    }


    public static function get_workdate($req, $d)
    {
        $sql = 'm.isHapus = 0';

        if(!empty($req->sektor) )
        {
            $sql .= " AND nama_sector = '$req->sektor'";
        }

        if(!empty($req->mitra) )
        {
            $sql .= " AND nama_mitra = '$req->mitra'";
        }

        if(!empty($req->tgl) )
        {
            $sql .= " AND timeplan LIKE '$req->tgl%'";
        }
        else
        {
            $sql .= " AND timeplan LIKE '$d%'";
        }

        $data_nog = $data_alpro = [];

        // $data_nog = DB::SELECT("SELECT m.*
        // FROM nog_master nm
        // LEFT JOIn maintaince m On m.nog_order_id = nm.id
        // WHERE $sql AND m.id IS NOT NULL AND m.timeplan IS NOT NULL AND timeplan != '0000-00-00' AND jenis_order = 17");

        $data_alpro = DB::Select("SELECT m.*,
        (select u_nama As nama from user where id_karyawan = m.username1 GROUP BY id_karyawan) as nama1,
        (select u_nama As nama from user where id_karyawan = m.username2 GROUP BY id_karyawan) as nama2
        FROM
        maintaince m
        LEFT JOIN master_OdpCare mo ON m.id = mo.maintenance_id
        WHERE
        $sql AND DATE_FORMAT(timeplan, '%Y-%m') = '$d' AND dispatch_regu_name IS NOT NULL AND (timeplan IS NOT NULL OR timeplan != '0000-00-00')
        ORDER BY timeplan ASC");

        return array_merge($data_alpro, $data_nog);
    }

    public static function get_active_user($req)
    {
        // $sql = 'm.isHapus = 0';
        $sql = '1 = 1';

        // if(!empty($req->sektor) )
        // {
        //     $sql .= " AND nama_sector = '$req->sektor'";
        // }

        // if(!empty($req->mitra) )
        // {
        //     $sql .= " AND nama_mitra = '$req->mitra'";
        // }

        // if(!empty($req->tgl) )
        // {
        //     $sql .= " AND DATE(timeplan) LIKE '$req->tgl%'";
        // }
        // else
        // {
        //     $sql .= " AND DATE(timeplan) LIKE '$d%'";
        // }

        // if(!empty($req->sektor) )
        // {
        //     $sql .= " AND nama_sector = '$req->sektor'";
        // }

        // if(!empty($req->mitra) )
        // {
        //     $sql .= " AND nama_mitra = '$req->mitra'";
        // }

        if(!empty($req->tgl) )
        {
            $sql .= " AND DATE(mo.created_at) LIKE '$req->tgl%'";
        }
        else
        {
            $d = date('Y-m');
            $sql .= " AND DATE(mo.created_at) LIKE '$d%'";
        }

        if(!empty($req->sto) )
        {
            $sql .= " AND odp LIKE '%-$req->sto-%'";
        }

        // return DB::Select("SELECT mo.chat_id, emp.nama As nama_pelapor, emp.nik As nik_pelapor, COUNT(*) As jumlah, m.*,
        // (select u_nama As nama from user where id_karyawan = m.username1 GROUP BY id_karyawan) as nama1,
        // (select u_nama As nama from user where id_karyawan = m.username2 GROUP BY id_karyawan) as nama2
        // FROM
        // master_OdpCare mo
        // LEFT JOIN maintaince m ON m.id = mo.maintenance_id
        // LEFT JOIN 1_2_employee emp ON emp.chat_id = mo.chat_id
        // WHERE
        // $sql AND DATE_FORMAT(timeplan, '%Y-%m') = '$d' AND dispatch_regu_name IS NOT NULL AND (timeplan IS NOT NULL OR timeplan != '0000-00-00') GROUP BY sto, nama_sector, nama_pelapor
        // ORDER BY timeplan ASC");

        return DB::Select("SELECT mo.chat_id, emp.nama As nama_pelapor, emp.nik As nik_pelapor, emp.Witel_New As witel, COUNT(*) As jumlah
        FROM
        master_OdpCare mo
        LEFT JOIN 1_2_employee emp ON emp.chat_id = mo.chat_id
        WHERE
        $sql GROUP BY nama_pelapor
        ORDER BY created_at ASC");
    }

    public static function detail_list_laporan_active($id, $tgl)
    {
        return DB::Table('master_OdpCare As mo')
        ->select('mo.*', 'mo.id As id_mo', 'mo.odp As nama_odp')
        ->Where([
            ['chat_id', $id],
            ['created_at', 'LIKE', $tgl.'%']
        ])->get();
    }
}