<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
class STO
{
    public static function getById($id)
    {
        return DB::table('maintenance_datel')
            ->where('id', $id)->first();
    }
    public static function getAllSTO()
    {
        return DB::table('maintenance_datel')->select('*')->get();
    }
    public static function insert($query)
    {
        return DB::table('maintenance_datel')->insert($query);
    }
    public static function update($id,$query)
    {
        return DB::table('maintenance_datel')->where('id', $id)->update($query);
    }
}