<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
class Repair
{
    const TABLE = 'maintenance_repair';
    const HIST = 'maintenance_repair_hist';
    public static function getAll()
    {
        return DB::table(self::TABLE)->get();
    }
    public static function getById($id)
    {
        return DB::table(self::TABLE)->where('id', $id)->first();
    }
    public static function isOrdered($id)
    {
        return DB::table('maintaince')->where([
            ['repair_id', $id],
            ['isHapus', 0]
        ])->first();
    }
    public static function getLogs($id)
    {
        return DB::table(self::HIST)->where('repair_id', $id)->get();
    }
    public static function getByStatus($sts)
    {
        return DB::table(self::TABLE)->where('status', $sts)->get();
    }

    public static function getMatrik()
    {
        //registered,disetujui,ditolak,in-tech,selesai,pending
        //query slow memakai sub select dan beberapa table
//         return DB::select('select *,
// (select count(*) from maintenance_repair where maintenance_repair.status="registered" and jenis_alpro=mja.id) as registered,
// (select count(*) from maintenance_repair where maintenance_repair.status="disetujui" and jenis_alpro=mja.id) as disetujui,
// (select count(*) from maintenance_repair where maintenance_repair.status="ditolak" and jenis_alpro=mja.id) as ditolak,
// (select count(*) from maintenance_repair where maintenance_repair.status="in-tech" and jenis_alpro=mja.id) as intech,
// (select count(*) from maintenance_repair where maintenance_repair.status="selesai" and jenis_alpro=mja.id) as selesai,
// (select count(*) from maintenance_repair where maintenance_repair.status="pending" and jenis_alpro=mja.id) as pending from maintenance_jenis_alpro mja where 1');

        //sum case faster query
        return DB::select('SELECT mja.id,mja.jenis_alpro,
sum(case when mr.status="registered" AND mr.jenis_alpro=mja.id THEN 1 ELSE 0 END) as registered,
sum(case when mr.status="disetujui" AND mr.jenis_alpro=mja.id THEN 1 ELSE 0 END) as disetujui,
sum(case when mr.status="ditolak" AND mr.jenis_alpro=mja.id THEN 1 ELSE 0 END) as ditolak,
sum(case when mr.status="in-tech" AND mr.jenis_alpro=mja.id THEN 1 ELSE 0 END) as intech,
sum(case when mr.status="selesai" AND mr.jenis_alpro=mja.id THEN 1 ELSE 0 END) as selesai,
sum(case when mr.status="pending" AND mr.jenis_alpro=mja.id THEN 1 ELSE 0 END) as pending
 FROM maintenance_repair mr left join maintenance_jenis_alpro mja on mr.jenis_alpro=mja.id GROUP BY mr.jenis_alpro ORDER BY mja.id');

    }
    public static function getMatrikList($jo, $sts)
    {
        return DB::table(self::TABLE)->where('jenis_alpro', $jo)->where('status', $sts)->get();
    }
    public static function register($req)
    {
        return DB::table(self::TABLE)->insertGetId(array_merge($req,["status"=>"registered"]));
    }
    public static function registerUpdate($id,$req)
    {
        return DB::table(self::TABLE)->where('id', $id)->update(array_merge($req,["status"=>"registered"]));
    }

    public static function registerByTeknisi($req)
    {
        return DB::table(self::TABLE)->insert(array_merge($req,["status"=>"registered_by_teknisi"]));
    }

    public static function updateUsulan($id,$usulan)
    {
        DB::table(self::TABLE)->where('id', $id)->update($usulan);
    }

    public static function insertLog($insert)
    {
        DB::table(self::HIST)->insertGetId($insert);
    }
}