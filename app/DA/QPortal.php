<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
class QPortal
{
    public static function getAll()
    {
        return DB::table('QPortal')->orderBy('id', 'desc')->get();
    }
    public static function getById($id)
    {
        return DB::table('QPortal')->where('id', $id)->first();
    }
    public static function save($id, $jawaban)
    {
        DB::table('QPortal')->where('id', $id)->update(["jawaban"=>$jawaban]);
    }
}