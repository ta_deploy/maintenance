<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
class ODC
{
    const TABLE = 'maintenance_sector_alpro';
    public static function query(){
        return DB::table(self::TABLE)
        ->select('maintenance_sector_alpro.*', 'maintenance_sector.sector as nama_sektor')
        ->leftJoin('maintenance_sector', 'maintenance_sector_alpro.sector_id', '=', 'maintenance_sector.id')->orderBy('maintenance_sector_alpro.id', 'desc');
    }
    public static function getById($id)
    {
        return DB::table(self::TABLE)
        ->where('id', $id)->first();
    }
    public static function getAll()
    {
        return self::query()->get();
    }
    public static function getAllSelect2()
    {
        return DB::table(self::TABLE)
        ->select('id', 'alpro as text')->get();
    }
    public static function insert($query)
    {
        DB::table(self::TABLE)->insert($query);
    }
    public static function update($id, $query)
    {
        DB::table(self::TABLE)->where('id', $id)->update($query);
    }
}