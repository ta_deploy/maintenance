<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
class User
{
    const TABLE = 'user';
    // const KARYAWAN = 'karyawan';
    // const PROFILE = 'manci_profile';

    public static function userExists($id)
    {
        return DB::table(self::TABLE)
        ->select(self::TABLE.'.*', self::TABLE.".u_nama As nama", self::TABLE.".u_nama_instansi As nama_instansi")
        ->where('id_user', $id)->first();
    }
    public static function absen($id)
    {
        // DB::table(self::KARYAWAN)
        // ->where('id_karyawan', $id)
        // ->update([
        //     'mt_absen' => DB::raw('now()')
        // ]);
        DB::table(self::TABLE)
        ->where('id_karyawan', $id)
        ->update([
            'u_mt_absen' => DB::raw('now()')
        ]);
        DB::table('maintenance_absen_log')->insert([
            'id_user' => $id,
            'status' => 1,
            'created_by' => $id,
            'created_at' => DB::raw('now()')
        ]);
    }
    public static function verifAbsen($id, $verif_by)
    {
        DB::table(self::TABLE)->where('id_user', $id)->update([
            'u_mt_verif_absen' => DB::raw('now()')
        ]);
        DB::table('maintenance_absen_log')->insert([
            'id_user' => $id,
            'status' => 2,
            'created_by' => $verif_by,
            'created_at' => DB::raw('now()')
        ]);
    }
    public static function getUser()
    {
        return DB::table(self::TABLE)
        ->select(self::TABLE.'.*', self::TABLE.".u_nama As nama", self::TABLE.".u_nama_instansi As nama_instansi")
        ->whereNotNull('id_user')->get();
    }

    public static function getUserByAproval($id = 0)
    {
        return DB::table(self::TABLE)
        ->select(self::TABLE.'.*', self::TABLE.".u_nama As nama", self::TABLE.".u_nama_instansi As nama_instansi")
        ->whereNotNull('id_user')->where('approval', $id)->get();
    }

    // public static function getRecordedUser()
    // {
    //     return DB::table(self::PROFILE)
    //     ->select(self::PROFILE.'.*', self::KARYAWAN.".nama")
    //     ->leftJoin(self::KARYAWAN, self::KARYAWAN.'.id_karyawan', '=', self::PROFILE.'.nik')
    //     ->get();
    // }
    // public static function getProfile($id)
    // {
    //     return DB::table('manci_profile')->where('nik', $id)->first();
    // }
    // public static function updateProfile($req, array $input)
    // {
    //     return DB::table('manci_profile')->where('nik', $req->nik)
    //     ->update($input);
    // }
    // public static function updateProfileByNik($nik, array $input)
    // {
    //     return DB::table('manci_profile')->where('nik', $nik)
    //     ->update($input);
    // }
    public static function updateProfileByNik($nik, array $input)
    {
        return DB::table('user')->where('id_user', $nik)
        ->update($input);
    }
    public static function getByRememberToken($token)
    {
        return DB::table(self::TABLE)
        ->where('psb_remember_token', $token)->first();
    }
    // public static function storeProfile(array $input)
    // {
    //     return DB::table('manci_profile')->insert($input);
    // }
    public static function getUserById($id)
    {
        return DB::table(self::TABLE)
        ->select(self::TABLE.'.*', self::TABLE.".u_nama As nama", self::TABLE.".u_nama_instansi As nama_instansi", self::TABLE.".u_no_ktp As no_ktp", self::TABLE.".u_no_gsm1 As no_gsm1", self::TABLE.".u_alamat As alamat")
        ->where('id_user', $id)->first();
    }

    public static function userCreateNew($req)
    {
        DB::table(self::TABLE)->insert([
            "id_user" => $req->id_user,
            "password" => MD5($req->pwd),
            "id_karyawan" => $req->id_user,
            "maintenance_level" => $req->maintenance_level
        ]);
    }
    public static function userCreate($req)
    {
        DB::table(self::TABLE)->insert([
            "id_user" => $req->id_user,
            "password" => MD5($req->pwd),
            "id_karyawan" => $req->id_user,
            "status" => $req->status,
            "maintenance_level" => $req->maintenance_level,
            "jenis_verif" => $req->jenis_verif
        ]);
    }
    public static function userUpdate($req)
    {
        if($req->pwd){
            DB::table(self::TABLE)
            ->where("id_user" , $req->hid)
            ->update([
                "password" => MD5($req->pwd)
            ]);
        }
        DB::table(self::TABLE)
        ->where("id_user" , $req->hid)
        ->update([
            "id_user" => $req->id_user,
            "id_karyawan" => $req->id_user,
            "status" => $req->status,
            "maintenance_level" => $req->maintenance_level,
            "jenis_verif" => $req->jenis_verif,
            "approval" => $req->approval
        ]);
    }
    public static function karyawanExists($id)
    {
        return DB::table(self::TABLE)->where('id_karyawan', $id)->first();
    }
    // public static function karyawanCreate($req)
    // {
    //     DB::table(self::KARYAWAN)->insert([
    //         "id_karyawan" => $req->id_user,
    //         "nama" => $req->nama,
    //         "nama_instansi" => $req->nama_instansi,
    //         "status_kerja" => "MT",
    //         'no_ktp' => $req->ktp,
    //         'no_gsm1' => $req->no_hp,
    //         'alamat' => $req->alamat
    //     ]);
    // }
    public static function karyawanUpdate($req)
    {
        DB::table(self::TABLE)
        ->where("id_karyawan", $req->hid)
        ->update([
            "id_karyawan"     => $req->id_user,
            "u_nama"          => $req->nama,
            "u_nama_instansi" => $req->nama_instansi,
            'u_no_ktp'        => $req->ktp,
            'u_no_gsm1'       => $req->no_hp,
            'u_alamat'        => $req->alamat
        ]);
    }

    //regu
    public static function getRegu()
    {
        return DB::SELECT("SELECT r.*,
            (SELECT CONCAT(u_nama, ' (', r.nik1, ')') FROM user WHERE id_user = nik1 GROUP BY id_karyawan)as nik_1,
            (SELECT CONCAT(u_nama, ' (', r.nik2, ')') FROM user WHERE id_user = nik2 GROUP BY id_karyawan)as nik_2,
            (SELECT CONCAT(u_nama, ' (', r.TL, ')') FROM user WHERE id_user = TL GROUP BY id_karyawan)as T_L
            FROM regu r WHERE r.jenis_regu = 'MARINA'");
    }

    public static function getRegu_ajx($data, $jenis)
    {
        $search_unique = DB::table('regu')->where([
            ['jenis_regu', '=', 'MARINA'],
            ['ACTIVE', '=', 1]
        ])
        ->where(function($join) use($data)
        {
            $join->where('nik1', 'LIKE', '%'.$data.'%')
            ->orWhere('nik2', 'LIKE', '%'.$data.'%');
        }
        )->first();

        $available_nik = $used_nik = '';
        if($jenis == 'teknisi')
        {
            if(!$search_unique)
            {
                $available_nik = DB::table(self::TABLE)
                ->select(self::TABLE.'.*', self::TABLE.".u_nama As nama", self::TABLE.".u_nama_instansi As nama_instansi", self::TABLE.".u_no_ktp As no_ktp", self::TABLE.".u_no_gsm1 As no_gsm1", self::TABLE.".u_alamat As alamat")
                ->where('id_user', 'LIKE', '%'.$data.'%')->get();
            }
            else
            {
                $used_nik = $search_unique;
            }
        }
        elseif($jenis == 'tl')
        {
            $available_nik = DB::table(self::TABLE)
            ->select(self::TABLE.'.*', self::TABLE.".u_nama As nama", self::TABLE.".u_nama_instansi As nama_instansi", self::TABLE.".u_no_ktp As no_ktp", self::TABLE.".u_no_gsm1 As no_gsm1", self::TABLE.".u_alamat As alamat")
            ->where('id_user', 'LIKE', '%'.$data.'%')->get();
        }

        return ['nik_ada' => $available_nik, 'dipakai' => $used_nik];
    }

    public static function getReguById($id)
    {
        return DB::table('regu As r')
        ->select('r.*', DB::RAW(" (SELECT CONCAT(u_nama, ' (', r.nik1, ')') FROM user WHERE id_user = nik1 GROUP BY id_karyawan)as nik_1,
            (SELECT CONCAT(u_nama, ' (', r.nik2, ')') FROM user WHERE id_user = nik2 GROUP BY id_karyawan)as nik_2,
            (SELECT CONCAT(u_nama, ' (', r.TL, ')') FROM user WHERE id_user = TL GROUP BY id_karyawan)as T_L"))
        ->where('id_regu', $id)->first();
    }
    public static function reguCreate($req,$label_regu)
    {
        $nama1=$nama2=null;
        if($req->nik1){
            $nama1 = self::userExists($req->nik1)->nama;
        }
        if($req->nik2){
            $nama2 = self::userExists($req->nik2)->nama;
        }
        DB::table('regu')->insert([
            "uraian"            => $req->nama_regu,
            "label_regu"        => $label_regu,
            "label_sektor"      => $req->label_sektor,
            "nama_mitra"        => $req->mitra,
            "nomor_urut"        => $req->nomor_urut,
            "spesialis"         => $req->spesialis,
            "nik1"              => $req->nik1,
            "nama1"             => $nama1,
            "nik2"              => $req->nik2,
            "nama2"             => $nama2,
            "TL"                => $req->niktl,
            "mainsector_marina" => $req->chat_id,
            "ACTIVE"            => $req->status_regu,
            "pekerjaan"         => $req->kerjaan,
            "jenis_regu"        => "MARINA"
        ]);
    }
    public static function reguUpdate($req,$label_regu)
    {
        $nama1=$nama2=null;
        if($req->nik1){
            $nama1 = self::userExists($req->nik1)->nama;
        }

        if($req->nik2){
            $nama2 = @self::userExists($req->nik2)->nama;
        }

        DB::table('regu')
        ->where("id_regu" , $req->hid)
        ->update([
            "uraian"            => $req->nama_regu,
            "label_regu"        => $label_regu,
            "label_sektor"      => $req->label_sektor,
            "nama_mitra"        => $req->mitra,
            "nomor_urut"        => $req->nomor_urut,
            "spesialis"         => $req->spesialis,
            "nik1"              => $req->nik1,
            "nama1"             => $nama1,
            "nik2"              => $req->nik2,
            "nama2"             => $nama2,
            "TL"                => $req->niktl,
            "mainsector_marina" => $req->chat_id,
            "ACTIVE"            => $req->status_regu,
            "pekerjaan"         => $req->kerjaan,
            "jenis_regu"        => "MARINA"
        ]);

        DB::table('maintaince')
        ->where(function($join){
            $join->WhereNull('status')
            ->OrWhere(DB::RAW("DATE_FORMAT(tgl_selesai, '%Y-%m%-%d')"), DB::RAW("DATE_FORMAT(NOW(), '%Y-%m%-%d')") );
        })
        ->where([
            ['isHapus', 0],
            ['dispatch_regu_id', $req->hid]
        ])
        ->update([
            'dispatch_regu_name' => $req->nama_regu
        ]);
    }
    public static function getMitraByNama($nama)
    {
        return DB::table('maintenance_mitra')
        ->where("mitra" , $nama)
        ->first();
    }

    public static function getRegu_download($id)
    {
        return DB::SELECT("SELECT r.*,
            (SELECT CONCAT(u_nama, ' (', r.nik1, ')') FROM user WHERE id_user = nik1 GROUP BY id_karyawan)as nik_1,
            (SELECT CONCAT(u_nama, ' (', r.nik2, ')') FROM user WHERE id_user = nik2 GROUP BY id_karyawan)as nik_2,
            (SELECT CONCAT(u_nama, ' (', r.TL, ')') FROM user WHERE id_user = TL GROUP BY id_karyawan)as T_L
            FROM regu r WHERE r.jenis_regu = 'MARINA' AND ACTIVE = $id");
    }

    public static function getUser_download($id)
    {
        return DB::table(self::TABLE)
        ->select(self::TABLE.'.*', self::TABLE.".u_nama As nama", self::TABLE.".u_nama_instansi As nama_instansi")
        ->whereNotNull('id_user')
        ->where(self::TABLE.'.status', $id)
        ->get();
    }
}