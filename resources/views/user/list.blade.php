@extends('layout2')

@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>List User</span>
</h1>
@endsection
@section('content')
    <?php $authLevel = Session::get('auth')->maintenance_level ?>

    @if ($authLevel == '1')
        <a href="/user/create" class="btn btn-info" style="margin: 0 0 15px;">
            <span class="glyphicon glyphicon-plus"></span>
            <span>Create User</span>
        </a>
    @endif
    <a href="/download/user_status/0" class="btn btn-danger jns_hrf" style="margin: 0 0 15px;">
      <span class="glyphicon glyphicon-plus"></span>
      <span>Download User <span id="jenis">Aktif</span></span>
  </a>
    <div class="panel">
      <div class="panel-body">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#tabs-aktif" data-toggle="tab" data-aktif = '0'>
              Aktif
            </a>
          </li>
          <li>
            <a href="#tabs-suspend" data-toggle="tab" data-aktif = '1'>
              Tidak Aktif
            </a>
          </li>
        </ul>

        <div class="tab-content tab-content-bordered">
          <div class="tab-pane fade in active" id="tabs-aktif">
            @if (isset($list))
                <?php $url = '/user/' ?>
                @php
                  $lvl = [
                  0 => 'USER',
                  'ADMIN',
                  'TL',
                  'SM',
                  'TELKOM',
                  'MITRA LOCAL',
                  'ADMIN MITRA',
                  'HD'
                  ];
                @endphp
                <div class="row">
                    @foreach($list as $item)
                        @if(!$item->status)
                        <div class="col-md-3">
                            <div class="box  {{ $item->status?'bg-warning':'bg-info' }} darken">
                              <div class="box-cell p-x-3 p-y-1">
                                <a href="{{ $url . $item->id_user }}">
                                    <div class="font-weight-semibold font-size-12">{{ $item->id_user ?: 'ID KOSONG' }}</div>
                                    <div class="font-weight-semibold font-size-12">{{ ($item->nama ?: $item->nama)?: "NAMA KOSONG" }}</div>
                                    <div class="font-weight-semibold font-size-14">{!! isset($item->maintenance_level) ? 'level '.$lvl[$item->maintenance_level] : "<span style='color: darkred;'>level kosong</span>" !!}</div>
                                    <div class="font-weight-bold font-size-20">{!! $item->nama_instansi ?: "<span style='color: red;'>INSTANSI KOSONG</span>" !!}</div>
                                </a>
                              </div>
                            </div>
                          </div>
                        @endif
                    @endforeach
                </div>
            @endif
          </div>
          <div class="tab-pane fade" id="tabs-suspend">
            @if (isset($list))
                <?php $url = '/user/' ?>
                <div class="row">
                    @foreach($list as $item)
                        @if($item->status)
                        <div class="col-md-3">
                            <div class="box  {{ $item->status?'bg-warning':'bg-info' }} darken">
                              <div class="box-cell p-x-3 p-y-1">
                                <a href="{{ $url . $item->id_user }}">
                                    <div class="font-weight-semibold font-size-12">{{ $item->id_user ?: 'ID KOSONG' }}</div>
                                    <div class="font-weight-semibold font-size-12">{{ ($item->nama ?: $item->nama)?: "NAMA KOSONG" }}</div>
                                    <div class="font-weight-semibold font-size-14">{{ isset($item->maintenance_level) ? 'level '.$lvl[$item->maintenance_level] : 'level kosong' }}</div>
                                    <div class="font-weight-bold font-size-20">{{ $item->nama_instansi ?: "INSTANSI KOSONG" }}</div>
                                </a>
                              </div>
                            </div>
                          </div>
                        @endif
                    @endforeach
                </div>
            @endif
          </div>
        </div>
      </div>
    </div>
<script>
$(function() {
  $('.nav-tabs').on("click", "li", function (event) {         
    var activeTab = $(this).find('a').attr('href').split('-')[1],
    stts = $(this).find('a').attr('data-aktif');

    activeTab = activeTab.toLowerCase().replace(/\b[a-z]/g, function(letter) {
      return letter.toUpperCase();
    });

      $('#jenis').text(activeTab);
      $('.jns_hrf').attr('href', '/download/user_status/' + stts)
  });
});
</script>
@endsection



