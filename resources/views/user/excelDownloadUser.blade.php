<div class="panel panel-default" id="info">
  <div class="panel-heading">Data User {{ $id == 0 ? 'Aktif' : 'Tidak Aktif' }}</div>
    <div id="fixed-table-container-demo" class="fixed-table-container">
      <table class="table table-bordered table-fixed">
        <tr>
          <th>#</th>
          <th>NIK</th>
          <th>Nama</th>
          <th>User</th>
        </tr>
        @foreach($data as $no => $list) 
        @php
          switch ($list->maintenance_level) {
            case '0':
            $stts = 'USER';
            break;
            case '1':
              $stts = 'ADMIN';
            break;
            case '2':
              $stts = 'TL';
            break;
            case '3':
              $stts = 'SM';
            break;
            case '4':
              $stts = 'TELKOM';
            break;
            case '5':
              $stts = 'MITRA LOCAL';
            break;
            case '6':
              $stts = 'ADMIN MITRA';
            break;
            case '7':
              $stts = 'HD';
            break;                  
            default:
              $stts = 'TIDAK ADA';
            break;
          }
        @endphp
          <tr>
            <td>{{ ++$no }}</td>
            <td>{{ $list->id_user }}</td>
            <td>{{ $list->nama }}</td>
            <td>{{ $stts }}</td>
          </tr>
        @endforeach
      </table>
    </div>
</div>