<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

  
  <title>MARINA</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
  <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!-- Core stylesheets -->
  <link href="/css/bootstrap-dark.min.css" rel="stylesheet" type="text/css">
  <link href="/css/pixeladmin-dark.min.css" rel="stylesheet" type="text/css">
  <link href="/css/widgets-dark.min.css" rel="stylesheet" type="text/css">

  <!-- Theme -->
  <link href="/css/themes/mint-dark.min.css" rel="stylesheet" type="text/css">
  <!-- Pace.js -->
  <style>
    .page-signup-header {
      box-shadow: 0 2px 2px rgba(0,0,0,.05), 0 1px 0 rgba(0,0,0,.05);
    }

    .page-signup-header .btn {
      position: absolute;
      top: 12px;
      right: 15px;
    }

    html[dir="rtl"] .page-signup-header .btn {
      right: auto;
      left: 15px;
    }

    .page-signup-container {
      width: auto;
      margin: 30px 10px;
    }

    .page-signup-container form {
      border: 0;
      box-shadow: 0 2px 2px rgba(0,0,0,.05), 0 1px 0 rgba(0,0,0,.05);
    }

    @media (min-width: 544px) {
      .page-signup-container {
        width: 350px;
        margin: 60px auto;
      }
    }

    .page-signup-social-btn {
      width: 40px;
      padding: 0;
      line-height: 40px;
      text-align: center;
      border: none !important;
    }
  </style>
</head>
<body>

  <div class="page-signup-container">
    <h2 class="m-t-0 m-b-4 text-xs-center font-weight-semibold font-size-20">Daftar Akun</h2>

    <form method="post" class="panel p-a-4">
      <fieldset class="form-group form-group-lg">
        <input name="nama" type="text" id="nama" class="form-control input-sm" placeholder="Nama">
      </fieldset>

      <fieldset class="form-group form-group-lg">
        <input name="nama_instansi" type="text" id="mitra" class="form-control input-sm" placeholder="Nama Perusahaan">
      </fieldset>

      <fieldset class="form-group form-group-lg">
        <input name="id_user" type="text" id="id_user" class="form-control input-sm" placeholder="IDUser/NIK" />
      </fieldset>

      <fieldset class="form-group form-group-lg">
        <input name="pwd" type="text" id="pwd" class="form-control input-sm" placeholder="Password">
      </fieldset>

      <fieldset class="form-group form-group-lg">
        <input name="maintenance_level" id="level" class="form-control input-sm" placeholder="Pilih Level">
      </fieldset>

      

      <button type="submit" class="btn btn-block btn-lg btn-primary m-t-3">Daftar</button>
    </form>

  </div>
  
  

    

  <!-- ==============================================================================
  |
  |  SCRIPTS
  |
  =============================================================================== -->

  <!-- Load jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    
  <!-- Core scripts -->
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/pixeladmin.min.js"></script>

  <!-- Your scripts -->
  <script src="/pace/pace1.min.js"></script>
  <script src="/js/app.js"></script>
  <script type="text/javascript">
        $(function() {
            var mitra = <?= json_encode($mitra) ?>;
            $('#mitra').select2({
                data: mitra,
                placeholder: 'Pilih Mitra',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });
            $('#level').select2({
                data: [{"id":"0","text":"Teknisi"},{"id":"2","text":"TL"},{"id":"3","text":"SM"},{"id":"4","text":"TELKOM"},{"id":"6","text":"ADMIN MITRA"}],
                placeholder: 'Pilih level'
            });
        });
    </script>
</body>
</html>
