@extends('layout2')
@section('head')
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>@if (isset($user))
                Edit User {{ $user->nama }}
            @else
                Input User Baru
            @endif</span>
</h1>
@endsection
@section('content')
    <style type="text/css">
        .id-card-holder {
            width: 225px;
            padding: 4px;
            margin: 0 auto;
            background-color: #1f1f1f;
            border-radius: 5px;
            color:black;
        }
        .id-card {
            background-color: #fff;
            padding: 10px;
            border-radius: 10px;
            text-align: center;
            box-shadow: 0 0 1.5px 0px #b9b9b9;
        }
        .id-card img {
            margin: 0 auto;
        }
        .header img {
            width: 150px;
        }
        .mitra{
            margin-top: 15px;
        }
        .photo img {
            width: 100px;
        }
        h2 {
            font-size: 15px;
            margin: 5px 0;
        }
        h3 {
            font-size: 12px;
            margin: 2.5px 0;
            font-weight: 300;
        }
        .qr-code img {
            width: 100px;
            height:50px;
        }

        </style>
    <div class="panel panel-default">
        <?php
            $lvl = session('auth')->maintenance_level;
        ?>
        <div class="panel-body">
            <div class="col-sm-9">
            <form method="post" class="form-horizontal" enctype="multipart/form-data" >
                <input name="hid" type="hidden" value="{{ @$user->id_user }}"/>
                <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
                    <label for="nama" class="col-sm-4 col-md-3 control-label">Nama</label>
                    <div class="col-sm-5">

                        <input name="nama" type="text" id="nama" class="form-control input-sm {{ $lvl==1?:'hidden' }}" value="{{ old('nama') ?: @$user->nama ?: @$user->nama }}">
                        @foreach($errors->get('nama') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                        @if(!session('auth')->maintenance_level)
                            {{ @$user->nama ?: @$user->nama }}
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('ktp') ? 'has-error' : '' }}">
                    <label for="ktp" class="col-sm-4 col-md-3 control-label">Nomor KTP</label>
                    <div class="col-sm-5">

                        <input name="ktp" type="text" id="ktp" class="form-control input-sm" value="{{ old('ktp') ?: @$user->no_ktp }}">
                        @foreach($errors->get('ktp') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>

                <div class="form-group {{ $errors->has('no_hp') ? 'has-error' : '' }}">
                    <label for="no_hp" class="col-sm-4 col-md-3 control-label">Nomor HP Aktif</label>
                    <div class="col-sm-5">

                        <input name="no_hp" type="text" id="no_hp" class="form-control input-sm" value="{{ old('no_hp') ?: @$user->no_gsm1 }}">
                        @foreach($errors->get('no_hp') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>

                <div class="form-group {{ $errors->has('alamat') ? 'has-error' : '' }}">
                    <label for="alamat" class="col-sm-4 col-md-3 control-label">Alamat Rumah</label>
                    <div class="col-sm-5">

                        <textarea name="alamat" id="alamat" class="form-control input-sm">{{ old('alamat') ?: @$user->alamat }}</textarea>
                        @foreach($errors->get('alamat') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>

                <div class="form-group {{ $errors->has('nama_instansi') ? 'has-error' : '' }}">
                    <label for="nama" class="col-sm-6 col-md-3 control-label">Nama Perusahaan</label>
                    <div class="col-sm-5">
                        <input name="nama_instansi" type="text" id="mitra" class="form-control input-sm {{ $lvl==1?:'hidden' }}" value="{{ old('nama_instansi') ?: @$user->nama_instansi }}">
                        @foreach($errors->get('nama_instansi') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                        @if(!session('auth')->maintenance_level)
                            {{ @$user->nama_instansi?:"Belum Diisi Admin" }}
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('id_user') ? 'has-error' : '' }}">
                    <label for="id_user" class="col-sm-3 col-md-3 control-label">IDUser/NIK</label>
                    <div class="col-sm-5">

                        <input name="id_user" type="text" id="id_user" class="form-control input-sm {{ $lvl==1?:'hidden' }}"
                                   value="{{ old('id_user') ?: @$user->id_user }}"/>
                        @foreach($errors->get('id_user') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                        @if(!session('auth')->maintenance_level)
                            {{ @$user->id_user }}
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="pwd" class="col-sm-3 col-md-3 control-label">password</label>
                    <div class="col-sm-5">
                        <input name="pwd" type="text" id="pwd" class="form-control input-sm">
                    </div>
                </div>
                <div class="form-group">
                    <label for="stat" class="col-sm-3 col-md-3 control-label">Status</label>
                    <div class="col-sm-5">
                        <select class="form-control input-sm {{ $lvl==1?:'hidden' }}" id="stat" name="status">
                            <option value="0" {{ @$user->status=="0" ? "selected" : "" }}>ACTIVE</option>
                            <option value="1" {{ @$user->status=="1" ? "selected" : "" }}>NON-ACTIVE</option>
                        </select>
                        @if(!session('auth')->maintenance_level)
                            {{ $user->status?"Non-Active":"Active" }}
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="stat" class="col-sm-3 col-md-3 control-label">Level</label>
                    <div class="col-sm-5">
                        <select name="maintenance_level" id="level" class="form-control input-sm {{ $lvl==1?:'hidden' }}">
                            <option value="0" {{ @$user->maintenance_level=="0" ? "selected" : "" }}>USER</option>
                            <option value="1" {{ @$user->maintenance_level=="1" ? "selected" : "" }}>ADMIN</option>
                            <option value="2" {{ @$user->maintenance_level=="2" ? "selected" : "" }}>TL</option>
                            <option value="3" {{ @$user->maintenance_level=="3" ? "selected" : "" }}>SM</option>
                            <option value="4" {{ @$user->maintenance_level=="4" ? "selected" : "" }}>TELKOM</option>
                            <option value="5" {{ @$user->maintenance_level=="5" ? "selected" : "" }}>MITRA LOCAL</option>
                            <option value="6" {{ @$user->maintenance_level=="6" ? "selected" : "" }}>ADMIN MITRA</option>
                            <option value="7" {{ @$user->maintenance_level=="7" ? "selected" : "" }}>HD</option>
                        </select>
                        @if(!session('auth')->maintenance_level)
                            {{ $user->status==2?"TL":$user->status==3?"SM":$user->status==4?"TELKOM":$user->status==5?"MITRA LOKAL":$user->status==4?"ADMIN MITRA":$user->status==0?"Teknisi":"Admin" }}
                        @endif
                    </div>
                </div>
                @if(in_array(session('auth')->maintenance_level, [1, 3]) )
                    <div class="form-group {{ $errors->has('jenis_verif') ? 'has-error' : '' }}">
                        <label for="nama" class="col-sm-6 col-md-3 control-label">Jenis Verifikasi</label>
                        <div class="col-sm-5">
                            <input name="jenis_verif" type="text" id="jenis_verif" class="form-control input-sm {{ $lvl==1?:'hidden' }}" value="{{ old('jenis_verif') ?: @$user->jenis_verif }}">
                            @foreach($errors->get('jenis_verif') as $msg)
                                <span class="help-block">{{ $msg }}</span>
                            @endforeach
                            @if(!session('auth')->maintenance_level)
                                {{ @$user->jenis_verif }}
                            @endif
                        </div>
                    </div>
                @endif
                <div class="form-group">
                    <label for="nama" class="col-sm-6 col-md-3 control-label">Approval</label>
                    <div class="col-sm-5">
                        <input name="approval" type="text" id="approval" class="form-control input-sm {{ $lvl==1?:'hidden' }}" value="{{ old('approval') ?: @$user->approval }}">
                        @foreach($errors->get('approval') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                        @if(!session('auth')->maintenance_level)
                            {{ @$user->approval }}
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-6 col-sm-2">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
                <div class="row col-md-9 col-sm-5 text-center input-photos" style="margin: 20px 0">
                @foreach($foto as $input)
                    <div class="col-sm-6">
                      <?php
                        $path = "/upload/profile/".@$user->id_user."/$input";
                        $th   = "$path-th.jpg";
                        $img  = "$path.jpg";
                      ?>
                      @if (file_exists(public_path().$th))
                        <a href="{{ $img }}">
                          <img src="{{ $th }}" alt="{{ $input }}" />
                        </a>
                      @else
                        <img src="/image/placeholder.gif" alt="" />
                      @endif
                      <br />
                      <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
                      <button type="button" class="btn btn-sm btn-info">
                        <i class="fa fa-camera"></i>
                      </button>
                      <p>{{ str_replace('_',' ',$input) }}</p>
                    </div>
                @endforeach
            </div>
            </form>
        </div>
        <div class="col-sm-3">
            <div class="id-card-holder">
                <div class="id-card">
                    <div class="header">
                        <img src="https://www.telkomakses.co.id/images/logo_ta.png">
                    </div>
                    <h2 class="mitra">MITRA LOCAL</h2>
                    <?php
                        $path = "/upload/profile/".@$user->id_user."/FOTO.jpg";
                    ?>
                    @if (file_exists(public_path().$path))
                        <img src="{{ $th }}" alt="{{ $th }}" />
                    @else
                        <img src="/image/placeholder.gif" alt="" />
                    @endif
                    <h2>{{@$user->nama}}</h2>
                    <h3>{{@$user->id_user}}</h3>

                    <div class="qr-code">
                        <img src="http://bwipjs-api.metafloor.com/?bcid=code128&text=12345">
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            $('input[type=file]').change(function() {
                console.log(this.name);
                var inputEl = this;
                if (inputEl.files && inputEl.files[0]) {
                  $(inputEl).parent().find('input[type=text]').val(1);
                  var reader = new FileReader();
                  reader.onload = function(e) {
                    $(inputEl).parent().find('img').attr('src', e.target.result);

                  }
                  reader.readAsDataURL(inputEl.files[0]);
                }
            });
            $('.input-photos').on('click', 'button', function() {
                $(this).parent().find('input[type=file]').click();
            });
            var mitra = <?= json_encode($mitra) ?>;
            $('#mitra').select2({
                data: mitra,
                placeholder: 'Pilih Mitra'
            });
            $('#jenis_verif').select2({
                data: [{id:1,text:"Verifikasi TA"},{id:3,text:"Verifikasi TELKOM"}, {id:4,text:"NOK"}],
                placeholder: 'Pilih Jenis Verifikasi',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });
            $('#approval').select2({
                data: [{id:0,text:"Belum Approve"},{id:1,text:"Sudah Approve"}],
                placeholder: 'Pilih Jenis Verifikasi',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });
        });
    </script>
@endsection