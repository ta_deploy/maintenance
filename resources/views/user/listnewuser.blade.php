@extends('layout2')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>List New User</span>
</h1>
@endsection
@section('content')
    <?php $authLevel = Session::get('auth')->maintenance_level ?>


    @if (isset($list))
        <?php $url = '/user/' ?>
        <div class="row">
            @foreach($list as $item)
                <div class="col-md-3">
                    <div class="box bg-info darken">
                      <div class="box-cell p-x-3 p-y-1">
                        <a href="{{ $url . $item->id_user }}">
                            <div class="font-weight-semibold font-size-12">{{ $item->id_user }} : {{ ($item->nama ?: $item->nama2)?: "NAMA KOSONG" }}</div>
                            <div class="font-weight-bold font-size-20">{{ $item->nama_instansi ?: "INSTANSI KOSONG" }}</div>
                        </a>
                      </div>
                    </div>
                  </div>
            @endforeach
        </div>
    @endif
@endsection



