<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

  
  <title>MARINA</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
  <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!-- Core stylesheets -->
  <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="/css/pixeladmin.min.css" rel="stylesheet" type="text/css">
  <link href="/css/widgets.min.css" rel="stylesheet" type="text/css">

  <!-- Theme -->
  <link href="/css/themes/dark-red.min.css" rel="stylesheet" type="text/css">
  <style type="text/css">
    .input-photos img {
      width: 100px;
      height: 150px;
      margin-bottom: 5px;
    }
    .layout_search::placeholder {
      color: white;
      opacity: 1; /* Firefox */
    }

    .layout_search::-ms-input-placeholder { /* Internet Explorer 10-11 */
      color: white;
   }

   .layout_search::-ms-input-placeholder { /* Microsoft Edge */
    color: white;
   }
 </style>
 <!-- Pace.js -->

 @yield('head')
</head>
<body>
  <!--1:admin;2:sdi;3:warroom;4:sales:5:optima;6:HD PROV;7:HS;0:view;-->
  <!-- Nav -->
  <nav class="px-nav px-nav-left">
    <button type="button" class="px-nav-toggle" data-toggle="px-nav">
      <span class="px-nav-toggle-arrow"></span>
      <span class="navbar-toggle-icon"></span>
      <span class="px-nav-toggle-label font-size-11">HIDE MENU</span>
    </button>

    <ul class="px-nav-content">
      <li class="px-nav-box p-a-3 b-b-1" id="demo-px-nav-box">
        <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        <img src="/image/placeholder.gif" alt="" class="pull-xs-left m-r-2 border-round" style="width: 54px; height: 54px;">
        <div class="font-size-16"><span class="font-weight-light">Welcome</strong></div>
      </li>
      <!-- <li class="px-nav-item px-nav-dropdown">
        <a href="#"><i class="px-nav-icon ion-ios-plus"></i><span class="px-nav-label">Admin</span></a>
        <ul class="px-nav-dropdown-menu">
          <li class="px-nav-item"><a href="/user"><span class="px-nav-label">User</span></a></li>
        </ul>
      </li> -->
      
          <li class="px-nav-item"><a href="/test"><i class="px-nav-icon fa fa-sign-out"></i><span class="px-nav-label">test</span></a></li>
        </ul>
      </nav>

      <!-- Navbar -->
      <nav class="navbar px-navbar">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">MARINA</a>
        </div>

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#px-navbar-collapse" aria-expanded="false"><i class="navbar-toggle-icon"></i></button>

        <div class="collapse navbar-collapse" id="px-navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li>
              <form class="navbar-form" role="search" action="#">
                <div class="form-group">
                  <input type="text" name="q" class="form-control layout_search" placeholder="Search Tiket" style="width: 140px;">
                </div>
              </form>
            </li>
          </ul>
        </div>
      </nav>
      <div id="block-alert-with-timer" class="m-b-1"></div>
      <div class="px-content">

        <!-- Content -->
        <div>
          @yield('content')
        </div>
      </div>



      <!-- Footer -->
<!--   <footer class="px-footer px-footer-bottom">
      Marina
    </footer> -->

  <!-- ==============================================================================
  |
  |  SCRIPTS
  |
  =============================================================================== -->

  <!-- Load jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Core scripts -->
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/pixeladmin.min.js"></script>

  <!-- Your scripts -->
  <script src="/pace/pace1.min.js"></script>
  <script src="/js/app.js"></script>
  <script type="text/javascript">
    $(function(){
      var alertExist = <?= json_encode(Session::has('alerts')); ?>;
      if(alertExist){
        toastr.options = {
          "closeButton": true,
          "positionClass": "toast-top-center",
          "onclick": null,
          "timeOut": "200000"
        }
        var msg = <?= json_encode(session('alerts')[0]); ?>;
        toastr.info(msg.text);
      }

      var alertBlock = <?= json_encode(Session::has('alertblock')); ?>;
      if(alertBlock){
        var $container = $('#block-alert-with-timer');
        var alrt = <?= json_encode(session('alertblock')[0]); ?>;
        $container.pxBlockAlert(alrt.text, { type: alrt.type, style: 'light', timer: 3 });
      }

    });
  </script>
  @yield('script')
</body>
</html>
