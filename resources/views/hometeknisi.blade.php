@extends('layout2')
@section('head')
@endsection
@section('content')
<?php
	$auth = session('auth');
?>
<div>
  <div class="row">
    <a href="/listorder/order">
      <div class="col-md-3 col-sm-6 col-xs-6">
        <div class="box bg-info darken">
          <div class="box-cell p-x-3 p-y-1">
            <div class="font-weight-semibold font-size-12">Index Order</div>
            <div class="font-weight-bold font-size-20">{{ $order }}</div>
            <i class="box-bg-icon middle right font-size-52 ion-ios-box"></i>
          </div>
        </div>
      </div>
    </a>
    <a href="/listorder/close">
      <div class="col-md-3 col-sm-6 col-xs-6">
        <div class="box bg-success darken">
          <div class="box-cell p-x-3 p-y-1">
            <div class="font-weight-semibold font-size-12">Closed</div>
            <div class="font-weight-bold font-size-20">{{ $closed }}</div>
            <i class="box-bg-icon middle right font-size-52 ion-android-done-all"></i>
          </div>
        </div>
      </div>
    </a>
    <a href="/listorder/kendala">
      <div class="col-md-3 col-sm-6 col-xs-6">
        <div class="box bg-warning darken">
          <div class="box-cell p-x-3 p-y-1">
            <div class="font-weight-semibold font-size-12">Kendala</div>
            <div class="font-weight-bold font-size-20">{{ $kendala }}</div>
            <i class="box-bg-icon middle right font-size-52 ion-ios-cart"></i>
          </div>
        </div>
      </div>
    </a>
    <a href="/listorder/cancel">
      <div class="col-md-3 col-sm-6 col-xs-6">
        <div class="box bg-danger darken">
          <div class="box-cell p-x-3 p-y-1">
            <div class="font-weight-semibold font-size-12">Cancel Order</div>
            <div class="font-weight-bold font-size-20"><small class="font-weight-light"></small>{{ $cancel }}</div>
            <i class="box-bg-icon middle right font-size-52 ion-android-close"></i>
          </div>
        </div>
      </div>
    </a>

    <a href="/listorder/decline">
      <div class="col-md-3 col-sm-6 col-xs-6">
        <div class="box bg-secondary darken">
          <div class="box-cell p-x-3 p-y-1">
            <div class="font-weight-semibold font-size-12">Decline</div>
            <div class="font-weight-bold font-size-20"><small class="font-weight-light"></small>{{ $decline }}</div>
            <i class="box-bg-icon middle right font-size-52 ion-arrow-graph-down-right"></i>
          </div>
        </div>
      </div>
    </a>

    <!-- / Stats -->

  </div>
  <div id="owl-carousel-autoheight" class="owl-carousel">
    <div class="demo-item bg-primary" style="height: auto;"><img src="/image/slide1.jpg" width="150px" height="400px"/></div>
    <div class="demo-item bg-primary" style="height: auto;"><img src="/image/slide2.jpg" width="150px" height="400px"/></div>
  </div>
</div>
@endsection
@section('script')
	<script>
    $(function() {
      $('#owl-carousel-autoheight').owlCarousel({
        items:      1,
        loop:            true,
        margin:     10,
        autoHeight: true,
        autoWidth: true,
        autoplay:        true,
        autoplayTimeout: 5000,

        rtl: $('html').attr('dir') === 'rtl',
      });
    });
  </script>
@endsection