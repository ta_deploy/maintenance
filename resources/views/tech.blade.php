<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <title>MARINA</title>
  @yield('header')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="/bower_components/vue/dist/vue.min.js"></script>
  <script src="/bower_components/bootstrap-list-filter/bootstrap-list-filter.min.js"></script>
  <link rel="stylesheet" href="/bower_components/bootswatch-dist/css/bootstrap.min.css" />
  <script src="/bower_components/bootswatch-dist/js/bootstrap.min.js"></script>

  
  <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
  <link rel="stylesheet" href="/style.css"/>
  <style media="screen">
    h3 {
      font-size: 24px;
    }
    .list-group-item span, .form-group p.form-control-static {
      word-wrap: break-word;
    }
    .input-photos img {
      width: 80px;
      height: 130px;
      margin-bottom: 5px;
    }
    @media (max-width: 900px) {
        #menu-main .navbar-right li {
            border-top: 1px solid #E7E7E7;
        }
    }
    .navbar {
		    box-shadow: 0 1px 10px rgba(0,0,0,0.3);
		}
    .nav-tabs {
	    border : 1px solid #D6D6D6;
    }
    .btn-info {
	    background-color: #FF5442;
    }
    .label-info {
		    background-color: #2196f3;
		}
		.color_UP {
			color: #FFF;
			background-color : #1cd43e;
		}
		.color_OGP {
			color: #FFF;
			background-color : #d4841c;
		}
		.color_KENDALA {
			color: #FFF;
			background-color : #ff0000;
		}
		.color_BELUM {
			color: #FFF;
			background-color : #527ee6;
		}
		.report {
		}
		#tanggal {
			padding-left: 10px;
		}
    .pengumuman {
      padding: 0px;
      font-weight: bold;
    }


  </style>
</head>
<body>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            @if(session('auth')->maintenance_level)
              <a class="navbar-brand" href="/">MAINTENANCE</a>
            @else
              <a class="navbar-brand" href="/tech">MAINTENANCE</a>
            @endif
        </div>

        <div id="menu-main" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
							<!-- <li><a href="/ba">BERITA ACARA</a></li> -->
              <li class="{{ Request::segment(1) == 'saldo' ? 'active' : '' }}">
                  <a href="/saldo">Saldo Material</a>
              </li>
              <li>
                  <a href="/logout">Logout</a>
              </li>
            </ul>
        </div>
    </div>
  </nav>

  <div class="container-fluid" style="margin-bottom : 50px">
    @yield('content')
  </div>

  <script>
    // $('.container').css('min-height', window.innerHeight - 66);
  </script>

<script src="/bower_components/select2/select2.min.js"></script>
<!-- <div class="footer">  -->
<!-- <div class="pengumuman">
  <marquee>• Habiskan semua WO •   </marquee>
</div> -->
    <!-- <a href="/logout" style="margin-top:10px" class="btn btn-sm btn-warning pull-right">Logout</a>
    <span>Login Sebagai</span>
    <strong>[{{ session('auth')->id_user }}] {{ session('auth')->nama }}</strong>
  </div> -->
  <div class="wait-indicator">
    <span class="glyphicon glyphicon-refresh gly-spin"></span>
</div>
  @yield('script')
  @yield('plugins')
  <script>
    $('form').submit(function () {
        
    })
</script>
</body>
</html>
