@extends('layout2')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Absensi</span>
</h1>
@endsection
@section('content')
  <style>
.container-table {
  width:auto;
  height:400px;
  border:1px solid black;
}
.vertical-center-row {
  margin:auto;
  width:90%;
  padding-top:150px;
  text-align:center;
}
  </style>
  @include('partial.alert')
    @if(!isset($data))
    <div class="container container-table">
    <div class="row vertical-center-row">
        <div class="text-center col-md-4 col-md-offset-4">{{ $msg ?:"ANDA BELUM ABSEN, SILAHKAN KLIK TOMBOL DI BAWAH" }}</div>
      @if(!$msg)
      <form method="post" class="form-horizontal" enctype="multipart/form-data" action="/absenTech">
        <button class="btn btn-xs btn-success button_absen" id-mt="asd">ABSEN</button>
      </form>
      @endif
    </div>
    </div>
    @else
      <h3>LIST ABSENSI MARINA</h3>
      <div class="row">
        @foreach($data as $d)
          <div class="col-sm-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <span class="label label-primary">{{$d->uraian}}</span><br/>
                    -{{ $d->idt1 }}
                    @if($d->idt1)
                      @if(substr($d->verift1, 0, 10) == date('Y-m-d'))
                        <span class="label label-success">Absen Verified</span>
                      @else
                        @if(substr($d->absent1, 0, 10) == date('Y-m-d'))

                          <form method="post" action="/verifAbsen" accept-charset="UTF-8" style="display:inline" onsubmit="return submitForm(this);">
                            <input name="id_user" type="hidden" value="{{ $d->idt1 }}"/>
                            <input name="nama" type="hidden" value="{{ $d->namat1 }}"/>
                            <button class="btn btn-xs btn-info" type="submit">
                              Perlu Verifikasi
                            </button>
                          </form>
                        @else
                          <span class="label label-warning">Belum Absen</span>
                        @endif
                      @endif
                    @endif
                    <br/>.{{$d->namat1}}
                    <br/>
                    -{{ $d->idt2 }}
                    @if($d->idt2)
                      @if(substr($d->verift2, 0, 10) == date('Y-m-d'))
                        <span class="label label-success">Absen Verified</span>
                      @else
                        @if(substr($d->absent2, 0, 10) == date('Y-m-d'))
                          <form method="post" action="/verifAbsen" accept-charset="UTF-8" style="display:inline" onsubmit="return submitForm(this);">
                            <input name="id_user" type="hidden" value="{{ $d->idt2 }}"/>
                            <input name="nama" type="hidden" value="{{ $d->namat2 }}"/>
                            <button class="btn btn-xs btn-info" type="submit">
                              Perlu Verifikasi
                            </button>
                          </form>
                        @else
                          <span class="label label-warning">Belum Absen</span>
                        @endif
                      @endif
                    @endif
                    <br/>.{{$d->namat2}}
                    
                </div>
            </div>
          </div>
        @endforeach
      </div>

    @endif

@endsection
@section('plugins')

  <script type="text/javascript">
  $(function() {
    $('.button_absen').click(function(){
      alert('href');
    })

  });
  function submitForm() {
    return confirm('Yakin lah?');
  }
  </script>
@endsection
