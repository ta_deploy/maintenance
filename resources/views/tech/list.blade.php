@extends('layout2')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LIST</span>
</h1>
@endsection
@section('content')
@include('partial.alert')

  <div class="panel">
    <div class="panel-heading">
      <span class="panel-title">List {{ Request::segment(2) }}</span>
    </div>
    @if($list)
      @foreach($list as $no => $data)
        <div class="widget-support-tickets-item">
          <span class="label label-primary">{{ ++$no }}. {{ $data->nama_order }}</span>
          @if($data->jenis_order==1)
          <strong><a href="/order/{{ $data->id }}" >{{ $data->no_tiket }}</a></strong>
          @else
          <strong>{{ $data->no_tiket }}</strong>
          @endif
          @php
            $now = time(); // or your date as well
            $your_date = strtotime($data->created_at);
            $datediff = $now - $your_date;
          @endphp
          <br><code>Umur Tiket Adalah {{ round($datediff / (60 * 60 * 24)) }} Hari</code>
          <br />
          <span>{{ $data->headline }}</span><br />
          <strong>{{ $data->nama_odp_m }}</strong><br/>
          @if($data->jenis_order == 3)
          <strong>( {{ $data->koordinat }} )</a></strong><br/>
          @endif
          <a href="/tech/{{ $data->id }}" class="btn btn-xs btn-info">
            {{ $data->status or 'Progress' }}
          </a>
          <!-- <a href="/tech-photos/{{ $data->id }}" class="btn btn-xs btn-info">
            Photos
          </a> -->
          @if(@$data->psb_laporan_id)
          <a href="#"  class="btn btn-xs btn-info button_info" id-psb="{{ $data->psb_laporan_id }}">
            Info
          </a>
          @endif
          @if($data->last_stts == 'DECLINE')
          <a href="#" class="btn btn-xs btn-warning" style="color: black;">
            DECLINE
          </a>
          @endif
          <span class="label label-success">
            {{ $data->dispatch_regu_name or '---' }}
          </span>
          &nbsp
        </div>
      @endforeach
    @else
      @if(@$waiting_list)
        @foreach($waiting_list as $no => $data)
          <div class="widget-support-tickets-item">
            <span class="label label-primary">{{ ++$no }}. {{ $data->nama_order }}</span>
            @if($data->jenis_order==1)
            <strong><a href="/order/{{ $data->id }}" >{{ $data->no_tiket }}</a></strong>
            @else
            <strong>{{ $data->no_tiket }}</strong>
            @endif
            @php
              $now = time(); // or your date as well
              $your_date = strtotime($data->timeplan);
              $datediff = $your_date - $now;
              $result = round($datediff / (60 * 60 * 24) );
            @endphp
            <br />
            <span>{{ $data->headline }}</span><br />
            <strong>{{ $data->nama_odp_m }}</strong><br/>
            @if($data->jenis_order == 3)
            <strong>( {{ $data->koordinat }} )</a></strong><br/>
            @endif
            <a href="#" class="btn btn-xs btn-warning" style="color: black;">
              Pekerjaan Dapat Dikerjakan Pada Tanggal <b>{{ $data->timeplan }} ({{ $result }} Hari Lagi)</b>
            </a>
            <span class="label label-success">
              {{ $data->dispatch_regu_name or '---' }}
            </span>
            &nbsp
          </div>
        @endforeach
      @endif
    @endif
  </div>

  <div id="material-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #eee">
          <strong style="color:black;">Detail PSB</strong>
          <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
        </div>
        <div class="modal-body" style="overflow-y:auto;height : 450px">
          <div class="form-group">
            <label for="nama_odp" class="col-sm-4 col-md-3 control-label">Nama ODP</label>
            <input name="nama_odp" id="nama_odp" class="form-control input-sm" rows="1" value="" readonly/>
          </div>
          <div class="form-group">
            <label for="koordinat_odp" class="col-sm-4 col-md-3 control-label">Koordinat ODP</label>
            <input name="koordinat_odp" id="koordinat_odp" class="form-control input-sm" rows="1" value="" readonly/>
          </div>
          <div class="form-group">
            <label for="koordinat_pelanggan" class="col-sm-4 col-md-3 control-label">Koordinat Pelanggan</label>
            <input name="koordinat_pelanggan" id="koordinat_pelanggan" class="form-control input-sm" rows="1" value="" readonly/>
          </div>
          <div class="form-group">
            <label for="catatan" class="col-sm-4 col-md-3 control-label">Catatan</label>
            <input name="catatan" id="catatan" class="form-control input-sm" rows="1" value="" readonly/>
          </div>
        </div>
        <div class="modal-footer" style="background: #eee">

        </div>
      </div>
    </div>
  </div>


  @endsection
  @section('plugins')
  <script>
    $(function() {
      $(".button_info").click(function(event) {
        $(".button_info").hide();
        psb = this.getAttribute("id-psb");
        $.getJSON( "/jsonPsb/"+psb, function(data) {
          $("#nama_odp").val(data.nama_odp_m);
          $("#koordinat_odp").val(data.koordinat);
          $("#koordinat_pelanggan").val(data.kordinat_pelanggan);
          $("#catatan").val(data.catatan);
        })
        .done(function(data) {
          $(".button_info").show();
        });
        $('#material-modal').modal({show:true});
      });
    });
  </script>
  @endsection