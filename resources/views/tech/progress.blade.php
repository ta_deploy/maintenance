@extends('layout2')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>TAMBAH PEKERJAAN ALPRO</span>
</h1>
@endsection
@section('content')
<style>
  .btn strong.fa {
    opacity: 0;
  }
  .btn.active strong.fa {
    opacity: 1;
  }


/* Transparent Overlay */
.sk-cube-grid {
    position: fixed;
    left: 50%;
    z-index: 9999999;
    width: 40px;
    height: 40px;
    margin: 100px auto;
  }
  .sk-cube-grid:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
    background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

  background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
  }

.sk-cube-grid .sk-cube {
  width: 33%;
  height: 33%;
  background-color: #ffffff;
  float: left;
  -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
          animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
}
.sk-cube-grid .sk-cube1 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }
.sk-cube-grid .sk-cube2 {
  -webkit-animation-delay: 0.3s;
          animation-delay: 0.3s; }
.sk-cube-grid .sk-cube3 {
  -webkit-animation-delay: 0.4s;
          animation-delay: 0.4s; }
.sk-cube-grid .sk-cube4 {
  -webkit-animation-delay: 0.1s;
          animation-delay: 0.1s; }
.sk-cube-grid .sk-cube5 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }
.sk-cube-grid .sk-cube6 {
  -webkit-animation-delay: 0.3s;
          animation-delay: 0.3s; }
.sk-cube-grid .sk-cube7 {
  -webkit-animation-delay: 0s;
          animation-delay: 0s; }
.sk-cube-grid .sk-cube8 {
  -webkit-animation-delay: 0.1s;
          animation-delay: 0.1s; }
.sk-cube-grid .sk-cube9 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }

@-webkit-keyframes sk-cubeGridScaleDelay {
  0%, 70%, 100% {
    -webkit-transform: scale3D(1, 1, 1);
            transform: scale3D(1, 1, 1);
  } 35% {
    -webkit-transform: scale3D(0, 0, 1);
            transform: scale3D(0, 0, 1);
  }
}

@keyframes sk-cubeGridScaleDelay {
  0%, 70%, 100% {
    -webkit-transform: scale3D(1, 1, 1);
            transform: scale3D(1, 1, 1);
  } 35% {
    -webkit-transform: scale3D(0, 0, 1);
            transform: scale3D(0, 0, 1);
  }
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="/bower_components/vue/dist/vue.min.js"></script>
<script src="/bower_components/bootstrap-list-filter/bootstrap-list-filter.min.js"></script>
<!-- <link rel="stylesheet" href="/bower_components/select2/select2.css" /> -->
<!-- <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" /> -->
@include('partial.alert')
<div class="alert_photo"></div>
<div class="sk-cube-grid" style="display: none;">
  <div class="sk-cube sk-cube1"></div>
  <div class="sk-cube sk-cube2"></div>
  <div class="sk-cube sk-cube3"></div>
  <div class="sk-cube sk-cube4"></div>
  <div class="sk-cube sk-cube5"></div>
  <div class="sk-cube sk-cube6"></div>
  <div class="sk-cube sk-cube7"></div>
  <div class="sk-cube sk-cube8"></div>
  <div class="sk-cube sk-cube9"></div>
</div>
<div class="parent"></div>

<form id="submit-form" method="post" autocomplete="off">
  <div class="row col-sm-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <span class="panel-title">
          <a href="/" class="btn btn-sm btn-default">
            <span class="ion ion-arrow-left-a"></span>
          </a>
          Proggress {{ @$data->no_tiket }} || {{ @$data->nama_order }}
          </a>
        </span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
        <div class="form-group">
          <label class="control-label" for="no_tiket">Nomor Tiket</label>
          <input name="no_tiket" required type="text" id="no_tiket" class="form-control input-sm" value="{{ old('no_tiket') ?: @$data[0]->no_tiket }}" />
        </div>
        <div class="form-group">
          <label class="control-label" for="id_dalapa">ID Dalapa</label>
          <input name="id_dalapa" type="text" id="id_dalapa" class="form-control input-sm" value="{{ old('id_dalapa') ?: @$data[0]->id_dalapa }}" />
        </div>
        <div class="form-group">
          <label for="status_dalapa" class="control-label">Status</label>
          <select name="status_dalapa" id="status_dalapa" class="form-control input-sm">
            <option value="Selesai">Selesai</option>
            <option value="Kendala">Kendala</option>
          </select>
        </div>
        <div class="form-group">
          <label for="input-status-detail" class="control-label">Timeplan</label>
          <input name="timeplan" type="text" id="timeplan" class="form-control input-sm" value="{{ old('timeplan') ?: @$data->timeplan }}"/>
          @foreach($errors->get('timeplan') as $msg)
            <span class="help-block">{{ $msg }}</span>
          @endforeach
        </div>
        <div class="form-group">
          <label for="jenis_order" class="control-label">Jenis Order</label>
          <select name="jenis_order" id="jenis_order" class="form-control input-sm">
            @foreach($jenis_order as $jo)
              <option value="{{ $jo->id }}" {{ @$data->jenis_order== $jo->id ? "selected" : "" }}>{{ $jo->nama_order }}</option>
            @endforeach
          </select>
        </div>
        <div class="field_regu form-group {{ $errors->has('regu') ? 'has-error' : '' }}">
          <label for="regu" class="control-label">Order ke Tim</label>
          <input name="regu" type="text" required id="regu" class="form-control input-sm" value="{{ old('regu') ?: @$data->dispatch_regu_id }}"/>
          @foreach($errors->get('regu') as $msg)
            <span class="help-block">{{ $msg }}</span>
          @endforeach
        </div>
        {{-- <div class="form-group">
          <label for="kategori" class="control-label">Kategory</label>
          <select name="kategori" id="kategori" class="form-control input-sm">
          </select>
        </div> --}}
        <div class="form-group">
          <label class="control-label" for="input-namaodp">Nama Odp</label>
          <input name="nama_odp" required type="text" id="input-namaodp" class="form-control input-sm" value="{{ old('nama_odp') ?: @$data[0]->odp }}" />
        </div>
        <div class="form-group">
          <label class="control-label" for="input-koor">Koordinat</label>
          <input name="koordinat" type="text" id="input-koor" class="form-control input-sm" value="{{ old('koordinat') ?: @$data[0]->latt.', '.@$data[0]->longt }}" />
        </div>
        <div class="form-group">
          <label for="kondisi_alpro" class="control-label">Flagging Kondisi ODP</label>
          <select name="kondisi_alpro[]" id="kondisi_alpro" class="form-control input-sm" multiple></select>
        </div>
        <div class="form-group">
          <label class="control-label" for="input-koor">Headline</label>
          <textarea name="headline" id="headline" class="form-control">{{ old('headline') ?: str_replace('_', ' ', implode(', ', json_decode(@$data[0]->jenis_kerusakan) ) ) }}</textarea>
        </div>
        <div class="form-group">
          <label class="control-label" for="input-koor">Action</label>
          <textarea name="action" id="act" class="form-control">{!! old('action') ?: @$data[0]->catatan ."\nDengan Kerusakan:\n- ".str_replace('_', ' ', implode("\n- ", json_decode(@$data[0]->jenis_kerusakan) ) ) !!}</textarea>
          <code style="font-size: 13px;">Contoh Action Yang Baik: Tarik KU 24 250 M
            Sambung core 24 X 4 = 96 CORE
            Tarik KU 12 150 M
            SAMBUNG CORE 12 X 2 = 24 CORE</code>
          <span id="act_notify" style="color: red;"></span>
        </div>
        <span style="font-size:10px;">
          {{-- <button data-toggle="modal" data-target="#material-modal" class="btn btn-sm btn-info" type="button">
            <span class="fa fa-list"></span>
            Pekerjaan
          </button> --}}
          <!-- <button data-toggle="modal" data-target="#material-alista" class="btn btn-sm btn-danger" type="button">
            <span class="fa fa-list"></span>
            Material Alista
          </button> -->
          <br/>
          <br/>
          <div class="form-group">
            <ul id="material-list" class="list-group">
              <li class="list-group-item" v-repeat="$data | hasQty ">
                <span class="badge" v-text="qty"></span>
                <strong v-text="id_item"></strong>
                <p v-text="nama_item"></p>
              </li>
            </ul>
          </div>
        </span>
      </div>
    </div>
    <!-- end panel form data -->
  </div>
  <input type="hidden" name="materials" value="[]" />
  <input type="hidden" name="saldo" value="[]" />
  <div class="col-sm-6 text-center">
    <div class="panel panel-info">
      <div class="panel-heading">
        <span class="panel-title">
          Dokumentasi
        </span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
        <!-- <label class="control-label"><b>Dokumentasi</b></label><br /> -->

        @foreach($foto as $k => $v)
          @foreach ($v['base64'] as $kk => $vv)
            <div class="col-xs-6 col-sm-6 col-md-4 text-center input-photos">
              <img src="data:image/jpeg;base64,{{ $vv }}">
              <p style="font-size: 9px;">Foto {{ ++$kk }}</p>
            </div>
          @endforeach
        @endforeach
      </div>
      @if ($data[0]->maintenance_id == 0 && is_null($data[0]->status_dalapa) )
        <div style="margin:40px 0 20px" class="text-center">
          <button class="btn btn-primary btn-smpn">Simpan</button>
        </div>
      @endif
    </div>
  </div>
</form>
<script src="/bower_components/select2/select2.min.js"></script>
<script>
  $(function() {
    // var data = [
    //   {id: 'HVC_PLATINUM', text: 'HVC PLATINUM'},
    //   {id: 'HVC_GOLD', text: 'HVC GOLD'},
    //   {id: 'HVC_SILVER', text: 'HVC SILVER'},
    //   {id: 'REGULER', text: 'REGULER'}
    // ],
    // kategori = $('#kategori').select2({
    //   data: data,
    //   placeholder: "Pilih Kategory"
    // });

    var time = new Date().getTime();

    $('#status_dalapa').select2({
      width: '100%',
      allowClear: true,
      placeholder: 'Masukkan Status Dalap'
    });

    $('#status_dalapa').val(null).change();

    $('#status_dalapa').on('change', function(){
      if($(this).val() != null){
        if($(this).val() == 'Selesai'){
          $('#id_dalapa').attr('required', true);
        }else{
          $('#id_dalapa').removeAttr('required');
        }
        $('.field_regu').hide();
        $('#regu').removeAttr('required');
      }else{
        $('#id_dalapa').removeAttr('required');
        $('#regu').attr('required', true);
        $('.field_regu').show();
      }
    })

    $('#no_tiket').val('INM'+ time.toString().substring(time.toString().length - 8) )

    $('#jenis_order').select2({
      placeholder: 'Masukkan Jenis Ordernya'
    });

    var data = {!! json_encode($regu) !!},
    regu = $('#regu').select2({
      data: data,
      placeholder: "Pilih Regu"
    });

    var data = [
      {id: 'ODP_Rusak', text: 'ODP Rusak'},
      {id: 'ODP_Reti', text: 'ODP Reti'},
      {id: 'ODP_dengan_Slack', text: 'ODP dengan Slack'},
      {id: 'ODP_Gendong_Fisik', text: 'ODP Gendong Fisik'},
      {id: 'ODP_Gendong_UIM', text: 'ODP Gendong UIM'},
      {id: 'ODP_Merah', text: 'ODP Merah'},
      {id: 'ODP_DC_more_150m', text: 'ODP DC more 150m'},
      {id: 'ODP_di_Rute_Utama_Kunjungan2', text: 'ODP di Rute Utama Kunjungan2'},
      {id: 'ODP_belum_Type_Solid', text: 'ODP belum Type Solid'},
    ],
    kondisi_alpro = $('#kondisi_alpro').select2({
      data: data,
      placeholder: "Pilih Kondisi Alpro"
    });

    var stts_alpro = {!! json_encode($get_alpro_stts) !!};

    $('#kondisi_alpro').val(stts_alpro).change();

    var data = {!! json_encode($data2) !!}
    $('#status_dalapa').val(data[0].status_dalapa).change();
  });
</script>

@endsection
