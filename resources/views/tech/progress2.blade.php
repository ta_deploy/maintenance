@extends('layout2')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>UPDATE PEKERJAAN</span>
</h1>
@endsection
@section('content')
<style>
  .btn strong.fa {
    opacity: 0;
  }
  .btn.active strong.fa {
    opacity: 1;
  }


/* Transparent Overlay */
.sk-cube-grid {
    position: fixed;
    left: 50%;
    z-index: 9999999;
    width: 40px;
    height: 40px;
    margin: 100px auto;
  }
  .sk-cube-grid:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
    background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

  background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
  }

.sk-cube-grid .sk-cube {
  width: 33%;
  height: 33%;
  background-color: #ffffff;
  float: left;
  -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
          animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
}
.sk-cube-grid .sk-cube1 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }
.sk-cube-grid .sk-cube2 {
  -webkit-animation-delay: 0.3s;
          animation-delay: 0.3s; }
.sk-cube-grid .sk-cube3 {
  -webkit-animation-delay: 0.4s;
          animation-delay: 0.4s; }
.sk-cube-grid .sk-cube4 {
  -webkit-animation-delay: 0.1s;
          animation-delay: 0.1s; }
.sk-cube-grid .sk-cube5 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }
.sk-cube-grid .sk-cube6 {
  -webkit-animation-delay: 0.3s;
          animation-delay: 0.3s; }
.sk-cube-grid .sk-cube7 {
  -webkit-animation-delay: 0s;
          animation-delay: 0s; }
.sk-cube-grid .sk-cube8 {
  -webkit-animation-delay: 0.1s;
          animation-delay: 0.1s; }
.sk-cube-grid .sk-cube9 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }

@-webkit-keyframes sk-cubeGridScaleDelay {
  0%, 70%, 100% {
    -webkit-transform: scale3D(1, 1, 1);
            transform: scale3D(1, 1, 1);
  } 35% {
    -webkit-transform: scale3D(0, 0, 1);
            transform: scale3D(0, 0, 1);
  }
}

@keyframes sk-cubeGridScaleDelay {
  0%, 70%, 100% {
    -webkit-transform: scale3D(1, 1, 1);
            transform: scale3D(1, 1, 1);
  } 35% {
    -webkit-transform: scale3D(0, 0, 1);
            transform: scale3D(0, 0, 1);
  }
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="/bower_components/vue/dist/vue.min.js"></script>
<script src="/bower_components/bootstrap-list-filter/bootstrap-list-filter.min.js"></script>
<!-- <link rel="stylesheet" href="/bower_components/select2/select2.css" /> -->
<!-- <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" /> -->
@include('partial.alert')
<div class="alert_photo"></div>
<div class="sk-cube-grid" style="display: none;">
  <div class="sk-cube sk-cube1"></div>
  <div class="sk-cube sk-cube2"></div>
  <div class="sk-cube sk-cube3"></div>
  <div class="sk-cube sk-cube4"></div>
  <div class="sk-cube sk-cube5"></div>
  <div class="sk-cube sk-cube6"></div>
  <div class="sk-cube sk-cube7"></div>
  <div class="sk-cube sk-cube8"></div>
  <div class="sk-cube sk-cube9"></div>
</div>
<div class="parent"></div>
@if(@$data->jenis_order == 4)
  <div class="measure">
    <div class='alert alert-warning'>Harus ukur Ibooster sebelum <strong>Submit</strong> data!</div>
  </div>
  <div class="umur">
    <div class='alert alert-danger'>Harus ukur Umur sebelum <strong>Submit</strong> data!</div>
  </div>
@endif
@if(@$data->jenis_order == 5)
  <div class="pemberitahun">
    <div class='alert alert-danger'>Pastikan Foto Harus Diupload dengan <strong>Angle Yang Sama!</strong></div>
  </div>
@endif
<div class="modal fade" id="change_tiket" tabindex="-1" role="dialog" aria-labelledby="change_tiketLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form method="post" action="/change_tiket/{{ $data->id }}" autocomplete="off">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="change_tiketLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row col-sm-12">
            <div class="panel panel-info">
              <div class="panel-heading">
                <span class="panel-title">
                  Pergantian Nomor Tiket
                </a>
              </span>
              <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
              </div>
              <div class="panel-body">
                <div class="form-group">
                  <label class="control-label">Nomor Tiket Baru</label>
                  <input name="notiket" type="text" class="form-control input-sm" value="{{ old('no_tiket') ?: @$data->no_tiket }}" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="modal fade" id="see_valins" tabindex="-1" role="dialog" aria-labelledby="show_valins" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="panel panel-info">
          <div class="panel-heading">
            <span class="title_valins"></span>
          </div>
          <div class="panel-body">
            <table class="table table-bordered table-fixed">
              <thead>
                  <th>TIME VALINS</th>
                  <th>ODP</th>
                  <th>ONU ID</th>
                  <th>ONU SN</th>
                  <th>SIP 1</th>
                  <th>SIP 2</th>
                  <th>NO HSI1</th>
              </thead>
              <tbody class="contain_valins">

              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
  </div>
  </div>
</div>
<form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
  <input name="_method" type="hidden" value="PUT">
  <div class="row col-sm-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <span class="panel-title">
          <a href="/" class="btn btn-sm btn-default">
            <span class="ion ion-arrow-left-a"></span>
          </a>
          Proggress {{ $data->no_tiket }} || {{ $data->nama_order }}
        </span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
        <input name="team" type="hidden" id="input-team" class="form-control" value="{{ $data->team or '' }}" />
        <input name="tgl_selesai" type="hidden" id="input-tgl" class="form-control" value="{{ $data->tgl_selesai or '' }}" />
        <input name="stts" type="hidden" id="stts" class="form-control" value="{{ $data->jenis_order or '' }}" />
        @if(in_array(session('auth')->maintenance_level, [1, 7]) )
          <div class="form-group">
            <label for="no_tiket" class="control-label">Nomor Tiket</label>
            <a class="btn btn-sm btn-info" type="button" data-toggle="modal" data-target="#change_tiket">
              <span class="fa fa-list"></span>
              Ganti Nomor Tiket <b id="red"></b>
            </a>
          </div>
        @endif
        <div class="btn-group" role="group" aria-label="Basic example">
          @if(in_array($data->jenis_order, [8, 10, 17]) )
            <a href="/validasiawal/{{Request::segment(2)}}" class="btn btn-sm btn-success">
              <span class="fa fa-arrow-right"> Update Port Awal</span>
            </a>
            <a href="/reboundary/{{Request::segment(2)}}" class="btn btn-sm btn-info">
              <span class="fa fa-arrow-right"> Reboundary</span>
            </a>
            <a href="/validasi/{{Request::segment(2)}}" class="btn btn-sm btn-success">
              <span class="fa fa-arrow-right"> Update Port Akhir</span>
            </a>
          @endif
          {{-- @if(in_array($data->jenis_order, [1, 3, 4, 8, 10, 17 , 9, 19]))
            <a href="/inputValins/{{Request::segment(2)}}" class="btn btn-sm btn-info">
              <span class="fa fa-arrow-right">Input Valins</span>
            </a>
          @endif --}}
        </div>
        <div class="form-group">
          <label class="control-label" for="status">Status</label>
          <input name="status" required type="text" id="status" class="form-control input-sm" value="{{ old('status') ?: $data->status }}" />
          @foreach($errors->get('status') as $msg)
            <span class="help-block">{{ $msg }}</span>
          @endforeach
        </div>
        <div class="form-group">
          <label class="control-label" for="regu">Regu</label>
          <textarea name="regu" style="resize: vertical;" id="regu" class="form-control">{{ old('regu') ?: ($data->dispatch_regu_name ?? implode(' / ', $un) ) }}</textarea>
        </div>
        @foreach ($get_field as $v)
          @php
            $val = null;
            switch ($v->id) {
              case 'speedy':
                $val = $data->no_inet;
              break;
              case 'status_detail':
                $val = $data->status_detail;
              break;
              case 'redaman_awal':
                $val = $data->redaman_awal;
              break;
              case 'redaman_akhir':
                $val = $data->redaman_akhir;
              break;
              case 'splitter':
                $val = $data->splitter;
              break;
              case 'port_idle':
                $val = $data->port_idle;
              break;
              case 'port_used':
                $val = $data->port_used;
              break;
              case 'action_cause':
                $val = $data->action_cause;
              break;
              case 'gamas_cause':
                $val = $data->gamas_cause;
              break;
              case 'penyebab_id':
                $val = $data->penyebab_id;
              break;
              case 'nama_odp':
                $val = $data->odp_nama;
              break;
              case 'koordinat':
                $val = $data->koordinat;
              break;
              case 'koordinat_tiang':
                $val = $data->koordinat_tiang;
              break;
              case 'headline':
                $val = $data->headline;
              break;
              case 'action':
                $val = $data->action;
              break;
              case 'discovery':
                $val = $data->discovery;
              break;
              case 'valins_awal':
                $val = $data->valins_awal;
              break;
              case 'valins_akhir':
                $val = $data->valins_akhir;
              break;
              case 'valin_bot':
                $val = @$data->valin_bot ? html_entity_decode($data->valin_bot, ENT_QUOTES, 'UTF-8') : '';
              break;
            }
          @endphp
          <div class="form-group">
            @if(!in_array($v->id, ['koordinat_tiang']) )
              <label class="control-label" for="{{ $v->id }}">{{ $v->text }}</label>
            @else
              <label class="control-label">Masukkan Jumlah Tiang</label>
            @endif

            @if(in_array($v->id, ['headline', 'discovery']) )
              <textarea name="{{ $v->id }}" style="resize: vertical;" rows="10" id="{{ $v->id }}" class="form-control">{{ old($v->id) ?: $val }}</textarea>
            @elseif($v->id == 'action')
              <textarea name="{{ $v->id }}" id="{{ $v->id }}" class="form-control">{{ old($v->id) ?: $val }}</textarea>
              <code style="font-size: 13px;">Contoh Action Yang Baik: Tarik KU 24 250 M
              Sambung core 24 X 4 = 96 CORE
              Tarik KU 12 150 M
              SAMBUNG CORE 12 X 2 = 24 CORE</code>
              <span id="act_notify" style="color: red;"></span>
            @elseif($v->id == 'abd')
              @php
                $check_path = public_path() . '/upload/maintenance/abd/' . Request::segment(2) . '/';
                $files_abd = @preg_grep('~^File ABD.*$~', @scandir($check_path));
              @endphp
              @if (count($files_abd) != 0)
                <code><a href="/download_rfc/id/{{ Request::segment(2) }}">Silahkan Download ABD Disini</a></code>
              @endif
              <input name="{{ $v->id }}" type="file" id="{{ $v->id }}" class="form-control input-sm"/>
            @elseif($v->id == 'splitter_jenis')
              <select class="form-control" id="{{ $v->id }}" name="{{ $v->id }}">
                <option value="0">Sebelum</option>
                <option value="1">Sesudah</option>
              </select>
            @elseif($v->id == 'koordinat_tiang')
              <input name="jml_tiang" type="text" id="jml_tiang" class="form-control input-sm" placeholder="Masukkan Jumlah Tiang" value="{{ $val != null ? count(explode(';', $val) ) : '' }}" />
              <div id='field_tiang'></div>
            @elseif($v->id == 'speedy')
              <input name="{{ $v->id }}" type="text" id="{{ $v->id }}" class="form-control input-sm" value="{{ old($v->id) ?: $val }}" />
              <button class="btn btn-sm btn-info" type="button" id="ukur" style="margin-top: 5px;">
                <span class="fa fa-list"></span>
                Ukur <b id="red"></b>
              </button>
              <input type="hidden" name="ibooster" id="ibooster" class="form-control"/>
            @elseif($v->id == 'valin_bot')
              <textarea name="{{ $v->id }}" id="{{ $v->id }}" class="form-control">{{ old($v->id) ?: $val }}</textarea>
            @else
              <input name="{{ $v->id }}" type="text" id="{{ $v->id }}" class="form-control input-sm" value="{{ old($v->id) ?: $val }}" />
              @foreach($errors->get($v->id) as $msg)
                <span class="help-block">{{ $msg }}</span>
              @endforeach
              @if($v->id == 'valins_awal')
                <label class="control-label">Tanggal Valins Awal</label>
                <input name="tgl_valins_awal" type="text" id="tgl_valins_awal" class="form-control input-sm input-tgl-valins" required value="{{ old('tgl_valins_awal') ?? $data->tgl_valins_awal }}" />
                @if($val)
                  <span style="font-size:10px;">
                    <button data-toggle="modal" style="margin-top: 15px;" data-target="#see_valins" data-id_valins="{{ $val }}" data-jenis_valins='0' class="btn btn-sm btn-info see_valins" type="button">
                      <span class="fa fa-list"></span>
                      Lihat Valins Awal
                    </button>
                  </span>
                @endif
              @endif
              @if($v->id == 'valins_akhir')
                <label class="control-label">Tanggal Valins Akhir</label>
                <input name="tgl_valins_akhir" type="text" id="tgl_valins_akhir" class="form-control input-sm input-tgl-valins" required value="{{ old('tgl_valins_akhir') ?? $data->tgl_valins_akhir }}" />
                @if($val)
                  <span style="font-size:10px;">
                    <button data-toggle="modal" style="margin-top: 15px;" data-target="#see_valins" data-id_valins="{{ $val }}" data-jenis_valins='1' class="btn btn-sm btn-info see_valins" type="button">
                      <span class="fa fa-list"></span>
                      Lihat Valins Akhir
                    </button>
                  </span>
                @endif
              @endif
            @endif
          </div>
        @endforeach
        <span style="font-size:10px;">
          <button data-toggle="modal" data-target="#material-modal" class="btn btn-sm btn-info" type="button">
            <span class="fa fa-list"></span>
            Pekerjaan
          </button>
          <!-- <button data-toggle="modal" data-target="#material-alista" class="btn btn-sm btn-danger" type="button">
            <span class="fa fa-list"></span>
            Material Alista
          </button> -->
          @if (in_array($data->jenis_order, [6, 9]))
            <button data-toggle="modal" data-target="#material-modal-nte" class="btn btn-sm btn-default" type="button">
              <span class="fa fa-list"></span>
              Pemakaian NTE
            </button>
          @endif
          <input type="hidden" name="materialsNte" value="[]" />
          <br/>
          <br/>
          <div class="form-group">
            <ul id="material-list-nte" class="list-group">
              <li class="list-group-item" v-repeat="$data | hasQty ">
                <span class="badge" v-text="qty"></span>
                <strong v-text="jenis_nte"></strong>
                <p v-text="id"></p>
              </li>
            </ul>
            <ul id="material-list" class="list-group">
              <li class="list-group-item" v-repeat="$data | hasQty ">
                <span class="badge" v-text="qty"></span>
                <strong v-text="id_item"></strong>
                <p v-text="nama_item"></p>
              </li>
            </ul>
          </div>
        </span>
        @php
          $ver = $data->verify;
          $l   = session('auth')->jenis_verif;
          $nm  = session('auth')->nama_instansi;

          $approve = $decline = null;
          if($l == 1 || session('auth')->id_user == 18940469)
          {
            $decline = 4;
            $approve = 1;
          }
          elseif(in_array($l, [2, 3]) || session('auth')->id_user == 18940469)
          {
            $decline = 5;
            $approve = 2;

            if($ver == 2)
            {
              $approve = 3;
            }
          }
        @endphp
        @if( ($l == 1 && in_array($ver, [null, 4, 5] ) ) || in_array($ver, [1] ) && ($l == 2 && $nm == 'TELKOM AKSES' || $l == 3) )
          <div data-toggle="buttons" style="margin-bottom: 5px;">
            <button data-toggle="modal" class="btn btn-danger col-sm-6 decl" data-val="{{ $decline }}" type="button">
              <span class="fa fa-ban"></span>
              Decline
            </button>
            <button data-toggle="modal" class="btn btn-success col-sm-6 appr" data-val="{{ $approve }}" type="button">
              <span class="fa fa-check-circle"></span>
              Approve
            </button>
          </div>
        @else
          <input type="hidden" name="verify" value="{{ $data->verify or '' }}" />
        @endif
        <div class="table-responsive col-sm-12">
          <table class="table table-bordered table-striped">
            <tr>
              <th>NIK</th>
              <th>NAMA</th>
              <th>TANGGAL</th>
              <th>STATUS</th>
              <th>KOMENTAR</th>
            </tr>
            <tr>
              <td>{{ $data->created_by }}</td>
              <td>{{ $data->psbcreated ?? 'Tidak Ada Nama' }}</td>
              <td>{{ $data->created_at }}</td>
              <td>{{ $data->order_from ? 'TOMMANPSB':'MANUAL' }}</td>
              <td>{{ $data->psbcreated }}</td>
            </tr>
            @php
          @endphp
            @foreach($log as $dl)
              <tr>
                <td>{{ $dl->pelaku }}</td>
                <td>{{ $dl->nama ?? 'Tidak Ada Nama' }}</td>
                <td>{{ $dl->ts_dispatch }}</td>
                <td>{{ $dl->action }}</td>
                <td>{{ $dl->catatan }}</td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" name="materials" value="[]" />
  <input type="hidden" name="saldo" value="[]" />
  <div class="col-sm-6 text-center">
    <div class="panel panel-info">
      <div class="panel-heading">
        <span class="panel-title">
          Dokumentasi
        </span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
        <!-- <label class="control-label"><b>Dokumentasi</b></label><br /> -->
        <?php
          // dd($data);
          $number = 1;
          $col = 3;
          // echo "http://t1psb.tomman.info/upload/evidence/".$data->id_tbl_mj."/ODP.jpg";
          // if($data->psb_laporan_id){
            // if (file_exists("http://t1psb.tomman.info/upload/evidence/".$data->id_tbl_mj."/ODP.jpg")){
              // echo "http://t1psb.tomman.info/upload/evidence/".$data->id_tbl_mj."/ODP.jpg";
              if($data->order_from){
                $folder="";
                $photos = [];
                if($data->order_from=="PSB"){
                  $folder = "evidence";
                  $photos = ["ODP","Redaman_ODP"];
                }
                else if($data->order_from=="ASSURANCE"){
                  $folder = "asurance";
                  $photos = ["ODP","Hasil_Ukur_OPM"];
                }

                foreach($photos as $photo){
                  $path   = $folder.'/'.$data->id_tbl_mj.'/'.$photo.'.jpg';
                  $pathth = $folder.'/'.$data->id_tbl_mj.'/'.$photo.'-th.jpg';

                  if(file_exists(public_path().'/upload/'.$path)){
                    $odp   = '/upload/'.$path;
                    $odpth = '/upload/'.$pathth;
                  }else if(file_exists(public_path().'/upload2/'.$path)){
                    $odp   = '/upload2/'.$path;
                    $odpth = '/upload2/'.$pathth;
                  }else if(file_exists(public_path().'/upload3/'.$path)){
                    $odp   = '/upload3/'.$path;
                    $odpth = '/upload3/'.$pathth;
                  }else if(file_exists(public_path().'/upload4/'.$path)){
                    $odp   = '/upload4/'.$path;
                    $odpth = '/upload4/'.$pathth;
                  }else{
                    $odp   = '/image/placeholder.gif';
                    $odpth = '/image/placeholder.gif';
                  }
                  // echo '<div class="col-xs-6 col-sm-6 col-md-4"><a href="'.$odp.'"><img src="'.$odpth.'" style="width:100px;height:150px;" /></a><br/>FOTO '.$photo.'('.$data->order_from.':'.$data->id_tbl_mj.')</div>';
                }
              }
            // }
          // }
        ?>

        @foreach($foto_bot as $k => $v)
          @foreach ($v['base64'] as $kk => $vv)
            <div class="col-xs-6 col-sm-6 col-md-4 text-center input-photos">
              <img src="data:image/jpeg;base64,{{ $vv }}">
              <p style="font-size: 9px;">Foto {{ ++$kk }}</p>
            </div>
          @endforeach
        @endforeach

        @foreach($get_field_photo as $v)
          <div class="col-xs-6 col-sm-6 col-md-4 text-center input-photos">
            <?php
              $input = $v['evident'];

              $path  = "/upload/maintenance/".Request ::segment(2)."/$input";
              $path2 = "/upload2/maintenance/".Request::segment(2)."/$input";
              $path3 = "/upload3/maintenance/".Request::segment(2)."/$input";
              $th    = "$path-th.jpg";
              $th2   = "$path2-th.jpg";
              $th3   = "$path3-th.jpg";
              $img   = "$path.jpg";
              $img2  = "$path2.jpg";
              $img3  = "$path3.jpg";
              $flag  = "";
              $name  = "flag_".$input;
            ?>
            @if (file_exists(public_path().$th))
              <a href="{{ $img }}">
                <img src="{{ $th }}?x={{ filemtime(public_path().$th) }}" alt="{{ $input }}" />
              </a>
              <?php
                $flag = 1;
              ?>
            @elseif(file_exists(public_path().$th2))
              <a href="{{ $img2 }}">
                <img src="{{ $th2 }}?x={{ filemtime(public_path().$th2) }}" alt="{{ $input }}" />
              </a>
              <?php
                $flag = 1;
              ?>
            @elseif(file_exists(public_path().$th3))
              <a href="{{ $img3 }}">
                <img src="{{ $th3 }}?x={{ filemtime(public_path().$th3) }}" alt="{{ $input }}" />
              </a>
              <?php
                $flag = 1;
              ?>
            @else
              <img src="/image/placeholder.gif" alt="" />
            @endif
              <br />
              <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
              <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
            {{-- @if(@$data->status != 'close' || !in_array($data->verify, [2, 3] ) ) --}}
              <button type="button" class="btn btn-sm btn-info">
                <i class="ion ion-camera"></i>
              </button>
            {{-- @endif --}}
            <p style="font-size: 9px;">{{ str_replace('_',' ',$input) }}</p>
            {!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
          </div>
          <?php
            $number++;
          ?>
        @endforeach
      </div>
      <div style="margin:40px 0 20px" class="text-center">
        @if( ( !is_null($check_regu) && session('auth')->maintenance_level == 0 && !in_array($data->verify, [1, 2] ) || in_array($l, [1, 3]) || $l == 2 && $nm == 'TELKOM AKSES') && $data->verify != 3 || in_array(session('auth')->id_user, [18940469]) )
          <button class="btn btn-primary btn-smpn">Simpan</button>
        @endif
      </div>
    </div>
  </div>
</form>
<div id="detail_dec_app" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header header-success" style="background: #eee">
        <strong style="color:black;" class="judul_decapp">Detail</strong>
        <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="komen" name="catatan" id="catatan"/>
          <input type="hidden" name="jns_decapp" id="jns_decapp">
          <input type="hidden" name="count_me" id="count_me">
        </div>
      </div>
      <div class="modal-footer" style="background: #eee">
        <button class="btn btn-block btn-info submit_decapp" type="button">Submit</button>
      </div>
    </div>
  </div>
</div>
<div id="material-modal-nte" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background: #eee">
        <strong style="color:black;">Stock NTE</strong>
        <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
      </div>
      <div class="modal-body" style="overflow-y:auto;height : 450px">
        <div class="form-group">
          <input id="searchinput" class="form-control" type="search" placeholder="Search..." />
        </div>
        <ul id="searchlist" class="list-group">
          <li class="list-group-item" v-repeat="$data">
            <strong v-text="jenis_nte"></strong><br>
            <strong v-text="id"></strong><br>
            <div class="input-group" style="width:150px">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button" v-on="click: onMinus(this, 1)">
                  <span class="fa fa-minus"></span>
                </button>
              </span>
              <input v-on="change: limit(this, 1), keypress: onlyNumber($event)" v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center" />
              <span class="input-group-btn">
                <button class="btn btn-default" type="button" v-on="click: onPlus(this, 1)">
                  <span class="fa fa-plus"></span>
                </button>
              </span>
            </div>
          </li>
        </ul>
      </div>
      <div class="modal-footer" style="background: #eee">
        <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
      </div>
    </div>
  </div>
</div>
<div id="material-modal" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background: #eee">
        <strong style="color:black;">Laporan Material</strong>
        <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
      </div>
      <div class="modal-body" style="overflow-y:auto;height : 450px">
        <input id="searchinput_lm" class="form-control input-sm" type="search" placeholder="Search..." />
        <ul id="searchlist_lm" class="list-group">
          <li class="list-group-item" v-repeat="$data">
            <strong v-text="id_item"></strong>
            <div class="input-group" style="width:150px;float:right;">
              <span class="input-group-btn">
                <button class="btn btn-default btn-sm" type="button" v-on="click: onMinus(this)">
                  <span class="fa fa-minus"></span>
                </button>
              </span>
              <!-- <button class="btn btn-default" type="button" v-text="qty | doubleDigit" disabled></button> -->
              <input v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center input-sm" />
              <span class="input-group-btn">
                <button class="btn btn-default btn-sm" type="button" v-on="click: onPlus(this)">
                  <span class="fa fa-plus"></span>
                </button>
              </span>
            </div>
            <p v-text="nama_item" style="font-size:10px;"></p>
          </li>
        </ul>
      </div>
      <div class="modal-footer" style="background: #eee">

      </div>
    </div>
  </div>
</div>
<div id="material-alista" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background: #eee">
        <strong style="color:black;">Laporan Material Alista</strong>
        <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
      </div>
      <div class="modal-body" style="overflow-y:auto;height : 450px">
        <input id="searchinput_al" class="form-control input-sm" type="search" placeholder="Search..." />
        <ul id="searchlist_al" class="list-group">
          <li class="list-group-item" v-repeat="$data">
            <strong v-text="id_item"></strong>
            <div class="input-group" style="width:150px;float:right;">
              <span class="input-group-btn">
                <button class="btn btn-default btn-sm" type="button" v-on="click: onMinus(this)">
                  <span class="fa fa-minus"></span>
                </button>
              </span>
              <!-- <button class="btn btn-default" type="button" v-text="qty | doubleDigit" disabled></button> -->
              <input v-model="pemakaian" style="border-top: 1px solid #eeeeee" class="form-control text-center input-sm"/>
              <span class="input-group-btn">
                <button class="btn btn-default btn-sm" type="button" v-on="click: onPlus(this)">
                  <span class="fa fa-plus"></span>
                </button>
              </span>
            </div>
            <p v-text="rfc" style="font-size:10px;"></p>
            <p style="font-size:10px;">In-Tech=<b v-text="sisa"></b>;Pemakaian=<b v-text="pemakaian"></b>;</p>
          </li>
        </ul>
      </div>
      <div class="modal-footer" style="background: #eee">
      </div>
    </div>
  </div>
</div>
<script src="/bower_components/select2/select2.min.js"></script>
<script>
  $(function() {

    $(".see_valins").on('click', function(){
      var id_valins = $(this).data('id_valins'),
      jenis_valins = $(this).data('jenis_valins'),
      id_wo = window.location.href.split('/')[4];

      $.ajax({
        url: "/get_valins",
        type: 'GET',
        data: {
          jenis_valins: jenis_valins,
          id_valins: id_valins,
          id_wo: id_wo
        },
        dataType: 'JSON'
      }).done(function(data){
        $('.title_valins').text(`Data Valins ${id_valins}`);
        var table = '';

        $.each(data, function(k, v){
            table +='<tr>';
            table += `<td>${v.time_valins}</td>`
            table += `<td>${v.odp}</td>`
            table += `<td>${v.onu_id}</td>`
            table += `<td>${v.onu_sn}</td>`
            table += `<td>${v.sip1}</td>`
            table += `<td>${v.sip2}</td>`
            table += `<td>${v.no_hsi1}</td>`
            table +='</tr>';
          });
          console.log(table)
        $('.contain_valins').html(table)
      })
    })

    $("#valins_akhir, #valins_awal").on("keypress keyup",function (e){
			$(this).val($(this).val().replace(/\D/g, ''));
			var charCode = (e.which) ? e.which : e.keyCode;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) {
				return false;
			}
		});

    $(".input-tgl-valins").datepicker({
      format: "yyyy-mm-dd"
    });

    var field = {!! json_encode($get_field) !!};

    function upperword(str){
      return str.toUpperCase();
    }

    function countWords(str) {
      str = str.replace(/(^\s*)|(\s*$)/gi,"");
      str = str.replace(/[ ]{2,}/gi," ");
      str = str.replace(/\n /,"\n");
      return str.split(' ').length;
    }

    data_session = {!! json_encode($data2) !!};

    $('#regu').prop("readonly", true);

    if($.inArray(data_session.jenis_order, [2, 5, 6]) != -1){
      if($("#status").val() == 'close'){
        $('#act_notify').css({
          'display': 'block'
        })

        if ($.trim($("#action").val()).length >= 15 && countWords($('#action').val()) >= 10 ){
          $('#act_notify').html('Sudah Benar')
          $('#act_notify').css({
            'color': 'green'
          })
          $('.btn-smpn').removeAttr('disabled');
        }else{
          $('#act_notify').html('Keterangan Action Harus 15 Karakter Dan 9 Kalimat ')
          $('#act_notify').css({
            'color': 'red'
          })
          $('.btn-smpn').attr('disabled', true);
        }
      }else{
        $('.btn-smpn').removeAttr('disabled');
        $('#act_notify').css({
          'display': 'none'
        })
      }
    }

    $(".decl").on('click', function(){
      $('#detail_dec_app').modal('show')
      $(".judul_decapp").html('Detail Decline')
      $("#jns_decapp").val('DECLINE')
      $("#count_me").val($(this).attr('data-val'))
    })

    $(".appr").on('click', function(){
      $('#detail_dec_app').modal('show')
      $(".judul_decapp").html('Detail Approve')
      $("#jns_decapp").val('APPROVE')
      $("#count_me").val($(this).attr('data-val'))
    })

    $('.detail_status_').css({
      'display': 'none'
    });

    if($("#status").val() == 'kendala pelanggan'){
      $('.detail_status_').css({
        'display': 'block'
      });

      var status = [{"id":"alamat tidak ditemukan", "text": upperword("Alamat Tidak Ditemukan")},{"id":"rukos", "text": upperword("Rukos")},{"id":"pelanggan menolak", "text": upperword("Pelanggan Menolak")}];

      $('#status_detail').select2({
        data: status,
        placeholder: 'Input status'
      });

      $('#status_detail').attr('required', true);

    }else if($("#status").val() == 'kendala teknis'){
      $('.detail_status_').css({
        'display': 'block'
      });

      var status = [{"id":"odp reti", "text": upperword("ODP Reti")},{"id":"spl 1:16", "text": upperword("Spl 1:16")},{"id":"odp gendong", "text": upperword("Odp Gendong")}, {"id":"tarikan jauh", "text": upperword("Tarikan Jauh")}, {"id":"feeder rusak", "text": upperword("Feeder Rusak")}, {"id":"distribusi rusak", "text": upperword("Distribusi Rusak")}];

      $('#status_detail').select2({
        data: status,
        placeholder: 'Input status'
      });

      $('#status_detail').attr('required', true);

    }else{
      $('#status_detail').removeAttr('required');
      $('#status_detail').val('');

      $('.detail_status_').css({
        'display': 'none'
      });
    }

    if($("#status").val() == 'close'){
      $.each(field, function(k, v){
        // console.log($('#' + v.id))
        if(v.id == 'koordinat_tiang'){
          $('#jml_tiang').attr('required', 'required');
        }

        $('#' + v.id).attr('required', v.require);

        if(v.visible == true){
          $('#' + v.id).show();
        }else{
          $('#' + v.id).hide();
        }
      })
    }

    $('#nama_odp').attr('required', true)

    $("#status").on('select2:select', function(){
      $.each(field, function(k, v){
        if($("#status").val() == 'close'){
          if(v.id == 'koordinat_tiang'){
            $('#jml_tiang').attr('required', 'required');
          }

          $('#' + v.id).attr('required', v.require);
        }else{
          $('#' + v.id).removeAttr('required');
        }
        if(v.visible == true){
          $('#' + v.id).show();
        }else{
          $('#' + v.id).hide();
        }
      });

      if($.inArray(data_session.jenis_order, [2, 5, 6]) != -1){
        if($(this).val() == 'close'){
          if(data_session.jenis_order == 4){
            if($("ibooster").val().length != 0){
              $('.btn-smpn').attr('disabled', true);
            }else{
              $('.btn-smpn').removeAttr('disabled');
            }
          }

          $('#act_notify').css({
            'display': 'block'
          })

          if ($.trim($("#action").val()).length >= 15 && countWords($('#action').val()) >= 10 ){
            $('#act_notify').html('Sudah Benar')
            $('#act_notify').css({
              'color': 'green'
            })
            $('.btn-smpn').removeAttr('disabled');
          }else{
            $('#act_notify').html('Keterangan Action Harus 15 Karakter Dan 9 Kalimat ')
            $('#act_notify').css({
              'color': 'red'
            })
            $('.btn-smpn').attr('disabled', true);
          }
        }else{
          $('.btn-smpn').removeAttr('disabled');
          $('#act_notify').css({
            'display': 'none'
          })
        }
      }

      if($(this).val() == 'kendala pelanggan'){
        $('.detail_status_').css({
          'display': 'block'
        });

        var status = [{"id":"alamat tidak ditemukan", "text": upperword("Alamat Tidak Ditemukan")},{"id":"rukos", "text": upperword("Rukos")},{"id":"pelanggan menolak", "text": upperword("Pelanggan Menolak")}];

        $('#status_detail').select2({
          data: status,
          placeholder: 'Input status'
        });

        $('#status_detail').attr('required', true);

      }else if($(this).val() == 'kendala teknis'){
        $('.detail_status_').css({
          'display': 'block'
        });

        var status = [{"id":"odp reti", "text": upperword("ODP Reti")},{"id":"spl 1:16", "text": upperword("Spl 1:16")},{"id":"odp gendong", "text": upperword("Odp Gendong")}, {"id":"tarikan jauh", "text": upperword("Tarikan Jauh")}, {"id":"feeder rusak", "text": upperword("Feeder Rusak")}, {"id":"distribusi rusak", "text": upperword("Distribusi Rusak")}];

        $('#status_detail').select2({
          data: status,
          placeholder: 'Input status'
        });

        $('#status_detail').attr('required', true);

      }else{
        $('#status_detail').removeAttr('required');
        $('#status_detail').val('');

        $('.detail_status_').css({
          'display': 'none'
        });
      }
    })

    $('.input_remo').hide();

    $('input[type=file]').change(function() {
      var data = new FormData(),
      id = window.location.href.split('/'),
      nama_foto = this.name;
      data.append(this.name, $(this).prop('files')[0]);
      data.append('id', id[4]);
      console.log(data, $(this).prop('files')[0], this.name)

      $.ajax({
        type: 'POST',
        processData: false,
        contentType: false,
        data: data,
        url: '/upload_photo_ajx',
        beforeSend: function() {
          $('.sk-cube-grid').css({
            'display': 'block'
          });
        }
      }).done(function(e){
        console.log(e)
        $('.sk-cube-grid').css({
          'display': 'none'
        });

        $('.alert_photo').addClass('alert alert-success');
        $('.alert_photo').html('Label '+nama_foto+' Sudah Disimpan!');
      }).fail(function(e){
        console.log(e)
        $('.alert_photo').html('error', e);
      });

      var inputEl = this;
      if (inputEl.files && inputEl.files[0]) {
        $(inputEl).parent().find('input[type=text]').val(1);
        var reader = new FileReader();
        reader.onload = function(e) {
          $(inputEl).parent().find('img').attr('src', e.target.result);

        }
        reader.readAsDataURL(inputEl.files[0]);
      }
    });

    $('.input-photos').on('click', 'button', function() {
      $(this).parent().find('input[type=file]').click();
    });

    Vue.filter('hasQty', function(value) {
      return value.filter(function(a) { return a.qty > 0 });
    });

    var ntelist = {!! json_encode($nte) !!} ?? null;
      var listVm_nte = new Vue({
        el: '#material-list-nte',
        data: ntelist
      });
      var modalVm = new Vue({
        el: '#material-modal-nte',
        data: ntelist,
        methods: {
          onPlus: function(item, a) {
            if (item.qty >= a)
              item.qty = a;
            else
              item.qty++;
          },
          onMinus: function(item, a) {
            if (item.qty <= 0)
              item.qty = 0;
            else
              item.qty--;
          },
          limit: function(item, a){
            if (item.qty >= a) item.qty = a;
          },
          onlyNumber ($event) {
            let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
            console.log($event)
            if ((keyCode < 48 || keyCode > 57) && keyCode != 8) {
              $event.preventDefault();
            }
          },
        }
      });

    var materials = {!! json_encode($materials) !!};

    Vue.filter('doubleDigit', function(value) {
      var v = Number(value);
      if (v < 1) return '00';
      else if (String(v).length < 2) return '0' + v;
    });

    var listVm = new Vue({
      el: '#material-list',
      data: materials
    });

    var modalVm = new Vue({
      el: '#material-modal',
      data: materials,
      methods: {
        onPlus: function(item) {
          if (!item.qty) item.qty = 0;
          item.qty++;
        },
        onMinus: function(item) {
          if (!item.qty) item.qty = 0;
          else item.qty--;
        }
      }
    });

    var saldo = {!! json_encode($saldo) !!};
    var modalVmSaldo = new Vue({
      el: '#material-alista',
      data: saldo,
      methods: {
        onPlus: function(item) {
          if (item.sisa==null || item.sisa==0){
            item.sisa=0;
          }else{
            item.pemakaian++;
            item.sisa--;
          }
        },
        onMinus: function(item) {
          if (!item.pemakaian){
            item.pemakaian = 0;
          }else{
            item.pemakaian--;
            item.sisa++;
          }
        }
      }
    });

    $('#action').on('keyup keypress', function(){
      if($("#status").val() == 'close')
      {
        $('#act_notify').css({
          'color': 'red'
        })
        var msg = 'Keterangan Action Harus ',
        char =  15 - $(this).val().length,
        words = 10 - countWords($(this).val());

        if(char >= 0 ){
          msg += char + ' Karakter Dan ';
        }

        if(words >= 0 ){
          msg += words + ' Kalimat ';
        }

        if(char <= 0 && words <= 0){
          msg = 'Sudah Benar'
          $('#act_notify').css({
            'color': 'green'
          })

          $('.btn-smpn').removeAttr('disabled');
        }else{
          $('.btn-smpn').attr('disabled', true);
        }

        $('#act_notify').html(msg)
        $(this).focus();
      }
    });

    $('#submit-form').submit(function(event) {
      var result_nte = [];

      ntelist.forEach(function(item) {
        if (item.qty > 0) result_nte.push({id: item.id, qty: item.qty, jenis: item.jenis_nte, kat: item.nte_jenis_kat});
      });

      $('input[name=materialsNte]').val(JSON.stringify(result_nte));

      if($("#status").val() == "close"){
        if($.trim($("#action").val()).length < 15 && countWords($('#action').val()) <= 10 ){
          var msg = 'Keterangan Action Harus ',
          char =  15 - $.trim($("#action").val()).length,
          words = 10 - countWords($('#action').val());

          if(char != 0 ){
            msg += char + ' Karakter Dan ';
          }

          if(words != 0 ){
            msg += words + ' Kalimat ';
          }

          $('#act_notify').html(msg)
          $('#action').focus();
          event.preventDefault();
        }
      }
      var result = [];
      materials.forEach(function(item) {
        if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty});
      });
      var mtr = [];
      var isSaldoAdd = 0;
      saldo.forEach(function(item) {
        if (item.pemakaian > 0) {
          mtr.push(item);
          isSaldoAdd = 1;
        }
      });

        $('input[name=materials]').val(JSON.stringify(result));
        $('input[name=saldo]').val(JSON.stringify(mtr));
        $('.wait-indicator').show();
      });

      $(".submit_decapp").on('click', function(){
      $(this).prop("disabled", true);
      var result_nte_2 = [],
      result_mat = [],
      mtr = [],
      isSaldoAdd = 0;

      ntelist.forEach(function(item) {
        if (item.qty > 0) result_nte_2.push({id: item.id, qty: item.qty, jenis: item.jenis_nte, kat: item.nte_jenis_kat});
      });

      materials.forEach(function(item) {
        if (item.qty > 0) result_mat.push({id_item: item.id_item, qty: item.qty});
      });

      saldo.forEach(function(item) {
        if (item.pemakaian > 0) {
          mtr.push(item);
          isSaldoAdd = 1;
        }
      });

      $.ajax({
        url: "/decpass/"+window.location.href.split('/')[4],
        type: 'POST',
        data: {
          jenis       : $("#jns_decapp").val(),
          komen       : $("#catatan").val(),
          status      : $('#status').val(),
          action      : $('#action').val(),
          count_me    : $('#count_me').val(),
          materialsNte: JSON.stringify(result_nte_2),
          materials   : JSON.stringify(result_mat),
          saldo       : JSON.stringify(mtr)
        },
        dataType: 'JSON'
      }).done(function(data){
        window.location.href = data.url;
      })
    })

    level = {!! json_encode(session('auth')->level) !!};
    data = {!! json_encode($data2) !!};
    if(data.jenis_order==3){
      var status = [{"id":"close", "text":"Close"},{"id":"kendala pelanggan", "text":"Kendala Pelanggan"},{"id":"kendala teknis", "text":"Kendala Teknis"},{"id":"ogp", "text":"OGP"},{"id":"Redaman Sudah Aman", "text":"Redaman Sudah Aman(evidence menyusul)"},{"id":"cancel order", "text":"Cancel Order"}];
    }else{
      var status = [{"id":"close", "text":"Close"},{"id":"kendala pelanggan", "text":"Kendala Pelanggan"},{"id":"kendala teknis", "text":"Kendala Teknis"},{"id":"ogp", "text":"OGP"},{"id":"cancel order", "text":"Cancel Order"}];
    }

    if(data.jenis_order == 6){
      status.push({
        id: 'kendala izin',
        text: 'Kendala Izin'
      },
      {
        id: 'sudah tanam',
        text: 'Sudah Tanam'
      })
    }

    if(data.koordinat_tiang && data.koordinat_tiang.length != 0){
      let data_kor = data.koordinat_tiang.split(';');
      let isi = data_kor.length;
      var html = '';

      if(isi >= 10){
        $('#field_tiang').html("<code style='font-size: 13px;'>Maksimal Tiang Adalah 10 Buah!</code>")
        return false;
      }

      for (let i = 1; i <= isi; i++) {
        html += `<input name='koordinat_tiang[]' type='text' required='required' class='form-control input-sm input_tiangnya' style='margin-top:10px;' placeholder='Masukkan Koordinat Tiang ${i}' value='${data_kor[i - 1]}' />`;
      }

      $('#field_tiang').html(html);
    }

    $('#jml_tiang').on("keypress keyup",function (e){
      let isi = $(this).val();
      var html = '';

      if(parseInt(isi) == 0)
      {
        $(this).val(1)
        isi = 1;
      }

      if(isi >= 10){
        $('#field_tiang').html("<code style='font-size: 13px;'>Maksimal Tiang Adalah 10 Buah!</code>")
        return false;
      }

      for (let i = 1; i <= isi; i++) {
        html += `<input name='koordinat_tiang[]' type='text' class='form-control input-sm input_tiangnya' required='required' style='margin-top:10px;' placeholder='Masukkan Koordinat Tiang ${i}' />`;
      }

      $('#field_tiang').html(html);
    });

    $(document).on('keypress keyup', '.input_tiangnya', function(){
      // if(event.which >= 37 && event.which <= 40) return;
      $(this).val(function(index, value) {
        return value
        .replace(/[a-zA-Z]/g, "")
        ;
      });

      var collect_Dup_tiang = [];

      $.each($('.input_tiangnya'), function(k, v){
        if($(this).val().length != 0){
          collect_Dup_tiang.push({
            isi: $(this).val(),
            data: $(this)
          });
        }
      })

      if(collect_Dup_tiang.length != 0){
        const lookup = collect_Dup_tiang.reduce((a, e) => {
          a[e.isi] = ++a[e.isi] || 0;
          return a;
        }, {});

        const get_dup = collect_Dup_tiang.filter(e => lookup[e.isi]);

        $('.btn-smpn').show();

        $('.input_tiangnya').css({
            'border-color': ''
          });

        $.each(get_dup, function(k, v){
          v.data.css({
            'border-color': 'red'
          });

          $('.btn-smpn').hide();
        })
      }
    })

    var sts = $('#status').select2({
      data: status,
      placeholder: 'Input status'
    });

    var day = {
      format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0
    };

    $("#input-tgl").datepicker(day).on('changeDate', function (e) {
      $(this).datepicker('hide');
    });

    $("#searchinput").keyup(function() {
      setTimeout(function(){
        var input, filter, ul, li, a, i;
        input = document.getElementById("searchinput");
        filter = input.value.toUpperCase();
        ul = document.getElementById("searchlist");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
          a = li[i].getElementsByTagName("strong")[0];
          if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
          } else {
            li[i].style.display = "none";

          }
        }
      }, 1000);
    });

    $("#searchinput_lm").keyup(function() {
      setTimeout(function(){
        var input, filter, ul, li, a, i;
        input = document.getElementById("searchinput_lm");
        filter = input.value.toUpperCase();
        ul = document.getElementById("searchlist_lm");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
          a = li[i].getElementsByTagName("strong")[0];
          if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
          } else {
            li[i].style.display = "none";

          }
        }
      }, 1000);
    });

    $("#searchinput_al").keyup(function() {
      setTimeout(function(){
        var input, filter, ul, li, a, i;
        input = document.getElementById("searchinput_al");
        filter = input.value.toUpperCase();
        ul = document.getElementById("searchlist_lm");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
          a = li[i].getElementsByTagName("strong")[0];
          if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
          } else {
            li[i].style.display = "none";

          }
        }
      }, 1000);
    });

    $('#splitter_jenis').select2();

    var action_cause = {!! json_encode($action_cause) !!};
    var ac = $('#action_cause').select2({
      data: action_cause,
      minimumResultsForSearch: Infinity,
      placeholder: "Pilih action"
    });

    var gamas_cause = {!! json_encode($gamas_cause) !!};
    $('#gamas_cause').select2({
      data: gamas_cause,
      minimumResultsForSearch: Infinity,
      placeholder: "Pilih Penyebab GAMAS"
    });

    var penyebab = {!! json_encode($penyebab) !!};
    var pb = $('#penyebab_id').select2({
      data: penyebab,
      minimumResultsForSearch: Infinity,
      placeholder: "Pilih Penyebab"
    });

    var splitter = [{"id":"1:8", "text":"1:8"},{"id":"1:16", "text":"1:16"},{"id":"1:8+1:2", "text":"1:8+1:2"},{"id":"1:8+1:4", "text":"1:8+1:4"},{"id":"1:8+1:8", "text":"1:8+1:8"},{"id":"1:4+1:4+1:8", "text":"1:4+1:4+1:8"},
    {"id":"1:8+1:8+1:4", "text":"1:8+1:8+1:4"},
    {"id":"1:8+1:8+1:4", "text":"1:2+1:2+1:8"},
    {"id":"1:8+1:8+1:2", "text":"1:8+1:8+1:2"}];
    var spl = $('#splitter').select2({
      data: splitter,
      minimumResultsForSearch: Infinity,
      placeholder: "Pilih Splitter"
    });

    $("#status").change(function(){
      $.getJSON("/getActionCause/"+$("#status").val(), function(data){
        ac.select2({data:data,minimumResultsForSearch: Infinity, placeholder: "Pilih action"});
      });
    });

    $('#speedy').val(function(index, value) {
      return value
      .replace(/\D/g, "");
    });

    $("#speedy, #jml_tiang").on("keypress keyup",function (e){
      $(this).val($(this).val().replace(/\D/g, ''));
      var charCode = (e.which) ? e.which : e.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
    });

    $("#ukur").click(function(){
      var tiket = window.location.href.split('/'),
      pesan,
      get_iboost = async() => {
        try{
          await $.ajax({
            url: "/ibooster/"+$("#speedy").val(),
            type: 'GET',
            dataType: 'JSON'
          }).done(function(data){
            console.log(data)
            if(data[0].ONU_Rx == ''){
              $('.measure').html("<div class='alert alert-warning'>Kosong!</div>");
            }
            $('#ibooster').val(data[0]);
            $('.btn-smpn').removeAttr('disabled');
            if(parseFloat(data[0].ONU_Rx) < -13 && parseFloat(data[0].ONU_Rx) > -24){
              $('.measure').html("<div class='alert alert-info'>Ukuran Ibooster Adalah "+ parseFloat(data[0].ONU_Rx) +" Kategory SPEC</div>");

              status.push({"id": 'Nonaktif', 'text': 'Nonaktif'});
              sts.select2({
                data: status,
                placeholder: 'Input status'
              });
            }else if(parseFloat(data[0].ONU_Rx) >= -13){
              $('.measure').html("<div class='alert alert-warning'>Ukuran Ibooster Adalah "+ parseFloat(data[0].ONU_Rx) +" Kategory UNSPEC P1</div>");
            }else if(parseFloat(data[0].ONU_Rx) <= -24){
              $('.measure').html("<div class='alert alert-warning'>Ukuran Ibooster Adalah "+ parseFloat(data[0].ONU_Rx) +" Kategory UNSPEC P2</div>");
            }else{
              $('.measure').html("<div class='alert alert-danger'>Ukuran Ibooster Adalah "+ parseFloat(data[0].ONU_Rx) +" </div>");
            }
            if(parseFloat(data[0].ONU_Rx) >= -13 || parseFloat(data[0].ONU_Rx) <= 24){
              // data_session.status
            }
          }).fail(function(e){
            $('.measure').html("<div class='alert alert-danger'>Terjadi Error, Harap CEK kembali ukuran Jika Bersikeras Submit!</div>");
            $('.btn-smpn').removeAttr('disabled');
          });
          pesan = 'ok';
        }
        catch(e){
          console.log(e);
          pesan = 'gagal';
        }
        return pesan;
      }

      get_iboost().then((msg) =>{
        $.ajax({
          url: "/check_umur_inet",
          type: 'GET',
          data: {
            inet: $("#speedy").val(),
            id: tiket[4]
          },
          dataType: 'JSON'
        }).done(function(data){
          // if(data == 200){
          //   $('.btn-smpn').removeAttr('disabled');

          //   $('.umur').html("<div class='alert alert-info'>Nomor Internet Bisa Digunakan Bulan Ini!</div>");
          // }else{
          //   $('.umur').html("<div class='alert alert-danger'>Nomor Internet Terdeteksi Digunakan Bulan Ini, Hubungi HD untuk Cek Data!</div>");
          // }
          $('.btn-smpn').removeAttr('disabled');

          $('.umur').html("<div class='alert alert-info'>Nomor Internet Bisa Digunakan Bulan Ini!</div>");
        })
      }).catch((e) =>
        console.log(e)
      );
      // $.getJSON("/ibooster/"+$("#speedy").val(), function(data){
      //   $('.measure').html("<div class='alert alert-info'>Ukuran Ibooster Adalah "+ data[0].ONU_Rx +"</div>");
      //   // $('.input_remo').show();
      //   console.log(data[0])
      //   $('#ibooster').val(data[0]);
      //   $('.btn-smpn').attr('disabled', true);
      //   if(data[0].ONU_Rx > 2 || data[0].ONU_Rx < -24){
      //     console.log("hapus close");
      //     // sts.select2({data: [{"id":"kendala pelanggan", "text":"Kendala Pelanggan"},{"id":"kendala teknis", "text":"Kendala Teknis"}]});
      //   }else{
      //     console.log("nonaktif");
      //     status.push({"id": 'Nonaktif', 'text': 'Nonaktif'});
      //     console.log(status)
      //     sts.select2({
      //       data: status,
      //       placeholder: 'Input status'
      //     });
      //   }
      //   // $('.wait-indicator').hide();
      // });
    });
  });
</script>

@endsection