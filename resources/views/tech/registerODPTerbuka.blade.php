@extends('layout2')
@section('head')
    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css" />
@endsection
@section('content')
    <?php
        $auth = (session('auth'));
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            INPUT ORDER ODP TERBUKA
        </div>
        <div class="panel-body">
            <div class="form col-sm-8 col-md-8">
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <input type="hidden" name="jenis_order" value="19" />
                <div class="form-group">
                    <label for="tipe" class="col-sm-4 col-md-3 control-label">Nama ODP</label>
                    <div class="col-sm-8">
                        <input name="nama_odp" type="text" id="nama_odp" class="form-control" value="{{ old('nama_odp') }}"/>
                    </div>
                </div>
                <div class="form-group {{ $errors->has('koordinat') ? 'has-error' : '' }}">
                    <label for="" class="col-sm-4 col-md-3 control-label">Koordinat</label>
                    <div class="col-sm-8">
                        <input name="koordinat" id="input-koordinat" class="form-control" rows="1" value="{{ old('koordinat') }}" />
                        @foreach($errors->get('koordinat') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group {{ $errors->has('sto') ? 'has-error' : '' }}">
                    <label for="sto" class="col-sm-4 col-md-3 control-label">STO</label>
                    <div class="col-sm-8">
                        <input name="sto" type="text" id="sto" class="form-control" value="{{ old('sto') }}"/>
                        @foreach($errors->get('sto') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group {{ $errors->has('regu') ? 'has-error' : '' }}">
                    <label for="regu" class="col-sm-4 col-md-3 control-label">Order ke Tim</label>
                    <div class="col-sm-8">
                        <input name="regu" type="text" id="regu" class="form-control" value="{{ old('regu') }}"/>
                        @foreach($errors->get('regu') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-3 col-sm-3">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
    <script>
        $(function() {
            var data = <?= json_encode($sto) ?>;
            var combo = $('#sto').select2({
                data: data,
                placeholder : 'Pilih STO'
            });
            var data = <?= json_encode($regu) ?>;
            var regu = $('#regu').select2({
                data: data,
                placeholder : 'Pilih Regu',
                allowClear: true
            });
            $("#nama_odp").inputmask("AAA-AAA-A{2,3}/9{3,4}");
        });
    </script>
@endsection