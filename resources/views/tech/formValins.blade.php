@extends('layout2')
@section('content')
<style>
  .btn strong.glyphicon {
    opacity: 0;
  }
  .btn.active strong.glyphicon {
    opacity: 1;
  }
</style>
@include('partial.alert')
<div class="panel">
  <div class="panel-body">
    <code class="validate_no_koor_odp" style="display: none;">*Format Koordinat Salah, Contoh Yang Benar  -3.092600, 115.283800 </code>
    <form id="submit-form" method="post" autocomplete="off">
      <h3>
        <a href="/tech/{{ Request::segment(2) }}" class="btn btn-sm btn-default">
          <span class="fa fa-arrow-left"></span>
        </a>
        Input Valins
      </h3>
      @if($data->jenis_order == 17)
        <div class="form-group">
          <label for="jenis_valins" class="control-label">Jenis Valins</label>
          <select class="form-control" id="jenis_valins" name="jenis_valins">
            <option value="sebelum">ID Valins Gendong Sebelum</option>
            <option value="sesudah">ID Valins Gendong Sesudah</option>
            <option value="reboundary">ID Valins ODP Reboundary</option>
          </select>
        </div>
      @endif
      <div class="form-group">
        <label for="valins_id">ID VALINS</label>
        <input name="valins_id" type="text" id="valins_id" class="form-control" value="0" placeholder="isi id valins" >
      </div>
      <div class="form-group">
        <label for="kapasitas">Kapasiitas ODP</label>
        <input name="kapasitas" type="text" id="kapasitas" class="form-control" value="0" placeholder="isi kapasitas" >
      </div>
      <div class="form-group">
        <label for="odp_koor">Lokasi ODP</label>
        <input name="odp_koor" type="text" id="odp_koor" class="form-control" value="0" placeholder="isi Koordinat ODP" >
      </div>
      <div class="form-group">
        <label for="barcode_kode">Nomor Barcode</label>
        <input name="barcode_kode" type="text" id="barcode_kode" class="form-control" value="0" placeholder="isi Nomor Barcode" >
      </div>
      <div class="form-group">
        <label for="koor_pelanggan">Lokasi Pelanggan</label>
        <input name="koor_pelanggan" type="text" id="koor_pelanggan" class="form-control" value="0" placeholder="isi Koordinat Pelanggan" >
      </div>
      <button type="submit" class="btn btn-primary">Submit!</button>
    </form>
  </div>
</div>
@if(count($valins))
<div class="panel panel-default" id="info">
  <div id="fixed-table-container-demo" class="fixed-table-container table-responsive">
    <table class="table table-bordered table-fixed">
      <thead>
        <tr>
          <th class="head">ID</th>
          <th class="head">ODP</th>
          <th class="head">Koordinat ODP</th>
          <th class="head">Koordinat Pelanggan</th>
          <th class="head">Jenis</th>
          <th class="head">Port</th>
          <th class="head">No. Layanan</th>
          <th class="head">QRCODE ODP</th>
          <th class="head">QRCODE DC</th>
          <th class="head">SN ONU</th>
        </tr>
      </thead>
      <tbody>
        @foreach($valins as $no => $d)
        <tr>
          <td>{{ $d->VALINS_ID }}</td>
          <td>{{ $d->Nama_ODP }}</td>
          <td>{{ $d->koordinat_odp }}</td>
          <td>{{ $d->koor_pelanggan }}</td>
          <td>{{ $d->Jenis_mv }}</td>
          <td>{{ $d->Port_ODP }}</td>
          <td>{{ $d->SIP_1 }} / {{ $d->Inet }}</td>
          <td>{{ $d->QR_Code_ODP }}</td>
          <td>{{ $d->QR_Code_Dropcore }}</td>
          <td>{{ $d->ONU_SN }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endif
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="/bower_components/select2/select2.min.js"></script>
<script>
  $(function() {
    $('#jenis_valins').select2({
      width: '100%'
    });

    $('#submit-form').submit(function(e){
      var regex =/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)[,°]\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?°?)$/;

      if(!regex.test($.trim($('#odp_koor').val())) && $.trim($('#odp_koor').val()) !== ''){
        $('.validate_no_koor_odp').css({
          "display": "block",
        });

        $('#odp_koor').css({
          border: "2px solid red",
        });

        $('#odp_koor').focus();
        e.preventDefault();
      }else{
        $('.validate_no_koor_odp').css({display: "none"});
        $('#odp_koor').css({border: ""})
      }
    });
  });
</script>
@endsection