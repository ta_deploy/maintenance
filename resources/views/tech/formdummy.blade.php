  @extends('layout')

@section('content')
  @include('partial.alert')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <h3>
      <a href="/maintaince/status/np" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      Input Ticket Dummy
    </h3>
    <div class="form-group">
      <label class="control-label" for="input-tiket">No Tikcet</label>
      <input name="tiket" type="text" id="input-tiket" class="form-control" value="{{ $data->no_tiket or '' }}" />
    </div>
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>
@endsection
