@extends('layout2')
@section('head')
@endsection
@section('content')
<?php
	$auth = session('auth');
?>
<div class="panel">
  <div class="panel-heading">
    <span class="panel-title">List Absensi</span>
  </div>
  @foreach($data as $d)
    <div class="widget-support-tickets-item">
      <span class="label {{$d->count == 2?'label-success':'label-info'}} pull-right">{{ $d->count == 2?'Approved':'Wait For Approve' }}</span>
      <a href="#" title="" class="widget-support-tickets-title">
        {{ date('d M Y', strtotime($d->created_at)) }}
        <!-- <span class="widget-support-tickets-id">[#201798]</span> -->
      </a>
      <span class="widget-support-tickets-info">Pada jam <a href="#" title="">{{ substr($d->created_at,11) }}</a></span>
    </div>
  @endforeach
</div>
@endsection
@section('script')
@endsection