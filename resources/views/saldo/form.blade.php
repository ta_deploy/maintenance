@extends('tech')

@section('head')
    <link rel="stylesheet" href="/bower_components/select2/select2.css" />
    <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css" />
@endsection

@section('content')
    <?php
        $auth = (session('auth'));
        if($idregu){
            $idr = $idregu->id;
        }else{
            $idr = null;
        }
    ?>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            INPUT SALDO RFC/RFR
        </div>
        <div class="panel-body">
            <div class="form col-sm-8 col-md-8">
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="txtKode" class="col-sm-4 col-md-3 control-label">RFC</label>
                    <div class="col-sm-8">
                        <input name="rfc" type="text" id="txtKode" class="form-control" placeholder="No RFC" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tipe" class="col-sm-4 col-md-3 control-label">Status</label>
                    <div class="col-sm-8">
                        <select name="status" id="nama_odp" class="form-control">
                            <option value="1">Insert Saldo</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="regu" class="col-sm-4 col-md-3 control-label">Saldo Ke Regu</label>
                    <div class="col-sm-8">
                        <input name="regu" type="text" id="regu" class="form-control input-lg" value="{{ old('regu') ?: $idr }}" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-3 col-sm-3">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="/bower_components/select2/select2.min.js"></script>
    <script>
        $(function() {
            var data = <?= json_encode($regu) ?>;
            var regu = $('#regu').select2({
                data: data,
                placeholder:"Select Regu",
                templateSelection: function(data) {
                    return  '<strong style="margin-left:5px"> '+data.text+' </strong> '+data.nik1+'</span>';
                },
                escapeMarkup: function(m) {
                    return m;
                }
            });
        });
    </script>
@endsection