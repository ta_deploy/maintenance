<!doctype html>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/bootswatch-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/bower_components/bootswatch-dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/style.css"/>
    <style type="text/css">
        .input-photos img {
          width: 100px;
          height: 150px;
          margin-bottom: 5px;
        }
    </style>
    <title>MARINA</title>

    @yield('head')
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a href="/" class="navbar-brand">
                <span class="glyphicon glyphicon-cog"></span>
                <span>MARINA</span>
            </a>
        </div>
        
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?php
                    $lvl = session('auth')->maintenance_level;
                ?>
                @if($lvl != 6 && $lvl != 0 )
                <!-- <li class="{{ Request::segment(1) == 'nossa' ? 'active' : '' }}">
                    <a href="/nossa">Nossa</a>
                </li> -->
                <li class="dropdown {{ Request::segment(1) == 'order' ? 'active' : '' }}">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Order<span class="badge text-danger">
                        ?
                    </span></a>
                  <ul class="dropdown-menu">
                    <li><a href="/order/none/none/{{ date('Y-m') }}/all?q=14894226">Search</a></li>
                    <li><a href="/order/input">Register</a></li>
                    <li><a href="/listtommanpsb/24">ODP FULL(tPSB)</a></li>
                    <li><a href="/listtommanpsb/2">ODP LOSS(tPSB)</a></li>
                    <li><a href="/listtommanpsb/11">INSERT TIANG(tPSB)</a></li>
                    <li><a href="/order/none/none/{{date('Y-m')}}/all">Matrix</a></li>
                  </ul>
                </li>
                <li class="dropdown {{ Request::segment(1) == 'listverify' ? 'active' : '' }}">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Verifikasi<span class="badge text-danger">
                        ?
                    </span></a>
                  <ul class="dropdown-menu">
                    <li><a href="/listverify/all">WO</a></li>
                    <li><a href="/tech">Absensi</a></li>
                    <li><a href="/verifRemo">WO REMO( HD )</a></li>
                  </ul>
                </li>
                @endif
                @if($lvl == 1 || $lvl == 2 || $lvl == 3 || $lvl == 4)
                <li class="dropdown {{ Request::segment(1) == 'repair' ? 'active' : '' }}">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Repair ALPRO<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="/repair/input">Input Usulan</a></li>
                    <li><a href="/repair/matrik">Matrik</a></li>
                    <li><a href="/repair/approval">Approval</a></li>
                    <li><a href="/repair/dispatch">Dispatch</a></li>
                  </ul>
                </li>
                @endif
                <li class="dropdown {{ Request::segment(1) == 'reportOdpLoss' ? 'active' : '' }}">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Report<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    @if($lvl == 1 || $lvl == 2 || $lvl == 3 || $lvl == 4)
                    <li><a href="/reportUmur/{{date('Y-m-d')}}/3">Umur Odp Loss</a></li>
                    <li><a href="/reportUmur/{{date('Y-m-d')}}/5">Umur Utilitas</a></li>
                    <li><a href="/reportUmur/{{date('Y-m-d')}}/6">Umur Insert Tiang</a></li>
                    <li><a href="/verify/{{date('Y-m')}}">Dashboard Verifikasi</a></li>
                    <li><a href="/reportAbsen/{{date('Y-m-d')}}">Absen</a></li>
                    @endif
                    <li><a href="/produktifitas/{{date('Y-m')}}">Rekap Closing</a></li>
                    <li><a href="/download">Download</a></li>
                  </ul>
                </li>
                <li class="dropdown {{ (Request::segment(1) == 'dashboard') ? 'active' : '' }}">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Revenue<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    @if ($lvl == 1)
                        <!--<li><a href="/dashboard/revenue/MITRA/{{date('Y-m')}}">Mitra</a></li>
                        <li><a href="/dashboard/revenue/TA-TELKOM/{{date('Y-m')}}">TA-Telkom</a></li>
                        <li><a href="/revenueTelkom/{{date('Y-m')}}">TA-Telkom</a>-->
                        <li><a href="/revenue/program/{{date('Y-m')}}">Per Program</a></li>
                    @endif
                    @if ($lvl != 4)
                    <li><a href="/revenuev2/MITRA/{{date('Y-m')}}">Rev-V2(Mitra)</a></li>
                    @endif
                    @if ($lvl == 1 || $lvl == 4)
                        <li><a href="/revenuev2/TELKOM/{{date('Y-m')}}">Rev-V2(Telkom)</a></li>
                    @endif
                  </ul>
                </li>
                <li class="dropdown {{ Request::segment(1) == 'matrix' ? 'active' : '' }}">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Matrix<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="/matrix/{{date('Y-m-d')}}/0">ALL</a></li>
                    <li><a href="/matrix/{{date('Y-m-d')}}/1">Benjar</a></li>
                    <li><a href="/matrix/{{date('Y-m-d')}}/2">Gamas</a></li>
                    <li><a href="/matrix/{{date('Y-m-d')}}/3">Odp Loss</a></li>
                    <li><a href="/matrix/{{date('Y-m-d')}}/4">Remo</a></li>
                    <li><a href="/matrix/{{date('Y-m-d')}}/5">Utilitas</a></li>
                    <li><a href="/matrix/{{date('Y-m-d')}}/6">Insert Tiang</a></li>
                    <li><a href="/matrix/{{date('Y-m-d')}}/7">Revitalisasi FTM</a></li>
                    <li><a href="/matrix/{{date('Y-m-d')}}/8">ODP SEHAT</a></li>
                    <li><a href="/matrix/{{date('Y-m-d')}}/9">ODP Full</a></li>
                    <li><a href="/matrix/{{date('Y-m-d')}}/10">Validasi ODP</a></li>
                    <li><a href="/matrix/{{date('Y-m-d')}}/11">Project Pihak Ke3</a></li>
                    <li><a href="/dptiang/{{ date('Y-m') }}">DELTA C</a></li>
                  </ul>
                </li>
                @if ($lvl == 1)
                    <li class="dropdown {{ (Request::path() == 'user' or Request::path() == 'regu') ? 'active' : '' }}">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">User&Regu<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="/user">User</a></li>
                        <li><a href="/regu">Regu</a></li>
                      </ul>
                    </li>
                @endif
                @if($lvl != 6 && $lvl != 0 )
                <li class="dropdown {{ Request::segment(1) == 'setting' ? 'active' : '' }}">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Setting<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="/khs">KHS</a></li>
                    <li><a href="/setting/1">Benjar</a></li>
                    <li><a href="/setting/2">Gamas</a></li>
                    <li><a href="/setting/3">Odp Loss</a></li>
                    <li><a href="/setting/4">Remo</a></li>
                    <li><a href="/setting/5">Utilitas</a></li>
                    <li><a href="/setting/6">Insert Tiang</a></li>
                    <li><a href="/setting/7">Revitalisasi FTM</a></li>
                    <li><a href="/setting/8">Benjar ODP</a></li>
                    <li><a href="/setting/9">ODP Full</a></li>
                    <li><a href="/setting/10">Validasi ODP</a></li>
                    <li><a href="/setting/11">Project Pihak Ke3</a></li>
                  </ul>
                </li>
                <li class="dropdown {{ Request::segment(1) == 'logistik' ? 'active' : '' }}">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tools<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="/logistik">Alista</a></li>
                    <li><a href="/stok">Alista Stok</a></li>
                  </ul>
                </li>
                @endif
                <!--
                <li class="{{ Request::path() == 'top10' ? 'active' : '' }}">
                    <a href="#">
                        Report
                    </a>
                </li>
                -->
                @if($lvl == 1 || $lvl == 2 || $lvl == 3 || $lvl == 4)
                <li class="{{ Request::segment(1) == 'saldo' ? 'active' : '' }}">
                    <a href="/saldo">Saldo Material</a>
                </li>
                @endif
                <li class="dropdown {{ (Request::path() == 'qc' or Request::path() == 'foto') ? 'active' : '' }}">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Foto<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="/qc/8/{{date('Y-m-d')}}">ODP Sehat</a></li>
                  </ul>
                </li>
                <li>
                    <a href="/logout">Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    @include('partial.alert')

    @yield('content')
</div>
<!--
<div class="footer">
    <a href="/logout" style="margin-top:10px" class="btn btn-sm btn-warning pull-right">Logout</a>
   
    <strong>[{{ session('auth')->id_user }}] {{ session('auth')->nama }}</strong>
</div>
-->
<div class="wait-indicator">
    <span class="glyphicon glyphicon-refresh gly-spin"></span>
</div>
@yield('script')


<script>
    $('form').submit(function () {
        $('.wait-indicator').show()
    })
</script>
</body>
</html>