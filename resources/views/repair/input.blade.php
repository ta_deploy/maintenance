@extends('layout2')
@section('head')
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Input Usulan Baru</span>
</h1>
@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-group {{ $errors->has('jenis_alpro') ? 'has-error' : '' }}">
                    <label for="jenis_alpro" class="col-sm-3 col-md-2 control-label">Jenis Alpro</label>
                    <div class="col-sm-5">
                        <input name="jenis_alpro" type="text" id="jenis_alpro" class="form-control" value="{{ old('jenis_alpro') ?: @$data->jenis_alpro }}">
                        @foreach($errors->get('jenis_alpro') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group {{ $errors->has('nama_alpro') ? 'has-error' : '' }}">
                    <label for="nama_alpro" class="col-sm-3 col-md-2 control-label">Nama Alpro</label>
                    <div class="col-sm-5">
                        <input name="nama_alpro" type="text" id="nama_alpro" class="form-control" value="{{ old('nama_alpro') ?: @$data->nama_alpro }}">
                        @foreach($errors->get('nama_alpro') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group {{ $errors->has('rab') ? 'has-error' : '' }}">
                    <label for="rab" class="col-sm-3 col-md-2 control-label">RAB</label>
                    <div class="col-sm-5">
                        <input name="rab" type="text" id="rab" class="form-control" value="{{ old('rab') ?: @$data->rab }}">
                        @foreach($errors->get('rab') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group {{ $errors->has('catatan') ? 'has-error' : '' }}">
                    <label for="catatan" class="col-sm-3 col-md-2 control-label">Catatan</label>
                    <div class="col-sm-5">
                        <textarea name="catatan" id="catatan" class="form-control">{{ old('catatan') ?: @$data->catatan }}</textarea>
                        @foreach($errors->get('catatan') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>

                <div class="form-group {{ $errors->has('boq') ? 'has-error' : '' }}">
                    <label for="boq" class="col-sm-3 col-md-2 control-label">BOQ</label>
                    <div class="col-sm-5">
                        <input name="boq" type="file" id="boq" class="form-control">
                        @foreach($errors->get('boq') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group {{ $errors->has('kml') ? 'has-error' : '' }}">
                    <label for="kml" class="col-sm-3 col-md-2 control-label">KML</label>
                    <div class="col-sm-5">
                        <input name="kml" type="file" id="kml" class="form-control">
                        @foreach($errors->get('kml') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group {{ $errors->has('otdr') ? 'has-error' : '' }}">
                    <label for="otdr" class="col-sm-3 col-md-2 control-label">OTDR</label>
                    <div class="col-sm-5">
                        <input name="otdr" type="file" id="otdr" class="form-control">
                        @foreach($errors->get('otdr') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(function() {
            var jenis_alpro = [{id:1,text:"Feeder"},{id:2,text:"Distribusi"},{id:3,text:"ODC"},{id:4,text:"ODP"},{id:5,text:"FTM"}];
            $('#jenis_alpro').select2({
                data: jenis_alpro,
                placeholder: 'Pilih Alpro',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });
        });
    </script>
@endsection