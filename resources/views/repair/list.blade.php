@extends('layout2')
@section('head')
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>List {{Request::segment(2)}}</span>
</h1>
@endsection
@section('content')
<div id="modalLogs" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #eee">
          <strong style="color:black;">Logs Update</strong>
          <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
        </div>
        <div class="modal-body" style="overflow-y:auto;height : 450px">
            
        </div>
        <div class="modal-footer" style="background: #eee">
          
        </div>
      </div>
    </div>
</div>
<div class="panel panel-default" id="info">
    <div id="fixed-table-container-demo" class="fixed-table-container">
        <table class="table table-bordered table-fixed">
            <thead>
            <tr>
                <th>#</th>
                <th>Jenis ALPRO</th>
                <th>Nama ALPRO</th>
                <th>RAB</th>
                <th>Status</th>
                <th>Catatan</th>
                <th>Files</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach($list as $no => $d)
                <?php
                    $ja = [1=>"Feeder","Distribusi","ODC","ODP","FTM"];
                    $href="/upload/repair/".$d->id."/";
                    $boq="/upload/repair/".$d->id."/".$d->boq;
                    $kml="/upload/repair/".$d->id."/".$d->kml;
                    $otdr="/upload/repair/".$d->id."/".$d->otdr;
                ?>
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ @$ja[$d->jenis_alpro] }}</td>
                    <td>{{ $d->nama_alpro }}</td>
                    <td>{{ $d->rab }}</td>
                    <td>{{ $d->status }}</td>
                    <td>{{ $d->catatan }}</td>
                    <td>
                        {!! $d->kml?'<span class="label label-info"><a href="'.$kml.'">kml</a></span>':'' !!}
                        {!! $d->boq?'<span class="label label-info"><a href="'.$boq.'">boq</a></span>':'' !!}
                        {!! $d->otdr?'<span class="label label-info"><a href="'.$otdr.'">otdr</a></span>':'' !!}
                    </td>
                    <td>
                        <a href="#" class="btn btn-info btn-xs logs" data-idlog="{{ $d->id }}">
                            <span class="glyphicon glyphicon-list-alt"></span>
                            <span>Logs</span>
                        </a>
                        @if(Request::segment(2)=='listusulanteknisi')
                            <a href="/repair/{{ $d->id }}" class="btn btn-primary btn-xs">
                                <span class="glyphicon glyphicon-pencil"></span>
                                <span>Update</span>
                            </a>
                        @endif
                        @if(Request::segment(2)=='approval')
                            <a href="/repair/approval/{{ $d->id }}" class="btn btn-primary btn-xs">
                                <span class="glyphicon glyphicon-pencil"></span>
                                <span>Update</span>
                            </a>
                        @endif
                        @if(Request::segment(2)=='dispatch')
                            <a href="/repair/dispatch/{{ $d->id }}" class="btn btn-primary btn-xs">
                                <span class="glyphicon glyphicon-send"></span>
                                <span>Dispatch</span>
                            </a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function(){
        $(".logs").click(function(e){
            var id = $(this).data('idlog');
            var url = "/repair/getLogs/"+id;
            $.getJSON(url, function(r){
                var html = '<table class="table table-bordered table-fixed"><thead><tr><th>#</th><th>Tgl</th><th>Note</th><th>By</th></tr></thead><tbody>';
                $.each( r, function( key, val ) {
                    var no=key+1;
                    html+= '<tr><td>'+no+'</td><td>'+val.update_at+'</td><td>'+val.catatan+'</td><td>'+val.update_by+'</td></tr>';
                });
                html+='</tbody></table>';
                $(".modal-body").html(html);
                $("#modalLogs").modal('show');
            });
        });
    });
</script>
@endsection