@extends('layout2')
@section('head')
@endsection

@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Matrik</span>
</h1>
@endsection
@section('content')

<a href="/repair/input" class="btn btn-info btn-xs" style="margin: 0 0 5px 0;">
        <span class="glyphicon glyphicon-plus"></span>
        <span>Input Baru</span>
    </a>
    <div class="panel panel-default" id="info">
        <div id="fixed-table-container-demo" class="fixed-table-container">
            <table class="table table-bordered table-fixed">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Jenis ALPRO</th>
                    <th>Registered</th>
                    <th>Disetujui</th>
                    <th>Ditolak</th>
                    <th>In-Tech</th>
                    <th>Pending</th>
                    <th>Selesai</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($list as $no => $d)
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $d->jenis_alpro }}</td>
                        <td><a href="/repair/matrik/{{ $d->id }}/registered">{{ $d->registered }}</a></td>
                        <td><a href="/repair/matrik/{{ $d->id }}/disetujui">{{ $d->disetujui }}</a></td>
                        <td><a href="/repair/matrik/{{ $d->id }}/ditolak">{{ $d->ditolak }}</a></td>
                        <td><a href="/repair/matrik/{{ $d->id }}/in-tech">{{ $d->intech }}</a></td>
                        <td><a href="/repair/matrik/{{ $d->id }}/pending">{{ $d->pending }}</a></td>
                        <td><a href="/repair/matrik/{{ $d->id }}/selesai">{{ $d->selesai }}</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('script')
@endsection