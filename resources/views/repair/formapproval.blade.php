@extends('layout2')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Update Approval</span>
</h1>
@endsection
@section('head')
@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="post" class="form-horizontal">
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label for="status" class="col-sm-3 col-md-2 control-label">Status</label>
                    <div class="col-sm-5">
                        <input name="status" type="text" id="status" class="form-control">
                        @foreach($errors->get('status') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group {{ $errors->has('jenis_alpro') ? 'has-error' : '' }}">
                    <label for="jenis_alpro" class="col-sm-3 col-md-2 control-label">Jenis Alpro</label>
                    <div class="col-sm-5">
                        <input name="jenis_alpro" type="text" id="jenis_alpro" class="form-control" value="{{ old('jenis_alpro') ?: @$data->jenis_alpro }}" readonly>
                        @foreach($errors->get('jenis_alpro') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group {{ $errors->has('nama_alpro') ? 'has-error' : '' }}">
                    <label for="nama_alpro" class="col-sm-3 col-md-2 control-label">Nama Alpro</label>
                    <div class="col-sm-5">
                        <input name="nama_alpro" type="text" id="nama_alpro" class="form-control" value="{{ old('nama_alpro') ?: @$data->nama_alpro }}" readonly>
                        @foreach($errors->get('nama_alpro') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group {{ $errors->has('rab') ? 'has-error' : '' }}">
                    <label for="rab" class="col-sm-3 col-md-2 control-label">RAB</label>
                    <div class="col-sm-5">
                        <input name="rab" type="text" id="rab" class="form-control" value="{{ old('rab') ?: @$data->rab }}" readonly>
                        @foreach($errors->get('rab') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group {{ $errors->has('catatan') ? 'has-error' : '' }}">
                    <label for="catatan" class="col-sm-3 col-md-2 control-label">Catatan</label>
                    <div class="col-sm-5">
                        <textarea name="catatan" id="catatan" class="form-control">{{ old('catatan') }}</textarea>
                        @foreach($errors->get('catatan') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>

                
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(function() {
            var jenis_alpro = [{id:1,text:"Feeder"},{id:2,text:"Distribusi"},{id:3,text:"ODC"},{id:4,text:"ODP"},{id:5,text:"FTM"}];
            $('#jenis_alpro').select2({
                data: jenis_alpro,
                placeholder: 'Pilih Alpro',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });
            var status = [{id:"disetujui",text:"Setuju"},{id:"ditolak",text:"Tolak"}];
            $('#status').select2({
                data: status,
                placeholder: 'Pilih Status',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });
        });
    </script>
@endsection