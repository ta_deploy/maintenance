@extends('layout2')
@section('head')
@endsection

@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>@if (isset($gembok))
        Edit Regu {{ $regu->nama_regu }}
        @else
        Input Regu Baru
        @endif</span>
</h1>
@endsection
@section('content')

@if (session('auth')->maintenance_level == '1')
<div class="panel panel-default">
    <div class="panel-body">
        <form method="post" class="form-horizontal">
            <input name="hid" type="hidden" value="{{ @$regu->id_regu }}"/>
            <div class="form-group">
                <label for="label_regu" class="col-sm-3 col-md-2 control-label">Label Regu</label>
                <div class="col-sm-5">
                    <input name="label_regu" type="text" id="label_regu" class="form-control" value="{{ $regu->label_regu or '' }}" readonly="">
                </div>
            </div>
            <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
                <label for="nama" class="col-sm-3 col-md-2 control-label">Nama Regu</label>
                <div class="col-sm-5">
                    <input name="nama_regu" type="text" id="nama" class="form-control" value="{{ $regu->uraian or old('nama_regu') }}">
                    @foreach($errors->get('nama_regu') as $msg)
                        <small class="form-message dark">{{ $msg }}</small>
                    @endforeach
                </div>
            </div>

            <div class="form-group {{ $errors->has('mitra') ? 'has-error' : '' }}">
                <label for="mitra" class="col-sm-3 col-md-2 control-label">Mitra</label>
                <div class="col-sm-5">
                    <input name="mitra" type="text" id="mitra" class="form-control" value="{{ $regu->nama_mitra or old('mitra') }}">
                    @foreach($errors->get('mitra') as $msg)
                        <small class="form-message dark">{{ $msg }}</small>
                    @endforeach
                </div>
            </div>

            <div class="form-group {{ $errors->has('spesialis') ? 'has-error' : '' }}">
                <label for="spesialis" class="col-sm-3 col-md-2 control-label">Pekerjaan</label>
                <div class="col-sm-2">
                    <input name="spesialis" type="text" id="spesialis" class="form-control" value="{{ $regu->spesialis or old('spesialis') }}">
                     @foreach($errors->get('spesialis') as $msg)
                        <small class="form-message dark">{{ $msg }}</small>
                    @endforeach
                </div>
            </div>
            <div class="form-group {{ $errors->has('label_sektor') ? 'has-error' : '' }}">
                <label for="label_sektor" class="col-sm-3 col-md-2 control-label">Sektor</label>
                <div class="col-sm-5">
                    <input name="label_sektor" type="text" id="label_sektor" class="form-control" value="{{ $regu->label_sektor or old('label_sektor') }}">
                    @foreach($errors->get('label_sektor') as $msg)
                        <small class="form-message dark">{{ $msg }}</small>
                    @endforeach
                </div>
            </div>
            @if($regu)
                <div class="form-group {{ $errors->has('nomor_urut') ? 'has-error' : '' }}">
                    <label for="nomor_urut" class="col-sm-3 col-md-2 control-label">Nomor Urut</label>
                    <div class="col-sm-2">
                        <input name="nomor_urut" type="text" id="nomor_urut" class="form-control"
                            value="{{ $regu->nomor_urut or old('nomor_urut') }}">
                        @foreach($errors->get('nomor_urut') as $msg)
                        <small class="form-message dark">{{ $msg }}</small>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="form-group {{ $errors->has('nik1') ? 'has-error' : '' }}">
                <label for="nik1" class="col-sm-3 col-md-2 control-label">NIK1</label>
                <div class="col-sm-3">
                    <select id="nik1" name="nik1" class="form-control cari_nik">
                    </select>
                    @foreach($errors->get('nik1') as $msg)
                        <small class="form-message dark">{{ $msg }}</small>
                    @endforeach
                </div>
            </div>
            <div class="form-group {{ $errors->has('nik2') ? 'has-error' : '' }}">
                <label for="nik2" class="col-sm-3 col-md-2 control-label">NIK2</label>
                <div class="col-sm-3">
                    <select id="nik2" name="nik2" class="form-control cari_nik">
                    </select>
                </div>
            </div>
            <div class="form-group {{ $errors->has('niktl') ? 'has-error' : '' }}">
                <label for="niktl" class="col-sm-3 col-md-2 control-label">NIK TL</label>
                <div class="col-sm-3">
                    <select id="niktl" name="niktl" class="form-control cari_nik_tl">
                    </select>
                    @foreach($errors->get('niktl') as $msg)
                        <small class="form-message dark">{{ $msg }}</small>
                    @endforeach
                </div>
            </div>
            <div class="form-group {{ $errors->has('chat_id') ? 'has-error' : '' }}">
                <label for="chat_id" class="col-sm-3 col-md-2 control-label">GRUP</label>
                <div class="col-sm-5">
                    <input name="chat_id" type="text" id="chat_id" class="form-control"
                    value="{{ old('chat_id') ?: @$regu->mainsector_marina }}"/>
                    @foreach($errors->get('chat_id') as $msg)
                        <small class="form-message dark">{{ $msg }}</small>
                    @endforeach
                </div>
            </div>
            <div class="form-group {{ $errors->has('status_regu') ? 'has-error' : '' }}">
                <label for="stat" class="col-sm-3 col-md-2 control-label">Status</label>
                <div class="col-sm-5">
                    <select class="form-control input-sm" id="stat" name="status_regu">
                        <option value="1" {{ @$regu->ACTIVE=="1" ? "selected" : "" }}>ACTIVE</option>
                        <option value="0" {{ @$regu->ACTIVE=="0" ? "selected" : "" }}>NON-ACTIVE</option>
                    </select>
                    @foreach($errors->get('chat_id') as $msg)
                        <small class="form-message dark">{{ $msg }}</small>
                    @endforeach
                </div>
            </div>
            <div class="form-group">
                <label for="kerjaan" class="col-sm-3 col-md-2 control-label">Status Pekerjaan</label>
                <div class="col-sm-5">
                    <select class="form-control input-sm" id="kerjaan" name="kerjaan">
                        <option value="1" {{ @$regu->pekerjaan=="1" ? "selected" : "" }}>IOAN</option>
                        <option value="0" {{ @$regu->pekerjaan=="0" ? "selected" : "" }}>NON-IOAN</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                    <button class="btn btn-primary" type="submit">
                        <span class="glyphicon glyphicon-floppy-disk"></span>
                        <span>Simpan</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endif
@endsection
@section('script')
<script>
    $(function() {
        var msg_select2;
        $('.cari_nik').select2({
            placeholder:"Masukkan NIK Karyawan",
            allowClear: true,
            minimumInputLength: 6,
            ajax: {
                url: "/ajax/regu_search",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term,
                        jenis: 'teknisi'
                    };
                },
                processResults: function (response) {
                    msg_select2 = response.ada;
                    console.log(response, response.kosong)
                    return {
                        results: response.kosong
                    };
                },
                cache: true,
                success: function(value) {
                    // callback(value);
                },
            },
            language: {
                noResults: function() {
                    return msg_select2;
                },
            },
            escapeMarkup: function(markup) {
                return markup;
            },
        });

        $('.cari_nik_tl').select2({
            placeholder:"Masukkan NIK Karyawan",
            minimumInputLength: 6,
            ajax: {
                url: "/ajax/regu_search",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term,
                        jenis: 'tl'
                    };
                },
                processResults: function (response) {
                    msg_select2 = response.ada;
                    console.log(response, response.kosong)
                    return {
                        results: response.kosong
                    };
                },
                cache: true,
                success: function(value) {
                    // callback(value);
                },
                language: {
                    noResults: function() {
                        return msg_select2;
                    },
                },
                escapeMarkup: function(markup) {
                    return markup;
                },
            }
        });

        function isEmpty(obj) {
            for(var prop in obj) {
                if(obj.hasOwnProperty(prop))
                    return false;
            }

            return true;
        }

        var data = <?= json_encode($grup) ?>,
        input = <?= json_encode($regu) ?>;
        if (!isEmpty(input)){
            console.log(input)
            var nik1 = $("<option selected='selected'></option>").val(input.nik1).text(input.nik_1),
            nik2 = $("<option selected='selected'></option>").val(input.nik2).text(input.nik_2),
            TL = $("<option selected='selected'></option>").val(input.TL).text(input.T_L);
            $('#nik1').append(nik1).trigger('change');
            $('#nik2').append(nik2).trigger('change');
            $('#niktl').append(TL).trigger('change');
        }
        $()
        var select2Options = function() {
            return {
                data: data,
                placeholder: 'Input Grup',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                    return  data.text;
                }
            }
        }
        $('#chat_id').select2(select2Options());
        var mitra = <?= json_encode($mitra) ?>;
        $('#mitra').select2({
            data: mitra,
            placeholder: 'Pilih Mitra',
            formatSelection: function(data) { return data.text },
            formatResult: function(data) {
                return  data.text;
            }
        });
        var spesialis = [{"id":"IOAN", "text":"IOAN"},{"id":"SQM", "text":"SQM"},{"id":"WILSUS", "text":"WILSUS"},{"id":"INSERT", "text":"INSERT"},{"id":"UNSPEC", "text":"UNSPEC"},{"id":"ODC_SEHAT", "text":"ODC SEHAT"},{"id":"ODP_SEHAT", "text":"ODP SEHAT"},{"id":"ODP_LOSS", "text":"ODP LOSS"},{"id":"URC", "text":"URC"},{"id":"PROV", "text":"PROV"},{"id":"PT2", "text":"PT2"}];
        $('#spesialis').select2({
            data: spesialis,
            placeholder: 'Pilih Spesialis',
            formatSelection: function(data) { return data.text },
            formatResult: function(data) {
                return  data.text;
            }
        });
        var label_sektor = <?= json_encode($sektor); ?>;
        $('#label_sektor').select2({
            data: label_sektor,
            placeholder: 'Pilih Sektor',
            formatSelection: function(data) { return data.text },
            formatResult: function(data) {
                return  data.text;
            }
        });
    });
</script>
@endsection