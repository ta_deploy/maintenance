@extends('layout2')

@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>List Regu</span>
</h1>
@endsection
@section('content')
    <?php $authLevel = Session::get('auth')->maintenance_level ?>

    @if ($authLevel == '1')
        <a href="/regu/create" class="btn btn-info" style="margin: 0 0 15px;">
            <span class="glyphicon glyphicon-plus"></span>
            <span>Create Regu</span>
        </a>
    @endif
    <a href="/download/regu_status/1" class="btn btn-danger jns_hrf" style="margin: 0 0 15px;">
          <span class="glyphicon glyphicon-plus"></span>
          <span>Download Regu <span id="jenis">Aktif</span></span>
      </a>
<div class="panel">
  <div class="panel-body">
    <ul class="nav nav-tabs">
      <li class="active">
        <a href="#tabs-aktif" data-toggle="tab" data-aktif = '1'>
          Aktif
        </a>
      </li>
      <li>
        <a href="#tabs-suspend" data-toggle="tab" data-aktif = '0'>
          Tidak Aktif
        </a>
      </li>
    </ul>

    <div class="tab-content tab-content-bordered">
      <div class="tab-pane fade in active" id="tabs-aktif">
        @if (isset($list))
            <?php $url = '/regu/' ?>
            <div class="row">
                @foreach($list as $item)
                    @if($item->ACTIVE)
                    <?php
                      $pekerjaan = '<span class="label label-danger">BLM_SET</span>';
                      if($item->pekerjaan == 1){
                        $pekerjaan = '<span class="label label-success">IOAN</span>';
                      }else if($item->pekerjaan == 0){
                        $pekerjaan = '<span class="label label-success">NON-IOAN</span>';
                      }
                    ?>
                    <div class="col-md-3">
                        <div class="box {{ $item->mainsector_marina?'bg-info':'bg-danger' }} darken">
                          <div class="box-cell p-x-3 p-y-1">
                            <a href="{{ $url . $item->id_regu }}">
                                <div class="font-weight-semibold font-size-12 {{ $item->label_regu ? '' : 'bg-danger' }}">Label: {{ $item->label_regu ? $item->label_regu : 'LABEL KOSONG' }}</div>
                                <div class="font-weight-semibold font-size-12">{{ strlen($item->uraian)>25?substr($item->uraian,0,25):$item->uraian }}</div>
                                <div class="font-weight-semibold font-size-12">{{ !empty($item->nik_1) ? (strlen($item->nik_1)>25?substr($item->nik_1,0,25):$item->nik_1) : 'USER1 KOSONG' }}</div>
                                <div class="font-weight-semibold font-size-12">{{ !empty($item->nik_2) ? (strlen($item->nik_2)>25?substr($item->nik_2,0,25):$item->nik_2) : 'USER2 KOSONG' }}</div>
                                <div class="font-weight-semibold font-size-12">Pekerjaan: {!! $pekerjaan !!} : {{ $item->spesialis ?: "SPESIALIS KOSONG" }}</div>
                                <div class="font-weight-semibold font-size-12">TL: {{ (strlen($item->T_L)>25?substr($item->T_L,0,25):$item->T_L) }}</div>
                                <div class="font-weight-semibold font-size-12">{{ $item->nama_mitra ?: "MITRA KOSONG" }}</div>
                            </a>
                          </div>
                        </div>
                      </div>
                    @endif
                @endforeach
            </div>
        @endif
      </div>
      <div class="tab-pane fade" id="tabs-suspend">
        @if (isset($list))
            <?php $url = '/regu/' ?>
            <div class="row">
                @foreach($list as $item)
                    @if(!$item->ACTIVE)
                    <?php
                      $pekerjaan = '<span class="label label-danger">BLM_SET</span>';
                      if($item->pekerjaan == 1){
                        $pekerjaan = '<span class="label label-success">IOAN</span>';
                      }else if($item->pekerjaan == 0){
                        $pekerjaan = '<span class="label label-success">NON-IOAN</span>';
                      }
                    ?>
                    <div class="col-md-3">
                        <div class="box bg-warning darken">
                          <div class="box-cell p-x-3 p-y-1">
                            <a href="{{ $url . $item->id_regu }}">
                                <div class="font-weight-semibold font-size-12">{{ strlen($item->uraian)>25?substr($item->uraian,0,25):$item->uraian }}</div>
                                <div class="font-weight-semibold font-size-12">{{ !empty($item->nik_1) ? (strlen($item->nik_1)>25?substr($item->nik_1,0,25):$item->nik_1) : 'USER1 KOSONG' }}</div>
                                <div class="font-weight-semibold font-size-12">{{ !empty($item->nik_2) ? (strlen($item->nik_2)>25?substr($item->nik_2,0,25):$item->nik_2) : 'USER2 KOSONG' }}</div>
                                <div class="font-weight-semibold font-size-12">Pekerjaan: {!! $pekerjaan !!} : {{ $item->spesialis ?: "SPESIALIS KOSONG" }}</div>
                                <div class="font-weight-semibold font-size-12">TL: {{ (strlen($item->T_L)>25?substr($item->T_L,0,25):$item->T_L) }}</div>
                                <div class="font-weight-semibold font-size-12">{{ $item->nama_mitra ?: "MITRA KOSONG" }}</div>
                            </a>
                          </div>
                        </div>
                      </div>
                    @endif
                @endforeach
            </div>
        @endif
      </div>
    </div>
  </div>
</div>
<script>
$(function() {
  $('.nav-tabs').on("click", "li", function (event) {
    var activeTab = $(this).find('a').attr('href').split('-')[1],
    stts = $(this).find('a').attr('data-aktif');

    activeTab = activeTab.toLowerCase().replace(/\b[a-z]/g, function(letter) {
      return letter.toUpperCase();
    });

      $('#jenis').text(activeTab);
      $('.jns_hrf').attr('href', '/download/regu_status/' + stts)
  });
});
</script>
@endsection