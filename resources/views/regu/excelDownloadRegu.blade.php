    <div class="panel panel-default" id="info">
        <div class="panel-heading">Data Regu {{ $id == 1 ? 'Aktif' : 'Tidak Aktif' }}</div>
         <div id="fixed-table-container-demo" class="fixed-table-container">
            <table class="table table-bordered table-fixed">
              <tr>
                <th>#</th>
                <th>Label</th>
                <th>Nomor Urut</th>
                <th>Nama Regu</th>
                <th>Mitra</th>
                <th>Pekerjaan</th>
                <th>Sektor</th>
                <th>Nik1</th>
                <th>Nik2</th>
                <th>Nik TL</th>
                <th>Group</th>
                <th>Status</th>
                <th>Status Pekerjaan</th>
              </tr>
            @foreach($data as $no => $list) 
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $list->label_regu }}</td>
                    <td>{{ $list->nomor_urut }}</td>
                    <td>{{ $list->uraian }}</td>
                    <td>{{ $list->nama_mitra }}</td>
                    <td>{{ $list->spesialis }}</td>
                    <td>{{ $list->label_sektor }}</td>
                    <td>{{ $list->nik_1 }}</td>
                    <td>{{ $list->nik_2 }}</td>
                    <td>{{ $list->T_L }}</td>
                    <td>{{ $list->mainsector_marina }}</td>
                    <td>{{ $list->ACTIVE }}</td>
                    <td>{{ $list->pekerjaan }}</td>
                </tr>
            @endforeach
          </table>
      </div>
    </div>