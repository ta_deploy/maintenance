@extends('layout2')
@section('head')

@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Form Finance
        </div>
        <div class="panel-body">
            <form method="post" enctype="multipart/form-data" autocomplete="off">
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label for="posisi_doc" class="col-md-12 control-label">Posisi Doc</label>
                  <input type="text" name="posisi_doc" class="form-control" id="posisi_doc" placeholder="Pilih Posisi Doc" value="{{ $data->posisi_doc or '' }}">
                </div>
                <div class="col-sm-4 form-group">
                  <label for="status_pembayaran" class="col-md-12 control-label">Status Pembayaran</label>
                  <input type="text" name="status_pembayaran" class="form-control" id="status_pembayaran" placeholder="Pilih Status Pembayaran" value="{{ $data->status_pembayaran or '' }}">
                </div>
                <div class="col-sm-4 form-group">
                  <label for="status_doc" class="col-md-12 control-label">Status Doc</label>
                  <input type="text" name="status_doc" class="form-control" id="status_doc" placeholder="Pilih Status Doc" value="{{ $data->status_doc or '' }}">
                </div>
              </div>

            <div class="row">
                <div class="col-md-9">
                  <button type="submit" class="btn"><span class="btn-label-icon left fa fa-database"></span>Submit</button>
                </div>
            </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            $('#posisi_doc').select2({
                data:[{id:"Procurement Regional",text:"Procurement Regional"},{id:"Finance Regional",text:"Finance Regional"}],
                placeholder:"Pilih Posisi Doc"
            });
            $('#status_pembayaran').select2({
                data:[{id:"Verifikasi",text:"Verifikasi"},{id:"Counting",text:"Counting"},{id:"Payment",text:"Payment"},{id:"Cash&Bank",text:"Cash&Bank"}],
                placeholder:"Pilih Status Pembayaran"
            });
            $('#status_doc').select2({
                data:[{id:"Proses TTD",text:"Proses TTD"},{id:"Verifikasi",text:"Verifikasi"},{id:"Revisi",text:"Revisi"}],
                placeholder:"Pilih Status Doc"
            });
        });
    </script>
@endsection