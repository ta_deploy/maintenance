@extends('layout2')
@section('head')

@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Form Pemberkasan ( Mitra )
        </div>
        <div class="panel-body">
            <form method="post" enctype="multipart/form-data" autocomplete="off">
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label for="baut" class="col-md-3 control-label">Baut</label>
                  <input name="baut" type="file" class="form-control" id="baut" >
                </div>
                <div class="col-sm-6 form-group">
                  <label for="ba_rekon" class="col-md-3 control-label">BA Rekon</label>
                  <input name="ba_rekon" type="file" class="form-control" id="ba_rekon" >
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label for="boq_rekon" class="col-md-3 control-label">BoQ Rekon</label>
                  <input name="boq_rekon" type="file" class="form-control" id="boq_rekon" >
                </div>
                <div class="col-sm-6 form-group">
                  <label for="rekapitulasi_rekon" class="col-md-6 control-label">Rekapitulasi Rekon</label>
                  <input name="rekapitulasi_rekon" type="file" class="form-control" id="rekapitulasi_rekon" >
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label for="rekon_material" class="col-md-3 control-label">Rekon Material</label>
                  <input name="rekon_material" type="file" class="form-control" id="rekon_material" >
                </div>
                <div class="col-sm-6 form-group">
                  <label for="bast_abd" class="col-md-3 control-label">BAST ABD</label>
                  <input name="bast_abd" type="file" class="form-control" id="bast_abd" >
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12 form-group">
                  <label for="bast_pekerjaan" class="col-md-3 control-label">BAST Pekerjaan</label>
                  <input name="bast_pekerjaan" type="file" class="form-control" id="bast_pekerjaan" >
                </div>
              </div>
              <div class="row">
                <div class="col-md-9">
                  <button type="submit" class="btn"><span class="btn-label-icon left fa fa-database"></span>Submit</button>
                </div>
              </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
    </script>
@endsection