@extends('layout2')
@section('head')

@endsection
@section('content')
<div class="panel panel-default">
  <div class="panel-heading">
    @if (isset($data))
    Edit {{ $data->judul }}
    @else
    Input Baru
    @endif
  </div>
  <div class="panel-body">
    <form method="post" enctype="multipart/form-data" autocomplete="off">
      <div class="row">
        <div class="col-sm-6 form-group">
          <label for="judul" class="col-md-3 control-label">Judul Pekerjaan</label>
          <input type="text" name="judul" class="form-control" id="judul" placeholder="Judul Pekerjaan" value="{{ $data->judul or '' }}" >
        </div>
        <div class="col-sm-6 form-group">
          <label for="mitra" class="col-md-3 control-label">Mitra</label>
          <select name="mitra" id="mitra">
           @foreach($mitra as $m)
           <option value='{{ $m->id }}'>{{ $m->mitra }}</option>
           @endforeach
         </select>
       </div>
     </div>
     <div class="row">
      <div class="col-sm-4 form-group">
        <label for="program" class="col-md-4 control-label">Program</label>
        <input type="text" name="program" class="form-control" id="program" placeholder="program" value="{{ $data->program or '' }}">
      </div>
      <div class="col-sm-4 form-group">
        <label for="bulan" class="col-md-4 control-label">Bulan</label>
        <select name="bulan" id="bulan">
          <option value="JANUARI">JANUARI</option>
          <option value="FEBRUARI">FEBRUARI</option>
          <option value="MARET">MARET</option>
          <option value="APRIL">APRIL</option>
          <option value="MEI">MEI</option>
          <option value="JUNI">JUNI</option>
          <option value="JULI">JULI</option>
          <option value="AGUSTUS">AGUSTUS</option>
          <option value="SEPTEMBER">SEPTEMBER</option>
          <option value="OKTOBER">OKTOBER</option>
          <option value="NOVEMBER">NOVEMBER</option>
          <option value="DESEMBER">DESEMBER</option>
        </select>
      </div>
    {{--   <div class="col-sm-4 form-group">
        <label for="nilai" class="col-md-4 control-label">Nilai</label>
        <input type="text" name="nilai" class="form-control" id="nilai" placeholder="Nilai" value="{{ $data->nilai or '' }}">
      </div> --}}
    </div>
    <div class="row">
      <div class="col-sm-6 form-group">
        <label for="npwp" class="col-md-4 control-label">NPWP</label>
        <input type="text" name="npwp" class="form-control" id="npwp" placeholder="Nomor NPWP" value="{{ $data->npwp or '' }}">
      </div>
      <div class="col-sm-6 form-group">
        <label for="sp_no" class="col-md-4 control-label">NO SP</label>
        <input type="text" name="sp_no" class="form-control" id="sp_no" placeholder="Nomor SP/PO" value="{{ $data->sp_no or '' }}">
      </div>
    </div>
    <div class="row">
      <div class="col-md-9">
        <button type="submit" class="btn"><span class="btn-label-icon left fa fa-database"></span>Submit</button>
      </div>
    </div>
  </form>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
  $(function() {
    $('#program').select2({
      data:[{id:"Maintenance",text:"Maintenance"},{id:"Insert Tiang",text:"Insert Tiang"}],
      placeholder:"Pilih Program"
    });
    $('#mitra, #bulan').select2();
  });
</script>
@endsection