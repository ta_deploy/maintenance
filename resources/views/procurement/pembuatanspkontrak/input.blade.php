@extends('layout2')
@section('head')

@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Form Pembuatan SP/Kontrak
        </div>
        <div class="panel-body">
            <form method="post" enctype="multipart/form-data" autocomplete="off">
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label for="no_khs" class="col-md-4 control-label">No.KHS</label>
                  <input type="text" name="no_khs" class="form-control" id="no_khs" placeholder="Isi No. KHS" value="{{ $data->no_khs or '' }}">
                </div>
                <div class="col-sm-4 form-group">
                  <label for="no_sp" class="col-md-4 control-label">No. SP</label>
                  <input type="text" name="no_sp" class="form-control" id="no_sp" placeholder="Isi No. SP" value="{{ $data->no_sp or '' }}">
                </div>
                <div class="col-sm-4 form-group">
                  <label for="toc" class="col-md-4 control-label">ToC</label>
                  <input type="text" name="toc" class="form-control" id="toc" placeholder="Isi Target of Complete" value="{{ $data->toc or '' }}">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 form-group">
                  <label for="kesanggupan" class="col-md-3 control-label">Penetapan Kesanggupan</label>
                  <input name="kesanggupan" type="file" class="form-control" id="kesanggupan" >
                </div>
              </div>

              <div class="row">
                <div class="col-md-9">
                  <button type="submit" class="btn"><span class="btn-label-icon left fa fa-database"></span>Submit</button>
                </div>
              </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
          $('#toc').datepicker({
            format: 'yyyy-mm-dd'
          });
        });
    </script>
@endsection