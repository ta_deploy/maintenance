@extends('layout2')
@section('content')
<div class="modal fade" id="modal_sejarah" tabindex="-1" role="dialog" aria-labelledby="modal_sejarahLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_sejarahLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th>Update By</th>
                        <th>Catatan</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody class="konten_tbl">

                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>
<div class="panel panel-default" id="info">
    <div class="panel-heading">Index Pembuatan SP</div>
    <div id="fixed-table-container-demo" class="fixed-table-container">
        <table class="table table-bordered table-fixed">
            <thead>
              <tr>
                <th>#</th>
                <th>Judul</th>
                <th>Bulan</th>
                <th>Nilai/Files</th>
                <th>Status</th>
                <th>Catatan</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $list) 
            <?php
            $nde="/upload/proc/admin/".$list->id."/".$list->nde;
            $justifikasi="/upload/proc/admin/".$list->id."/".$list->justifikasi;
            $boq_real="/upload/proc/admin/".$list->id."/".$list->boq_real;
            ?>
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $list->judul }}</td>
                <td>{{ $list->bulan }}</td>
                <td>{{ $list->nilai }}<br/>
                    {!! $list->nde?'<span class="label label-info"><a href="'.$nde.'" target="_BLANK">nde</a></span>':'' !!}
                    {!! $list->justifikasi?'<span class="label label-info"><a href="'.$justifikasi.'" target="_BLANK">justifikasi</a></span>':'' !!}
                    {!! $list->boq_real?'<span class="label label-info"><a href="'.$boq_real.'" target="_BLANK">boq_real</a></span>':'' !!}
                </td>
                <td>{{ $list->status }}</td>
                <td>{{ $list->catatan }}</td>
                <td>
                    <a href="/proc/pembuatansp/{{ $list->id }}">
                        <button type="button" class="btn btn-success btn-outline btn-rounded btn-xs">
                            <span class="btn-label-icon left fa fa-pencil"></span>Edit
                        </button>
                    </a>
                    <a type="button" class="btn btn-success btn_sejarah btn-outline btn-rounded btn-xs" data-aku="
                    {{ $list->id }}" data-toggle="modal" data-target="#modal_sejarah">
                    <span class="btn-label-icon left fa fa-align-justify"></span>History
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        $('.btn_sejarah').on('click', function(){
            $.ajax({
                url: '/proc/ajax/hist',
                datatype: 'json',
                type: 'GET',
                data: {id: $(this).data('aku')},
            })
            .done(function(data){
                 var data;
                $.each(data, function(key, value){
                    data += "<tr>";
                    data += "<td>"+value.updater+"</td>";
                    data += "<td>"+value.catatan+"</td>";
                    data += "<td>"+value.status+"</td>";
                    data += "</tr>";
                });
                $('.konten_tbl').html(data);
            });
        });
    });
</script>
@endsection