@extends('layout2')
@section('head')

@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Form Verifikasi
        </div>
        <div class="panel-body">
            <form method="post" enctype="multipart/form-data" autocomplete="off">
              <div class="row">
                <div class="col-sm-12 form-group">
                  <label for="status" class="col-md-4 control-label">Status</label>
                  <input type="text" name="status" class="form-control" id="status" placeholder="Pilih Status">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 form-group">
                  <label for="catatan" class="col-md-4 control-label">Catatan</label>
                  <textarea name="catatan" class="form-control" id="catatan" placeholder="Isi Catatan"></textarea>
                </div>
              </div>

            <div class="row">
                <div class="col-md-9">
                  <button type="submit" class="btn"><span class="btn-label-icon left fa fa-database"></span>Submit</button>
                </div>
            </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            $('#status').select2({
                data:[{id:"Diterima",text:"Diterima"},{id:"Ditolak",text:"Ditolak"}],
                placeholder:"Pilih Status"
            });
        });
    </script>
@endsection