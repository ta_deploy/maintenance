@extends('layout2')
@section('head')

@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Form Pengiriman Doc
        </div>
        <div class="panel-body">
            <form method="post" enctype="multipart/form-data" autocomplete="off">
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label for="tgl_kirim" class="col-md-4 control-label">Tgl. Kirim</label>
                  <input type="text" name="tgl_kirim" class="form-control" id="tgl_kirim" placeholder="Isi Tgl. Kirim" value="{{ $data->tgl_kirim or '' }}">
                </div>
                <div class="col-sm-4 form-group">
                  <label for="no_resi" class="col-md-4 control-label">No. Resi</label>
                  <input type="text" name="no_resi" class="form-control" id="no_resi" placeholder="Isi No. Resi" value="{{ $data->no_resi or '' }}">
                </div>
                <div class="col-sm-4 form-group">
                  <label for="kurir" class="col-md-4 control-label">Kurir</label>
                  <input type="text" name="kurir" class="form-control" id="kurir" placeholder="Isi Target of Complete" value="{{ $data->kurir or '' }}">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 form-group">
                  <label for="catatan" class="col-md-4 control-label">Catatan</label>
                  <textarea name="catatan" class="form-control" id="catatan" placeholder="Isi Catatan"></textarea>
                </div>
              </div>

              <div class="row">
                <div class="col-md-9">
                  <button type="submit" class="btn"><span class="btn-label-icon left fa fa-database"></span>Submit</button>
                </div>
              </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            $('#tgl_kirim').datepicker({
              format: 'yyyy-mm-dd'
            });
        });
    </script>
@endsection\