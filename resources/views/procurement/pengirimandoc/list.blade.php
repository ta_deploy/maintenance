@extends('layout2')
@section('content')
    <div class="panel panel-default" id="info">
        <div class="panel-heading">Index Pengiriman Doc</div>
        <div id="fixed-table-container-demo" class="fixed-table-container">
            <table class="table table-bordered table-fixed">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Judul</th>
                    <th>Bulan</th>
                    <th>Nilai/Files</th>
                    <th>Status</th>
                    <th>Catatan</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($data as $no => $list) 
                    <?php
                        $nde="/upload/proc/admin/".$list->id."/".$list->nde;
                        $justifikasi="/upload/proc/admin/".$list->id."/".$list->justifikasi;
                        $boq_real="/upload/proc/admin/".$list->id."/".$list->boq_real;
                        $kesanggupan="/upload/proc/pembuatansp/".$list->id."/".$list->kesanggupan;
                    ?>
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td>{{ $list->judul }}</td>
                            <td>{{ $list->bulan }}</td>
                            <td>{{ $list->nilai }}<br/>
                                {!! $list->nde?'<span class="label label-info"><a href="'.$nde.'" target="_BLANK">nde</a></span>':'' !!}
                                {!! $list->justifikasi?'<span class="label label-info"><a href="'.$justifikasi.'" target="_BLANK">justifikasi</a></span>':'' !!}
                                {!! $list->boq_real?'<span class="label label-info"><a href="'.$boq_real.'" target="_BLANK">boq_real</a></span>':'' !!}
                                {!! $list->kesanggupan?'<span class="label label-info"><a href="'.$kesanggupan.'" target="_BLANK">kesanggupan</a></span>':'' !!}
                            </td>
                            <td>{{ $list->status }}</td>
                            <td>{{ $list->catatan }}</td>
                            <td>
                                <a href="/proc/pengirimandoc/{{ $list->id }}">
                                    <button type="button" class="btn btn-success btn-outline btn-rounded btn-xs">
                                        <span class="btn-label-icon left fa fa-pencil"></span>Edit
                                    </button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection