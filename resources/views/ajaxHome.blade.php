
	<style type="text/css">
        .container {
            width: 100%;
            margin: 5px;
        }
        .table-condensed{
          font-size: 10px;
        }
        
    </style>
    <link rel="stylesheet" href="/bower_components/amcharts3/amcharts/plugins/export/export.css" type="text/css" media="all" />
    <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />

<div class="txtHint">
<div class="row">
    <div class="col-md-6">
        <div id="chart1div" style="height:250px;background-color:white;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;"></div>
    </div>
    <div class="col-md-6">
        <div id="chart2div" style="height:250px;background-color:white;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;"></div>
    </div>
</div>
<div class="row">
	<div class="col-md-6" style="margin-top:10px;">
		<div id="chart3div" style="height:250px;background-color:white;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;"></div>
	</div>
</div>
</div>
	<script src="/bower_components/amcharts3/amcharts/amcharts.js" type="text/javascript"></script>
    <script src="/bower_components/amcharts3/amcharts/serial.js" type="text/javascript"></script>
    <script src="/bower_components/amcharts3/amcharts/pie.js" type="text/javascript"></script>
    <script src="/bower_components/amcharts3/amcharts/themes/light.js" type="text/javascript"></script>
    <script src="/bower_components/amcharts3/amcharts/plugins/export/export.min.js" type="text/javascript"></script>
    <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
    $(function() {
    	$(".ajax").on("click", function(){
    		var tgl = $('#tgl').val();
		    $.get("/ajaxHome/"+tgl, function(text) {
		    	$("#txtHint").html(text);
		    }).done(function(){
		        
		    });
	    });
    	var day = {
              format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0
            };
            $("#tgl").datepicker(day).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
		var data = <?= json_encode($data) ?>;
		AmCharts.makeChart("chart1div", {
		    "type": "serial",
			"theme": "light",
		    "legend": {
		        "horizontalGap": 10,
		        "maxColumns": 1,
		        "position": "right",
				"useGraphSettings": true,
				"markerSize": 10
		    },
		    "dataProvider": data,
		    "valueAxes": [{
		        "stackType": "regular",
		        "axisAlpha": 0.3,
		        "gridAlpha": 0

		    }],
		    "graphs": [{
		        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
		        "fillAlphas": 0.8,
		        "labelText": "[[value]]",
		        "lineAlpha": 0.3,
		        "title": "Close",
		        "type": "column",
		        "valueField": "close"
		    }, {
		        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
		        "fillAlphas": 0.8,
		        "labelText": "[[value]]",
		        "lineAlpha": 0.3,
		        "title": "Kendala Pelanggan",
		        "type": "column",
		        "valueField": "kendala_pelanggan"
		    }, {
		        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
		        "fillAlphas": 0.8,
		        "labelText": "[[value]]",
		        "lineAlpha": 0.3,
		        "title": "Kendala Teknis",
		        "type": "column",
		        "valueField": "kendala_teknis"
		    }],
		    "categoryField": "nama_order",
		    "categoryAxis": {
		        "gridPosition": "start",
		        "axisAlpha": 0,
		        "gridAlpha": 0,
		        "position": "left",
		        "title": "Status Per Program Chart"
		    },
		  	"export": {
		    	"enabled": true
		  	}
		});
		var action = <?= json_encode($action) ?>;
		AmCharts.makeChart("chart2div", {
			"theme": "light",
		    "type": "serial",
		    "dataProvider": action,
		    "valueAxes": [{
		        "title": "Action/Cause Chart"
		    }],
		    "graphs": [{
		        "balloonText": "Tiket [[category]]:[[value]]",
		        "fillAlphas": 1,
		        "lineAlpha": 0.2,
		        "title": "Income",
		        "type": "column",
		        "valueField": "rows"
		    }],
		    "depth3D": 20,
		    "angle": 30,
		    "rotate": true,
		    "categoryField": "action_cause",
		    "categoryAxis": {
		        "gridPosition": "start",
		        "fillAlpha": 0.05,
		        "position": "left"
		    },
		  	"export": {
		    	"enabled": true
		  	}
		});
		var remo = <?= json_encode($remo) ?>;
		var chart = AmCharts.makeChart( "chart3div", {
		  	"type": "pie",
		  	"theme": "light",
		  	"dataProvider": [{
			    "country": "Close",
			    "value": remo.close,
			    "url":"/listRemo/close"
		  	},{
			    "country": "kendala pelanggan",
			    "value": remo.kendala_pelanggan,
			    "url":"/listRemo/kendala_pelanggan"
		  	},{
			    "country": "kendala teknis",
			    "value": remo.kendala_teknis,
			    "url":"/listRemo/kendala_teknis"
		  	},{
			    "country": "Sisa Order",
			    "value": remo.order_remo,
			    "url":"/listRemo/sisa"
		  	}],
		  	"urlField": "",
		  	"valueField": "value",
		  	"titleField": "country",
		  	"urlField": "url",
		  	"labelText": "[[country]] : [[value]]",
		  	"outlineAlpha": 0.4,
		  	"depth3D": 15,
		  	"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[percents]]%</b> ([[value]])</span>",
		  	"angle": 60,
		  	"categoryAxis": {
		        "title": "Remo Per Bulan Chart"
		    },
		  	"export": {
		    	"enabled": true
		  	}
		});
	});
</script>