@extends('layout2')
@section('head')
    <style type="text/css">
        th{
            text-align: center;
            vertical-align: middle;
        }
    </style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>SABER</span>
</h1>
@endsection
@section('content')
<form method="GET">
    <div class="form-group row">
        <div class="col-sm-12">
            <div class="input-group">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-info dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pilih <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                    <li><a href="#" id="daily">Daily</a></li>
                    <li><a href="#" id="monthly">Monthly</a></li>

                    </ul>
                </span>
                <span class="ins">
                    <input type="text" name="tgl" id="tgl" value="{{ date('Y-m') }}" class="form-control input-sm">
                </span>
            </div>
        </div>
    </div>
</form>
    <br/>
    <div class="panel panel-default">
        <div class="panel-heading">prod</div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped" id="table">
                <tr>
                    <th rowspan="2" class="valign-middle">#</th>
                    <th rowspan="2" class="valign-middle">Regu</th>
                    <th colspan="3">PSB</th>
                    <th colspan="3">Marina</th>
                    <th colspan="3">PT2</th>
                    <th colspan="3" class="valign-middle">Revenue</th>
                </tr>
                <tr>
                    <th>UP</th>
                    <th>Kendala</th>
                    <th>Sisa</th>
                    <th>UP</th>
                    <th>Kendala</th>
                    <th>Sisa</th>
                    <th>UP</th>
                    <th>Kendala</th>
                    <th>Sisa</th>
                    <th>PSB</th>
                    <th>Marina</th>
                    <th>PT-2</th>
                </tr>
                @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->uraian }}</td>
                    <td class="text-right">{{ @$d->close_PSB != 0 ? $d->close_PSB : '-' }}</td>
                    <td class="text-right">{{ @$d->kendala_PSB != 0 ? $d->kendala_PSB : '-' }}</td>
                    <td class="text-right">{{ @$d->inbox_teknisi_PSB != 0 ? $d->inbox_teknisi_PSB : '-' }}</td>
                    <td class="text-right">{{ @$d->close_M != 0 ? $d->close_M : '-' }}</td>
                    <td class="text-right">{{ @$d->kendala_M != 0 ? $d->kendala_M : '-' }}</td>
                    <td class="text-right">{{ @$d->inbox_teknisi_M != 0 ? $d->inbox_teknisi_M : '-' }}</td>
                    <td class="text-right">{{ @$d->close_pt2 != 0 ? $d->close_pt2 : '-' }}</td>
                    <td class="text-right">{{ @$d->kendala_pt2 != 0 ? $d->kendala_pt2 : '-' }}</td>
                    <td class="text-right">{{ @$d->inbox_teknisi_pt2 != 0 ? $d->inbox_teknisi_pt2 : '-' }}</td>
                    <td class="text-right">{{ @$d->rev_PSB != 0 ? number_format($d->rev_PSB) : '-' }}</td>
                    <td class="text-right">{{ @$d->rev_M != 0 ? number_format($d->rev_M) : '-' }}</td>
                    <td class="text-right">{{ @$d->rev_pt2 != 0 ? number_format($d->rev_pt2) : '-' }}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function() {
            var day = {
                format: "yyyy-mm-dd",
                viewMode: 0,
                minViewMode: 0,
                autoclose:true
            };

            var month = {
                format: "yyyy-mm",
                viewMode: 2,
                minViewMode: 1,
                autoclose:true
            };

            var d = new Date(),
            monthly = '' + (d.getMonth() + 1),
            dayly = '' + d.getDate(),
            year = d.getFullYear();

            if (monthly.length < 2) monthly='0' + monthly;
            if (dayly.length < 2) dayly='0' + dayly;

            $("#tgl").datepicker(month).on('changeDate',function(ev){
                window.location.href = document.location.origin+"/saber/"+ev.target.value;
                // $("#order").val('').change();
            });

            $("#daily").click(function(){
                $("#tgl").remove();
                $(".ins").after("<input type=text class='form-control input-sm' id=tgl>");
                $("#tgl").datepicker(day);
                $("#tgl").val([year, monthly, dayly].join('-'));
            });

            $("#monthly").click(function(){
                $("#tgl").remove();
                $(".ins").after("<input type=text class='form-control input-sm' id=tgl>");
                $("#tgl").datepicker(month);
                $("#tgl").val([year, monthly].join('-'));
            });
        });
    </script>
@endsection