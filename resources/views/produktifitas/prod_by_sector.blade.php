@extends('layout2')
@section('head')
  <style type="text/css">
    th{
        text-align: center;
    }
  </style>
@endsection
@section('content')
<input name="tglnav" id="tglnav" type="hidden" value="{{Request::segment(3)}}"/>
    <div class="panel panel-default" id="info" class="align-middle">
        <div class="panel-heading"> 
        <div class="row">
      <span class="pull-left m-t-2">SEKTOR</span>
  <div class="pull-right">
    <form class="navbar-form navbar-left" role="search">
      <div class="form-group">
         Tahun
        <input type="text" id="tahun" class="form-control input-sm" value="{{ substr(Request::segment(2),0,4) }}" placeholder="Tahun">
      </div>
      <div class="form-group">
        Bulan
        <input type="text" id="bulan" class="form-control input-sm" value="{{ substr(Request::segment(2),5,2) }}" placeholder="Bulan">
      </div>
      <button type="button" id="goto" class="btn btn-success btn-sm">Go!</button>
    </form>
  </div><!-- /.col-lg-6 -->
</div></div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-fixed">
                <tr>
                    <th style="vertical-align:middle" rowspan=1>#</th>
                    <th style="vertical-align:middle" rowspan=1>SEKTOR</th>
                    <th style="vertical-align:middle" rowspan=1>BLM DISPATCH</th>
                    <th style="vertical-align:middle" rowspan=1>INBOX TEKNISI</th>
                    <th style="vertical-align:middle" rowspan=1>OGP</th>
                    <th style="vertical-align:middle" rowspan=1>KENDALA PELANGGAN</th>
                    <th style="vertical-align:middle" rowspan=1>KENDALA TEKNIS</th>
                    <th style="vertical-align:middle" rowspan=1>CLOSE</th>
                </tr>
                @foreach($data as $no => $d)
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $d->nama_sector?:"Anomali Sektor" }}</td>
                        <td>{{ $d->blm_dispatch }}</td>
                        <td>{{ $d->inbox_teknisi }}</td>
                        <td>{{ $d->ogp }}</td>
                        <td>{{ $d->kendala_pelanggan }}</td>
                        <td>{{ $d->kendala_teknis }}</td>
                        <td>{{ $d->close }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div id="detil-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #eee">
          <strong style="color:black;">Detail Material</strong>
          <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
        </div>
        <div class="modal-body" style="overflow-y:auto;height : 450px">

        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            $('#tahun').datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years",
                autoclose: true
            });
            $('#bulan').datepicker({
                format: "mm",
                viewMode: "months",
                minViewMode: "months",
                autoclose: true
            });
            $("#goto").click(function(e){
                var segments = $(location).attr('href').split('/');
                var goto=segments[3]+'/'+segments[4]+'/'+$('#tahun').val()+'-'+$('#bulan').val();
                window.location = '/'+goto;
            });
        })
    </script>
@endsection