@extends('layout2')
@section('head')
  <style type="text/css">
    th{
        text-align: center;
        vertical-align: middle;
    }
    .geser_kiri_dikit {
      position: relative;
      left : -10px;
    }
  </style>
@endsection

@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>PRODUKTIFITAS SEKTOR</span>
</h1>
<div class="pull-right">
    <form class="navbar-form navbar-left" role="search">
    <div class="form-group">
        Tahun
        <input type="text" id="tahun" style="width: 60px;" class="form-control input-sm" value="{{ substr(Request::segment(2),0,4) }}" placeholder="Tahun">
    </div>
    <div class="form-group">
        Bulan
        <input type="text" id="bulan" style="width: 35px;" class="form-control input-sm" value="{{ substr(Request::segment(2),5,2) }}" placeholder="Bulan">
    </div>
    <div class="form-group">
        Hari
        <input type="text" id="hari" style="width: 35px;" class="form-control input-sm" value="{{ substr(Request::segment(2),8,2) }}" placeholder="Hari">
    </div>
      <button type="button" id="goto" class="btn btn-success btn-sm">Go!</button>
    </form>
</div>
@endsection
@section('content')
    @foreach($regu as $no => $r)
    <div class="panel panel-default">
        <div class="panel-heading"><span class="pull-left m-t-2">SEKTOR {{ $r['grup'] }} </span>
        </div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped" id="table">
                <tr>
                    <th rowspan="2">#</th>
                    <th rowspan="2">Nama<br/>Tim</th>
                    <th colspan="4">WO {{ Request::segment(2) }}</th>
                    <th rowspan="2">Close/bln</th>
                    <th rowspan="2">List</th>
                </tr>
                <tr>
                    <th>Sisa</th>
                    <th>Kendala<br/>Teknis</th>
                    <th>Kendala<br/>Pelanggan</th>
                    <th>Close</th>
                </tr>
                @php $no = 0; @endphp
                @foreach($r['data'] as $noo => $d)
                    <?php
                        @$id = $list[$d->id_regu];
                    ?>
                    @if(count($id))
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $d->uraian }}:<span class="label label-success">{{ $d->spesialis }}</span><br/>
                            @if($d->nama1)
                            <span class="label label-primary">
                                {{ $d->nama1 }}:
                                {{ (substr($d->verif1,0,10) == date('Y-m-d')) ? 'HADIR' : 'ABSEN' }}</span>
                            @endif
                            <br/>
                            @if($d->nama2)
                            <span class="label label-primary">
                                {{$d->nama2}}:
                                {{ (substr($d->verif2,0,10) == date('Y-m-d')) ? 'HADIR' : 'ABSEN' }}</span>
                            @endif
                            <br/>
                        </td>
                        <!--<td>{{ $d->jumlah }}</td>-->
                        <td>{{ $d->no_update }}</td>
                        <td>{{ $d->kendala_teknis }}</td>
                        <td>{{ $d->kendala_pelanggan }}</td>
                        <td>{{ $d->close }}</td>
                        <td>{{ $d->close_bln }}</td>
                        <td>

                            @foreach($id as $nolist => $l)
                                <?php
                                if ($l['status_laporan'] == 'close') {
                                        $label = 'success';
                                        $date2=date_create($l['tgl_selesai']);
                                    }else if($l['status_laporan'] == null) {
                                        $label = 'default';
                                        $date2=date_create(date('Y-m-d H:i:s'));
                                    }else if($l['status_laporan'] == 'ogp') {
                                        $label = 'warning';
                                        $date2=date_create(date('Y-m-d H:i:s'));
                                    }else{
                                        $label = 'danger';
                                        $date2=date_create($l['tgl_selesai']);
                                    }
                                    $date1= date_create($l['tgl_dispatch']?:$l['created_at']);

                                    $diff=date_diff($date1,$date2);
                                    if($diff->format('%m')){
                                        $waktu = $diff->format("%m Bulan %d Hari %h Jam %i Menit");
                                    }else if($diff->format('%d')){
                                        $waktu = $diff->format("%d Hari %h Jam %i Menit");
                                    }else if($diff->format('%h')){
                                        $waktu = $diff->format("%h Jam %i Menit");
                                    }else{
                                        $waktu = $diff->format("%i Menit");
                                    }
                                ?>
                                <!--<span class="label label-{{ $label }}">{{ $l['tiket'] }}</span>-->
                                <a data-html="true" data-original-title data-toggle="popover" data-placement="top" title=""  data-html="true" data-content="<b>Status</b> : <br />{{ $l['status_laporan']? : 'NO UPDATE ' }}&nbsp;<a class='btn btn-sm btn-{{ $label}}' href='/tech/{{ $l['id'] }}'>Detil</a>" class="label label-{{ $label }}">{{ $l['tiket'] }} // {{ $l['odp_nama']}} // ({{ $waktu }})</a>
                                <!-- @if(substr($l['created_at'], 0, 10)==date('Y-m-d'))
                                    <img src="http://acpe.alaska.gov/portals/3/Images/NEW_burst_for_webpage_item.png" class="geser_kiri_dikit" width="42" height="20"/>
                                @endif -->
                            @endforeach
                        </td>
                    </tr>
                    @endif
                @endforeach

            </table>
        </div>
    </div>
    @endforeach
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $("[data-toggle='popover']").popover({html:true});
            setInterval(function() {

              var currentTime = new Date();
              var hours = currentTime.getHours();
              var minutes = currentTime.getMinutes();
              var seconds = currentTime.getSeconds();

              // Add leading zeros
              hours = (hours < 10 ? "0" : "") + hours;
              minutes = (minutes < 10 ? "0" : "") + minutes;
              seconds = (seconds < 10 ? "0" : "") + seconds;

              // Compose the string for display
              var currentTimeString = hours + ":" + minutes + ":" + seconds;

              $(".clock").html(currentTimeString);

            }, 1000);
            $('#tahun').datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years",
                autoclose: true
            });
            $('#bulan').datepicker({
                format: "mm",
                viewMode: "months",
                minViewMode: "months",
                autoclose: true
            });
            $('#hari').datepicker({
                format: "dd",
                viewMode: "days",
                minViewMode: "days",
                autoclose: true
            });
            $("#goto").click(function(e){
                var segments = $(location).attr('href').split('/');
                var goto=segments[3]+'/'+$('#tahun').val()+'-'+$('#bulan').val()+'-'+$('#hari').val();
                window.location = '/'+goto;
            });
        });
    </script>
@endsection