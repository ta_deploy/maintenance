<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">


  <title>MARINA</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
  <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!-- Core stylesheets -->
  <!-- <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="/css/pixeladmin.min.css" rel="stylesheet" type="text/css">
  <link href="/css/widgets.min.css" rel="stylesheet" type="text/css">

  <link href="/css/themes/{{ session('auth')->pixeltheme?:'frost' }}.min.css" rel="stylesheet" type="text/css"> -->
  <?php
        $time = time();
        $auth = session('auth') ?: '';
        $path = Request::path();
        $theme = 'mint-dark';
        if(isset($auth->pixeltheme)){
            if($auth->pixeltheme){
                $theme = $auth->pixeltheme;
            }
        }
        $color = '';
        if (str_contains($theme, 'dark')) {
            $color = '-dark';
        }
        $isBpp = str_contains(Request::root(), ['marina.tomman.app']);
    ?>
    <link href="/css/bootstrap{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/css/pixeladmin{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/css/widgets{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/css/themes/{{ $theme }}.min.css" rel="stylesheet" type="text/css">
  <style type="text/css">
    .input-photos img {
      width: 100px;
      height: 150px;
      margin-bottom: 5px;
    }
    .layout_search::placeholder {
      color: white;
      opacity: 1; /* Firefox */
    }

    .layout_search::-ms-input-placeholder { /* Internet Explorer 10-11 */
      color: white;
   }

   .layout_search::-ms-input-placeholder { /* Microsoft Edge */
    color: white;
   }
 </style>
 <!-- Pace.js -->

 @yield('head')
</head>
<body>
  <!--1:admin;2:sdi;3:warroom;4:sales:5:optima;6:HD PROV;7:HS;0:view;-->
  <!-- Nav -->
  <nav class="px-nav px-nav-left px-nav-fixed">
    <button type="button" class="px-nav-toggle" data-toggle="px-nav">
      <span class="px-nav-toggle-arrow"></span>
      <span class="navbar-toggle-icon"></span>
      <span class="px-nav-toggle-label font-size-11">HIDE MENU</span>
    </button>

    <ul class="px-nav-content">
      <li class="px-nav-box p-a-3 b-b-1" id="demo-px-nav-box">
        <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php
        $path = "/upload/profile/".session('auth')->id_user."/FOTO.jpg";
        ?>
        @if (file_exists(public_path().$path))
        <img src="{{ $path }}" alt="" class="pull-xs-left m-r-2 border-round" style="width: 54px; height: 54px;">
        @else
        <img src="/image/placeholder.gif" alt="" class="pull-xs-left m-r-2 border-round" style="width: 54px; height: 54px;">
        @endif
        <div class="font-size-16"><span class="font-weight-light">Welcome, </span><strong>{{ session('auth')->nama }}</strong></div>
      </li>
      <!-- <li class="px-nav-item px-nav-dropdown">
        <a href="#"><i class="px-nav-icon ion-ios-plus"></i><span class="px-nav-label">Admin</span></a>
        <ul class="px-nav-dropdown-menu">
          <li class="px-nav-item"><a href="/user"><span class="px-nav-label">User</span></a></li>
        </ul>
      </li> -->
      <?php
      $lvl = session('auth')->maintenance_level;
          // dd(session('auth'));
      ?>
      @if($lvl != 6 && $lvl != 0)
        <!-- <li class="{{ Request::segment(1) == 'nossa' ? 'active' : '' }}">
            <a href="/nossa">Nossa</a>
          </li> -->
          <li class="px-nav-item px-nav-dropdown">
            <a href="#"><i class="px-nav-icon ion-usb"></i><span class="px-nav-label">Order</span></a>
            <ul class="px-nav-dropdown-menu">
              <!-- <li class="px-nav-item"><a href="/order/none/none/{{ date('Y-m') }}/all?q=14894226">Search</a></li> -->
              <li class="px-nav-item"><a href="/order/input">Register</a></li>
              <li class="px-nav-item"><a href="/classification/odc">Klasifikasi ODC</a></li>
              @if(!$isBpp)
              <li class="px-nav-item"><a href="/listtommanpsb/24">ODP FULL(tPSB)</a></li>
              <li class="px-nav-item"><a href="/listtommanpsb/2">ODP LOSS(tPSB)</a></li>
              <li class="px-nav-item"><a href="/listtommanpsb/11">INSERT TIANG(tPSB)</a></li>
              @endif
              <li class="px-nav-item"><a href="/orderM/list/{{date('Y-m')}}">Matrix</a></li>
              <li class="px-nav-item"><a
              href="/search/ibooster">Ibooster</a></li>
            </ul>
          </li>
          <li class="px-nav-item px-nav-dropdown">
            <a href="#"><i class="px-nav-icon ion-shuffle"></i><span class="px-nav-label">Verifikasi</span></a>
            <ul class="px-nav-dropdown-menu">
              <li class="px-nav-item"><a href="/listverify/all">WO</a></li>
              <li class="px-nav-item"><a href="/tech">Absensi</a></li>
              <!-- <li class="px-nav-item {{ Request::segment(1) == 'verifRemo'  ? 'active' : '' }}"><a href="/verifRemo">WO REMO( HD )</a></li> -->
            </ul>
          </li>
          @endif

          @if ($lvl)
          <li class="px-nav-item px-nav-dropdown">
            <a href="#"><i class="px-nav-icon ion-ios-bookmarks"></i><span class="px-nav-label">Reporting</span></a>
            <ul class="px-nav-dropdown-menu">
              @if($lvl == 1 || $lvl == 2 || $lvl == 3 || $lvl == 4)
                <li class="px-nav-item"><a href="/reportUmur/{{date('Y-m-d')}}/3">Umur Odp Loss</a></li>
                <li class="px-nav-item"><a href="/reportUmur/{{date('Y-m-d')}}/5">Umur Utilitas</a></li>
                <li class="px-nav-item"><a href="/reportUmur/{{date('Y-m-d')}}/6">Umur Insert Tiang</a></li>
                <li class="px-nav-item"><a href="/verify/{{date('Y-m')}}">Dashboard Verifikasi</a></li>
                <!-- <li class="px-nav-item"><a href="/remaining_saldo/{{date('Y-m')}}">Dashboard Sisa Saldo</a></li> -->

              @endif
              @if($lvl == 1 || $lvl == 5 || $lvl == 6)
                <li class="px-nav-item"><a href="/decline/wo">Dashboard Decline</a></li>
              @endif
              @if($lvl == 1 || $lvl == 2 || $lvl == 3 || $lvl == 4)
                @if(!$isBpp)
                <li class="px-nav-item"><a href="/odpLossC/{{date('Y-m')}}">Matrix Tomman PSB x Marina</a></li>
                @endif
                <li class="px-nav-item"><a href="/reportAbsen/{{date('Y-m-d')}}">Absen</a></li>
              @endif
              <li class="px-nav-item"><a href="/produktifitas/{{date('Y-m')}}">Rekap Closing</a></li>
              <li class="px-nav-item"><a href="/download">Download</a></li>
              <li class="px-nav-item"><a href="/download_undersp">Download Underspeck</a></li>
            </ul>
          </li>
          <li class="px-nav-item px-nav-dropdown">
            <a href="#"><i class="px-nav-icon ion-cash"></i><span class="px-nav-label">Revenue&Cost</span></a>
            <ul class="px-nav-dropdown-menu">
              @if ($lvl == 1)
            <!--<li><a href="/dashboard/revenue/MITRA/{{date('Y-m')}}">Mitra</a></li>
            <li><a href="/dashboard/revenue/TA-TELKOM/{{date('Y-m')}}">TA-Telkom</a></li>
            <li><a href="/revenueTelkom/{{date('Y-m')}}">TA-Telkom</a>-->
              <li class="px-nav-item"><a href="/revenue/program/{{date('Y-m')}}">Per Program</a></li>
              @endif
              @if ($lvl != 4)
              <li class="px-nav-item"><a href="/revenuev2/MITRA/{{date('Y-m')}}">Cost(KHS Mitra)</a></li>
              @endif
              @if ($lvl == 1 || $lvl == 4)
              <li class="px-nav-item"><a href="/revenuev2/TELKOM/{{date('Y-m')}}">Cost(KHS Telkom)</a></li>
              @endif
            </ul>
          </li>
          <li class="px-nav-item px-nav-dropdown">
            <a href="#"><i class="px-nav-icon ion-podium"></i><span class="px-nav-label">Produktifitas</span></a>
            <ul class="px-nav-dropdown-menu">
                <li class="px-nav-item"><a href="/matrix/{{date('Y-m-d')}}/0/all/all">ALL</a></li>
                <li class="px-nav-item"><a href="/prod_all">Jenis Order</a></li>
                <!-- <li class="px-nav-item {{ Request::segment(1) == 'prod_sector'  ? 'active' : '' }}"><a href="/prod_sector">Sektor ALL</a></li> -->
                <li class="px-nav-item"><a href="/prod_sektor_detil/{{ date('Y-m-d') }}">Sektor ALL</a></li>
                <!-- <li class="px-nav-item {{ Request::segment(1) == 'dptiang'  ? 'active' : '' }}"><a href="/dptiang/{{ date('Y-m') }}">DELTA C</a></li> -->
                <li class="px-nav-item"><a href="/mitra/prod">Mitra</a></li>
                <li class="px-nav-item"><a href="/saber/{{ date('Y-m') }}">Tim Saber</a></li>
            </ul>
          </li>
          @endif
          <!-- @if($lvl == 1)
          <li class="px-nav-item px-nav-dropdown {{ str_contains(Request::segment(1), ['commerce'])? 'px-open active' : '' }}">
            <a href="#">Rekon TA/TELKOM</a>
            <ul class="px-nav-dropdown-menu">
              <li class="px-nav-item {{ (Request::segment(1) == 'commerce' && Request::segment(2) == 'admin') ? 'active' : '' }}"><a href="/commerce/admin/list">Index Admin</a></li>
              <li class="px-nav-item {{ (Request::segment(1) == 'commerce' && Request::segment(2) == 'approval') ? 'active' : '' }}"><a href="/commerce/approval/list">Index Approval</a></li>
            </ul>
          </li>
          @endif
          @if($lvl == 1)
          <li class="px-nav-item px-nav-dropdown {{ str_contains(Request::segment(1), ['proc'])? 'px-open active' : '' }}">
            <a href="#">Rekon TA/MITRA</a>
            <ul class="px-nav-dropdown-menu">
              <li class="px-nav-item {{ (Request::segment(2) == 'admin') ? 'active' : '' }}"><a href="/proc/admin/list">Index Admin</a></li>
              <li class="px-nav-item {{ (Request::segment(2) == 'pembuatansp') ? 'active' : '' }}"><a href="/proc/pembuatansp/list">Index Pembuatan SP/Kontrak</a></li>
              <li class="px-nav-item {{ (Request::segment(2) == 'pemberkasan') ? 'active' : '' }}"><a href="/proc/pemberkasan/list">Index Pemberkasan</a></li>
              <li class="px-nav-item {{ (Request::segment(2) == 'verifikasi') ? 'active' : '' }}"><a href="/proc/verifikasi/list">Index Verifikasi</a></li>
              <li class="px-nav-item {{ (Request::segment(2) == 'pengirimandoc') ? 'active' : '' }}"><a href="/proc/pengirimandoc/list">Index Pengiriman Doc</a></li>
              <li class="px-nav-item {{ (Request::segment(2) == 'finance') ? 'active' : '' }}"><a href="/proc/finance/list">Finance</a></li>
              <li class="px-nav-item {{ (Request::segment(2) == 'report') ? 'active' : '' }}"><a href="/proc/report/list">Reporting</a></li>
            </ul>
          </li>
          @endif -->
          @if(!$isBpp)
          @if($lvl == 1 || $lvl == 2 || $lvl == 3 || $lvl == 4)
          <li class="px-nav-item px-nav-dropdown">
            <a href="#"><i class="px-nav-icon ion-wrench"></i><span class="px-nav-label">Repair ALPRO</span></a>
            <ul class="px-nav-dropdown-menu">
              <li class="px-nav-item"><a href="/repair/listusulanteknisi">List Usulan Teknisi</a></li>
              <li class="px-nav-item"><a href="/repair/input">Input Usulan</a></li>
              <li class="px-nav-item"><a href="/repair/matrik">Matrik</a></li>
              <li class="px-nav-item"><a href="/repair/approval">Approval</a></li>
              <li class="px-nav-item"><a href="/repair/dispatch">Dispatch</a></li>
            </ul>
          </li>
          @endif
          @endif
          @if($lvl)
          <li class="px-nav-item px-nav-dropdown">
            <a href="#"><i class="px-nav-icon ion-android-contacts"></i><span class="px-nav-label">Peduli ODP</span></a>
            <ul class="px-nav-dropdown-menu">
              <li class="px-nav-item"><a href="/list_saldo/alpro/{{date('Y-m')}}">Dashboard</a></li>
              <li class="px-nav-item"><a href="/kalender_alpro/{{ date('Y-m') }}"></i>Kalender</a></li>
              <li class="px-nav-item"><a href="/lapor_active"></i>Pelapor Aktif</a></li>
            </ul>
          </li>
          @endif
          @if($lvl == 1)
          <li class="px-nav-item px-nav-dropdown">
            <a href="#"><i class="px-nav-icon ion-ios-gear"></i><span class="px-nav-label">Setting</span></a>
            <ul class="px-nav-dropdown-menu">
              <li class="px-nav-item"><a href="/mitra">Mitra</a></li>
              <li class="px-nav-item"><a href="/sto">STO</a></li>
              <li class="px-nav-item"><a href="/teritori">Teritory</a></li>
              <li class="px-nav-item"><a href="/mapping_odc">Mapping ODC</a></li>
              <li class="px-nav-item"><a href="/user">User</a></li>
              <li class="px-nav-item"><a href="/newuser">Approval User</a></li>
              <li class="px-nav-item"><a href="/regu">Regu</a></li>
              <li class="px-nav-item"><a href="/khs">KHS</a></li>
              <li class="px-nav-item"><a href="/grand_sett">Program</a></li>
            </ul>
          </li>
          <!-- <li class="px-nav-item px-nav-dropdown {{ str_contains(Request::segment(1), ['logistik', 'stok'])? 'px-open active' : '' }}">
            <a href="#">Tools</a>
            <ul class="px-nav-dropdown-menu">
              <li class="px-nav-item {{ Request::segment(1) == 'logistik'  ? 'active' : '' }}"><a href="/logistik">Alista</a></li>
              <li class="px-nav-item {{ Request::segment(1) == 'khs'  ? 'stok' : '' }}"><a href="/stok">Alista Stok</a></li>
            </ul>
          </li> -->
          @endif
          @if(!$lvl)
          <li class="px-nav-item"><a href="/hometech"><i class="px-nav-icon fa fa-home"></i><span class="px-nav-label">Home Teknisi</span></a></li>
          <li class="px-nav-item"><a href="/user/{{ session('auth')->id_user }}"><i class="px-nav-icon fa fa-user"></i><span class="px-nav-label">Profile</span></a></li>
          <li class="px-nav-item"><a href="/listabsen"><i class="px-nav-icon fa fa-calendar-check-o"></i><span class="px-nav-label">List Absen</span></a></li>
          <li class="px-nav-item"><a href="/repair/usulanteknisi"><i class="px-nav-icon fa fa-hand-paper-o"></i><span class="px-nav-label">Usulan Repair Alpro</span></a></li>
          <li class="px-nav-item"><a href="/nog_manual/input"><i class="px-nav-icon fa fa-hand-paper-o"></i><span class="px-nav-label">Lapor ODP Gendong</span></a></li>
          <!-- <li class="px-nav-item"><a href="/registerODPTerbuka/input"><i class="px-nav-icon fa fa-eye"></i><span class="px-nav-label">Lapor ODP Terbuka</span></a></li> -->
          @endif
          @if($lvl==2)
          <li class="px-nav-item px-nav-dropdown">
            <a href="#"><i class="px-nav-icon ion-shuffle"></i><span class="px-nav-label">Performansi</span></a>
            <ul class="px-nav-dropdown-menu">
              <li class="px-nav-item"><a href="/kpi">KPI</a></li>
              <li class="px-nav-item"><a href="/order_manual">Order Manual</a></li>
            </ul>
          </li>
          @endif
          <li class="px-nav-item">
            <a href="https://perwira.tomman.app/input_potensi_b/{{ session('auth')->id_karyawan }}"><i class="px-nav-icon fa fa-life-buoy"></i><span class="px-nav-label">Lapor Potensi Bahaya</span></a>
          </li>
          <li class="px-nav-item"><a href="/theme"><i class="px-nav-icon fa fa-shopping-basket"></i><span class="px-nav-label">Theme</span></a></li>
          <li class="px-nav-item"><a href="/logout"><i class="px-nav-icon fa fa-sign-out"></i><span class="px-nav-label">Logout</span></a></li>
        </ul>
      </nav>

      <!-- Navbar -->
      <nav class="navbar px-navbar">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">MARINA</a>
        </div>

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#px-navbar-collapse" aria-expanded="false"><i class="navbar-toggle-icon"></i></button>

        <div class="collapse navbar-collapse" id="px-navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li>
              <form class="navbar-form" role="search" action="/order/none/none/{{ date('Y-m') }}/all">
                <div class="form-group">
                  <input type="text" name="q" class="form-control layout_search" placeholder="Cari Tiket Atau ODP" style="width: 140px;">
                </div>
              </form>
            </li>
          </ul>
        </div>
      </nav>
      <div id="block-alert-with-timer" class="m-b-1"></div>
      <div class="px-content">
        <div class="page-header">
            @yield('heading')
        </div>
        <!-- Content -->
        <div>
          @yield('content')
        </div>
      </div>

      <!-- Footer -->
<!--   <footer class="px-footer px-footer-bottom">
      Marina
    </footer> -->

  <!-- ==============================================================================
  |
  |  SCRIPTS
  |
  =============================================================================== -->

  <!-- Load jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Core scripts -->
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/pixeladmin.min.js"></script>

  <!-- Your scripts -->
  <script type="text/javascript">
    $(function(){
      var alertExist = <?= json_encode(Session::has('alerts')); ?>;
      if(alertExist){
        toastr.options = {
          "closeButton": true,
          "positionClass": "toast-top-center",
          "onclick": null,
          "timeOut": "200000"
        }
        var msg = <?= json_encode(session('alerts')[0]); ?>;
        toastr.info(msg.text);
      }

      var alertBlock = <?= json_encode(Session::has('alertblock')); ?>;
      if(alertBlock){
        var $container = $('#block-alert-with-timer');
        var alrt = <?= json_encode(session('alertblock')[0]); ?>;
        $container.pxBlockAlert(alrt.text, { type: alrt.type, style: 'light', timer: 3 });
      }
      var file = window.location.pathname;

      $('body > .px-nav')
      .find('.px-nav-item > a[href="' + file + '"]')
      .parent()
      .addClass('active');

      $('body > .px-nav').pxNav();
      $('body > .px-footer').pxFooter();
    });
  </script>
  @yield('script')
</body>
</html>
