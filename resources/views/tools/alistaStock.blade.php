@extends('layout2')
@section('head')
@endsection
@section('content')
<?php
    $no=0;
?>
@foreach($data as $da)
    <div class="row col-sm-4">
    <div class="panel panel-default">
        <div class="panel-heading">Stock</div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped" id="table">
                <tr>
                    <th>ID DESIGNATOR</th>
                    <th>VOL</th>
                </tr>
                @foreach($da as $d)
                <tr>
                    <td>{{ $d->id_barang }}</td>
                    <td>{{ $d->total_stok }}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endforeach
@endsection
@section('script')
@endsection