
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;\
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
.table {
  width: 24%;
  float: left;
  margin-left: 10px;
  border: 1px solid red;
}
p{
    margin-left:10px;
    float: left;
    font-size:32;
}
</style>
<?php
    $no=0;
?>
@foreach($data as $no=> $da)
<table border="1" class="table">
    @if($no == 3)
        <p>Stok WH Banjarmasin Area<br/>
            Syncron by: TommaN<br/>
        {{ date('d-F-Y H:i:s') }}</p>
    @endif
    <tr>
        <th>ID DESIGNATOR</th>
        <th>VOL</th>
    </tr>
    @foreach($da as $d)
    <tr>
        <td>{{ $d->id_barang }}</td>
        <td>{{ $d->total_stok }}</td>
    </tr>
    @endforeach
</table>
@endforeach