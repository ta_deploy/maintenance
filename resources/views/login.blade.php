<!doctype html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>MARINA</title>

    <link rel="stylesheet" href="/bower_components/bootswatch-dist/css/bootstrap.min.css" />
    <!-- <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css" /> -->
    <link rel="stylesheet" href="/style.css" />

    <style>
        @media(min-width: 769px) {
            form {
                width: 400px;
                margin: 100px auto;
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <form method="post">
            @include('partial.alert')
            
            <div style="margin-bottom:15px;"><img src="/asset/img/backgound/logo-marina.png" /></div>
            
            <div class="form-group">
                <label for="txtLogin" class="control-label">Login</label>
                <input type="text" name="login" id="txtLogin" class="form-control" autofocus />
            </div>
            <div class="form-group">
                <label for="txtPass" class="control-label">Password</label>
                <input type="password" class="form-control" name="password" id="txtPass" />
            </div>
            <div>
                <button class="btn btn-primary">
                    <span class="glyphicon glyphicon-lock"></span>
                    <span>Login</span>
                </button>
            </div>
        </form>
    </div>
</body>
</html>