@extends('layout2')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LIST KHS</span>
</h1>
<a href="/khs/create" class="btn btn-info btn-xs pull-right" style="margin: 0 0 5px 0;">
        <span class="glyphicon glyphicon-plus"></span>
        <span>Input Baru</span>
    </a>
@endsection
@section('content')
    
    <div class="panel panel-default" id="info">
        <div id="fixed-table-container-demo" class="fixed-table-container">
            <table class="table table-bordered table-fixed">
                <thead>
                  <tr>
                    <th width="20" class="head">Id Item</th>
                    <th width="20" class="head">uraian</th>
                    <th width="20" class="head">Jasa Telkom</th>
                    <th width="20" class="head">Material Telkom</th>
                    <th width="20" class="head">Jasa TA</th>
                    <th width="20" class="head">Material TA</th>
                    <th width="20" class="head">Jenis KHS</th>
                    <th width="20" class="head">Alista</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($data as $no => $list) 
                        <tr>
                            <td><a href="/khs/{{ $list->id }}">{{ $list->id_item }}</a></td>
                            <td>{{ $list->uraian }}</td>
                            <td>{{ number_format($list->jasa_telkom) }}</td>
                            <td>{{ number_format($list->material_telkom) }}</td>
                            <td>{{ number_format($list->jasa_ta) }}</td>
                            <td>{{ number_format($list->material_ta) }}</td>
                            <td>{{ $list->jenis_khs }}</td>
                            <td>{{ $list->alista_designator }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection