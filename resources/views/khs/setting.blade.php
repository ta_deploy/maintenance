@extends('layout2')
@section('head')
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Setting Program {{ $data->nama_order or 'Baru' }}</span>
</h1>
@endsection
@section('content')
    @if (session('auth')->maintenance_level == '1')
        <div class="box">
        <div class="box-row">
            <div class="box-cell">
                <div class="panel panel-default m-a-1">
                    <div class="panel-heading">Program</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="post" id="form-register" action="/setting/{{ Request::segment(2) }}">
                            <div class="form-group">
                                <label for="nama_order" class="col-md-2">Nama Program</label>
                                <div class="col-md-4 form-message-dark">
                                    <input type="text" name="nama_order" id="nama_order" class="form-control" required value="{{ @$data->nama_order }}" />
                                </div>
                                <label for="aktif" class="col-md-1 control-label">Status</label>
                                <div class="col-md-3 form-message-dark">
                                    <input type="text" name="aktif" id="aktif" class="form-control" required value="{{ @$data->aktif }}"/>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary pull-right"><i class="ion-soup-can"></i> Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
        @php
            $temp = [
                (object)['id' => 'order_id', 'text' => 'Order ID', 'visible' => '', 'require' => ''],
                // (object)['id' => 'Jenis_Order', 'text' => 'Jenis Order', 'visible' => '', 'require' => ''],
                (object)['id' => 'kategori', 'text' => 'Kategory', 'visible' => '', 'require' => ''],
                (object)['id' => 'no_inet', 'text' => 'No.INET', 'visible' => '', 'require' => ''],
                (object)['id' => 'nama_odp', 'text' => 'Nama ODP', 'visible' => '', 'require' => ''],
                (object)['id' => 'headline', 'text' => 'Headline', 'visible' => '', 'require' => ''],
                (object)['id' => 'pic', 'text' => 'PIC', 'visible' => '', 'require' => ''],
                (object)['id' => 'koordinat', 'text' => 'Koordinat', 'visible' => '', 'require' => ''],
                (object)['id' => 'regu', 'text' => 'Order ke Tim', 'visible' => '', 'require' => ''],
                (object)['id' => 'sektor', 'text' => 'Sektor', 'visible' => '', 'require' => ''],
                (object)['id' => 'timeplan', 'text' => 'Timeplan', 'visible' => '', 'require' => ''],
                (object)['id' => 'sto', 'text' => 'STO', 'visible' => '', 'require' => '']
            ];

            $field_order = json_decode(@$jenis_order->field_register_order) ?: $temp;
            //cek if ada tambahan field
            if(count($field_order) != count($temp) )
            {
                for($i=count($field_order);$i<count($temp);$i++)
                {
                    echo $i.$temp[$i]->id.'<br>';
                    array_push($field_order,$temp[$i]);
                }
            }

            // $temp_laporan = [(object)['id'=>'Jenis_Splitter','text'=>'Jenis Splitter','visible'=>'','require'=>''],
            // (object)['id'=>'Jumlah_Port_ODP_Kosong','text'=>'Jumlah Port ODP Kosong','visible'=>'','require'=>''],
            // (object)['id'=>'Jumlah_Port_ODP_Isi','text'=>'Jumlah Port ODP Isi','visible'=>'','require'=>''],
            // (object)['id'=>'Action_Dropdown','text'=>'Action Dropdown','visible'=>'','require'=>''],
            // (object)['id'=>'Penyebab_Dropdown','text'=>'Penyebab Dropdown','visible'=>'','require'=>''],
            // (object)['id'=>'Edit_Nama_Odp','text'=>'Edit Nama Odp','visible'=>'','require'=>''],
            // (object)['id'=>'Koordinat','text'=>'Koordinat','visible'=>'','require'=>''],
            // (object)['id'=>'Headline','text'=>'Headline','visible'=>'','require'=>''],
            // (object)['id'=>'Upload_ABD','text'=>'Upload ABD','visible'=>'','require'=>''],
            // (object)['id'=>'Action_Detail','text'=>'Action Detail','visible'=>'','require'=>'']];

            $temp_laporan = [
                (object)['id' => 'speedy', 'text' => 'NO_Speedy Ibooster', 'visible' => '', 'required' => ''],
                (object)['id' => 'status_detail', 'text' => 'Detail Status', 'visible' => '', 'required' => ''],
                (object)['id' => 'redaman_awal', 'text' => 'Redaman Sebelum', 'visible' => '', 'required' => ''],
                (object)['id' => 'redaman_akhir', 'text' => 'Redaman Sesudah', 'visible' => '', 'required' => ''],
                (object)['id' => 'splitter', 'text' => 'Jenis Splitter', 'visible' => '', 'required' => ''],
                (object)['id' => 'nog_project', 'text' => 'NOG PROJECT', 'visible' => '', 'required' => ''],
                (object)['id' => 'port_idle', 'text' => 'Jumlah Port ODP Kosong', 'visible' => '', 'required' => ''],
                (object)['id' => 'port_used', 'text' => 'Jumlah Port ODP Isi', 'visible' => '', 'required' => ''],
                (object)['id' => 'action_cause', 'text' => 'Action', 'visible' => '', 'required' => ''],
                (object)['id' => 'splitter_jenis', 'text' => 'Status Splitter!', 'visible' => '', 'required' => ''],
                (object)['id' => 'valins_awal', 'text' => 'Valins Awal', 'visible' => '', 'required' => ''],
                (object)['id' => 'valins_akhir', 'text' => 'Valins Akhir', 'visible' => '', 'required' => ''],
                (object)['id' => 'gamas_cause', 'text' => 'Penyebab Gamas!', 'visible' => '', 'required' => ''],
                (object)['id' => 'penyebab_id', 'text' => 'Penyebab', 'visible' => '', 'required' => ''],
                (object)['id' => 'nama_odp', 'text' => 'Edit Nama Odp', 'visible' => '', 'required' => ''],
                (object)['id' => 'koordinat', 'text' => 'Koordinat', 'visible' => '', 'required' => ''],
                (object)['id' => 'koordinat_tiang', 'text' => 'Koordinat Tiang', 'visible' => '', 'required' => ''],
                (object)['id' => 'headline', 'text' => 'Headline', 'visible' => '', 'required' => ''],
                (object)['id' => 'abd', 'text' => 'Upload ABD', 'visible' => '', 'required' => ''],
                (object)['id' => 'action', 'text' => 'Action', 'visible' => '', 'required' => ''],
                (object)['id' => 'discovery', 'text' => 'Discovery info', 'visible' => '', 'required' => ''],
                (object)['id' => 'valin_bot', 'text' => 'Isi Bot Valin', 'visible' => '', 'require' => '']
            ];

            $field_laporan = json_decode(@$jenis_order->field_laporan_order) ?: $temp_laporan;
            //cek if ada tambahan field
            // dd(json_decode($jenis_order->field_laporan_order), $temp_laporan);

            // if(count($field_laporan) != count($temp_laporan)){
            //     for($i=count($field_laporan);$i<count($temp_laporan);$i++){
            //         array_push($field_laporan, $temp_laporan[$i]);
            //     }
            // }

            foreach($temp_laporan as $k => &$v)
            {
                $check = json_decode(json_encode($field_laporan), TRUE);

                $check_data = array_search($v->id, array_column($check, 'id') );

                if($check_data !== FALSE)
                {
                    unset($temp_laporan[$k]);
                }
            }

            unset($v);
            $field_laporan = array_merge($field_laporan, $temp_laporan);
        @endphp
        @if($data)
            <div class="box">
                <div class="box-row">
                    <div class="box-cell">
                        <div class="panel panel-default m-a-1">
                            <div class="panel-heading">Setting Field Register Order <button type="button" class="btn btn-xs btn-primary pull-right btn-reg-order"><i class="ion-soup-can"></i> Simpan</button></div>
                            <div class="panel-body">
                                <form method="post" id="field_register_order_form" action="/setRegisterOrder/{{ Request::segment(2) }}">
                                    <input type="hidden" name="field_register_order">
                                </form>
                            <div class="box-container" id="listcheck">
                                <div class="box-row valign-middle">
                                <div class="box-cell p-y-1">
                                    <div class="font-size-11">FIELD</div>
                                </div>
                                <div class="box-cell p-y-1">
                                    <div class="font-size-11">VISIBLE</div>
                                </div>
                                <div class="box-cell p-y-1">
                                    <div class="font-size-11">REQUIRED</div>
                                </div>
                                </div>
                                    <div class="box-row valign-middle"  v-repeat="$data">
                                    <div class="box-cell p-y-1 b-t-1">
                                        <div class="font-size-11" v-text="text"></div>
                                    </div>
                                    <div class="box-cell p-y-1 b-t-1">
                                        <label class="switcher switcher-rounded switcher-primary">
                                        <input type="checkbox" v-model="visible">
                                        <div class="switcher-indicator">
                                            <div class="switcher-yes">YES</div>
                                            <div class="switcher-no">NO</div>
                                        </div>
                                        </label>
                                    </div>
                                    <div class="box-cell p-y-1 b-t-1">
                                        <label class="switcher switcher-rounded switcher-primary">
                                        <input type="checkbox" v-model="require">
                                        <div class="switcher-indicator">
                                            <div class="switcher-yes">YES</div>
                                            <div class="switcher-no">NO</div>
                                        </div>
                                        </label>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-cell">
                        <div class="panel panel-default m-a-1">
                            <div class="panel-heading">Setting Field Laporan jika status Close <button type="submit" class="btn btn-xs btn-primary pull-right btn-laporan-order"><i class="ion-soup-can"></i> Simpan</button></div>
                            <div class="panel-body">
                                <form method="post" id="field_laporan_order_form" action="/setLaporanOrder/{{ Request::segment(2) }}">
                                    <input type="hidden" name="field_laporan_order">
                                </form>
                                <div class="box-container" id="listlaporan">
                                <div class="box-row valign-middle">
                                <div class="box-cell p-y-1">
                                    <div class="font-size-11">FIELD</div>
                                </div>
                                <div class="box-cell p-y-1">
                                    <div class="font-size-11">VISIBLE</div>
                                </div>
                                <div class="box-cell p-y-1">
                                    <div class="font-size-11">REQUIRED</div>
                                </div>
                                </div>
                                <div class="box-row valign-middle"  v-repeat="$data">
                                    <div class="box-cell p-y-1 b-t-1">
                                        <div class="font-size-11" v-text="text"></div>
                                    </div>
                                    <div class="box-cell p-y-1 b-t-1">
                                        <label class="switcher switcher-rounded switcher-primary">
                                        <input type="checkbox" v-model="visible">
                                        <div class="switcher-indicator">
                                            <div class="switcher-yes">YES</div>
                                            <div class="switcher-no">NO</div>
                                        </div>
                                        </label>
                                    </div>
                                    <div class="box-cell p-y-1 b-t-1">
                                        <label class="switcher switcher-rounded switcher-primary">
                                        <input type="checkbox" v-model="require">
                                        <div class="switcher-indicator">
                                            <div class="switcher-yes">YES</div>
                                            <div class="switcher-no">NO</div>
                                        </div>
                                        </label>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="box-row">
                    <div class="box-cell">
                        <div class="panel panel-default m-a-1">
                            <div class="panel-heading">Laporan Material & Jasa</div>
                            <div class="panel-body">
                                <form method="post" class="form-horizontal" action="/materialsetting/{{ Request::segment(2) }}">
                                    <input name="hid" type="hidden" value="{{ $data->id }}"/>
                                    <div class="form-group">
                                        <div class="col-sm-8">
                                            <input name="id_item" type="text" id="id_item" class="form-control" required />
                                            @foreach($errors->get('id_item') as $msg)
                                            <span class="help-block">{{ $msg }}</span>
                                            @endforeach
                                        </div>
                                        <div class="col-sm-offset-2 col-md-offset-2 col-sm-2">
                                            <button class="btn btn-primary pull-right" type="submit">
                                                <span class="ion-plus"></span>
                                                <span>Tambah</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                    </div>
                                </form>
                                <div class="row">
                                    @foreach($itemModif as $i)
                                    <div class="col-sm-4">
                                        <div class="box bg-primary darken">
                                            <div class="box-cell p-x-3 p-y-1">
                                                <a href="#">
                                                    <div class="font-weight-semibold font-size-12">{{ $i->id_item }}</div>
                                                    <div class="font-weight-semibold font-size-12"><span style="color: black;" class="badge badge-warning">{{ $i->jenis_khs }}</span></div>
                                                    <div class="font-weight-semibold font-size-12"><span style="color: black;" class="badge badge-info">{{ $i->created_at }}</span></div>
                                                    <a href="#" style="float: right;" class="btn btn_rmv btn-warning m-t-1 pull-right" data-id="{{$i->id}}">Delete</a>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-cell">
                        <div class="panel panel-default m-a-1">
                            <div class="panel-heading">Laporan Evident</div>
                            <div class="panel-body">
                                <form method="post" class="form-horizontal" action="/evidentsetting/{{ Request::segment(2) }}">
                                    <div class="form-group {{ $errors->has('id_item') ? 'has-error' : '' }}">
                                        <div class="col-sm-5">
                                            <input name="evident" type="text" id="evident" class="form-control"
                                            placeholder="Evident" required />
                                        </div>
                                        <div class="col-sm-5">
                                            <input name="hukum" type="text" id="hukum" class="form-control"
                                            placeholder="Hukum" required />
                                        </div>
                                        <div class="col-sm-2">
                                            <button class="btn btn-primary pull-right" type="submit">
                                                <span class="ion-plus"></span>
                                                <span>Tambah</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                    </div>
                                </form>
                                <div class="row">
                                    @foreach($evident as $i)
                                    <div class="col-md-3">
                                        <div class="box bg-primary darken">
                                            <div class="box-cell p-x-3 p-y-1">
                                                <a href="#">
                                                    <div class="font-weight-semibold font-size-12">{{ $i->evident }}</div>
                                                    <div class="font-weight-semibold font-size-12"><span style="color: black;" class="badge badge-warning">{{ $i->hukum }}</span></div>
                                                    <a href="#" style="float: right;" class="btn btn_rmv_ev btn-warning m-t-1 pull-right" data-id="{{$i->id}}">Delete</a>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @else
        Harus Admin
    @endif
@endsection
@section('script')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="/bower_components/vue/dist/vue.min.js"></script>
<script>
    $(function() {
        var item = {!! json_encode($item) !!},
        field_order = {!! json_encode($field_order) ?? Null !!},
        field_laporan = {!! json_encode($field_laporan) !!};
        console.log(item, field_order);

        $('.btn-reg-order').click(function(){
            var field_order_json = [];
            field_order.forEach(function(item) {
                field_order_json.push({id: item.id, text: item.text, require: item.require, visible: item.visible});
            });

            $('input[name=field_register_order]').val(JSON.stringify(field_order_json));
            $('#field_register_order_form').submit();
        });
        var listVm = new Vue({
          el: '#listcheck',
          data: field_order
        });

        $('.btn-laporan-order').click(function(){
            var field_laporan_order_json = [];
            field_laporan.forEach(function(item) {
                field_laporan_order_json.push({id: item.id, text: item.text, require: item.require, visible: item.visible});
            });
            $('input[name=field_laporan_order]').val(JSON.stringify(field_laporan_order_json));
            $('#field_laporan_order_form').submit();
        });

        var listVm = new Vue({
          el: '#listlaporan',
          data: field_laporan
        });
        $('#id_item').select2({
            data: item,
            placeholder: 'Select Designator'
        });
        $('#hukum').select2({
            data: [{id:"require",text:"require"},{id:"optional",text:"optional"}],
            placeholder: 'Select Hukum'
        });
        $('#hukum').val('require').change();
        $('#aktif').select2({
            data: [{id:"0",text:"Non-Aktif"},{id:"1",text:"Aktif"}],
            placeholder: 'Select Status'
        });

        $('.btn_rmv').on('click',function(){
            Swal.fire({
                title: $(this).prev().text()+' Akan Dihapus',
                text: "Data tidak bisa dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: '/delete/setting',
                        data: {'id': $(this).attr('data-id')}
                    }).done(function(){
                        Swal.fire(
                            'Dihapus!',
                            'Data Berhasil Dihapus!',
                            'success'
                            );
                        setTimeout(function(){
                         location.reload();
                     }, 2000);
                    });
                }
            })
        });
        $('.btn_rmv_ev').on('click',function(){
            Swal.fire({
                title: $(this).prev().text()+' Akan Dihapus',
                text: "Data tidak bisa dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: '/evidentDel',
                        data: {'id': $(this).attr('data-id')}
                    }).done(function(){
                        Swal.fire(
                            'Dihapus!',
                            'Data Berhasil Dihapus!',
                            'success'
                            );
                        setTimeout(function(){
                         location.reload();
                     }, 2000);
                    });
                }
            })
        });
    });
</script>
@endsection