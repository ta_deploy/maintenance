@extends('layout2')
@section('head')
  <style type="text/css">
    th{
        text-align: center;
    }
  </style>
@endsection
@section('content')
    <div class="panel panel-default" id="info">
        <div class="panel-heading">REVENUE {{Request::segment(3)}} BULAN {{Request::segment(4)}} ON : <span class="clock"></span></div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-fixed">
                <tr>
                    <th style="vertical-align:middle" rowspan=2>#</th>
                    <th style="vertical-align:middle" rowspan=2>MITRA</th>
                    <th colspan=3>VERIFIKASI</th>
                    <th rowspan=2>SIAP REKON</th>
                    <th rowspan=2>TOTAL</th>
                    @if(Request::segment(3)=='MITRA')
                        <th>TANAM TIANG CLOSE</th>
                        <th>POTENSI PSB</th>
                    @endif
                </tr>
                <tr>
                    <th bgcolor="#8E001C">TL</th>
                    <th bgcolor="#228B22">SM</th>
                    <th bgcolor="#425ff4">TELKOM</th>
                </tr>
                <?php
                    $total=0;$totalTl=0;$totalSm=0;$totalTm=0;$totalSr=0;
                    $tl=0;$sm=0;$telkom=0;$sr=0;
                ?>
                @foreach($data as $no => $d)
                    @if(session('auth')->nama_instansi == 'TELKOM AKSES' || session('auth')->nama_instansi == $d->mitra)
                    @if($d->id!=3)
                    @if(Request::segment(3)=='TA-TELKOM')
                    <?php
                        $tl=0;$sm=0;$telkom=0;$sr=0;$ttl=0;
                        $tl=$d->tl_jasa+$d->tl_mtr;$sm=$d->sm_jasa+$d->sm_mtr;$telkom=$d->telkom_jasa+$d->telkom_mtr;$sr=$d->siap_rekon_jasa+$d->siap_rekon_mtr;
                        $ttl=$d->total_jasa+$d->total_mtr;
                    ?>
                    @endif
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $d->mitra }}</td>
                        <td bgcolor="#8E001C" align="right"><a href="/dashboard/rincianRevenue/{{Request::segment(3)}}/{{$d->mitra}}/null/{{Request::segment(4)}}">{{ Request::segment(3)=='TA-TELKOM'? number_format($tl) : number_format($d->tl) }}</a></td>
                        <td bgcolor="#228B22" align="right"><a href="/dashboard/rincianRevenue/{{Request::segment(3)}}/{{$d->mitra}}/1/{{Request::segment(4)}}">{{ Request::segment(3)=='TA-TELKOM'? number_format($sm) : number_format($d->sm )}}</a></td>
                        <td bgcolor="#425ff4" align="right"><a href="/dashboard/rincianRevenue/{{Request::segment(3)}}/{{$d->mitra}}/2/{{Request::segment(4)}}">{{ Request::segment(3)=='TA-TELKOM'? number_format($telkom) : number_format($d->telkom) }}</a></td>
                        <td align="right"><a href="/dashboard/rincianRevenue/{{Request::segment(3)}}/{{$d->mitra}}/3/{{Request::segment(4)}}">{{ Request::segment(3)=='TA-TELKOM'? number_format($sr) : number_format($d->siap_rekon) }}</a></td>
                        <td align="right"><a href="/dashboard/rincianRevenue/{{Request::segment(3)}}/{{$d->mitra}}/all/{{Request::segment(4)}}">{{ Request::segment(3)=='TA-TELKOM'? number_format($ttl) : number_format($d->total) }}</a></td>
                        @if(Request::segment(3)=='MITRA')
                            <td align="right">{{ $d->tanam_tiang }}</td>
                            <td align="right">{{ number_format($d->tanam_tiang*414813) }}</td>
                        @endif
                    </tr>
                    <?php
                        $total+=$d->total;$totalTl+=$d->tl;$totalSm+=$d->sm;$totalTm+=$d->telkom;$totalSr+=$d->siap_rekon;
                    ?>
                    @endif
                    @endif
                @endforeach
                <tr><td colspan=2>TOTAL</td>
                    <td align="right"><a href="/dashboard/rincianRevenue/{{Request::segment(3)}}/all/null/{{Request::segment(4)}}">{{ number_format($totalTl) }}</a></td>
                    <td align="right"><a href="/dashboard/rincianRevenue/{{Request::segment(3)}}/all/1/{{Request::segment(4)}}">{{ number_format($totalSm) }}</a></td>
                    <td align="right"><a href="/dashboard/rincianRevenue/{{Request::segment(3)}}/all/2/{{Request::segment(4)}}">{{ number_format($totalTm) }}</a></td>
                    <td align="right"><a href="/dashboard/rincianRevenue/{{Request::segment(3)}}/all/3/{{Request::segment(4)}}">{{ number_format($totalSr) }}</a></td>
                    <td align="right"><a href="/dashboard/rincianRevenue/{{Request::segment(3)}}/all/all/{{Request::segment(4)}}">{{ number_format($total) }}</a></td>
                </tr>
            </table>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            setInterval(function() {

              var currentTime = new Date();
              var hours = currentTime.getHours();
              var minutes = currentTime.getMinutes();
              var seconds = currentTime.getSeconds();

              // Add leading zeros
              hours = (hours < 10 ? "0" : "") + hours;
              minutes = (minutes < 10 ? "0" : "") + minutes;
              seconds = (seconds < 10 ? "0" : "") + seconds;

              // Compose the string for display
              var currentTimeString = hours + ":" + minutes + ":" + seconds;

              $(".clock").html(currentTimeString);

            }, 1000);
            $(".link_list").click(function(event) {
                tl = this.getAttribute("data-nik");
                ds = this.getAttribute("data-status");
                dj = this.getAttribute("data-jenis");
                dt = this.getAttribute("data-tgl");
                $.get( "/ajaxVerifikasi/"+tl+"/"+ds+"/"+dj+"/"+dt, function(data) {
                    $(".modal-body").html(data);
                })
                .done(function(data) {
                    console.log("success");
                });
                
                $('#listModal').modal({show:true});
            });
        })
    </script>
@endsection