@extends('layout2')
@section('head')
  <style type="text/css">
    th{
        text-align: center;
    }
  </style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LIST REVENUE PER PROGRAM</span>
</h1>
@endsection
@section('content')
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Download Dokumen</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <label class="switcher switcher-rounded switcher-primary">
                <input type="checkbox" id="generate_evi">
                <div class="switcher-indicator">
                  <div class="switcher-yes">YES</div>
                  <div class="switcher-no">NO</div>
                </div>
                Generate Foto Evidence
            </label>
            <label class="switcher switcher-rounded switcher-primary">
                <input type="checkbox" id="generate_doc">
                <div class="switcher-indicator">
                  <div class="switcher-yes">YES</div>
                  <div class="switcher-no">NO</div>
                </div>
                Generate Data
            </label>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn download_btn btn-primary">Download</button>
        </div>
      </div>
    </div>
</div>
    <div class="panel panel-default" id="info">
        <div class="panel-heading">REVENUE {{Request::segment(3)}} BULAN {{Request::segment(4)}} ON : <span class="clock"></span></div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-fixed">
                <tr>
                    <th style="vertical-align:middle" rowspan=2>#</th>
                    <th style="vertical-align:middle" rowspan=2>PROGRAM</th>
                    <th colspan=2>VERIFIKASI</th>
                    <th colspan=2>PEKERJAAN</th>
                    <th rowspan=2>SIAP REKON</th>
                    <th rowspan=2>TOTAL</th>
                </tr>
                <tr>
                    <th bgcolor="#8E001C" style="color: white;">TL</th>
                    <th bgcolor="#425ff4" style="color: white;">TELKOM</th>
                    <th>IOAN</th>
                    <th>NON-IOAN</th>
                </tr>
                <?php
                    $totalAll=0;$totalTl=0;$totalSm=0;$totalTm=0;$totalSr=0;
                ?>
                @foreach($data as $no => $d)
                    @if(session('auth')->nama_instansi == 'TELKOM AKSES' || session('auth')->nama_instansi == $d->mitra)
                    <?php
                        if($d->id == 4)
                        {
                            $tl                  = $d->tl_mtr;
                            $telkom              = $d->telkom_mtr;
                            $siap_rekon          = $d->siap_rekon_mtr;
                            $siap_rekon_ioan     = $d->siap_rekon_mtr_ioan;
                            $siap_rekon_non_ioan = $d->siap_rekon_mtr_non_ioan;
                        }
                        else
                        {
                            $tl                  = $d->tl_mtr + $d->tl_jasa;
                            $telkom              = $d->telkom_mtr + $d->telkom_jasa;
                            $siap_rekon          = $d->siap_rekon_mtr + $d->siap_rekon_jasa;
                            $siap_rekon_ioan     = $d->siap_rekon_mtr_ioan + $d->siap_rekon_jasa_ioan;
                            $siap_rekon_non_ioan = $d->siap_rekon_mtr_non_ioan + $d->siap_rekon_jasa_non_ioan;
                        }

                        $total = $tl + $telkom + $siap_rekon;
                    ?>

                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $d->nama_order }}</td>
                        <td bgcolor="#8E001C" align="right"><a style="color: white;" href="/rincianRevenueProgramTa/{{Request::segment(3)}}/{{$d->id}}/null/all">{{ number_format($tl) }}</a></td>
                        <td bgcolor="#228B22" align="right"><a style="color: white;" href="/rincianRevenueProgramTa/{{Request::segment(3)}}/{{$d->id}}/1/all">{{ number_format($telkom )}}</a></td>
                        <td align="right"><a href="/rincianRevenueProgramTa/{{Request::segment(3)}}/{{$d->id}}/2/ioan">{{ number_format($siap_rekon_ioan) }}</td>
                        <td align="right"><a href="/rincianRevenueProgramTa/{{Request::segment(3)}}/{{$d->id}}/2/non_ioan">{{ number_format($siap_rekon_non_ioan) }}</td>
                        <td align="right"><a href="/rincianRevenueProgramTa/{{Request::segment(3)}}/{{$d->id}}/2/all">{{ number_format($siap_rekon) }}</a></td>
                        <td align="right">
                            <a href="/rincianRevenueProgramTa/{{Request::segment(3)}}/{{$d->id}}/all/all">{{ number_format($total) }}</a>
                            <a data-toggle="modal" data-target="#exampleModal" data-href="/cetakSubProgram/{{Request::segment(3)}}/{{$d->id}}/all" class="btn btn-info show_modal btn-xs">
                                <span class="glyphicon glyphicon-download"></span>
                                <span>Download</span>
                            </a>
                        </td>
                    </tr>
                    <?php
                        $totalAll+=$total;
                        $totalTl+=$tl;
                        $totalTm+=$telkom;
                        $totalSr+=$siap_rekon;
                    ?>
                    @endif
                @endforeach
                <tr><td colspan=2>TOTAL</td>
                    <td align="right"><a href="/rincianRevenueSubProgramTa/{{Request::segment(3)}}/all/null">{{ number_format($totalTl) }}</a></td>

                    <td align="right"><a href="/rincianRevenueSubProgramTa/{{Request::segment(3)}}/all/2">{{ number_format($totalTm) }}</a></td>
                    <td></td>
                    <td></td>
                    <td align="right"><a href="/rincianRevenueSubProgramTa/{{Request::segment(3)}}/all/3">{{ number_format($totalSr) }}</a></td>
                    <td align="right"><a href="/rincianRevenueSubProgramTa/{{Request::segment(3)}}/all/all">{{ number_format($totalAll) }}</a></td>
                </tr>
            </table>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            setInterval(function() {

              var currentTime = new Date();
              var hours = currentTime.getHours();
              var minutes = currentTime.getMinutes();
              var seconds = currentTime.getSeconds();

              // Add leading zeros
              hours = (hours < 10 ? "0" : "") + hours;
              minutes = (minutes < 10 ? "0" : "") + minutes;
              seconds = (seconds < 10 ? "0" : "") + seconds;

              // Compose the string for display
              var currentTimeString = hours + ":" + minutes + ":" + seconds;

              $(".clock").html(currentTimeString);

            }, 1000);
            $(".link_list").click(function(event) {
                tl = this.getAttribute("data-nik");
                ds = this.getAttribute("data-status");
                dj = this.getAttribute("data-jenis");
                dt = this.getAttribute("data-tgl");
                $.get( "/ajaxVerifikasi/"+tl+"/"+ds+"/"+dj+"/"+dt, function(data) {
                    $(".modal-body").html(data);
                })
                .done(function(data) {
                    console.log("success");
                });

                $('#listModal').modal({show:true});
            });

            var href = '';

            $('.show_modal').on('click', function(){
                href = $(this).data('href');
            })


            $('.download_btn').on('click', function(){
                var generate_evidence = $("#generate_evi").is(":checked"),
                generate_doc = $("#generate_doc").is(":checked"),
                params_evi = 'nok',
                params_doc = 'nok',
                new_link = '';

                if(generate_evidence){
                    params_evi = 'ok'
                }

                if(generate_doc){
                    params_doc = 'ok'
                }

                new_link = href + '/' + params_evi + '/' + params_doc;

                window.location = new_link;
            });
        })
    </script>
@endsection