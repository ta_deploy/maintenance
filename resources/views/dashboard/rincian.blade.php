@extends('layout2')
@section('head')
<style type="text/css">
    .container {
        width: 100%;
        margin: 0;
        padding: 0;
    }
    .table-condensed{
        font-size: 10px;
    }
    .head{
        color: #303030;
    }
    td{
        text-align: right;
    }
</style>
<link rel="stylesheet" href="/manual_components/fixed-table-master/fixed-table.css"/>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>RINCIAN REVENUE</span>
</h1>
@endsection
@section('content')
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Download Dokumen</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <label class="switcher switcher-rounded switcher-primary">
                <input type="checkbox" id="generate_evi">
                <div class="switcher-indicator">
                  <div class="switcher-yes">YES</div>
                  <div class="switcher-no">NO</div>
                </div>
                Generate Foto Evidence
            </label>
            <label class="switcher switcher-rounded switcher-primary">
                <input type="checkbox" id="generate_doc">
                <div class="switcher-indicator">
                  <div class="switcher-yes">YES</div>
                  <div class="switcher-no">NO</div>
                </div>
                Generate Data
            </label>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn download_btn btn-primary">Download</button>
        </div>
      </div>
    </div>
</div>

<a class="btn btn-primary show_modal" type="button" data-toggle="modal" data-target="#exampleModal" data-href="/rincianRevenueProgramTa_download/{{Request::segment(2)}}/{{Request::segment(3)}}/{{Request::segment(4)}}/{{Request::segment(5)}}">
    <span class="glyphicon glyphicon-floppy-disk"></span>
    <span>Unduh Excel</span>
</a>
<div class="panel panel-default" id="info">
    <div class="panel-heading">Rincian Revenue</div>
    <div id="fixed-table-container-demo" class="fixed-table-container">
        <table class="table table-bordered table-fixed">
            <thead>
                <tr>
                    <th width="20" class="head"><br/>Id<br/>Item<br/></th>
                    <th width="20" class="head">Jasa</th>
                    <th width="20" class="head">Material</th>
                    <th width="20" class="head">Satuan</th>
                    @foreach($head as $h)
                    <th width="100" class="head">{!! $title[$h] !!}</th>
                    @endforeach
                    <th width="20" class="head">Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $no => $list)
                <?php
                $subtotal = 0;
                ?>
                <tr>
                    <td class="head">{{ $list['id_item'] }}</td>
                    <td>{{ number_format($list['jasa']) }}</td>
                    <td>{{ number_format($list['material']) }}</td>
                    <td>{{ $list['satuan'] }}</td>
                    @foreach($list['no_tiket'] as $no2 => $l2)
                    <td>{{ $l2 ?: '-' }}</td>
                    <?php
                    if($list['jenis_order']==4)
                        $subtotal += ($l2*$list['material']);
                    else
                        $subtotal += ($l2*$list['jasa'])+($l2*$list['material']);
                    ?>
                    @endforeach
                    <td align="right">{{ number_format($subtotal) }}</td>
                </tr>
                @endforeach
                <tr><td class="head">Total Jasa</td><td></td><td></td><td></td>
                    <?php
                    $sumtotal = 0;
                    ?>
                    @foreach($head as $h)
                    <td align="right">{{ number_format($totaljasa[$h]) }}</td>
                    <?php
                    $sumtotal += $totaljasa[$h];
                    ?>
                    @endforeach
                    <td>{{ number_format($sumtotal) }}</td>
                </tr>

                <tr><td class="head">Total Material</td><td></td><td></td><td></td>
                    <?php
                    $sumtotal = 0;
                    ?>
                    @foreach($head as $h)
                    <td align="right">{{ number_format($totalmaterial[$h]) }}</td>
                    <?php
                    $sumtotal += $totalmaterial[$h];
                    ?>
                    @endforeach
                    <td>{{ number_format($sumtotal) }}</td>
                </tr>
                <tr><td class="head">Total</td><td></td><td></td><td></td>
                    <?php
                    $sumtotal = 0;
                    ?>
                    @foreach($head as $h)
                    <td align="right">{{ number_format($total[$h]) }}</td>
                    <?php
                    $sumtotal += $total[$h];
                    ?>
                    @endforeach
                    <td>{{ number_format($sumtotal) }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
<script src="/manual_components/fixed-table-master/fixed-table.js"></script>
<script type="text/javascript">
    var fixedTable = fixTable(document.getElementById('fixed-table-container-demo'));
    var href = '';

    $('.show_modal').on('click', function(){
        href = $(this).data('href');
    })


    $('.download_btn').on('click', function(){
        var generate_evidence = $("#generate_evi").is(":checked"),
        generate_doc = $("#generate_doc").is(":checked"),
        params_evi = 'nok',
        params_doc = 'nok',
        new_link = '';

        if(generate_evidence){
            params_evi = 'ok'
        }

        if(generate_doc){
            params_doc = 'ok'
        }

        new_link = href + '/' + params_evi + '/' + params_doc;

        console.log(params_evi)

        window.location = new_link;
    });

</script>
@endsection