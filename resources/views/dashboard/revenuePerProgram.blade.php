@extends('layout2')
@section('head')
  <style type="text/css">
    th{
        text-align: center;
    }
  </style>
@endsection
@section('content')
    <div class="panel panel-default" id="info">
        <div class="panel-heading">REVENUE {{Request::segment(3)}} BULAN {{Request::segment(4)}} ON : <span class="clock"></span></div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-fixed">
                <tr>
                    <th style="vertical-align:middle" rowspan=2>#</th>
                    <th style="vertical-align:middle" rowspan=2>PROGRAM</th>
                    <th colspan=3>VERIFIKASI</th>
                    <th rowspan=2>SIAP REKON</th>
                    <th rowspan=2>TOTAL</th>
                </tr>
                <tr>
                    <th bgcolor="#8E001C">TL</th>
                    <th bgcolor="#228B22">SM</th>
                    <th bgcolor="#425ff4">TELKOM</th>
                </tr>
                <?php
                    $totalAll=0;$totalTl=0;$totalSm=0;$totalTm=0;$totalSr=0;
                ?>
                @foreach($data as $no => $d)
                    @if(session('auth')->nama_instansi == 'TELKOM AKSES' || session('auth')->nama_instansi == $d->mitra)
                    <?php
                        $tl=$d->tl_mtr+$d->tl_jasa;
                        $sm=$d->sm_mtr+$d->sm_jasa;
                        $telkom=$d->telkom_mtr+$d->telkom_jasa;
                        $siap_rekon=$d->siap_rekon_mtr+$d->siap_rekon_jasa;
                        if($d->id==3){
                            $tl=$d->tl_mtr;
                            $sm=$d->sm_mtr;
                            $telkom=$d->telkom_mtr;
                            $siap_rekon=$d->siap_rekon_mtr;
                        }
                        $total=$tl+$sm+$telkom+$siap_rekon;
                    ?>
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $d->judul_rekon }}</td>
                        <td bgcolor="#8E001C" align="right"><a href="/rincianRevenueProgram/{{Request::segment(2)}}/{{$d->id}}/null">{{ number_format($tl) }}</a></td>
                        <td bgcolor="#228B22" align="right"><a href="/rincianRevenueProgram/{{Request::segment(2)}}/{{$d->id}}/1">{{ number_format($sm )}}</a></td>
                        <td bgcolor="#425ff4" align="right"><a href="/rincianRevenueProgram/{{Request::segment(2)}}/{{$d->id}}/2">{{ number_format($telkom) }}</a></td>
                        <td align="right"><a href="/rincianRevenueProgram/{{Request::segment(2)}}/{{$d->id}}/3">{{ number_format($siap_rekon) }}</a></td>
                        <td align="right">
                            <a href="/rincianRevenueProgram/{{Request::segment(2)}}/{{$d->id}}/all">{{ number_format($total) }}</a>
                            <a href="/cetak/{{Request::segment(2)}}/{{$d->id}}/all" class="btn btn-info btn-xs">
                                <span class="glyphicon glyphicon-download"></span>
                                <span>Download</span>
                            </a>
                        </td>
                    </tr>
                    <?php
                        $totalAll+=$total;
                        $totalTl+=$tl;
                        $totalSm+=$sm;
                        $totalTm+=$telkom;
                        $totalSr+=$siap_rekon;
                    ?>
                    @endif
                @endforeach
                <tr><td colspan=2>TOTAL</td>
                    <td align="right"><a href="/rincianRevenueProgram/{{Request::segment(2)}}/all/null">{{ number_format($totalTl) }}</a></td>
                    <td align="right"><a href="/rincianRevenueProgram/{{Request::segment(2)}}/all/1">{{ number_format($totalSm) }}</a></td>
                    <td align="right"><a href="/rincianRevenueProgram/{{Request::segment(2)}}/all/2">{{ number_format($totalTm) }}</a></td>
                    <td align="right"><a href="/rincianRevenueProgram/{{Request::segment(2)}}/all/3">{{ number_format($totalSr) }}</a></td>
                    <td align="right"><a href="/rincianRevenueProgram/{{Request::segment(2)}}/all/all">{{ number_format($totalAll) }}</a></td>
                </tr>
            </table>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            setInterval(function() {

              var currentTime = new Date();
              var hours = currentTime.getHours();
              var minutes = currentTime.getMinutes();
              var seconds = currentTime.getSeconds();

              // Add leading zeros
              hours = (hours < 10 ? "0" : "") + hours;
              minutes = (minutes < 10 ? "0" : "") + minutes;
              seconds = (seconds < 10 ? "0" : "") + seconds;

              // Compose the string for display
              var currentTimeString = hours + ":" + minutes + ":" + seconds;

              $(".clock").html(currentTimeString);

            }, 1000);
            $(".link_list").click(function(event) {
                tl = this.getAttribute("data-nik");
                ds = this.getAttribute("data-status");
                dj = this.getAttribute("data-jenis");
                dt = this.getAttribute("data-tgl");
                $.get( "/ajaxVerifikasi/"+tl+"/"+ds+"/"+dj+"/"+dt, function(data) {
                    $(".modal-body").html(data);
                })
                .done(function(data) {
                    console.log("success");
                });
                
                $('#listModal').modal({show:true});
            });
        })
    </script>
@endsection