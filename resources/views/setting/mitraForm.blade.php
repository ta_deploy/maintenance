@extends('layout2')
@section('head')
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>{{ $data?'Edit':'Input' }} Mitra {{ @$data->mitra }}</span>
</h1>
@endsection
@section('content')
@if (session('auth')->maintenance_level == '1')
<form class="form-horizontal" method="post" id="form-register">
    <div class="form-group">
        <label for="mitra" class="col-md-2 control-label">Nama Mitra</label>
        <div class="col-md-4 form-message-dark">
            <input type="text" name="mitra" id="mitra" class="form-control" required value="{{ @$data->mitra }}" />
        </div>
        <label for="label" class="col-md-1 control-label">Label</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" name="label" id="label" class="form-control" required value="{{ @$data->label }}"/>
        </div>
        <label for="isAktif" class="col-md-1 control-label">Status</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" name="isAktif" id="isAktif" class="form-control" required value="{{ @$data->isAktif }}"/>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
@endif
@endsection
@section('script')
<script>
    $(function() {
        $('#isAktif').select2({
            data:[{'id':0, 'text':'Suspend'},{'id':1, 'text':'Aktif'}],
            placeholder:'Select Status'
        })
    });
</script>
@endsection