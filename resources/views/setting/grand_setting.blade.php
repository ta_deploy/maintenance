@extends('layout2')
@section('head')
@endsection

@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>List Program</span>
</h1>

<a href="/setting/tambah" class="pull-right"><span class="btn btn-info"><i class="ion ion-plus"></i> Program</span></a>
@endsection
@section('content')
@if (session('auth')->maintenance_level == '1')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            @foreach($data as $i)
            <div class="col-md-3">
                <div class="box bg-info darken">
                    <div class="box-cell p-x-3 p-y-1">
                        <a href="/setting/{{ $i->id }}">
                            <div class="font-weight-semibold font-size-17">{{ $i->nama_order }}</div>
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif
@endsection
@section('script')
<script>
    $(function() {

    });
</script>
@endsection