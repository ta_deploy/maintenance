@extends('layout2')
@section('head')
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>EDIT STO {{ $data->sto }}</span>
</h1>
@endsection
@section('content')
@if (session('auth')->maintenance_level == '1')
<form class="form-horizontal" method="post" id="form-register">
    <div class="form-group">
        <label for="sto" class="col-md-2 control-label">sto</label>
        <div class="col-md-4 form-message-dark">
            <input type="text" name="sto" id="sto" class="form-control" required value="{{ @$data->sto }}" />
        </div>
        <label for="datel" class="col-md-1 control-label">datel</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" name="datel" id="datel" class="form-control" required value="{{ @$data->datel }}"/>
        </div>
        <label for="witel" class="col-md-2 control-label">witel</label>
        <div class="col-md-1 form-message-dark">
            <input type="text" name="witel" id="witel" class="form-control" required value="{{ @$data->witel }}"/>
        </div>
    </div>
    <div class="form-group">
        <label for="sektor_prov" class="col-md-2 control-label">sektor_prov</label>
        <div class="col-md-4 form-message-dark">
            <input type="text" name="sektor_prov" id="sektor_prov" class="form-control" required value="{{ @$data->sektor_prov }}" />
        </div>
        <label for="kota" class="col-md-1 control-label">kota</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" name="kota" id="kota" class="form-control" required value="{{ @$data->kota }}"/>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
@endif
@endsection
@section('script')
<script>
    $(function() {

    });
</script>
@endsection