@extends('layout2')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>List Teritory</span>
</h1>
<a href="/teritori/tambah" class="pull-right"><span class="btn btn-info"><i class="ion ion-plus"></i> Teritory</span></a>
@endsection
@section('content')
@if (session('auth')->maintenance_level == '1')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Sektor</th>
                <th>Label</th>
                <th>Status</th>
                <th>Act</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->sector }}</td>
                    <td>{{ $d->label }}</td>
                    <td><span class="label label-{{ $d->isAktif?'success':'danger' }}">{{ $d->isAktif?'Aktif':'Suspend' }}</span></td>
                    <td>
                        <a href="/teritori/{{ $d->id }}" class="btn btn-xs btn-success"><i class="ion ion-compose"></i>&nbsp;&nbsp;Update</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@endif
@endsection
@section('script')
<script>
    $(function() {
        
    });
</script>
@endsection