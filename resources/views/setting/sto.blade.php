@extends('layout2')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LIST STO</span>
</h1>
<a href="/sto/tambah" class="pull-right"><span class="btn btn-info"><i class="ion ion-plus"></i> STO</span></a>
@endsection
@section('content')
@if (session('auth')->maintenance_level == '1')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>STO</th>
                <th>DATEL</th>
                <th>WITEL</th>
                <th>SEKTOR</th>
                <th>KOTA</th>
                <th>Act</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->sto }}</td>
                    <td>{{ $d->datel }}</td>
                    <td>{{ $d->witel }}</td>
                    <td>{{ $d->sektor_prov }}</td>
                    <td>{{ $d->kota }}</td>
                    <td>
                        <a href="/sto/{{ $d->id }}" class="btn btn-xs btn-success"><i class="ion ion-compose"></i>&nbsp;&nbsp;Update</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@endif
@endsection
@section('script')
<script>
    $(function() {
        
    });
</script>
@endsection