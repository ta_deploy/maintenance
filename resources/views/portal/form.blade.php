@extends('layout2')
@section('head')
@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Input Jawaban
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal">
                <?php
                    $url = substr($data->question,strpos($data->question, "<img")+10);
                    $url = substr($url, 0, (strpos($url, '" width="200px"/>')));
                ?>
                <div class="form-group">
                    <label for="jawaban" class="col-sm-3 col-md-2 control-label">Jawaban</label>
                    <div class="col-sm-5">
                        <input name="jawaban" type="text" id="jawaban" class="form-control" value="{{ old('jenis_alpro') ?: @$data->jawaban }}" autocomplete="off">
                    </div>
                    <div class="col-sm-offset-3 col-md-offset-2 col-sm-6" style="margin-top:5px;">
                        <img src="https://apps.telkomakses.co.id/portal/{{$url}}"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
@endsection