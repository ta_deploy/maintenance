@extends('layout2')
@section('head')
@endsection
@section('content')
<div id="modalLogs" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #eee">
          <strong style="color:black;">Logs Update</strong>
          <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
        </div>
        <div class="modal-body" style="overflow-y:auto;height : 450px">
            
        </div>
        <div class="modal-footer" style="background: #eee">
          
        </div>
      </div>
    </div>
</div>
<div class="panel panel-default" id="info">
    <div class="panel-heading">List Captcha</div>
    <div id="fixed-table-container-demo" class="fixed-table-container">
        <table class="table table-bordered table-fixed">
            <thead>
            <tr>
                <th>#</th>
                <th>Syntax</th>
                <th>Jawaban</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach($list as $no => $d)
                <tr>
                <?php
                    $url = substr($d->question,strpos($d->question, "<img")+10);
                    $url = substr($url, 0, (strpos($url, '" width="200px"/>')))
                ?>
                    <td>{{ ++$no }}</td>
                    <td><a href="https://apps.telkomakses.co.id/portal/{{ $url }}" target="_BLANK">https://apps.telkomakses.co.id/portal/{{ $url }}</a></td>
                    <td>{{ $d->jawaban }}</td>
                    <td>
                        <a href="/portal/{{ $d->id }}" class="btn btn-info btn-xs">
                            <span class="glyphicon glyphicon-list-alt"></span>
                            <span>Jawab</span>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function(){
        $(".logs").click(function(e){
            var id = $(this).data('idlog');
            var url = "/repair/getLogs/"+id;
            $.getJSON(url, function(r){
                var html = '<table class="table table-bordered table-fixed"><thead><tr><th>#</th><th>Tgl</th><th>Note</th><th>By</th></tr></thead><tbody>';
                $.each( r, function( key, val ) {
                    var no=key+1;
                    html+= '<tr><td>'+no+'</td><td>'+val.update_at+'</td><td>'+val.catatan+'</td><td>'+val.update_by+'</td></tr>';
                });
                html+='</tbody></table>';
                $(".modal-body").html(html);
                $("#modalLogs").modal('show');
            });
        });
    });
</script>
@endsection