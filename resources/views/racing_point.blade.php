@extends('nomenu')
@section('head')
@endsection
@section('content')
    <div class="panel panel-default panel-primary">
        <div class="panel-heading text-xs-center font-size-20">
            REKAP RACING POIN BULAN {{ Request::segment(2) }}
        </div>
        <div class="panel-body">
            @foreach($data as $no => $d)
            <div class="col-md-4">
                <!-- Counters -->
                <div class="panel panel-info panel-dark widget-profile">
                  <div class="panel-heading">
                    <h3 class="widget-profile-header">
                      {{ $d->WITEL }}
                      <a href="#" class="widget-profile-secondary-text">Total Poin : {{ $d->POIN }}</a>
                      <a href="#" class="widget-profile-secondary-text">Total Teknisi : {{ $d->TEKNISI }}</a>
                    </h3>
                    <i class="widget-profile-bg-icon font-size-52">#{{ ++$no }}</i>
                  </div>
                  <div class="widget-profile-counters">
                    <a href="#" class="col-xs-4">
                      <span class="widget-profile-counter">{{ $d->DIG_CHANNEL }}</span>
                      DIGITAL CHANNEL
                    </a>
                    <a href="#" class="col-xs-4">
                      <span class="widget-profile-counter">{{ $d->NON_DIG_CHANNEL }}</span>
                      NON DIGITAL CHANNEL
                    </a>
                    <a href="#" class="col-xs-4">
                      <span class="widget-profile-counter">{{ $d->SOBI }}</span>
                      SOBI
                    </a>
                  </div>
                  <div class="widget-profile-counters">
                    <a href="#" class="col-xs-6">
                      <span class="widget-profile-counter">{{ $d->PS }}</span>
                      PS
                    </a>
                    <a href="#" class="col-xs-6">
                      <span class="widget-profile-counter">{{ $d->GARANSI_PSB }}</span>
                      GARANSI PSB
                    </a>
                  </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection
@section('script')
@endsection