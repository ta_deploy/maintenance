@extends('layout2')
@section('head')

@endsection
@section('content')
  <!-- Q -->
  <div class="panel panel-default" id="info">
      <div id="fixed-table-container-demo" class="fixed-table-container">
          <table class="table table-bordered table-fixed" id="datatablesQ">
              <thead>
                <tr>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">#</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">SEKTOR</th>
                  <th colspan="{{ date('t') }}" style="vertical-align:middle;text-align: center;">Q</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">TARGET</th>
                </tr>
                <tr>
                  @for($i=1;$i<=date("t");$i++)
                      <th>{{ $i }}</th>
                  @endfor
                </tr>
              </thead>
              <tbody>
                  @foreach($q as $no => $list) 
                      <tr>
                          <td>{{ ++$no }}</td>
                          <td>{{ str_replace('SEKTOR - KALSEL - ','',$list['NAMA_SEKTOR']) }}</td>
                          @for($i=1;$i<=date("t");$i++)
                              <td>{{ $list[$i] }}</td>
                          @endfor
                          <td>-</td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
      <div id="c3-graph" style="height: 250px"></div>
  </div>

  <!-- sugar -->
  <div class="panel panel-default" id="info">
      <div id="fixed-table-container-demo" class="fixed-table-container">
          <table class="table table-bordered table-fixed" id="datatablesSugar">
              <thead>
                <tr>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">#</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">SEKTOR</th>
                  <th colspan="{{ date('t') }}" style="vertical-align:middle;text-align: center;">SUGAR</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">TARGET</th>
                </tr>
                <tr>
                  @for($i=1;$i<=date("t");$i++)
                      <th>{{ $i }}</th>
                  @endfor
                </tr>
              </thead>
              <tbody>
                  @foreach($sugar as $no => $list) 
                      <tr>
                          <td>{{ ++$no }}</td>
                          <td>{{ str_replace('SEKTOR - KALSEL - ','',$list['NAMA_SEKTOR']) }}</td>
                          @for($i=1;$i<=date("t");$i++)
                              <td>{{ $list[$i] }}</td>
                          @endfor
                          <td>-</td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
      <div id="sugar-graph" style="height: 250px"></div>
  </div>

  <!-- ttr3 -->
  <div class="panel panel-default" id="info">
      <div id="fixed-table-container-demo" class="fixed-table-container">
          <table class="table table-bordered table-fixed" id="datatablesTTR3">
              <thead>
                <tr>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">#</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">SEKTOR</th>
                  <th colspan="{{ date('t') }}" style="vertical-align:middle;text-align: center;">TTR 3 JAM</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">TARGET</th>
                </tr>
                <tr>
                  @for($i=1;$i<=date("t");$i++)
                      <th>{{ $i }}</th>
                  @endfor
                </tr>
              </thead>
              <tbody>
                  @foreach($ttr3 as $no => $list) 
                      <tr>
                          <td>{{ ++$no }}</td>
                          <td>{{ str_replace('SEKTOR - KALSEL - ','',$list['NAMA_SEKTOR']) }}</td>
                          @for($i=1;$i<=date("t");$i++)
                              <td>{{ $list[$i] }}</td>
                          @endfor
                          <td>-</td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
      <div id="ttr3-graph" style="height: 250px"></div>
  </div>

  <!-- ttr12 -->
  <div class="panel panel-default" id="info">
      <div id="fixed-table-container-demo" class="fixed-table-container">
          <table class="table table-bordered table-fixed" id="datatablesTTR12">
              <thead>
                <tr>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">#</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">SEKTOR</th>
                  <th colspan="{{ date('t') }}" style="vertical-align:middle;text-align: center;">TTR 12 JAM</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">TARGET</th>
                </tr>
                <tr>
                  @for($i=1;$i<=date("t");$i++)
                      <th>{{ $i }}</th>
                  @endfor
                </tr>
              </thead>
              <tbody>
                  @foreach($ttr12 as $no => $list) 
                      <tr>
                          <td>{{ ++$no }}</td>
                          <td>{{ str_replace('SEKTOR - KALSEL - ','',$list['NAMA_SEKTOR']) }}</td>
                          @for($i=1;$i<=date("t");$i++)
                              <td>{{ $list[$i] }}</td>
                          @endfor
                          <td>-</td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
      <div id="ttr12-graph" style="height: 250px"></div>
  </div>

  <!-- VALDA -->
  <div class="panel panel-default" id="info">
      <div id="fixed-table-container-demo" class="fixed-table-container">
          <table class="table table-bordered table-fixed" id="datatablesVALDA">
              <thead>
                <tr>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">#</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">SEKTOR</th>
                  <th colspan="{{ date('t') }}" style="vertical-align:middle;text-align: center;">VALIDASI DATA</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">TARGET</th>
                </tr>
                <tr>
                  @for($i=1;$i<=date("t");$i++)
                      <th>{{ $i }}</th>
                  @endfor
                </tr>
              </thead>
              <tbody>
                  @foreach($valda as $no => $list) 
                      <tr>
                          <td>{{ ++$no }}</td>
                          <td>{{ str_replace('SEKTOR - KALSEL - ','',$list['NAMA_SEKTOR']) }}</td>
                          @for($i=1;$i<=date("t");$i++)
                              <td>{{ $list[$i] }}</td>
                          @endfor
                          <td>-</td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
      <div id="valda-graph" style="height: 250px"></div>
  </div>

  <!-- tangibleKuan -->
  <div class="panel panel-default" id="info">
      <div id="fixed-table-container-demo" class="fixed-table-container">
          <table class="table table-bordered table-fixed" id="datatablesTANGIBLE">
              <thead>
                <tr>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">#</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">SEKTOR</th>
                  <th colspan="{{ date('t') }}" style="vertical-align:middle;text-align: center;">TANGIBLE (KUANTITAS)</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">TARGET</th>
                </tr>
                <tr>
                  @for($i=1;$i<=date("t");$i++)
                      <th>{{ $i }}</th>
                  @endfor
                </tr>
              </thead>
              <tbody>
                  @foreach($tangibleKuan as $no => $list) 
                      <tr>
                          <td>{{ ++$no }}</td>
                          <td>{{ str_replace('SEKTOR - KALSEL - ','',$list['NAMA_SEKTOR']) }}</td>
                          @for($i=1;$i<=date("t");$i++)
                              <td>{{ $list[$i] }}</td>
                          @endfor
                          <td>-</td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
      <div id="tangibleKuan-graph" style="height: 250px"></div>
  </div>

  <!-- tangibleCheck -->
  <div class="panel panel-default" id="info">
      <div id="fixed-table-container-demo" class="fixed-table-container">
          <table class="table table-bordered table-fixed" id="datatablesTANGIBLE2">
              <thead>
                <tr>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">#</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">SEKTOR</th>
                  <th colspan="{{ date('t') }}" style="vertical-align:middle;text-align: center;">TANGIBLE (QUALITY)</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">TARGET</th>
                </tr>
                <tr>
                  @for($i=1;$i<=date("t");$i++)
                      <th>{{ $i }}</th>
                  @endfor
                </tr>
              </thead>
              <tbody>
                  @foreach($tangibleCheck as $no => $list) 
                      <tr>
                          <td>{{ ++$no }}</td>
                          <td>{{ str_replace('SEKTOR - KALSEL - ','',$list['NAMA_SEKTOR']) }}</td>
                          @for($i=1;$i<=date("t");$i++)
                              <td>{{ $list[$i] }}</td>
                          @endfor
                          <td>-</td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
      <div id="tangibleCheck-graph" style="height: 250px"></div>
  </div>

  <!-- unspec -->
  <div class="panel panel-default" id="info">
      <div id="fixed-table-container-demo" class="fixed-table-container">
          <table class="table table-bordered table-fixed" id="datatablesSALDOUNSPEC">
              <thead>
                <tr>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">#</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">SEKTOR</th>
                  <th colspan="{{ date('t') }}" style="vertical-align:middle;text-align: center;">SALDO UNDERSPEC</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">TARGET</th>
                </tr>
                <tr>
                  @for($i=1;$i<=date("t");$i++)
                      <th>{{ $i }}</th>
                  @endfor
                </tr>
              </thead>
              <tbody>
                  @foreach($unspec as $no => $list) 
                      <tr>
                          <td>{{ ++$no }}</td>
                          <td>{{ str_replace('SEKTOR - KALSEL - ','',$list['NAMA_SEKTOR']) }}</td>
                          @for($i=1;$i<=date("t");$i++)
                              <td>{{ $list[$i] }}</td>
                          @endfor
                          <td>-</td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
      <div id="unspec-graph" style="height: 250px"></div>
  </div>

  <!-- PRODUKTIFITAS UNSPEC -->
  <div class="panel panel-default" id="info">
      <div id="fixed-table-container-demo" class="fixed-table-container">
          <table class="table table-bordered table-fixed" id="datatablesPRODUNSPEC">
              <thead>
                <tr>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">#</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">SEKTOR</th>
                  <th colspan="{{ date('t') }}" style="vertical-align:middle;text-align: center;">PRODUKTIFITAS UNSPEC</th>
                  <th rowspan="2" style="vertical-align:middle;text-align: center;">TARGET</th>
                </tr>
                <tr>
                  @for($i=1;$i<=date("t");$i++)
                      <th>{{ $i }}</th>
                  @endfor
                </tr>
              </thead>
              <tbody>
                  @foreach($prod as $no => $list) 
                      <tr>
                          <td>{{ ++$no }}</td>
                          <td>{{ str_replace('SEKTOR - KALSEL - ','',$list['NAMA_SEKTOR']) }}</td>
                          @for($i=1;$i<=date("t");$i++)
                              <td>{{ $list[$i] }}</td>
                          @endfor
                          <td>-</td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
      <div id="prod-graph" style="height: 250px"></div>
  </div>
@endsection
@section('script')

<script>
  $(function() {
    $('#datatablesQ').dataTable({
      "paging": false,
      "bFilter": false
    });
    $('#datatablesSugar').dataTable({
      "paging": false,
      "bFilter": false
    });
    $('#datatablesTTR3').dataTable({
      "paging": false,
      "bFilter": false
    });
    $('#datatablesTTR12').dataTable({
      "paging": false,
      "bFilter": false
    });
    $('#datatablesVALDA').dataTable({
      "paging": false,
      "bFilter": false
    });
    $('#datatablesTANGIBLE').dataTable({
      "paging": false,
      "bFilter": false
    });
    $('#datatablesTANGIBLE2').dataTable({
      "paging": false,
      "bFilter": false
    });
    $('#datatablesSALDOUNSPEC').dataTable({
      "paging": false,
      "bFilter": false
    });
    $('#datatablesPRODUNSPEC').dataTable({
      "paging": false,
      "bFilter": false
    });
    var chart = c3.generate({
      bindto: '#c3-graph',
      data: {
          x: 'x',
          columns: <?= $qChart; ?>,
      },
      axis: {
          x: {
              type: 'timeseries',
              tick: {
                  format: '%d'
              }
          }
      }
    });
    var sugar = c3.generate({
      bindto: '#sugar-graph',
      color: { pattern: [ '#000000', '#FF0000', '#800000', '#FFFF00', '#808000', '#00FF00', '#008000', '#00FFFF', '#008080', '#0000FF', '#000080', '#FF00FF' ] },
      data: {
        x: 'x',
        columns: <?= $sugarChart ?>,
      },
      axis: {
          x: {
              type: 'timeseries',
              tick: {
                  format: '%d'
              }
          }
      }
    });

    var ttr3 = {
        x: 'x',
      columns: <?= $ttr3Chart ?>,
    };
    c3.generate({
      bindto: '#ttr3-graph',
      color: { pattern: [ '#000000', '#FF0000', '#800000', '#FFFF00', '#808000', '#00FF00', '#008000', '#00FFFF', '#008080', '#0000FF', '#000080', '#FF00FF' ] },
      data: ttr3,
      axis: {
          x: {
              type: 'timeseries',
              tick: {
                  format: '%d'
              }
          }
      }
    });

    var ttr12 = {
        x: 'x',
      columns: <?= $ttr12Chart ?>,
    };
    c3.generate({
      bindto: '#ttr12-graph',
      color: { pattern: [ '#000000', '#FF0000', '#800000', '#FFFF00', '#808000', '#00FF00', '#008000', '#00FFFF', '#008080', '#0000FF', '#000080', '#FF00FF' ] },
      data: ttr12,
      axis: {
          x: {
              type: 'timeseries',
              tick: {
                  format: '%d'
              }
          }
      }
    });

    var valda = {
        x: 'x',
      columns: <?= $valdaChart ?>,
    };
    c3.generate({
      bindto: '#valda-graph',
      color: { pattern: [ '#000000', '#FF0000', '#800000', '#FFFF00', '#808000', '#00FF00', '#008000', '#00FFFF', '#008080', '#0000FF', '#000080', '#FF00FF' ] },
      data: valda,
      axis: {
          x: {
              type: 'timeseries',
              tick: {
                  format: '%d'
              }
          }
      }
    });

    var tangibleKuan = {
        x: 'x',
      columns: <?= $tangibleKuanChart ?>,
    };
    c3.generate({
      bindto: '#tangibleKuan-graph',
      color: { pattern: [ '#000000', '#FF0000', '#800000', '#FFFF00', '#808000', '#00FF00', '#008000', '#00FFFF', '#008080', '#0000FF', '#000080', '#FF00FF' ] },
      data: tangibleKuan,
      axis: {
          x: {
              type: 'timeseries',
              tick: {
                  format: '%d'
              }
          }
      }
    });

    var tangibleCheck = {
        x: 'x',
      columns: <?= $tangibleCheckChart ?>,
    };
    c3.generate({
      bindto: '#tangibleCheck-graph',
      color: { pattern: [ '#000000', '#FF0000', '#800000', '#FFFF00', '#808000', '#00FF00', '#008000', '#00FFFF', '#008080', '#0000FF', '#000080', '#FF00FF' ] },
      data: tangibleCheck,
      axis: {
          x: {
              type: 'timeseries',
              tick: {
                  format: '%d'
              }
          }
      }
    });

    var unspec = {
        x: 'x',
      columns: <?= $unspecChart ?>,
    };
    c3.generate({
      bindto: '#unspec-graph',
      color: { pattern: [ '#000000', '#FF0000', '#800000', '#FFFF00', '#808000', '#00FF00', '#008000', '#00FFFF', '#008080', '#0000FF', '#000080', '#FF00FF' ] },
      data: unspec,
      axis: {
          x: {
              type: 'timeseries',
              tick: {
                  format: '%d'
              }
          }
      }
    });

    var prod = {
        x: 'x',
      columns: <?= $prodChart ?>,
    };
    c3.generate({
      bindto: '#prod-graph',
      color: { pattern: [ '#000000', '#FF0000', '#800000', '#FFFF00', '#808000', '#00FF00', '#008000', '#00FFFF', '#008080', '#0000FF', '#000080', '#FF00FF' ] },
      data: prod,
      axis: {
          x: {
              type: 'timeseries',
              tick: {
                  format: '%d'
              }
          }
      }
    });
  });
</script>
@endsection