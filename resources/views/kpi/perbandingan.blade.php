<html class="loading" lang="en" data-textdirection="ltr">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
        <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
        <meta name="author" content="ThemeSelect">
        <title>Tactical</title>
        <link rel="apple-touch-icon" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/images/ico/favicon.ico">
        <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
        <!--<link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">-->
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/line-awesome.min.css">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/vendors/css/charts/chartist.css">
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/vendors/css/charts/chartist-plugin-tooltip.css">
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN CHAMELEON  CSS-->
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/app.css">
        <!-- END CHAMELEON  CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/core/colors/palette-gradient.css">
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/core/colors/palette-gradient.css">
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/pages/chat-application.css">
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/app-assets/css/pages/dashboard-analytics.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="https://apps.telkomakses.co.id/tactical/assets/assets/css/style.css">
        <!-- END Custom CSS-->
      </head><table class="table-sm table-bordered table-responsive-sm text-nowrap table-fixed dt_rekap_witel" id="regional_tabel" style="text-align: center; width: 100%;">
    										<thead>
    											<tr class="white" style="background: #FF4259">
                                                    <th scope="col" style="visibility:visible" rowspan="3">Witel</th>
                                                    <th scope="col" style="visibility:visible" rowspan="3">DATE</th>
    												<th scope="col" colspan="44">Performansi I-OAN all WITEL </th>
    												<th scope="col" rowspan="3">Total Nilai KPI</th>
    											</tr>
    											<tr>
    												<th scope="col" class="white" style="background: #FF8942" colspan="4">Q(10%)</th>
                                                    <th scope="col" colspan="4" class="white" style="background: #1A97F0">Assurance Guarantee (15%)</th>
                                                    <th scope="col" class="white" style="background: #48D7A4" colspan="4">HVC 3 Jam (15%)</th>
                                                    <th scope="col" class="white" style="background: #48D7A4" colspan="4">TTR 3 Jam (10%)</th>
                                                    <th scope="col" class="white" style="background: #ACCFFF" colspan="4">TTR 1DS 12 Jam (10%)</th>
                                                    <th scope="col" class="white" style="background: #BCBFFF" colspan="4">Unspec Saldo (5%)</th>
                                                    <th scope="col" class="white" style="background: #BCBFFF" colspan="4">Unspec Prod (10%)</th>
                                                    <th scope="col" class="white" style="background: #FFB280" colspan="4">Tangible Kuantitas (5%)</th>
                                                    <th scope="col" class="white" style="background: #FFB280" colspan="4">Tangible Kualitas (5%)</th>
                                                    <th scope="col" class="white" style="background: #0FB365" colspan="4">Valins (10%)</th>
                                                    <th scope="col" class="white" style="background: #0FB365" colspan="4">SQM (5%)</th>
    											</tr>
    											<tr>
    												<!-- Q -->
                                                    <th scope="col" class="white" style="background: #FF8942">Target</th>
                                                    <th scope="col" class="white" style="background: #FF8942">Real</th>
                                                    <th scope="col" class="white" style="background: #FF8942">Ach</th>
                                                    <th scope="col" class="white" style="background: #FF8942">Nilai KPI</th>

                                                    <!-- assurance guarantee -->
                                                    <th scope="col" class="white" style="background: #1A97F0">Target</th>
                                                    <th scope="col" class="white" style="background: #1A97F0">Real</th>
                                                    <th scope="col" class="white" style="background: #1A97F0">Ach</th>
                                                    <th scope="col" class="white" style="background: #1A97F0">Nilai KPI</th>

                                                    <!-- hvc 3 jam -->
                                                    <th scope="col" class="white" style="background: #48D7A4">Target</th>
                                                    <th scope="col" class="white" style="background: #48D7A4">Real</th>
                                                    <th scope="col" class="white" style="background: #48D7A4">Ach</th>
                                                    <th scope="col" class="white" style="background: #48D7A4">Nilai KPI</th>

                                                    <!-- ttr 3 jam -->
                                                    <th scope="col" class="white" style="background: #48D7A4">Target</th>
                                                    <th scope="col" class="white" style="background: #48D7A4">Real</th>
                                                    <th scope="col" class="white" style="background: #48D7A4">Ach</th>
                                                    <th scope="col" class="white" style="background: #48D7A4">Nilai KPI</th>

                                                    <!-- ttr 12 jam -->
                                                    <th scope="col" class="white" style="background: #ACCFFF">Target</th>
                                                    <th scope="col" class="white" style="background: #ACCFFF">Real</th>
                                                    <th scope="col" class="white" style="background: #ACCFFF">Ach</th>
                                                    <th scope="col" class="white" style="background: #ACCFFF">Nilai KPI</th>

                                                    <!-- unspec saldo -->
                                                    <th scope="col" class="white" style="background: #BCBFFF">Target</th>
                                                    <th scope="col" class="white" style="background: #BCBFFF">Real</th>
                                                    <th scope="col" class="white" style="background: #BCBFFF">Ach</th>
                                                    <th scope="col" class="white" style="background: #BCBFFF">Nilai KPI</th>

                                                    <!-- unspec prod -->
                                                    <th scope="col" class="white" style="background: #BCBFFF">Target</th>
                                                    <th scope="col" class="white" style="background: #BCBFFF">Real</th>
                                                    <th scope="col" class="white" style="background: #BCBFFF">Ach</th>
                                                    <th scope="col" class="white" style="background: #BCBFFF">Nilai KPI</th>

                                                    <!-- tangible kuantitas -->
                                                    <th scope="col" class="white" style="background: #FFB280">Target</th>
                                                    <th scope="col" class="white" style="background: #FFB280">Real</th>
                                                    <th scope="col" class="white" style="background: #FFB280">Ach</th>
                                                    <th scope="col" class="white" style="background: #FFB280">Nilai KPI</th>

                                                    <!-- tangible kualitas -->
                                                    <th scope="col" class="white" style="background: #FFB280">Target</th>
                                                    <th scope="col" class="white" style="background: #FFB280">Real</th>
                                                    <th scope="col" class="white" style="background: #FFB280">Ach</th>
                                                    <th scope="col" class="white" style="background: #FFB280">Nilai KPI</th>

                                                    <!-- valins -->
                                                    <th scope="col" class="white" style="background: #0FB365">Target</th>
                                                    <th scope="col" class="white" style="background: #0FB365">Real</th>
                                                    <th scope="col" class="white" style="background: #0FB365">Ach</th>
                                                    <th scope="col" class="white" style="background: #0FB365">Nilai KPI</th>

                                                    <!-- sqm -->
                                                    <th scope="col" class="white" style="background: #0FB365">Target</th>
                                                    <th scope="col" class="white" style="background: #0FB365">Real</th>
                                                    <th scope="col" class="white" style="background: #0FB365">Ach</th>
                                                    <th scope="col" class="white" style="background: #0FB365">Nilai KPI</th>
    											</tr>
    										</thead>
    										<tbody>
@foreach($witel as $no => $w)
    <?php
        $QA=$QNK=$SUGARA=$SUGARNK=$HVCA=$HVCNK=$TTR3A=$TTR3NK=$TTR12A=$TTR12NK=$SALDOUNSPECA=$SALDOUNSPECNK=$PRODUNSPECA=$PRODUNSPECNK=$TGBKUANA=$TGBKUANNK=$TGBQUALA=$TGBQUALNK=$VALINSA=$VALINSNK=$SQMA=$SQMNK=$TNK=-1;
    ?>
    @foreach($return as $f => $r)
        <tr>
            <td>{{ $w }}</td>
            <td>TGL {{ substr($f,6,2) }} JAM {{ substr($f,8,2) }}</td>
            @if($r)
                @foreach($return[$f] as $key => $data)
                    @if($data["WITEL"] == $w)
                        <td>{{ $return[$f][$key]["QT"] }}</td>
                        <td>{{ $return[$f][$key]["QR"] }}</td>
                        <td style="{{ ($QA!=-1 && $QA!=$return[$f][$key]['QA'])?(($QA<$return[$f][$key]['QA'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }} {{ $return[$f][$key]['QA']<100?'background-color: #ff4d4d;color:white;':'background-color: lime;' }}">{{ $return[$f][$key]["QA"] }}</td>
                        <td style="{{ ($QNK!=-1 && $QNK!=$return[$f][$key]['QNK'])?(($QNK<$return[$f][$key]['QNK'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none' }}">{{ $return[$f][$key]["QNK"] }}</td>
                        <td>{{ $return[$f][$key]["SUGART"] }}</td>
                        <td>{{ $return[$f][$key]["SUGARR"] }}</td>
                        <td style="{{ ($SUGARA!=-1 && $SUGARA!=$return[$f][$key]['SUGARA'])?(($SUGARA<$return[$f][$key]['SUGARA'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }} {{ $return[$f][$key]['SUGARA']<100?'background-color: #ff4d4d;color:white;':'background-color: lime;' }}">{{ $return[$f][$key]["SUGARA"] }}</td>
                        <td style="{{ ($SUGARNK!=-1 && $SUGARNK!=$return[$f][$key]['SUGARNK'])?(($SUGARNK<$return[$f][$key]['SUGARNK'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }}">{{ $return[$f][$key]["SUGARNK"] }}</td>
                        <td>{{ $return[$f][$key]["HVCT"] }}</td>
                        <td>{{ $return[$f][$key]["HVCR"] }}</td>
                        <td style="{{ ($HVCA!=-1 && $HVCA!=$return[$f][$key]['HVCA'])?(($HVCA<$return[$f][$key]['HVCA'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }} {{ $return[$f][$key]['HVCA']<100?'background-color: #ff4d4d;color:white;':'background-color: lime;' }}">{{ $return[$f][$key]["HVCA"] }}</td>
                        <td style="{{ ($HVCNK!=-1 && $HVCNK!=$return[$f][$key]['HVCNK'])?(($HVCNK<$return[$f][$key]['HVCNK'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }}">{{ $return[$f][$key]["HVCNK"] }}</td>
                        <td>{{ $return[$f][$key]["TTR3T"] }}</td>
                        <td>{{ $return[$f][$key]["TTR3R"] }}</td>
                        <td style="{{ ($TTR3A!=-1 && $TTR3A!=$return[$f][$key]['TTR3A'])?(($TTR3A<$return[$f][$key]['TTR3A'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }} {{ $return[$f][$key]['TTR3A']<100?'background-color: #ff4d4d;color:white;':'background-color: lime;' }}">{{ $return[$f][$key]["TTR3A"] }}</td>
                        <td style="{{ ($TTR3NK!=-1 && $TTR3NK!=$return[$f][$key]['TTR3NK'])?(($TTR3NK<$return[$f][$key]['TTR3NK'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }}">{{ $return[$f][$key]["TTR3NK"] }}</td>
                        <td>{{ $return[$f][$key]["TTR12T"] }}</td>
                        <td>{{ $return[$f][$key]["TTR12R"] }}</td>
                        <td style="{{ ($TTR12A!=-1 && $TTR12A!=$return[$f][$key]['TTR12A'])?(($TTR12A<$return[$f][$key]['TTR12A'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }} {{ $return[$f][$key]['TTR12A']<100?'background-color: #ff4d4d;color:white;':'background-color: lime;' }}">{{ $return[$f][$key]["TTR12A"] }}</td>
                        <td style="{{ ($TTR12NK!=-1 && $TTR12NK!=$return[$f][$key]['TTR12NK'])?(($TTR12NK<$return[$f][$key]['TTR12NK'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }}">{{ $return[$f][$key]["TTR12NK"] }}</td>
                        <td>{{ $return[$f][$key]["SALDOUNSPECT"] }}</td>
                        <td>{{ $return[$f][$key]["SALDOUNSPECR"] }}</td>
                        <td style="{{ ($SALDOUNSPECA!=-1 && $SALDOUNSPECA!=$return[$f][$key]['SALDOUNSPECA'])?(($SALDOUNSPECA<$return[$f][$key]['SALDOUNSPECA'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }} {{ $return[$f][$key]['SALDOUNSPECA']<100?'background-color: #ff4d4d;color:white;':'background-color: lime;' }}">{{ $return[$f][$key]["SALDOUNSPECA"] }}</td>
                        <td style="{{ ($SALDOUNSPECNK!=-1 && $SALDOUNSPECNK!=$return[$f][$key]['SALDOUNSPECNK'])?(($SALDOUNSPECNK<$return[$f][$key]['SALDOUNSPECNK'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }}">{{ $return[$f][$key]["SALDOUNSPECNK"] }}</td>
                        <td>{{ $return[$f][$key]["PRODUNSPECT"] }}</td>
                        <td>{{ $return[$f][$key]["PRODUNSPECR"] }}</td>
                        <td style="{{ ($PRODUNSPECA!=-1 && $PRODUNSPECA!=$return[$f][$key]['PRODUNSPECA'])?(($PRODUNSPECA<$return[$f][$key]['PRODUNSPECA'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }} {{ $return[$f][$key]['PRODUNSPECA']<100?'background-color: #ff4d4d;color:white;':'background-color: lime;' }}">{{ $return[$f][$key]["PRODUNSPECA"] }}</td>
                        <td style="{{ ($PRODUNSPECNK!=-1 && $PRODUNSPECNK!=$return[$f][$key]['PRODUNSPECNK'])?(($PRODUNSPECNK<$return[$f][$key]['PRODUNSPECNK'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }}">{{ $return[$f][$key]["PRODUNSPECNK"] }}</td>
                        <td>{{ $return[$f][$key]["TGBKUANT"] }}</td>
                        <td>{{ $return[$f][$key]["TGBKUANR"] }}</td>
                        <td style="{{ ($TGBKUANA!=-1 && $TGBKUANA!=$return[$f][$key]['TGBKUANA'])?(($TGBKUANA<$return[$f][$key]['TGBKUANA'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }} {{ $return[$f][$key]['TGBKUANA']<100?'background-color: #ff4d4d;color:white;':'background-color: lime;' }}">{{ $return[$f][$key]["TGBKUANA"] }}</td>
                        <td style="{{ ($TGBKUANNK!=-1 && $TGBKUANNK!=$return[$f][$key]['TGBKUANNK'])?(($TGBKUANNK<$return[$f][$key]['TGBKUANNK'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }}">{{ $return[$f][$key]["TGBKUANNK"] }}</td>
                        <td>{{ $return[$f][$key]["TGBQUALT"] }}</td>
                        <td>{{ $return[$f][$key]["TGBQUALR"] }}</td>
                        <td style="{{ ($TGBQUALA!=-1 && $TGBQUALA!=$return[$f][$key]['TGBQUALA'])?(($TGBQUALA<$return[$f][$key]['TGBQUALA'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }} {{ $return[$f][$key]['TGBQUALA']<100?'background-color: #ff4d4d;color:white;':'background-color: lime;' }}">{{ $return[$f][$key]["TGBQUALA"] }}</td>
                        <td style="{{ ($TGBQUALNK!=-1 && $TGBQUALNK!=$return[$f][$key]['TGBQUALNK'])?(($TGBQUALNK<$return[$f][$key]['TGBQUALNK'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }}">{{ $return[$f][$key]["TGBQUALNK"] }}</td>
                        <td>{{ $return[$f][$key]["VALINST"] }}</td>
                        <td>{{ $return[$f][$key]["VALINSR"] }}</td>
                        <td style="{{ ($VALINSA!=-1 && $VALINSA!=$return[$f][$key]['VALINSA'])?(($VALINSA<$return[$f][$key]['VALINSA'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }} {{ $return[$f][$key]['VALINSA']<100?'background-color: #ff4d4d;color:white;':'background-color: lime;' }}">{{ $return[$f][$key]["VALINSA"] }}</td>
                        <td style="{{ ($VALINSNK!=-1 && $VALINSNK!=$return[$f][$key]['VALINSNK'])?(($VALINSNK<$return[$f][$key]['VALINSNK'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }}">{{ $return[$f][$key]["VALINSNK"] }}</td>
                        <td>{{ $return[$f][$key]["SQMT"] }}</td>
                        <td>{{ $return[$f][$key]["SQMR"] }}</td>
                        <td style="{{ ($SQMA!=-1 && $SQMA!=$return[$f][$key]['SQMA'])?(($SQMA<$return[$f][$key]['SQMA'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }} {{ $return[$f][$key]['SQMA']<100?'background-color: #ff4d4d;color:white;':'background-color: lime;' }}">{{ $return[$f][$key]["SQMA"] }}</td>
                        <td style="{{ ($SQMNK!=-1 && $SQMNK!=$return[$f][$key]['SQMNK'])?(($SQMNK<$return[$f][$key]['SQMNK'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }}">{{ $return[$f][$key]["SQMNK"] }}</td>
                        <td style="{{ ($TNK!=-1 && $TNK!=$return[$f][$key]['TNK'])?(($TNK<$return[$f][$key]['TNK'])?'outline-style: solid;outline-color: green;':'outline-style: solid;outline-color: red;'):'none;' }}">{{ $return[$f][$key]["TNK"] }}</td>
                        <?php
                            $QA=$return[$f][$key]["QA"];$QNK=$return[$f][$key]["QNK"];$SUGARA=$return[$f][$key]["SUGARA"];$SUGARNK=$return[$f][$key]["SUGARNK"];$HVCA=$return[$f][$key]["HVCA"];$HVCNK=$return[$f][$key]["HVCNK"];$TTR3A=$return[$f][$key]["TTR3A"];$TTR3NK=$return[$f][$key]["TTR3NK"];$SALDOUNSPECA=$return[$f][$key]["SALDOUNSPECA"];$SALDOUNSPECNK=$return[$f][$key]["SALDOUNSPECNK"];$PRODUNSPECA=$return[$f][$key]["PRODUNSPECA"];$PRODUNSPECNK=$return[$f][$key]["PRODUNSPECNK"];$TGBKUANA=$return[$f][$key]["TGBKUANA"];$TGBKUANNK=$return[$f][$key]["TGBKUANNK"];$TGBQUALA=$return[$f][$key]["TGBQUALA"];$TGBQUALNK=$return[$f][$key]["TGBQUALNK"];$VALINSA=$return[$f][$key]["VALINSA"];$VALINSNK=$return[$f][$key]["VALINSNK"];$SQMA=$return[$f][$key]["SQMA"];$SQMNK=$return[$f][$key]["SQMNK"];$TNK=$return[$f][$key]["TNK"];
                        ?>
                    @endif
                @endforeach
            @else
            <td>-</td>
            @endif
        </tr>
    @endforeach
@endforeach