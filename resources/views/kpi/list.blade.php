@extends('layout2')
@section('content')
  <a href="/order_manual/input" class="btn btn-info btn-xs" style="margin: 0 0 5px 0;">
      <span class="fa fa-plus"></span>
      <span>Input Baru</span>
  </a>
  <div class="panel panel-default" id="info">
    <div id="fixed-table-container-demo" class="fixed-table-container">
      <table class="table table-bordered table-fixed">
        <thead>
          <tr>
            <th>#</th>
            <th>NO INET</th>
            <th>NAMA ODP</th>
            <th>MITRA</th>
            <th>NAMA TIM</th>
            <th>NIK1</th>
            <th>NIK2</th>
            <th>TGL SELESAI</th>
            <th>STATUS</th>
            <th>TGL UPDATE</th>
            <th>ACT</th>
          </tr>
        </thead>
        <tbody>
          @foreach($data as $no => $list) 
            <tr>
              <td>{{ ++$no }}</td>
              <td>{{ $list->NO_INET }}</td>
              <td>{{ $list->NAMA_ODP }}</td>
              <td>{{ $list->MITRA }}</td>
              <td>{{ $list->NAMA_TIM }}</td>
              <td>{{ $list->NIK1 }}</td>
              <td>{{ $list->NIK2 }}</td>
              <td>{{ $list->TGL_SELESAI }}</td>
              <td>{{ $list->STATUS }}</td>
              <td>{{ $list->TGL_UPDATE }}</td>
              <td>-</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection
@section('script')
@endsection