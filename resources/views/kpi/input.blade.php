@extends('layout2')
@section('head')

@endsection
@section('content')
<div class="panel panel-default">
  <div class="panel-heading">
    @if (count($data))
      Edit {{ $data->NO_INET }}
    @else
      Input Baru
    @endif
  </div>
  <div class="panel-body">
      <form method="post" class="form-horizontal">
          <div class="form-group">
              <label for="NO_INET" class="col-sm-3 col-md-2 control-label">NO_INET</label>
              <div class="col-sm-5">
                  <input name="NO_INET" type="text" id="NO_INET" class="form-control" value="{{ $data->NO_INET or old('NO_INET')}}">
                  @foreach($errors->get('NO_INET') as $msg)
                      <span class="help-block">{{ $msg }}</span>
                  @endforeach
              </div>
          </div>
          <div class="form-group">
              <label for="NAMA_ODP" class="col-sm-3 col-md-2 control-label">NAMA_ODP</label>
              <div class="col-sm-5">
                  <input name="NAMA_ODP" type="text" id="NAMA_ODP" class="form-control" value="{{ $data->NAMA_ODP or old('NAMA_ODP')}}">
                  @foreach($errors->get('NAMA_ODP') as $msg)
                      <span class="help-block">{{ $msg }}</span>
                  @endforeach
              </div>
          </div>
          <div class="form-group">
              <label for="REGU_ID" class="col-sm-3 col-md-2 control-label">REGU</label>
              <div class="col-sm-5">
                  <input name="REGU_ID" type="text" id="REGU_ID" class="form-control" value="{{ $data->REGU_ID or old('REGU_ID')}}">
                  @foreach($errors->get('REGU_ID') as $msg)
                      <span class="help-block">{{ $msg }}</span>
                  @endforeach
              </div>
          </div>
          <div class="form-group">
              <label for="STATUS" class="col-sm-3 col-md-2 control-label">STATUS</label>
              <div class="col-sm-5">
                  <input name="STATUS" type="text" id="STATUS" class="form-control" value="{{ $data->STATUS or old('STATUS')}}">
                  @foreach($errors->get('STATUS') as $msg)
                      <span class="help-block">{{ $msg }}</span>
                  @endforeach
              </div>
          </div>
          <div class="form-group">
              <label for="TGL_SELESAI" class="col-sm-3 col-md-2 control-label">TGL_SELESAI</label>
              <div class="col-sm-5">
                  <input name="TGL_SELESAI" type="text" id="TGL_SELESAI" class="form-control" value="{{ $data->TGL_SELESAI or old('TGL_SELESAI')}}">
                  @foreach($errors->get('TGL_SELESAI') as $msg)
                      <span class="help-block">{{ $msg }}</span>
                  @endforeach
              </div>
          </div>
          <div class="form-group">
              <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                  <button class="btn btn-primary" type="submit">
                      <span class="glyphicon glyphicon-floppy-disk"></span>
                      <span>Simpan</span>
                  </button>
              </div>
          </div>
      </form>
  </div>
</div>
</div>
@endsection
@section('script')
<script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
<script type="text/javascript">
  $(function() {
    $('#program').select2({
      data:[{id:"Maintenance",text:"Maintenance"},{id:"Insert Tiang",text:"Insert Tiang"}],
      placeholder:"Pilih Program"
    });
    $('#TGL_SELESAI').datepicker({
              format: 'yyyy-mm-dd'
            });
    $('#STATUS').select2({data:[{id:"CLOSE",text:"CLOSE"},{id:"KENDALA",text:"KENDALA"}],
                placeholder:"Pilih Status"});

    var data = <?= json_encode($regu) ?>;
    var regu = $('#REGU_ID').select2({
        data: data,
        placeholder:"Pilih Regu"
    });
    $("#NAMA_ODP").inputmask("AAA-AAA-A{2,3}/9{3,4}");
  });
</script>
@endsection