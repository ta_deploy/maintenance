@extends('layout2')
@section('head')
<style type="text/css">
	.container {
		width: 100%;
		margin: 5px;
	}
	.table-condensed{
		font-size: 10px;
	}
	.txtHint{
		margin-top:10px;
	}
</style>
<link rel="stylesheet" href="/bower_components/amcharts3/amcharts/plugins/export/export.css" type="text/css" media="all" />
<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
@endsection
@section('content')
<?php
$auth = session('auth');
?>
<div class="row">
	<div class="col-sm-2">
		<input type="text" id="tgl" value="{{ date('Y-m-d') }}" class="form-control input-sm">
	</div>
    {{-- <div class="col-sm-2">
		<select name="jenis_order" id="jenis_order" class="form-control input-sm">
		    @foreach($data as $jo)
		        <option value="{{ $jo->id }}">{{ $jo->nama_order }}</option>
		    @endforeach
		</select>
	</div> --}}
	<div class="col-sm-2">
		<input type="submit" class="btn btn-sm ajax" value="Filter"/>
	</div>
</div>
<div class="txtHint">
	@if($auth->nama_instansi == "TELKOM AKSES")
	<div class="row">
		<div class="col-md-6">
			<div id="chart1div" style="height:450px;background-color:white;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;"></div>
		</div>
		<div class="col-md-6">
			<div id="chart2div" style="height:450px;background-color:white;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;"></div>
		</div>
	</div>
	<div class="row" style="margin-top: 25px;">
		<div class="col-md-6">
			<div id="chart3div" style="height:450px;background-color:white;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;"></div>
		</div>
	</div>
	@endif
<!-- <div class="row">
	@if($auth->nama_instansi == "TELKOM AKSES")
	<div class="col-md-6" style="margin-top:10px;">
		<div id="chart3div" style="height:250px;background-color:white;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;"></div>
	</div>
	@endif
	<div class="col-md-6" style="margin-top:10px;">
        <div id="chartrev" style="height:250px;background-color:white;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;"></div>
    </div>
</div> -->
</div>
@endsection
@section('script')
<script src="/bower_components/amcharts3/amcharts/amcharts.js" type="text/javascript"></script>
<script src="/bower_components/amcharts3/amcharts/serial.js" type="text/javascript"></script>
<script src="/bower_components/amcharts3/amcharts/pie.js" type="text/javascript"></script>
<script src="/bower_components/amcharts3/amcharts/themes/light.js" type="text/javascript"></script>
<script src="/bower_components/amcharts3/amcharts/plugins/export/export.min.js" type="text/javascript"></script>
<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(function() {

		var day = {
			format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0
		};
		$("#tgl").datepicker(day).on('changeDate', function (e) {
			$(this).datepicker('hide');
		});
		var data = <?= json_encode($data) ?>;
		var datachart = AmCharts.makeChart("chart1div", {
			"type": "serial",
			"theme": "light",
			"legend": {
				"maxColumns": 3,
				"position": "bottom"
			},
			"dataProvider": data,
			"graphs": [{
				"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
				"fillAlphas": 0.8,
				"labelText": "[[value]]",
				"lineAlpha": 0.3,
				"title": "Close",
				"type": "column",
				"valueField": "close"
			}, {
				"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
				"fillAlphas": 0.8,
				"labelText": "[[value]]",
				"lineAlpha": 0.3,
				"title": "Kendala Pelanggan",
				"type": "column",
				"valueField": "kendala_pelanggan"
			}, {
				"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
				"fillAlphas": 0.8,
				"labelText": "[[value]]",
				"lineAlpha": 0.3,
				"title": "Kendala Teknis",
				"type": "column",
				"valueField": "kendala_teknis"
			}],
			"valueAxes": [{
				"stackType": "regular",
				"axisAlpha": 0.3,
				"gridAlpha": 0,
				"title": "All Work Chart"

			}],
			"categoryField": "nama_order",

			"depth3D": 20,
			"angle": 30,
			"rotate": true,
			"categoryAxis": {
				"gridPosition": "start",
				"fillAlpha": 0.05,
				"position": "left"
			},
			"export": {
				"enabled": true
			}
		});
		var action = <?= json_encode($action) ?>;
		var actchart = AmCharts.makeChart("chart2div", {
			"theme": "light",
			"type": "serial",
			"dataProvider": action,
			"valueAxes": [{
				"title": "Action/Cause Chart"
			}],
			"graphs": [{
				"balloonText": "Tiket [[category]]:[[value]]",
				"fillAlphas": 1,
				"lineAlpha": 0.2,
				"title": "Income",
				"type": "column",
				"labelText": "[[value]]",
				"valueField": "rows"
			}],
			"depth3D": 20,
			"angle": 30,
			"rotate": true,
			"categoryField": "action_cause",
			"categoryAxis": {
				"gridPosition": "start",
				"fillAlpha": 0.05,
				"position": "left"
			},
			"export": {
				"enabled": true
			}
		});

		var gamas = <?= json_encode($gamas) ?>;
		var gamaschart = AmCharts.makeChart("chart3div", {
			"theme": "light",
			"type": "serial",
			"dataProvider": gamas,
			"valueAxes": [{
				"title": "GAMAS Chart"
			}],
			"graphs": [{
				"balloonText": "Tiket [[category]]:[[value]]",
				"fillAlphas": 1,
				"lineAlpha": 0.2,
				"title": "Income",
				"type": "column",
				"labelText": "[[value]]",
				"valueField": "rows"
			}],
			"depth3D": 20,
			"angle": 30,
			"rotate": true,
			"categoryField": "gamas_cause",
			"categoryAxis": {
				"gridPosition": "start",
				"fillAlpha": 0.05,
				"position": "left"
			},
			"export": {
				"enabled": true
			}
		});
		// var remo = <?= json_encode($remo) ?>;
		// var remochart = AmCharts.makeChart( "chart3div", {
		//   	"type": "pie",
		//   	"theme": "light",
		//   	"dataProvider": [{
		// 	    "country": "Close",
		// 	    "value": remo.close,
		// 	    "url":"/listRemo/close"
		//   	},{
		// 	    "country": "kendala pelanggan",
		// 	    "value": remo.kendala_pelanggan,
		// 	    "url":"/listRemo/kendala_pelanggan"
		//   	},{
		// 	    "country": "kendala teknis",
		// 	    "value": remo.kendala_teknis,
		// 	    "url":"/listRemo/kendala_teknis"
		//   	},{
		// 	    "country": "Sisa Order",
		// 	    "value": remo.order_remo,
		// 	    "url":"/listRemo/sisa"
		//   	}],
		//   	"urlField": "",
		//   	"valueField": "value",
		//   	"titleField": "country",
		//   	"urlField": "url",
		//   	"labelText": "[[country]] : [[value]]",
		//   	"outlineAlpha": 0.4,
		//   	"depth3D": 15,
		//   	"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[percents]]%</b> ([[value]])</span>",
		//   	"angle": 60,
		//   	"categoryAxis": {
		//         "title": "Remo Per Bulan Chart"
		//     },
		//   	"export": {
		//     	"enabled": true
		//   	}
		// });
		$(".ajax").on("click", function(){
			$('.wait-indicator').show();
			var tgl = $('#tgl').val();
  		// var jenis_order = $('#jenis_order').val();
  		$.getJSON("/ajaxHome/"+tgl+"/all", function(text) {
  			datachart.dataProvider = text.datachart;
  			actchart.dataProvider = text.action;
  			gamaschart.dataProvider = text.gamas;
	    // 	remochart.dataProvider = [{
			  //   "country": "Close",
			  //   "value": text.remo.close,
			  //   "url":"/listRemo/close"
		  	// },{
			  //   "country": "kendala pelanggan",
			  //   "value": text.remo.kendala_pelanggan,
			  //   "url":"/listRemo/kendala_pelanggan"
		  	// },{
			  //   "country": "kendala teknis",
			  //   "value": text.remo.kendala_teknis,
			  //   "url":"/listRemo/kendala_teknis"
		  	// },{
			  //   "country": "Sisa Order",
			  //   "value": text.remo.order_remo,
			  //   "url":"/listRemo/sisa"
		  	// }];
		  	datachart.validateData();
		  	actchart.validateData();
		  	gamaschart.validateData();
	    	// remochart.validateData();
	    }).done(function(){
	    	$('.wait-indicator').hide()
	    });
	});
		// var line = <?= json_encode($line) ?>;
		// var chartrev = AmCharts.makeChart( "chartrev", {
		//   	"type": "serial",
		//     "theme": "light",
		//     "legend": {
		//         "useGraphSettings": true
		//     },
		//     "dataProvider": line,
		//     "valueAxes": [{
		//         "integersOnly": true,
		//         "axisAlpha": 0,
		//         "dashLength": 5,
		//         "gridCount": 10,
		//         "position": "left",
		//         "title": "Place taken"
		//     }],
		//     "startDuration": 0.5,
		//     "graphs": [{
		//         "balloonText": "place taken by UPATEK in [[category]]: [[value]]",
		//         "bullet": "round",
		//         "title": "UPATEK",
		//         "valueField": "upatek",
		// 				"fillAlphas": 0
		// 		    }, {
		// 		        "balloonText": "place taken by SPM in [[category]]: [[value]]",
		// 		        "bullet": "round",
		// 		        "title": "SPM",
		// 		        "valueField": "spm",
		// 				"fillAlphas": 0
		// 		    }, {
		// 		        "balloonText": "place taken by CUI in [[category]]: [[value]]",
		// 		        "bullet": "round",
		// 		        "title": "CUI",
		// 		        "valueField": "cui",
		// 				"fillAlphas": 0
		// 		    }, {
		// 		        "balloonText": "place taken by AGB in [[category]]: [[value]]",
		// 		        "bullet": "round",
		// 		        "title": "AGB",
		// 		        "valueField": "agb",
		// 				"fillAlphas": 0
		// 		    }],
		//     "chartCursor": {
		//         "cursorAlpha": 0,
		//         "zoomable": false
		//     },
		//     "categoryField": "bulan",
		//     "categoryAxis": {
		//         "gridPosition": "start",
		//         "axisAlpha": 0,
		//         "fillAlpha": 0.05,
		//         "fillColor": "#000000",
		//         "gridAlpha": 0,
		//         "position": "top"
		//     },
		//     "export": {
		//     	"enabled": true,
		//         "position": "bottom-right"
		//      }
		// } );
	});
</script>
@endsection