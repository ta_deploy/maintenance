@extends('layout2')
@section('head')
  <style type="text/css">
    .datepicker {
        z-index:2600 !important; 
    }
  </style>
@endsection
@section('content')
<div id="input_cutoff_mod" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header header-success" style="background: #eee">
          <strong style="color:black;" class="judul_cutoff">Detail Cut Off</strong>
          <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
        </div>
        <div class="modal-body">
            <form id="submit-form" method="post" autocomplete="off">
                <div v class="panel panel-info">
                    <div class="panel-heading">
                        <span class="panel-title">
                            <a href="/" class="btn btn-sm btn-default">
                                <span class="ion ion-arrow-left-a"></span>
                            </a>
                            Proggress
                        </span>
                        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="start_date" class="control-label">Tanggal Mulai</label>
                            <input name="start_date" type="text" id="start_date" class="form-control date_me" value="{{ date('Y-m-d', strtotime('first day of this month') ) }}"/>
                        </div>
                        <div class="form-group">
                            <label for="end_date" class="control-label">Tanggal Selesai</label>
                            <input name="end_date" type="text" id="end_date" class="form-control date_me" value="{{ date('Y-m-d', strtotime('last day of this month') ) }}"/>
                        </div>
                        <code class="eror_warn" style="display: none;">*Jarak Tanggal Salah, Harap Dicek Kembali!</code>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-block btn-info submit_cutoff" type="button">Submit</button>
        </div>
      </div>
    </div>
</div>
<div id="undo_cutoff_mod" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header header-success" style="background: #eee">
          <strong style="color:black;" class="judul_cutoff">Detail Cut Off</strong>
          <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
        </div>
        <div class="modal-body">
            <table class="table table-bordered table-fixed">
                <thead>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Tanggal Pembuatan</th>
                    <th>Action</th>
                </thead>
                <tbody class="body_tbl">

                </tbody>
            </table>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-block btn-info submit_cutoff" type="button">Submit</button>
        </div>
      </div>
    </div>
</div>
    <a href="/commerce/admin/input" class="btn btn-info btn-xs" style="margin: 0 0 5px 0;">
        <span class="fa fa-plus"></span>
        <span>Input Baru</span>
    </a>
    <a data-toggle="modal" class="btn btn-success btn-xs cutoff" style="margin: 0 0 5px 0;" type="button">
        <span class="fa fa-check-circle"></span>
        Cut Off
    </a>
    <a data-toggle="modal" class="btn btn-warning btn-xs undo_cutoff" style="margin: 0 0 5px 0;" type="button">
        <span class="fa fa-close"></span>
        Undo Cut Off
    </a>
    <div class="panel panel-default" id="info">
        <div class="panel-heading">Index Admin</div>
        <div id="fixed-table-container-demo" class="fixed-table-container">
            <table class="table table-bordered table-fixed">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Judul</th>
                    <th>Bulan</th>
                    <th>Jenis Pekerjaan</th>
                    <th>Nilai/Files</th>
                    <th>Approval</th>
                    <th>Status</th>
                    <th>Catatan</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($data as $no => $list) 
                    <?php
                        $nde="/upload/commerce/admin/".$list->id."/".$list->nde;
                        $justifikasi="/upload/commerce/admin/".$list->id."/".$list->justifikasi;
                    ?>
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td>{{ $list->judul }}</td>
                            <td>{{ $list->bulan }}</td>
                            <td>{{ $list->jenis }}</td>
                            <td>{{ $list->nilai }}<br/>
                                {!! $list->nde?'<span class="label label-info"><a href="'.$nde.'">nde</a></span>':'' !!}
                                {!! $list->justifikasi?'<span class="label label-info"><a href="'.$justifikasi.'">justifikasi</a></span>':'' !!}
                            </td>
                            <td>{{ $list->status }}</td>
                            <td>{{ $list->approval }}</td>
                            <td>{{ $list->catatan }}</td>
                            <td>
                                <a href="/commerce/admin/{{ $list->id }}">
                                    <button type="button" class="btn btn-info btn-outline btn-rounded btn-xs">
                                        <span class="btn-label-icon left fa fa-pencil"></span>Edit
                                    </button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
    $(function() {

        $(".cutoff").on('click', function(){
            $('#input_cutoff_mod').modal('show')
            $('.judul_cutoff').text('detail Cut Off '+ $(this).attr('data-judul'))
            $('#id_comm').val($(this).attr('data-id'))
        })

        $('#input_cutoff_mod').on('shown.bs.modal', function() {
            var month = {
                format: "yyyy-mm",
                viewMode: 2,
                minViewMode: 1,
                todayBtn: "linked",
                autoclose: true,
                todayHighlight: true,
                container: '#input_cutoff_mod .modal-body'
            };
            
            var day = {
                format: "yyyy-mm-dd",
                viewMode: 0,
                minViewMode: 0,
                todayBtn: "linked",
                autoclose: true,
                todayHighlight: true,
                container: '#input_cutoff_mod .modal-body'
            };

            $(".date_me").datepicker(day).on('changeDate',function(ev){
                console.log('chagne')
                if(new Date($('#start_date').val() ).getTime() >= new Date($('#end_date').val() ).getTime()){
                    $('#start_date, #end_date').val('')

                    $(".eror_warn").css({
                        'display': 'block'
                    })
                }else{
                    $(".eror_warn").css({
                        'display': 'none'
                    })
                }
            });
        });

        $('.undo_cutoff').on('click', function(){
            $('#undo_cutoff_mod').modal('show')
        })

        $('#undo_cutoff_mod').on('shown.bs.modal', function() {
            $.ajax({
                url: "/get_undo_cutoff",
                type: 'GET',
                dataType: 'JSON'
            }).done(function(data){
                var body;
                
                $.each(data, function(key, val) {
                    body += '<tr>'
                    body += '<td>', val.start_date, '</td>'
                    body += '<td>', val.end_date, '</td>'
                    body += '<td>'+ val.tgl_cutoff+ '</td>'
                    body += '<td> <a href="/get_undo_cutoff/'+ val.id +'"> <button type="button" class="btn btn-danger btn-outline btn-rounded btn-xs"> <span class="btn-label-icon left fa fa-exclamation-triangle"></span>Cut oFF</button></a></td>';
                    body += '</tr>';
                })
                
                $('.body_tbl').html(body)
            })
        })

        $(".submit_cutoff").on('click', function(){
            $.ajax({
            url: "/input_cutoff",
            type: 'POST',
            data: {
                start_date: $("#start_date").val(),
                end_date: $("#end_date").val()
            },
            dataType: 'JSON'
            }).done(function(data){
                window.location.href = data.url;
            })
        })

        })    
    </script>
@endsection