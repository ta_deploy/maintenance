@extends('layout2')
@section('head')

@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            @if (isset($data))
                Edit {{ $data->judul }}
            @else
                Input Baru
            @endif
        </div>
        <div class="panel-body">
            <form method="post" enctype="multipart/form-data" autocomplete="off">
              <div class="row">
                <div class="col-sm-12 form-group">
                  <label for="judul" class="col-md-3 control-label">Judul Pekerjaan</label>
                  <input type="text" name="judul" class="form-control" id="judul" placeholder="Judul Pekerjaan" value="{{ $data->judul or '' }}" >
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label for="jenis" class="col-md-5 control-label">Jenis Pekerjaan</label>
                  <input type="text" name="jenis" class="form-control" id="jenis" placeholder="Jenis Pekerjaan" value="{{ $data->jenis or '' }}">
                </div>
                <div class="col-sm-4 form-group">
                  <label for="bulan" class="col-md-4 control-label">Bulan</label>
                  <input type="text" name="bulan" class="form-control" id="bulan" placeholder="Bulan" value="{{ $data->bulan or '' }}">
                </div>
                <div class="col-sm-4 form-group">
                  <label for="nilai" class="col-md-4 control-label">Nilai</label>
                  <input type="text" name="nilai" class="form-control" id="nilai" placeholder="Nilai" value="{{ $data->nilai or '' }}">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 form-group">
                  <label for="justifikasi" class="col-md-3 control-label">Justifikasi</label>
                  <input name="justifikasi" type="file" class="form-control" id="justifikasi" >
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 form-group">
                  <label for="nde" class="col-md-3 control-label">NDE</label>
                  <input name="nde" type="file" class="form-control" id="nde" >
                </div>
              </div>

            <div class="row">
                <div class="col-md-9">
                  <button type="submit" class="btn"><span class="btn-label-icon left fa fa-database"></span>Submit</button>
                </div>
            </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            $('#jenis').select2({
                data:[{id:"QE AKSES",text:"QE AKSES"},{id:"QE RECOVERY",text:"QE RECOVERY"},{id:"RELOKASI UTILITAS",text:"RELOKASI UTILITAS"}],
                placeholder:"Pilih Jenis Pekerjaan"
            });
        });
    </script>
@endsection