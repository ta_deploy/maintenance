@extends('layout2')
@section('head')

@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Approval
        </div>
        <div class="panel-body">
            <form  method="post" autocomplete="off">
              <div class="row">
                <div class="col-sm-12 form-group">
                  <label for="status" class="col-md-3 control-label">Status</label>
                  <input type="text" name="status" class="form-control" id="status">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 form-group">
                  <label for="pid" class="col-md-3 control-label">PID</label>
                  <input type="text" name="pid" class="form-control" id="pid" placeholder="PID" value="{{ $data->pid or '' }}" >
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 form-group">
                  <label for="catatan" class="col-md-3 control-label">Catatan</label>
                  <textarea name="catatan" class="form-control" id="catatan" placeholder="Catatan"></textarea>
                </div>
              </div>

            <div class="row">
                <div class="col-md-9">
                  <button type="submit" class="btn"><span class="btn-label-icon left fa fa-database"></span>Submit</button>
                </div>
            </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            $('#status').select2({
                data:[{id:"Approve",text:"Approve"},{id:"Reject",text:"Reject"}],
                placeholder:"Pilih Status"
            });
        });
    </script>
@endsection