@extends('layout2')
@section('content')
    <div class="panel panel-default" id="info">
        <div class="panel-heading">Index Approval</div>
        <div id="fixed-table-container-demo" class="fixed-table-container">
            <table class="table table-bordered table-fixed">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Judul</th>
                    <th>Bulan</th>
                    <th>Jenis Pekerjaan</th>
                    <th>Nilai</th>
                    <th>Approval</th>
                    <th>Status</th>
                    <th>Catatan</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($data as $no => $list) 
                    <?php
                        $nde="/upload/commerce/admin/".$list->id."/".$list->nde;
                        $justifikasi="/upload/commerce/admin/".$list->id."/".$list->justifikasi;
                    ?>
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td>{{ $list->judul }}</td>
                            <td>{{ $list->bulan }}</td>
                            <td>{{ $list->jenis }}</td>
                            <td>{{ $list->nilai }}<br/>
                                {!! $list->nde?'<span class="label label-info"><a target="_BLANK" href="'.$nde.'">nde</a></span>':'' !!}
                                {!! $list->justifikasi?'<span class="label label-info"><a target="_BLANK" href="'.$justifikasi.'">justifikasi</a></span>':'' !!}
                            </td>
                            <td>{{ $list->status }}</td>
                            <td>{{ $list->approval }}</td>
                            <td>{{ $list->catatan }}</td>
                            <td>
                                <a href="/commerce/approval/{{ $list->id }}">
                                    <button type="button" class="btn btn-success btn-outline btn-rounded btn-xs">
                                        <span class="btn-label-icon left fa fa-pencil"></span>Edit
                                    </button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection