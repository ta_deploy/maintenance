@extends('layout2')
@section('head')
    <link rel="stylesheet" href="/bower_components/select2/select2.css" />
    <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
@endsection
@section('content')
    @if (session('auth')->level == '2' || session('auth')->level == '53')
    <div class="panel panel-default">
        <div class="panel-heading">
            Logistik
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal">
                <div class="form-group">
                    <label for="proaktif_id" class="col-sm-3 col-md-2 control-label">Project ID</label>
                    <div class="col-sm-8">
                        <input name="proaktif_id" type="text" id="proaktif_id" class="form-control" value="{{ old('proaktif_id') }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama_gudang" class="col-sm-3 col-md-2 control-label">Nama Gudang</label>
                    <div class="col-sm-8">
                        <input name="nama_gudang" type="text" id="nama_gudang" class="form-control" value="{{ old('nama_gudang') }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="mitra" class="col-sm-3 col-md-2 control-label">Peruntukan</label>
                    <div class="col-sm-8">
                        <input name="mitra" type="text" id="mitra" class="form-control" value="{{ old('mitra') }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tgl" class="col-sm-3 col-md-2 control-label">Tgl Pengeluaran</label>
                    <div class="col-sm-4">
                        <input name="tgl" type="text" id="tgl" class="form-control" value="{{ old('tgl') }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel panel-default" id="info">
    <div class="panel-heading">List</div>
    <div class="panel-body table-responsive">
        <table class="table table-bordered table-fixed">
            <tr>
                <th>Alista Id</th>
                <th>tgl</th>
                <th>nama gudang</th>
                <th>project</th>
                <th>pengambil</th>
                <th>mitra</th>
                <th>id barang</th>
                <th>nama barang</th>
                <th>jumlah</th>
                <th>no rfc</th>
                
            </tr>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ $d->alista_id }}</td>
                <td>{{ $d->tgl }}</td>
                <td>{{ $d->nama_gudang }}</td>
                <td>{{ $d->project }}</td>
                <td>{{ $d->pengambil }}</td>
                <td>{{ $d->mitra }}</td>
                <td>{{ $d->id_barang }}</td>
                <td>{{ $d->nama_barang }}</td>
                <td>{{ $d->jumlah }}</td>
                <td>{{ $d->no_rfc }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
    @endif
@endsection
@section('script')
    <script src="/bower_components/select2/select2.min.js"></script>
    <script>
        $(function() {
            var project = <?= json_encode($project) ?>;
            $('#proaktif_id').select2({
                data: project,
                placeholder: 'Pilih project',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });
            var gudang = <?= json_encode($gudang) ?>;
            $('#nama_gudang').select2({
                data: gudang,
                placeholder: 'Pilih Gudang',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });
            var mitra = <?= json_encode($mitra) ?>;
            $('#mitra').select2({
                data: mitra,
                placeholder: 'Pilih Peruntukan',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });
        });
    </script>
@endsection