@extends('layout2')
@section('head')
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Input Nog Manual</span>
</h1>
@endsection
@section('content')
  <?php
      $auth = (session('auth'));
  ?>

<div class="panel panel-default">
  <div class="panel-body">
    <div class="form col-sm-12 col-md-12">
      <code class="validate_no_koor_odp" style="display: none;">*Format Koordinat Salah, Contoh Yang Benar  -3.092600, 115.283800 </code>
      <form method="post" class="form-horizontal" id="submit-form" enctype="multipart/form-data">
        <div class="form-group">
          <label for="odp_nm" class="col-sm-4 col-md-3 control-label">Nama ODP</label>
          <div class="col-sm-8">
            <input name="nama_odp" type="text" id="odp_nm" class="form-control input-sm" value="{{ @$data->nama_odp }}"/>
          </div>
        </div>
        <div class="form-group">
          <label for="sto" class="col-sm-4 col-md-3 control-label">STO</label>
          <div class="col-sm-8">
            <input name="sto" type="text" id="sto" class="form-control input-sm" value="{{ @$data->sto_nog }}"/>
          </div>
        </div>
        <div class="form-group">
          <label for="source" class="col-sm-4 col-md-3 control-label">Source</label>
          <div class="col-sm-8">
            <select name="source" id="source" class="form-control">
              <option value="">-</option>
              <option value="IXSA">IXSA</option>
              <option value="TOMMAN">TOMMAN</option>
              <option value="INFRACARE">INFRACARE</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="port" class="col-sm-4 col-md-3 control-label">Jumlah Port</label>
          <div class="col-sm-8">
            <input name="jml_port" type="text" id="port" class="form-control input-sm" value="{{ @$data->jml_port }}"/>
          </div>
        </div>
        <div class="form-group">
          <label for="odp_koor" class="col-sm-4 col-md-3 control-label">Koordinat</label>
          <div class="col-sm-8">
            <input name="odp_koor" type="text" id="odp_koor" class="form-control input-sm" value="{{ @$data->odp_koor }}"/>
          </div>
        </div>
        <div class="form-group {{ $errors->has('tindak_lanjut') ? 'has-error' : '' }}">
          <label for="tindak_lanjut" class="col-sm-4 col-md-3 control-label">Tindak Lanjut</label>
          <div class="col-sm-8">
            <select name="tindak_lanjut" id="tindak_lanjut" class="form-control">
              <option value="">-</option>
              <option value="PT-1">PT-1</option>
              <option value="PT-2">PT-2</option>
              <option value="PT-3">PT-3</option>
            </select>
          </div>
        </div>
        <div class="form-group {{ $errors->has('status_order') ? 'has-error' : '' }}">
          <label for="status_order" class="col-sm-4 col-md-3 control-label">Status Order</label>
          <div class="col-sm-8">
            <select name="status_order" id="status_order" class="form-control">
              <option value="">-</option>
              <option value="DONE">Done</option>
              <option value="KENDALA">Kendala</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="note" class="col-sm-4 col-md-3 control-label">Note</label>
          <div class="col-sm-8">
            <textarea name="note" id="note" class="form-control input-sm">{{ @$data->note }}</textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-3 col-md-offset-3 col-sm-3">
            <button class="btn btn-primary" type="submit">
              <span class="glyphicon glyphicon-floppy-disk"></span>
              <span>Simpan</span>
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('script')
  <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
  <script>
    $(function(){
      $('#tindak_lanjut').select2();
      $('#status_order').select2();

      var splitter = [{"id":"1:16", "text":"1:16"},{"id":"1:8+1:2", "text":"1:8+1:2"},{"id":"1:8+1:4", "text":"1:8+1:4"},{"id":"1:8+1:8", "text":"1:8+1:8"},{"id":"1:4+1:4+1:8", "text":"1:4+1:4+1:8"},
      {"id":"1:8+1:8+1:4", "text":"1:8+1:8+1:4"},
      {"id":"1:8+1:8+1:4", "text":"1:2+1:2+1:8"},
      {"id":"1:8+1:8+1:2", "text":"1:8+1:8+1:2"}];

      $('#port').select2({
        data: splitter,
        minimumResultsForSearch: Infinity,
        placeholder: "Pilih Splitter"
      });

      var data = {!! json_encode($sto) !!},
      combo = $('#sto').select2({
          data: data,
          placeholder: "Pilih STO"
      });

      $("#odp_nm").inputmask("AAA-AAA-A{2,3}/9{3,4}");

      var data = {!! json_encode($data ?? null) !!};

      if(!$.isEmptyObject(data) ){
        $('#odp_nm, #port').attr('readonly', 'readonly')

        $('#tindak_lanjut').val(data.tindak_lanjut).change();
        $('#sto').val(data.sto_nog).change();

        if(data.jenis_order.length != 0){
          $('#source').val(data.jenis_order).change();
        }

        if(data.status_order != null && data.status_order.length != 0){
          $('#status_order').val(data.status_order).change();
        }
      }

      $('#submit-form').submit(function(e){
        var regex =/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)[,°]\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?°?)$/;

        if(!regex.test($.trim($('#odp_koor').val())) && $.trim($('#odp_koor').val()) !== ''){
          $('.validate_no_koor_odp').css({
            "display": "block",
          });

          $('#odp_koor').css({
            border: "2px solid red",
          });

          $('#odp_koor').focus();
          e.preventDefault();
        }else{
          $('.validate_no_koor_odp').css({display: "none"});
          $('#odp_koor').css({border: ""})
        }
      });
    })
  </script>
@endsection