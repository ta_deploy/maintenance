@extends('layout2')
@section('head')
<link rel="stylesheet" href="/bower_components/select2/select2.css" />
<link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>List ALPRO {{ $sto }}</span>
</h1>
@endsection
@section('content')
<div class="form-group" style="margin-left: 20px;">
    <a href="/download_alpro/{{ Request::segment(3) }}/{{ Request::segment(5) }}" class="btn btn-info get_url_load btn-xs">
        <span class="glyphicon glyphicon-download"></span>
        <span>Download</span>
    </a>
</div>
<div class="panel panel-default" id="info">
    <div id="fixed-table-container-demo" class="fixed-table-container">
        <table class="table table-bordered table-fixed">
            <thead>
            <tr>
                <th>#</th>
                <th>Nama ODP</th>
                <th>Sumber Data</th>
                <th>Jumlah Port</th>
                <th>Kondisi ODP</th>
                <th>Tanggal Dibuat</th>
                <th>Tindak Lanjut</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->nama_odp }}</td>
                    <td>
                        @if(@$d->jml_port)
                            {{ str_replace('_', ' ', $d->jenis_order) }}
                        @else
                            BOT Lapor ODP
                        @endif
                    </td>
                    <td>{{ $d->jml_port ?? '-' }}</td>
                    <td>
                        @if(@$d->jenis_kerusakan)
                            <ul>
                                @foreach($d->jenis_kerusakan as $v)
                                    <li>{{ str_replace('_', ' ', $v) }}</li>
                                @endforeach
                            </ul>
                        @else
                            -
                        @endif
                    </td>
                    <td>{{ $d->created_at }}</td>
                    <td>{{ $d->tindak_lanjut }}</td>
                    <td>
                        @if(@$d->jml_port)
                            <a class="btn btn-info" href="/action_nog/{{ $d->id }}">Lihat</a>
                            @if(in_array($d->tindak_lanjut, ['PT-1']) && is_null($d->dispatch_regu_id) )
                                <a class="btn btn-success" href="/order_Nog/{{ $d->id }}">Dispatch</a>
                            @endif
                        @else
                            @if($d->maintenance_id == 0)
                                <a class="btn btn-info" href="/lapor_alpro/{{ $d->id_mo }}">Lihat</a>
                            @endif
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
    $(function(){
        $('#status').select2({
            width: '100%',
            placeholder: 'Sumber Pekerjaan Bisa Dipilih Lebih Dari Satu'
        });
    });
</script>
@endsection