@extends('layout2')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LIST SALDO ALPRO {{ Request::Segment(3) }}</span>
</h1>
{{-- <a href="/nog_manual/input" class="btn btn-info pull-right" style="margin: 0 0 15px;">
    <span class="glyphicon glyphicon-plus"></span>
    <span>Tambah Nog Manual</span>
</a> --}}
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Dashboard Alpro</span>
</h1>
@endsection
@section('content')
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Download Dokumen</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <label class="switcher switcher-rounded switcher-primary">
                <input type="checkbox" id="generate_evi">
                <div class="switcher-indicator">
                  <div class="switcher-yes">YES</div>
                  <div class="switcher-no">NO</div>
                </div>
                Generate Foto Evidence
            </label>
            <label class="switcher switcher-rounded switcher-primary">
                <input type="checkbox" id="generate_doc">
                <div class="switcher-indicator">
                  <div class="switcher-yes">YES</div>
                  <div class="switcher-no">NO</div>
                </div>
                Generate Data
            </label>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn download_btn btn-primary">Download</button>
        </div>
      </div>
    </div>
</div>
<div class="panel panel-default" id="info">
    <div class="pull-left">
        <form class="navbar-form navbar-left" method="get" style="margin-bottom: 10px;">
            <a class="btn btn-info btn-xs show_modal" type="button" data-toggle="modal" data-target="#exampleModal" data-href="/download_alpro/{{ Request::segment(2) == 'alpro' ? 'all' : Request::segment(2) }}/{{ Request::segment(3) }}"><span class="glyphicon glyphicon-download"></span>Download</a>

            {{-- <div class="form-group" style="width: 300px; margin-left: 20px;">
                <select class="form-control input-sm" id="status" name="status[]" multiple>
                    @foreach ($get_stat as $k => $v)
                        <option value={{ $k }}>{{ $v }}</option>
                    @endforeach
                </select>
            </div> --}}
            @if($tampilan == 'progress')
                <div class="form-group" style="width: 300px; margin-left: 20px;">
                    <select class="form-control input-sm" id="kendala_maintenance" name="kendala_maintenance[]" multiple>
                        @foreach ($get_rusak as $k => $v)
                            <option value={{ $k }}>{{ $v }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
            <div class="form-group" style="width: 300px; margin-left: 20px;">
                <select class="form-control input-sm" id="tampilan_dashboard" name="tampilan_dashboard">
                    <option value="progress">Per Progress</option>
                    <option value="kondisi_odp">Per Kondisi ODP</option>
                </select>
            </div>
            <button type="submit" id="goto" class="btn btn-success btn-sm">Go!</button>
        </form>
    </div>
    <div id="fixed-table-container-demo" class="fixed-table-container">
        @if($tampilan == 'progress')
            <table class="table table-bordered table-fixed table-bordered">
                <thead>
                    <tr>
                        <th rowspan="2" class="text-center valign-middle">#</th>
                        <th rowspan="2" class="text-center valign-middle">STO</th>

                        <th rowspan="2" class="text-center valign-middle">TOTAL ORDER</th>
                        <th rowspan="2" class="text-center valign-middle">SISA ORDER</th>
                        {{-- <th rowspan="2" class="text-center valign-middle">INIT. ORDER</th>
                        <th colspan="3" class="text-center">Tindak Lanjut</th> --}}
                        {{-- <th colspan="3" class="text-center">HD</th> --}}
                        <th colspan="1" class="text-center">HD</th>
                        {{-- <th colspan="3" class="text-center">Laporan Teknisi PT-1&2</th> --}}
                        <th colspan="3" class="text-center">Laporan Teknisi</th>
                    </tr>
                    <tr>
                        {{-- <th>PT-1</th>
                        <th class="text-center">PT-2</th>
                        <th class="text-center">PT-3</th>
                        <th class="text-center">PT-1</th>
                        <th class="text-center">PT-2</th> --}}
                        <th class="text-center">Maintenance</th>
                        <th class="text-center">Progress</th>
                        <th class="text-center">Kendala</th>
                        <th class="text-center">Selesai</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = $total_all = $total = $hd_mt = $progress = $kendala = $done = 0;
                    @endphp
                    @if(isset($fn) )
                        @foreach($fn as $k => $v)
                            <tr>
                                <td>{{ ++$no }}</td>
                                <td>{{ $v['sto'] }}</td>
                                @if($v['status']['total_all'] != 0)
                                    @php
                                        $total_all += $v['status']['total_all'];
                                    @endphp
                                    <td class="text-right">{{ $v['status']['total_all'] }}</td>
                                @else
                                    <td class="text-right"> - </td>
                                @endif

                                @if($v['status']['total'] != 0)
                                    @php
                                        $total += $v['status']['total'];
                                    @endphp
                                <td class="text-right"><a class="get_url_load" href="/detail_list/nog/{{ $k }}/total/{{ Request::segment(3) }}">{{ $v['status']['total'] }}</a></td>
                                @else
                                    <td class="text-right"> - </td>
                                @endif

                                {{-- @if($v['status']['init'] != 0)
                                    <td class="text-right"><a class="get_url_load" href="/detail_list/nog/{{ $k }}/In_Progress/{{ Request::segment(3) }}">{{ $v['status']['init'] }}</a></td>
                                @else
                                    <td class="text-right"> - </td>
                                @endif

                                @if($v['status']['pt_1'] != 0)
                                    <td class="text-right"><a class="get_url_load" href="/detail_list/nog/{{ $k }}/pt_1/{{ Request::segment(3) }}">{{ $v['status']['pt_1'] }}</a></td>
                                @else
                                    <td class="text-right"> - </td>
                                @endif

                                @if($v['status']['pt_2'] != 0)
                                    <td class="text-right"><a class="get_url_load" href="/detail_list/nog/{{ $k }}/pt_2/{{ Request::segment(3) }}">{{ $v['status']['pt_2'] }}</a></td>
                                @else
                                    <td class="text-right"> - </td>
                                @endif

                                @if($v['status']['pt_3'] != 0)
                                    <td class="text-right"><a class="get_url_load" href="/detail_list/nog/{{ $k }}/pt_3/{{ Request::segment(3) }}">{{ $v['status']['pt_3'] }}</a></td>
                                @else
                                    <td class="text-right"> - </td>
                                @endif

                                @if($v['status']['hd_pt1'] != 0)
                                    <td class="text-right"><a class="get_url_load" href="/detail_list/nog/{{ $k }}/hd_pt1/{{ Request::segment(3) }}">{{ $v['status']['hd_pt1'] }}</a></td>
                                @else
                                    <td class="text-right"> - </td>
                                @endif

                                @if($v['status']['hd_pt2'] != 0)
                                    <td class="text-right"><a class="get_url_load" href="/detail_list/nog/{{ $k }}/hd_pt2/{{ Request::segment(3) }}">{{ $v['status']['hd_pt2'] }}</a></td>
                                @else
                                    <td class="text-right"> - </td>
                                @endif --}}

                                @if($v['status']['hd_mt'] != 0)
                                    @php
                                        $hd_mt += $v['status']['hd_mt'];
                                    @endphp
                                    <td class="text-right"><a class="get_url_load" href="/detail_list/nog/{{ $k }}/hd_mt/{{ Request::segment(3) }}">{{ $v['status']['hd_mt'] }}</a></td>
                                @else
                                    <td class="text-right"> - </td>
                                @endif


                                @if($v['status']['progress'] != 0)
                                    @php
                                        $progress += $v['status']['progress'];
                                    @endphp
                                    <td class="text-right"><a class="get_url_load" href="/detail_list/nog/{{ $k }}/progress/{{ Request::segment(3) }}">{{ $v['status']['progress'] }}</a></td>
                                @else
                                    <td class="text-right"> - </td>
                                @endif


                                @if($v['status']['kendala'] != 0)
                                    @php
                                        $kendala += $v['status']['kendala'];
                                    @endphp
                                    <td class="text-right"><a class="get_url_load" href="/detail_list/nog/{{ $k }}/kendala/{{ Request::segment(3) }}">{{ $v['status']['kendala'] }}</a></td>
                                @else
                                    <td class="text-right"> - </td>
                                @endif

                                @if($v['status']['done'] != 0)
                                    @php
                                        $done += $v['status']['done'];
                                    @endphp
                                    <td class="text-right"><a class="get_url_load" href="/detail_list/nog/{{ $k }}/done/{{ Request::segment(3) }}">{{ $v['status']['done'] }}</a></td>
                                @else
                                    <td class="text-right"> - </td>
                                @endif
                            </tr>
                        @endforeach
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" style="text-align:center;">Total Pekerjaan</td>
                        @if($total_all != 0)
                            <td class="text-right">{{ $total_all }}</td>
                        @else
                            <td class="text-right"> - </td>
                        @endif

                        @if($total != 0)
                            <td class="text-right"><a class="get_url_load" href="/detail_list/nog/all/total/{{ Request::segment(3) }}">{{ $total }}</a></td>
                        @else
                            <td class="text-right"> - </td>
                        @endif

                        {{-- @if($v['status']['init'] != 0)
                            <td class="text-right"><a class="get_url_load" href="/detail_list/nog/all/In_Progress/{{ Request::segment(3) }}">{{ $v['status']['init'] }}</a></td>
                        @else
                            <td class="text-right"> - </td>
                        @endif

                        @if($v['status']['pt_1'] != 0)
                            <td class="text-right"><a class="get_url_load" href="/detail_list/nog/all/pt_1/{{ Request::segment(3) }}">{{ $v['status']['pt_1'] }}</a></td>
                        @else
                            <td class="text-right"> - </td>
                        @endif

                        @if($v['status']['pt_2'] != 0)
                            <td class="text-right"><a class="get_url_load" href="/detail_list/nog/all/pt_2/{{ Request::segment(3) }}">{{ $v['status']['pt_2'] }}</a></td>
                        @else
                            <td class="text-right"> - </td>
                        @endif

                        @if($v['status']['pt_3'] != 0)
                            <td class="text-right"><a class="get_url_load" href="/detail_list/nog/all/pt_3/{{ Request::segment(3) }}">{{ $v['status']['pt_3'] }}</a></td>
                        @else
                            <td class="text-right"> - </td>
                        @endif

                        @if($v['status']['hd_pt1'] != 0)
                            <td class="text-right"><a class="get_url_load" href="/detail_list/nog/all/hd_pt1/{{ Request::segment(3) }}">{{ $v['status']['hd_pt1'] }}</a></td>
                        @else
                            <td class="text-right"> - </td>
                        @endif

                        @if($v['status']['hd_pt2'] != 0)
                            <td class="text-right"><a class="get_url_load" href="/detail_list/nog/all/hd_pt2/{{ Request::segment(3) }}">{{ $v['status']['hd_pt2'] }}</a></td>
                        @else
                            <td class="text-right"> - </td>
                        @endif --}}

                        @if($hd_mt != 0)
                            <td class="text-right"><a class="get_url_load" href="/detail_list/nog/all/hd_mt/{{ Request::segment(3) }}">{{ $hd_mt }}</a></td>
                        @else
                            <td class="text-right"> - </td>
                        @endif


                        @if($progress != 0)
                            <td class="text-right"><a class="get_url_load" href="/detail_list/nog/all/progress/{{ Request::segment(3) }}">{{ $progress }}</a></td>
                        @else
                            <td class="text-right"> - </td>
                        @endif


                        @if($kendala != 0)
                            <td class="text-right"><a class="get_url_load" href="/detail_list/nog/all/kendala/{{ Request::segment(3) }}">{{ $kendala }}</a></td>
                        @else
                            <td class="text-right"> - </td>
                        @endif

                        @if($done != 0)
                            <td class="text-right"><a class="get_url_load" href="/detail_list/nog/all/done/{{ Request::segment(3) }}">{{ $done }}</a></td>
                        @else
                            <td class="text-right"> - </td>
                        @endif
                    </tr>
                </tfoot>
            </table>
        @else
            <table class="table table-bordered table-fixed table-bordered">
                <thead>
                    <tr>
                        <th rowspan="2" class="text-center valign-middle">STO</th>
                        <th colspan="{{ count($get_rusak) }}" class="text-center">Jenis Kerusakan</th>
                    </tr>
                    <tr>
                        @foreach ($get_rusak as $v)
                            <th class="text-center">{{ $v }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach ($fn as $k => $v)
                        <tr>
                            <td><a class="get_url_load" href="/detail_list/kondisi_alpro/{{ $k }}/all/{{ Request::segment(3) }}">{{ $v['sto'] }}</a></td>
                            {{-- <td>{{ $v['sto'] }}</td> --}}
                            @foreach ($v['kendala'] as $kk => $vv)
                                <td>
                                    @if ($vv != 0)
                                        <a class="get_url_load" href="/detail_list/kondisi_alpro/{{ $k }}/{{ $kk }}/{{ Request::segment(3) }}">{{ $vv }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function(){
        var href = '',
        tampilan = {!! json_encode($tampilan) !!};

        $('#status').select2({
            width: '100%',
            placeholder: 'Sumber Pekerjaan Bisa Dipilih Lebih Dari Satu'
        });

        $('#tampilan_dashboard').select2({
            width: '100%',
            placeholder: 'Tampilan Dashboard'
        });

        $('#tampilan_dashboard').val(tampilan).change();

        $('#kendala_maintenance').select2({
            width: '100%',
            placeholder: 'Gangguan BOT Bisa Dipilih Lebih Dari Satu'
        });

        $.each($('.get_url_load'), function(k, v){
            var url_extend = $(this).attr('href');
            if(window.location.href.split('?')[1]){
                url_extend = $(this).attr('href') + '?' + window.location.href.split('?')[1]
            }
            $(this).attr('href', url_extend)
        })

        var data_stts = {!! json_encode($list_data) !!} ?? null;

        if(data_stts != null && data_stts.length != 0){
            $('#status').val(data_stts).change();
        }

        $('.show_modal').on('click', function(){
            href = $(this).data('href');
        });

        $('.download_btn').on('click', function(){
            var generate_evidence = $("#generate_evi").is(":checked"),
            generate_doc = $("#generate_doc").is(":checked"),
            params_evi = 'nok',
            params_doc = 'nok',
            new_link = '';

            if(generate_evidence){
                params_evi = 'ok'
            }

            if(generate_doc){
                params_doc = 'ok'
            }

            new_link = href + '/' + params_evi + '/' + params_doc;

            if(window.location.href.split('?')[1]){
                new_link = new_link + '?' + window.location.href.split('?')[1]
            }

            window.location = new_link;
        });
    });
</script>
@endsection