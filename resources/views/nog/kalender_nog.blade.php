@extends('layout2')
@section('heading')
{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/vis/4.20.1/vis.min.css" rel="stylesheet" type="text/css" /> --}}
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>KALENDER NOG</span>
</h1>
<style>
    #visualization {
      box-sizing: border-box;
      width: 100%;
      height: 300px;
    }

    .vis-item > .vis-dot {
      border-color: red;
    }

    .vis-item.vis-point {
      background-color: #59c4e4;
    }

</style>
@endsection
@section('head')
@endsection
@section('content')
{{-- <div id="visualization"></div>
<div class="modal fade" id="eventModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div> --}}
<form method="GET" style="margin-bottom: 10px;">
  <div class="form-group row">
      <div class="col-sm-12">
          <div class="input-group">
              <span class="input-group-btn">
                  <button type="button" class="btn btn-info dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pilih <span class="caret"></span></button>
                  <ul class="dropdown-menu">
                    <li><a href="#" id="daily">Daily</a></li>
                    <li><a href="#" id="monthly">Monthly</a></li>
                  </ul>
              </span>
              <span class="ins">
                  <input type="text" name="tgl" id="tgl" value="{{ date('Y-m') }}" class="form-control input-sm">
              </span>
          </div>
      </div>
  </div>
  <div class="form-group row">
    <div class="col-md-4">
      <input class="form-control get_data_selection" id="mitra" name="mitra">
    </div>
    <div class="col-md-4">
      <input class="form-control get_data_selection" id="sektor" name="sektor">
    </div>
    <div class="col-md-4">
      <input class="form-control get_data_selection" id="sto" name="sto">
    </div>
  </div>
  <span class="input-group-btn">
    <button type="submit" class="btn btn-default btn-primary btn-sm go" type="button">Cari!</button>
  </span>
</form>
<div class="panel panel-default" id="info">
  <div class="panel-heading">Kalender ODP</div>
  <div id="fixed-table-container-demo" class="fixed-table-container">
    <table class="table table-bordered table-fixed">
      <thead>
        <tr>
          <th rowspan="2">#</th>
          <th rowspan="2">ODP</th>
          <th style="text-align: center;" colspan="{{ count($days) }}">{{ $bulan }}</th>
        </tr>
        <tr>
          @foreach($days as $h)
            <th>{{ $h }}</th>
          @endforeach
        </tr>
      </thead>
        <tbody>
          @foreach($fd as $k => $v)
            <tr>
              <td>{{ ++$k }}</td>
              <td>{{ $v['odp'] }}</td>
              @foreach($v['tgl'] as $kk => $vv)
                <td style="{{ $v['status'] == 'close' && $vv != 0 ? "background-color: green; color: black;" : '' }}">{{ $vv == 0 ? '-' : $vv }}</td>
              @endforeach
            </tr>
          @endforeach
        </tbody>
        <tfoot>
          @foreach($tfoot as $k => $v)
            <tr>
              <td colspan="2">{{ $k }}</td>
              @foreach($v as $vv)
                <td>{{ $vv == 0 ? '-' : $vv }}</td>
              @endforeach
            </tr>
          @endforeach
        </tfoot>
    </table>
  </div>
</div>
@endsection
@section('script')
<script type="application/javascript" src="https://unpkg.com/moment@2.23.0/moment.js"></script>
{{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vis/4.20.1/vis.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script> --}}
<script type="text/javascript">
  $(function(){
    var day = {
      format: "yyyy-mm-dd",
      viewMode: 0,
      minViewMode: 0,
      autoclose:true
    };

    var month = {
      format: "yyyy-mm",
      viewMode: 2,
      minViewMode: 1,
      autoclose:true
    };

    var mitra = $('#mitra').select2({
      allowClear: true,
      data: {!! json_encode($mitra) !!},
      placeholder: 'Pilih Mitra',
      formatSelection: function(data) {
        return data.text
      },
      formatResult: function(data) {
        return data.text;
      }
    });

    var sektor = $('#sektor').select2({
      allowClear: true,
      data: {!! json_encode($sektor) !!},
      placeholder: 'Pilih Sektor',
      formatSelection: function(data) {
        return data.text
      },
      formatResult: function(data) {
        return data.text;
      }
    });

    var sto = $('#sto').select2({
      allowClear: true,
      data: {!! json_encode($sto_get) !!},
      placeholder: 'Pilih STO',
      formatSelection: function(data) {
        return data.text
      },
      formatResult: function(data) {
        return data.text;
      }
    });

    var q_u = {!! json_encode($requestan) !!};
    if (!$.isEmptyObject(q_u) ) {
        var mitra = {!! json_encode($mitra) !!};
        $('#mitra').select2({
            allowClear: true,
            data: mitra,
            placeholder: 'Pilih Mitra',
            formatSelection: function(data) {
                return data.text
            },
            formatResult: function(data) {
                return data.text;
            }
        });

        var sektor_jns = {!! json_encode($sektor) !!};
        $('#sektor').select2({
            allowClear: true,
            data: sektor_jns,
            placeholder: 'Pilih Sektor',
            formatSelection: function(data) {
                return data.text
            },
            formatResult: function(data) {
                return data.text;
            }
        });

        var sto_get_jns = {!! json_encode($sto_get) !!};
        $('#sto').select2({
            allowClear: true,
            data: sto_get_jns,
            placeholder: 'Pilih STO',
            formatSelection: function(data) {
                return data.text
            },
            formatResult: function(data) {
                return data.text;
            }
        });

        $('#mitra').val(q_u.mitra).trigger('change');
        $('#sektor').val(q_u.sektor).trigger('change');
        $('#sto').val(q_u.sto).trigger('change');

        $('#tgl').val(q_u.tgl);
    }

    var d = new Date(),
    monthly = '' + (d.getMonth() + 1),
    dayly = '' + d.getDate(),
    year = d.getFullYear();

    if (monthly.length < 2) monthly='0' + monthly;
    if (dayly.length < 2) dayly='0' + dayly;

    $("#tgl").datepicker(month).on('changeDate',function(ev){
        $("#mitra").val('').change();
        $.ajax({
            type: 'GET',
            url: '/mitra/prod_search/ajx',
            dataType: 'json',
            data: {
                tgl: $(this).val()
            },
            success: function(value) {
                mitra.select2({
                    allowClear: true,
                    data: value.mitra,
                    placeholder: 'Pilih Mitra',
                    formatSelection: function(data) {
                        return data.text
                    },
                    formatResult: function(data) {
                        return data.text;
                    }
                });
            }
        });
        $("#sektor").val('').change();
        // $("#order").val('').change();
    });

    $("#daily").click(function(){
      $("#tgl").remove();
      $(".ins").after("<input type=text class='form-control input-sm' id=tgl>");
      $("#tgl").datepicker(day);
      $("#tgl").val([year, monthly, dayly].join('-'));
    });

    $("#monthly").click(function(){
      $("#tgl").remove();
      $(".ins").after("<input type=text class='form-control input-sm' id=tgl>");
      $("#tgl").datepicker(month);
      $("#tgl").val([year, monthly].join('-'));
    });

    // events = [],
    // isi_group = [],
    // settings = {};

    // settings.stages = [];
    // var no = 1
    // no_2 = 1;

    // $.each(data_event, function(k, v) {
    //   var jmlnya = 0
    //   $.each(v, function(kk, vv){
    //     ++no;
    //     events.push({
    //       'id': no,
    //       'name': String(vv.jml),
    //       // 'name': vv.nama_odp,
    //       'id_main': vv.id,
    //       'rekap_odp': vv.rekap_odp,
    //       'category': vv.nama_regu,
    //       'interval': {
    //         'from': vv.tgl_start,
    //         'to': vv.tgl_end,
    //       }
    //     });
    //   });
    // });

    // $.each(regu_load, function(k, v){
    //   ++no_2;
    //   settings.stages.push({
    //     'category': v.nama_regu,
    //     'from': v.tgl_start,
    //     'to': v.tgl_end,
    //     'id_regu': v.id_regu,
    //     'color': "#"
    //   })

    //   isi_group.push({
    //     'id': no_2,
    //     'content': v.nama_regu,
    //   });
    // })

    // let itemsArray = [];
    // // let itemsArray = settings.stages.map(s => {
    // //   return {
    // //     id: s.category,
    // //     content: s.category,
    // //     start: s.from,
    // //     end: s.to,
    // //     id_regu: s.id_regu,
    // //     color: s.color,
    // //     type: 'background',
    // //     group: settings.stages.indexOf(s) + 2,
    // //     className: s.category.toLocaleLowerCase()
    // //   };
    // // });
    // // for (let i = 0; i < events.length; i++) {

    // //   let stage = settings.stages.filter(s=>s.category === events[i].category)[0];
    // //   let stageIdx = 0;
    // //   if (stage) {
    // //     stageIdx = settings.stages.indexOf(stage) + 1;
    // //   }

    // //   if(typeof events[i].name != 'undefined' && events[i].name){
    // //     itemsArray.push({
    // //       id: events[i].id + '' + i,
    // //       content: events[i].name,
    // //       start: events[i].interval.from,
    // //       end: events[i].interval.to,
    // //       color: events[i].color,
    // //       className: categoryToClassName(events[i].category),
    // //       type: 'point',
    // //       group: stageIdx + 1,
    // //       editable: {
    // //           add: true,         // add new items by double tapping
    // //           updateTime: false,  // drag items horizontally
    // //           updateGroup: false, // drag items from one group to another
    // //           remove: false,       // delete an item by tapping the delete button top right
    // //       }
    // //     });
    // //   }
    // // }

    // $.each(events, function(k, v){
    //   let stage = settings.stages.filter(s=>s.category === v.category)[0];
    //   let stageIdx = 0;
    //   if (stage) {
    //     stageIdx = settings.stages.indexOf(stage) + 1;
    //   }

    //   if(typeof v.name != 'undefined' && v.name){
    //     itemsArray.push({
    //       id: v.id + '' + k,
    //       id_m: v.id_main,
    //       rekap_odp: v.rekap_odp,
    //       nama_regu: v.category,
    //       content: v.name,
    //       start: v.interval.from,
    //       end: v.interval.to,
    //       className: categoryToClassName(v.category),
    //       type: 'point',
    //       group: stageIdx + 1,
    //       editable: {
    //         add: true,         // add new items by double tapping
    //         updateTime: false,  // drag items horizontally
    //         updateGroup: false, // drag items from one group to another
    //         remove: false,       // delete an item by tapping the delete button top right
    //       }
    //     });
    //   }
    // })

    // let items = new vis.DataSet(itemsArray);
    // var groups = new vis.DataSet(isi_group);

    // // var groups = new vis.DataSet([
    // //   {id: 1, content: 'Leadership Team Meetings'},
    // //   {id: 2, content: 'Think'},
    // //   {id: 3, content: 'Act'},
    // //   {id: 4, content: 'Deliver'}
    // // ]);
    // var container = document.getElementById('visualization');
    // var options = {
    //   // start: tgl.tgl_awal,
    //   // end: tgl.tgl_end,
    //   start: moment(tgl.tgl_awal,'YYYY-MM-DD').subtract(1,'days'),
    //   end: moment(tgl.tgl_end,'YYYY-MM-DD').add(1,'days'),
    //   min: moment(tgl.tgl_awal,'YYYY-MM-DD').subtract(1,'days'),
    //   max: moment(tgl.tgl_end,'YYYY-MM-DD').add(1,'days'),
    //   zoomMin: 1000 * 60 * 60 * 24,
    //   zoomMax: 1000 * 60 * 60 * 24 * 31 * 3,
    //   editable: {
    //     add: true,         // add new items by double tapping
    //     updateTime: true,  // drag items horizontally
    //     updateGroup: false, // drag items from one group to another
    //     remove: false,       // delete an item by tapping the delete button top right
    //     overrideItems: false  // allow these options to override item.editable
    //   },
    //   orientation: {axis: "both"},
    //   onUpdate: function (item, callback) {
    //     // console.log(item);
    //     $('#eventModal').on('shown.bs.modal', function (event) {
    //       var eventElt = $(event.relatedTarget); // Button that triggered the modal
    //       console.log(eventElt, item);
    //       var eventContent = item.content; //eventElt.data('content'); // Extract info from data-* attributes
    //       // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    //       // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    //       var modal = $(this);
    //       modal.find('.modal-title').html(`List ODP <u>${item.nama_regu.split('<br/>')[0]}</u> periode <b>${item.start}</b> sampai <b>${item.end}</b>`);

    //       var table_html = "<table class='tbl_material table table-striped table-bordered table-hover'>";
		// 			table_html += "<thead class='thead-dark'>";
		// 			table_html += "<tr>";
		// 			table_html += "<th>Nomor</th>";
		// 			table_html += "<th>Nama ODP</th>";
		// 			table_html += "</tr>";
		// 			table_html += "</thead>";
		// 			table_html += "<tbody>";

    //       var no_tbl = 0;
		// 			$.each(item.rekap_odp, function(key, val){
		// 				table_html += "<tr>";
		// 				table_html += `<td> ${++no_tbl} </td>`;
		// 				table_html += `<td> ${val} </td>`;
		// 				table_html += "</tr>";
		// 			});

		// 			table_html += "</tbody>";
		// 			table_html += "</table>";


    //       modal.find('.modal-body').html(table_html);
    //     });
    //     $('#eventModal').modal('toggle');
    //   },
    //   onAdd: function (item, callback) {
    //     callback(null); // cancel item creation
    //   },
    // };

    // var timeline = new vis.Timeline(container, items, groups, options);

    // $('#eventModal').on('hidden.bs.modal', function (e) {
    //   $('#eventModal').off('shown.bs.modal');
    //   $('#eventModal').find('.modal-title').text('');
    //   $('#eventModal').find('.modal-body input#eventContent').val('');
    // })

    // function categoryToClassName(categoryName) {
    //   return 'timeline-item-'+ spacesToHyphens(categoryName.toLocaleLowerCase());
    // }

    // function spacesToHyphens(stringInputWithSpaces) {
    //   return stringInputWithSpaces.replace(/\s/g, '-');
    // }
  })
</script>
@endsection