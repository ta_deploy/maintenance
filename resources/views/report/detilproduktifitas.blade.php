@extends('layout2')
@section('head')
  <style type="text/css">
    th{
        text-align: center;
        vertical-align: middle;
    }
    .total{
        color:#425ff4;
    }
    
  </style>
@endsection
@section('content')
    <div class="panel panel-default" id="info">
        <div class="panel-heading"><b>Rekap Closing Tiket Bulan  Regu </b></div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped" id="table">
                <tr>
                    <th>#</th>
                    <th>ORDER</th>
                    <th>Tgl Selesai</th>
                    <th>Material</th>
                    <th>Jasa</th>
                    <th>TOT</th>
                </tr>
                @foreach($data as $no => $d)
                    <?php
                      if(session('auth')->nama_instansi=='TELKOM AKSES'){
                        $jasa=$d->jasa_telkom;$material=$d->material_telkom;
                      }else{
                        $jasa=$d->jasa_ta;$material=$d->material_ta;
                      }
                    ?>
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>
                            @if(session('auth')->maintenance_level==1)
                                <a href="/tech/{{ $d->id }}">{{ $d->no_tiket }}</a>
                            @else
                                {{ $d->no_tiket }}
                            @endif
                             <span class="label label-primary">{{ $d->nama1 }}</span> & <span class="label label-primary">{{ $d->nama2 }}</span></td>
                        <td>{{ $d->tgl_selesai }}</td>
                        <td>{{ number_format($material) ?: '-' }}</td>
                        <td>{{ number_format($jasa) ?: '-' }}</td>
                        <td>{{ number_format($material+$jasa) ?: '-' }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
@section('script')
@endsection