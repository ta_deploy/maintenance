@foreach($data as $no => $d)
    @if($d->status1)
    <tr>
        <td></td>
        <td><span class="success">{{ $d->nik1 }}:{{$d->nama1}}</span></td>
        <td>{{ $d->spesialis }}</td>
    </tr>
    @endif
    @if($d->status2)
    <tr>
        <td></td>
        <td><span class="success">{{ $d->nik2 }}:{{$d->nama2}}</span></td>
        <td>{{ $d->spesialis }}</td>
    </tr>
    @endif
    @if($d->status3)
    <tr>
        <td></td>
        <td><span class="success">{{ $d->nik3 }}:{{$d->nama3}}</span></td>
        <td>{{ $d->spesialis }}</td>
    </tr>
    @endif
    @if($d->status4)
    <tr>
        <td></td>
        <td><span class="success">{{ $d->nik4 }}:{{$d->nama4}}</span></td>
        <td>{{ $d->spesialis }}</td>
    </tr>
    @endif
@endforeach