@extends('layout2')
@section('head')
<link rel="stylesheet" href="/bower_components/select2/select2.css" />
<link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
<style type="text/css">
    .datepicker thead tr:first-child th{
        background: #c64648;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>DOWNLOAD DATA</span>
</h1>
@endsection
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Form Download Jenis Order
    </div>
    <div class="panel-body">
        <form method="post" class="form-horizontal" id="submit_dwn">
            <div class="form-group">
                <label for="mitra" class="col-sm-3 col-md-2 control-label">Jenis Order</label>
                <div class="col-sm-5">
                    <input name="jenis_order" type="text" id="jenis_order" class="form-control" value="{{ old('jenis_order') }}">
                </div>
            </div>
            <div class="form-group">
                <label for="mitra" class="col-sm-3 col-md-2 control-label">Date</label>
                <div class="col-sm-5">
                    <input type="text" name="tgl" id="tgl" value="{{ date('Y-m') }}" class="form-control input-sm">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                    <a class="btn btn-primary common_dwn" type="button" data-jns="dwn">
                        <span class="glyphicon glyphicon-floppy-disk"></span>
                        <span>Download</span>
                    </a>
                </div>
                <div class="col-sm-2 rebound" style="display: none;">
                    <a class="btn btn-info common_dwn" type="button" data-jns="dwn_reb">
                        <span class="glyphicon glyphicon-floppy-disk"></span>
                        <span>Download Rebound</span>
                    </a>
                </div>
                <input type="hidden" name="type" value="" id="hid_jns">
            </div>
        </form>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        Form Download Kategory
    </div>
    <div class="panel-body">
        <form method="post" class="form-horizontal" action="/download_kategory">
            <div class="form-group">
                <label for="mitra" class="col-sm-3 col-md-2 control-label">Kategory</label>
                <div class="col-sm-5">
                    <select class="form-control" name="kategory" class="kategory">
                        <option value="1">Rekap Closing (Insert Tiang, ODP LOSS, Reboundary)</option>
                        <option value="2">Rekap Closing Majid (REBOUNDARY, ODP SEHAT , INSERT TIANG, ODP FULL, ODP LOSS)</option>
                        <option value="3">Rekap VALINS</option>
                        <option value="4">Rekap NOG</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="mitra" class="col-sm-3 col-md-2 control-label">Date</label>
                <div class="col-sm-5">
                    <input type="text" name="tgl_flex" id="tgl_flex" value="{{ date('Y-m') }}" class="form-control input-sm">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                    <button class="btn btn-primary common_dwn" type="submit" data-jns="dwn">
                        <span class="glyphicon glyphicon-floppy-disk"></span>
                        <span>Download</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script src="/bower_components/select2/select2.min.js"></script>
<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
<script>
    $(function() {
        var day = {
            format: "yyyy-mm-dd",
            viewMode: 0,
            minViewMode: 0,
            todayHighlight: true,
            autoclose: true,
            todayBtn: "linked",

        };

        $("#tgl, #tgl_flex").datepicker(day)

        var data = <?= json_encode($data) ?>;
        $('#jenis_order').select2({
            data: data,
            placeholder: 'Pilih jenis order',
            formatSelection: function(data) { return data.text },
            formatResult: function(data) {
                return  data.text;
            }
        });

        $('#jenis_order').on("change", function(e) {
            console.log($(this).val())
            if($.inArray($(this).val(), ['8', '10']) !== -1){
                $('.rebound').css({'display' : 'block'});
            }else{
                $('.rebound').css({'display' : 'none'});
            }
        });

        $('.common_dwn').on('click', function(e){
            $('#hid_jns').val($(this).attr('data-jns'));
            $('#submit_dwn').submit();
        });
    });
</script>
@endsection