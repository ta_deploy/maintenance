<style type="text/css">
    table, th, td {
        border: 1px solid black;
    }
    .currency {
        mso-number-format:"\#\,\#\#0\.000";
    }
</style>
<div class="panel panel-default" id="info">
    <div class="panel-heading">Rincian Revenue</div>
    <div id="fixed-table-container-demo" class="fixed-table-container">
        <table class="table table-bordered table-fixed">
          <tr>
            <th width="5" rowspan="5" class="head">#</th>
            <th width="15" rowspan="5" class="head">Id Item</th>
            <th width="40" rowspan="5" class="head">Uraian</th>
            <th width="15" rowspan="5" class="head">Jasa</th>
            <th width="15" rowspan="5" class="head">Material</th>
            <th width="9" rowspan="5" class="head">Satuan</th>
            @foreach($head as $h)
                <th width="15" class="head">{{ $h }}</th>
            @endforeach
            <th width="15" rowspan="5" class="head">Total</th>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                @foreach($head as $h)
                    <th width="15" class="head">{{ $title[$h]['action'] }}</th>
                @endforeach
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    @foreach($head as $h)
                        <th width="15" class="head">{{ $title[$h]['regu_name'] }}</th>
                    @endforeach

                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    @foreach($head as $h)
                        <th width="15" class="head">{{ $title[$h]['nik1'] }} & {{ $title[$h]['nik2'] }}</th>
                    @endforeach

                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    @foreach($head as $h)
                        <th width="15" class="head">{{ $title[$h]['sto'] }}</th>
                    @endforeach

                </tr>
                @foreach($data as $no => $list)
                <?php
                $subtotal = 0;
                ?>
                <tr>
                    <td valign="middle">{{ ++$no }}</td>
                    <td class="head" valign="middle">{{ $list['id_item'] }}</td>
                    <td valign="middle">{{ ($list['uraian']) }}</td>
                    <td valign="middle">{{ ($list['jasa']) }}</td>
                    <td valign="middle" class="currency">{{ ($list['material']) }}</td>
                    <td valign="middle" class="currency">{{ $list['satuan'] }}</td>
                    @foreach($list['no_tiket'] as $no2 => $l2)
                    <td valign="middle" class="currency">{{ $l2 ?: '-' }}</td>
                    <?php
                    $subtotal += ($l2*$list['jasa'])+($l2*$list['material']);
                    ?>
                    @endforeach
                    <td align="right" valign="middle" class="currency">{{ ($subtotal) }}</td>
                </tr>
                @endforeach
                <?php
                $sumtotal = 0;
                $sumtotaljasa = 0;
                $sumtotalmaterial = 0;
                ?>
                <tr><td class="head" colspan=6 align="right">Total Material</td>
                    @foreach($head as $h)
                    <td align="right" class="currency">{{ (@$totalmaterial[$h]) }}</td>
                    <?php
                    $sumtotalmaterial += @$totalmaterial[$h];
                    ?>
                    @endforeach
                    <td class="currency">{{ ($sumtotalmaterial) }}</td>
                </tr>
                <tr><td class="head" colspan=6 align="right">Total Jasa</td>
                    @foreach($head as $h)
                    <td align="right" class="currency">{{ (@$totaljasa[$h]) }}</td>
                    <?php
                    $sumtotaljasa += @$totaljasa[$h];
                    ?>
                    @endforeach
                    <td class="currency">{{ ($sumtotaljasa) }}</td>
                </tr>
                <tr><td class="head" colspan=6 align="right">Total</td>
                    @foreach($head as $h)
                    <td align="right" class="currency">{{ ($total[$h]) }}</td>
                    <?php
                    $sumtotal += $total[$h];
                    ?>
                    @endforeach
                    <td class="currency">{{ ($sumtotal) }}</td>
                </tr>
            </table>
        </div>
    </div>