<style type="text/css">
    th{
        text-align: center;
        vertical-align: middle;
    }
</style>
<div class="panel panel-default">
    <div class="panel-heading">Reporting Procurement</div>
    <div class="panel-body table-responsive">
        <table class="table table-bordered table-striped" id="table">
            <tr>
                <th>#</th>
                <th>Nama<br/>Tim</th>
                @foreach($mitra as $key => $d)
                    <td>{{ $key }}</td>
                @endforeach 
            </tr>
            @foreach($data as $no => $c)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $c['mitra'] }}</td>
                @foreach($c['status'] as $key => $d)
                <td>{{ $d }}</td>
                @endforeach
            </tr>
            @endforeach
        </table>
    </div>
</div>