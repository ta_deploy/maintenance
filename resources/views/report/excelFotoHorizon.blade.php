<style type="text/css">
    table, th, td {
       border: 1px solid black;
    }
    .currency {
        mso-number-format:"\#\,\#\#0\.000";
    }
</style>
<div class="panel panel-default" id="info">
    <div class="panel-heading">Evidence</div>
     <div id="fixed-table-container-demo" class="fixed-table-container">
        <table class="table table-bordered table-fixed">
            @php
                $nomor = 0
            @endphp
            @foreach($mt as $no => $list)
                @php
                    $path = public_path() . "/upload/maintenance/" . $list->id . "/";
                    $path_file = @preg_grep('~^\w(.*-th).*$~', @scandir($path) );
                    // dd($path_file, $list->id);
                    $path2 = public_path() . "/upload2/maintenance/" . $list->id . "/";
                    $path2_file = @preg_grep('~^\w(.*-th).*$~', @scandir($path2) );

                    $path3 = public_path() . "/upload3/maintenance/" . $list->id . "/";
                    $path3_file = @preg_grep('~^\w(.*-th).*$~', @scandir($path3) );

                    if(count($path_file) != 0)
                    {
                        $path_photo_arr = [];

                        $files_path = array_values($path_file);
                        if ($files_path) {
                            foreach($files_path as $v)
                            {
                                $path_photo_arr[$list->id][] = public_path() . "/upload/maintenance/" . $list->id . "/" . $v;
                            }
                        }
                    }
                    elseif(count($path2_file) != 0)
                    {
                        $path_photo_arr = [];

                        $files_path = array_values($path2_file);
                        if ($files_path) {
                            foreach($files_path as $v)
                            {
                                $path_photo_arr[$list->id][] = public_path() . "/upload2/maintenance/" . $list->id . "/" . $v;
                            }
                        }
                    }
                    elseif(count($path3_file) != 0)
                    {
                        $path_photo_arr = [];

                        $files_path = array_values($path3_file);
                        if ($files_path) {
                            foreach($files_path as $v)
                            {
                                $path_photo_arr[$list->id][] = public_path() . "/upload3/maintenance/" . $list->id . "/" . $v;
                            }
                        }
                    }
                    else
                    {
                        $path_photo_arr[$list->id][] = public_path() . "/image/placeholder.gif";
                    }
                @endphp
                <tr>
                    <td valign="middle">{{ ++$nomor }}. {{$list->no_tiket}}</td>
                    @foreach ($path_photo_arr as $k => $v)
                        @foreach ($v as $vv)
                            @if($k == $list->id)
                                <td valign="middle">
                                    <a>
                                        <img src="{{ $vv }}" />
                                    </a>
                                </td>
                            @endif
                        @endforeach
                    @endforeach
                </tr>
            @endforeach
        </table>
    </div>
</div>