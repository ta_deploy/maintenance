    <div class="panel panel-default" id="info">
        <div class="panel-heading">Data Maintenance</div>
         <div id="fixed-table-container-demo" class="fixed-table-container">
            <table class="table table-bordered table-fixed">
              <tr>
                <th>#</th>
                <th>Regu</th>
                <th>Jumlah Tiang</th>
                <th>ODP LOSS</th>
                <th>REBOUNDARY</th>
                <th>INSERT TIANG</th>
                <th>ODP SEHAT</th>
                <th>Jenis</th>
                <th>Status</th>
              </tr>
            @foreach($data as $no => $list)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $list->regu }}</td>
                    <td>{{ $list->jumlah_tiang }}</td>
                    <td>{{ $list->odp_loss }}</td>
                    <td>{{ $list->rebo }}</td>
                    <td>{{ $list->ins_tiang }}</td>
                    <td>{{ $list->odp_sehat }}</td>
                    <td>{{ $list->Jenis }}</td>
                    <td>{{ $list->status ? ucwords($list->status) : 'No Update' }}</td>
                </tr>
            @endforeach
          </table>
      </div>
    </div>