  <style>
  .btn strong.glyphicon {         
    opacity: 0;       
  }
  .btn.active strong.glyphicon {        
    opacity: 1;       
  }
  </style>
  <div class="panel panel-default" id="info" style="margin-top: 5px;">
   <div id="fixed-table-container-demo" class="fixed-table-container table-responsive">
      <table class="table table-bordered table-fixed">
          <thead>
          <tr>
              <th class="head">No</th>
              <th class="head">No. Layanan</th>
              <th class="head">ODP Lama</th>
              <th class="head">ODP Baru</th>
              <th class="head">QRCODE DC</th>
              <th class="head">Tanggal Open</th>
              <th class="head">Tanggal Close</th>
              <th class="head">Mitra</th>
              <th class="head">Regu</th>
              <th class="head">Label Regu</th>
              <th class="head">Label Sektor</th>
          </tr>
          </thead>
          <tbody>
          @foreach($list as $no => $d)
              <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $d->no_layanan }}</td>
                  <td>{{ $d->odp_lama }}</td>
                  <td>{{ $d->odp_baru }}</td>
                  <td>{{ $d->qrcode_dc }}</td>
                  <td>{{ $d->created_at }}</td>
                  <td>{{ $d->nama_mitra }}</td>
                  <td>{{ $d->dispatch_regu_name }}</td>
                  <td>{{ $d->label_regu }}</td>
                  <td>{{ $d->label_sektor }}</td>
              </tr>
          @endforeach
          </tbody>
      </table>
  </div>
</div>