@extends('layout2')
@section('head')
    <style type="text/css">
        th{
            text-align: center;
            vertical-align: middle;
        }
    </style>
    <link rel="stylesheet" href="/bower_components/select2/select2.css" />
    <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
@endsection
@section('content')
    <div class="input-group">
        <span class="input-group-btn">
            <button type="button" class="btn btn-info dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pilih <span class="caret"></span></button>
            <ul class="dropdown-menu">
              <li><a href="#" id="daily">Daily</a></li>
              <li><a href="#" id="monthly">Monthly</a></li>
             
            </ul>
        </span>
        <span class="ins">
            <input type="text" name="tgl" id="tgl" value="{{ date('Y-m-d') }}" class="form-control input-sm">
        </span>
        <span class="input-group-btn">
            <button class="btn btn-default btn-sm go" type="button">Go!</button>
        </span>
    </div>            
    <br/>
    <div class="panel panel-default">
        <div class="panel-heading">MATRIX TEAM DELTA C(TANAM TIANG PSB) </div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped" id="table">
                <tr>
                    <th>KANDATEL</th>
                    <th>JUMLAH TIANG DITANAM</th>
                </tr>
                @foreach($data as $d)
                <tr>
                    <td><a href="#" class="btnnewrow" data-datel="{{ $d->datel }}" data-tgl="{{ Request::segment(2) }}">{{ $d->datel }}</a><a href="#" class="btnrmrow">{{ $d->datel }}</a></td>
                    <td>{{ $d->tiang }}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
    
@section('script')
    <script src="/bower_components/select2/select2.min.js"></script>
    <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        $(function() {
            var day = {
                format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0,autoclose:true
            };
            var month = {
                format: "yyyy-mm", viewMode: 2, minViewMode: 1,autoclose:true
            };
            $("#tgl").datepicker(day);
            $("#daily").click(function(){
                $("#tgl").remove();
                $(".ins").after("<input type=text class='form-control input-sm' id=tgl>");
                $("#tgl").datepicker(day);
            });

            $("#monthly").click(function(){
                $("#tgl").remove();
                $(".ins").after("<input type=text class='form-control input-sm' id=tgl>");
                $("#tgl").datepicker(month);
            });

            $(".go").click(function(){
                window.location = '/dptiang/'+$('#tgl').val();
            });
            $('.btnrmrow').hide();
            $(".btnnewrow").click(function(e){
                var $this = $(this);
                var tgl = this.getAttribute("data-tgl");
                var dtl = this.getAttribute("data-datel");
                $.ajax({
                    type: "GET",
                    url: "/ajaxTiang/"+tgl+"/"+dtl,
                    dataType: "html"
                }).done(function(data) {
                    $('.btnrmrow').show();
                    $('.btnnewrow').hide();
                    $this.closest('tr').after(data);
                });
            });
            $(".btnrmrow").click(function(e){
                $('.ajax').remove();
                $('.btnrmrow').hide();
                $('.btnnewrow').show();
            });

        });
    </script>
@endsection