@extends('layout2')
@section('head')
  <style type="text/css">
    th{
        text-align: center;
        vertical-align: middle;
    }
    .total{
        color:#425ff4;
    }

  </style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>REPORT ABSENSI</span>
</h1>
@endsection
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">REPORT ABSEN {{ Request::segment(2) }}</div>
    <div class="panel-body table-responsive">
        <table class="table table-bordered table-striped" id="table">
            <tr>
                <th>#</th>
                <th>Witel</th>
                <th>Hadir</th>
            </tr>
            <?php
                $total=0;
            ?>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->grup }}</td>
                    <td><a href="#" class="btnnewrow" data-idgrup="{{ $d->chat_id }}" data-tgl="{{ Request::segment(2) }}">{{ $d->nik1+$d->nik2}}</a></td>
                </tr>
                <?php
                    $total+=$d->nik1+$d->nik2;
                ?>
            @endforeach
            <tr>
                <th colspan="2">Total</th>
                <th><a href="#" class="btnnewrow" data-idgrup="0" data-tgl="{{ Request::segment(2) }}">{{ $total }}</a></th>
            </tr>
        </table>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        var shtml="";
        $(function() {
            $(".btnnewrow").click(function(e){
                var $this = $(this);
                var id = this.getAttribute("data-idgrup");
                var tgl = this.getAttribute("data-tgl");
                $.ajax({
                    type: "GET",
                    url: "/ajaxReportAbsen/"+id+"/"+tgl,
                    dataType: "html"
                }).done(function(data) {
                    $this.closest('tr').after(data);
                });
            });
        });
    </script>
@endsection