@extends('layout2')
@section('head')
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Produktifitas By Jenis Order</span>
</h1>
@endsection
@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            @foreach($data as $i)
            <div class="col-md-3">
                <div class="box bg-warning darken">
                    <div class="box-cell p-x-3 p-y-1">
                        <a href="/matrix/{{ date('Y-m-d') }}/{{ $i->id }}/all/all">
                            <div class="font-weight-semibold font-size-17">{{ $i->nama_order }}</div>
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(function() {

    });
</script>
@endsection