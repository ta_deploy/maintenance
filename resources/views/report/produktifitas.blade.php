@extends('layout2')
@section('head')
  <style type="text/css">
    th{
        text-align: center;
        vertical-align: middle;
    }
    .total{
        color:#425ff4;
    }

  </style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>REPORT PRODUKTIFITAS</span>
</h1>
@endsection
@section('content')
    <div class="panel panel-default" id="info">
        <div class="panel-heading"><b>Rekap Closing Tiket Bulan {{ Request::segment('2') }}</b> <span class="badge clock"></span></div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped" id="table">
                <tr>
                    <th>REGU</th>
                    <th>BENJAR</th>
                    <th>GAMAS</th>
                    <th>ODP</th>
                    <th>REMO</th>
                    <th>UTILITAS</th>
                    <th>TIANG</th>
                    <th>TOT</th>
                    <th>Material</th>
                    <th>Jasa</th>
                    <th>TOT</th>
                </tr>
                <?php
                  $benjar = 0;$gamas = 0;$odp_loss = 0;$remo = 0;$utilitas =0;$insert_tiang = 0;$close = 0;
                  $mtr = 0;$js = 0;$mtrjs = 0;

                ?>
                @foreach($data as $d)
                    <?php
                      if($d->mitra=='TELKOM AKSES'){
                        $jasa=(!empty($d->jasa_telkom)) ? $d->jasa_telkom : 0;
                        $material=(!empty($d->material_telkom)) ? $d->material_telkom : 0;
                      }else{
                        $material=(!empty($d->material_ta)) ? $d->material_ta : 0;
                        $jasa=(!empty($d->jasa_ta)) ? $d->jasa_ta : 0;
                      }
                      $benjar += $d->benjar;$gamas += $d->gamas;$odp_loss += $d->odp_loss;$remo += $d->remo;$utilitas +=$d->utilitas;$insert_tiang += $d->insert_tiang;$close += $d->close;
                      $mtr += $material;$js += $jasa;$mtrjs += $material+$jasa;
                      $nama1=(!empty($d->nama1)) ? "<span class='label label-primary'>$d->nama1</span>" : '';
                      $nama2=(!empty($d->nama2)) ? "<span class='label label-primary'>$d->nama2</span>" : '';
                      $join = [$nama1, $nama2 ];
                      $nama = implode(' & ', array_filter($join));
                    ?>
                    <tr>
                        <td>{{ $d->nama_mitra }} {!! $nama !!}</td>
                        <td><a href='/detilprod/{{Request::segment(2)}}/1/{{$d->id_regu}}' target='_BLANK' class='detil' data-jenis='1' data-regu='{{ $d->id_regu }}'>{{ $d->benjar ?: '' }}</a></td>
                        <td><a href='/detilprod/{{Request::segment(2)}}/2/{{$d->id_regu}}' target='_BLANK' class='detil' data-jenis='2' data-regu='{{ $d->id_regu }}'>{{ $d->gamas ?: '' }}</a></td>
                        <td><a href='/detilprod/{{Request::segment(2)}}/3/{{$d->id_regu}}' target='_BLANK' class='detil' data-jenis='3' data-regu='{{ $d->id_regu }}'>{{ $d->odp_loss ?: '' }}</a></td>
                        <td><a href='/detilprod/{{Request::segment(2)}}/4/{{$d->id_regu}}' target='_BLANK' class='detil' data-jenis='4' data-regu='{{ $d->id_regu }}'>{{ $d->remo ?: '' }}</a></td>
                        <td><a href='/detilprod/{{Request::segment(2)}}/5/{{$d->id_regu}}' target='_BLANK' class='detil' data-jenis='5' data-regu='{{ $d->id_regu }}'>{{ $d->utilitas ?: '' }}</a></td>
                        <td><a href='/detilprod/{{Request::segment(2)}}/6/{{$d->id_regu}}' target='_BLANK' class='detil' data-jenis='6' data-regu='{{ $d->id_regu }}'>{{ $d->insert_tiang ?: '' }}</a></td>
                        <td><a href='/detilprod/{{Request::segment(2)}}/0/{{$d->id_regu}}' target='_BLANK' class='detil' data-jenis='0' data-regu='{{ $d->id_regu }}'>{{ $d->close ?: '' }}</a></td>
                        <td>{{ number_format($material) ?: '-' }}</td>
                        <td>{{ number_format($jasa) ?: '-' }}</td>
                        <td>{{ number_format($material+$jasa) ?: '-' }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td>TTL</td>
                    <td>{{$benjar}}</td>
                    <td>{{$gamas}}</td>
                    <td>{{$odp_loss}}</td>
                    <td>{{$remo}}</td>
                    <td>{{$utilitas}}</a></td>
                    <td>{{$insert_tiang}}</td>
                    <td>{{$close}}</td>
                    <td>{{ number_format($mtr) ?: '-' }}</td>
                    <td>{{ number_format($js) ?: '-' }}</td>
                    <td>{{ number_format($mtrjs) ?: '-' }}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            setInterval(function() {

              var currentTime = new Date();
              var hours = currentTime.getHours();
              var minutes = currentTime.getMinutes();
              var seconds = currentTime.getSeconds();

              // Add leading zeros
              hours = (hours < 10 ? "0" : "") + hours;
              minutes = (minutes < 10 ? "0" : "") + minutes;
              seconds = (seconds < 10 ? "0" : "") + seconds;

              // Compose the string for display
              var currentTimeString = hours + ":" + minutes + ":" + seconds;

              $(".clock").html(currentTimeString);

            }, 1000);

        });
    </script>
@endsection