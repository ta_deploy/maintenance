<style type="text/css">
    table, th, td {
       border: 1px solid black;
    }
    .currency {
        mso-number-format:"\#\,\#\#0\.000";
    }
</style>
<div class="panel panel-default" id="info">
    <div class="panel-heading">Evidence</div>
     <div id="fixed-table-container-demo" class="fixed-table-container">
        <table class="table table-bordered table-fixed">
            @php
                $nomor = 0
            @endphp
                @foreach ($c_mt['data'] as $k => $v )
                <tr>
                    @foreach ($v as $vv)
                        <td valign="middle">{{ ++$nomor }}</td>
                    @endforeach
                </tr>
                <tr>
                    @foreach ($v as $vv)
                        <td valign="middle">{{ $vv['no_tiket'] }}</td>
                    @endforeach
                </tr>
                <tr>
                    @foreach ($v as $vv)
                        <td valign="middle">{{ $vv['sto'] }}</td>
                    @endforeach
                </tr>
                <tr>
                    @foreach ($v as $vv)
                        @php
                            if(@$jenis == 'rincian revenue' && @$vv['jenis_order'] == 17)
                            {
                                $template_photo[$vv['id'] ][] = $vv['id']."/Redaman-IN-OUT-Sesudah";
                            }
                            else
                            {
                                if ($vv['jenis_order'] == 5)
                                {
                                    $template_photo[$vv['id'] ]["Pemasangan-Aksesoris-Tiang-Sesudah(dekat)"] = $vv['id']."/Pemasangan-Aksesoris-Tiang-Sesudah(dekat)";
                                }
                                else
                                {
                                    $template_photo[$vv['id'] ]["Foto-GRID"] = $vv['id']."/Foto-GRID";
                                }
                            }
                        @endphp
                        @foreach ($template_photo as $k => $v)
                            @if ($k == $vv['id'])
                                @foreach ($v as $kk => $photo)
                                    @php
                                        $path = "/upload/maintenance/".$photo;
                                        $path2 = "/upload2/maintenance/".$photo;
                                        $path3 = "/upload3/maintenance/".$photo;
                                        $th   = "$path-th.jpg";
                                        $th2   = "$path2-th.jpg";
                                        $th3   = "$path3-th.jpg";
                                        $img  = "$path.jpg";
                                        $img2  = "$path2.jpg";
                                        $img3  = "$path3.jpg";
                                    @endphp
                                    <td valign="middle">
                                        {{-- <p style="vertical-align: bottom">{{ str_replace('-', ' ', $k) }}</p> --}}
                                        @if (file_exists(public_path().$th))
                                        <a>
                                            <img src="{{ public_path().$th }}" />
                                        </a>
                                        @elseif (file_exists(public_path().$th2))
                                        <a>
                                            <img src="{{ public_path().$th2 }}" />
                                        </a>
                                        @elseif (file_exists(public_path().$th3))
                                        <a>
                                            <img src="{{ public_path().$th3 }}" />
                                        </a>
                                        @else
                                            <img src="{{public_path()}}/image/placeholder.gif" alt="" />
                                        @endif
                                    </td>
                                @endforeach
                            @endif
                        @endforeach
                    @endforeach
                </tr>
                @endforeach
        </table>
    </div>
</div>