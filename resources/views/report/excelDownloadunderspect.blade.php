    <div class="panel panel-default" id="info">
      <div class="panel-heading">Data Underspect</div>
      <div div class="panel-body table-responsive">
        <table>
          <thead>
            <tr>
              <th rowspan='2'>#</th>
              <th rowspan='2'>Nomor Tiket</th>
              <th rowspan='2'>Tanggal</th>
              <th rowspan='2'>Status</th>
              <th rowspan='2'>Detail Status</th>
              <th rowspan='2'>STO</th>
              <th rowspan='2'>TIM</th>
              <th rowspan='2'>Nama Tim</th>
              <th rowspan='2'>Label Regu</th>
              <th rowspan='2'>Label Sektor</th>
              <th colspan="4">Data Pelanggan</th>
              <th rowspan='2' >Nama ODP</th>
              <th rowspan='2' >Nama ODC</th>
              <th rowspan='2' >Hasil Ukur Awal (I-Booster)</th>
              <th rowspan='2' >Hasil Ukur Akhir (I-Booster)</th>
              <th rowspan='2' >Penyebab Remo</th>
              <th rowspan='2' >Tindak Lanjur</th>
              <th colspan="{{ count($list_material) }}" >Material</th>
            </tr>
            <tr>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th>DN</th>
              <th>No Internet</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              @foreach ($list_material as $item)
                <th>{{ $item }}</th>
              @endforeach
            </tr>
          </thead>
          @foreach($redata as $no => $list) 
            <tr>
              <td>{{ ++$no }}</td>
              <td>{{ $list->no_tiket }}</td>
              <td>{{ $list->tgl_selesai }}</td>
              <td>{{ strtoupper($list->status) }}</td>
              <td>{{ strtoupper($list->status_detail) }}</td>
              <td>{{ $list->sto }}</td>
              <td>{{ $list->dispatch_regu_name }}</td>
              <td>{{ $list->label_regu }}</td>
              <td>{{ $list->label_sektor }}</td>
              <td>{{ $list->nama_tim }}</td>
              <td>{{ $list->ND }}</td>
              <td>{{ $list->nomor_internet }}</td>
              <td>{{ $list->nama }}</td>
              <td>{{ $list->alamat }}</td>
              <td>{{ $list->nama_odp }}</td>
              @php
                $exp = explode('-', $list->nama_odp);
              @endphp
              <td>{{ $exp[1] ?? '' }}</td>
              <td>{{ $list->redaman_awal }}</td>
              <td>{{ $list->redaman_akhir }}</td>
              <td>{{ ucwords($list->penyebab_remo) }}</td>
              <td>{{ ucwords($list->tindak_lanjut) }}</td>
              @foreach ($list->material as $item)
                <td>{{ $item }}</td>
              @endforeach
            </tr>
          @endforeach
        </table>
      </div>
    </div>