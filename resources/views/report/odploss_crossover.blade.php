@extends('layout2')
@section('head')
  <style type="text/css">
    th, td{
        text-align: center;
        vertical-align: middle;
    }
    .total{
        font-weight: bold;
        text-decoration:underline;
        color:#0ce3ac;
    }

  </style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>MATRIX TOMMAN PSB X MARINA</span>
</h1>
@endsection
@section('content')
<div class="panel panel-default" id="info">
    <input name="tglnav" id="tglnav" type="hidden" value="{{Request::segment(2)}}" />
    <div class="panel-heading">
        <div class="row">
            <span class="pull-left m-t-2">Odp Loss Marina x PSB {{Request::segment(2)}}</span>
            <div class="pull-right">
                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        Tahun
                        <input type="text" id="tahun" class="form-control input-sm"
                            value="{{ substr(Request::segment(2),0,4) }}" placeholder="Tahun">
                    </div>
                    <div class="form-group">
                        Bulan
                        <input type="text" id="bulan" class="form-control input-sm"
                            value="{{ substr(Request::segment(2),5,2) }}" placeholder="Bulan">
                    </div>
                    <button type="button" id="goto" class="btn btn-success btn-sm">Go!</button>
                </form>
            </div><!-- /.col-lg-6 -->
        </div>
    </div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped" id="table">
               <thead>
                   <tr>
                       <th rowspan="2" style="vertical-align: middle">Status/Div</th>
                       <th colspan="5">ODP LOSS</th>
                       <th colspan="5">INSERT TIANG</th>
                       <th colspan="6">REBOUNDARY</th>
                   </tr>
                   <tr>
                        <th style="background-color: yellow;">Undispatch</th>
                        <th style="background-color: yellow;">No Update</th>
                        <th style="background-color: yellow;">Kendala Pelanggan</th>
                        <th style="background-color: yellow;">Kendala Teknis</th>
                        <th style="background-color: yellow;">Close</th>
                        <th style="background-color: chocolate;">Undispatch</th>
                        <th style="background-color: chocolate;">No Update</th>
                        <th style="background-color: chocolate;">Kendala Pelanggan</th>
                        <th style="background-color: chocolate;">Kendala Teknis</th>
                        <th style="background-color: chocolate;">Close</th>
                        <th style="background-color: lime;">Undispatch</th>
                        <th style="background-color: lime;">No Update</th>
                        <th style="background-color: lime;">Kendala Pelanggan</th>
                        <th style="background-color: lime;">Kendala Teknis</th>
                        <th style="background-color: lime;">Close</th>
                        <th style="background-color: lime;">Port Kosong</th>
                        <th style="background-color: lime;">PT-1 Not Valins</th>
                </tr>
               </thead>
               <tbody>
                   @php
                        $g_odp_loss_kp = 0;
                        $g_odp_loss_kt = 0;
                        $g_odp_loss_cl = 0;
                        $g_odp_loss_NU = 0;
                        $g_odp_loss_psb = 0;
                        $g_inst_kp = 0;
                        $g_inst_kt = 0;
                        $g_inst_cl = 0;
                        $g_inst_NU = 0;
                        $g_inst_psb = 0;
                        $g_rebo_psb = 0;
                        $g_rebo_kp = 0;
                        $g_rebo_kt = 0;
                        $g_rebo_val = 0;
                        $g_rebo_notval = 0;
                        $g_rebo_cl = 0;
                        $g_rebo_NU = 0;
                   @endphp
                   @foreach ($data as $d)
                   @php
                    $g_odp_loss_kp += $d->odp_loss_kp;
                    $g_odp_loss_kt += $d->odp_loss_kt;
                    $g_odp_loss_cl += $d->odp_loss_cl;
                    $g_odp_loss_NU += $d->odp_loss_NU;
                    $g_odp_loss_psb = $psb_odp_loss->odp_loss_ioan + $psb_odp_loss->odp_loss_psb;
                    $g_inst_kp += $d->inst_kp;
                    $g_inst_kt += $d->inst_kt;
                    $g_inst_cl += $d->inst_cl;
                    $g_inst_NU += $d->inst_NU;
                    $g_inst_psb = $insertpsb->insert_ioan + $insertpsb->insert_psb;
                    $g_rebo_kp += $d->rebo_kp;
                    $g_rebo_kt += $d->rebo_kt;
                    $g_rebo_cl += $d->rebo_cl;
                    $g_rebo_val += $d->rebo_cl_val;
                    $g_rebo_notval += $d->rebo_cl_notval;
                    $g_rebo_NU += $d->rebo_NU;
                    $g_rebo_psb = $odpfullpsb->rebo_ioan + $odpfullpsb->rebo_psb;
                    @endphp
                       <tr>
                            <td>
                                {{ $d->jenis_order_frm }}
                            </td>
                            <td>
                                @if ($d->order_from == 'ASSURANCE')
                                    @if ($psb_odp_loss->odp_loss_ioan > 0)
                                    <a href="/listtommanpsbCr/2/ioan">{{ $psb_odp_loss->odp_loss_ioan }}</a>
                                    @else
                                    -
                                    @endif
                                @elseif ($d->order_from == 'PSB')
                                    @if ($psb_odp_loss->odp_loss_psb > 0)
                                    <a href="/listtommanpsbCr/2/psb">{{ $psb_odp_loss->odp_loss_psb }}</a>
                                    @else
                                    -
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if ($d->odp_loss_NU > 0)
                                <a href="/odpLossC/3/null/null/{{ $d->order_from ?: 'null' }}">{{ $d->odp_loss_NU }}</a>
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if ($d->odp_loss_kp > 0)
                                <a href="/odpLossC/3/kendala pelanggan/{{ Request::segment(2) }}/{{ $d->order_from ?: 'null' }}">{{ $d->odp_loss_kp }}</a>
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if ($d->odp_loss_kt > 0)
                                <a href="/odpLossC/3/kendala teknis/{{ Request::segment(2) }}/{{ $d->order_from ?: 'null' }}">{{ $d->odp_loss_kt }}</a>
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if ($d->odp_loss_cl > 0)
                                <a href="/odpLossC/3/close/{{ Request::segment(2) }}/{{ $d->order_from ?: 'null' }}">{{ $d->odp_loss_cl }}</a>
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if ($d->order_from == 'ASSURANCE')
                                    @if ($insertpsb->insert_ioan > 0)
                                    <a href="/listtommanpsbCr/11/ioan">{{ $insertpsb->insert_ioan }}</a>
                                    @else
                                    -
                                    @endif
                                @elseif ($d->order_from == 'PSB')
                                    @if ($insertpsb->insert_psb > 0)
                                    <a href="/listtommanpsbCr/11/psb">{{ $insertpsb->insert_psb }}</a>
                                    @else
                                    -
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if ($d->inst_NU > 0)
                                <a href="/odpLossC/6/null/null/{{ $d->order_from ?: 'null' }}">{{ $d->inst_NU }}</a>
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if ($d->inst_kp > 0)
                                <a href="/odpLossC/6/kendala pelanggan/{{ Request::segment(2) }}/{{ $d->order_from ?: 'null' }}">{{ $d->inst_kp }}</a>
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if ($d->inst_kt > 0)
                                <a href="/odpLossC/6/kendala teknis/{{ Request::segment(2) }}/{{ $d->order_from ?: 'null' }}">{{ $d->inst_kt }}</a>
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if ($d->inst_cl > 0)
                                <a href="/odpLossC/6/close/{{ Request::segment(2) }}/{{ $d->order_from ?: 'null' }}">{{ $d->inst_cl }}</a>
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if ($d->order_from == 'ASSURANCE')
                                    @if ($odpfullpsb->rebo_ioan > 0)
                                    <a href="/listtommanpsbCr/24/ioan">{{ $odpfullpsb->rebo_ioan }}</a>
                                    @else
                                    -
                                    @endif
                                @elseif ($d->order_from == 'PSB')
                                    @if ($odpfullpsb->rebo_psb > 0)
                                    <a href="/listtommanpsbCr/24/psb">{{ $odpfullpsb->rebo_psb }}</a>
                                    @else
                                    -
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if ($d->rebo_NU > 0)
                                <a href="/odpLossC/9/null/null/{{ $d->order_from ?: 'null' }}">{{ $d->rebo_NU }}</a>
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if ($d->rebo_kp > 0)
                                <a
                                    href="/odpLossC/9/kendala pelanggan/{{ Request::segment(2) }}/{{ $d->order_from ?: 'null' }}">{{ $d->rebo_kp }}</a>
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if ($d->rebo_kt > 0)
                                <a href="/odpLossC/9/kendala teknis/{{ Request::segment(2) }}/{{ $d->order_from ?: 'null' }}">{{ $d->rebo_kt }}</a>
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if ($d->rebo_cl > 0)
                                <a href="/odpLossC/9/close/{{ Request::segment(2) }}/{{ $d->order_from ?: 'null' }}">{{ $d->rebo_cl }}</a>
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if ($d->rebo_cl_val > 0)
                                <a href="/odpLossC/9/close_val/{{ Request::segment(2) }}/{{ $d->order_from ?: 'null' }}">{{ $d->rebo_cl_val }}</a>
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if ($d->rebo_cl_notval > 0)
                                <a href="/odpLossC/9/close_notval/{{ Request::segment(2) }}/{{ $d->order_from ?: 'null' }}">{{ $d->rebo_cl_notval }}</a>
                                @else
                                -
                                @endif
                            </td>
                       </tr>
                   @endforeach
               </tbody>
               <tfoot>
                   <tr>
                        <td>Total</td>
                        <td>{{ $g_odp_loss_psb }}</td>
                        <td>{{ $g_odp_loss_NU }}</td>
                        <td>{{ $g_odp_loss_kp }}</td>
                        <td>{{ $g_odp_loss_kt }}</td>
                        <td>{{ $g_odp_loss_cl }}</td>
                        <td>{{ $g_inst_psb }}</td>
                        <td>{{ $g_inst_NU }}</td>
                        <td>{{ $g_inst_kp }}</td>
                        <td>{{ $g_inst_kt }}</td>
                        <td>{{ $g_inst_cl }}</td>
                        <td>{{ $g_rebo_psb }}</td>
                        <td>{{ $g_rebo_NU }}</td>
                        <td>{{ $g_rebo_kp }}</td>
                        <td>{{ $g_rebo_kt }}</td>
                        <td>{{ $g_rebo_cl }}</td>
                        <td>{{ $g_rebo_val }}</td>
                        <td>{{ $g_rebo_notval }} </td>
                   </tr>
               </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            $('#tahun').datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years",
                autoclose: true
            });

            $('#bulan').datepicker({
                format: "mm",
                viewMode: "months",
                minViewMode: "months",
                autoclose: true
            });

            $("#goto").click(function(e){
                var segments = $(location).attr('href').split('/');
                var goto=segments[3]+'/'+$('#tahun').val()+'-'+$('#bulan').val();
                window.location = '/'+goto;
            });
        });
    </script>
@endsection