    <div class="panel panel-default" id="info">
        <div class="panel-heading">Data Maintenance</div>
         <div id="fixed-table-container-demo" class="fixed-table-container">
            <table class="table table-bordered table-fixed">
              <tr>
                <th>#</th>
                <th>Id</th>
                <th>Nomor Tiket</th>
                <th>Nomor Inet</th>
                <th>Headline</th>
                <th>PIC</th>
                <th>Koordinat</th>
                <th>Info</th>
                <th>Nama</th>
                <th>PIC Pelanggan</th>
                <th>Jenis Order</th>
                <th>Tanggal Dibuat</th>
                <th>Dibuat Oleh</th>
                <th>Tanggal Diubah</th>
                <th>Diubah Oleh</th>
                {{-- <th>program_id</th>
                <th>program_name</th>
                <th>no_tiket_temp</th> --}}
                <th>Nama Mitra</th>
                <th>Nama Regu</th>
                <th>Label Regu</th>
                <th>Label Sektor</th>
                <th>Status</th>
                {{-- <th>status_name</th> --}}
                <th>Detail Status</th>
                <th>Hasil User</th>
                <th>Hasil QC</th>
                <th>Hasil Rekon</th>
                <th>Tanggal Selesai</th>
                <th>Nama TL</th>
                <th>Nik 1</th>
                <th>Nik 2</th>
                {{-- <th>username3</th>
                <th>username4</th> --}}
                {{-- <th>tiket_by</th>
                <th>psb_laporan_id</th> --}}
                <th>Action</th>
                <th>Sumber Order</th>
                <th>Kandatel</th>
                <th>STO</th>
                <th>ODP</th>
                <th>ODC</th>
                <th>Refer</th>
                <th>Action</th>
                <th>verify</th>
                <th>Jumlah Material</th>
                <th>Splitter</th>
                <th>Alamat</th>
                <th>Telp</th>
                {{-- <th>verifHd</th> --}}
                <th>Port Used</th>
                <th>Port Idle</th>
                <th>Redaman Awal</th>
                <th>Redaman Akhir</th>
              </tr>
              @foreach($data as $no => $list)
              <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $list->id }}</td>
                    <td>{{ $list->no_tiket }}</td>
                    <td>{{ $list->no_inet }}</td>
                    <td>{{ str_replace('=', '', $list->headline) }}</td>
                    <td>{{ str_replace('=', '', $list->pic) }}</td>
                    <td>{{ str_replace('=', '', $list->koordinat) }}</td>
                    <td>{{ $list->info }}</td>
                    <td>{{ $list->customerName?? $list->nama_customer_kpro }}</td>
                    <td>{{ $list->myirpic }}</td>
                    <td>{{ $list->nama_order }}</td>
                    <td>{{ $list->created_by }}</td>
                    <td>{{ $list->created_at }}</td>
                    <td>{{ $list->modified_at }}</td>
                    <td>{{ $list->modified_by }}</td>
                    {{-- <td>{{ $list->program_id }}</td>
                    <td>{{ $list->program_name }}</td>
                    <td>{{ $list->no_tiket_temp }}</td> --}}
                    <td>{{ $list->nama_mitra }}</td>
                    <td>{{ $list->ur_regu }}</td>
                    <td>{{ $list->label_regu }}</td>
                    <td>{{ $list->label_sektor }}</td>
                    <td>{{ strtoupper($list->status) }}</td>
                    {{-- <td>{{ strtoupper($list->status_name) }}</td> --}}
                    <td>{{ strtoupper($list->status_detail) }}</td>
                    <td>{{ $list->hasil_by_user }}</td>
                    <td>{{ $list->hasil_by_qc }}</td>
                    <td>{{ $list->hasil_by_rekon }}</td>
                    <td>{{ $list->tgl_selesai }}</td>
                    <td>{{ $list->nama_tl }} ({{ $list->TL }})</td>
                    <td>{{ $list->nama1 }} {{ $list->username1 != '' ? '('.$list->username1.')' : '' }}</td>
                    <td>{{ $list->nama2 }} {{ $list->username2 != '' ? '('.$list->username2.')' : '' }}</td>
                    {{-- <td>{{ $list->nama3 }} {{ $list->username3 != '' ? '('.$list->username3.')' : '' }}</td>
                    <td>{{ $list->nama4 }} {{ $list->username4 != '' ? '('.$list->username4.')' : '' }}</td> --}}
                    {{-- <td>{{ $list->tiket_by }}</td>
                    <td>{{ $list->psb_laporan_id }}</td> --}}
                    <td>{{ str_replace('=', '', $list->action) }}</td>
                    <td>{{ $list->order_from }}</td>
                    <td>{{ $list->kandatel }}</td>
                    <td>{{ $list->sto }}</td>
                    <td>{{ $list->nama_odp }}</td>
                    @php
                      $exp = explode('-', $list->nama_odp);
                    @endphp
                    <td>{{ $exp[1] ?? '' }}</td>
                    <td>{{ $list->refer }}</td>
                    <td>{{ $list->action_cause }}</td>
                    @php
                      switch ($list->verify) {
                        case '1':
                          $verify = 'Verifikasi TA';
                        break;
                        case '2':
                          $verify = 'Verifikasi Telkom';
                        break;
                        case '3':
                          $verify = 'Siap Rekon';
                        break;
                        case '4':
                          $verify = 'Ditolak TA';
                        break;
                        case '5':
                          $verify = 'Ditolak Telkom';
                        break;
                        default:
                          $verify = 'Disubmit Teknisi';
                        break;
                      }
                    @endphp
                    <td>{{ $verify }}</td>
                    <td>{{ $list->mtrcount }}</td>
                    <td>{{ $list->splitter }}</td>
                    <td>{{ $list->alamat ?? $list->alamat_kpro }}</td>
                    {{-- <td>{{ $list->verifHd }}</td> --}}
                    <td>{{ $list->port_used }}</td>
                    <td>{{ $list->port_idle }}</td>
                    <td>{{ $list->redaman_awal }}</td>
                    <td>{{ $list->redaman_akhir }}</td>
                </tr>
            @endforeach
          </table>
      </div>
    </div>