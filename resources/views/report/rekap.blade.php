@extends('layout2')
@section('head')
  <style type="text/css">
    th{
        text-align: center;
        vertical-align: middle;
    }
  </style>
@endsection
@section('content')
    @foreach($regu as $no => $r)
    <div class="panel panel-default">
        <div class="panel-heading">MATRIX PRODUKTIFITAS {{ $r['grup'] }}</div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped" id="table">
                <tr>
                    <th rowspan="2">#</th>
                    <th rowspan="2">Nama<br/>Tim</th>
                    <th colspan="4">WO {{ Request::segment(2) }}</th>
                    <th rowspan="2">Prod</th>
                    <th rowspan="2">List</th>
                </tr>
                <tr>
                    <th>Sisa</th>
                    <th>Kendala<br/>Teknis</th>
                    <th>Kendala<br/>Pelanggan</th>
                    <th>Close</th>
                </tr>
                @foreach($r['data'] as $noo => $d)

                    <tr>
                        <td>{{ ++$noo }}</td>
                        <td>{{ $d->nama_regu }}:<span class="label label-success">{{ $d->spesialis }}</span><br/><span class="label label-primary">{{$d->nama1}}&{{$d->nama2}}</span></td>
                        <!--<td>{{ $d->jumlah }}</td>-->
                        <td>{{ $d->no_update }}</td>
                        <td>{{ $d->kendala_teknis }}</td>
                        <td>{{ $d->kendala_pelanggan }}</td>
                        <td>{{ $d->close }}</td>
                        <td></td>
                        <td>
                            <?php
                                @$id = $list[$d->id];
                            ?>
                            @if(count($id))
                            @foreach($id as $nolist => $l)
                    
                                <span class="label label-{{ $l['status_laporan'] == 'close' ? 'success' : 'warning' }}">{{ $l['tiket'] }}</span>
                            @endforeach
                            @endif
                        </td>
                    </tr>
                @endforeach

            </table>
        </div>
    </div>
    @endforeach
@endsection