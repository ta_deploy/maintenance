<style type="text/css">
    table, th, td {
       border: 1px solid black;
    }
    .currency {
        mso-number-format:"\#\,\#\#0\.000";
    }
</style>
<div class="panel panel-default" id="info">
    <div class="panel-heading">Evidence</div>
     <div id="fixed-table-container-demo" class="fixed-table-container">
        <table class="table table-bordered table-fixed">
            @php
                $nomor_in = 0;
            @endphp
            @foreach($mt as $no => $list)
                @if($no%6==0)
                    <tr>
                @endif

                @php
                    if(@$jenis == 'rincian revenue' && @$list->jenis_order == 17)
                    {
                        $template_photo[$list->id][] = $list->id."/Redaman-IN-OUT-Sesudah";
                    }
                    else
                    {
                        if ($list->jenis_order == 5)
                        {
                            $template_photo[$list->id]["Pemasangan-Aksesoris-Tiang-Sesudah(dekat)"] = $list->id."/Pemasangan-Aksesoris-Tiang-Sesudah(dekat)";
                        }
                        else
                        {
                            $template_photo[$list->id]["Foto-GRID"] = $list->id."/Foto-GRID";
                        }
                    }
                @endphp

                <td valign="middle">{{ ++$nomor_in }}. {{$list->no_tiket}}</td>
                @foreach ($template_photo as $k => $v)
                    @if ($k == $list->id)
                        @foreach ($v as $kk => $photo)
                            @php
                                $path = "/upload/maintenance/".$photo;
                                $path2 = "/upload2/maintenance/".$photo;
                                $path3 = "/upload3/maintenance/".$photo;
                                $th   = "$path-th.jpg";
                                $th2   = "$path2-th.jpg";
                                $th3   = "$path3-th.jpg";
                                $img  = "$path.jpg";
                                $img2  = "$path2.jpg";
                                $img3  = "$path3.jpg";
                            @endphp
                            <td valign="middle">
                                <p style="vertical-align: bottom">{{ str_replace('-', ' ', $k) }}</p>
                                @if (file_exists(public_path().$img))
                                <a>
                                    <img src="{{ public_path().$img }}" />
                                </a>
                                @elseif (file_exists(public_path().$img2))
                                <a>
                                    <img src="{{ public_path().$img2 }}" />
                                </a>
                                @elseif (file_exists(public_path().$img3))
                                <a>
                                    <img src="{{ public_path().$img3 }}" />
                                </a>
                                @else
                                    <img src="{{public_path()}}/image/placeholder.gif" alt="" />
                                @endif
                            </td>
                            @if($no%6==5)
                                </tr>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endforeach
        </table>
    </div>
</div>