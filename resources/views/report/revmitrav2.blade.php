@extends('layout2')
@section('head')
  <style type="text/css">
    th{
        text-align: center;
    }
  </style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>RINCIAN REVENUE {{ Request::Segment(2) }}</span>
</h1>
@endsection
@section('content')
<input name="tglnav" id="tglnav" type="hidden" value="{{Request::segment(3)}}"/>
    <div class="panel panel-default" id="info" class="align-middle">
        <div class="panel-heading">
        <div class="row">
      <span class="pull-left m-t-2">REVENUE {{Request::segment(3)}}</span>
  <div class="pull-right">
    <form class="navbar-form navbar-left" role="search">
      <div class="form-group">
         Tahun
        <input type="text" id="tahun" class="form-control input-sm" value="{{ substr(Request::segment(3),0,4) }}" placeholder="Tahun">
      </div>
      <div class="form-group">
        Bulan
        <input type="text" id="bulan" class="form-control input-sm" value="{{ substr(Request::segment(3),5,2) }}" placeholder="Bulan">
      </div>
      <button type="button" id="goto" class="btn btn-success btn-sm">Go!</button>
    </form>
  </div><!-- /.col-lg-6 -->
</div></div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-fixed">
                <tr>
                    <th style="vertical-align:middle" colspan=2>PEKERJAAN</th>
                    <th style="vertical-align:middle" colspan=8>QE AKSES</th>
                    <th style="vertical-align:middle" colspan=2>QE RECOVERY</th>
                    <th style="vertical-align:middle" colspan=1>QE RELOKASI</th>
                    <th style="vertical-align:middle" colspan=3>OTHER</th>

                    <th style="vertical-align:middle" rowspan=2>TOTAL</th>
                </tr>
                <tr>
                    <th style="vertical-align:middle" rowspan=1>#</th>
                    <th style="vertical-align:middle" rowspan=1>MITRA</th>
                    {{-- <th style="vertical-align:middle" rowspan=1>TANGIBLE PROMAN</th> --}}
                    <th style="vertical-align:middle" rowspan=1>UNSPEC</th>
                    <th style="vertical-align:middle" rowspan=1>ODP LOSS</th>
                    <!-- <th style="vertical-align:middle" rowspan=1>VALIDASI ODP</th> -->
                    <th style="vertical-align:middle" rowspan=1>ODP FULL</th>
                    <!-- <th style="vertical-align:middle" rowspan=1>Rev. FTM</th> -->
                    <!-- <th style="vertical-align:middle" rowspan=1>Project Pihak-3</th> -->
                    <th style="vertical-align:middle" rowspan=1>ODP SEHAT</th>
                    <th style="vertical-align:middle" rowspan=1>ODP NORMALISASI</th>
                    <th style="vertical-align:middle" rowspan=1>ODC SEHAT</th>
                    <th style="vertical-align:middle" rowspan=1>VALINS</th>
                    <th style="vertical-align:middle" rowspan=1>GAMAS</th>
                    <th style="vertical-align:middle" rowspan=1>REPAIR FEEDER/DIST</th>
                    <th style="vertical-align:middle" rowspan=1>UTILITAS</th>
                    <th style="vertical-align:middle" rowspan=1>INSERT TIANG</th>
                    <th style="vertical-align:middle" rowspan=1>QE HEM</th>
                    <th style="vertical-align:middle" rowspan=1>WASAKA</th>
                    <th style="vertical-align:middle" rowspan=1>ODP RUSAK</th>
                    <!-- <th style="vertical-align:middle" rowspan=1>REBOUNDARY</th> -->
                    <!-- <th style="vertical-align:middle" rowspan=1>PENUTUPAN ODP</th> -->
                </tr>
                <?php
                    $benjar=$gamas=$odp_loss=$remo=$utilitas=$insert_tiang=$odp_full=$odp_sehat=$val_odp=$rev_ftm=$pihak3=$normalisasi=$reboundary=$tutupodp=$repair=$odc_sehat=$valins=$qe_hem=$wasaka=$odp_rusak=$total=0;
                ?>
                @foreach($data as $no => $d)

                    @if(session('auth')->nama_instansi == 'TELKOM AKSES' || session('auth')->nama_instansi == $d->mitra || session('auth')->maintenance_level == 1 || session('auth')->maintenance_level == 4 )
                    <?php
                        $benjar+=$d->benjar;$gamas+=$d->gamas;$odp_loss+=$d->odp_loss;$remo+=$d->remo;$utilitas+=$d->utilitas;$insert_tiang+=$d->insert_tiang;$odp_full+=$d->odp_full;$odp_sehat+=$d->odp_sehat;$valins+=$d->valins;$qe_hem+=$d->qe_hem;$wasaka+=$d->wasaka;
                        // $val_odp+=$d->val_odp;$rev_ftm+=$d->rev_ftm;$pihak3+=$d->pihak3;
                        $normalisasi+=$d->normalisasi;
                        $repair+=$d->repair;
                        $odc_sehat+=$d->odc_sehat;
                        $odp_rusak+=$d->odp_rusak;
                        // $reboundary+=$d->reboundary;$tutupodp+=$d->tutup_odp;
                        $total+=$d->total;
                    ?>
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td><a href="#" class="btnnewrow" data-mitra="{{ $d->mitra }}" data-sgmt2="{{ Request::segment(2) }}">{{ $d->mitra }}</a>
                        <a href="#" class="btnrmrow">{{ $d->mitra }}</a></td>
                        {{-- <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/1/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->benjar) }}</a></td> --}}
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/4/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->remo) }}</a></td>
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/3/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->odp_loss) }}</a></td>
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/9/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->odp_full) }}</a></td>
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/8/{{ $d->mitra }}/{{ Request::segment(2) }}"><b style="color:blue;">{!! $d->tiket_odp_sehat?'('.$d->tiket_odp_sehat.' Tiket) ':'' !!}</b>{{ number_format($d->odp_sehat) }}</a></td>
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/17/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->normalisasi) }}</a></td>
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/20/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->odc_sehat) }}</a></td>
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/21/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->valins) }}</a></td>
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/2/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->gamas) }}</a></td>
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/16/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->repair) }}</a></td>
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/5/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->utilitas) }}</a></td>
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/6/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->insert_tiang) }}</a></td>
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/22/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->qe_hem) }}</a></td>
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/23/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->wasaka) }}</a></td>
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/28/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->odp_rusak) }}</a></td>
                        <td align="right"><a href="/cetak_rev/{{ Request::segment(3) }}/ALL/{{ $d->mitra }}/{{ Request::segment(2) }}">{{ number_format($d->total) }}</a></td>
                    </tr>
                    @endif
                @endforeach
                @if(session('auth')->nama_instansi == 'TELKOM AKSES' || session('auth')->maintenance_level == 1 || session('auth')->maintenance_level == 4 )
                    <tr>
                        <td colspan="2">TOTAL</td>
                        {{-- <td align="right">{{ number_format($benjar) }}</td> --}}
                        <td align="right">{{ number_format($remo) }}</td>
                        <td align="right">{{ number_format($odp_loss) }}</td>
                        <td align="right">{{ number_format($odp_full) }}</td>
                        <td align="right">{{ number_format($odp_sehat) }}</td>
                        <td align="right">{{ number_format($normalisasi) }}</td>
                        <td align="right">{{ number_format($odc_sehat) }}</td>
                        <td align="right">{{ number_format($valins) }}</td>
                        <td align="right">{{ number_format($gamas) }}</td>
                        <td align="right">{{ number_format($repair) }}</td>
                        <td align="right">{{ number_format($utilitas) }}</td>
                        <td align="right">{{ number_format($insert_tiang) }}</td>
                        <td align="right">{{ number_format($qe_hem) }}</td>
                        <td align="right">{{ number_format($wasaka) }}</td>
                        <td align="right">{{ number_format($odp_rusak) }}</td>
                        <!-- <td align="right">{{ number_format($val_odp) }}</td> -->
                        <!-- <td align="right">{{ number_format($rev_ftm) }}</td>
                        <td align="right">{{ number_format($pihak3) }}</td> -->
                        <!-- <td align="right">{{ number_format($reboundary) }}</td>
                        <td align="right">{{ number_format($tutupodp) }}</td> -->
                        <td align="right">{{ number_format($total) }}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
    <div id="detil-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #eee">
          <strong style="color:black;">Detail Material</strong>
          <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
        </div>
        <div class="modal-body" style="overflow-y:auto;height : 450px">

        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            $('.btnrmrow').hide();
            $(".btnnewrow").click(function(e){
                var $this = $(this);
                var mt = this.getAttribute("data-mitra");
                var dd = this.getAttribute("data-sgmt2");
                var tglnav = $('#tglnav').val();
                $.ajax({
                    type: "GET",
                    url: "/ajaxrev/"+dd+"/"+mt+"/"+tglnav,
                    dataType: "html"
                }).done(function(data) {
                    $('.btnrmrow').show();
                    $('.btnnewrow').hide();
                    $this.closest('tr').after(data);
                });
            });
            $(".btnrmrow").click(function(e){
                $('.ajax').remove();
                $('.btnrmrow').hide();
                $('.btnnewrow').show();
            });

            $('#tahun').datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years",
                autoclose: true
            });
            $('#bulan').datepicker({
                format: "mm",
                viewMode: "months",
                minViewMode: "months",
                autoclose: true
            });
            $("#goto").click(function(e){
                var segments = $(location).attr('href').split('/');
                var goto=segments[3]+'/'+segments[4]+'/'+$('#tahun').val()+'-'+$('#bulan').val();
                window.location = '/'+goto;
            });
        })
    </script>
@endsection