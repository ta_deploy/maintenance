@extends('layout2')
@section('head')
<link rel="stylesheet" href="/bower_components/select2/select2.css" />
<link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
<style type="text/css">
    .datepicker thead tr:first-child th{
        background: #c64648;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>DOWNLOAD DATA UNDERSPEC</span>
</h1>
@endsection
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Form Download Underspect
    </div>
    <div class="panel-body">
        <form method="post" class="form-horizontal" id="submit_dwn">
            <div class="form-group">
                <label for="mitra" class="col-sm-3 col-md-2 control-label">Date</label>
                <div class="col-sm-5">
                    <input type="text" name="tgl" id="tgl" value="{{ date('Y-m') }}" class="form-control input-sm">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                    <a class="btn btn-primary common_dwn" type="button" data-jns="dwn">
                        <span class="glyphicon glyphicon-floppy-disk"></span>
                        <span>Download</span>
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script src="/bower_components/select2/select2.min.js"></script>
<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
<script>
    $(function() {
        var month = {
            format: "yyyy-mm",
            viewMode: 2,
            minViewMode: 1,
            autoclose:true
        };
        $("#tgl").datepicker(month);

        $('.common_dwn').on('click', function(e){
            $('#submit_dwn').submit();
        });
    });
</script>
@endsection