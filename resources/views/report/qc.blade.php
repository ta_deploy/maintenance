@extends('layout2')
@section('head')
@endsection
@section('content')
<div class="panel panel-default" id="info">
    <div class="panel-heading">List {{Request::segment(2)}}</div>
    <div id="fixed-table-container-demo" class="fixed-table-container">
        <table class="table table-bordered table-fixed">
            <thead>
            <tr>
                <th>#</th>
                <th>Tiket</th>
                <th>Regu</th>
                <th>Evident</th>
            </tr>
            </thead>
            <tbody>
                @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->no_tiket }}</td>
                    <td>{{ $d->dispatch_regu_name }}</td>
                    <td>
                        
                        @foreach($foto as $input)
                            <?php
                                $path = "/upload/maintenance/".$d->id."/$input";
                                $th   = "$path-th.jpg";
                                $img  = "$path.jpg";
                              ?>
                            @if (file_exists(public_path().$th))
                              <a href="{{ $img }}">
                            <img src="{{ $th }}" alt="{{ $input }}" />
                        </a>
                            @else
                                <img src="/image/placeholder.gif" alt="" />
                            @endif
                            
                        @endforeach
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
@endsection