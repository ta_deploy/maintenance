@extends('layout2')
@section('head')
  <style type="text/css">
    th{
        text-align: center;
        vertical-align: middle;
    }
    .total{
        font-weight: bold;
        text-decoration:underline;
        color:#0ce3ac;
    }

  </style>
@endsection

@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>REPORT UMUR {{ @$order->nama_order }}</span>
</h1>
@endsection
@section('content')
<div class="panel panel-default" id="info">
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped" id="table">
                <tr>
                    <th rowspan="2">STO/WITEL</th>
                    <th colspan="7">REPORT UMUR {{ @$order->nama_order }} {{ date('Y-m-d') }} <span class="clock"></span></th>
                </tr>
                <tr>
                    <th>>30</th>
                    <th>7-30</th>
                    <th>3-7</th>
                    <th>2-3</th>
                    <th>1-2</th>
                    <th><1</th>
                    <th>TTL</th>
                </tr>
                <?php
                    $datel = 0;
                    $a=0;$b=0;$c=0;$de=0;$e=0;$f=0;$ttl=0;
                    $a_all=0;$b_all=0;$c_all=0;$de_all=0;$e_all=0;$f_all=0;$ttlall=0;
                    $so = array("BANJARBARU", "BANJARMASIN", "BATULICIN", "TANJUNG");
                ?>
                @foreach($so as $n)
                    <?php
                        $a=0;$b=0;$c=0;$de=0;$e=0;$f=0;$ttl=0;
                    ?>
                    @foreach($data as $no => $d)
                        @if($d->datel == $n)
                            <?php
                                $a+=$d->a;$b+=$d->b;$c+=$d->c;$de+=$d->d;$e+=$d->e;$f+=$d->f;$ttl+=$d->total;
                            ?>
                            <tr>
                                <td>{{ $d->sto }}</td>
                                <td><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>30 and sto='{{ $d->sto }}' and jenis_order={{ $order->id }}">{{ $d->f ?: '-' }}</a></td>
                                <td><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>7 and DATEDIFF( NOW( ) , created_at )<=30 and sto='{{ $d->sto }}' and jenis_order={{ $order->id }}">{{ $d->e ?: '-' }}</a></td>
                                <td><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>3 and DATEDIFF( NOW( ) , created_at )<=7 and sto='{{ $d->sto }}' and jenis_order={{ $order->id }}">{{ $d->d ?: '-' }}</a></td>
                                <td><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>2 and DATEDIFF( NOW( ) , created_at )<=3 and sto='{{ $d->sto }}' and jenis_order={{ $order->id }}">{{ $d->c ?: '-' }}</a></td>
                                <td><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>=1 and DATEDIFF( NOW( ) , created_at )<=2 and sto='{{ $d->sto }}' and jenis_order={{ $order->id }}">{{ $d->b ?: '-' }}</a></td>
                                <td><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )<1 and sto='{{ $d->sto }}' and jenis_order={{ $order->id }}">{{ $d->a ?: '-' }}</a></td>
                                <td><a href="#" class="link_list" data-query="sto='{{ $d->sto }}' and jenis_order={{ $order->id }}">{{ $d->total }}</a></td>
                            </tr>
                        @endif
                    @endforeach
                    @if($ttl)
                    <tr>
                        <td class="total">{{ $n }}</td>
                        <td class="total"><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>30 and kandatel='{{ $n }}' and jenis_order={{ $order->id }}">{{ $f }}</a></td>
                        <td class="total"><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>7 and DATEDIFF( NOW( ) , created_at )<=30 and kandatel='{{ $n }}' and jenis_order={{ $order->id }}">{{ $e }}</a></td>
                        <td class="total"><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>3 and DATEDIFF( NOW( ) , created_at )<=7 and kandatel='{{ $n }}' and jenis_order={{ $order->id }}">{{ $de }}</a></td>
                        <td class="total"><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>2 and DATEDIFF( NOW( ) , created_at )<=3 and kandatel='{{ $n }}' and jenis_order={{ $order->id }}">{{ $c }}</a></td>
                        <td class="total"><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>=1 and DATEDIFF( NOW( ) , created_at )<=2 and kandatel='{{ $n }}' and jenis_order={{ $order->id }}">{{ $b }}</a></td>
                        <td class="total"><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )<1 and kandatel='{{ $n }}' and jenis_order={{ $order->id }}">{{ $a }}</a></td>
                        <td class="total"><a href="#" class="link_list" data-query="kandatel='{{ $n }}' and jenis_order={{ $order->id }}">{{ $ttl }}</a></td>
                    </tr>
                    @endif
                    <?php
                        $a_all+=$a;$b_all+=$b;$c_all+=$c;$de_all+=$de;$e_all+=$e;$f_all+=$f;$ttlall+=$ttl;
                    ?>
                @endforeach
                <tr>
                    <td>TOTAL</td>
                    <td><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>30' and jenis_order={{ $order->id }}">{{ $f_all }}</a></td>
                    <td><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>7 and DATEDIFF( NOW( ) , created_at )<=30 and jenis_order={{ $order->id }}">{{ $e_all }}</a></td>
                    <td><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>3 and DATEDIFF( NOW( ) , created_at )<=7 and jenis_order={{ $order->id }}">{{ $de_all }}</a></td>
                    <td><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>2 and DATEDIFF( NOW( ) , created_at )<=3 and jenis_order={{ $order->id }}">{{ $c_all }}</a></td>
                    <td><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )>=1 and DATEDIFF( NOW( ) , created_at )<=2 and jenis_order={{ $order->id }}">{{ $b_all }}</a></td>
                    <td><a href="#" class="link_list" data-query="DATEDIFF( NOW( ) , created_at )<1 and jenis_order={{ $order->id }}">{{ $a_all }}</a></td>
                    <td><a href="#" class="link_list" data-query="jenis_order={{ $order->id }}">{{ $ttlall }}</a></td>
                </tr>
            </table>
        </div>
    </div>
</div>
    <div id="listModal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        List
                    </h4>
                </div>
                <div class="modal-body">
                    
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            setInterval(function() {

              var currentTime = new Date();
              var hours = currentTime.getHours();
              var minutes = currentTime.getMinutes();
              var seconds = currentTime.getSeconds();

              // Add leading zeros
              hours = (hours < 10 ? "0" : "") + hours;
              minutes = (minutes < 10 ? "0" : "") + minutes;
              seconds = (seconds < 10 ? "0" : "") + seconds;

              // Compose the string for display
              var currentTimeString = hours + ":" + minutes + ":" + seconds;

              $(".clock").html(currentTimeString);

            }, 1000);
            $(".link_list").click(function(event) {
                dq = this.getAttribute("data-query");
                $.get( "/ajaxReport/"+dq, function(data) {
                    $(".modal-body").html(data);
                })
                .done(function(data) {
                    console.log("success");
                });
                
                $('#listModal').modal({show:true});
            });
        });
    </script>
@endsection