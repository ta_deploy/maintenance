@extends('layout2')
@section('head')
    <style type="text/css">
        th{
            text-align: center;
            vertical-align: middle;
        }
    </style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>{!! $title !!}</span>
</h1>
@endsection
@section('content')
<form method="GET">
    <div class="form-group row">
        <div class="col-sm-12">
            <div class="input-group">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-info dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pilih <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="#" id="daily">Daily</a></li>
                        <li><a href="#" id="monthly">Monthly</a></li>
                    </ul>
                </span>
                <span class="ins">
                    <input type="text" name="tgl" id="tgl" value="{{ date('Y-m') }}" class="form-control input-sm">
                </span>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-4">
            <input class="form-control get_data_selection" id="mitra" name="mitra">
        </div>
        <div class="col-md-4">
            <input class="form-control get_data_selection" id="sektor" name="sektor">
        </div>
        <div class="col-md-4">
            <input class="form-control get_data_selection" id="order" name="order">
        </div>
    </div>
    <span class="input-group-btn">
        <button type="submit" class="btn btn-default btn-primary btn-sm go" type="button">Cari!</button>
    </span>
</form>
    <br/>
    <div class="panel panel-default">
        <div class="panel-heading">{!! $title !!}</div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped" id="table">
                <tr>
                    <th>Regu</th>
                    @if(session('auth')->id_karyawan == 91155668)
                        <th>Nama Regu</th>
                    @endif
                    <th>Sektor</th>
                    <th>Mitra</th>
                    <th>Order</th>
                    <th>UP</th>
                    <th>Sisa</th>
                    <th>OGP</th>
                    <th>Kendala</th>
                    <th>Target</th>
                    <th>Kekurangan</th>
                </tr>
                @foreach($data as $d)
                <tr>
                    <td>{{ $d->label_regu }}</td>
                    @if(session('auth')->id_karyawan == 91155668)
                        <td>{{ $d->uraian }}</td>
                    @endif
                    <td>{{ $d->label_sektor }}</td>
                    <td>{{ $d->nama_mitra }}</td>
                    <td>
                        @if($d->order_data != 0 )
                            <a href="/matrx_prod/list/order_data/{{ $d->id_reg }}/{{ isset($requestan['tgl']) ? $requestan['tgl'] : 'all' }}/{{ isset($requestan['mitra']) ? $requestan['mitra'] : 'all' }}/{{ isset($requestan['sektor']) ? $requestan['sektor'] : 'all' }}/{{ isset($requestan['order']) ? $requestan['order'] : 'all' }}">{{ $d->order_data }}</a>
                        @else
                            -
                        @endif
                    </td>
                    <td>

                        @if($d->up != 0 )
                            <a href="/matrx_prod/list/up/{{ $d->id_reg }}/{{ isset($requestan['tgl']) ? $requestan['tgl'] : 'all' }}/{{ isset($requestan['mitra']) ? $requestan['mitra'] : 'all' }}/{{ isset($requestan['sektor']) ? $requestan['sektor'] : 'all' }}/{{ isset($requestan['order']) ? $requestan['order'] : 'all' }}">{{ $d->up }}</a>
                        @else
                            -
                        @endif
                    </td>
                    <td>
                        @if($d->sisa != 0 )
                            <a href="/matrx_prod/list/sisa/{{ $d->id_reg }}/{{ isset($requestan['tgl']) ? $requestan['tgl'] : 'all' }}/{{ isset($requestan['mitra']) ? $requestan['mitra'] : 'all' }}/{{ isset($requestan['sektor']) ? $requestan['sektor'] : 'all' }}/{{ isset($requestan['order']) ? $requestan['order'] : 'all' }}">{{ $d->sisa }}</a>
                        @else
                            -
                        @endif
                    </td>
                    <td>
                        @if($d->ogp != 0 )
                            <a href="/matrx_prod/list/ogp/{{ $d->id_reg }}/{{ isset($requestan['tgl']) ? $requestan['tgl'] : 'all' }}/{{ isset($requestan['mitra']) ? $requestan['mitra'] : 'all' }}/{{ isset($requestan['sektor']) ? $requestan['sektor'] : 'all' }}/{{ isset($requestan['order']) ? $requestan['order'] : 'all' }}">{{ $d->ogp }}</a>
                        @else
                            -
                        @endif
                    </td>
                    <td>
                        @if($d->kendala != 0 )
                            <a href="/matrx_prod/list/kendala/{{ $d->id_reg }}/{{ isset($requestan['tgl']) ? $requestan['tgl'] : 'all' }}/{{ isset($requestan['mitra']) ? $requestan['mitra'] : 'all' }}/{{ isset($requestan['sektor']) ? $requestan['sektor'] : 'all' }}/{{ isset($requestan['order']) ? $requestan['order'] : 'all' }}">{{ $d->kendala }}</a>
                        @else
                            -
                        @endif
                    </td>
                    <td>
                        @if($d->target != 0 )
                            <a href="/matrx_prod/list/target/{{ $d->id_reg }}/{{ isset($requestan['tgl']) ? $requestan['tgl'] : 'all' }}/{{ isset($requestan['mitra']) ? $requestan['mitra'] : 'all' }}/{{ isset($requestan['sektor']) ? $requestan['sektor'] : 'all' }}/{{ isset($requestan['order']) ? $requestan['order'] : 'all' }}">{{ $d->target }}</a>
                        @else
                            -
                        @endif
                    </td>
                    <td>
                        @if($d->kekurangan != 0 )
                            <a href="/matrx_prod/list/kekurangan/{{ $d->id_reg }}/{{ isset($requestan['tgl']) ? $requestan['tgl'] : 'all' }}/{{ isset($requestan['mitra']) ? $requestan['mitra'] : 'all' }}/{{ isset($requestan['sektor']) ? $requestan['sektor'] : 'all' }}/{{ isset($requestan['order']) ? $requestan['order'] : 'all' }}">{{ $d->kekurangan }}</a>
                        @else
                            -
                        @endif
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function() {
            var day = {
                format: "yyyy-mm-dd",
                viewMode: 0,
                minViewMode: 0,
                autoclose:true
            };

            var month = {
                format: "yyyy-mm",
                viewMode: 2,
                minViewMode: 1,
                autoclose:true
            };

            var mitra = $('#mitra').select2({
                allowClear: true,
                data: <?= json_encode($mitra) ?>,
                placeholder: 'Pilih Mitra',
                formatSelection: function(data) {
                    return data.text
                },
                formatResult: function(data) {
                    return data.text;
                }
            });

            var sektor = $('#sektor').select2({
                allowClear: true,
                data: <?= json_encode($sektor) ?>,
                placeholder: 'Pilih Sektor',
                formatSelection: function(data) {
                    return data.text
                },
                formatResult: function(data) {
                    return data.text;
                }
            });

            var order = $('#order').select2({
                allowClear: true,
                data: <?= json_encode($orderjenis) ?>,
                placeholder: 'Pilih Jenis Order',
                formatSelection: function(data) {
                    return data.text
                },
                formatResult: function(data) {
                    return data.text;
                }
            });

            $('#mitra').change(function(){
                $.ajax({
                    type: 'GET',
                    url: '/mitra/prod_search/ajx',
                    dataType: 'json',
                    data: {
                        mitra: $(this).val(),
                        tgl: $('#tgl').val()
                    },
                    success: function(value) {
                        sektor.select2({
                            allowClear: true,
                            data: value.sektor,
                            placeholder: 'Pilih Sektor',
                            formatSelection: function(data) {
                                return data.text
                            },
                            formatResult: function(data) {
                                return data.text;
                            }
                        });

                        // order.select2({
                        //     allowClear: true,
                        //     data: value.orderjenis,
                        //     placeholder: 'Pilih Jenis Order',
                        //     formatSelection: function(data) {
                        //         return data.text
                        //     },
                        //     formatResult: function(data) {
                        //         return data.text;
                        //     }
                        // });
                    }
                });
            });

            $('#sektor').change(function(){
                $.ajax({
                    type: 'GET',
                    url: '/mitra/prod_search/ajx',
                    dataType: 'json',
                    data: {
                        mitra: $('#mitra').val(),
                        tgl: $('#tgl').val(),
                        sektor: $(this).val()
                    },
                    success: function(value) {
                        // order.select2({
                        //     allowClear: true,
                        //     data: value.orderjenis,
                        //     placeholder: 'Pilih Jenis Order',
                        //     formatSelection: function(data) {
                        //         return data.text
                        //     },
                        //     formatResult: function(data) {
                        //         return data.text;
                        //     }
                        // });
                    }
                });
            });

            var q_u = <?=json_encode($requestan) ?>;
            if (!$.isEmptyObject(q_u)) {
                var mitra = <?= json_encode($mitra) ?>;
                $('#mitra').select2({
                    allowClear: true,
                    data: mitra,
                    placeholder: 'Pilih Mitra',
                    formatSelection: function(data) {
                        return data.text
                    },
                    formatResult: function(data) {
                        return data.text;
                    }
                });

                var sektor_jns = <?= json_encode($sektor) ?>;
                $('#sektor').select2({
                    allowClear: true,
                    data: sektor_jns,
                    placeholder: 'Pilih Sektor',
                    formatSelection: function(data) {
                        return data.text
                    },
                    formatResult: function(data) {
                        return data.text;
                    }
                });

                var orderjenis = <?= json_encode($orderjenis) ?>;
                $('#order').select2({
                    data: orderjenis,
                    placeholder: 'Pilih Jenis Order',
                    formatSelection: function(data) {
                        return data.text
                    },
                    formatResult: function(data) {
                        return data.text;
                    }
                });

                $('#mitra').val(q_u.mitra).trigger('change');
                $('#sektor').val(q_u.sektor).trigger('change');
                $('#order').val(q_u.order).trigger('change');

                $('#tgl').val(q_u.tgl);
            }

            var d = new Date(),
            monthly = '' + (d.getMonth() + 1),
            dayly = '' + d.getDate(),
            year = d.getFullYear();

            if (monthly.length < 2) monthly='0' + monthly;
            if (dayly.length < 2) dayly='0' + dayly;

            $("#tgl").datepicker(month).on('changeDate',function(ev){
                $("#mitra").val('').change();
                $.ajax({
                    type: 'GET',
                    url: '/mitra/prod_search/ajx',
                    dataType: 'json',
                    data: {
                        tgl: $(this).val()
                    },
                    success: function(value) {
                        mitra.select2({
                            allowClear: true,
                            data: value.mitra,
                            placeholder: 'Pilih Mitra',
                            formatSelection: function(data) {
                                return data.text
                            },
                            formatResult: function(data) {
                                return data.text;
                            }
                        });
                    }
                });
                $("#sektor").val('').change();
                // $("#order").val('').change();
            });

            $("#daily").click(function(){
                $("#tgl").remove();
                $(".ins").after("<input type=text class='form-control input-sm' id=tgl>");
                $("#tgl").datepicker(day);
                $("#tgl").val([year, monthly, dayly].join('-'));
            });

            $("#monthly").click(function(){
                $("#tgl").remove();
                $(".ins").after("<input type=text class='form-control input-sm' id=tgl>");
                $("#tgl").datepicker(month);
                $("#tgl").val([year, monthly].join('-'));
            });
        });
    </script>
@endsection