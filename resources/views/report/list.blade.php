@extends('layout2')
@section('head')
<style type="text/css">
    th{
        text-align: center;
    }

    /*.select2-selection__arrow{
        display:none !important;
        }*/
    </style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>List</span>
</h1>
@endsection
@section('content')
<?php $authLevel = Session::get('auth')->maintenance_level ?>

<div class="panel panel-default" id="info">
    <div class="panel-body table-responsive">
        <table class="table table-bordered table-striped" id="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>NO TIKET</th>
                    <th>IMPACT</th>
                    <th>ACTION</th>
                    <th>STATUS</th>
                    <th>HEADLINE</th>
                    <th>UMUR</th>
                    <th>SELESAI</th>
                    <th>Regu</th>
                    <th>STO</th>
                    <th>Datel</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $no => $d)
                <?php
                $dteStart = new \DateTime($d->created_at);
                if($d->tgl_selesai)
                    $dteEnd   = new \DateTime($d->tgl_selesai);
                else
                    $dteEnd   = new \DateTime(date('Y-m-d H:i:s'));
                $dteDiff  = $dteStart->diff($dteEnd);
                $durasi   = $dteDiff->format("%Dd %Hh %im");
                ?>
                <tr>
                    <td>{{ ++$no }}</td>
                    <td><a href="/tech/{{ $d->id }}">{{ $d->no_tiket }}</a><br /><span
                            class="label label-primary">{{ $d->nama_dp or $d->nama_odp }}</span>
                        @if($d->status != 'close')
                        <br />
                        <span>
                            <button class="btn btn-xs btn-success button_refer" id-mt="{{ $d->id }}">Refer</button>
                            @if (!is_null($d->check_regu) && session('auth')->maintenance_level == 0 || session('auth')->maintenance_level != 0)
                                <button class="btn btn-xs btn-success button_check"
                                    id-disp="{{ $d->id }}">{{ empty($d->dispatch_regu_id) ? 'Dispatch' : 'Re-Dispatch' }}</button>
                                <button class="btn btn-xs btn-danger button_delete" id-mt="{{ $d->id }}">Delete</button>
                            @endif
                        </span>
                        @endif
                        <br /><span class="label label-primary">{{ $d->nama_order }}</span>
                    </td>
                    <td>
                        <p class="anak_tiket" id-mtt="{{ $d->id }}" style="color:#0ce3ac;text-decoration:underline;">
                            {{ $d->jml_anak }} Tiket</p>
                        <div id-mtt="{{ $d->id }}"></div>
                    </td>
                    <td>{{ $d->action }}</td>
                    <td>{{ $d->status }}</td>
                    <td>{{ $d->headline }}</td>
                    <td>{{ $d->created_at }} / {{ $durasi }}</td>
                    <td>{{ $d->tgl_selesai }}</td>
                    <td><span class="nama_regu">{{ $d->dispatch_regu_name }}</span>
                        <br />
                        <span class="label label-success">{{ $d->nama1 }}&{{ $d->nama2 }}</span>
                        <br />
                        <span class="label label-info">{{ $d->koordinat }}</span>
                    </td>
                    <td>{{ $d->sto }}</td>
                    <td>{{ $d->kandatel }}</td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>



    @endsection
    @section('script')
    <script>
        $(function() {

        });
    </script>
    @endsection