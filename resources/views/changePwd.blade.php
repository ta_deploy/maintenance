@extends('layoutnomenu')
@section('head')
@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Chage pwd nik {{ $data->user }}
        </div>
        <div class="panel-body">
            <div class="col-sm-9">
            <form method="post" class="form-horizontal" enctype="multipart/form-data" >
                <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
                    <label for="pwd" class="col-sm-4 col-md-3 control-label">Pwd</label>
                    <div class="col-sm-5">
                        
                        <input name="pwd" type="text" id="nama" class="form-control input-sm" value="{{ old('pwd') ?: $data->pwd }}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-6 col-sm-2">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
@endsection