@extends('layout2')

@section('head')
    <link rel="stylesheet" href="/bower_components/select2/select2.css" />
    <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css" />
@endsection

@section('content')
    <?php
        $auth = (session('auth'));
    ?>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            @if($auth->maintenance_level == 1)
            Input Order dan Assignment
            @else
            INPUT ORDER BENJAR
            @endif
        </div>
        <div class="panel-body">
            <div class="form col-sm-8 col-md-8">
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-group {{ $errors->has('order_id') ? 'has-error' : '' }}">
                    <label for="txtKode" class="col-sm-4 col-md-3 control-label">Order ID</label>
                    <div class="col-sm-8">
                        <input name="order_id" type="text" id="txtKode" class="form-control input-sm" value="{{ old('order_id') ?: @$data->Incident }}" disabled="true" />
                        @foreach($errors->get('order_id') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                @if($auth->level ==2 || $auth->level == 53)
                <div class="form-group">
                    <label for="jenis_order" class="col-sm-4 col-md-3 control-label">Jenis Order</label>
                    <div class="col-sm-8">
                        <select name="jenis_order" id="jenis_order" class="form-control input-sm" {{ ($auth->level == '2' || $auth->level == '53') ? '' : 'disabled' }}>
                            @foreach($jenis_order as $jo)
                                @if($jo->id == 3 || $jo->id == 5 || $jo->id == 2)
                                    <option value="{{ $jo->id }}">{{ $jo->nama_order }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                @else
                    <input type="hidden" name="jenis_order" value="1" />
                @endif
                <div class="form-group">
                    <label for="tipe" class="col-sm-4 col-md-3 control-label">Nama ODP</label>
                    <div class="col-sm-8">
                        <input name="nama_odp" type="text" id="nama_odp" class="form-control input-sm" value="{{ old('nama_odp') }}"/>
                    </div>
                </div>
                <div class="form-group {{ $errors->has('headline') ? 'has-error' : '' }}">
                    <label for="txtPin" class="col-sm-4 col-md-3 control-label">Headline</label>
                    <div class="col-sm-8">
                        <textarea name="headline" rows="4" class="form-control">{{ old('headline') ?: @$data->Summary }}</textarea>
                        @foreach($errors->get('headline') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>

                <div class="form-group">
                    <label for="tipe" class="col-sm-4 col-md-3 control-label">PIC</label>
                    <div class="col-sm-8">
                        <input name="pic" type="text" id="pic" class="form-control input-sm" value="{{ old('pic') }}"/>
                    </div>
                </div>
                <!--
                <div class="form-group">
                    <label for="koordinat" class="col-sm-4 col-md-3 control-label">Koordinat</label>
                    <div class="col-sm-7">
                        <input name="koordinat" type="text" id="koor" class="form-control" value="{{ old('koordinat') ?: @$data->koordinat }}"/>
                    </div>
                </div>
                -->
                <div class="form-group {{ $errors->has('koordinat') ? 'has-error' : '' }}">
                    <label for="" class="col-sm-4 col-md-3 col-xs-12 control-label">Koordinat</label>
                    <div class="col-sm-6 col-xs-9">
                        <input name="koordinat" id="input-koordinat" class="form-control input-sm" rows="1" value="{{ old('koordinat') }}" />
                        @foreach($errors->get('koordinat') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                    <div class="col-sm-2 col-xs-3" align="right">
                        <button id="btnLoadMap" title="Beri tanda pada Peta" class="btn btn-default btn-sm" data-toggle="tooltip" type="button">
                            <i class="fa fa-map-marker"></i>
                        </button>
                    </div>
                </div>
                @if($auth->level ==2 || $auth->level == 53)
                    <div class="form-group {{ $errors->has('regu') ? 'has-error' : '' }}">
                        <label for="regu" class="col-sm-4 col-md-3 control-label">Order ke Tim</label>
                        <div class="col-sm-8">
                            <input name="regu" type="text" id="regu" class="form-control input-sm" value="{{ old('regu') }}"/>
                            @foreach($errors->get('regu') as $msg)
                                <span class="help-block">{{ $msg }}</span>
                            @endforeach
                        </div>
                    </div>
                @endif
                <div class="form-group {{ $errors->has('sto') ? 'has-error' : '' }}">
                    <label for="sto" class="col-sm-4 col-md-3 control-label">STO</label>
                    <div class="col-sm-8">
                        <input name="sto" type="text" id="sto" class="form-control input-sm" value="{{ old('sto') ?: @$data->Workzone }}"/>
                        @foreach($errors->get('sto') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-3 col-sm-3">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    <div id="mapModal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        Koordinat ODP (<span id="lonText">0</span>, <span id="latText">0</span>)
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="mapView" style="height:350px;"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button id="btnGetMarker" class="btn btn-primary">OK</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSn96DCIJdATC6AHuV3sLF3ddwdaIsW10"></script>
    <script src="/js/mapmarkerlonlat.js"></script>
    <script src="/bower_components/select2/select2.min.js"></script>
    <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
    <script>
        $(function() {
            var data = <?= json_encode($sto) ?>;
            var combo = $('#sto').select2({
                data: data
            });
            var data = <?= json_encode($regu) ?>;
            var regu = $('#regu').select2({
                data: data
            });
            $("#nama_odp").inputmask("AAA-AAA-A{2,3}/9{3,4}");
        });
    </script>
@endsection