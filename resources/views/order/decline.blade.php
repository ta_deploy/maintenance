@extends('layout2')
@section('head')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowgroup/1.1.3/css/rowGroup.dataTables.min.css">
<style type="text/css">
    .container {
        width: 100%;
        margin: 5px;
    }
    .table-condensed{
      font-size: 10px;
    }
    .head{
        color: #303030;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LIST DECLINE</span>
</h1>
@endsection
@section('content')
<form method="GET" style="margin-bottom: 11px;">
  <div class="form-group row">
      <div class="col-sm-12">
        <span class="ins">
          <input type="text" name="tgl" id="tgl" value="{{ date('Y-m') }}" class="form-control input-sm">
        </span>
      </div>
    </div>
    <span class="input-group-btn">
      <button type="submit" class="btn btn-default btn-primary btn-sm go" type="button">Cari!</button>
    </span>
</form>
<div class="panel panel-default" id="info">

  <div class="panel-heading">LIST TIKET Decline Diambil Dari Tanggal {{ date('Y-m-d', strtotime($tgl .'-1 month') ) }} sampai {{ $tgl }}</div>
    <div id="fixed-table-container-demo" class="fixed-table-container">
      <table class="table table-bordered table-fixed" style="width: 100%;">
        <thead>
          <tr>
            <th class="head no-sort">Nomor</th>
            <th class="head no-sort">Nama Mitra</th>
            <th class="head">Nama Regu</th>
            <th class="head">Nomor Tiket</th>
            <th class="head">Tanggal Open</th>
            <th class="head">Headline</th>
            <th class="head">Jenis Order</th>
            <th class="head">ODP</th>
            <th class="head">STO</th>
            <th class="head">Nomor Internet</th>
            <th class="head">Nik</th>
            <th class="head">Alasan Decline</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/rowgroup/1.1.3/js/dataTables.rowGroup.min.js"></script>
<script type="text/javascript">
  $(function(){
    var data = {!! json_encode($data) !!},
    kolom = [],
    final_dta = [],
    month = {
      format: "yyyy-mm",
      minViewMode: 1,
      autoclose:true
    };

    $("#tgl").datepicker(month);

    $.each(data, function(key, val) {
      kolom.push([
        ++key,
        val.nama_mitra ?? 'Tidak Ada Mitra',
        val.dispatch_regu_name,
        val.no_tiket,
        val.created_at,
        val.headline,
        val.nama_order,
        val.nama_odp_m,
        val.sto,
        val.no_inet,
        val.pelaku + ' (' + val.nama + ')',
        val.cttn_decline,
        "<a href='/profile/view?nik="+val.id+"&grafik=recruit' class='btn btn-info'>View</a>"
      ]);
    })

    final_dta = {
      BO: kolom,
    };

    // console.log(final_dta)

    $(".table").DataTable({
      columnDefs: [
        {
          targets  : 'no-sort',
          orderable: false,
        }
      ],
      drawCallback: function () {
        $( 'table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
      },
      fixedHeader: {
        header: true,
        footer: true
      },
      data: final_dta.BO,
      deferRender: true,
      scrollCollapse: true,
      scroller: true,
      orderFixed: [1, 'asc'],
      rowGroup: {
        dataSrc: [1, 8],
        startRender: function ( rows, group ) {
          var value = (rows.count())
            return 'Jumlah WO '+ group +' Adalah ('+value+')';
        },
      },
    }).columns.adjust().draw();

  })
</script>
@endsection