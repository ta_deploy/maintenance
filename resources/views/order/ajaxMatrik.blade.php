@foreach($data as $no => $d)
    <?php
        $total = $d->undisp+$d->no_update+$d->kendala_pelanggan+$d->kendala_teknis+$d->close;
    ?>
    <tr class="ajax">
        <td style="color:green;">{{ $d->datel }}</td>
        <td style="color:green;"><a href="/order/{{ $id }}/undisp/all/{{ $d->datel }}" style="color:green;">{{ $d->undisp }}</td>
        <td style="color:green;"><a href="/order/{{ $id }}/null/all/{{ $d->datel }}" style="color:green;">{{ $d->no_update }}</td>
        <td style="color:green;"><a href="/order/{{ $id }}/kendala pelanggan/all/{{ $d->datel }}" style="color:green;">{{ $d->kendala_pelanggan }}</td>
        <td style="color:green;"><a href="/order/{{ $id }}/kendala teknis/all/{{ $d->datel }}" style="color:green;">{{ $d->kendala_teknis }}</td>
        <td style="color:green;"><a href="/order/{{ $id }}/close/{{date('Y-m')}}/{{ $d->datel }}" style="color:green;">{{ $d->close }}</td>
    </tr>
@endforeach