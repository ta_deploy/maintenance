@extends('layout2')

@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Input Order dan Assignment</span>
</h1>
@endsection
@section('content')
    <?php
        $auth = (session('auth'));
    ?>
    <div class="panel panel-default">
    @if(in_array($auth->maintenance_level, [1, 3, 7]))
        <div class="umur"></div>
        <div class="dup_order_id"></div>
        <div class="panel-body">
            <div class="form col-sm-12 col-md-12">
                <form method="post" id="submit_form_register" class="form-horizontal" enctype="multipart/form-data">
                    @if(Request::segment(1) == 'order_Nog')
                        <input type="hidden" name="nog_order_id" value="{{ Request::segment(2) }}">
                    @endif
                    @if(in_array($auth->maintenance_level, [1, 2, 3, 7]))
                        <div class="form-group">
                            <label for="jenis_order" class="col-sm-4 col-md-3 control-label">Jenis Order</label>
                            <div class="col-sm-8">
                                <select name="jenis_order" id="jenis_order" class="form-control input-sm" {{ (in_array($auth->maintenance_level, [1, 2, 3, 7])) ? '' : 'disabled' }}>
                                    @foreach($jenis_order as $jo)
                                            <option value="{{ $jo->id }}" {{ @$data->jenis_order== $jo->id ? "selected" : "" }}>{{ $jo->nama_order }}</option>
                                        @if($jo->id != 4)
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    <div class="inject_field"></div>
                    {{-- <div class="form-group {{ $errors->has('order_id') ? 'has-error' : '' }}">
                        <label for="txtKode" class="col-sm-4 col-md-3 control-label">Order ID</label>
                        <div class="col-sm-8">
                            <input name="order_id" type="text" id="txtKode" class="form-control order_id input-sm" value="{{ old('order_id') ?: @$data->order_id }}"/>
                            @foreach($errors->get('order_id') as $msg)
                                <span class="help-block">{{ $msg }}</span>
                            @endforeach
                        </div>
                    </div>
                    @if (Request::segment(1) != 'order_Nog')
                        @if(in_array($auth->maintenance_level, [1, 2, 3, 7]))
                            <div class="form-group unspect_only">
                                <label for="kategori" class="col-sm-4 col-md-3 control-label">Kategory</label>
                                <div class="col-sm-8">
                                    <select name="kategori" id="kategori" class="form-control input-sm" {{ (in_array($auth->maintenance_level, [1, 2, 3, 7])) ? '' : 'disabled' }}>
                                        <option value="HVC_PLATINUM">HVC PLATINUM</option>
                                        <option value="HVC_GOLD">HVC GOLD</option>
                                        <option value="HVC_SILVER">HVC SILVER</option>
                                        <option value="REGULER">REGULER</option>
                                    </select>
                                </div>
                            </div>
                        @else
                            <input type="hidden" name="jenis_order" value="1" />
                            <input type="hidden" name="kategori" value="" />
                        @endif
                        <div class="form-group unspect_only {{ $errors->has('no_inet') ? 'has-error' : '' }}">
                            <label for="tipe" class="col-sm-4 col-md-3 control-label">No.INET</label>
                            <div class="col-sm-8">
                                <input name="no_inet" type="text" id="no_inet" class="form-control input-sm" value="{{ old('no_inet') ?: @$data->no_inet }}"/>
                                @foreach($errors->get('no_inet') as $msg)
                                    <span class="help-block">{{ $msg }}</span>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="tipe" class="col-sm-4 col-md-3 control-label">Nama ODP</label>
                        <div class="col-sm-8">
                            <input name="nama_odp" type="text" id="nama_odp" class="form-control input-sm" value="{{ old('nama_odp') ?: @$data->nama_odp }}"/>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('headline') ? 'has-error' : '' }}">
                        <label for="txtPin" class="col-sm-4 col-md-3 control-label">Headline</label>
                        <div class="col-sm-8">
                            <textarea name="headline" rows="4" class="form-control">{{ old('headline') ?: @$data->headline }}</textarea>
                            @foreach($errors->get('headline') as $msg)
                                <span class="help-block">{{ $msg }}</span>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tipe" class="col-sm-4 col-md-3 control-label">PIC</label>
                        <div class="col-sm-8">
                            <input name="pic" type="text" id="pic" class="form-control input-sm" value="{{ old('pic') ?: @$data->pic }}"/>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('koordinat') ? 'has-error' : '' }}">
                        <label for="" class="col-sm-4 col-md-3 col-xs-12 control-label">Koordinat</label>
                        <div class="col-sm-6 col-xs-9">
                            <input name="koordinat" id="input-koordinat" class="form-control input-sm" rows="1" value="{{ old('koordinat') ?: @$data->koordinat }}" />
                            @foreach($errors->get('koordinat') as $msg)
                                <span class="help-block">{{ $msg }}</span>
                            @endforeach
                        </div>
                    </div>
                    @if(in_array($auth->maintenance_level, [1, 2, 3, 7]))
                        <div class="form-group {{ $errors->has('regu') ? 'has-error' : '' }}">
                            <label for="regu" class="col-sm-4 col-md-3 control-label">Order ke Tim</label>
                            <div class="col-sm-8">
                                <input name="regu" type="text" id="regu" class="form-control input-sm" value="{{ old('regu') ?: @$data->dispatch_regu_id }}"/>
                                @foreach($errors->get('regu') as $msg)
                                    <span class="help-block">{{ $msg }}</span>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <div class="form-group {{ $errors->has('sto') ? 'has-error' : '' }}">
                        <label for="sto" class="col-sm-4 col-md-3 control-label">STO</label>
                        <div class="col-sm-8">
                            <input name="sto" type="text" id="sto" class="form-control input-sm" value="{{ old('sto') ?: @$data->sto }}"/>
                            @foreach($errors->get('sto') as $msg)
                                <span class="help-block">{{ $msg }}</span>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('sektor') ? 'has-error' : '' }}">
                        <label for="sektor" class="col-sm-4 col-md-3 control-label">Sektor</label>
                        <div class="col-sm-8">
                            <input name="sektor" type="text" id="sektor" class="form-control input-sm" value="{{ old('sektor') ?: @$data->sektor }}"/>
                            @foreach($errors->get('sektor') as $msg)
                                <span class="help-block">{{ $msg }}</span>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('timeplan') ? 'has-error' : '' }}">
                        <label for="timeplan" class="col-sm-4 col-md-3 control-label">Timeplan</label>
                        <div class="col-sm-8">
                            <input name="timeplan" type="text" id="timeplan" class="form-control input-sm" value="{{ old('timeplan') ?: @$data->timeplan }}"/>
                            @foreach($errors->get('timeplan') as $msg)
                                <span class="help-block">{{ $msg }}</span>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-md-offset-3 col-sm-3">
                            <button class="btn btn-primary btn-smpn" disabled type="submit">
                                <span class="fa fa-floppy-o"></span>
                                <span>Simpan</span>
                            </button>
                        </div>
                    </div> --}}
                </form>
            </div>
        </div>
    @else
        Hanya HD yg bisa meulah tiket
    @endif
    </div>

    <div id="mapModal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        Koordinat ODP (<span id="lonText">0</span>, <span id="latText">0</span>)
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="mapView" style="height:350px;"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button id="btnGetMarker" class="btn btn-primary">OK</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSn96DCIJdATC6AHuV3sLF3ddwdaIsW10"></script>
    <script src="/js/mapmarkerlonlat.js"></script>
    <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
    <script>
        $(function() {
            $('#jenis_order').select2({
                placeholder: 'Masukkan Jenis Ordernya'
            });


            function get_inet(vall, val2){
                // if(vall.length > 10){
                //     $.ajax({
                //         url: "/check_umur_inet",
                //         type: 'GET',
                //         data: {
                //             inet: vall,
                //             jenis_order: val2,
                //             id: 'all'
                //         },
                //         dataType: 'JSON'
                //         }).done(function(data){
                //         if($.isEmptyObject(data)){
                //             $('.btn-smpn').removeAttr('disabled');

                //             $('.umur').html("<div class='alert alert-info'>Data Bisa Di Submit!</div>");
                //         }else{
                //             $('.btn-smpn').attr('disabled', true);
                //             $('.umur').html(`<div class='alert alert-danger'>Nomor Internet Terdeteksi Digunakan Pada Tanggal <b>${data.created_at}</b> Oleh User <b>${data.created_by}</b> Dengan Judul <b>${data.no_tiket}</b>, Cek Data Kembali!</div>`);
                //         }
                //     })
                // }else{
                //     var jml = 11 - vall.length;
                //     $('.umur').html("<div class='alert alert-warning'>Masukkan " + jml + " nomor Internet!</div>");
                //     $('.btn-smpn').attr('disabled', true);
                // }
                $.ajax({
                    url: "/check_umur_inet",
                    type: 'GET',
                    data: {
                        inet: vall,
                        jenis_order: val2,
                        id: 'all'
                    },
                    dataType: 'JSON'
                    }).done(function(data){
                        $('.btn-smpn').removeAttr('disabled');

                        $('.umur').html("<div class='alert alert-info'>Data Bisa Di Submit!</div>");
                    // if($.isEmptyObject(data)){
                    //     $('.btn-smpn').removeAttr('disabled');

                    //     $('.umur').html("<div class='alert alert-info'>Data Bisa Di Submit!</div>");
                    // }else{
                    //     $('.btn-smpn').attr('disabled', true);
                    //     $('.umur').html(`<div class='alert alert-danger'>Nomor Internet Terdeteksi Digunakan Pada Tanggal <b>${data.created_at}</b> Oleh User <b>${data.created_by}</b> Dengan Judul <b>${data.no_tiket}</b>, Cek Data Kembali!</div>`);
                    // }
                })
            }

            function get_order_id(vall){
                if(vall.length > 6){
                    $.ajax({
                        url: "/check_dup_OI",
                        type: 'GET',
                        data: {
                            order_id: vall,
                            id: window.location.href.split('/')[4],
                            jenis_order: $('#jenis_order').val()
                        },
                        dataType: 'JSON'
                    }).done(function(data){
                        if($.isEmptyObject(data) ){
                            $('.dup_order_id').html("<div class='alert alert-info'>Order ID Belum Dipakai Sama Sekali!</div>");
                        }else{
                            $('.dup_order_id').html(`<div class='alert alert-danger'>Order ID Sudah Terpakai Pada Tanggal <b>${data.created_at}</b> Oleh User <b>${data.created_by}</b> Untuk Pekerjaan <b>${data.nama_order}</b></div>`);
                        }
                    })
                }
            }


            var url = {!! json_encode(Request::segment(1) ) !!};
            data_sumber = {!! json_encode($data) ?? null !!};

            var get_field = [];

            $('#jenis_order').on('change', function(){
                var get_setting = {!! json_encode($jenis_order) !!};
                var result_search = $.grep(get_setting, function(e){ return e.id == $('#jenis_order').val(); })[0];
                var get_link = window.location.href.split('/')[3];

                var order_id = $('.order_id').val();
                var no_inet = $("#no_inet").val();
                var nama_odp = $('#nama_odp').val();
                var headline = $("textarea[name='headline']").val();
                var pic = $('#pic').val();
                var koordinat = $('#koordinat').val();
                var regu = $('#regu').val();
                var sto = $('#sto').val();
                var sektor = $('#sektor').val();
                var timeplan = $('#timeplan').val();

                if(result_search.field_register_order.length != 0){
                    get_field = JSON.parse(result_search.field_register_order);
                }
                // console.log(result_search, get_field)
                field_html = '';

                // if(get_link == 'order_Nog'){
                //     get_field = get_field.filter(function(v) {
                //         return v.id != 'timeplan';
                //     });
                // }

                $.each(get_field, function(k, v){
                    field_html += "<div class='form-group' style='display: "+(v.visible ? 'block' : 'none')+"'>";
                    field_html += "<label for='"+v.id.toLowerCase()+"' class='col-sm-4 col-md-3 control-label'>"+v.text+"</label>";
                    field_html += "<div class='col-sm-8'>";

                    if(v.id == 'Headline'){
                        field_html += "<textarea rows=4 name='"+v.id.toLowerCase()+"' id='"+v.id.toLowerCase()+"' "+(v.require ? 'required' : '')+" class='form-control "+v.id.toLowerCase()+" input-sm' value=''/>";
                    }
                    else{
                        field_html += "<input name='"+v.id.toLowerCase()+"' type='text' id='"+v.id.toLowerCase()+"' "+(v.require ? 'required' : '')+" class='form-control "+v.id.toLowerCase()+" input-sm' value=''/>";
                    }

                    field_html += "</div>";
                    field_html += "</div>";
                });

                if($('#jenis_order').val() == 2){
                    field_html += "<div class='form-group'>";
                    field_html += "<label for='pilih_jointer' class='col-sm-4 col-md-3 control-label'>Jointer</label>";
                    field_html += "<div class='col-sm-8'>";
                    field_html += "<input name='pilih_jointer' type='text' id='pilih_jointer' required class='form-control pilih_jointer input-sm' value=''/>";
                    field_html += "</div>";
                    field_html += "</div>";

                    field_html += "<div class='form-group field_jointer_tim'>";
                    field_html += "<label for='tim_jointer' class='col-sm-4 col-md-3 control-label'>Tim Jointer</label>";
                    field_html += "<div class='col-sm-8'>";
                    field_html += "<input name='tim_jointer' type='text' id='tim_jointer' class='form-control tim_jointer input-sm' value=''/>";
                    field_html += "</div>";
                    field_html += "</div>";
                }

                field_html += "<div class='form-group'>";
                field_html += "<div class='col-sm-offset-3 col-md-offset-3 col-sm-3'>";
                field_html += "<button class='btn btn-primary btn-smpn' disabled type='submit'>";
                field_html += "<span class='fa fa-floppy-o'></span>";
                field_html += "<span>&nbsp;Simpan</span>";
                field_html += "</button>";
                field_html += "</div>";
                field_html += "</div>";

                $(".inject_field").html(field_html);

                var data = {!! json_encode($sto) !!},
                combo = $('#sto').select2({
                    data: data,
                    placeholder: "Pilih STO"
                });

                var data = {!! json_encode($sektor) !!},
                combo = $('#sektor').select2({
                    data: data,
                    placeholder: "Pilih Sektor"
                });

                var data = {!! json_encode($regu) !!},
                regu = $('#regu').select2({
                    data: data,
                    placeholder: "Pilih Regu"
                });

                var data = [
                    {id: 'HVC_PLATINUM', text: 'HVC PLATINUM'},
                    {id: 'HVC_GOLD', text: 'HVC GOLD'},
                    {id: 'HVC_SILVER', text: 'HVC SILVER'},
                    {id: 'REGULER', text: 'REGULER'}
                ],
                kategori = $('#kategori').select2({
                    data: data,
                    placeholder: "Pilih Kategory"
                });

                if($('#jenis_order').val() == 2){
                    var data = [
                        {id: 'pick_up', text: 'Pilih Team'},
                        {id: 'freelance', text: 'Freelance'},
                    ],
                    jointer_tim_jenis = $('#pilih_jointer').select2({
                        data: data,
                        placeholder: "Pilih Jenis Tim"
                    });

                    $('#pilih_jointer').on('change', function(){
                        if($(this).val() == 'pick_up'){
                            $(".field_jointer_tim").show();
                            $('#tim_jointer').attr('required', 'required');
                        }else{
                            $(".field_jointer_tim").hide();
                            $('#tim_jointer').removeAttr('required');
                        }
                    });

                    $('#pilih_jointer').val('pick_up').change();

                    var data = {!! json_encode($tim_jointer) !!},
                    data_tim = [];
                    $.each(data, function(k, v){
                        data_tim.push({
                            id: v.id_user,
                            text: v.nama
                        });
                    })
                    var tim = $('#tim_jointer').select2({
                        data: data_tim,
                        placeholder: "Pilih Tim"
                    });
                }

                $('#timeplan').datepicker({
                    format: 'yyyy-mm-dd'
                }).datepicker("setDate",'now');

                $("#nama_odp").inputmask("AAA-AAA-A{2,3}/9{3,4}");

                if($(this).val() == 17){
                    var time = new Date().getTime();

                    if(!$.isEmptyObject(data_sumber) ){
                        $('.order_id').val(data_sumber.order_id);
                        $('#nama_odp').val(data_sumber.nama_odp);
                        $("textarea[name='headline']").val('NORMALISASI NOG SUMBER DATA ' + data_sumber.jenis_order.replace(/_/g, " ") );
                        $('#koordinat').val(data_sumber.koordinat_odp)
                        $('#sto').val(data_sumber.sto_nog).change();
                    }
                    else
                    {
                        $('.order_id').val('INM'+ time.toString().substring(time.toString().length - 8) )
                        $('#nama_odp').val(nama_odp);
                        $("textarea[name='headline']").val(headline);
                        $('#koordinat').val(koordinat);
                        $('#sto').val(sto);
                    }
                }
                else
                {
                    if(!$.isEmptyObject(data_sumber) ){
                        $('.order_id').val(data_sumber.order_id);
                        $("#no_inet").val(data_sumber.no_inet);
                        $('#nama_odp').val(data_sumber.nama_odp);
                        $("textarea[name='headline']").val(data_sumber.headline);
                        $('#pic').val(data_sumber.pic);
                        $('#koordinat').val(data_sumber.koordinat);
                        $('#regu').val(data_sumber.dispatch_regu_id);
                        $('#sto').val(data_sumber.sto);
                        $('#sektor').val(data_sumber.sektor);
                        $('#timeplan').val(data_sumber.timeplan);
                    }
                    else
                    {
                        $('.order_id').val(order_id);
                        $("#no_inet").val(no_inet);
                        $('#nama_odp').val(nama_odp);
                        $("textarea[name='headline']").val(headline);
                        $('#pic').val(pic);
                        $('#koordinat').val(koordinat);
                        $('#regu').val(regu);
                        $('#sto').val(sto);
                        $('#sektor').val(sektor);
                        $('#timeplan').val(timeplan);
                    }
                }

                $(".order_id").on('keyup', function(){
                    var type = $(this).val();
                    get_order_id(type);
                })

                if($.inArray(parseInt( $(this).val() ), [4] ) != -1){
                    get_inet($("#no_inet").val(), $(this).val() );

                    $("#no_inet").on('keyup', function(){
                        var type = $(this).val();
                        get_inet(type, $('#jenis_order').val());
                    })
                    var time = new Date().getTime(),
                    nik = {!! session('auth')->id_karyawan !!};
                    // $(".unspect_only").show();
                    $('#order_id').val('INM'+nik.toString().substring(0, 2) + time.toString().substring(time.toString().length - 5) + nik.toString().substring(nik.toString().length - 2) )
                }else{
                    // $("#no_inet").unbind('keyup');
                    $('.btn-smpn').removeAttr('disabled');
                    $('.umur').html("");
                    // $(".unspect_only").hide();
                }
            })

            if(url == 'order_Nog'){
                var time = new Date().getTime();

                $('#jenis_order').val(17).change();
                $('#nama_odp').val(data_sumber.nama_odp);
                $('.order_id').val('INM'+ time.toString().substring(time.toString().length - 8) )
                $("textarea[name='headline']").val('NORMALISASI NOG SUMBER DATA ' + data_sumber.jenis_order.replace(/_/g, " ") );
                $('#koordinat').val(data_sumber.koordinat_odp)
                $('#sto').val(data_sumber.sto_nog).change();
            }
            else
            {
                $('#jenis_order').val(1).change();
            }

            $('#form_submit_register').on('submit', function(){
                $.each(get_field, function(k, v){
                    value = $(v.id.toLowerCase() ).val();

                    if(v.id == 'Headline'){
                        field_html += "<textarea rows=4 name='"+v.id.toLowerCase()+"' id='"+v.id.toLowerCase()+"' "+(v.require ? 'required' : '')+" class='form-control "+v.id.toLowerCase()+"'>"+value+"</textarea>";
                    }
                    else{
                        field_html += "<input name='"+v.id.toLowerCase()+"' type='text' id='"+v.id.toLowerCase()+"' "+(v.require ? 'required' : '')+" class='form-control "+v.id.toLowerCase()+" input-sm'  value='" + value + "'/>";
                    }
                });
            })
        });
    </script>
@endsection