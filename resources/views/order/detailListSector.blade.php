@extends('layout2')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>KLASIFIKASI ODP {{ $title->sector }}</span>
</h1>
@endsection
@section('head')
    <style type="text/css">
        .container {
            width: 100%;
            margin: 5px;
        }
        .table-condensed{
          font-size: 10px;
        }
        .head{
            color: #303030;
        }
    </style>
    <link rel="stylesheet" href="/manual_components/fixed-table-master/fixed-table.css"/>
@endsection
@section('content')
<div class="panel panel-default" id="info">
    <div class="panel-heading">Detail Sektor {{ $title->sector }}</div>
     <div id="fixed-table-container-demo" class="fixed-table-container">
        <table class="table table-bordered table-fixed">
            <thead>
            <tr>
                <th class="head">No</th>
                <th class="head">Alpro</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->alpro }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('script')
        <script src="/manual_components/fixed-table-master/fixed-table.js"></script>
        <script type="text/javascript">
            var fixedTable = fixTable(document.getElementById('fixed-table-container-demo'));

        </script>
@endsection