@extends('layout2')

@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Input Order dan Assignment</span>
</h1>
@endsection

@section('content')
    <?php
        $auth = (session('auth'));
    ?>
    
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form col-sm-8 col-md-8">
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-group {{ $errors->has('order_id') ? 'has-error' : '' }}">
                    <label for="txtKode" class="col-sm-4 col-md-3 control-label">Order ID</label>
                    <div class="col-sm-8">
                        <input name="order_id" type="text" id="txtKode" class="form-control input-sm" value="{{ old('order_id') ?: @$data->Ndem }}" disabled="true" />
                        @foreach($errors->get('order_id') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                @if(in_array($auth->maintenance_level, [1, 7]))
                <div class="form-group">
                    <label for="jenis_order" class="col-sm-4 col-md-3 control-label">Jenis Order</label>
                    <div class="col-sm-8">
                        <select name="jenis_order" id="jenis_order" class="form-control input-sm" {{ (in_array($auth->maintenance_level, [1, 7])) ? '' : 'disabled' }}>
                            <option value=18 selected>REBOUNDARY</option>
                            @foreach($jenis_order as $jo)
                                @if((Request::segment(2)==2 && $jo->id == 3) || (Request::segment(2)==24 && $jo->id == 9) || (Request::segment(2)==11 && $jo->id == 6))
                                    <option value="{{ $jo->id }}">{{ $jo->nama_order }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                @else
                    <input type="hidden" name="jenis_order" value="1" />
                @endif
                <div class="form-group">
                    <label for="tipe" class="col-sm-4 col-md-3 control-label">Nama ODP</label>
                    <div class="col-sm-8">
                        <input name="nama_odp" type="text" id="nama_odp" class="form-control input-sm" value="{{ @$data->nama_odp }}"/>
                    </div>
                </div>
                <div class="form-group {{ $errors->has('headline') ? 'has-error' : '' }}">
                    <label for="txtPin" class="col-sm-4 col-md-3 control-label">Headline</label>
                    <div class="col-sm-8">
                        <textarea name="headline" rows="4" class="form-control">{{ @$data->catatan }}</textarea>
                        @foreach($errors->get('headline') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>

                <!--
                <div class="form-group">
                    <label for="tipe" class="col-sm-4 col-md-3 control-label">PIC</label>
                    <div class="col-sm-8">
                        <input name="pic" type="text" id="pic" class="form-control input-sm" value="{{ old('pic') }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="koordinat" class="col-sm-4 col-md-3 control-label">Koordinat</label>
                    <div class="col-sm-7">
                        <input name="koordinat" type="text" id="koor" class="form-control" value="{{ old('koordinat') ?: @$data->koordinat }}"/>
                    </div>
                </div>
                -->
                <div class="form-group {{ $errors->has('koordinat') ? 'has-error' : '' }}">
                    <label for="" class="col-sm-4 col-md-3 col-xs-12 control-label">Koordinat</label>
                    <div class="col-sm-8 col-xs-8">
                        <input name="koordinat" id="input-koordinat" class="form-control input-sm" rows="1" value="{{ @$data->kordinat_odp }}" />
                        @foreach($errors->get('koordinat') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                    <!-- <div class="col-sm-2 col-xs-3" align="right">
                        <button id="btnLoadMap" title="Beri tanda pada Peta" class="btn btn-default btn-sm" data-toggle="tooltip" type="button">
                            <i class="fa fa-map-marker"></i>
                        </button>
                    </div> -->
                </div>
                @if(in_array($auth->maintenance_level, [1, 7]))
                    <div class="form-group {{ $errors->has('regu') ? 'has-error' : '' }}">
                        <label for="regu" class="col-sm-4 col-md-3 control-label">Order ke Tim</label>
                        <div class="col-sm-8">
                            <input name="regu" type="text" id="regu" class="form-control input-sm" value="{{ old('regu') }}"/>
                            @foreach($errors->get('regu') as $msg)
                                <span class="help-block">{{ $msg }}</span>
                            @endforeach
                        </div>
                    </div>
                @endif
                <div class="form-group {{ $errors->has('sto') ? 'has-error' : '' }}">
                    <label for="sto" class="col-sm-4 col-md-3 control-label">STO</label>
                    <div class="col-sm-8">
                        <input name="sto" type="text" id="sto" class="form-control input-sm" value="{{ old('sto') ?: @$data->Workzone }}"/>
                        @foreach($errors->get('sto') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-3 col-sm-3">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
            </div>
            <div>
                <?php
                $odp = 'tommanpsb/evidence/'.Request::segment(2).'/ODP.jpg';
                if (file_exists($odp)){
                  echo'<div class="col-xs-4 col-sm-4"><a href="/tommanpsb/evidence/'.Request::segment(2).'/Redaman_ODP.jpg">
                  <img src="/'.$odp.'"" style="width:100px;height:150px;" />
                  </a><br/>REDAMAN ODP</div>';
                }
            ?>
            </div>
        </div>
    </div>
    <div id="mapModal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        Koordinat ODP (<span id="lonText">0</span>, <span id="latText">0</span>)
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="mapView" style="height:350px;"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button id="btnGetMarker" class="btn btn-primary">OK</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSn96DCIJdATC6AHuV3sLF3ddwdaIsW10"></script>
    <script src="/js/mapmarkerlonlat.js"></script>
    <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
    <script>
        $(function() {
            var usedNames = {};
            $("select[name='jenis_order'] > option").each(function () {
                if (usedNames[this.value]) {
                    $(this).remove();
                } else {
                    usedNames[this.value] = this.text;
                }
            });
            var data = <?= json_encode($sto) ?>;
            var combo = $('#sto').select2({
                data: data,
                placeholder:"Pilih STO"
            });
            var data = <?= json_encode($regu) ?>;
            var regu = $('#regu').select2({
                data: data,
                placeholder:"Pilih Regu"
            });
            $("#nama_odp").inputmask("AAA-AAA-A{2,3}/9{3,4}");
        });
    </script>
@endsection