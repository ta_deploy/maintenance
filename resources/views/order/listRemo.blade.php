@extends('layout2')
@section('head')
    <style type="text/css">
        .container {
            width: 100%;
            margin: 5px;
        }
        .table-condensed{
          font-size: 10px;
        }

    </style>
    <link rel="stylesheet" href="/bower_components/select2/select2.css" />
@endsection
@section('content')
<div class="panel panel-default" id="info">
    <div class="panel-heading">LIST</div>
    <div class="panel-body table-responsive">
        <table class="table table-bordered table-striped" id="table">
            <tr>
                <th>#</th>
                <th>NO TIKET</th>
                <th>ODP</th>
                <th>ALAMAT</th>
                <th>STO</th>
                <th>NODE ID</th>
                <th>ONU TYPE</th>
                <th>ONU SN</th>
                <th>STATUS</th>
            </tr>

            @foreach($data as $no => $d)
                <?php
                    $dteStart = new \DateTime($d->created_at);
                    if($d->tgl_selesai)
                        $dteEnd   = new \DateTime($d->tgl_selesai);
                    else
                        $dteEnd   = new \DateTime(date('Y-m-d H:i:s'));
                    $dteDiff  = $dteStart->diff($dteEnd);
                    $durasi   = $dteDiff->format("%Dd %Hh");
                ?>
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>
                        @if($d->no_tiket)
                            <a href="/tech/{{$d->id_mt}}">{{ $d->NO_SPEEDY }}</a>
                        @else
                            {{ $d->NO_SPEEDY }}
                        @endif
                        <br/>
                        <span>
                            @if (!is_null($d->check_regu) && session('auth')->maintenance_level == 0 || session('auth')->maintenance_level != 0)
                                <button class="btn btn-xs btn-success button_check" id-disp="{{ $d->NO_SPEEDY }}">{{ empty($d->dispatch_regu_id) ? 'Dispatch' : 'Re-Dispatch' }}</button>
                            @endif
                        </span>
                    </td>
                    <td><span class="label label-primary">{{ $d->DP }}</span></td>
                    <td>{{ $d->ALAMAT }}<br/>{{ $d->dispatch_regu_name }}<br/>{{ $d->status_name }} {{ $d->tgl_selesai }}</td>
                    <td>{{ $d->sto ?: $d->CMDF}}</td>
                    <td>{{ $d->NODE_ID }}</td>
                    <td>{{ $d->ONU_TYPE }}</td>
                    <td>{{ $d->ONU_SN }}</td>
                    <td>{{ $d->STATUS }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<div id="mapModal" class="modal fade">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    Kirim Tiket <span id="wo" class="label label-primary"></span> ke Teknisi
                </h4>
            </div>
            <div class="modal-body">
                <div>
                    <input name="id_maint" type="hidden" id="id_maint" class="form-control" rows="1" value="" />
                    <div class="form-group">
                        <label for="jenis_order" class="col-sm-4 col-md-3 control-label">Order ID</label>
                        <input name="no_tiket" id="no_tiket" class="form-control input-sm" rows="1" value="" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="jenis_order" class="col-sm-4 col-md-3 control-label">Regu</label>
                        <input type="hidden" name="regu" id="regu" class="form-control" rows="1" value="" placeholder="Pilih Regu"/>
                    </div>
                    <div class="form-group">
                        <label for="sto" class="col-sm-4 col-md-3 control-label">STO</label>
                        <input type="hidden" name="sto" type="text" id="sto" class="form-control" value=""/>
                    </div>
                    <div class="form-group">
                        <label for="koordinatodp" class="col-sm-4 col-md-3 control-label">Koordinat ODP</label>
                        <input name="koordinatodp" id="koordinatodp" class="form-control input-sm" rows="1" value=""/>
                    </div>
                    <div class="form-group">
                        <label for="pic" class="col-sm-4 col-md-3 control-label">PIC</label>
                        <input name="pic" id="pic" class="form-control input-sm" rows="1" value=""/>
                    </div>
                    <div class="form-group">
                        <label for="alamat" class="col-sm-4 col-md-3 control-label">Alamat</label>
                        <input name="alamat" id="alamat" class="form-control input-sm" rows="1" value=""/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default btn-sm" data-dismiss="modal">Batal</button>
                <button id="hideButton" id-dispatch="" class="btn btn-primary btn-sm">OK</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="/bower_components/select2/select2.min.js"></script>
<script type="text/javascript">
    $(function() {
        var nt = 0;
        var id_maintenance = 0;
        var sto = 0;
        var tim = 0;
        var pic = 0;
        var alamat = 0;
        var koordinatodp = 0;
        var data = <?= json_encode($regu) ?>;
        var combo = $('#regu').select2({
            data: data,
            formatResult: function(data) {
                return    '<span class="label label-default">'+data.text+'</span>'+
                        '<strong style="margin-left:5px">'+data.nama1+'&'+data.nama2+'</strong>';
            }
        });
        var data = <?= json_encode($sto) ?>;
        var sto = $('#sto').select2({
            data: data
        });
        $(".button_check").click(function(event) {
            nt = this.getAttribute("id-disp");
            $("#sto").val(0).trigger('change');
            $("#hideButton").attr("id-dispatch", nt);
            $("#no_tiket").val(nt);
            $("#wo").html(data.nt);
            $("#koordinatodp").val('');
            $("#pic").val('');
            $("#alamat").val('');
            $("#regu").val('').trigger('change');
            $("#sto").val('').trigger('change');
            $.getJSON( "/jsonOrderRemo/"+nt, function(data) {
                id_maintenance = data.id;
                tim = data.dispatch_regu_id;
                sto = data.sto;
                $("#id_maint").val(data.id);
                $("#koordinatodp").val(data.koordinat);
                $("#pic").val(data.pic);
                $("#alamat").val(data.alamat);
                $("#regu").val(data.dispatch_regu_id).trigger('change');
                $("#sto").val(data.sto).trigger('change');
            })
            .done(function(data) {
                $("#hideButton").show();
            });
            $('#mapModal').modal({show:true});
        });
        $("#hideButton").click(function(event) {
            tim = $("#regu").val();
            sto = $("#sto").val();
            pic = $("#pic").val();
            alamat = $("#alamat").val();
            no_tiket = $("#no_tiket").val();
            koordinatodp = $("#koordinatodp").val();
            var msg = "";
            if(!tim)
                msg += "REGU isii uuuh!\n";
            if(!sto)
                msg += "STO isii uuuh!\n";
            if(!pic)
                msg += "pic isii uuuh!\n";
            if(!alamat)
                msg += "alamat isii uuuh!\n";
            if(!koordinatodp)
                msg += "koordinat odp isii uuuh!\n";
            if(msg){
                alert(msg);
                event.preventDefault();
            }else{
                $.ajax({
                  type: "POST",
                  url: "/dispatchOrderRemo",
                  data: { no_tiket: no_tiket, regu: tim, sto: sto, pic: pic, alamat: alamat, koordinatodp: koordinatodp },
                  dataType: "html"
                }).done(function(data) {
                    $("button[id-disp='"+nt+"']").parent().html(data);
                });
                $('#mapModal').modal('toggle');
            }
        });
    });
</script>
@endsection