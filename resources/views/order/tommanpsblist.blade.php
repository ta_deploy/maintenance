@extends('layout2')
@section('head')
    <style type="text/css">
        .container {
            width: 100%;
            margin: 5px;
        }
        .table-condensed{
          font-size: 10px;
        }
        .head{
            color: #303030;
        }
    </style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>List ORDER</span>
</h1>
@endsection
@section('content')
    <div class="panel panel-default" >
        
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>#</th>
                    <th>JENIS ORDER / Headline</th>
                    <th>NO TIKET / By</th>
                    <th>ODP/Pelanggan</th>
                    <th>Jenis</th>
                </tr>
                <?php
                    //dd($data);
                ?>
                @foreach($data as $no => $d)
                    
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td><span class="label label-default">No Tiket</span> <a href="/listtommanpsb/{{ Request::segment(2) }}/{{ $d->id_dispatch }}">{{ $d->no_tiket }}</a><br/>
                            <span class="label label-default">Laporan Dari</span>{{ $d->dispatch_regu_name }}<br/>
                            <a href="/tpsbcutoff/{{ $d->id_dispatch }}/{{ Request::segment(2) }}" class="btn btn-info btn-xs" style="margin: 5px 0 5px;">
                                <span>Cut Off</span>
                            </a>
                            @if(@$d->checked_by)
                            <a href="/tpsbfixdispatch/{{ $d->id_dispatch }}/{{ Request::segment(2) }}" class="btn btn-success btn-xs pull-right" style="margin: 5px 0 5px;">
                                <span>Fix Dispatch</span>
                            </a>
                            @endif
                        </td>
                        <td><span class="label label-default">Order</span>{{ @$d->laporan_status }}<br/>
                            <span class="label label-default">Headline</span>{{ $d->headline }}<br/>
                            <span class="label label-default">Action</span>{{ @$d->aksi }}<br/>
                            <span class="label label-default">Laporan@</span>{{ @$d->created_at }}</td>
                        <td>
                            <span class="label label-default">Nama Odp</span>{{ $d->nama_odp }}</br>
                            <span class="label label-default">Koordinat Odp</span>{{ @$d->kordinat_odp }}</br>
                            <span class="label label-default">Koordinat Plg</span>{{ @$d->kordinat_pelanggan }}</br>
                            <span class="label label-default">Redaman</span>{{ @$d->redaman }}
                        </td>
                        <td>
                            {{ $d->jenis_psb }}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection

@section('script')
@endsection