@extends('layout2')
@section('head')
<link rel="stylesheet" href="/bower_components/select2/select2.css" />
<link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
@endsection
@section('content')
<div class="panel panel-default" id="info">
    <div class="panel-heading">NEED VERIFIKASI</div>
    <div class="panel-body table-responsive">
        <table class="table table-bordered table-striped" id="table">
            <tr>
                <th>#</th>
                <th>NO TIKET</th>
                <th>ACTION</th>
                <th>UMUR</th>
                <th>SELESAI</th>
                <th>REGU</th>   
                <th>STO</th>
                <th>DATEL</th>
                <th>ORDER DARI</th>
                <th>SEGMENT</th>
            </tr>
            @foreach($data as $no => $d)
            <?php
            $dteStart = new \DateTime($d->created_at);
            if($d->tgl_selesai)
                $dteEnd   = new \DateTime($d->tgl_selesai);
            else
                $dteEnd   = new \DateTime(date('Y-m-d H:i:s'));
            $dteDiff  = $dteStart->diff($dteEnd); 
            $durasi   = $dteDiff->format("%Dd %Hh");
            ?>
            <tr>
                <td>{{ ++$no }}</td>
                <td><a href="/tech/{{ $d->id }}">{{ $d->no_tiket }}</a><br/><span class="label label-primary">{{ $d->nama_dp or $d->nama_odp }}</span>

                    <button class="btn btn-xs btn-success button_refer" id-mt="{{ $d->id }}">Verif</button>
                </td>
                <td>{{ $d->action }}</td>
                <td>{{ $d->created_at }}/{{ $durasi }}</td>
                <td>{{ $d->tgl_selesai }}</td>
                <td>{{ $d->dispatch_regu_name }}</td>
                <td>{{ $d->sto }}</td>
                <td>{{ $d->kandatel }}</td>
                <td>{{ $d->order_from or '' }}</td>
                <td>{{ $d->nama_order }}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
<div id="referModal" class="modal fade">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <form method="post" action="/verifRemo" id="verifhd">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        Verif <span class="label label-primary wo"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div style="height:350px;">
                        <input name="id_maint" type="hidden" class="form-control id_maintenance" rows="1" value="" />
                        <div class="form-group">
                            <label for="jenis_order" class="col-sm-4 col-md-3 control-label">Tim</label>
                            <input name="no_tiket" id="tim" class="form-control input-sm" rows="1" value="" readonly/>
                        </div>
                        <div class="form-group">
                            <label for="refer" class="col-sm-4 col-md-3 control-label">Redaman</label>
                            <input name="redaman" id="refer" class="form-control" rows="1" value="" placeholder="Pilih Redaman"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default btn-sm" data-dismiss="modal">Batal</button>
                    <button id-dispatch="" class="btn btn-primary btn-sm">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="/bower_components/select2/select2.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(function() {
        $('#table').DataTable();
        $(".button_refer").click(function(event) {
            nt = this.getAttribute("id-mt");
            $.getJSON( "/jsonOrder/"+nt, function(data) {
                $("#tim").val(data.dispatch_regu_name);
                $(".id_maintenance").val(data.id);
                $(".wo").html(data.no_tiket);
            });
            var mt = [{"id":0,"text":"Tidak Aman"},{"id":1,"text":"Aman"}];
            $('#refer').select2({
                data : mt,
                formatResult: function(mt) {
                    return    '<span class="label label-default">'+mt.text+'</span>';
                }
            });
            $('#referModal').modal({show:true});
        });
    });
    $('#verifhd').submit(function (evt) {
        var redaman = $('#refer').val();
        if(redaman == ''){
            evt.preventDefault();
            alert('isi redaman!');
        }
    });
</script>
@endsection