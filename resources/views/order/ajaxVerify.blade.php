
<div class="panel-body table-responsive" style="height:420px;">
    <table class="table table-bordered table-fixed">
        <tr>
            <th class="align-middle">#</th>
            <th>ORDER</th>
            <th>JENIS ORDER</th>
            <th>REGU</th>
            <th>ACTION</th>
            <th>STO</th>
            <th>ODP</th>
            <th>TGL DISPATCH</th>
            <th>TGL SELESAI</th>
        </tr>
        @foreach($data as $no => $d)
            <tr>
                <td>{{ ++$no }}</td>
                <td><a href="/tech/{{ $d->id }}" target="_BLANK">{{ $d->no_tiket }}</a></td>
                <td>{{ $d->nama_order }}</td>
                <td>{{ $d->dispatch_regu_name }}</td>
                <td>{{ $d->action_cause }}</td>
                <td>{{ $d->sto }}</td>
                <td>{{ $d->nama_odp }}</td>
                <td>{{ $d->ts }}</td>
                <td>{{ $d->tgl_selesai }}</td>
            </tr>
        @endforeach
    </table>
</div>