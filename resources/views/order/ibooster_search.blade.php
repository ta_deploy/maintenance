@extends('layout2')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>SEARCH IBOOSTER</span>
</h1>
@endsection
@section('head')
    <link rel="stylesheet" href="/bower_components/select2/select2.css" />
    <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css" />
@endsection

@section('content')
<?php
    $auth = (session('auth'));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        Ukur Ibooster
    </div>
    <div class="panel-body">
        <form method="get" class="form-horizontal">
            <div class="form-group {{ $errors->has('order_id') ? 'has-error' : '' }}">
                <div class="col-sm-2">
                    <select class="form-control input-sm" name="jenis_srch">
                        <option value="0">Nomor Internet</option>
                        <option value="1">Nomor Tiket</option>
                    </select>
                </div>
                <div class="col-sm-9">
                    <input name="order_id" type="text" id="txtKode" class="form-control input-sm" value="{{ old('order_id') ?: @$rq['order_id'] }}"/>
                </div>
                <div class="col-sm-1">
                    <button class="btn btn-primary" type="submit">
                        <span class="glyphicon glyphicon-floppy-disk"></span>
                        <span>Cari</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@if (@$rq['order_id'])
<div class="panel panel-default">
    <div class="panel-heading">
        Hasil Pencarian Sebanyak ({{ count($data) }}) Buah
    </div>
    @if ($data)
    <div class="panel-body">
        <table class="table table-bordered table-striped" id="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>NO TIKET</th>
                    <th>NO INET</th>
                    <th>HEADLINE</th>
                    <th>STATUS</th>
                    <th>UKURAN IBOOSTER</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $key => $d)
                <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $d->no_tiket }}</td>
                    <td>{{ $d->no_inet }}</td>
                    <td>{{ $d->headline }}</td>
                    <td>{{ $d->status ?: 'Tidak Ada' }}</td>
                    <td>{{ $d->iboost }}</td>
                    <td>
                        @if ($d->status == 'Nonaktif')
                        <a href="/ibooster/status/{{ $d->status_name }}/{{ $d->id }}" type="button" class="btn btn-sm btn-primary">Aktif</a>
                        @else
                        <a href="/ibooster/status/Nonaktif/{{ $d->id }}" type="button" class="btn btn-sm btn-danger">Nonaktif</a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @endif
</div>
@endif
@endsection
@section('script')
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSn96DCIJdATC6AHuV3sLF3ddwdaIsW10"></script>
<script src="/js/mapmarkerlonlat.js"></script>
<script src="/bower_components/select2/select2.min.js"></script>
<script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
<script>
    $(function() {

    });
</script>
@endsection