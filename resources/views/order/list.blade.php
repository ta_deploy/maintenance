@extends('layout2')
@section('head')
{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" /> --}}
<link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
<style type="text/css">
    th{
        text-align: center;
    }

    #select2-fsto-container{
        width: 90px;
    }
    #select2-fdatel-container{
        width: 200px;
    }

    /*.select2-selection__arrow{
        display:none !important;
        }*/
    </style>
    @endsection
    @section('heading')
    <h1>
        <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>DETAIL LIST</span>
    </h1>
    @endsection
    @section('content')
<?php $authLevel = Session::get('auth')->maintenance_level ?>
@if ($authLevel == '1')
<div class="form-group row">
<!-- <div class="col-sm-2">
<a href="/order/input" class="btn btn-info btn-sm" style="margin: 0 -15px;">
<span class="glyphicon glyphicon-plus"></span>
<span>Input Order</span>
</a>
</div> -->
<div class="col-sm-1">
    <button class="btn btn-default btn-sm btnFilter" type="button">
        <span class="glyphicon glyphicon-filter">Filter</span>
    </button>
    <button class="btn btn-default btn-sm btnHide" type="button">
        <span class="glyphicon glyphicon-filter">Hide</span>
    </button>
</div>
<div class="col-sm-2 filter">
    <input name="sd" type="text" id="sd" class="form-control input-sm" placeholder="Start Date" />
</div>
<input name="jenis" type="hidden" id="jenis" class="form-control" value="{{ Request::segment(2) }}" />
<div class="col-sm-4 filter">
    <select name="sts" id="sts" class="form-control"/></select>
</div>
<div class="col-sm-2 input-group filter">
    <span class="input-group-btn">
        <input name="ed" type="text" id="ed" class="form-control input-sm" placeholder="End Date" />
        <button class="btn btn-default btn-sm" type="button" id="btnFilter">
            <span class="glyphicon glyphicon-filter">Filter</span>
        </button>
    </span>
</div>
</div>
@endif
<br />
<div class="panel panel-default" id="info">
    <div class="panel-heading">LIST</div>
    <div class="panel-body table-responsive">
        <table class="table table-bordered table-striped" id="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>NO TIKET</th>
                    <th>NAMA ODP</th>
                    <th>IMPACT</th>
                    <th>ACTION</th>
                    <th>STATUS</th>
                    <th>HEADLINE</th>
                    <th>ORDER PLAN</th>
                    <th>UMUR</th>
                    <th>SELESAI</th>
                    <th><select name="fregu" id="fregu" class="form-control"></select></th>
                    <th><select name="fsto" id="fsto" class="form-control"></select></th>
                    <th><select name="fdatel" id="fdatel" class="form-control"></select></th>
                    <th>ORDER DARI</th>
                    @if (in_array(Request::segment(3), ['close', 'close_val',]) && Request::Segment(1) == 'odpLossC')
                    <th>Jumlah Valins</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                <tr style="display: none">
                    <td colspan="20">Data Kosong</td>
                </tr>
                @foreach($data as $no => $d)
                <?php
                $dteStart = new \DateTime($d->created_at);
                if($d->tgl_selesai)
                    $dteEnd   = new \DateTime($d->tgl_selesai);
                else
                    $dteEnd   = new \DateTime(date('Y-m-d H:i:s'));
                $dteDiff  = $dteStart->diff($dteEnd);
                $durasi   = $dteDiff->format("%Dd %Hh %im");
                ?>
                <tr>
                    <td>{{ ++$no }}</td>
                    <td><a href="/tech/{{ $d->id }}">{{ $d->no_tiket }}</a><br /><span
                            class="label label-primary">{{ $d->nama_dp or $d->nama_odp }}</span>
                        @if($d->status != 'close')
                        <br />
                        <span>
                            <button class="btn btn-xs btn-success button_refer" id-mt="{{ $d->id_mt }}">Refer</button>
                            @if (!is_null($d->check_regu) && session('auth')->maintenance_level == 0 || session('auth')->maintenance_level != 0)
                                <button class="btn btn-xs btn-success button_check" id-disp="{{ $d->id_mt }}">{{ empty($d->dispatch_regu_id) ? 'Dispatch' : 'Re-Dispatch' }}</button>
                                <button class="btn btn-xs btn-danger button_delete" id-mt="{{ $d->id_mt }}">Delete</button>
                            @endif
                        </span>
                        @endif
                        <br /><span class="label label-primary">{{ $d->nama_order }}</span>
                    </td>
                    <td>{{ $d->nama_dp }}</td>
                    <td>
                        <p class="anak_tiket" id-mtt="{{ $d->id_mt }}" style="color:#0ce3ac;text-decoration:underline;">
                            {{ $d->jml_anak }} Tiket</p>
                        <div id-mtt="{{ $d->id_mt }}"></div>
                    </td>
                    <td>{{ $d->action }}</td>
                    <td>{{ $d->status }}</td>
                    <td>{{ $d->headline }}</td>
                    <td>{{ $d->timeplan }}</td>
                    <td>{{ $d->created_at }} / {{ $durasi }}</td>
                    <td>{{ $d->tgl_selesai }}</td>
                    <td><span class="nama_regu">{{ $d->dispatch_regu_name }}</span>
                        <br />
                        <span class="label label-success">{{ $d->nama1 }}&{{ $d->nama2 }}</span>
                        <br />
                        <span class="label label-info">{{ $d->koordinat }}</span>
                    </td>
                    <td>{{ $d->sto }}</td>
                    <td>{{ $d->kandatel }}</td>
                    <td>{{ $d->order_from }}</td>
                    @if (in_array(Request::segment(3), ['close', 'close_val',]) && Request::Segment(1) == 'odpLossC')
                    <td>{{ $d->kaps_vals }}</td>
                    @endif
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
    <div id="mapModal" class="modal fade">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        Kirim Tiket <span id="wo" class="label label-primary"></span> ke Teknisi
                    </h4>
                </div>
                <div class="modal-body">
                    <div style="height:350px;">
                        <input name="id_maint" type="hidden" id="id_maint" class="form-control" rows="1" value="" />
                        <div class="form-group">
                            <label for="jenis_order" class="col-sm-4 col-md-3 control-label">Order ID</label>
                            <input name="no_tiket" id="no_tiket" class="form-control input-sm" rows="1" value="" readonly/>
                        </div>
                        <div class="form-group">
                            <label for="jenis_order" class="col-sm-4 col-md-3 control-label">Regu</label>
                            <select name="regu" id="regu" class="form-control"></select>
                        </div>
                        <div class="form-group">
                            <label for="sto" class="col-sm-4 col-md-3 control-label">STO</label>
                            <select name="sto" id="sto" class="form-control"></select>
                        </div>
                        <div class="form-group">
                            <label for="odp" class="col-sm-4 col-md-3 control-label">ODP</label>
                            <input name="odp" id="odp" class="form-control input-sm" rows="1" value="" readonly/>
                        </div>
                        <div class="form-group">
                            <label for="koordinat" class="col-sm-4 col-md-3 control-label">Koordinat</label>
                            <input name="koordinat" id="koordinat" class="form-control input-sm" rows="1" value="" readonly/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default btn-sm" data-dismiss="modal">Batal</button>
                    <button id="hideButton" id-dispatch="" class="btn btn-primary btn-sm">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div id="referModal" class="modal fade">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <form method="post" action="/refer">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">
                            Refer Tiket <span class="label label-primary wo"></span>
                        </h4>
                    </div>
                    <div class="modal-body bodyrefer">
                        <div style="height:350px;">
                            <input name="id_maint" type="hidden" class="form-control id_maintenance" rows="1" value="" />
                            <div class="form-group">
                                <label for="jenis_order" class="col-sm-4 col-md-3 control-label">Order ID</label>
                                <input name="no_tiket" id="tiket_no" class="form-control input-sm" rows="1" value="" readonly/>
                            </div>
                            <div class="form-group">
                                <label for="refer" class="col-sm-4 col-md-3 control-label">Refer ke</label>
                                <select name="refer" id="refer" class="select-style"></select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default btn-sm" data-dismiss="modal">Batal</button>
                        <button id-dispatch="" class="btn btn-primary btn-sm">OK</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @endsection
    @section('script')
    <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(function() {
            var nt = 0;
            var id_maintenance = 0;
            var sto = 0;
            var tim = 0;
            $("#fdatel").change(function() {
                var filter, table, tr, td, i;
                filter = $( "#fdatel option:selected" ).text();
                table = document.getElementById("table");
                tr = table.getElementsByTagName("tr");

// Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    if(filter=="ALL"){
                        tr[i].style.display = "";
                    }else{
                        td = tr[i].getElementsByTagName("td")[12];
                        if (td) {
                            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                                tr[i].style.display = "";
                            } else {
                                tr[i].style.display = "none";
                            }
                        }
                    }
                }
                table.focus();
            });
            $("#fregu").change(function() {
                var filter, table, tr, td, rg, i;
                filter = $( "#fregu option:selected" ).text();
                table = document.getElementById("table");
                tr = table.getElementsByTagName("tr");
// Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    if(filter=="ALL"){
                        tr[i].style.display = "";
                    }else{
                        td = tr[i].getElementsByTagName("td")[10];
                        if (typeof td !== "undefined"){
                            rg = td.getElementsByClassName('nama_regu')[0];
                        }
                        if (td) {
                            if (rg && rg.innerHTML.toUpperCase().indexOf(filter) > -1) {
                                tr[i].style.display = "";
                            } else {
                                tr[i].style.display = "none";
                            }
                        }
                    }
                }
            });
            $("#fsto").change(function() {
                var filter, table, tr, td, rg, i;
                filter = $( "#fsto option:selected" ).text();
                table = document.getElementById("table");
                tr = table.getElementsByTagName("tr");
// Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    if(filter=="ALL"){
                        tr[i].style.display = "";
                    }else{
                        td = tr[i].getElementsByTagName("td")[11];
                        if (td) {
                            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                                tr[i].style.display = "";
                            } else {
                                tr[i].style.display = "none";
                            }
                        }
                    }
                }
            });
            $("#hideButton").hide();
            $(".button_check").click(function(event) {
                nt = this.getAttribute("id-disp");
                $("#sto").val(0).trigger('change');
                $("#hideButton").attr("id-dispatch", nt);
                $.getJSON( "/jsonOrder/"+nt, function(data) {
                    id_maintenance = data.id;
                    tim = data.dispatch_regu_id;
                    sto = data.sto;
                    $("#id_maint").val(data.id);
                    $("#wo").html(data.nama_order);
                    $("#no_tiket").val(data.no_tiket);
                    $("#regu").val(data.dispatch_regu_id).trigger('change');
                    $("#sto").val(data.sto).trigger('change');
                    if(data.koordinat)
                        $("#koordinat").val(data.koordinat);
                    else
                        $("#koordinat").val(data.kordinat_odp);
                    if(data.nama_dp)
                        $("#odp").val(data.nama_dp);
                    else
                        $("#odp").val(data.nama_odp);
                })
                .done(function(data) {
                    $("#hideButton").show();
                });

                $('#mapModal').modal({show:true});
            });

            $("#hideButton").click(function(event) {
                tim = $("#regu").val();
                sto = $("#sto").val();
                $.ajax({
                    type: "POST",
                    url: "/dispatchOrder",
                    data: { id_mt: id_maintenance, regu: tim, sto: sto},
                    dataType: "html"
                }).done(function(data) {
                    $("button[id-disp='"+nt+"']").parent().html(data);
                });
                $('#mapModal').modal('toggle');
            });
            var data = <?= json_encode($regu) ?>;
            var combo = $('#regu').select2({
                dropdownParent: $('#mapModal'),
                data: data,
                escapeMarkup: function(markup) {
                    return markup;
                },
                templateResult: function(data) {
                    var tim = [],
                    judul_tim;
                    if(data.nama1){
                        tim.push(data.nama1);
                    }
                    if(data.nama2){
                        tim.push(data.nama2)
                    }
                    judul_tim = tim.join(" & ");
                    return '<span class="label label-default">'+data.text+'</span>'+
                    '<strong style="margin-left:5px">'+judul_tim+'</strong>';
                },
                templateSelection: function(data) {
                    return data.text;
                }
            });
            var data = <?= json_encode($sto) ?>;
            var sto = $('#sto').select2({
                dropdownParent: $('#mapModal'),
                placeholder: 'STO',
                data: data
            });
            var fsto = $('#fsto').select2({
                placeholder: 'STO',
                width: '100%',
                allowClear: true,
                data: data
            });
            var data = <?= json_encode($datel) ?>;
            data.push({"id":"all", "text":"ALL"});

            var fdatel = $('#fdatel').select2({
                placeholder: 'DATEL',
                allowClear: true,
                data: data
            });

            $('#fdatel').val('').trigger('change');
            var data = <?= json_encode($regu) ?>;
            data.push({"id":"all", "text":"ALL"});

            var fregu = $('#fregu').select2({
                placeholder: 'REGU',
                allowClear: true,
                data: data
            });
            $('#fregu, #fsto').val('').trigger('change');
            $(".button_delete").click(function(event) {
                mt = this.getAttribute("id-mt");
                var r = confirm("Yakin lah pian handak menDELETE order ngini?"+mt);
                if (r == true) {
                    $.ajax({
                        type: "POST",
                        url: "/deleteOrder",
                        data: { id_mt: mt},
                        dataType: "html"
                    }).done(function(data) {
                        $("button[id-disp='"+mt+"']").parent().html(data);
                    });
                }
            });

            var shtml="";

            $(".button_refer").click(function(event) {
                nt = this.getAttribute("id-mt");
                $("#tiket_no").val(nt);
                $.getJSON( "/jsonOrder/"+nt, function(data) {
                    $(".id_maintenance").val(data.id);
                    $(".wo").html(data.no_tiket);
                });

                $('#referModal').modal({show:true});
            });

            $( "#referModal" ).on('show.bs.modal', function(){
               /* function formatItem (item) {
                    if (!item.no_tiket) {
                        return item.no_tiket;
                    }
                    return $('<div><span class="label label-default">'+item.no_tiket+'</span>'+item.nama_order+'<br><span class="label label-primary">'+item.status+'</span>'+'<br><span class="label label-primary">'+item.nama_odp+'</span></div>');
                }

                function tes(item){
                    return item.no_tiket;
                }*/
                $('#refer').select2({
                    placeholder:"Masukkan Nomor Tiket",
                    containerCssClass: 'refer_cls',
                    dropdownParent: $('#referModal'),
                    minimumInputLength: 4,
                    ajax: {
                        url: "/ajax/refer_search",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                searchTerm: params.term,
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function(markup) {
                        return markup;
                    },
                    templateResult: function(data) {
                        return data.text;
                    },
                    templateSelection: function(data) {
                        return data.title;
                    }
                });

                $('.refer_cls .select2-selection__rendered .select2-selection__placeholder').text('Masukkan Nomor Tiket');
            });

            $(".anak_tiket").click(function() {
                nt = this.getAttribute("id-mtt");
                shtml="";
                $(this).slideUp();
                $.getJSON( "/jsonAnak/"+nt, function(data){
                    $.each(data, function(i, item) {
                        shtml += "<span class='label label-primary'>"+item.no_tiket+"</span><br/>";
                    });
                    $("div[id-mtt='"+nt+"']").html(shtml);
                });
            });
            $('.btnrmrow').hide();
            $(".btnnewrow").click(function(e){
                var $this = $(this);
                var mt = this.getAttribute("data-jenisorder");
                var dd = this.getAttribute("data-datel");
                var tglnav = $('#tglnav').val();
                $.ajax({
                    type: "GET",
                    url: "/ajaxOrder/"+mt+"/"+dd+"/"+tglnav,
                    dataType: "html"
                }).done(function(data) {
                    $('.btnrmrow').show();
                    $('.btnnewrow').hide();
                    $this.closest('tr').after(data);
                });
            });
            $(".btnrmrow").click(function(e){
                $('.ajax').remove();
                $('.btnrmrow').hide();
                $('.btnnewrow').show();
            });
            var data = [{"id":"all", "text":"ALL"},{"id":"close", "text":"close"},{"id":"kendala pelanggan", "text":"kendala pelanggan"},{"id":"kendala teknis", "text":"kendala teknis"}];
            var sts = $('#sts').select2({
                placeholder: 'Status',
                allowClear: true,
                data: data
            });
            $('#sts').val('').trigger('change');
            var day = {
                format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0
            };
            $("#sd").datepicker(day).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
            $("#ed").datepicker(day).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
            $(".filter").hide();
            $(".btnHide").hide();
            $(".btnFilter").click(function() {
                $(".filter").show();
                $(".btnHide").show();
                $(".btnFilter").hide();
            });
            $(".btnHide").click(function() {
                $(".filter").hide();
                $(".btnHide").hide();
                $(".btnFilter").show();
            });
            $("#btnFilter").click(function() {
                var sd = $("#sd").val();
                var jenis = $("#jenis").val();
                var ed = 0;
                if($("#ed").val())
                    ed = $("#ed").val();
                var sts = 0;
                if($("#sts").val())
                    sts = $("#sts").val();
                if(sd){
                    $('.wait-indicator').show();
                    $.get("/filter/"+sd+"/"+ed+"/"+sts+"/"+jenis, function( data ) {
                        $("#info").html(data);
                        $('.wait-indicator').hide();
                    });
                }else{
                    alert("Isi Start Date BosQue!")
                }
            });
        });
    </script>
    @endsection