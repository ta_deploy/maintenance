@extends('layout2')
@section('head')
<style type="text/css">
    th{
        text-align: center;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>MATRIK {{ date('Y-m-d') }}</span>
</h1>
@endsection
@section('content')
<form style="margin-bottom: 20px;" role="search" action="/order/none/none/{{ Request::segment(3) }}/all">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Search Order" name="q"/>
        <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
                <span class="px-nav-icon fa fa-search"></span>
            </button>
        </span>
    </div>
</form>
<?php $authLevel = Session::get('auth')->maintenance_level ?>
@if ($authLevel == '1')
<div class="form-group row">
<!-- <div class="col-sm-2">
<a href="/order/input" class="btn btn-info btn-sm" style="margin: 0 -15px;">
<span class="glyphicon glyphicon-plus"></span>
<span>Input Order</span>
</a>
</div> -->
<div class="col-sm-1">
    <button class="btn btn-default btnFilter" type="button">
        <span class="glyphicon glyphicon-filter">Filter</span>
    </button>
    <button class="btn btn-default btnHide" type="button">
        <span class="glyphicon glyphicon-filter">Hide</span>
    </button>
</div>
<div class="col-sm-2 filter">
    <input name="sd" type="text" id="sd" class="form-control" placeholder="Start Date" />
</div>
<input name="jenis" type="hidden" id="jenis" class="form-control" value="{{ Request::segment(2) }}" />
<div class="col-sm-4 filter">
    <input name="sts" type="text" id="sts" class="form-control" placeholder="Status" />
</div>
<div class="col-sm-2 input-group filter">
    <span class="input-group-btn">
        <input name="ed" type="text" id="ed" class="form-control" placeholder="End Date" />
        <button class="btn btn-default" type="button" id="btnFilter">
            <span class="glyphicon glyphicon-filter">Filter</span>
        </button>
    </span>
</div>
</div>
@endif
<br />
<input name="tglnav" id="tglnav" type="hidden" value="{{Request::segment(3)}}"/>
<div class="panel panel-default" id="info">
    <div class="panel-body table-responsive">
        <table class="table table-bordered table-fixed table-striped">
            <tr>
                <th rowspan=2 class="align-middle">JENIS ORDER</th>
                <th colspan=2>ALL</th>
                <th colspan=3>MTD</th>
            </tr>
            <tr>
                
                <th>NO<br/>DISPATCH</th>
                <th>NO<br/>UPDATE</th>
                <th>KENDALA<br/>PELANGGAN</th>
                <th>KENDALA<br/>TEKNIS</th>
                <th class="align-middle">CLOSE</th>
                <!--<th class="align-middle">TOTAL</th>-->
            </tr>
            <?php
            $total = 0;
            $undisp = 0;
            $no_update = 0;
            $kendala_pelanggan = 0;
            $kendala_teknis = 0;
            $close = 0;
            $stotal = 0;
            ?>
            @foreach($data as $no => $d)
            <?php

            $total = $d->undisp+$d->no_update+$d->kendala_pelanggan+$d->kendala_teknis+$d->close;
            $undisp += $d->undisp;
            $no_update += $d->no_update;
            $kendala_pelanggan += $d->kendala_pelanggan;
            $kendala_teknis += $d->kendala_teknis;
            $close += $d->close;
            $stotal += $total;
            ?>
            <tr>
                <td class="col-md-5"><a href="#" class="btnnewrow" data-jenisorder="{{ $d->id }}" data-datel="all">{{ $d->jenis_order }}</a>
                    <a href="#" class="btnrmrow">{{ $d->jenis_order }}</a>
                <!-- @if($d->jenis_order == "REMO")
                <a href="/listRemo/all">Lookup</a>
                @endif -->
            </td>
            @if($d->jenis_order=="REMO")
            <td class="col-md-1 text-right"><a href="/listRemo/sisa">{{-- {{ $d->order_remo }} --}} 0</a></td>
            @else
            <td class="col-md-1 text-right"><a href="/order/{{$d->id}}/undisp/all/all">{{ $d->undisp }}</a></td>
            @endif
            <td class="col-md-1 text-right"><a href="/order/{{$d->id}}/null/all/all">{{ $d->no_update }}</a></td>
            <td class="col-md-1 text-right"><a href="/order/{{$d->id}}/kendala pelanggan/{{ Request::segment(3) }}/all">{{ $d->kendala_pelanggan }}</a></td>
            <td class="col-md-1 text-right"><a href="/order/{{$d->id}}/kendala teknis/{{ Request::segment(3) }}/all">{{ $d->kendala_teknis }}</a></td>
            <td class="col-md-1 text-right"><a href="/order/{{$d->id}}/close/{{ Request::segment(3) }}/all">{{ $d->close }}</a></td>
            <!--<td class="col-md-1"><a href="/order/{{$d->id}}/all">{{ $total }}</a></td>-->
        </tr>
        @endforeach
        <tr>
            <td colspan="1">TOTAL STATUS</td>
            <td class="text-right"><a href="/order/all/undisp/all/all">{{ $undisp }}</a></td>
            <td class="text-right"><a href="/order/all/null/all/all">{{ $no_update }}</a></td>
            <td class="text-right"><a href="/order/all/kendala pelanggan/all/all">{{ $kendala_pelanggan }}</a></td>
            <td class="text-right"><a href="/order/all/kendala teknis/all/all">{{ $kendala_teknis }}</a></td>
            <td class="text-right"><a href="/order/all/close/all/all">{{ $close }}</a></td>
            <!--<td><a href="/order/all/all">{{ $stotal }}</a></td>-->
        </tr>
    </table>
</div>
</div>   
@endsection
@section('script')
<script>
    $(function() {
        setInterval(function() {

            var currentTime = new Date();
            var hours = currentTime.getHours();
            var minutes = currentTime.getMinutes();
            var seconds = currentTime.getSeconds();

        // Add leading zeros
            hours = (hours < 10 ? "0" : "") + hours;
            minutes = (minutes < 10 ? "0" : "") + minutes;
            seconds = (seconds < 10 ? "0" : "") + seconds;

        // Compose the string for display
            var currentTimeString = hours + ":" + minutes + ":" + seconds;

            $(".clock").html(currentTimeString);

        }, 1000);

        var nt = 0;
        var id_maintenance = 0;
        var tim = 0;

        var data = <?= json_encode($datel) ?>;
        data.push({"id":"all", "text":"ALL"});

        var shtml="";

        $('.btnrmrow').hide();

        $(".btnnewrow").click(function(e){
            var $this = $(this);
            var mt = this.getAttribute("data-jenisorder");
            var dd = this.getAttribute("data-datel");
            var tglnav = $('#tglnav').val();
            console.log(tglnav);
            $.ajax({
                type: "GET",
                url: "/ajaxOrder/"+mt+"/"+dd+"/"+tglnav,
                dataType: "html"
            }).done(function(data) {
                $('.btnrmrow').show();
                $('.btnnewrow').hide();
                $this.closest('tr').after(data);
            });
        });

        $(".btnrmrow").click(function(e){
            $('.ajax').remove();
            $('.btnrmrow').hide();
            $('.btnnewrow').show();
        });

        var data = [{"id":"all", "text":"ALL"},{"id":"close", "text":"close"},{"id":"kendala pelanggan", "text":"kendala pelanggan"},{"id":"kendala teknis", "text":"kendala teknis"}];

        var sts = $('#sts').select2({
            data: data,
            placeholder:"Pilih Status"
        });

        var day = {
            format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0
        };
        $("#sd").datepicker(day).on('changeDate', function (e) {
            $(this).datepicker('hide');
        });

        $("#ed").datepicker(day).on('changeDate', function (e) {
            $(this).datepicker('hide');
        });

        $(".filter").hide();
        $(".btnHide").hide();
        $(".btnFilter").click(function() {
            $(".filter").show();
            $(".btnHide").show();
            $(".btnFilter").hide();
        });

        $(".btnHide").click(function() {
            $(".filter").hide();
            $(".btnHide").hide();
            $(".btnFilter").show();
        });

        $("#btnFilter").click(function() {
            var sd = $("#sd").val();
            var jenis = $("#jenis").val();
            var ed = 0;
            if($("#ed").val())
                ed = $("#ed").val();
            var sts = 0;
            if($("#sts").val())
                sts = $("#sts").val();
            if(sd){
                $('.wait-indicator').show();
                $.get("/filter/"+sd+"/"+ed+"/"+sts+"/"+jenis, function( data ) {
                    $("#info").html(data);
                    $('.wait-indicator').hide();
                });
            }else{
                alert("Isi Start Date BosQue!")
            }
        });
    });
</script>
@endsection