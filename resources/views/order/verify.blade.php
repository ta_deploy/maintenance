@extends('layout2')
@section('head')
<style type="text/css">
    th{
        text-align: center;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>MATRIX VERIFIKASI</span>
</h1>
@endsection
@section('content')
<div class="panel panel-default" id="info">
    <div class="panel-heading">MATRIK VERIFIKASI <span class="clock"></span></div>
    <div class="panel-body table-responsive">
        <table class="table table-bordered table-fixed">
            <thead>
                <tr>
                    <th style="vertical-align:middle" rowspan=2>#</th>
                    <th style="vertical-align:middle" rowspan=2>Jenis Order</th>
                    <th colspan='4'>Perlu Verifikasi</th>
                    <th colspan='2'>Approve Telkom</th>

                </tr>
                <tr>
                    <th style="background-color:#425ff4; color:black;">DECLINE TA</th>
                    <th style="background-color:#8E001C; color:white;">TA</th>
                    <th style="background-color:#425ff4; color:black;">DECLINE TELKOM</th>
                    <th style="background-color:#228B22; color:white;">TELKOM</th>
                    <th style="color:black;">MATERIAL</th>
                    <th style="color:black;">NON-MATERIAL</th>
                </tr>
            </thead>
            <?php
                $TELKOM = $DECLINETA = $DECLINET = $TA= $SIAPM= $SIAPNM=0;
            ?>
            <tbody>
                @foreach($data as $no => $d)
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $d->nama_order }}</td>
                        <td style='background-color:#425ff4;align-content:right;'>
                            <a style="color:black;" href="#" class="link_list" data-tgl="{{Request::segment(2)}}" data-status="4" data-jenis="{{ $d->id }}">{{ $d->Decline_ta_mat ?: '' }}</a>
                        </td>
                        <td style='background-color:#8E001C;align-content:right;'>
                            <a style="color:white;" href="#" class="link_list" data-tgl="{{Request::segment(2)}}" data-status="null" data-jenis="{{ $d->id }}">{{ $d->TA_mat ?: '' }}</a>
                        </td>
                        <td style='background-color:#425ff4;align-content:right;'>
                            <a style="color:black;" href="#" class="link_list" data-tgl="{{Request::segment(2)}}" data-status="5" data-jenis="{{ $d->id }}">{{ $d->Decline_t ?: '' }}</a>
                        </td>
                        <td style='background-color:#228B22;align-content:right;'>
                            <a style="color:white;" href="#" class="link_list" data-tgl="{{Request::segment(2)}}" data-status="1" data-jenis="{{ $d->id }}">{{ $d->TELKOM_mat ?: '' }}</a>
                        </td>
                        <td style='align-content:right;'>
                            <a style="color:black;" href="#" class="link_list" data-tgl="{{Request::segment(2)}}" data-status="siap_rekon_material" data-jenis="{{ $d->id }}">{{ $d->siap_rekon_mat ?: '' }}</a>
                        </td>
                        <td style='align-content:right;'>
                            <a style="color:black;" href="#" class="link_list" data-tgl="{{Request::segment(2)}}" data-status="siap_rekon_non_material" data-jenis="{{ $d->id }}">{{ $d->siap_rekon_non_mat ?: '' }}</a>
                        </td>
                    </tr>
                <?php
                    $TA        += $d->TA_mat;
                    $DECLINETA += $d->Decline_ta_mat;
                    $DECLINET  += $d->Decline_t;
                    $TELKOM    += $d->TELKOM_mat;
                    $SIAPM     += $d->siap_rekon_mat;
                    $SIAPNM    += $d->siap_rekon_non_mat;
                ?>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td style="align-content:center" colspan=2>TOTAL</td>
                    <td style="align-content:center">{{ $DECLINETA ?: '0' }}</td>
                    <td style="align-content:center">{{ $TA ?: '0' }}</td>
                    <td style="align-content:center">{{ $DECLINET ?: '0' }}</td>
                    <td style="align-content:center">{{ $TELKOM ?: '0' }}</td>
                    <td style="align-content:center">{{ $SIAPM ?: '0' }}</td>
                    <td style="align-content:center">{{ $SIAPNM ?: '0' }}</td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<div id="listModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    List
                </h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        setInterval(function() {

            var currentTime = new Date();
            var hours = currentTime.getHours();
            var minutes = currentTime.getMinutes();
            var seconds = currentTime.getSeconds();

                // Add leading zeros
                hours = (hours < 10 ? "0" : "") + hours;
                minutes = (minutes < 10 ? "0" : "") + minutes;
                seconds = (seconds < 10 ? "0" : "") + seconds;

                // Compose the string for display
                var currentTimeString = hours + ":" + minutes + ":" + seconds;

                $(".clock").html(currentTimeString);

        }, 1000);

        $(".link_list").click(function(event) {
            ds = this.getAttribute("data-status");
            dj = this.getAttribute("data-jenis");
            dt = this.getAttribute("data-tgl");
            $.get( "/ajaxVerifikasi/"+ds+"/"+dj+"/"+dt, function(data) {
                $(".modal-body").html(data);
            })
            .done(function(data) {
                console.log("success");
            });

            $('#listModal').modal({show:true});
        });
    })
</script>
@endsection