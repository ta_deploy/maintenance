@extends('layout2')

@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>NEED VERIFIKASI</span>
</h1>
@endsection
@section('content')
    <div class="panel panel-default" id="info">
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped" id="table">
                <tr>
                    <th>#</th>
                    <th>NO TIKET</th>
                    <th>IMPACT</th>
                    <th>ACTION</th>
                    <th>UMUR</th>
                    <th>SELESAI</th>
                    <th>REGU</th>
                    <th>STO</th>
                    <th>DATEL</th>
                    <th>ORDER DARI</th>
                    <th>SEGMENT</th>
                </tr>

                @foreach($data as $no => $d)
                    <?php
                        $dteStart = new \DateTime($d->created_at);
                        if($d->tgl_selesai)
                            $dteEnd   = new \DateTime($d->tgl_selesai);
                        else
                            $dteEnd   = new \DateTime(date('Y-m-d H:i:s'));
                        $dteDiff  = $dteStart->diff($dteEnd);
                        $durasi   = $dteDiff->format("%Dd %Hh");
                    ?>
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td><a href="/tech/{{ $d->id }}">{{ $d->no_tiket }}</a><br/><span class="label label-primary">{{ $d->nama_dp }}</span>
                            @if($d->status != 'close')
                            <br/>
                            <span>
                                <button class="btn btn-xs btn-success button_refer" id-mt="{{ $d->id_mt }}">Refer</button>
                                @if (!is_null($d->check_regu) && session('auth')->maintenance_level == 0 || session('auth')->maintenance_level != 0)
                                    <button class="btn btn-xs btn-success button_check" id-disp="{{ $d->id_mt }}">{{ empty($d->dispatch_regu_id) ? 'Dispatch' : 'Re-Dispatch' }}</button>
                                    <button class="btn btn-xs btn-danger button_delete" id-mt="{{ $d->id_mt }}">Delete</button>
                                @endif
                            </span>
                            @else
                                <br/>
                                <a href="/tech/{{ $d->id }}" class="btn btn-xs btn-success" style = "margin-top:5px;">
                                    Verifikasi
                                </a>
                            @endif
                        </td>
                        <td>
                            {{ ++$d->jml_anak }} Tiket <br/>
                            <span class="label label-primary">{{ $d->nama_order }}</span>
                        </td>
                        <td>{{ $d->action }}</td>
                        <td>{{ $d->created_at }}/{{ $durasi }}</td>
                        <td>{{ $d->tgl_selesai }}</td>
                        <td>{{ $d->dispatch_regu_name }}<br/><span class="label label-success">{{ $d->nama1 }}&{{ $d->nama2 }}</span></td>
                        <td>{{ $d->sto }}</td>
                        <td>{{ $d->kandatel }}</td>
                        <td>{{ $d->order_from }}</td>
                        <td>{{ $d->nama_order }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

@endsection