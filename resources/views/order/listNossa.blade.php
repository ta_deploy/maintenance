@extends('layout2')
@section('head')
    <style type="text/css">
        .container {
            width: 100%;
            margin: 5px;
        }
        .table-condensed{
          font-size: 10px;
        }
        .head{
            color: #303030;
        }
    </style>
    <link rel="stylesheet" href="/manual_components/fixed-table-master/fixed-table.css"/>
@endsection
@section('content')
<div class="panel panel-default" id="info">
    <div class="panel-heading">LIST TIKET NOSSA Non Remo</div>
     <div id="fixed-table-container-demo" class="fixed-table-container">
        <table class="table table-bordered table-fixed">
            <thead>
            <tr>
                <th class="head">INCIDENT</th>
                <th class="head">Customer Name</th>
                <th class="head">Summary</th>
                <th class="head">Assigned to</th>
                <th class="head">Customer Segment</th>
                <th class="head">Service ID</th>
                <th class="head">Service No</th>
                <th class="head">Service Type</th>
                <th class="head">Service Date</th>
                <th class="head">Status</th>
                <th class="head">Status Date</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td class="head">
                        @if($d->isreg)
                            {{ ++$no }}. {{ $d->Incident }}
                        @else
                            <a href="/inossa/{{ $d->Incident }}">{{ ++$no }}. {{ $d->Incident }}</a>
                        @endif
                        </td>
                    <td>{{ $d->Customer_Name }}</td>
                    <td>{{ $d->Summary }}</td>
                    <td>{{ $d->Assigned_to }}</td>
                    <td>{{ $d->Customer_Segment }}</td>
                    <td>{{ $d->Service_ID }}</td>
                    <td>{{ $d->Service_No }}</td>
                    <td>{{ $d->Service_Type }}</td>
                    <td>{{ $d->Reported_Date }}</td>
                    <td>{{ $d->Status }}</td>
                    <td>{{ $d->Status_Date }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('script')
        <script src="/manual_components/fixed-table-master/fixed-table.js"></script>
        <script type="text/javascript">
            var fixedTable = fixTable(document.getElementById('fixed-table-container-demo'));

        </script>
@endsection